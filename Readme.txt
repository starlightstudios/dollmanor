============================================== Readme =============================================
String Tyrant Full v103

Primary website: https://hopremastered.wordpress.com/string-tyrant/
Contact email: Pandemonium AT starlightstudios DAWT org

--[[ Common System Questions ]]--
1) Where are the saves and my configuration stored?
   
   Your savefiles are stored in the Saves/ folder locally. Your control configurations are also saved
   there. Engine configurations are stored in Config_Engine.lua and Config_StringTyrant.lua.

2) What are the different executables for?
 
   These are here for players using older hardware. Sometimes, Allegro or SDL are unable to acquire
   the screen due to driver issues and the game cannot start. If this happens, use one of the 
   other executables and hope that the issue is fixed. Most players report that one or the other
   executable works, and both are otherwise identical.
   The SDLFMOD executable uses FMOD instead of Bass for audio, and should only be used if the
   program's audio fails to boot.

3) What are the different .bat files for?

   A batch file is provided that disable audio playback automatically. This may help if your system
   crashes when booting the sound engine for some reason.
