//--[Version 1.00]
*Initial Release

//--[Version 1.01]
*Added 'dark' and 'light' UI themes. You can toggle them from the in-game options menu.
*Added ending relive to the options menu. You can replay endings you've already unlocked.
*Increased text window text size. Can be toggled from the in-game options menu.
*Added itch.io manifest so the app doesn't launch the no-sound file.
*Fixed the tutorial text concerning doors.
*The 'think' handler in the tutorial will now display unique text.
*Fixed a bug that caused items to not appear on the locality window.
*Modified some colors to be easier on the eyes.
*Fixed a certain entity not appearing on the entity list until the next turn.
*Fixed clay infection not showing the correct image after loading the game. (Does not affect existing saves).
*Changed the dialogue for meeting Lauren if you have not met Pygmalie yet.
*Added a notification to talk to Jessie/Lauren the first time you meet them.
*Stranger will now only start to spawn more aggressively after completing two major traps. Should make early game exploring easier.
*Updated the 'think' text for the early part of the game to give a few more hints on what to do.

//--[Version 1.02]
*Fixed Jessie not having dialogue when spoken to while in the main hall as you go to find Lauren.
*Fixed the map piece in the chapel showing the wrong area.
*Fixed several typos.
*Fixed storybooks not costing a turn to drop.
*Fixed incorrect door highlighting with mouseover for west/east doors in some rooms.
*Fullscreen, Dark Mode, and Large Text size options are now saved between playthroughs.
*You can now set doll type/color preference from the options menu after getting any game-over.
*Options are now split into their own configuration files.
*Added OSX/Linux ports.

//--[Version 1.02a]
*OSX port now uses local SDL dylib.
*Fixed a bug where the doll transformation images are not visible.

//--[Version 1.03]
*Fixed loading from title showing "Dogs" instead of "No files found" if no files are found.
*Fixed being unable to unequip gloves. Apparently, you just take them off. Also fixed it in the game.
*Fixed fog/darkness overlays not changing when loading the game in a foggy/dark area.
*Fixed clicking the locality scrollbar affecting the text scrollbar.
*Fixed high numbered endings not displaying their description on the endings menu.
*Typing 'options', 'menu', or 'settings' will now open the in-game options menu.
*Expanded Eileen's dialogue to cover more situations, including late-game ones.

//--[Version 1.04]
*Fixed OSX executable to work on more distros

//--[Version 1.05]
*Fixed the kerning up for the large font.
*Changed HP bar to render in front of enemy portrait.
*Fixed SDL version showing saves in the incorrect order.
*Fixed some typos.

//--[Version 1.06]
*Fixed a map bug on the second floor
*Fixed an error with Eileen's dialogue.
*Fixed a bug where "Welcome to the Manor" was being awarded during the tutorial.
*Fixed keyboard settings not respecting non-English keyboard layouts in SDL.
*Added whitespace trimmer to text parser.
*Alt and Tab no longer count as keypresses so you can switch programs without advancing the text.

//--[Version 1.07]
*Fixed a bug when reading journal entry 4.
*Fixed not reverting Mary's portrait during the Grand Archives trap if completed by finding the crystal.

//--[Version 1.10]
*Fixed several typos.
*Updated engine to handle translations
*Can no longer merge the crystal pieces when a monster.
*Taking items in the trapped house will no longer trigger a reaction message when a monster.
*Fixed a few examination/dialogue cases with monsters when as specific monsters.
*Mary's Think command should no longer reference Pygmalie if you haven't met her.
*Taking Jessie and Lauren into the strangely lit hall won't cause them to remain at the entrance if you're a monster.
*Fixed potions used in combat not being removed when battle ends.
