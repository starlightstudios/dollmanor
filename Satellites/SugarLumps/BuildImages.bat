SugarLumps.exe -Redirect "BuildImagesLog.txt" -Call BuildImages.lua
cd Output/
if exist "DollManor.slf" (
	copy "DollManor.slf" "../../../Games/DollManor/Datafiles/DollManor.slf"
	del "DollManor.slf"
	echo "Copying DollManor.slf"
)
if exist "StringTyrantDemo.slf" (
	copy "StringTyrantDemo.slf" "../../../Games/DollManorDemo/Datafiles/StringTyrantDemo.slf"
	del "StringTyrantDemo.slf"
	echo "Copying StringTyrantDemo.slf"
)
if exist "UITextAdventure.slf" (
	copy "UITextAdventure.slf" "../../../Games/DollManor/Datafiles/UITextAdventure.slf"
	copy "UITextAdventure.slf" "../../../Games/DollManorDemo/Datafiles/UITextAdventure.slf"
	copy "UITextAdventure.slf" "../../../../adventure/Games/ElectrospriteTextAdventure/Datafiles/UITextAdventure.slf"
	copy "UITextAdventure.slf" "../../../../adventure/Games/AdventureMode/Datafiles/UITextAdventure.slf"
	del "UITextAdventure.slf" /q
	echo "Copying UITextAdventure.slf"
)
if exist "StringTyrantTitle.slf" (
	copy "StringTyrantTitle.slf" "../../../Games/DollManor/Datafiles/StringTyrantTitle.slf"
	del "StringTyrantTitle.slf" 
	echo "Copying StringTyrantTitle.slf"
)
if exist "STLoad.slf" (
	copy "STLoad.slf" "../../../Games/DollManor/Datafiles/STLoad.slf"
	del "STLoad.slf" 
	echo "Copying STLoad.slf"
)