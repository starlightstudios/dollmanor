--[Image Compression Execution]
--Execs image compression for your project.
Debug_PushPrint(false, "Beginning image compression\n")

--Debug flags.
local sCurrentPath = fnResolvePath()

--[Automated Compression Function]
local fnCompressFolder = function(sFolderName, sExecFile)

	--Setup
	local bCompressedAnything = false
	local sCurrentPath = fnResolvePath()

	--Push the stack.
	TS_PushStack()

		--Get starting data.
		TS_LoadFrom(sCurrentPath .. sFolderName .. ".log")

		--Scan for new files/modifications/removals.
		local bHasDirectoryChanged = TS_ScanDirectory(sCurrentPath .. sFolderName .. "/")
		if(bHasDirectoryChanged) then
			--Compress here.
			LM_ExecuteScript(sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua")
			io.write("Files have changed!\n")
			io.write("Exec: " .. sCurrentPath .. sFolderName .. "/" .. sExecFile .. ".lua" .. "\n")
			bCompressedAnything = true
			
			--Save a log file.
			TS_PrintTo(sCurrentPath .. sFolderName .. ".log")
		else
			io.write("Files have not changed!\n")
		end
	
	--Clean up.
	TS_PopStack()
	return bCompressedAnything
end

--Setup.
local baCompressionFlags = {}

--Auto-exec the folders.
gbUseLowDefinition = false
gbUseEnhancedGraphics = false
ImageLump_SetAllowLineCompression(true)
baCompressionFlags[ 1] = fnCompressFolder("DollManor", "ZRouting")
baCompressionFlags[ 2] = fnCompressFolder("UITextAdventure", "ZRouting")
baCompressionFlags[ 3] = fnCompressFolder("STTitle", "ZRouting")
baCompressionFlags[ 4] = fnCompressFolder("STLoad", "ZRouting")
ImageLump_SetAllowLineCompression(false)

--Low-definition stuff.
gbUseLowDefinition = true
ImageLump_SetAllowLineCompression(true)
--Insert!
ImageLump_SetAllowLineCompression(false)
gbUseLowDefinition = false

--Done
Debug_PopPrint("Finished image compression activities!\n")