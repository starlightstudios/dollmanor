--[ ==================================== String Tyrant Assets =================================== ]
--Images specific to the Doll Manor text game.
local sBasePath = fnResolvePath()
local ciNoTransparencies = 16

--Setup
SLF_Open("Output/StringTyrantTitle.slf")
ImageLump_SetCompression(1)

--[ ======================================== Title Screen ======================================= ]
ImageLump_Rip("Map|Layer0",  sBasePath .. "Title0.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Map|Layer1",  sBasePath .. "Title1.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Map|Layer2",  sBasePath .. "Title2.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Map|Layer3",  sBasePath .. "Title3.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Map|Layer4",  sBasePath .. "Title4.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Map|Layer5",  sBasePath .. "Title5.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Map|Layer6",  sBasePath .. "Title6.png",    0, 0, -1, -1, ciNoTransparencies)

--[ ========================================= Finish Up ========================================= ]
SLF_Close()
