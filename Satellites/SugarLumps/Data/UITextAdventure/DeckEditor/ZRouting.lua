--[ ====================================== Deck Editor UI ======================================= ]
--UI that allows the player to edit their card deck.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "TxtDeckEd|"
local saNames = {"Backing", "ButtonCancel", "ButtonReset", "ButtonSave", "ButtonSmallAdd", "ButtonSmallSub", "ButtonQuestion", "HelpInlay"}
local saPaths = {"Backing", "ButtonCancel", "ButtonReset", "ButtonSave", "ButtonSmallAdd", "ButtonSmallSub", "ButtonQuestion", "HelpInlay"}

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
