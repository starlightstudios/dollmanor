--[ ========================================= Dialogue UI ========================================= ]
--Dialogue, shows up during the endings.
local sBasePath = fnResolvePath()
ImageLump_Rip("TxtDialogue|ST|BorderCard",  sBasePath .. "BorderCard.png",  0, 0, -1, -1, 0)
ImageLump_Rip("TxtDialogue|ST|NamelessBox", sBasePath .. "NamelessBox.png", 0, 0, -1, -1, 0)
ImageLump_Rip("TxtDialogue|ST|NamePanel",   sBasePath .. "NamePanel.png",   0, 0, -1, -1, 0)
