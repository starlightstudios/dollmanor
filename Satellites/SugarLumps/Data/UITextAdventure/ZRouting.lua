--[UI Text Adventure]
--UI for Text Adventure modes. This includes the "basic" and "advanced" cases. Both borrow from the same files.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/UITextAdventure.slf")
ImageLump_SetCompression(1)

--[UI Components]
--Border Card
ImageLump_Rip("CombatWordBorderCard", sBasePath .. "CombatWordBorderCard.png", 0, 0, -1, -1, 0)

--Navigation Buttons
ImageLump_Rip("Navigation|Look",     sBasePath .. "NavButtons.png",  0,   0,  48, 48, 0)
ImageLump_Rip("Navigation|North",    sBasePath .. "NavButtons.png", 48,   0,  48, 48, 0)
ImageLump_Rip("Navigation|Up",       sBasePath .. "NavButtons.png", 96,   0,  48, 48, 0)
ImageLump_Rip("Navigation|West",     sBasePath .. "NavButtons.png",  0,  48,  48, 48, 0)
ImageLump_Rip("Navigation|Wait",     sBasePath .. "NavButtons.png", 48,  48,  48, 48, 0)
ImageLump_Rip("Navigation|East",     sBasePath .. "NavButtons.png", 96,  48,  48, 48, 0)
ImageLump_Rip("Navigation|Think",    sBasePath .. "NavButtons.png",  0,  96,  48, 48, 0)
ImageLump_Rip("Navigation|South",    sBasePath .. "NavButtons.png", 48,  96,  48, 48, 0)
ImageLump_Rip("Navigation|Down",     sBasePath .. "NavButtons.png", 96,  96,  48, 48, 0)
ImageLump_Rip("Navigation|ZoomBar",  sBasePath .. "NavButtons.png",  0, 144, 143, 17, 0)
ImageLump_Rip("Navigation|ZoomTick", sBasePath .. "NavButtons.png",  0, 161,   7, 11, 0)

--[Subfiles]
LM_ExecuteScript(sBasePath .. "Dialogue/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "DeckEditor/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Electrosprite/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "Options/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "StringTyrant/ZRouting.lua")

--Finish
SLF_Close()