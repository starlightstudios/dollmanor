--[Options Menu]
--Rip it.
local sBasePath = fnResolvePath()
ImageLump_Rip("TxtOptions|Backing",    sBasePath .. "OptionsBack.png",    0, 0, -1, -1, 0)
ImageLump_Rip("TxtOptions|BtnBack",    sBasePath .. "OptionsBtnBack.png", 0, 0, -1, -1, 0)
ImageLump_Rip("TxtOptions|BtnCancel",  sBasePath .. "OptionsCancel.png",  0, 0, -1, -1, 0)
ImageLump_Rip("TxtOptions|BtnSave",    sBasePath .. "OptionsSave.png",    0, 0, -1, -1, 0)
ImageLump_Rip("TxtOptions|BtnEndings", sBasePath .. "OptionsEndings.png", 0, 0, -1, -1, 0)
ImageLump_Rip("TxtOptions|Confirm",    sBasePath .. "OptionsConfirm.png", 0, 0, -1, -1, 0)