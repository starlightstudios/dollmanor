--[String Tyrant UI]
--Most of the UI that appears on the non-combat parts of the game.
local sBasePath = fnResolvePath()

--Variable setup.
local sPrefix = "TxtDialogue|EA|"
local saNames = {"BorderCard", "GearWheel", "Mask_CharacterRender", "Mask_LocalityRender", "Mask_TextRender", "Mask_WorldRender", "PopupBorderCard", "TextAdventureMapParts", "TextAdventureScrollbar", "TextInput", "TextInputDark"}
local saPaths = saNames

--Ripping loop.
local i = 1
while(saNames[i] ~= nil) do
	ImageLump_Rip(sPrefix .. saNames[i], sBasePath .. saPaths[i] .. ".png", 0, 0, -1, -1, 0)
	i = i + 1
end
