--[ ==================================== String Tyrant Assets =================================== ]
--Images specific to the Doll Manor text game.
local sBasePath = fnResolvePath()
local ciNoTransparencies = 16

--Setup
SLF_Open("Output/DollManor.slf")
ImageLump_SetCompression(1)

--[ ======================================= Maps and Tiles ====================================== ]
--Tile Sheet
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Tileset", sBasePath .. "Tiles/TileSheet.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)

--Map.
ImageLump_Rip("Map|Base",    sBasePath .. "Map/Base.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer00", sBasePath .. "Map/Layer00.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer01", sBasePath .. "Map/Layer01.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer02", sBasePath .. "Map/Layer02.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer03", sBasePath .. "Map/Layer03.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer04", sBasePath .. "Map/Layer04.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer05", sBasePath .. "Map/Layer05.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer06", sBasePath .. "Map/Layer06.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer07", sBasePath .. "Map/Layer07.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Map|Layer08", sBasePath .. "Map/Layer08.png", 0, 0, -1, -1, 0)

--[ ========================================== Overlays ========================================= ]
ImageLump_Rip("Overlay|Black0", sBasePath .. "Overlays/Black0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Black1", sBasePath .. "Overlays/Black1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Fog0",   sBasePath .. "Overlays/Fog0.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Fog1",   sBasePath .. "Overlays/Fog1.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Fog2",   sBasePath .. "Overlays/Fog2.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Fog3",   sBasePath .. "Overlays/Fog3.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Fog4",   sBasePath .. "Overlays/Fog4.png",   0, 0, -1, -1, 0)

--Big, with torch.
ImageLump_Rip("Overlay|Black0Big", sBasePath .. "Overlays/Black0Big.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|Fog0Big",   sBasePath .. "Overlays/Fog0Big.png",   0, 0, -1, -1, 0)

--UI Stuff
ImageLump_Rip("Overlay|Move",  sBasePath .. "Overlays/MoveOverlay.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|DoorH", sBasePath .. "Overlays/DoorOverlayH.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Overlay|DoorV", sBasePath .. "Overlays/DoorOverlayV.png", 0, 0, -1, -1, 0)

--[ ========================================= Ending CGs ======================================== ]
ImageLump_Rip("Ending|BigHug",    sBasePath .. "Endings/BigHug.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Ending|Escape_JL", sBasePath .. "Endings/Escape_JL.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Ending|Escape_MJ", sBasePath .. "Endings/Escape_MJ.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Ending|Escape_ML", sBasePath .. "Endings/Escape_ML.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Ending|Claygirls", sBasePath .. "Endings/Claygirls.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Ending|Dolls",     sBasePath .. "Endings/Dolls.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Ending|DollsMary", sBasePath .. "Endings/DollsMary.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Ending|Statues",   sBasePath .. "Endings/Statues.png",   0, 0, -1, -1, 0)

--[ ============================================ Mary =========================================== ]
--Mary.
ImageLump_Rip("Mary",           sBasePath .. "HumanMary/Mary.png",           0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MarySecretClay", sBasePath .. "HumanMary/MarySecretClay.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MarySecretDoll", sBasePath .. "HumanMary/MarySecretDoll.png", 0, 0, -1, -1, ciNoTransparencies)

--Clay
ImageLump_Rip("MaryClayBlueTF0",   sBasePath .. "HumanMary/MaryClayBlueTF0.png",   0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryClayRedTF0",    sBasePath .. "HumanMary/MaryClayRedTF0.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryClayYellowTF0", sBasePath .. "HumanMary/MaryClayYellowTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Doll TF
ImageLump_Rip("MaryDollTF0", sBasePath .. "HumanMary/MaryDollTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryDollTF1", sBasePath .. "HumanMary/MaryDollTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--Glass
ImageLump_Rip("MaryGlass",    sBasePath .. "HumanMary/MaryGlass.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryGlassTF0", sBasePath .. "HumanMary/MaryGlassTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Ice
ImageLump_Rip("MaryIce",    sBasePath .. "HumanMary/MaryIce.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryIceTF0", sBasePath .. "HumanMary/MaryIceTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryIceTF1", sBasePath .. "HumanMary/MaryIceTF1.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryIceTF2", sBasePath .. "HumanMary/MaryIceTF2.png", 0, 0, -1, -1, ciNoTransparencies)

--Stone
ImageLump_Rip("MaryStone",    sBasePath .. "HumanMary/MaryStone.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStoneTF0", sBasePath .. "HumanMary/MaryStoneTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStoneTF1", sBasePath .. "HumanMary/MaryStoneTF1.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStoneTF2", sBasePath .. "HumanMary/MaryStoneTF2.png", 0, 0, -1, -1, ciNoTransparencies)

--Mary's String Trap TF
ImageLump_Rip("MaryStringTrap0", sBasePath .. "HumanMary/StringTrap0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStringTrap1", sBasePath .. "HumanMary/StringTrap1.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStringTrap2", sBasePath .. "HumanMary/StringTrap2.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStringTrap3", sBasePath .. "HumanMary/StringTrap3.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryStringTrap4", sBasePath .. "HumanMary/StringTrap4.png", 0, 0, -1, -1, ciNoTransparencies)

--Mary's Rubber Trap TF
ImageLump_Rip("MaryRubberTrap0",  sBasePath .. "HumanMary/RubberTrap0.png",  0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryRubberTrap1",  sBasePath .. "HumanMary/RubberTrap1.png",  0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryRubberTrap2",  sBasePath .. "HumanMary/RubberTrap2.png",  0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("MaryRubberTrap3",  sBasePath .. "HumanMary/RubberTrap3.png",  0, 0, -1, -1, ciNoTransparencies)

--Rubber Trap CG
ImageLump_Rip("MaryRubberFinale", sBasePath .. "HumanMary/RubberFinale.png", 0, 0, -1, -1, ciNoTransparencies)

--[ =========================================== Jessie ========================================== ]
--Jessie, normal.
ImageLump_Rip("Jessie", sBasePath .. "HumanJessie/Jessie.png", 0, 0, -1, -1, ciNoTransparencies)

--TF, General
ImageLump_Rip("JessieGeneralTF0", sBasePath .. "HumanJessie/JessieGeneralTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Special Bride TF
ImageLump_Rip("JessieStringTrap0", sBasePath .. "HumanJessie/JessieBrideTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieStringTrap1", sBasePath .. "HumanJessie/JessieBrideTF1.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieStringTrap2", sBasePath .. "HumanJessie/JessieBrideTF2.png", 0, 0, -1, -1, ciNoTransparencies)

--Clay
ImageLump_Rip("JessieClayBlueTF0",   sBasePath .. "HumanJessie/JessieClayBlueTF0.png",   0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieClayRedTF0",    sBasePath .. "HumanJessie/JessieClayRedTF0.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieClayYellowTF0", sBasePath .. "HumanJessie/JessieClayYellowTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Doll TF
ImageLump_Rip("JessieDollTF0", sBasePath .. "HumanJessie/JessieDollTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieDollTF1", sBasePath .. "HumanJessie/JessieDollTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--Glass
ImageLump_Rip("JessieGlass",    sBasePath .. "HumanJessie/JessieGlass.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieGlassTF0", sBasePath .. "HumanJessie/JessieGlassTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Ice
ImageLump_Rip("JessieIce",    sBasePath .. "HumanJessie/JessieIce.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieIceTF0", sBasePath .. "HumanJessie/JessieIceTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieIceTF1", sBasePath .. "HumanJessie/JessieIceTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--Stone
ImageLump_Rip("JessieStone",    sBasePath .. "HumanJessie/JessieStone.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieStoneTF0", sBasePath .. "HumanJessie/JessieStoneTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("JessieStoneTF1", sBasePath .. "HumanJessie/JessieStoneTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--[ =========================================== Lauren ========================================== ]
--Lauren, normal.
ImageLump_Rip("Lauren", sBasePath .. "HumanLauren/Lauren.png", 0, 0, -1, -1, ciNoTransparencies)

--TF, General
ImageLump_Rip("LaurenGeneralTF0", sBasePath .. "HumanLauren/LaurenGeneralTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Clay.
ImageLump_Rip("LaurenClayBlueTF0",   sBasePath .. "HumanLauren/LaurenClayBlueTF0.png",   0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenClayRedTF0",    sBasePath .. "HumanLauren/LaurenClayRedTF0.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenClayYellowTF0", sBasePath .. "HumanLauren/LaurenClayYellowTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Doll TF
ImageLump_Rip("LaurenDollTF0", sBasePath .. "HumanLauren/LaurenDollTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenDollTF1", sBasePath .. "HumanLauren/LaurenDollTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--Glass
ImageLump_Rip("LaurenGlass",    sBasePath .. "HumanLauren/LaurenGlass.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenGlassTF0", sBasePath .. "HumanLauren/LaurenGlassTF0.png", 0, 0, -1, -1, ciNoTransparencies)

--Ice
ImageLump_Rip("LaurenIce",    sBasePath .. "HumanLauren/LaurenIce.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenIceTF0", sBasePath .. "HumanLauren/LaurenIceTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenIceTF1", sBasePath .. "HumanLauren/LaurenIceTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--Stone
ImageLump_Rip("LaurenStone",    sBasePath .. "HumanLauren/LaurenStone.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenStoneTF0", sBasePath .. "HumanLauren/LaurenStoneTF0.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("LaurenStoneTF1", sBasePath .. "HumanLauren/LaurenStoneTF1.png", 0, 0, -1, -1, ciNoTransparencies)

--[ ================================ Other Characters and Scenes ================================ ]
--Pygmalie. Has no TFs.
ImageLump_Rip("Pygmalie", sBasePath .. "Other/Pygmalie.png",  0, 0, -1, -1, ciNoTransparencies)

--Doll Crowd Shot. Used during one bad end.
ImageLump_Rip("Doll Crowd Shot", sBasePath .. "Other/Doll Crowd Shot.png", 0, 0, -1, -1, ciNoTransparencies)

--The Stranger
ImageLump_Rip("Stranger0", sBasePath .. "Other/Stranger0.png",  0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Stranger1", sBasePath .. "Other/Stranger1.png",  0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Stranger2", sBasePath .. "Other/Stranger2.png",  0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("Stranger3", sBasePath .. "Other/Stranger3.png",  0, 0, -1, -1, ciNoTransparencies)

--Sarah the Book
ImageLump_Rip("Sarah", sBasePath .. "Other/Sarah.png",  0, 0, -1, -1, ciNoTransparencies)

--[ ======================================== Blank Doll ========================================= ]
--Blank Dolls. Used during Doll TF sequence.
ImageLump_Rip("BlankDollA",      sBasePath .. "BlankDoll/BlankDollA.png",      0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollB",      sBasePath .. "BlankDoll/BlankDollB.png",      0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollC",      sBasePath .. "BlankDoll/BlankDollC.png",      0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollD",      sBasePath .. "BlankDoll/BlankDollD.png",      0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollE",      sBasePath .. "BlankDoll/BlankDollE.png",      0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollUpperA", sBasePath .. "BlankDoll/BlankDollUpperA.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollUpperB", sBasePath .. "BlankDoll/BlankDollUpperB.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollUpperC", sBasePath .. "BlankDoll/BlankDollUpperC.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollUpperD", sBasePath .. "BlankDoll/BlankDollUpperD.png", 0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("BlankDollUpperE", sBasePath .. "BlankDoll/BlankDollUpperE.png", 0, 0, -1, -1, ciNoTransparencies)

--[ =================================== Dressed Dolls and TFs =================================== ]
--All six doll types used the same patterns.
local iTotal = 6
local saPatterns = {"Bride", "Dancer", "Geisha", "Goth", "Princess", "Punk"}
local saDirs = {"DollBride/", "DollDancer/", "DollGeisha/", "DollGoth/", "DollPrincess/", "DollPunk/"}
for i = 1, iTotal, 1 do
    
    --Get the pattern.
    local sPattern = saPatterns[i]
    
    --Doll Standing.
    ImageLump_Rip("Doll " .. sPattern .. " A", sBasePath .. saDirs[i] .. "Doll " .. sPattern .. " A.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip("Doll " .. sPattern .. " B", sBasePath .. saDirs[i] .. "Doll " .. sPattern .. " B.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip("Doll " .. sPattern .. " C", sBasePath .. saDirs[i] .. "Doll " .. sPattern .. " C.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip("Doll " .. sPattern .. " D", sBasePath .. saDirs[i] .. "Doll " .. sPattern .. " D.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip("Doll " .. sPattern .. " E", sBasePath .. saDirs[i] .. "Doll " .. sPattern .. " E.png", 0, 0, -1, -1, ciNoTransparencies)
    
    --Doll TF Base (skin)
    ImageLump_Rip(sPattern .. "TFBaseA", sBasePath .. saDirs[i] .. sPattern .. "TFBaseA.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TFBaseB", sBasePath .. saDirs[i] .. sPattern .. "TFBaseB.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TFBaseC", sBasePath .. saDirs[i] .. sPattern .. "TFBaseC.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TFBaseD", sBasePath .. saDirs[i] .. sPattern .. "TFBaseD.png", 0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TFBaseE", sBasePath .. saDirs[i] .. sPattern .. "TFBaseE.png", 0, 0, -1, -1, ciNoTransparencies)
    
    --Doll TF sequence (eyes, clothes, makeup)
    ImageLump_Rip(sPattern .. "TF0",     sBasePath .. saDirs[i] .. sPattern .. "TF0.png",     0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TF1",     sBasePath .. saDirs[i] .. sPattern .. "TF1.png",     0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TF2",     sBasePath .. saDirs[i] .. sPattern .. "TF2.png",     0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TF3",     sBasePath .. saDirs[i] .. sPattern .. "TF3.png",     0, 0, -1, -1, ciNoTransparencies)
    ImageLump_Rip(sPattern .. "TF4",     sBasePath .. saDirs[i] .. sPattern .. "TF4.png",     0, 0, -1, -1, ciNoTransparencies)
    
end

--[ ======================================== Clay Girls ========================================= ]
ImageLump_Rip("ClayGirlBlue",   sBasePath .. "Claygirl/ClayGirlBlue.png",   0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("ClayGirlRed",    sBasePath .. "Claygirl/ClayGirlRed.png",    0, 0, -1, -1, ciNoTransparencies)
ImageLump_Rip("ClayGirlYellow", sBasePath .. "Claygirl/ClayGirlYellow.png", 0, 0, -1, -1, ciNoTransparencies)

--[ ========================================= World UI ========================================== ]
--Navigation
ImageLump_Rip("Navigation|Look",     sBasePath .. "UIWorld/NavButtons.png",  0,   0,  48, 48, 0)
ImageLump_Rip("Navigation|North",    sBasePath .. "UIWorld/NavButtons.png", 48,   0,  48, 48, 0)
ImageLump_Rip("Navigation|Up",       sBasePath .. "UIWorld/NavButtons.png", 96,   0,  48, 48, 0)
ImageLump_Rip("Navigation|West",     sBasePath .. "UIWorld/NavButtons.png",  0,  48,  48, 48, 0)
ImageLump_Rip("Navigation|Wait",     sBasePath .. "UIWorld/NavButtons.png", 48,  48,  48, 48, 0)
ImageLump_Rip("Navigation|East",     sBasePath .. "UIWorld/NavButtons.png", 96,  48,  48, 48, 0)
ImageLump_Rip("Navigation|Think",    sBasePath .. "UIWorld/NavButtons.png",  0,  96,  48, 48, 0)
ImageLump_Rip("Navigation|South",    sBasePath .. "UIWorld/NavButtons.png", 48,  96,  48, 48, 0)
ImageLump_Rip("Navigation|Down",     sBasePath .. "UIWorld/NavButtons.png", 96,  96,  48, 48, 0)
ImageLump_Rip("Navigation|ZoomBar",  sBasePath .. "UIWorld/NavButtons.png",  0, 144, 143, 17, 0)
ImageLump_Rip("Navigation|ZoomTick", sBasePath .. "UIWorld/NavButtons.png",  0, 161,   7, 11, 0)

--Locality
local ciLocalityW = 32
local ciLocalityH = 32
ImageLump_Rip("Locality|Commands|Up",  sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 0, ciLocalityH * 0, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Move|Up",      sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 1, ciLocalityH * 0, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Doors|Up",     sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 2, ciLocalityH * 0, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Inventory|Up", sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 3, ciLocalityH * 0, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Entities|Up",  sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 0, ciLocalityH * 1, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Items|Up",     sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 1, ciLocalityH * 1, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Equipment|Up", sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 2, ciLocalityH * 1, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Commands|Dn",  sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 0, ciLocalityH * 2, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Move|Dn",      sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 1, ciLocalityH * 2, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Doors|Dn",     sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 2, ciLocalityH * 2, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Inventory|Dn", sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 3, ciLocalityH * 2, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Entities|Dn",  sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 0, ciLocalityH * 3, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Items|Dn",     sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 1, ciLocalityH * 3, ciLocalityW, ciLocalityH, 0)
ImageLump_Rip("Locality|Equipment|Dn", sBasePath .. "UIWorld/LocaleButtons.png", ciLocalityW * 2, ciLocalityH * 3, ciLocalityW, ciLocalityH, 0)

--[ ========================================= Combat UI ========================================= ]
--Unused Combat UI
ImageLump_Rip("CombatWordBorderCard", sBasePath .. "CombatWordBorderCard.png", 0, 0, -1, -1, 0)

--Combat UI.
local sCombatPath = sBasePath .. "UICombat/"
ImageLump_Rip("Combat|AttackBarFrame",         sCombatPath .. "AttackBarFrame.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Combat|AttackBarFill",          sCombatPath .. "AttackBarFill.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Bar_Heart",              sCombatPath .. "Bar_Heart.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Bar_Half",               sCombatPath .. "Bar_Half.png",               0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Bar_Empty",              sCombatPath .. "Bar_Empty.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Bar_Shield",             sCombatPath .. "Bar_Shield.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Bar_ShieldOrbit",        sCombatPath .. "Bar_ShieldOrbit.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Ace",                sCombatPath .. "Btn_Ace.png",                0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Act",                sCombatPath .. "Btn_Act.png",                0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Draw",               sCombatPath .. "Btn_Draw.png",               0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Potion",             sCombatPath .. "Btn_Potion.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Shuffle",            sCombatPath .. "Btn_Shuffle.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Shuffle_Highlight",  sCombatPath .. "Btn_Shuffle_Highlight.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Btn_Surrender",          sCombatPath .. "Btn_Surrender.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_And",               sCombatPath .. "Card_And.png",               0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Of",                sCombatPath .. "Card_Of.png",                0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_AndOfHighlight",    sCombatPath .. "Card_AndOfHighlight.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Attack",            sCombatPath .. "Card_Attack.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Death",             sCombatPath .. "Card_Death.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Defend",            sCombatPath .. "Card_Defend.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Earth",             sCombatPath .. "Card_Earth.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Fire",              sCombatPath .. "Card_Fire.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Highlight",         sCombatPath .. "Card_Highlight.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Joker",             sCombatPath .. "Card_Joker.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Life",              sCombatPath .. "Card_Life.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Water",             sCombatPath .. "Card_Water.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Card_Wind",              sCombatPath .. "Card_Wind.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|ExpandableFrameL",       sCombatPath .. "ExpandableFrameL.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Combat|ExpandableFrameM",       sCombatPath .. "ExpandableFrameM.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Combat|ExpandableFrameR",       sCombatPath .. "ExpandableFrameR.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Combat|HealthBarFrame",         sCombatPath .. "HealthBarFrame.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Combat|HealthBarFill",          sCombatPath .. "HealthBarFill.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Attack",            sCombatPath .. "Icon_Attack.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Death",             sCombatPath .. "Icon_Death.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Defend",            sCombatPath .. "Icon_Defend.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Earth",             sCombatPath .. "Icon_Earth.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Fire",              sCombatPath .. "Icon_Fire.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Life",              sCombatPath .. "Icon_Life.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Water",             sCombatPath .. "Icon_Water.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Icon_Wind",              sCombatPath .. "Icon_Wind.png",              0, 0, -1, -1, 0)
ImageLump_Rip("Combat|Static_WordDivider",     sCombatPath .. "Static_WordDivider.png",     0, 0, -1, -1, 0)

--[ ========================================= Finish Up ========================================= ]
SLF_Close()

--[Call Demo Version]
LM_ExecuteScript(fnResolvePath() .. "ZRoutingDemo.lua")
