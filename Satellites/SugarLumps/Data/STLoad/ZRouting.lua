--[ =================================== String Tyrant Loading =================================== ]
--Images for the loading screen.
local sBasePath = fnResolvePath()
local ciNoTransparencies = 16

--Setup
SLF_Open("Output/STLoad.slf")
ImageLump_SetCompression(1)

for i = 1, 160, 1 do
    local sPath = string.format("%sLoad%05i.png", sBasePath, i)
    local sRip  = string.format("Img%03i", i-1)
    ImageLump_Rip(sRip, sPath, 0, 0, -1, -1, 0)
end

SLF_Close()
