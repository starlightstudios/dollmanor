-- |[ =================================== Options Post-Exec ==================================== ]|
--Script that executes after the options menu is closed with the Save button. Does not get called
-- if the player clicks the cancel button.
local iTypeValue = OM_GetOption("String Tyrant Type Pref")
local iColorValue = OM_GetOption("String Tyrant Color Pref")

-- |[Doll Type]|
--Do nothing if the player has already been transformed.
if(gzTextVar.gzPlayer.sFormState ~= "Human") then

--Do nothing if it's zero:
elseif(iTypeValue == 0) then

--Set types.
elseif(iTypeValue == 1) then
    gzTextVar.gzPlayer.sTFSeq = "Dancer"
elseif(iTypeValue == 2) then
    gzTextVar.gzPlayer.sTFSeq = "Geisha"
elseif(iTypeValue == 3) then
    gzTextVar.gzPlayer.sTFSeq = "Princess"
elseif(iTypeValue == 4) then
    gzTextVar.gzPlayer.sTFSeq = "Goth"
elseif(iTypeValue == 5) then
    gzTextVar.gzPlayer.sTFSeq = "Bride"
elseif(iTypeValue == 6) then
    gzTextVar.gzPlayer.sTFSeq = "Punk"
end

-- |[Doll Color]|
    gzTextVar.gzPlayer.iSkinColor = gzOptions.iOverrideDollSkin
if(gzTextVar.gzPlayer.sFormState ~= "Human") then

--Reset to generated value.
elseif(iColorValue == 0) then
    gzTextVar.gzPlayer.iSkinColor = gzTextVar.gzPlayer.iBaseSkinColor

--Set types.
elseif(iColorValue >= 1 and iColorValue <= 4) then
    gzTextVar.gzPlayer.iSkinColor = iColorValue-1
end