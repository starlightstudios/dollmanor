-- |[ ==================================== Post-Title Boot ===================================== ]|
--After the title screen decides what to do, this script gets run. The title screen will populate
-- certain values with the results of player input.

--Argument Listing:
-- 0: sHandlerType - Which type of game we should boot to. Each option on the title screen picks one.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Create the level.
TL_Create()
TL_SetProperty("Construct String Tyrant")
TL_SetProperty("Set Combat Type", 2)
TL_SetProperty("Construct Deck Editor")

--Map tileset.
TL_SetProperty("Automap Tilset", "Root/Images/DollManor/Tiles/Tileset", 64, 64)
TL_SetProperty("Automap Fade Positions", 1, 17, 3, 17, 5, 17, 7, 17, 9, 17, 11, 17)
TL_SetProperty("Automap Alarm Positions", 9, 4, 10, 4)

--Difficulty override, used when restarting.
local sDifficultyOverride = gsDifficultyOverride
if(sDifficultyOverride == nil) then sDifficultyOverride = "Normal" end

--Set the root path.
gzTextVar = {}
gzTextVar.sRootPath = fnResolvePath()
gzTextVar.sDifficulty = sDifficultyOverride
gzTextVar.sManorType = "Normal" --Normal, Demo, Simple, Tutorial

--Constants structure. Parallel to gzTextVar but doesn't get saved.
gzTextCon = {}

--Audio.
TL_SetProperty("Register Music", "Doll|Ambient|Basement", "Ambient_Basement", 0.0)
TL_SetProperty("Register Music", "Doll|Ambient|Interior", "Ambient_Interior", 0.0)
TL_SetProperty("Register Music", "Doll|Ambient|Rain",     "Ambient_Rain",     0.0)

-- |[ ====================================== Type Resolve ====================================== ]|
--Based on the passed in string, figure out which game to launch. Failure launches the text menu.
if(sString == "Tutorial") then
    
    --Use the tutorial mansion.
    gzTextVar.sManorType = "Tutorial"
    gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/Tutorial/"
    
    --Difficulty is always "Normal".
    gzTextVar.sDifficulty = "Normal"
    gzTextVar.bIsCombatWaitMode = true
    
    --Execute.
    LM_ExecuteScript(gzTextVar.sRootPath .. "999 Delayed Builder.lua")
    
    --Add some text.
    TL_SetProperty("Append", "Welcome to String Tyrant. This tutorial will teach you the basics of playing the game." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To begin, there is a note on the ground near you. Type [look note] to read it. You can also use the locality window in the bottom right." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To use the locality window, click the rightmost icon to show nearby objects, and then the note. You can read it from the popup menu." .. gsDE)
    TL_SetProperty("Create Blocker")

--New Game, Difficulty is the second part of the string.
elseif(string.sub(sString, 1, 9) == "New Game|") then
    
    --Resolve difficulty.
    local iStartCombatTypeAt = 0
    local sDifficulty = string.sub(sString, 10)
    if(string.sub(sDifficulty, 1, 7) == "Normal|") then
        iStartCombatTypeAt = 8
        gzTextVar.sDifficulty = "Normal"
    elseif(string.sub(sDifficulty, 1, 5) == "Hard|") then
        iStartCombatTypeAt = 6
        gzTextVar.sDifficulty = "Hard"
    elseif(string.sub(sDifficulty, 1, 5) == "Easy|") then
        iStartCombatTypeAt = 6
        gzTextVar.sDifficulty = "Easy"
    else
        io.write("Unhandled difficulty: " .. sDifficulty .. " setting to Normal.\n")
        gzTextVar.sDifficulty = "Normal"
        gzTextVar.bIsCombatWaitMode = true
    end

    --Resolve combat type.
    local sCombatType = string.sub(sDifficulty, iStartCombatTypeAt)
    if(sCombatType == "Active") then
        gzTextVar.bIsCombatWaitMode = false
    else
        gzTextVar.bIsCombatWaitMode = true
    end

    --Normal manor type.
    gzTextVar.sManorType = "Normal"
    gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/MainManor/"
    
    --Begin introduction.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/300 Introduction.lua")
    
--New Game, No Tutorial
elseif(string.sub(sString, 1, 15) == "New Game NoTut|") then
    
    --Resolve difficulty.
    local iStartCombatTypeAt = 0
    local sDifficulty = string.sub(sString, 16)
    if(string.sub(sDifficulty, 1, 7) == "Normal|") then
        iStartCombatTypeAt = 8
        gzTextVar.sDifficulty = "Normal"
        
    elseif(string.sub(sDifficulty, 1, 5) == "Hard|") then
        iStartCombatTypeAt = 6
        gzTextVar.sDifficulty = "Hard"
        
    elseif(string.sub(sDifficulty, 1, 5) == "Easy|") then
        iStartCombatTypeAt = 6
        gzTextVar.sDifficulty = "Easy"
    else
        io.write("Unhandled difficulty: " .. sDifficulty .. " setting to Normal.\n")
        gzTextVar.sDifficulty = "Normal"
        gzTextVar.bIsCombatWaitMode = true
    end

    --Resolve combat type.
    local sCombatType = string.sub(sDifficulty, iStartCombatTypeAt)
    if(sCombatType == "Active") then
        gzTextVar.bIsCombatWaitMode = false
    else
        gzTextVar.bIsCombatWaitMode = true
    end

    --Normal manor type.
    gzTextVar.sManorType = "Normal"
    gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/MainManor/"
    
    --Bypass introduction.
    LM_ExecuteScript(gzTextVar.sRootPath .. "999 Delayed Builder.lua")

--Load Game.
elseif(string.sub(sString, 1, 10) == "Load Game|") then

    --Savefile name is after the bar.
    local sSaveName = string.sub(sString, 11)
    LM_ExecuteScript(gzTextVar.sRootPath .. "999 Delayed Builder.lua")
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Load Game.lua", sSaveName)

--Error, can't identify mode.
else
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/000 Menu Boot.lua")
end

gsDifficultyOverride = nil