-- |[ ===================================== Think Handler ====================================== ]|
--Called whenever the player uses the "think" command. This lists out the player's current objectives
-- based on what form they are in and the scenario at hand.
--Does not consume a turn.

-- |[Demo]|
if(gzTextVar.sManorType == "Tutorial") then
    TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "'I'm in a tutorial. I should [look note] to read notes on the ground. When I'm done, I should [restart] to return to the main menu.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    return
end

-- |[Human]|
if(gzTextVar.gzPlayer.sFormState == "Human") then

    --Hypnotized.
    if(gzTextVar.iHypnoticCheck > 0) then
        if(gzTextVar.iHypnoticCheck < 5) then
            TL_SetProperty("Append", "'This light is making me nauseous. Unless I have some way of dealing with it, I should get out of here.'" .. gsDE)
        elseif(gzTextVar.iHypnoticCheck < 10) then
            TL_SetProperty("Append", "'The blue light is so pretty. I should go and stare at it some more...'" .. gsDE)
        else
            TL_SetProperty("Append", "'I love the light. I should OBEY and GO TO THE LIGHT. LET IT FILL ME.'" .. gsDE)
        end
        return
    end
    
    --Ice!
    if(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.200) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'It's freezing in here. I'd better find what I'm looking for and get out. Maybe I should look for something to keep me warm?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
        
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.400) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Gah, the cold stings. I'd better find what I'm looking for and get out. Maybe I should look for something to keep me warm?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
        
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.600) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I definitely need to find something to keep me warm, this is no ordinary cold!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
        
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.800) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I don't have long. I need to get someplace warm before I freeze...'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    
    --Mary freezes. This is in a subscript.
    elseif(gzTextVar.fMaryFreezeAmount > 0.000) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'It's so cold... Gotta warm up somehow...'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    end
    
    --String trap:
    if(gzTextVar.iMaryStringTrapState == 1) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'These clothes are probably bad news. I should either figure out how this trap works or get back to safety and put on something that isn't a magical trap.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
        
    elseif(gzTextVar.iMaryStringTrapState == 2) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I think these clothes have transformed me into a doll. If I can remove them, I think the magic might fade. I don't think I'll stay myself if I get caught again.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    end
    
    --Rubber trap:
    if(gzTextVar.iGoopTFStage == 1) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'This rubbery goop seems inert. It'll probably come off if I wash it a bit. Better watch my step - I liked those shoes.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
        
    elseif(gzTextVar.iGoopTFStage == 2) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'The rubber stopped but I won't withstand another mistake. I'd better watch my step, because this rubber almost has me!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    end
    
    --Phase 1. Opening.
    if(gzTextVar.iGameStage == 1) then
        TL_SetProperty("Append", "You take a second to think." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        --Clay infection! Not normally possible without debug commands.
        if(gzTextVar.gzPlayer.iClayInfection >= 0) then
            TL_SetProperty("Append", "'I'd better find some way to wash off this clay before it gets any worse!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            --Both Jessie and Lauren need to be found:
            if(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieMainHalled == false and gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                TL_SetProperty("Append", "'After that, I should make sure Jessie and Lauren are okay. They've got to be somewhere around here.'" .. gsDE)
            
            --Jessie is following:
            elseif(gzTextVar.bIsJessieFollowing == true) then
            
                --Lauren is not found:
                if(gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                    if(gzTextVar.bMetPygmalie) then
                        TL_SetProperty("Append", "'After that, I should get Jessie someplace safe, and find Lauren. That Pygmalie lady seemed harmless, maybe the Main Hall is safe?'" .. gsDE)
                    else
                        TL_SetProperty("Append", "'I should get Jessie someplace safe, and find Lauren. Maybe there's a safe place somewhere inside this mansion?'" .. gsDE)
                    end
                
                --Lauren is also following. Not normally possible without debug commands.
                elseif(gzTextVar.bIsLaurenFollowing == true) then
                    if(gzTextVar.bMetPygmalie) then
                        TL_SetProperty("Append", "'After that, I should get Jessie and Lauren someplace safe. That Pygmalie lady seemed harmless, maybe the Main Hall is safe?'" .. gsDE)
                    else
                        TL_SetProperty("Append", "'I should get Jessie and Lauren someplace safe, and find Lauren. Maybe there's a safe place somewhere inside this mansion?'" .. gsDE)
                    end
                
                --Lauren is in the main hall:
                elseif(gzTextVar.bIsLaurenMainHalled == true) then
                    TL_SetProperty("Append", "'After that, I should get Jessie to the Main Hall. I hope Lauren is still safe there.'" .. gsDE)
                end
            
            --Lauren is following:
            elseif(gzTextVar.bIsLaurenFollowing == true) then
            
                --Jessie is not found:
                if(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieMainHalled == false) then
                    if(gzTextVar.bMetPygmalie) then
                        TL_SetProperty("Append", "'After that, I should get Lauren someplace safe, and find Jessie. That Pygmalie lady seemed harmless, maybe the Main Hall is safe?'" .. gsDE)
                    else
                        TL_SetProperty("Append", "'After that, I should get Lauren someplace safe, and find Jessie. Maybe there's a safe place somewhere inside this mansion?'" .. gsDE)
                    end
                
                --Jessie is also following. Logically unreachable code.
                elseif(gzTextVar.bIsJessieFollowing == true) then
                
                --Jessie is in the main hall:
                elseif(gzTextVar.bIsJessieMainHalled == true) then
                    TL_SetProperty("Append", "'After that, I should get Lauren to the Main Hall. I hope Jessie is still safe there. I'm sure she can take care of herself.'" .. gsDE)
                end
            
            --Nobody is following:
            else
            
                --Lauren is in the main hall:
                if(gzTextVar.bIsLaurenMainHalled == true) then
                    TL_SetProperty("Append", "'After that, I should find Jessie, and get her to the Main Hall. I hope it's safe there.'" .. gsDE)
                
                --Jessie is in the main hall:
                elseif(gzTextVar.bIsJessieMainHalled == true) then
                    TL_SetProperty("Append", "'After that, I should find Lauren before something happens to him, and get him to the Main Hall. He'll be safe there. I hope.'" .. gsDE)
                end
            
            end
        
        --Normal.
        else
            --Both Jessie and Lauren need to be found:
            if(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieMainHalled == false and gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                TL_SetProperty("Append", "'I'd better figure out what happened to Jessie and Lauren before I worry about anything else. They can't be far.'" .. gsDE)
            
            --Jessie is following:
            elseif(gzTextVar.bIsJessieFollowing == true) then
            
                --Lauren is not found:
                if(gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                    if(gzTextVar.bMetPygmalie) then
                        TL_SetProperty("Append", "'I should get Jessie someplace safe, and find Lauren. That Pygmalie lady seemed harmless, maybe the Main Hall is safe?'" .. gsDE)
                    else
                        TL_SetProperty("Append", "'I should get Jessie someplace safe, and find Lauren. Maybe there's a safe place somewhere inside the mansion?'" .. gsDE)
                    end
                
                --Lauren is also following. Not normally possible without debug commands.
                elseif(gzTextVar.bIsLaurenFollowing == true) then
                    if(gzTextVar.bMetPygmalie) then
                        TL_SetProperty("Append", "'I should get Jessie and Lauren someplace safe. That Pygmalie lady seemed harmless, maybe the Main Hall is safe?'" .. gsDE)
                    else
                        TL_SetProperty("Append", "'I should get Jessie and Lauren someplace safe. Maybe there's a safe place somewhere inside the mansion?'" .. gsDE)
                    end
                
                --Lauren is in the main hall:
                elseif(gzTextVar.bIsLaurenMainHalled == true) then
                    TL_SetProperty("Append", "'I should get Jessie to the Main Hall. I hope Lauren is still safe there.'" .. gsDE)
                end
            
            --Lauren is following:
            elseif(gzTextVar.bIsLaurenFollowing == true) then
            
                --Jessie is not found:
                if(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieMainHalled == false) then
                    if(gzTextVar.bMetPygmalie) then
                        TL_SetProperty("Append", "'I should get Lauren someplace safe, and find Jessie. That Pygmalie lady seemed harmless, maybe the Main Hall is safe?'" .. gsDE)
                    else
                        TL_SetProperty("Append", "'I should get Lauren someplace safe, and find Jessie. Maybe there's a safe place somewhere inside the mansion?'" .. gsDE)
                    end
                
                --Jessie is also following. Logically unreachable code.
                elseif(gzTextVar.bIsJessieFollowing == true) then
                
                --Jessie is in the main hall:
                elseif(gzTextVar.bIsJessieMainHalled == true) then
                    TL_SetProperty("Append", "'I should get Lauren to the Main Hall. I hope Jessie is still safe there. I'm sure she can take care of herself.'" .. gsDE)
                end
            
            --Nobody is following:
            else
            
                --Lauren is in the main hall:
                if(gzTextVar.bIsLaurenMainHalled == true) then
                    TL_SetProperty("Append", "'I should find Jessie, and get her to the Main Hall. I hope it's safe there.'" .. gsDE)
                
                --Jessie is in the main hall:
                elseif(gzTextVar.bIsJessieMainHalled == true) then
                    TL_SetProperty("Append", "'I should find Lauren before something happens to him, and get him to the Main Hall. He'll be safe there. I hope.'" .. gsDE)
                end
            end
            
        end
    
    --Phase 2. Searching.
    elseif(gzTextVar.iGameStage == 2) then
        TL_SetProperty("Append", "You take a second to think." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        --Clay infection!
        if(gzTextVar.gzPlayer.iClayInfection >= 0) then
            TL_SetProperty("Append", "'I'd better find some way to wash off this clay before it gets any worse!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            if(gzTextVar.bSeenAntechamber == false) then
                TL_SetProperty("Append", "'After that, we should look around and try to find a way out of here. Pygmalie left a key behind, I wonder what it opens...'" .. gsDE)
                TL_SetProperty("Create Blocker")
            
            --At antechamber, light not dealt with.
            elseif(gzTextVar.bSeenAntechamber == true and gzTextVar.bDisabledLight == false) then
                TL_SetProperty("Append", "'After that, we should figure out what's going on in that strange room in the basement. There is clearly something important there.'" .. gsDE)
                TL_SetProperty("Create Blocker")
        
            --Light disabled.
            else
                TL_SetProperty("Append", "'After that, I should check the room behind the light in the basement. That light must have been guarding something.'" .. gsDE)
                TL_SetProperty("Create Blocker")
            end
        
        --Normal.
        else
            
            --Has started phase 2, doesn't have chapel key or seen antechamber yet:
            if(gzTextVar.bSeenAntechamber == false and gzTextVar.bHasChapelKey == false) then
                TL_SetProperty("Append", "'There's got to be a way out of here. Pygmalie left a key when she ran off, I wonder what it opens...'" .. gsDE)
                TL_SetProperty("Create Blocker")
            
            --Has not seen light, but has chapel key:
            elseif(gzTextVar.bSeenAntechamber == false and gzTextVar.bHasChapelKey == true) then
            
                --Has not gotten diamond key:
                if(gzTextVar.bHasDiamondKey == false) then
                    TL_SetProperty("Append", "'We found a key to the chapel. There must be something important in it. We should investigate.'" .. gsDE)
                    TL_SetProperty("Create Blocker")
            
                --Has diamond key:
                else
                    TL_SetProperty("Append", "'This diamond key opens a lot of doors. We should search for a way to escape. Maybe the map pieces we found can help.'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                end
            
            --Has seen the antechamber:
            elseif(gzTextVar.bSeenAntechamber == true) then
            
                --At antechamber, light not dealt with.
                if(gzTextVar.bDisabledLight == false) then
                    TL_SetProperty("Append", "'We should figure out what's going on in that strange room in the basement. There is clearly something important there. Maybe one of those storybooks has a clue on how to get past it?'" .. gsDE)
                    TL_SetProperty("Create Blocker")
            
                --Light disabled.
                else
                    TL_SetProperty("Append", "'I should check the room behind the light in the basement. That light must have been guarding something.'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                end
            end
        end
    
    --Phase 3. Journal pages.
    elseif(gzTextVar.iGameStage == 3) then
        TL_SetProperty("Append", "You take a second to think." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        --Clay infection!
        if(gzTextVar.gzPlayer.iClayInfection >= 0) then
            TL_SetProperty("Append", "'I'd better find some way to wash off this clay before it gets any worse!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "'After that, I need to find the rest of the journal pages Sarah mentioned. I should show what I have to her to see if that is enough.'" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Normal.
        else
            TL_SetProperty("Append", "'I need to find the rest of the journal pages Sarah mentioned. I should show what I have to her to see if that is enough.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "'Hopefully she will remember a way for us to escape...'" .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Phase 4. Find the vessel.
    elseif(gzTextVar.iGameStage == 4) then
        TL_SetProperty("Append", "You take a second to think." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        --Index.
        local iStorybookIndex = fnGetIndexOfItemInInventory("empty storybook")
    
        --Clay infection!
        if(gzTextVar.gzPlayer.iClayInfection >= 0) then
            TL_SetProperty("Append", "'I'd better find some way to wash off this clay before it gets any worse!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            --No storybook.
            if(iStorybookIndex == -1) then
                TL_SetProperty("Append", "'After that, Sarah mentioned that there might be a book I can use in the western chapel. I should check there.'" .. gsDE)
                TL_SetProperty("Create Blocker")
        
            --Storybook.
            else
                TL_SetProperty("Append", "'After that, I should get this empty book to Sarah. She'll know what to do with it.'" .. gsDE)
                TL_SetProperty("Create Blocker")
            end
        
        --Normal.
        else
            
            --No storybook.
            if(iStorybookIndex == -1) then
                TL_SetProperty("Append", "'Sarah mentioned that there might be a book I can use in the western chapel. I should check there.'" .. gsDE)
                TL_SetProperty("Create Blocker")
        
            --Storybook.
            else
                TL_SetProperty("Append", "'I should get this empty book to Sarah. She'll know what to do with it.'" .. gsDE)
                TL_SetProperty("Create Blocker")
            end
        end
    
    --Phase 5 changes this a bit.
    elseif(gzTextVar.iGameStage == 5) then
        TL_SetProperty("Append", "You take a second to think. Very little happens during this second." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "'A girl told me to volunteer? For something? It's fuzzy. I should do that.'" .. gsDE)
    
    --Phase 6. Finalie.
    elseif(gzTextVar.iGameStage == 6) then
        TL_SetProperty("Append", "You take a second to think." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "'It's time. We should go and confront Pygmalie. We need to end this.'" .. gsDE)
    
    else
        TL_SetProperty("Append", "No think handler for this state!" .. gsDE)
    end
    
-- |[Statue]|
elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    TL_SetProperty("Append", "YOU ARE A STATUE, YOU DO NOT THINK. YOU WILL FIND THE INTRUDERS. YOU WILL BRING THE INTRUDERS TO THE LIGHT. THEY WILL SEE THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You will obey. You are a statue. You do not think. Find the intruders. Bring them to the light." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Setup.
    local j = gzTextVar.iJessieIndex
    local l = gzTextVar.iLaurenIndex
    
    --Location, Identity
    local sJessieLocation = gzTextVar.zEntities[j].sLocation
    local sLaurenLocation = gzTextVar.zEntities[l].sLocation
    local iJessieIndex = fnGetRoomIndex(sJessieLocation)
    local iLaurenIndex = fnGetRoomIndex(sLaurenLocation)
    local sJessieIdentity = "No Identity"
    local sLaurenIdentity = "No Identity"
    if(iJessieIndex >= 1 and iJessieIndex <= gzTextVar.iRoomsTotal) then sJessieIdentity = gzTextVar.zRoomList[iJessieIndex].sIdentity end
    if(iLaurenIndex >= 1 and iLaurenIndex <= gzTextVar.iRoomsTotal) then sLaurenIdentity = gzTextVar.zRoomList[iLaurenIndex].sIdentity end
    
    --Neither are turned:
    if(gzTextVar.zEntities[j].sState ~= "Statue" and gzTextVar.zEntities[l].sState ~= "Statue") then
        
        TL_SetProperty("Append", "You think one intruder is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ") and the other is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Jessie turned:
    elseif(gzTextVar.zEntities[l].sState ~= "Statue") then
        if(iLaurenIndex >= 1 and iLaurenIndex <= gzTextVar.iRoomsTotal) then sLaurenIdentity = gzTextVar.zRoomList[iLaurenIndex].sIdentity end
        TL_SetProperty("Append", "You think the intruder is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Lauren turned:
    else
        if(iJessieIndex >= 1 and iJessieIndex <= gzTextVar.iRoomsTotal) then sJessieIdentity = gzTextVar.zRoomList[iJessieIndex].sIdentity end
        TL_SetProperty("Append", "You think the intruder is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ")." .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
-- |[Claygirl]|
elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then

    --Normal:
    if(gzTextVar.bIsClaySequence == false) then
        TL_SetProperty("Append", "You take a second to think." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "'I should find the humans somewhere in the manor and cover them in clay. That will make my creator happy.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        --Setup.
        local j = gzTextVar.iJessieIndex
        local l = gzTextVar.iLaurenIndex
        
        --Location, Identity
        local sJessieLocation = gzTextVar.zEntities[j].sLocation
        local sLaurenLocation = gzTextVar.zEntities[l].sLocation
        local iJessieIndex = fnGetRoomIndex(sJessieLocation)
        local iLaurenIndex = fnGetRoomIndex(sLaurenLocation)
        local sJessieIdentity = "No Identity"
        local sLaurenIdentity = "No Identity"
        if(iJessieIndex >= 1 and iJessieIndex <= gzTextVar.iRoomsTotal) then sJessieIdentity = gzTextVar.zRoomList[iJessieIndex].sIdentity end
        if(iLaurenIndex >= 1 and iLaurenIndex <= gzTextVar.iRoomsTotal) then sLaurenIdentity = gzTextVar.zRoomList[iLaurenIndex].sIdentity end
        
        --Neither are turned:
        if(gzTextVar.zEntities[j].sState == "Human" and gzTextVar.zEntities[l].sState == "Human") then
            
            local sJessieLocation = gzTextVar.zEntities[j].sLocation
            local sLaurenLocation = gzTextVar.zEntities[l].sLocation
            TL_SetProperty("Append", "You think Jessie is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ") and Lauren is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Jessie turned:
        elseif(gzTextVar.zEntities[l].sState == "Human") then
            local sLaurenLocation = gzTextVar.zEntities[l].sLocation
            TL_SetProperty("Append", "You think Lauren is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Lauren turned:
        else
            local sJessieLocation = gzTextVar.zEntities[j].sLocation
            TL_SetProperty("Append", "You think Jessie is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ")." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Clay sequence:
    else
        --Disguised:
        if(gzTextVar.bClayedFriends == false) then
            TL_SetProperty("Append", "You take a second to think." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "'I can make myself look like I did as a human, at least for the moment. That should give me enough time to turn Jessie and Lauren into clay.'" .. gsDE)
            TL_SetProperty("Create Blocker")

        --Ending.
        else
            TL_SetProperty("Append", "You take a second to think." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "'Now that we're all clay sisters, we should present ourselves to our creator. She's on the second floor of the western building.'" .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    end

-- |[Rubber]|
elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then

    if(gzTextVar.bGoopedFriends == false) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Hee hee! I bet my creator would love it if I made Jessie and Lauren rubbery just like me! It's the best feeling in the world!'" .. gsDE)
        TL_SetProperty("Create Blocker")
    else
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Now that we're all happy clones, we should present ourselves to our creator! She will be in the ritual altar on the second floor of the west building.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I hope she thinks I'm cute! I'm so happy!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
    end
    return

-- |[Ice]|
elseif(gzTextVar.gzPlayer.sFormState == "Ice") then

    if(gzTextVar.bFrozeFriends == false) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Jessie and Lauren are waiting for me. I should freeze them and bring them to my creator. Maybe then I can go back to doing nothing.'" .. gsDE)
        TL_SetProperty("Create Blocker")
    else
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'The creator is in the second floor of the western building. I should go there and present myself for inspection.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Then I can go back to doing nothing.'" .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    return

-- |[Glass]|
elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
    TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
    TL_SetProperty("Create Blocker")
        
    TL_SetProperty("Append", "Your creator said there were humans loose in the manor that might be dangerous." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You need to find them and make sure they aren't a threat to your beloved creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Setup.
    local j = gzTextVar.iJessieIndex
    local l = gzTextVar.iLaurenIndex
    
    --Location, Identity
    local sJessieLocation = gzTextVar.zEntities[j].sLocation
    local sLaurenLocation = gzTextVar.zEntities[l].sLocation
    local iJessieIndex = fnGetRoomIndex(sJessieLocation)
    local iLaurenIndex = fnGetRoomIndex(sLaurenLocation)
    local sJessieIdentity = "No Identity"
    local sLaurenIdentity = "No Identity"
    if(iJessieIndex >= 1 and iJessieIndex <= gzTextVar.iRoomsTotal) then sJessieIdentity = gzTextVar.zRoomList[iJessieIndex].sIdentity end
    if(iLaurenIndex >= 1 and iLaurenIndex <= gzTextVar.iRoomsTotal) then sLaurenIdentity = gzTextVar.zRoomList[iLaurenIndex].sIdentity end
    
    --Neither are turned:
    if(gzTextVar.zEntities[j].sState == "Human" and gzTextVar.zEntities[l].sState == "Human") then
        
        local sJessieLocation = gzTextVar.zEntities[j].sLocation
        local sLaurenLocation = gzTextVar.zEntities[l].sLocation
        TL_SetProperty("Append", "You think Jessie is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ") and Lauren is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Jessie turned:
    elseif(gzTextVar.zEntities[l].sState == "Human") then
        local sLaurenLocation = gzTextVar.zEntities[l].sLocation
        TL_SetProperty("Append", "You think Lauren is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Lauren turned:
    else
        local sJessieLocation = gzTextVar.zEntities[j].sLocation
        TL_SetProperty("Append", "You think Jessie is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ")." .. gsDE)
        TL_SetProperty("Create Blocker")
    end

-- |[Doll]|
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll") then
    
    --String trap:
    if(gzTextVar.iMaryStringTrapState == 3) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'My creator would love it if I transformed Jessie and Lauren with the same trap. I should lure them in here and let the wardrobe do to them what it did to me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    
    --String trap:
    elseif(gzTextVar.iMaryStringTrapState == 4) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'We should go to the ritual altar on the second floor of the western manor. Our creator will want to see how beautifully we turned out!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I hope she makes sure we stay obedient and silly forever. I have too much of my own willpower for my liking.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    end
    
    --In phases other than 6, remind the player to TF the humans:
    if(gzTextVar.iGameStage < 6) then
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Your creator asked you to play wherever you like. You would like to make your creator happy." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You are sure you can find some playmates. You should play with them, and bring them to your creator afterwards." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        --Setup.
        local j = gzTextVar.iJessieIndex
        local l = gzTextVar.iLaurenIndex
        
        --Location, Identity
        local sJessieLocation = gzTextVar.zEntities[j].sLocation
        local sLaurenLocation = gzTextVar.zEntities[l].sLocation
        local iJessieIndex = fnGetRoomIndex(sJessieLocation)
        local iLaurenIndex = fnGetRoomIndex(sLaurenLocation)
        local sJessieIdentity = "No Identity"
        local sLaurenIdentity = "No Identity"
        if(iJessieIndex >= 1 and iJessieIndex <= gzTextVar.iRoomsTotal) then sJessieIdentity = gzTextVar.zRoomList[iJessieIndex].sIdentity end
        if(iLaurenIndex >= 1 and iLaurenIndex <= gzTextVar.iRoomsTotal) then sLaurenIdentity = gzTextVar.zRoomList[iLaurenIndex].sIdentity end
        
        --Neither are turned:
        if(gzTextVar.zEntities[j].sState == "Human" and gzTextVar.zEntities[l].sState == "Human") then
            
            local sJessieLocation = gzTextVar.zEntities[j].sLocation
            local sLaurenLocation = gzTextVar.zEntities[l].sLocation
            TL_SetProperty("Append", "You think Jessie is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ") and Lauren is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Jessie turned, but not Lauren:
        elseif(gzTextVar.zEntities[j].sState ~= "Human" and gzTextVar.zEntities[l].sState == "Human") then
            local sLaurenLocation = gzTextVar.zEntities[l].sLocation
            TL_SetProperty("Append", "You think Lauren is at [" .. sLaurenLocation .. "] (" .. sLaurenIdentity .. ")." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Lauren turned, but not Jessie
        elseif(gzTextVar.zEntities[j].sState == "Human" and gzTextVar.zEntities[l].sState ~= "Human") then
            local sJessieLocation = gzTextVar.zEntities[j].sLocation
            TL_SetProperty("Append", "You think Jessie is at [" .. sJessieLocation .. "] (" .. sJessieIdentity .. ")." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Both are turned, print nothing:
        else
        
        end
    
    --Objective reminder.
    else
        TL_SetProperty("Append", "You think to yourself for a moment." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You will need to confront Pygmalie. She is probably still at the altar room on the second floor." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Fortunately, none of the other dolls suspect you. You can move among them effortlessly." .. gsDE)
        TL_SetProperty("Create Blocker")

    end

-- |[Transforming]|
else
    TL_SetProperty("Append", "Your mind is completely empty.")

end