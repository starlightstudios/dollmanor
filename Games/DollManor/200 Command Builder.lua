--[ ====================================== Command Builder ====================================== ]
--After every action, rebuilds the list of commands the player can use. This doesn't include some
-- standard commands like help since those just clutter the list.

--[ ========================================== Default ========================================== ]