-- |[ ======================================== Game Info ======================================= ]|
--This file is executed at program startup to specify that this is a game the engine can load.
local sBasePath       = fnResolvePath()
local sGameName       = "Doll Manor"
local sGamePath       = "Root/Paths/System/Startup/sDollManorPath" --Must exist in the C++ listing.
local sButtonText     = "Play String Tyrant"
local sLauncherScript = "YMenuLaunch.lua" --File in the same folder as this file that will launch the program.
local iPriority       = gciStringTyrant_Set_Start + 0
local iNegativeLen    = -23 --Indicates length of launcher name. ZLaunch.lua is -12.

--Local variables.
local zLaunchPath = sBasePath .. "000 Launcher Script.lua"

-- |[Compatibility Check]|
if(LM_IsGameRegistered("String Tyrant") == false) then
    if(SysPaths.bShowFailedRegistrations) then
        io.write("Not registering String Tyrant. Engine is incompatible.\n")
    end
    return
end

-- |[Registration]|
--Add the game entry. Does nothing if it already exists.
SysPaths:fnAddGameEntry(sGameName, sGamePath, sButtonText, iPriority, sLauncherScript, iNegativeLen)

--Add search path.
SysPaths:fnAddSearchPathToEntry(sGameName, zLaunchPath)
