-- |[Pygmalie's Study]|
--When returning to Pygmalie's study as a doll in phase 5, a cutscene runs.

--Must be phase 5.
if(gzTextVar.iGameStage ~= 5) then return end
    
--Player is in the "Pygmalie's Study".
if(gzTextVar.gzPlayer.sLocation ~= "Pygmalie's Study") then return end

--Mark handling this.
gbHandledSubscript = true

--Indices.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
        
--Dialogue.
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You enter the secret room. A girl and a boy are here, staring intently at two books. They look up at you." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Hello there, doll. Are you friendly?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I am! I am here to take you to my creator! Won't you be my sisters?'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Of course we will, dolly. We'd love to be your sisters.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*Lauren, you ready?*'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "The little boy nods. He has a determined look on his face." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hello, Lauren!'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I like your dress.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Why thank you! My creator made it just for me. You could have one just like it if you're a good boy!'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "The girl sidles up next to you. She whispers in your ear." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Lauren's a little sad. His sister went missing a while ago.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You grin widely. 'Don't worry, I'm a dolly. Making children happy is my reason for existing.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You kneel in front of the little boy." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Do you want to talk about your sister, Lauren? What's her name?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Why, that's my name! What a coincidence!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You try to smile as broad as possible. The boy retains his determined look. He nods at the girl behind you." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I drew a picture of her. Do you want to see it?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I would love to!'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "The boy opens the book he was carrying and holds it up for you to read. Odd. There's nothing on the page." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Suddenly, the girl behind you kicks you over! Your face plunges into the book!" .. gsDE)
fnDialogueFinale()
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Scenario Handlers/Doll/501 Pygmalies Study 2.lua", 0)

--Begin the sixth and final phase of the game.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Scenario Handlers/005 Set To Phase 6.lua", 0)
