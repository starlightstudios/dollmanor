-- |[ ================================== Turn Post Exec: Doll ================================== ]|
--Handles post-turn cutscenes if the player is a doll.
    
--Setup.
local sBasePath = fnResolvePath()

--"String Trap" bad end.
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 String Trap Entry.lua")
if(gbHandledSubscript == true) then return end

LM_ExecuteScript(sBasePath .. "201 String Trap Bad End.lua")
if(gbHandledSubscript == true) then return end

--"Pygmalie's Study" cutscene.
LM_ExecuteScript(sBasePath .. "500 Pygmalies Study.lua")
if(gbHandledSubscript == true) then return end

--"Ritual Altar" cutscene and finale.
LM_ExecuteScript(sBasePath .. "600 Ritual Altar.lua")
if(gbHandledSubscript == true) then return end

--"Ritual Altar" cutscene and finale, when TF'd due to the strings trap.
LM_ExecuteScript(sBasePath .. "601 Ritual Altar Strings.lua")
if(gbHandledSubscript == true) then return end
