-- |[String Trap Entry]|
--Much like the Strange Antechamber, Mary's friends stay at the entrance.

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Eerie Antechamber") then return end

--Mark handling this.
gbHandledSubscript = true

--Setup.
local sMary
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

-- |[Exiting the Trap]|
--Convince those silly humans to come with you.
if(gzTextVar.gzPlayer.sPrevLocation == "Grand Archive A") then

    --Mary is transformed. Bad end.
    if(gzTextVar.iMaryStringTrapState == 3 and gzTextVar.bStringTrapNoFollow == true) then
    
        --Setup for human Mary.
        local saHuMaryList    = {"Root/Images/DollManor/Characters/MarySecretDoll"}
        local saJessieDollTF0 = {"Root/Images/DollManor/Characters/JessieStringTrap0"}
        local saJessieDollTF1 = {"Root/Images/DollManor/Characters/JessieStringTrap1"}
        local saJessieDollTF2 = {"Root/Images/DollManor/Characters/JessieStringTrap2"}
    
        --Dialogue.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", -1, 0, 6, ciCodePlayer)
        fnDialogueCutscene("You",    iMaryCnt, saHuMaryList, "As you approach, you feel the magic of the clothes warp and change you. Suddenly, you appear human again." .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saHuMaryList, "You need to suppress a giggle. It seems your creator has a plan for this! You try to act like you did before she made you pretty." .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,   "'Mary?'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList,   "'Oh, good. Did you figure out the trap?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saHuMaryList, "'I did! Once you figure it out, it's no problem at all. Come on, I'll show you.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saHuMaryList, "'There's a special jewel at the far end, and I need your help to get the case open anyway.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saHuMaryList, "The silly humans will follow you, now. Lead them into the trap!" .. gsDE)
        
        --Flag.
        gzTextVar.bStringTrapNoFollow = false
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MarySecretDoll"
        gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
        gzTextVar.gzPlayer.sLayer1 = "Null"
        TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    end
end
    