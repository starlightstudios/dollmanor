-- |[Ritual Altar]|
--At the altar in phase 6. Starts the ending.

--Must be phase 6.
if(gzTextVar.iGameStage ~= 6) then return end
    
--Ritual Altar. It all ends here.
if(gzTextVar.gzPlayer.sLocation ~= "Ritual Altar") then return end

--Mark handling this.
gbHandledSubscript = true

--Indices.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local p = gzTextVar.iPygmalieIndex

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = gzTextVar.zEntities[p].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}
    
--Normal case.
if(gzTextVar.bIsBetrayal == false) then
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You enter the ritual room, Jessie and Lauren just behind you. You tense." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The dolls around the room regard them, but make no move towards them." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Creator, I found these two.'" .. gsDE)
    fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Is this your creator, doll?' Jessie asks, playing along." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Yes, she made me. She's going to make you a doll, as I promised.'" .. gsDE)
    fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'They will do nicely! Nice and full, boiling boiling yum!'" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Just sit in this chair, Jessie. I'll do the rest.'" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You lead the two over to the chair where Pygmalie dressed you. Jessie sits down. Pygmalie comes over to her." .. gsDE)
    fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Good dolly! Would you do the honors for this one?'" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Of course.'" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Pygmalie reaches into her smock to hand you a needle." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You respond by kicking straight up, right into her chin. Your agile, flexible doll body can do what your human form never could." .. gsDE)
    fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Pygmalie slumps over, knocked out by the sudden blow." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The dolls around the room start moving towards you, then stop. This has never happened before." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "They don't know what to do, and their creator isn't telling them. They stand idle." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You don't have long. If Pygmalie wakes up, you will be overrun. You'll need to make a sacrifice and escape." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Who will you sacrifice? Make your choice..." .. gsDE)

    --Change the input handler.
    TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Scenario Handlers/201 Special Input Sacrifice.lua")

--Betrayal ending.
else
    --Dialogue.
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You enter the ritual room, carrying two blank dolls behind you. Your creator turns to see you." .. gsDE)
    fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh, oh, oh. Yes, you've done it. Good dolly!'" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Thank you, creator.'" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You place the Jessie doll on the chair you sat in not too long ago. Pygmalie busies herself with the cosmetics." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Now is your chance... what will you do?" .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You may [submit] and become just another of Pygmalie's dolls..." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Or you might [betray] her and usurp her position as mistress of the manor..." .. gsDE)
    fnDialogueCutscene("You",      iMaryCnt, saMaryList, "[submit] or [betray]? The choice is yours." .. gsDE)
    fnDialogueFinale()

    --Change the input handler.
    TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Scenario Handlers/200 Special Input Betrayal.lua")

end
