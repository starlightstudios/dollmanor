-- |[Ritual Altar - Strings Bad End]|
--At the ritual altar, player was defeated by the strings trap.

--Ritual Altar.
if(gzTextVar.gzPlayer.sLocation ~= "Ritual Altar" or gzTextVar.iMaryStringTrapState ~= 4) then return end

--Mark handling this.
gbHandledSubscript = true

--Indices.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local p = gzTextVar.iPygmalieIndex

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = gzTextVar.zEntities[p].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Dialogue.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You enter the ritual room, Jessie and Lauren just behind you. Your creator turns and smiles, ear to ear, as she sees you." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh you three are just marvelous! Marvelous and tasty and juicy!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You and your sisters kneel on the ground. You bow as low as you can before your creator." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'So, Mary, my sweet beautiful doll, did you bring these two to my love?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I did. I brought them to the trap that transformed me.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Wonderful! What a good dolly you are!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Thank you, creator.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Jessie, you have something floating in your mind.'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Yes, creator. I wish to be Mary's bride, and remain with her for all time.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'I don't see why not. You deserve a reward for being such a good dolly.'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Thank you, creator. You are too kind to a silly dolly.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'And Lauren? Are you happy enough?'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Yes, creator. I have been made perfect.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Very good. There is one thing left. Give me your utter loyalty...'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Your creator holds her hand up and you wait. You feel something disappearing from your mind." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Your free will? Your ability to think complex thoughts? Your desire to do anything but serve her and love her?" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "It does not matter. You realize you cannot remember ever being anything but a dolly. A silly dolly serving her beloved creator." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The task complete, you and your sisters stand. You curtsey to your creator and leave the room, holding your bride's hand." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Somewhere, deep in the recesses of your simplified mind, a spark of thought burns in the darkness." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You needed to escape. To flee this horrible place with your humanity." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You smirk as that spark burns out, never to reignite." .. gsDE)

--Finale.
gzTextVar.bStringTrapEnding = true
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
