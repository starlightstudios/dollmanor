-- |[String Trap Bad End]|
--This takes place several moves into the string trap, if Mary is a doll and has lured the humans inside.

--Make sure we're in the right room. There are four candidates, doesn't matter which.
if(gzTextVar.gzPlayer.sLocation ~= "Grand Archive W" and gzTextVar.gzPlayer.sLocation ~= "Grand Archive O" and gzTextVar.gzPlayer.sLocation ~= "Grand Archive Q" and gzTextVar.gzPlayer.sLocation ~= "Grand Archive C") then return end

--No repeats.
if(gzTextVar.iMaryStringTrapState ~= 3) then return end
if(gzTextVar.bStringTrapNoFollow == true) then return end

--Mark handling this.
gbHandledSubscript = true

--Setup.
local sMary
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = "Root/Images/DollManor/Characters/Doll Goth A"
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

--Setup for human Mary.
local saHuMaryList    = {"Root/Images/DollManor/Characters/MarySecretDoll"}
local saJessieDollTF0 = {"Root/Images/DollManor/Characters/JessieStringTrap0"}
local saJessieDollTF1 = {"Root/Images/DollManor/Characters/JessieStringTrap1"}
local saJessieDollTF2 = {"Root/Images/DollManor/Characters/JessieStringTrap2"}
local saLaurenDollTF  = {"Root/Images/DollManor/Characters/Doll Geisha C"}
        
--Achievement
Steam_UnlockAchievement(gciAchievement_NoFightIt)
Steam_UnlockAchievement(gciAchievement_ItDoesntHurt)

--Dialogue.
fnDialogueCutscene("You",    iMaryCnt, saHuMaryList,    "You feel a voice whisper into your silly, empty mind. It's time, the silly humans are already defeated." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saHuMaryList,    "Your creator will be so proud of you! Time to make them pretty!" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saHuMaryList,    "You begin to giggle. You can't help it, it's just so funny!" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,      "'Mary?'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,      "'What's so funny?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saHuMaryList,    "'The trap is so easy to get through, it's surprising, really.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saHuMaryList,    "'But you humans are very easy to trick. A shame.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,      "'What? You said - '" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saHuMaryList,    "'Oh, I was tricking you. Look at your arm.'" .. gsDE)
fnDialogueCutscene("You",    iJessCnt, saJessieDollTF0, "With dread, Jessie looks at her arm. She hasn't even noticed the magic of the strings flowing into her!" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF0, "'Mary! It's got me! Cut the string, quick!'" .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 0, 4, ciCodePlayer)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "With no further need to hide, your true form is revealed in all its firm plastic glory. You're a thousand times cuter than you were." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,      "'Mary! No!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'Funny. It pulled me into the wardrobe in the ceiling, and changed me. But then again, I was fighting back.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'You've been letting the magic flow into you for a while now. Didn't you notice the strings on you? Didn't you feel them lace you and grip you?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'Seize you, control you. It's wonderful, Jessie.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF1, "'Fight it, Mary! We've got to get out of here!'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF1, "The strings have gripped Jessie by the wrists, shoulders, knees, feet... She can't move unless they force her to. She panics and thrashes." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF1, "Lauren holds still, more accepting. He knows he is defeated. Jessie calls out again." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF1, "'Mary, please! Do something!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'I did do something, I brought you here. I don't want to fight it, Jessie. It feels incredible.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF1, "The wardrobe drops a shawl. Jessie struggles right to the end." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF1, "'Mary! Stop! Please - '" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'...'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "Just as it did to you, the clothes starts placing ideas into Jessie's mind. You hope she is as receptive as you were." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "You hope she realizes how much happier she will be. She seems to be struggling for a moment, trying to think, trying to remember." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "And then, her struggle ceases. Her glazed, blank eyes meet yours." .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, j, 1, 4, ciCodeUnfriendly)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'Please, Mary, hold my hand.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'You see, Jessie? Wasn't it silly to resist?'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'It was. I did not know what it was like to be a doll. Now I do, and I love it.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'I am your bride, Mary. It is our wedding day. Will you kiss me?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'I will.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "You hear the sound of Lauren being carried up into the rafters. The wardrobe closes around him. He does not fight." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "You lift Jessie's shawl and kiss her. Her glassy stare continues as you savour her endless love and obedience." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'Now we can be together, Mary.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurenDollTF,  "'Together forever, sisters.'" .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, l, 3, 4, ciCodeFriendly)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurenDollTF,  "Lauren descends from above, bouyed by the strings. He has been transformed into an elegant geisha doll." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'So, my sister and my bride, what shall we do?'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'Shall I throw the bouquet?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurenDollTF,  "'I am the only one here to catch it!' Lauren says with a giggle." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessieDollTF2, "'Oh, we should go introduce ourselves to our creator.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'She will be so happy with how well we turned out.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurenDollTF,  "'Will she reward you for making such pretty sisters?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "'Being with you forever is the only reward I need. Let us be off.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,      "You should go to your creator, who awaits you in the ritual altar on the second floor of the western manor. She will be overjoyed with how well you turned out." .. gsDE)

--Flags.
gzTextVar.iMaryStringTrapState = 4
gzTextVar.zEntities[j].sQueryPicture = "Root/Images/DollManor/Characters/JessieStringTrap2"
gzTextVar.zEntities[j].sState = "Bridal Doll Special"
gzTextVar.zEntities[l].sQueryPicture = "Root/Images/DollManor/Characters/Doll Geisha C"
gzTextVar.zEntities[l].sState = "Geisha Doll Special"

gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Goth A"
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
gzTextVar.gzPlayer.sLayer1 = "Null"
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    