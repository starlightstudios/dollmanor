-- |[Ice Trap Entry]|
--Ice Trap entry, freeze them!

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Warm Respite") then return end

--Repeat check.
if(gzTextVar.bFrozeFriends == true) then return end
gzTextVar.bFrozeFriends = true

--Friends follow you now.
gzTextVar.bFreezingNoFollow = false

--Mark handling this.
gbHandledSubscript = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

--Frozen Frames
local saJessFrzAList = {"Root/Images/DollManor/Transformation/JessieIceTF0"}
local saLaurFrzAList = {"Root/Images/DollManor/Transformation/LaurenIceTF0"}
local saJessFrzBList = {"Root/Images/DollManor/Transformation/JessieIceTF1"}
local saLaurFrzBList = {"Root/Images/DollManor/Transformation/LaurenIceTF1"}
local saJessFrzCList = {"Root/Images/DollManor/Characters/JessieIce"}
local saLaurFrzCList = {"Root/Images/DollManor/Characters/LaurenIce"}
        
--Achievement
Steam_UnlockAchievement(gciAchievement_NoFightIt)
Steam_UnlockAchievement(gciAchievement_ItDoesntHurt)

fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'Mary? Oh my goodness, quickly! Warm up by the fire!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'All right. Come sit next to me, help me warm up.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "You sit next to the fire and the two humans gather around you. You begin siphoning heat from them." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,     "'You're frozen over, Mary. How are you still alive?' " .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'What do you mean?'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,     "'I mean there's ice all over your skin.' " .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'No there isn't.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,     "'Mary? Do you know where you are? Is your head okay? Did you bump it?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'Everything is fine. I am not worried about anything.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'Why are you so calm?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'It doesn't matter. Give me your heat.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzAList, "Jessie notices what you are doing first. You place your hand on her thigh and suck the heat from her. You have a bottomless thirst for heat. You freeze her in place." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurFrzAList, "Lauren winces as the cold stings him. His teeth chatter." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzAList, "'M-m-m-mary s-s-s-stop. We'll f-f-f-freeze...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'I know. And then you'll be just like me. It doesn't hurt.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurFrzAList, "'How c-c-c-could you?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'I don't feel anything. Remorse, pity, sadness. Nothing. It's easy. Freeze solid, human.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzBList, "'F-f-f...'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzBList, "'Freeeeeeze...'" .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, j, 4, 5, ciCodeUnfriendly)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzCList, "'Freeze, human. Become ice like us.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'It does not hurt, Lauren.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzCList, "'You have no choice in the matter. Become ice.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurFrzBList, "'...'" .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, l, 5, 5, ciCodeFriendly)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurFrzCList, "'It is done. I am like you, sister.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'Good.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzCList, "'What shall we do now?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurFrzCList, "'Nothing.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzCList, "'An eternity of nothing? All right.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'No. First we must present ourselves to the creator.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessFrzCList, "'How could I forget? Fine, let's go.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurFrzCList, "'Does it matter?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "'Nothing does, but we do as we are told. The creator is all.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "You will need to present yourself to your creator. She is on the second floor of the west building." .. gsDE)
fnDialogueFinale()

--Change display properties.
gzTextVar.zEntities[j].sQueryPicture = "Root/Images/DollManor/Characters/JessieIce"
gzTextVar.zEntities[j].sState = "Ice"
gzTextVar.zEntities[l].sQueryPicture = "Root/Images/DollManor/Characters/LaurenIce"
gzTextVar.zEntities[l].sState = "Ice"