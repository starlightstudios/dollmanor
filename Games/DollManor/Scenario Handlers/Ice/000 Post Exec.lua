-- |[ ================================== Turn Post Exec: Ice =================================== ]|
--Handles post-turn cutscenes if the player is an ice sculpture.

--Setup.
local sBasePath = fnResolvePath()

--Turn Jessie and Lauren to ice!
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 Freeze Your Friends.lua")
if(gbHandledSubscript == true) then return end

--Finale.
LM_ExecuteScript(sBasePath .. "201 Finale.lua")
if(gbHandledSubscript == true) then return end
