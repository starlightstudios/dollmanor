-- |[Finale]|
--Ice finale.

--Make sure this is the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Ritual Altar") then return end

--Mark handling this.
gbHandledSubscript = true
gzTextVar.bFreezingTrapEnding = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local p = gzTextVar.iPygmalieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = gzTextVar.zEntities[p].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Dialogue.
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Ah, there you are. Mary, Jessie, Lauren?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'We are here, creator.'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'What would you like us to do?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Hm, not my usual taste. Not fun, not happy. Kneel.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The three of you kneel. You feel something worming inside your head. It doesn't bother you." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'There. Go be miserable someplace else.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Yes, creator.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You and the two ice sculptures you brought with you return to the frozen caverns without another word." .. gsDE)
fnDialogueFinale()
    