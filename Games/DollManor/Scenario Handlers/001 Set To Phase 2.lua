-- |[ ===================================== Set to Phase 2 ===================================== ]|
--Instantly sets the game to Phase 2. Jessie and Lauren teleport to the player's position, the
-- appropriate key spawns if the player doesn't have it, and Pygmalie moves to the second floor.
gzTextVar.iGameStage = 2
gzTextVar.bIsJessieFollowing = true
gzTextVar.bIsJessieMainHalled = false
gzTextVar.bIsLaurenFollowing = true
gzTextVar.bIsLaurenMainHalled = false

--The stranger will spawn for the first time.
gzTextVar.bStrangerCanRespawn = true

--No more easy combats.
gzTextVar.iEasyCombats = 0

--Setup.
local p = gzTextVar.iPygmalieIndex
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex

--Move Pygmalie to the upper floor.
local sOldLocation = gzTextVar.zEntities[p].sLocation
gzTextVar.zEntities[p].sLocation = "Ritual Altar"
local fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
local fNewX, fNewY, fNewZ = fnGetRoomPosition("Ritual Altar")
TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[p].sIndicatorName, fNewX, fNewY, fNewZ)

--Instantly move Jessie to our position if she wasn't there already.
sOldLocation = gzTextVar.zEntities[j].sLocation
gzTextVar.zEntities[j].sLocation = gzTextVar.gzPlayer.sLocation
fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[j].sIndicatorName, fNewX, fNewY, fNewZ)

--Instantly move Lauren to our position if he wasn't there already.
sOldLocation = gzTextVar.zEntities[l].sLocation
gzTextVar.zEntities[l].sLocation = gzTextVar.gzPlayer.sLocation
fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[l].sIndicatorName, fNewX, fNewY, fNewZ)

-- |[Item Spawning]|
--Determine where to spawn items. First, if we entered phase 2 via debug, spawn them at the player's location.
local sSpawnItemsAt = "Main Hall"
if(gzTextVar.bEnteredPhase2Naturally == false) then
    sSpawnItemsAt = gzTextVar.gzPlayer.sLocation

--Otherwise, spawn them where Pygmalie left them.
else

    --In the "Normal" manor, this is the north of the main hall.
    if(gzTextVar.sManorType == "Normal") then
        sSpawnItemsAt = "Main Hall N"
    
    --In the smaller manors, it's just the main hall.
    else
        sSpawnItemsAt = "Main Hall"
    end
end

--Spawn the appropriate key if it has not been spawned yet.
if(gzTextVar.bSpawnedScenarioKey == false) then
    
    --Heart Key.
    if(gzTextVar.sPygmalieKey == "Heart Key") then
        gzTextVar.bSpawnedScenarioKey = true
        fnRegisterObject(sSpawnItemsAt, "heart key", "heart key", true, gzTextVar.sRootPath .. "Item Handlers/Keys/Heart Key.lua")
    end
end

--Spawn Pygmalie's journal entry.
fnCreateJournalPage(sSpawnItemsAt, 1)

--Spawn Pygmalie's map fragment.
fnRegisterObject(sSpawnItemsAt, "map piece", "map piece", true, gzTextVar.sRootPath .. "Item Handlers/MapPieces/MapPieceCGardens.lua")

-- |[Enemy Visibility]|
--The following rooms lose their "enemies can't see player" properties. They may not exist in all manor maps.
local fnAllowEnemiesToSee = function(sRoomName)
    local iRoomIndex = fnGetRoomIndex(sRoomName)
    if(iRoomIndex < 1 or iRoomIndex >= gzTextVar.iRoomsTotal) then return end
    gzTextVar.zRoomList[iRoomIndex].bEnemiesCannotSee = false
end
fnAllowEnemiesToSee("Main Hall")
fnAllowEnemiesToSee("Main Hall Stairs")
fnAllowEnemiesToSee("Main Hall NW")
fnAllowEnemiesToSee("Main Hall N")
fnAllowEnemiesToSee("Main Hall NE")
fnAllowEnemiesToSee("Main Hall SE")
fnAllowEnemiesToSee("Main Hall SW")

-- |[Enemy Spawning]|
--The smaller manors only have a few spawns.
if(gzTextVar.sManorType == "Demo" or gzTextVar.sManorType == "Simple") then

    --Spawn one Resin Titan. It patrols the main floor.
    for i = 1, #gzTextVar.zRespawnList, 1 do
        if(gzTextVar.zRespawnList[i].sIdentity == "Titan A") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnTitan(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        end
    end

    --Spawn two clay girls. They will take positions on random items in the manor.
    fnSpawnClayGirl("Claygirl Disguise A")
    fnSpawnClayGirl("Claygirl Disguise B")

--The normal manor spawns most of the remaining enemies. Some wait until the diamond key is acquired.
elseif(gzTextVar.sManorType == "Normal") then
    for i = 1, #gzTextVar.zRespawnList, 1 do
        local sID = gzTextVar.zRespawnList[i].sIdentity
        if(sID == "Doll F" or sID == "Doll H" or sID == "Doll I" or sID == "Doll J" or sID == "Doll K" or sID == "Doll M" or sID == "Doll N" or sID == "Doll O") then
            --io.write("Spawned doll " .. sID .. "\n")
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnDoll(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        --elseif(sID == "Titan A" or sID == "Titan B") then
        --    --io.write("Spawned titan " .. sID .. "\n")
        --    gzTextVar.zRespawnList[i].bIsActive = true
        --    fnSpawnTitan(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        elseif(sID == "Clay B") then
            --io.write("Spawned claygirl " .. sID .. "\n")
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnClayGirl(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        end
    end
    
    --Spawn two additional disguised claygirls.
    fnSpawnClayGirl("Claygirl Disguise A")
    fnSpawnClayGirl("Claygirl Disguise B")
end

--Rebuild locality info to show the nearby objects.
fnBuildLocalityInfo()