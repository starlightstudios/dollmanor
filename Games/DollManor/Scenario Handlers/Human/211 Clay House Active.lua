-- |[ ==================================== Clay House Active =================================== ]|
--As long as the clay house timer is ticking, run this script.

--If your friends are not following due to this variable, the house is active:
if(gzTextVar.bClayNoFollow == false) then return end

--Disarmed. Nothing happens.
if(gzTextVar.bClayDisarmed == true) then return end

--Mark handling this.
gbHandledSubscript = true

--Run the timer.
gzTextVar.iClayTimer = gzTextVar.iClayTimer + 1

--Clay door:
if(gzTextVar.bClayDoorThisTurn or gzTextVar.bClayItemThisTurn) then
    
    --Activate:
    if(gzTextVar.iClayTimer < 100) then
        gzTextVar.iClayTimer = 6
        if(gzTextVar.bClayDoorThisTurn == true) then
            TL_SetProperty("Append", "Opening the door seems to have woken the building up..." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Accelerate the timer.
    else
        if(gzTextVar.iClayTimer < 106) then
            gzTextVar.iClayTimer = gzTextVar.iClayTimer + 2
            if(gzTextVar.iClayTimer >= 106) then gzTextVar.iClayTimer = 106 end
            
        elseif(gzTextVar.iClayTimer < 110) then
            gzTextVar.iClayTimer = gzTextVar.iClayTimer + 2
            if(gzTextVar.iClayTimer >= 110) then gzTextVar.iClayTimer = 110 end
            
        elseif(gzTextVar.iClayTimer < 114) then
            gzTextVar.iClayTimer = gzTextVar.iClayTimer + 2
            if(gzTextVar.iClayTimer >= 114) then gzTextVar.iClayTimer = 114 end
        end
        if(gzTextVar.bClayDoorThisTurn == true) then
            TL_SetProperty("Append", "Opening the door seems to have agitated the building a great deal..." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    end
    
    --Reset flags.
    gzTextVar.bClayDoorThisTurn = false
    gzTextVar.bClayItemThisTurn = false
    
end

--If the timer hits 3, print a warning:
if(gzTextVar.iClayTimer == 3) then
    TL_SetProperty("Append", "You hear a groaning in the house somewhere..." .. gsDE)

--At time 6, the house wakes up if it wasn't already:
elseif(gzTextVar.iClayTimer == 6) then

    --Set the counter to 100.
    gzTextVar.iClayTimer = 100

    --First time:
    if(gzTextVar.bSawClayHouse == false) then
        gzTextVar.bSawClayHouse = true
        TL_SetProperty("Append", "You hear the building around you creak and groan as if alive!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You realize the floor, the walls, the roof - it's all made of the same sticky wet clay-like material!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You hear the walls smooth over a door on the south end. You'd better escape before your escape route is cut off!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Repeats:
    else
        TL_SetProperty("Append", "You hear the building around you creak and groan as if alive. It seems you have woken it up with your presence." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You hear the walls smooth over a door on the south end." .. gsDE)
        TL_SetProperty("Create Blocker")
    end

    --Bar the southern door.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Crystal Altar A", "C ClayUnopenable", "N", ciModifyVisibility, 0)

--At 106, notify the player.
elseif(gzTextVar.iClayTimer == 106) then
    TL_SetProperty("Append", "Woken by your presence, you hear the living house squish and attempt to reform itself. You should get out of here, quickly!" .. gsDE)
    TL_SetProperty("Create Blocker")

--At 110, notify the player.
elseif(gzTextVar.iClayTimer == 110) then
    TL_SetProperty("Append", "The house is preparing to do something..." .. gsDE)
    TL_SetProperty("Create Blocker")

--At 114, bad end.
elseif(gzTextVar.iClayTimer >= 114) then
    TL_SetProperty("Append", "Woken by your activities, the house finally works up the energy to do what it has been trying to do." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The floor boards deform into hands to grab you. You hop and jump to avoid them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You bolt for the exit, only to find that the doors have closed into formless clay. There's no way out!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You move back to the middle of the room and search up and down. There must be a way out." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A groaning indicates what the house has planned for you. The walls begin to close in around you..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Then, the walls surge! You are slammed into from all sides by the goopy clay walls!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirlBlue", ciImageLayerDefault)
    TL_SetProperty("Append", "..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You're crushed, squished, overwhelmed. You think you're going to choke, but you realize you don't need to breathe." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The walls recede and reform into their original shape. You are on your hands and knees in the middle of the room." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You realize you are changed. Different. Hands, knees? These terms no longer apply. You are only clay." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It feels strange. You poke your chest only to have your hand go through. You can concentrate to change how firm you are. You can even change your color." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Something was inside your head when you were being crushed. It changed your clay brain, changed how you thought, changed what you want." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You realize it was your creator changing you. Perfecting you. The very idea of her changing you fills you with joy." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "But the thing that really fills you with joy is how much Jessie and Lauren will love to be clay, too. In fact..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MarySecretClay", ciImageLayerDefault)
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 0, 3, ciCodePlayer)
    TL_SetProperty("Append", "You struggle. You push. You firm yourself up and change your color. It is difficult, perhaps the most difficult thing you have ever done. But you do it, because your creator will love it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You're pretty sure you made a mistake on your skirt, and it's hard to keep your fingers apart. You can't speak and your legs are threatening to dissociate into a pile at any moment. But you did it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The humans won't suspect a thing. Go show them your hard work!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flags.
    gzTextVar.bIsClaySequence = true
    gzTextVar.gzPlayer.sFormState = "Claygirl"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MarySecretClay"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
    
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_RightBehindYou)

end