-- |[Pygmalie's Study]|
--Cutscenes in the special study. Can be in several phases.

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Pygmalie's Study") then return end
if(gzTextVar.iGameStage >= 3) then return end
        
--Mark handling this.
gbHandledSubscript = true

--Setup.
local sMary
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local sSaraPath = "Root/Images/DollManor/Characters/Sarah"
local iSaraCnt = 1
local saSaraList = {sSaraPath}
    
--Cutscene.
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'So... This is what all the security was for?'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'A private study. There must be something important in here.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'There's a great big book...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Well of course there'd be books in a study. Guess we better start looking. Jessie, take that shelf. I got this one.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Should one of us watch the door? Just in case?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren, do you think you can handle that?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren? What are you doing?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'The book had words on it, but now it doesn't...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You take a look at the big book sitting on the desk. It has no title or author, and all the pages are blank." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What a bust. Are you seeing things, kiddo?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'No! I promise there were words!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You close the book. Lauren tugs at your shirt." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Look again! Please!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You decide to humour him, and open the book. On the first page, a single word is written. 'Hello'." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay, that was definitely not there a second ago.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Hello, book. My name is Lauren, this is my big sister Mary, and this is Jessie.' Lauren points you out in turn." .. gsDE)
fnDialogueCutscene("You",    iSaraCnt, saSaraList, "You watch the page. 'Pleased to meet you.' appears on it before your eyes." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'This can't be happening...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'After all we've seen, now you're a skeptic?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You think for a few moments. 'What is your name, book?'" .. gsDE)
fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'Sarah Lee-Anne'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'That's the same name as we saw in that page you found...'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is this a page from you, Miss Sarah?' Lauren asks, holding up the journal page." .. gsDE)
fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'No. I was the one who wrote them, but they are not from the book I am in'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The paper type doesn't match. Were you a person before?'" .. gsDE)
fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I don't remember. I think I was. It is very strange to me.'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'More to the point, can we get out of this place? Do you know anything about that, Sarah?'" .. gsDE)
fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I know I looked, but I don't remember. But the page, hold it near me please.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren holds the page up to the book. You hear a scribbling sound emanating from beneath the book." .. gsDE)
fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'That helped. Thank you. There are more pages. Please, find them. I may be able to help you if I can remember more.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You keep the page with you. You'll need to find the other pages and bring them here. Talk to [Sarah] when you have the pages." .. gsDE)
fnDialogueFinale()

--Activate phase 3.
LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/002 Set To Phase 3.lua")
