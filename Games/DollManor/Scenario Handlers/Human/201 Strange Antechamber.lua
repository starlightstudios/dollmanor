-- |[Strange Antechamber]|
--Room right before the hall of blue light. Has cutscenes.

--When the player enters from the not-blue-light side:
if(gzTextVar.gzPlayer.sLocation == "Strange Antechamber" and gzTextVar.gzPlayer.sPrevLocation ~= "Unsettling Passage") then

    --Mark handling this.
    gbHandledSubscript = true

    --Setup.
    local l = gzTextVar.iLaurenIndex
    local j = gzTextVar.iJessieIndex
    local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
    local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
    local sJessPath = gzTextVar.zEntities[j].sQueryPicture
    local iLaurCnt = 1
    local saLaurList = {sLaurPath}
    local iMaryCnt = 1
    local saMaryList = {sMaryPath}
    local iJessCnt = 1
    local saJessList = {sJessPath}

    --Dialogue is slightly different the first time.
    if(gzTextVar.bHasSeenHallwayDialogue == false and gzTextVar.bDisabledLight == false) then
        
        --Flag.
        gzTextVar.bHasSeenHallwayDialogue = true
        gzTextVar.bCloseDoorBehind = true
        
        --Dialogue.
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary, hold on a second...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Huh? What's up?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I don't like this... This place feels weird...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The room you're standing in is off-putting in a way that is difficult to place. The architecture looks normal, but a sense of unease hangs in the air." .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "There's something special nearby, but whether it's good or bad is impossible to say." .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Something's wrong. We shouldn't go this way.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Maybe, maybe not.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I don't wanna go in there...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Tell you what. I'll go in ahead and check if it's safe. Jessie, stay here with Lauren until I get back.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Think you can be brave without me for a bit, kiddo?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Yeah...'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Be careful. Very careful.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Close the door behind me. I'll knock if I need in.'" .. gsDE)
        fnDialogueFinale()

    --Repeat.
    elseif(gzTextVar.bDisabledLight == false) then
        gzTextVar.bCloseDoorBehind = true
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't worry. Stay here with Lauren.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Be careful...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Always am.'" .. gsDE)
        fnDialogueFinale()
    end

--When the player enters from the blue light side:
elseif(gzTextVar.gzPlayer.sLocation == "Strange Antechamber" and gzTextVar.gzPlayer.sPrevLocation == "Unsettling Passage") then

    --Mark handling this.
    gbHandledSubscript = true

    --Setup.
    local l = gzTextVar.iLaurenIndex
    local j = gzTextVar.iJessieIndex
    local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
    local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
    local sJessPath = gzTextVar.zEntities[j].sQueryPicture
    local iLaurCnt = 1
    local saLaurList = {sLaurPath}
    local iMaryCnt = 1
    local saMaryList = {sMaryPath}
    local iJessCnt = 1
    local saJessList = {sJessPath}
    
    --Player just disabled the light!
    if(gzTextVar.bDisabledLightCutscene == false and gzTextVar.bDisabledLight == true) then
        
        --Flag.
        gzTextVar.bDisabledLightCutscene = true

        --Dialogue.
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I heard something. Are you all right?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You saw the strange light through the door, right? I took care of it.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Really? Was it dangerous?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It was.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah. I can't describe it but it wasn't good for me. It's better that it's gone now.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Great work!'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren runs to you and gives you a big hug. You pat him on the head and smile." .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'So now what?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Follow me. There's a room beyond, and it's important. Otherwise, why put such security in the way?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Jessie and Lauren will follow you now." .. gsDE)
        fnDialogueFinale()

    --First time seeing this as a human.
    elseif(gzTextVar.bHasSeenHallwayReturnDialogue == false and gzTextVar.bDisabledLight == false) then
        
        --Flag.
        gzTextVar.bHasSeenHallwayReturnDialogue = true
        gzTextVar.bCloseDoorBehind = true
        
        --Close the western door.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", gzTextVar.gzPlayer.sLocation, "C Normal", "W", ciModifyVisibility, 1)
        AudioManager_PlaySound("Doll|CloseDoor")

        --Dialogue.
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Anything in there?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's a strange light in the hallway. Makes me feel uneasy.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Like an electric light? This place seems to predate electricity.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No idea, but I didn't like it. Maybe there's a way to shut it off.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'We shouldn't go near that light. I don't like it.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Scaredy-cat.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Don't!'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'If ever there was a time to be scared, this is it, Mary.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah, yeah.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's got to be something important in there, but we should find a way to get past that light first.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Maybe there's something about it in one of the books we keep seeing around here?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Good idea, Jessie.'" .. gsDE)
        fnDialogueFinale()
        
        --Reset hypnosis.
        gzTextVar.iHypnoticCheck = 0
        TL_SetProperty("Append", "You feel much better, having left the strange light." .. gsDE)

    --Repeat.
    elseif(gzTextVar.bDisabledLight == false) then
        
        --Close the western door.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", gzTextVar.gzPlayer.sLocation, "C Normal", "W", ciModifyVisibility, 1)
        AudioManager_PlaySound("Doll|CloseDoor")
        
        --Dialogue.
        gzTextVar.bCloseDoorBehind = true
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No good, light's still there, and still weird.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Keep searching?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'If we have to...'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'There's got to be some information on it, in a book or one of those stone slabs.'" .. gsDE)
        fnDialogueFinale()
        
        --Reset hypnosis.
        gzTextVar.iHypnoticCheck = 0
        TL_SetProperty("Append", "You feel much better, having left the strange light." .. gsDE)
    end

end
