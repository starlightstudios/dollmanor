-- |[Clay House Solved]|
--If you make it to the crystal room, the house is "Solved" and deactivates.

--Must be in this room.
if(gzTextVar.gzPlayer.sLocation ~= "Crystal Altar A") then return end
    
--Duplicates.
if(gzTextVar.bClayDisarmed == true) then return end
gzTextVar.bClayDisarmed = true

--Mark handling this.
gbHandledSubscript = true

--Dialogue.
TL_SetProperty("Append", "As you enter the chamber, the house seems to sigh around you. There is a jewel case on a pedestal." .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Append", "Wary of another trap, you quickly check the case over. There is nothing wrong with it. You chance to open it." .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Append", "Within is a crystal piece. But more importantly, the house itself... settles." .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Append", "As if bound by some unknown contract, the house gives up and allows you to take the crystal." .. gsDE)
TL_SetProperty("Create Blocker")
if(gzTextVar.bHasReadOrangeBook == true and gzTextVar.bHasSeenOrangeBookExtra == false) then
    gzTextVar.bHasSeenOrangeBookExtra = true
    TL_SetProperty("Append", "The crystal reminds you of the storybook you read. Light seems to grow dim around it." .. gsDE)
    TL_SetProperty("Create Blocker")
end
TL_SetProperty("Append", "It seems your walk out will be uneventful. Hopefully." .. gsDE)
TL_SetProperty("Create Blocker")

--Increment this counter.
gzTextVar.iTrapsDisarmed = gzTextVar.iTrapsDisarmed + 1
LM_ExecuteScript(fnResolvePath() .. "800 Complete Trap Stat Change.lua")