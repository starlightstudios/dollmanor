-- |[ ================================= Turn Post Exec: Human ================================== ]|
--Handles post-turn cutscenes if the player is a human.

-- |[Freezing]|
--If Mary has a clay infection in this room, the clay freezes and falls off.
if(gzTextVar.gzPlayer.sLocation == "Chilled Corridor C" and gzTextVar.gzPlayer.iClayInfection > -1) then
    gzTextVar.gzPlayer.iClayInfection = -1
    TL_SetProperty("Append", "You notice the clay that was on your body freeze. It crumbles and falls off. At least that's one thing you don't have to worry about." .. gsDE)
    TL_SetProperty("Create Blocker")
end

--When Mary is in the freezing caverns, this code takes priority. The clay infection is cancelled when this happens.
if(gzTextVar.bFreezingNoFollow == true) then
    
    --"Warm Respite" does not trigger this, nor do the immediate antechambers.
    if(gzTextVar.gzPlayer.sLocation == "Warm Respite" or gzTextVar.gzPlayer.sLocation == "Western Antechamber" or gzTextVar.gzPlayer.sLocation == "Southern Antechamber") then
        
        --Warm Respite actually zeroes the chill. The notification is present in the cutscene dialogue.
        if(gzTextVar.gzPlayer.sLocation == "Warm Respite" and gzTextVar.fMaryFreezeAmount > 0.000) then
            
            --Reset.
            gzTextVar.fMaryFreezeAmount = 0.0
    
            --Fix sprite.
            gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
            gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
            gzTextVar.gzPlayer.sLayer1 = "Null"
            TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
        end
        
    --The crystal rooms don't either.
    elseif(gzTextVar.gzPlayer.sLocation == "Crystal Altar C" or gzTextVar.gzPlayer.sLocation == "Crystal Altar D" or gzTextVar.gzPlayer.sLocation == "Crystal Altar E") then
    
    --Otherwise, the player is exposed to freezing.
    else
    
        --Compute the rate of freezing. The base rate is that Mary freezes in 25 turns.
        local iIndex = 1
        local faDenominators = {18.0, 25.0, 45.0, 1000.0}
        
        --If Mary is carrying a torch, up one denominator. It must be equipped.
        if(gzTextVar.gzPlayer.zWeapon ~= nil and gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
            iIndex = iIndex + 1
        end
        
        --If Mary is wearing the climber's coat, up one denominator.
        if(gzTextVar.gzPlayer.zArmor ~= nil and gzTextVar.gzPlayer.zArmor.sUniqueName == "climber's coat") then
            iIndex = iIndex + 1
        end
        
        --If Mary is wearing the Salamander Gloves, up one denominator.
        if(gzTextVar.gzPlayer.zGloves ~= nil and gzTextVar.gzPlayer.zGloves.sUniqueName == "salamander gloves") then
            iIndex = iIndex + 1
        end
        
        --Range-check.
        if(iIndex > #faDenominators) then iIndex = #faDenominators end
        local fFreezeRate = 1.0 / faDenominators[iIndex]
        
        --If the index is less than the max, increment Mary's freeze counter.
        if(iIndex < #faDenominators) then
            
            --Store old amount, update the freeze.
            local fOldAmount = gzTextVar.fMaryFreezeAmount
            gzTextVar.fMaryFreezeAmount = gzTextVar.fMaryFreezeAmount + (1.0 / faDenominators[iIndex])
            --io.write("Mary freeze counter: " .. gzTextVar.fMaryFreezeAmount .. "\n")
            
            --Notifications for crossing thresholds:
            if(fOldAmount < 0.200 and gzTextVar.fMaryFreezeAmount >= 0.200) then
                TL_SetProperty("Append", "You feel a chill set in. This place is colder than the arctic. You'd better find what you're looking for, or get back to a warm spot." .. gsDE)
                TL_SetProperty("Create Blocker")
                
            elseif(fOldAmount < 0.400 and gzTextVar.fMaryFreezeAmount >= 0.400) then
                TL_SetProperty("Append", "You shiver. You don't stop. The cold is starting to get to you." .. gsDE)
                TL_SetProperty("Create Blocker")
    
                --Query sprite becomes the ice TF.
                gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/MaryIceTF0"
                gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
                gzTextVar.gzPlayer.sLayer1 = "Null"
                TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
    
                TL_SetProperty("Append", "You realize this is no ordinary cold. Your skin is changing to ice before your eyes!" .. gsDE)
                TL_SetProperty("Create Blocker")
                
            elseif(fOldAmount < 0.600 and gzTextVar.fMaryFreezeAmount >= 0.600) then
                TL_SetProperty("Append", "Your teeth are chattering. Your skin is going numb and you can't warm up. You really need to get someplace warm." .. gsDE)
                TL_SetProperty("Create Blocker")
                
            elseif(fOldAmount < 0.800 and gzTextVar.fMaryFreezeAmount >= 0.800) then
    
                --Query sprite becomes the ice TF.
                gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/MaryIceTF0"
                gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
                gzTextVar.gzPlayer.sLayer1 = "Null"
                TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
                
                TL_SetProperty("Append", "You're numb all over, and you swear you can see through the ice on your skin. You don't have long before you freeze..." .. gsDE)
                TL_SetProperty("Create Blocker")
            
            --Mary freezes. This is in a subscript.
            elseif(gzTextVar.fMaryFreezeAmount >= 1.000) then
                LM_ExecuteScript(fnResolvePath() .. "903 Freeze.lua")
                return
            end
        end
    end

-- |[Claygirl!]|
--If the player has a clay infection, this handles that.
elseif(gzTextVar.gzPlayer.iClayInfection > -1) then
    
    --Increment.
    gzTextVar.gzPlayer.iClayInfection = gzTextVar.gzPlayer.iClayInfection + 1
    
    --Display notices as the infection progresses.
    if(gzTextVar.gzPlayer.iClayInfection == 10) then
        TL_SetProperty("Append", "The clay is very slowly working up your arm... You should find a place to [wash] it off!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.gzPlayer.iClayInfection == 20) then
        TL_SetProperty("Append", "The clay has taken over most of your arm and is starting to work on your body. You should [wash] it off soon!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.gzPlayer.iClayInfection == 35) then
        TL_SetProperty("Append", "The clay has coated most of your chest. You're not sure how it is multiplying, but it is. [wash] it off immediately!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.gzPlayer.iClayInfection == 40) then
        TL_SetProperty("Append", "You don't have long before the clay consumes you wholly. Do something..." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Full coverage. Player becomes a clay girl.
    elseif(gzTextVar.gzPlayer.iClayInfection >= 41) then
    
        --Determine the clay color. Mary's image should be: "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0"
        local sClayColor = string.sub(gzTextVar.gzPlayer.sQuerySprite, 46)
        local iIndicatorX = 6
        if(sClayColor ~= "Blue" and sClayColor ~= "Red" and sClayColor ~= "Yellow") then
            sClayColor = "Blue"
        end
        if(sClayColor == "Blue") then
            iIndicatorX = 6
        elseif(sClayColor == "Red") then
            iIndicatorX = 7
        elseif(sClayColor == "Yellow") then
            iIndicatorX = 8
        end
    
        TL_SetProperty("Append", "The clay spreads up your neck. It covers you. Completely." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor
        TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
        TL_SetProperty("Append", "Frightened, you tug at it, but your hands are already covered in clay. You can't get it off." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Then, you realize it. You can't get it off, because it's you. There is no you under the clay, you are the clay. Now it makes sense." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        --If both Jessie and Lauren are in the same room...
        if((gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsJessieFollowing) and (gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsLaurenFollowing)) then
            
            --Flags.
            gzTextVar.bIsJessieFollowing = false
            gzTextVar.bIsJessieMainHalled = false
            gzTextVar.bIsLaurenFollowing = false
            gzTextVar.bIsLaurenMainHalled = false

            --Dialogue.
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
            TL_SetProperty("Append", "'Mary? Mary, can you hear me? Let me help!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
            TL_SetProperty("Append", "Jessie approaches but you back away, waving her off. You can't speak, but somehow you can see despite not having real eyes." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You motion for them to run. You don't know how long you will stay yourself, but you don't want them to suffer the same fate." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Jessie nods and takes Lauren's hand. He doesn't want to, but she forces him to flee. You hope they can find some way to cure you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            --Flee Jessie.
            local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
            if(sLocation ~= "Null") then
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
            end
            
            --Flee Lauren.
            sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
            if(sLocation ~= "Null") then
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
            end
            
            --Rebuild locality info.
            fnRebuildEntityVisibility()
            fnBuildLocalityInfo()

        --Just Jessie.
        elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsJessieFollowing) then

            --Flags.
            gzTextVar.bIsJessieFollowing = false
            gzTextVar.bIsJessieMainHalled = false

            --Dialogue.
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
            TL_SetProperty("Append", "'Mary? Mary, can you hear me? Let me help!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
            TL_SetProperty("Append", "Jessie approaches but you back away, waving her off. You can't speak, but somehow you can see despite not having real eyes." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You motion for her to run. You don't know how long you will stay yourself, but you don't want Jessie to suffer the same fate." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Jessie nods, tears in her eyes, and flees. You hope she can find some way to cure you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            --Flee Jessie.
            local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
            if(sLocation ~= "Null") then
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
            end
            
            --Rebuild locality info.
            fnRebuildEntityVisibility()
            fnBuildLocalityInfo()

        --Just Lauren.
        elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsLaurenFollowing) then

            --Flags.
            gzTextVar.bIsLaurenFollowing = false
            gzTextVar.bIsLaurenMainHalled = false

            --Dialogue.
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
            TL_SetProperty("Append", "'Mary? Mary, are you still in there? Speak to me!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
            TL_SetProperty("Append", "Lauren approaches but you back away, afraid to let him touch you. You can't speak, but somehow you can see despite not having real eyes." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You point at yourself, and then make an X in the air. You shake your head. It's too late for you. Lauren begins to cry, understanding what this means." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Lauren runs from you. You hope he can find some way to cure you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            --Flee Lauren.
            local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
            if(sLocation ~= "Null") then
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
            end
            
            --Rebuild locality info.
            fnRebuildEntityVisibility()
            fnBuildLocalityInfo()

        end
        
        --Change Mary's indicator.
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
        
        TL_SetProperty("Append", "You look down at yourself. Your legs are a formless mass of clay, but you can move them with a bit of thought." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Unlike the other creatures in the manor, you seem to still be yourself. At least you won't attack Jessie and Lauren." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Then, a strange thought occurs to you. A feeling inside your head - or the clay where your head would be - that you're not done yet." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You start reshaping yourself. Your bust is the wrong size, so you make it bigger. Your face needs to be perfectly smooth, so you smooth it." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "There is an ideal in your head, and you change yourself until you reach that ideal. You feel better once you have remolded yourself." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Yet this strange feeling, like hunger but not quite, persists. You reach your clay hand into your head and feel at it directly." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You squeeze and squish. You reshape the feeling. It takes some time but you know what it should feel like, somehow." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Then, it is done. You remove your hand and reform your featureless clay face. The feeling is gone. You feel complete." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You wonder what you should do now. Maybe find Jessie and Lauren?" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Yes, that's what you should do. You definitely need to cover them in clay. They should be just like you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You set off into the manor..." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        --Change other properties.
        gzTextVar.bIsPlayerSurrendering = false
        gzTextVar.gzPlayer.sFormState = "Claygirl"

        --Fullheal the player.
        gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
        gzTextVar.gzPlayer.bIsCrippled = false
        TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
        --Change Mary's properties.
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor
        TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
        
        --Unlock all doors.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
    
    end
end

-- |[Same Tile As Pygmalie]|
if(gzTextVar.bMetPygmalie == false and gzTextVar.iPygmalieIndex ~= nil and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation) then
    gzTextVar.bMetPygmalie = true
    TL_SetProperty("Append", "There is a woman here. You may want to [talk pygmalie] to her." .. gsDE)
    TL_SetProperty("Create Blocker")
end

-- |[Same Tile as Jessie or Lauren]|
if(gzTextVar.bNotifiedAboutTalk == false and gzTextVar.iJessieIndex ~= nil and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation) then
    gzTextVar.bNotifiedAboutTalk = true
    TL_SetProperty("Append", "You've found Jessie! You should [talk jessie] with her." .. gsDE)
    TL_SetProperty("Create Blocker")
end

if(gzTextVar.bNotifiedAboutTalk == false and gzTextVar.iLaurenIndex ~= nil and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation) then
    gzTextVar.bNotifiedAboutTalk = true
    TL_SetProperty("Append", "You've found Lauren! You should [talk lauren] with him." .. gsDE)
    TL_SetProperty("Create Blocker")
end

-- |[Strange Antechamber]|
--Marks the think handler.
if(gzTextVar.gzPlayer.sLocation == "Strange Antechamber") then
    gzTextVar.bSeenAntechamber = true
end

--Setup.
local sBasePath = fnResolvePath()

--"Hall of Painted Dolls" cutscene.
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 Hall of Painted Dolls.lua")
if(gbHandledSubscript == true) then return end

--"Strange Antechamber" cutscene.
LM_ExecuteScript(sBasePath .. "201 Strange Antechamber.lua")
if(gbHandledSubscript == true) then return end

--Blue light scenes.
LM_ExecuteScript(sBasePath .. "202 Blue Light.lua")
if(gbHandledSubscript == true) then return end

--End of phase 2.
LM_ExecuteScript(sBasePath .. "203 Pygmalies Study.lua")
if(gbHandledSubscript == true) then return end

--Entering the string trap.
LM_ExecuteScript(sBasePath .. "204 String Trap Entry.lua")
if(gbHandledSubscript == true) then return end

--Overcoming the string trap.
LM_ExecuteScript(sBasePath .. "205 String Trap Completed.lua")
if(gbHandledSubscript == true) then return end

--Entering the winery.
LM_ExecuteScript(sBasePath .. "206 Wine Trap Entry.lua")
if(gbHandledSubscript == true) then return end

--Activating the winery trap.
LM_ExecuteScript(sBasePath .. "207 Wine Trap Activation.lua")
if(gbHandledSubscript == true) then return end

--Entering/Exiting the ice caves.
LM_ExecuteScript(sBasePath .. "208 Ice Trap Entry.lua")
if(gbHandledSubscript == true) then return end

--Finding the crystal in the ice caves.
LM_ExecuteScript(sBasePath .. "209 Ice Trap Crystal.lua")
if(gbHandledSubscript == true) then return end

--Entering the clay house.
LM_ExecuteScript(sBasePath .. "210 Clay House Entry.lua")
if(gbHandledSubscript == true) then return end

--Clay house solved.
LM_ExecuteScript(sBasePath .. "212 Clay House Solved.lua")
if(gbHandledSubscript == true) then return end

--Clay house timers.
LM_ExecuteScript(sBasePath .. "211 Clay House Active.lua")
if(gbHandledSubscript == true) then return end
    