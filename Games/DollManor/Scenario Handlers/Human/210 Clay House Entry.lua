-- |[Clay House Entry]|
--It is the clay house!

--In these rooms, your friends do not follow.
if(gzTextVar.gzPlayer.sLocation == "Curious House Entry") then
    gbHandledSubscript = true
    if(gzTextVar.bClayDisarmed == false) then
        gzTextVar.bClayNoFollow = true
    end
    
    return
end

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Curious Building Entrance") then return end

--Mark handling this.
gbHandledSubscript = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

-- |[Entering the Antechamber]|
if(gzTextVar.gzPlayer.sPrevLocation == "Cobblestone Path F") then

    --Do nothing when re-entering.
    if(gzTextVar.bClayDisarmed == true) then return end

    --First time.
    if(gzTextVar.bClayEntryCutscene == false) then
        
        --Flag.
        gzTextVar.bClayEntryCutscene = true
        
        --First pass.
        if(gzTextVar.bAtLeastOneTrapSeen == false) then
            gzTextVar.bAtLeastOneTrapSeen = true
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, wait. There's something weird.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah. This house is different.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What's different about it?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Just standing here is making my stomach turn.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know what it is, but my hair is standing on end despite the rain.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah, me too.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What do we do?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Stay here. I'll check it out.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'No, no it's dangerous.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's not going to get any less dangerous until we escape, Lauren.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Keep your eyes sharp and make sure nothing happens to Jessie.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oy!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Okay...'" .. gsDE)
            fnDialogueFinale()
        
        --Repeats.
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sinking feeling, growing sense of dread, nameless fear?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah. Something is up with this house.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's got to be something important. I'll take a look.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You want us to just stand out in the rain?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It's not safe inside.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Sheesh...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll be back.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Repeats.
    else
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Going to take another try, Mary?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Of course. I can figure this out.'" .. gsDE)
        fnDialogueFinale()
    end

-- |[Exiting the Trap]|
elseif(gzTextVar.gzPlayer.sPrevLocation == "Curious House Entry") then

    --Follow.
    gzTextVar.bClayNoFollow = false
    
    --Reset timer.
    gzTextVar.iClayTimer = 0

    --Found the crystal and got out.
    if(gzTextVar.bClayDisarmed == true) then
        
        --Cutscene.
        if(gzTextVar.bClayCompletedCutscene == false) then
            gzTextVar.bClayCompletedCutscene = true
            
            --First time.
            if(gzTextVar.bAtLeastOneTrapHandled == false) then
                gzTextVar.bAtLeastOneTrapHandled = true
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? What happened?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There was a room with a jewel case in it. I found a piece of crystal inside.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'That crystal looks scary...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah, but every time I disturbed the house, it tried to close off the room it was held in.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It must be important.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'So the house itself was alive, and guarding that crystal?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'That's my best guess. It was made to do that, and only that.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'But the feeling is gone. Does that mean it's safe to go inside?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I think so.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know how this place works, but there are rules. So if you feel that sinking feeling, stay put.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'We got one, we can get more. They're hidden for a reason, so let's find out what that reason is. Lead on, Mary.'" .. gsDE)
                fnDialogueFinale()
            
            --Repeats.
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? Did you do it?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Found a crystal room, just like the last one.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You're doing great, Mary!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks, kiddo. You're doing great, too.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Can we go inside now?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Maybe someplace else. It may be 'disarmed' but I still don't like this house.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The feeling of dread has evaporated. The house is safe to explore." .. gsDE)
                fnDialogueFinale()
            end
        end
    
    --Otherwise:
    else
    
        --Unbar the southern door.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Crystal Altar A", "C Normal", "N", ciModifyVisibility, 0)
    
        --Clay house was activated:
        if(gzTextVar.bSawClayHouse) then
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, we heard... things...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The damn house is alive.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Alive?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah. Alive.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'... But you're going back in?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's protecting something important.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It seems to go to sleep when I'm not inside, but it wakes up when I touch something, or if I take too long.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'We'll be fine out here in the rain. Figure it out, Mary. We're counting on you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll think of something.'" .. gsDE)
            fnDialogueFinale()
        
        --Nope.
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Did anything happen?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No, nothing. But I'm extremely suspicious.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'There must be something important in there.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Maybe.'" .. gsDE)
            fnDialogueFinale()
        end
    end
end
    