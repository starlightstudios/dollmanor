-- |[Freeze]|
--Mary freezes solid and becomes an ice sculpture!
TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Transformation/MaryIceTF1", ciImageLayerDefault)
TL_SetProperty("Append", "It's so cold... so cold..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You think back to school. Hypothermia is like going to sleep, a very peaceful way to die. This isn't that. You feel calm, at ease, but not sleepy." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You watch with fascination as the ice works its way through your skin and into your veins. You can see your blood freezing. Your muscles turn to ice." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It feels... like nothing. Nothing at all. It is completely numb." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 2, 5, ciCodePlayer)
TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryIce", ciImageLayerDefault)
TL_SetProperty("Append", "The ice snakes up your veins and into your heart. It pumps everywhere despite your heart freezing solid. And worst of all, you don't even care." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Nothing matters. You don't feel a thing. Your friends? Who cares. Escaping? Why bother." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "There's no point in doing anything, you should just stay here and freeze forever. Become ice, like the walls of the cave." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "But that would not do. There is one thing your frozen soul cares for - your creator. The woman who made this cavern of ice, the woman who is responsible for freezing you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It doesn't matter if you make her happy or not, but you'll do what she wills. You have no reason not to." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "And what she wills is that Jessie and Lauren become solid ice just as you did. So, you will do that. Now. Go." .. gsDE)
TL_SetProperty("Create Blocker")

--Flag.
gzTextVar.gzPlayer.sFormState = "Ice"
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryIce"
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
gzTextVar.gzPlayer.sLayer1 = "Null"
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    
--Unlock all doors.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
--Achievement.
Steam_UnlockAchievement(gciAchievement_ItsColdWhereWeAre)

