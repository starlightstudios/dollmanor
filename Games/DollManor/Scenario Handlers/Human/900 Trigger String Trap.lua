-- |[Trigger String Trap]|
--This occurs when the player moves the wrong direction in the string trap rooms.

--Close the entry to the grand archives if the player opened it.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Grand Archive A", "C Normal", "N", ciModifyVisibility, 1)

--Check if Mary has at least one pair of scissors in her inventory. If so, dull the scissors but prevent the trap from affecting her.
local iScissorsIndex = fnGetIndexOfItemInInventory("scissors")
if(iScissorsIndex ~= -1) then
    
    --Flag.
    gzTextVar.bStringTrapSawStrings = true
    
    --Dialogue.
    TL_SetProperty("Append", "As you move forward, you realize your hand is caught on something!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There are almost-invisible strings hanging in the room, and one has snagged on your hand. Others threaten, but you think quickly." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You quickly pull out the pair of scissors you are carrying and snip at the string. It is incredibly strong, far stronger than even piano wire." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The string snaps and withdraws into the darkness above. Your scissors were blunted by the ordeal, and will need to be sharpened before you can use them again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "But whatever nefarious purpose the strings served has likewise been blunted. You'll need to be more careful..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Replace with the blunted variant.
    gzTextVar.gzPlayer.zaItems[iScissorsIndex].sUniqueName = "blunted scissors"
    gzTextVar.gzPlayer.zaItems[iScissorsIndex].sDisplayName = "blunted scissors"
    gzTextVar.gzPlayer.zaItems[iScissorsIndex].sHandlerScript = gzTextVar.sRootPath .. "Item Handlers/ToolsConsumables/Blunted Scissors.lua"
    
    --Rebuild locality info.
    fnBuildLocalityInfo()
    return
end

--Mary is not affected yet:
if(gzTextVar.iMaryStringTrapState == 0) then
    
    --Flag.
    gzTextVar.bStringTrapSawStrings = true
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap0", ciImageLayerDefault)
    TL_SetProperty("Append", "As you move forward, you realize your hand is caught on something!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Worse, your other hand immediately snaps back, gripped by some unseen object!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Somehow, without you noticing, two almost-invisible strings have lashed themselves around your wrists. You struggle against them, but to no avail." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "With horror, you hear a creaking sound above you. You look up into the darkness, but can't see what's causing it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The strings go taut and you are hefted off the ground! The strings bite into your wrists and squeeze as you are pulled aloft, into the darkness." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You can make out the dim outline of a wardrobe, somehow attached to the ceiling. Its doors are open and the strings pull you inside!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You struggle and fight in the darkness as you feel the clothes in the wardrobe running all over your body. You kick and fight, and after several tense moments, the door opens." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap1", ciImageLayerDefault)
    TL_SetProperty("Append", "You plummet to the ground and land on your... boots?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A fall like that should have broken your legs, but you feel no pain at all. You quickly check over your new clothes, but see nothing out of the ordinary." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It seems the wardrobe merely wanted to change your outfit. You shudder to think what would have happened had you stayed inside a moment longer..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Query sprite becomes the goth outfit.
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryStringTrap1"
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
    TL_SetProperty("Unregister Image")
    
    --Flag.
    gzTextVar.iMaryStringTrapState = 1
    
    
--Mary was already dressed as a goth:
elseif(gzTextVar.iMaryStringTrapState == 1) then
    
    --Flag.
    gzTextVar.bStringTrapSawStrings = true
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap1", ciImageLayerDefault)
    TL_SetProperty("Append", "As you move forward, you realize your hand is caught on something!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your stomach drops as you realize you've fallen for the trap again! The familiar snag of your hands being gripped by strings fills your heart with dread." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The grinding sound of the wardrobe shuffling to its prey is followed by you being yanked into the air again, into its darkened maw." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Once inside, you feel clothes press against you. You stomp and kick and fight, shoving them away." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You managed to catch a latch or a weak spot, and the door bursts open. You fall free and land on the ground without making a noise at all." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 0, 5, ciCodePlayer)
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap2", ciImageLayerDefault)
    TL_SetProperty("Append", "You realize you're feeling lighter than before. With horror, you see that the wardrobe hasn't changed your clothes, but your body." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You look just like the doll girls in the manor, but you're still you. For now. There must be a way to change back!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your body feels much lighter and stronger than before. The smooth plastic skin actually looks good on you..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "No, you need to escape. You shake your head and decide what to do next." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Query sprite becomes the goth outfit.
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryStringTrap2"
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
    TL_SetProperty("Unregister Image")
    
    --Flag.
    gzTextVar.iMaryStringTrapState = 2

--Mary was already transformed into a doll:
elseif(gzTextVar.iMaryStringTrapState == 2) then
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap2", ciImageLayerDefault)
    TL_SetProperty("Append", "The familiar feeling of a string gripping you is met by quiet resignation. You've been through this before. You feel dread well up." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your body is much lighter than before, and your ball-jointed wrists are much more durable. You feel barely anything as the strings heft you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The wardrobe locks you in again, and the clothes brush against you. You kick and fight, you struggle, and it does nothing." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap3", ciImageLayerDefault)
    TL_SetProperty("Append", "Within the darkness of the wardrobe, you see yourself. Why are you even fighting? You're already a doll, there's no way to escape. This is your fate." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You could fight, and maybe stay human for a few more minutes. But why? Being plastic and cute feels so much better, doesn't it?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryStringTrap4", ciImageLayerDefault)
    TL_SetProperty("Append", "Yes, that's right. You prefer being a doll. You've never felt stronger, more alert, healthier, and... happier?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Of course you've never been happier! Being a cute gothic lolita is the thing you've always wanted! Why fight it?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You realize the wardrobe has lowered you to the ground. The strings detach as you smile darkly to yourself." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator. Yes. The woman you met earlier is your creator, the one who made you this way. You want to serve her, protect her, love her with all your heart. And make more sisters for her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The two humans outside don't even realize the wonderful gift they are about to receive. Go to them and lead them to the strings..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Goth A", ciImageLayerSkin)
    
    --Flag.
    gzTextVar.iMaryStringTrapState = 3
    gzTextVar.gzPlayer.sFormState = "Doll Goth"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Goth A"
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
    
    --Deactivate the trap.
    gzTextVar.bStringTrapActive = false
    
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_TurnedOutWonderfully)

end
