-- |[Complete Trap Stat Change]|
--Fires whenever a trap is completed. Causes enemy dolls to change their stats. Claygirls, Titans, and the Stranger are unaffected.
-- Existing dolls get an upgrade, while newly spawned dolls have the global stat bonus increased.
local bChangedAnything = false

--Every time a trap is completed, the stranger can respawn. If currently alive, the stranger will not respawn if defeated
-- since the flag is disabled after defeat.
gzTextVar.bStrangerCanRespawn = true

--Zero traps, used for debug only.
if(gzTextVar.iTrapsDisarmed == 0) then
    TL_SetProperty("Append", "Your debug code resets dolls back to their base stats." .. gsDE)
    TL_SetProperty("Create Blocker")
    bChangedAnything = true
    gzTextVar.iDollBonusHP = 0
    gzTextVar.iDollBonusAtp = 0
    gzTextVar.fDollBonusSpd = 0.00

--Upon completing two traps, all dolls increase in power.
elseif(gzTextVar.iTrapsDisarmed == 2) then
    bChangedAnything = true
    TL_SetProperty("Append", "A sense of dread overcomes you. You don't know how you know, but the dolls in the manor have become stronger. Faster. More ruthless." .. gsDE)
    TL_SetProperty("Create Blocker")
    gzTextVar.iDollBonusHP = 12
    gzTextVar.iDollBonusAtp = 1
    gzTextVar.fDollBonusSpd = -0.10
    
    --Completing the second trap spawns a few more enemies.
    for i = 1, #gzTextVar.zRespawnList, 1 do
        local sID = gzTextVar.zRespawnList[i].sIdentity
        if(sID == "Clay A") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnClayGirl(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        end
    end

--Four traps, oh boy.
elseif(gzTextVar.iTrapsDisarmed == 4) then
    bChangedAnything = true
    TL_SetProperty("Append", "A sense of dread overcomes you. The dolls are even more powerful than before." .. gsDE)
    TL_SetProperty("Create Blocker")
    gzTextVar.iDollBonusHP = 25
    gzTextVar.iDollBonusAtp = 2
    gzTextVar.fDollBonusSpd = -0.15
end

--Run across all existing dolls and update their stats.
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    --This flag must exist. Entities that do not scale have it as false, or nil.
    if(gzTextVar.zEntities[i].zCombatTable == nil or gzTextVar.zEntities[i].zCombatTable.bScalesWithPhase == nil or gzTextVar.zEntities[i].zCombatTable.bScalesWithPhase == false) then
    
    --Scale!
    else
    
        -- |[Dolls]|
        if(gzTextVar.zEntities[i].sAIHandlerHostility == "Doll") then
            --Easy:
            if(gzTextVar.sDifficulty == "Easy") then
                local fTimeFactor = 2.00 + gzTextVar.fDollBonusSpd
                gzTextVar.zEntities[i].zCombatTable.iHealth = 27 + gzTextVar.iDollBonusHP
                gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
                gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4 + gzTextVar.iDollBonusAtp, 300 * fTimeFactor, 0}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5 + gzTextVar.iDollBonusAtp, 400 * fTimeFactor, 0}
            
            --Normal:
            elseif(gzTextVar.sDifficulty == "Normal") then
                local fTimeFactor = 1.00 + gzTextVar.fDollBonusSpd
                gzTextVar.zEntities[i].zCombatTable.iHealth = 35 + gzTextVar.iDollBonusHP
                gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
                gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4 + gzTextVar.iDollBonusAtp, 400 * fTimeFactor, 0} --0.60 DPS
                gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 6 + gzTextVar.iDollBonusAtp, 500 * fTimeFactor, 0} --0.72 DPS
            
            --Hard:
            else
                local fTimeFactor = 1.00 + gzTextVar.fDollBonusSpd
                gzTextVar.zEntities[i].zCombatTable.iHealth = 45 + gzTextVar.iDollBonusHP
                gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
                gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4 + gzTextVar.iDollBonusAtp, 300 * fTimeFactor, 0} --0.80 DPS
                gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 6 + gzTextVar.iDollBonusAtp, 400 * fTimeFactor, 0} --0.90 DPS
            end
        
        -- |[Claygirls]|
        else
            --Easy:
            if(gzTextVar.sDifficulty == "Easy") then
                local fTimeFactor = 2.00 + gzTextVar.fDollBonusSpd
                gzTextVar.zEntities[i].zCombatTable.iHealth = 35 + gzTextVar.iDollBonusHP
                gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
                gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100,  5 + gzTextVar.iDollBonusAtp, 360 * fTimeFactor, 0}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {  5,  5 + gzTextVar.iDollBonusAtp,  90 * fTimeFactor, 0}
            
            --Normal:
            elseif(gzTextVar.sDifficulty == "Normal") then
                local fTimeFactor = 1.50 + gzTextVar.fDollBonusSpd
                gzTextVar.zEntities[i].zCombatTable.iHealth = 40 + gzTextVar.iDollBonusHP
                gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
                gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100,  5 + gzTextVar.iDollBonusAtp, 360 * fTimeFactor, 0}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = { 10,  5 + gzTextVar.iDollBonusAtp,  90 * fTimeFactor, 0}
            
            --Hard:
            else
                local fTimeFactor = 1.00 + gzTextVar.fDollBonusSpd
                gzTextVar.zEntities[i].zCombatTable.iHealth = 45 + gzTextVar.iDollBonusHP
                gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
                gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
                gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100,  5 + gzTextVar.iDollBonusAtp, 360 * fTimeFactor, 0} --0.83 DPS
                gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = { 30,  5 + gzTextVar.iDollBonusAtp,  90 * fTimeFactor, 0} --3.33 DPS (!)
            end
        end
    end
end
