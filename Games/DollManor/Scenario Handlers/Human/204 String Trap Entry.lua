-- |[String Trap Entry]|
--Much like the Strange Antechamber, Mary's friends stay at the entrance.

-- |[Door Handling/Trap Priming]|
if(gzTextVar.bStringTrapDisarmed == false) then
    
    --Entrance to the grand archives:
    if(gzTextVar.gzPlayer.sLocation == "Grand Archive A") then
        
        --Prime the trap.
        gzTextVar.bStringTrapActive = true
        gzTextVar.bStringTrapNoFollow = true
        
        --Shut the door to the north.
        local iRoomIndex = fnGetRoomIndex("Grand Archive A")
        local sDoorState = string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorN, 1, 1)
        if(sDoorState == "O") then
            TL_SetProperty("Append", "Jessie closes the door behind you." .. gsDE)
            LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Grand Archive A", "C Normal", "N", ciModifyVisibility, 1)
        end
        return
    
    --These other rooms make sure the door is closed. No text is displayed.
    elseif(gzTextVar.gzPlayer.sLocation == "Grand Archive B" or gzTextVar.gzPlayer.sLocation == "Grand Archive P") then
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Grand Archive A", "C Normal", "N", ciModifyVisibility, 1)
    end
end

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Eerie Antechamber") then return end

--Mark handling this.
gbHandledSubscript = true

--Entering the antechamber un-primes the trap.
gzTextVar.bStringTrapActive = false
gzTextVar.bStringTrapNoFollow = false

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

-- |[Entering the Antechamber]|
if(gzTextVar.gzPlayer.sPrevLocation ~= "Grand Archive A" and gzTextVar.gzPlayer.sPrevLocation ~= "Null") then
    
    --If disarmed, skip.
    if(gzTextVar.bStringTrapDisarmed == true) then return end
    
    --First time.
    if(gzTextVar.bStringTrapSeenAntechamber == false) then
        
        --Flag.
        gzTextVar.bStringTrapSeenAntechamber = true
        
        --First pass.
        if(gzTextVar.bAtLeastOneTrapSeen == false) then
            gzTextVar.bAtLeastOneTrapSeen = true
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, wait. There's something weird.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Huh? Did you hear something?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'No. Don't you feel it? Your stomach turning over?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It makes me feel sick.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah, I guess I do feel something. It's dangerous.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What is?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know, but all of us suddenly feeling nervous for no reason? Not a coincidence.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'We should turn back.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I smell a trap.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you going to keep going, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll take a look. Keep Lauren safe. I'll come straight back, won't be a minute.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Close the door behind me, and don't make a sound.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Right. Don't take any risks.'" .. gsDE)
            fnDialogueFinale()
        
        --Repeats.
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sinking feeling, growing sense of dread, nameless fear?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah. You're going ahead?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's what I do. Stay here and don't make a sound.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Repeats.
    else
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Going to take another try, Mary?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's got to be something important in there. Keep Lauren safe and don't make a peep.'" .. gsDE)
        fnDialogueFinale()
    end

-- |[Exiting the Trap]|
elseif(gzTextVar.gzPlayer.sPrevLocation == "Grand Archive A") then

    --Disarmed it, finishes this subplot.
    if(gzTextVar.bStringTrapDisarmed == true) then
        
        --Cutscene.
        if(gzTextVar.bStringTrapDisarmedCutscene == false) then
            gzTextVar.bStringTrapDisarmedCutscene = true
            
            --First time.
            if(gzTextVar.bAtLeastOneTrapHandled == false) then
                gzTextVar.bAtLeastOneTrapHandled = true
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, you did it!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Were you peeking?'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'No, but the awful feeling went away. You must have done it!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I got to the end and found a jewel case. I felt a lot better as soon as I opened it.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I knew it. I knew you could do it.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This place follows rules. I don't know what they are, but there are rules. You can feel the trap, and the trap knows when you've beaten it.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Another feeling of yours?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Got a better explanation?'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'So what do we do now?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Find the rest of these traps, and overcome them. This crystal I found must be important.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I guess that's as good a plan as any. Maybe the crystal is the key to getting out of here.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Maybe. Let's get going.'" .. gsDE)
                fnDialogueFinale()
            
            --Repeats.
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, you did it!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'm starting to get the hang of this.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You found another crystal piece?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah. As soon as I took it, the feeling went away.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'This is what we need to do. You're amazing, Mary.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks, squirt. You keep being a big boy and protecting Jessie.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Sheesh...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Let's get going.'" .. gsDE)
                fnDialogueFinale()
            end
        end
        return
    end

    --Did not see the trap:
    if(gzTextVar.bStringTrapSawStrings == false) then
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What did you see?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's an archive of old books and such, with a high ceiling. It's dead quiet.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Please don't go back in...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I know, there's something really wrong in there. Every fibre of my body screams to run away.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'But you're going back in, aren't you?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's something important in there. I think we need it, but I don't know how it's guarded. We should look for clues.'" .. gsDE)
        fnDialogueFinale()

    --Saw the trap.
    else
        
        --Mary is normal:
        if(gzTextVar.iMaryStringTrapState == 0) then
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What did you see?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's an archive of old books and such, with a high ceiling. It's dead quiet.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If you look closely, there are strings hanging from the ceiling. It's a trap.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'The strings will get you, and pull you up, and you won't come down.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Can you go around them?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'They're hard to see. It's too risky, there's got to be a pattern.'" .. gsDE)
            fnDialogueFinale()
    
        --Mary in a goth outfit:
        elseif(gzTextVar.iMaryStringTrapState == 1) then
        
            --Setup.
            local saMaryHumList = {"Root/Images/DollManor/Characters/Mary"}
        
            --Dialogue.
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'Mary! What are you wearing?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'Take it off, right now!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList,    "'I'm going to! Lauren, look away.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'Yeah yeah. Don't need to tell me.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList,    "Lauren looks away as you strip the gothic clothes off. Mysteriously, your normal clothes are sitting neatly folded in the corner." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'How long were my clothes sitting there?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'Huh? I didn't even notice.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'Can I turn around now?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'I'm back to normal.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'So what happened?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'There are strings in there. If you touch them, they grab you, and pull you into a wardrobe.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'And it changed your clothes?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'I managed to break out before it did anything else. That probably wasn't the last thing it was going to do.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'If you didn't break out...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'But I did, so don't worry.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "Lauren's lip trembles, but he says nothing." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'There's got to be something important, why else would they put that kind of trap in there?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'There's got to be a pattern to the strings...'" .. gsDE)
            fnDialogueFinale()
            
            --Reset.
            gzTextVar.iMaryStringTrapState = 0
            gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
            gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
            gzTextVar.gzPlayer.sLayer1 = "Null"
            TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
        
        --Mary is doll TF'd.
        elseif(gzTextVar.iMaryStringTrapState == 2) then
        
            --Setup.
            local saMaryHumList = {"Root/Images/DollManor/Characters/Mary"}
        
            --Dialogue.
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'No no no no!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'Mary! What - is that you?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList,    "'For the moment. Lauren, turn around, I need to change.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList,    "Lauren looks away as you strip the gothic clothes off. Mysteriously, your normal clothes are sitting neatly folded in the corner." .. gsDE)
            TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 0, 3, ciCodePlayer)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "As soon as you put your old clothes on, your plastic skin softens and returns to its familiar fleshy feeling." .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'But you're back to normal now? Somehow?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'Yes, I think. The clothes are... magic? I guess it's the same magic as my card deck, and whatever makes the doll people move.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'But it's not the same. And if you stay a doll too long, you'll never go back.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'And just how do you know that, Lauren?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,    "'I - I don't know,' Lauren says, his lip trembling." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'Hey, hey, she's not accusing you. She's just asking.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList,    "'Sorry, Lauren.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'I broke out of the trap before it got me, but any more and I don't think I'd be... me.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'There's got to be something important, why else would they put that kind of trap in there?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryHumList, "'There's got to be a pattern to the strings...'" .. gsDE)
            fnDialogueFinale()
            
            --Reset.
            gzTextVar.iMaryStringTrapState = 0
            gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
            gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
            gzTextVar.gzPlayer.sLayer1 = "Null"
            TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
        
        --The 3 handler is in the doll code, as Mary is transformed at that point.
    
        end
    end

end
    