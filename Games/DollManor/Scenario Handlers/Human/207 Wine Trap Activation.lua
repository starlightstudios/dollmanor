-- |[String Trap Completion]|
--Did it!

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Crystal Altar B") then return end

--No duplicates.
if(gzTextVar.bGoopTrapActivated == true) then return end

--Mark handling this.
gbHandledSubscript = true

--Flag.
gzTextVar.bGoopTrapActivated = true

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local saMaryHumList = {"Root/Images/DollManor/Characters/Mary"}
gzTextVar.iMaryStringTrapState = 0
        
--Dialogue.
fnDialogueCutscene("You", iMaryCnt, saMaryList, "The quiet room at the back of the brewery has a jewel case on a pedestal. On it sits an ornate jewelry box, thoroughly out of place in the brewery." .. gsDE)
fnDialogueCutscene("You", iMaryCnt, saMaryList, "It's too quiet, and it was too easy. You search it for traps, but there aren't any. No hidden latches, nothing attached. Gingerly, you open it." .. gsDE)
fnDialogueCutscene("You", iMaryCnt, saMaryList, "When you do, the latch opens and reveals a crystal piece, broken from a larger object. And you hear something slithering..." .. gsDE)
if(gzTextVar.bHasReadOrangeBook == true and gzTextVar.bHasSeenOrangeBookExtra == false) then
    gzTextVar.bHasSeenOrangeBookExtra = true
    fnDialogueCutscene("You", iMaryCnt, saMaryList, "The crystal reminds you of the storybook you read. Light seems to grow dim around it." .. gsDE)
    fnDialogueCutscene("You", iMaryCnt, saMaryList, "It's important enough that there's now something in the brewery behind you. Get it and get out!" .. gsDE)
else
    fnDialogueCutscene("You", iMaryCnt, saMaryList, "Nothing to do about it now. Get the crystal and get out!" .. gsDE)
end

-- |[Spawn the baddies]|
for i = 1, #gzTextVar.zRespawnList, 1 do
    if(gzTextVar.zRespawnList[i].sIdentity == "Goop A") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    elseif(gzTextVar.zRespawnList[i].sIdentity == "Goop B") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    elseif(gzTextVar.zRespawnList[i].sIdentity == "Goop C") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    elseif(gzTextVar.zRespawnList[i].sIdentity == "Goop D") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    elseif(gzTextVar.zRespawnList[i].sIdentity == "Goop E") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    elseif(gzTextVar.zRespawnList[i].sIdentity == "Goop F") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    elseif(gzTextVar.zRespawnList[i].sIdentity == "Goop G") then
        fnSpawnGoop(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
    end
end
