-- |[Ice Trap Entry]|
--Ice Trap entry, friends stay there as usual. This script can also act as the finale since escaping
-- the trap at the end also disarms it.

--In these rooms, your friends do not follow.
if(gzTextVar.gzPlayer.sLocation == "Western Antechamber" or gzTextVar.gzPlayer.sLocation == "Southern Antechamber") then
    gbHandledSubscript = true
    if(gzTextVar.bFreezingTrapDisarmed == false) then
        gzTextVar.bFreezingNoFollow = true
    end
    return
end

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Warm Respite") then return end

--Mark handling this.
gbHandledSubscript = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

-- |[Entering the Antechamber]|
if(gzTextVar.gzPlayer.sPrevLocation == "Chilled Corridor C") then

    --First time.
    if(gzTextVar.bFreezingAntechamberScene == false) then
        
        --Flag.
        gzTextVar.bFreezingAntechamberScene = true
        
        --First pass.
        if(gzTextVar.bAtLeastOneTrapSeen == false) then
            gzTextVar.bAtLeastOneTrapSeen = true
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, wait. There's something weird.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Huh? Did you hear something?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'No. Don't you feel it? Your stomach turning over?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It makes me feel sick.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah, I guess I do feel something. It's dangerous.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What is?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know, but all of us suddenly feeling nervous for no reason? Not a coincidence.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'We should turn back.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This place is freezing, it's way too cold. It's a trap.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you going to keep going, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll take a look. It's warm here, so stay here with Lauren and keep him safe.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll come back if it's too cold.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah, we're not exactly dressed for the weather.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'This wood stove is warm. We should be okay here.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'It doesn't upset you that it has wood in it? Who put it here?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If it's one of those dolls, run.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah, yeah. Get going.'" .. gsDE)
            fnDialogueFinale()
        
        --Repeats.
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sinking feeling, growing sense of dread, nameless fear?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah. That, and the extreme cold.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's got to be something important. I'll take a look.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'This wood stove is warm. We should be okay here.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'It doesn't upset you that it has wood in it? Who put it here, who stocked it?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If it's one of those dolls, run.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah, yeah. Get going.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Repeats.
    else
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Going to take another try, Mary?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Of course. If we have to go this way, I won't let a little ice stop me.'" .. gsDE)
        fnDialogueFinale()
    end

-- |[Exiting the Trap]|
elseif(gzTextVar.gzPlayer.sPrevLocation == "Western Antechamber" or gzTextVar.gzPlayer.sPrevLocation == "Southern Antechamber") then

    --Follow.
    gzTextVar.bFreezingNoFollow = false

    --Found the crystal and got out.
    if(gzTextVar.bFreezingTrapFoundCrystal == true) then
        
        --Cutscene.
        if(gzTextVar.bFreezingEndCutscene == false) then
            gzTextVar.bFreezingEndCutscene = true
            gzTextVar.bFreezingTrapDisarmed = true
            
            --First time.
            if(gzTextVar.bAtLeastOneTrapHandled == false) then
                gzTextVar.bAtLeastOneTrapHandled = true
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? What happened?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There was a room with a jewel case in it. I found a piece of crystal inside.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It looks scary...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I know, but it was hidden in a cave that makes Birmingham look hospitable. It must be special.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'A broken crystal? Are there others like it?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Could be, but not in these caves. We should look around the mansion and see if we can find any other traps.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You hear that, Lauren? We're winning! We're going to get out of here just fine, and all you have to do is be brave a few more times!'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I believe in you, Mary.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Oh don't get so sappy. Let's go.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You feel the cold in the caves relax. Somehow, the cold is no longer as penetrating. You can explore safely now." .. gsDE)
                fnDialogueFinale()
            
            --Repeats.
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? Did you do it?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Found a crystal room, just like the last one.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You're doing great, Mary!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks, kiddo. You're doing great, too.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Jessie is really worrying about you. I try to calm her down.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh that's true, except reverse the roles.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Glad to see you're in such high spirits. Let's go.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You feel the cold in the caves relax. Somehow, the cold is no longer as penetrating. You can explore safely now." .. gsDE)
                fnDialogueFinale()
            end
        end
    
        --Increment this counter.
        gzTextVar.iTrapsDisarmed = gzTextVar.iTrapsDisarmed + 1
        LM_ExecuteScript(fnResolvePath() .. "800 Complete Trap Stat Change.lua")
    
    --Otherwise:
    else
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You all right, Mary?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's freezing in there. I just need to warm up a bit.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'The fire is really nice.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You warm up next to the fire. You feel much better." .. gsDE)
        fnDialogueFinale()
    end
end
    