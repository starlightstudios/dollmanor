-- |[Ice Trap Crystal]|
--When Mary finds the crystal in the ice trap, this plays. It is variable which room it takes place in, since the room the crystal
-- is in changes between games.

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Crystal Altar C" and gzTextVar.gzPlayer.sLocation ~= "Crystal Altar D" and gzTextVar.gzPlayer.sLocation ~= "Crystal Altar E") then return end

--Mark handling this.
gbHandledSubscript = true

--Repeat check.
if(gzTextVar.bFreezingTrapFoundCrystal == true) then return end

--Make sure the chamber has the crystal in it.
local iRoomIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
if(iRoomIndex == -1) then return end

--Check if at least one item is a crystal piece:
local bFoundCrystalPiece = false
for i = 1, gzTextVar.zRoomList[iRoomIndex].iObjectsTotal, 1 do
    if(gzTextVar.zRoomList[iRoomIndex].zObjects[i].sUniqueName == "crystal piece") then
        bFoundCrystalPiece = true
        break
    end
end
if(bFoundCrystalPiece == false) then return end

--Flag.
gzTextVar.bFreezingTrapFoundCrystal = true

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local iMaryCnt = 1
local saMaryList = {sMaryPath}

fnDialogueCutscene("You", iMaryCnt, saMaryList, "As you enter the small chamber, you see a pedestal before you. On it is an ornate jewelry box." .. gsDE)
fnDialogueCutscene("You", iMaryCnt, saMaryList, "The latch fights you as you open it, having been frozen over. You pull until the ice cracks and gives way." .. gsDE)
fnDialogueCutscene("You", iMaryCnt, saMaryList, "Inside is a piece of crystal. The room seems darker as you hold the crystal up to observe it." .. gsDE)
if(gzTextVar.bHasReadOrangeBook == true and gzTextVar.bHasSeenOrangeBookExtra == false) then
    gzTextVar.bHasSeenOrangeBookExtra = true
    fnDialogueCutscene("You", iMaryCnt, saMaryList, "The crystal reminds you of the storybook you read. The one about the Greek hero." .. gsDE)
end
fnDialogueCutscene("You", iMaryCnt, saMaryList, "You shiver. Whatever it is, you can look at it when you're not in a freezing cave. Time to head back." .. gsDE)
fnDialogueFinale()
