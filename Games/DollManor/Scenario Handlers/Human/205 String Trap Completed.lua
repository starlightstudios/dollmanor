-- |[String Trap Completion]|
--Did it!

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Crystal Altar F") then return end

--No duplicates.
if(gzTextVar.bStringTrapDisarmed == true) then return end

--Mark handling this.
gbHandledSubscript = true

--Flag.
gzTextVar.bStringTrapDisarmed = true

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local saMaryHumList = {"Root/Images/DollManor/Characters/Mary"}
        
--Dialogue.
fnDialogueCutscene("You", iMaryCnt, saMaryList, "As you enter the guarded room at the edge of the grand archives, you see an ornate jewel case in the middle of the room." .. gsDE)
fnDialogueCutscene("You", iMaryCnt, saMaryList, "Alert for additional traps, you check over it. You can't see anything attached or any sort of trick handle. You chance opening it." .. gsDE)
fnDialogueCutscene("You", iMaryCnt, saMaryList, "When you do, the latch opens and reveals a crystal piece, broken from a larger object. Nothing else happens. You sigh with relief." .. gsDE)
if(gzTextVar.bHasReadOrangeBook == true and gzTextVar.bHasSeenOrangeBookExtra == false) then
    gzTextVar.bHasSeenOrangeBookExtra = true
    fnDialogueCutscene("You", iMaryCnt, saMaryList, "The crystal reminds you of the storybook you read. Light seems to grow dim around it." .. gsDE)
end
fnDialogueCutscene("You", iMaryCnt, saMaryList, "Outside, you see the strings that dogged you suddenly visible. Whatever magic hid them has been disabled, and they slowly ascend into the ceiling." .. gsDE)

if(gzTextVar.iMaryStringTrapState > 0) then
    fnDialogueCutscene("You", iMaryCnt, saMaryList, "In the corner, folded neatly, are your normal clothes. With no one around, you quickly change." .. gsDE)
    if(gzTextVar.iMaryStringTrapState > 1) then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 0, 3, ciCodePlayer)
        fnDialogueCutscene("You", iMaryCnt, saMaryHumList, "With your old clothes returned, your normal skin returns with them. It wasn't awful to be plastic, but you're glad the magic wore off." .. gsDE)
    end
end
fnDialogueCutscene("You", iMaryCnt, saMaryHumList, "You can now safely check the archives, and tell Jessie and Lauren what happened. Not to mention this crystal has to be important..." .. gsDE)

--Un-dollify Mary.
gzTextVar.iMaryStringTrapState = 0
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
gzTextVar.gzPlayer.sLayer1 = "Null"
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
TL_SetProperty("Unregister Image")

--Upon completing the trap, increment this counter.
gzTextVar.iTrapsDisarmed = gzTextVar.iTrapsDisarmed + 1
LM_ExecuteScript(fnResolvePath() .. "800 Complete Trap Stat Change.lua")