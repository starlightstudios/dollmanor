-- |[Blue Light]|
--The three rooms with the blue light in them trigger these events.

--Player is in the "Unsettling Passage".
if(gzTextVar.gzPlayer.sLocation == "Unsettling Passage") then

    --Mark handling this.
    gbHandledSubscript = true

    --If the door got opened for any reason, Jessie closes it here.
    if(gzTextVar.bDisabledLight == false and gzTextVar.bCloseDoorBehind) then
        
        --Clear flag.
        gzTextVar.bCloseDoorBehind = false
        
        --Get the string past the first letter. This is the key state.
        local iAnteIndex = fnGetRoomIndex("Strange Antechamber")
        local sDoorState = string.sub(gzTextVar.zRoomList[iAnteIndex].sDoorW, 1, 1)
        if(sDoorState == "O") then
            TL_SetProperty("Append", "Jessie closes the door behind you." .. gsDE)
            LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Strange Antechamber", "C Normal", "W", ciModifyVisibility, 1)
        end
    end
    
    --Increment hypnosis.
    if(gzTextVar.bDisabledLight == false) then
        gzTextVar.iHypnoticCheck = gzTextVar.iHypnoticCheck + 1
    end

    --No hypnosis.
    if(gzTextVar.iHypnoticCheck < 1) then
        
    --Hypnosis is less than 3
    elseif(gzTextVar.iHypnoticCheck < 3) then
        TL_SetProperty("Append", "Your stomach feels a little upset, probably due to the strange light." .. gsDE)

    --3 to 7.
    elseif(gzTextVar.iHypnoticCheck < 7) then
        TL_SetProperty("Append", "You feel a queasy feeling. You think you should go west." .. gsDE)

    --7 to 10.
    elseif(gzTextVar.iHypnoticCheck < 10) then
        TL_SetProperty("Append", "You catch yourself drooling. The light feels so good. You should gaze into it." .. gsDE)

    --10 or higher. Mary is hypnotised.
    else
        TL_SetProperty("Append", "The light overwhelms you. You must GO TO IT. GO WEST" .. gsDE)
    end

--Player is in the "Hypnotic Passage".
elseif(gzTextVar.gzPlayer.sLocation == "Hypnotic Passage") then

    --Mark handling this.
    gbHandledSubscript = true

    --If the door got opened for any reason, Jessie closes it here.
    if(gzTextVar.bDisabledLight == false) then
        
        --Clear flag.
        gzTextVar.bCloseDoorBehind = false
        
        --Get the string past the first letter. This is the key state.
        local iAnteIndex = fnGetRoomIndex("Strange Antechamber")
        local sDoorState = string.sub(gzTextVar.zRoomList[iAnteIndex].sDoorW, 1, 1)
        if(sDoorState == "O") then
            TL_SetProperty("Append", "Jessie closes the door behind you." .. gsDE)
            LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Modify Door State.lua", "Strange Antechamber", "C Normal", "W", ciModifyVisibility, 1)
        end
    end

    --Increment hypnosis.
    if(gzTextVar.bDisabledLight == false) then
        gzTextVar.iHypnoticCheck = gzTextVar.iHypnoticCheck + 2
    end

    --No hypnosis.
    if(gzTextVar.iHypnoticCheck < 1) then
        
    --Hypnosis is less than 3
    elseif(gzTextVar.iHypnoticCheck < 3) then
        TL_SetProperty("Append", "Your stomach feels a little upset, probably due to the strange light." .. gsDE)

    --3 to 7.
    elseif(gzTextVar.iHypnoticCheck < 7) then
        TL_SetProperty("Append", "You feel a queasy feeling. You think you should go west." .. gsDE)

    --7 to 10.
    elseif(gzTextVar.iHypnoticCheck < 10) then
        TL_SetProperty("Append", "You catch yourself drooling. The light feels so good. You should gaze into it. You should go west." .. gsDE)

    --10 or higher. Mary is hypnotised.
    else
        TL_SetProperty("Append", "The light overwhelms you. You must GO TO IT. GO WEST." .. gsDE)
    end

--Player is in the "Stone Gallery".
elseif(gzTextVar.gzPlayer.sLocation == "Stone Gallery") then

    --Mark handling this.
    gbHandledSubscript = true

    --Increment hypnosis.
    if(gzTextVar.bDisabledLight == false) then
        gzTextVar.iHypnoticCheck = gzTextVar.iHypnoticCheck + 3
    end

    --No hypnosis.
    if(gzTextVar.iHypnoticCheck < 1) then
        
    --Hypnosis is less than 3
    elseif(gzTextVar.iHypnoticCheck < 3) then
        TL_SetProperty("Append", "You are bathed in the blue light. You should stay here and stare into it." .. gsDE)

    --3 to 7.
    elseif(gzTextVar.iHypnoticCheck < 7) then
        TL_SetProperty("Append", "You are bathed in the blue light. You should stay here and stare into it." .. gsDE)

    --7 to 10.
    elseif(gzTextVar.iHypnoticCheck < 10) then
        TL_SetProperty("Append", "You are bathed in the blue light. You should stay here and stare into it." .. gsDE)

    --10 or higher. Mary is hypnotised.
    elseif(gzTextVar.iHypnoticCheck < 20) then
        TL_SetProperty("Append", "The light overwhelms your mind. STAND HERE. STAY HERE. BE CHANGED. You obey." .. gsDE)
    
    --At 20, the transformation begins.
    else
    
        --Transformation sequence.
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryStone"
        gzTextVar.gzPlayer.sFormState = "Statue"
    
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "You mindlessly gaze into the blue light. It feels so wonderful. You never want to leave." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You feel something in the back of your mind. Something is being taken away. That's fine. You LOOK INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You ALLOW THE LIGHT TO FILL YOU. You smile. You LOVE THE LIGHT. You ARE THE LIGHT. You agree. THE LIGHT WILL TAKE YOU." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "YOU ARE THE LIGHT, THE LIGHT IS YOU. YOU HAVE NO THOUGHTS, THE LIGHT IS YOUR THOUGHTS. MARY IS GONE. THERE IS NO MARY. THERE IS THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryStoneTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "Something feels odd. You smile. You seem to be changing. It feels strange, hard, rigid." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "YOU DO NOT MOVE. YOU DO NOT THINK. THIS IS CORRECT. You agree. This is correct. There is no you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryStoneTF1", ciImageLayerDefault)
        TL_SetProperty("Append", "Your body hardens. You feel your skin become rock. Your muscles and bones follow, then your organs." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryStoneTF2", ciImageLayerDefault)
        TL_SetProperty("Append", "The sensation overtakes you. The last vestiges of flesh are gone. There is only stone." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 3, 3, ciCodePlayer)
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "You have changed. You are a mere statue. The statues along the side of the room call to you. You should take your place." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "FIND THE INTRUDERS. You are not to take your place yet. You must find the intruders." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/MaryStone"
        TL_SetProperty("Append", "BRING THEM HERE. You will bring them here. They will resist. You will BRING THEM HERE, no matter what." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        --Flags.
        gzTextVar.bIsJessieFollowing = false
        gzTextVar.bIsLaurenFollowing = false
        
        --Unlock all doors.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
    
    end
end