-- |[Trigger Rubber Trap]|
--This occurs when the player moves either onto the same tile as a rubber goop, or tries to cross a rubber goop.

--Mary is not affected yet:
if(gzTextVar.iGoopTFStage == 0) then
    
    TL_SetProperty("Append", "As you creep through the darkened brewery, you realize you've stepped in something that wasn't there a moment ago." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryRubberTrap0", ciImageLayerDefault)
    TL_SetProperty("Append", "You glance down and realize that the - whatever it is you stepped in - stuck your shoes to the floor. You stepped out of them and cannot see them in the darkness." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Worse, the soft rubbery material has spread up your legs. You didn't feel a thing, but now that you think about it, the rubber feels somewhat pleasant. Like a pair of firm socks." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It seems to have stopped spreading where it is and gone inert. Hopefully you can find a way to remove it when you escape - and find a spare pair of shoes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You chance to reach down in the gloom of the brewery, hoping to find your lost shoes. It is then that you realize the goop hit your hand somehow, too. It has gone numb." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It is now an almost comically oversided, non-functional rubbery hand. Something like you might see on an inflatable pool toy, but very real and very much your hand." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You'd better escape and not step in any more of this goop..." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Unregister Image")
    
    --Query sprite becomes the goth outfit.
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryRubberTrap0"
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
    
    --Flag.
    gzTextVar.iGoopTFStage = 1
    
--Mary was gooped once.
elseif(gzTextVar.iGoopTFStage == 1) then
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryRubberTrap1", ciImageLayerDefault)
    TL_SetProperty("Append", "Not watching your step, you hit another puddle of the goop. This time, it is far more aggressive! The rubber that was already on you surges and boils, claiming as much of your body as it can!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You prepare for the worst as it surges over your face. You want to reach up and pull it off, but you realize both your arms are covered. Your hands are now useless parodies of themselves." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You're only able to see out of one eye, but the rubber stops before claiming the rest of your face. You look down. Only your midsection isn't covered, but the goop is inert again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The rubber shines despite the darkness. It's pleasant, very flexible and warm. It feels good. Maybe stepping in another puddle - " .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "No, absolutely not. You need to get out here, and watch your step very carefully." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Query sprite becomes the goth outfit.
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryRubberTrap1"
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerSkin)
    TL_SetProperty("Unregister Image")
    
    --Flag.
    gzTextVar.iGoopTFStage = 2

--Mary was already gooped. Bad end.
elseif(gzTextVar.iGoopTFStage == 2) then
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryRubberTrap1", ciImageLayerDefault)
    TL_SetProperty("Append", "Your goopy foot hits another puddle of the rubbery goo. You're filled with dread. But the goo does not move." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryRubberTrap2", ciImageLayerDefault)
    TL_SetProperty("Append", "You begin to feel calm. Accepting. It feels incredible. This is what the goo was trying to show you, but you were resisting it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The rubber just wants to seal you in and make you rubber, like it. It has already begun changing your body. You think to your legs and realize they're thick, solid, boneless. As are your arms." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It's wonderful, so wonderful. You love the rubber and can't wait for it to claim the rest of you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 1, 5, ciCodePlayer)
    TL_SetProperty("Register Image", "Mary", "Root/Images/DollManor/Characters/MaryRubberTrap3", ciImageLayerDefault)
    TL_SetProperty("Append", "Sensing your acceptance, the rubber encases you. Your vision goes dark, only to return. You realize your mouth is now a glassy permanent painted on grin, from ear to ear." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You can't feel your face, your arms, your legs, anything. But you do feel good, so good. Your whole body is goopy flexible rubber. The smile etches itself into your soul. You're so happy!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Why did you fight, why did you resist? This is the greatest joy you've ever felt! You love being rubber, and you're sure your friends will, too!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Yes. Your friends. Your thoughts are growing simpler, happier, more obedient. Your name is Mary, you are rubber, and you want to make Jessie and Lauren rubber." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There was a creator, a woman who made you this way. You want to thank her. You want to help her bring the joy of goopy rubber to everyone in the world." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "But for now, you'll need to bring the joy to the two humans who are waiting nearby. You can't wait to smear a permanent grin on their faces!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flag.
    gzTextVar.iGoopTFStage = 3
    gzTextVar.gzPlayer.sFormState = "Rubber"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryRubberTrap3"
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
    
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_Squeak)

end
