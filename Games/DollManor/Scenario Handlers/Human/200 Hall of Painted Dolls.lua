-- |[Hall of Painted Dolls]|
--Right in front of where Pygmalie flees to at the start of phase 2. A journal page is here.

--Make sure this is the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Hall of Painted Dolls") then return end

--Mark handling this.
gbHandledSubscript = true
        
--Cutscene only runs once.
if(gzTextVar.bHasSeenPaintedDollDialogue ~= false) then return end
gzTextVar.bHasSeenPaintedDollDialogue = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

--Dialogue.
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Everyone, shhhh' Jessie whispers, holding up a hand." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What is it?' you whisper." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'She's in there. Past that door. Hear her?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You can hear someone in the room beyond, talking to herself. " .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'These dolls are creepy' Lauren says in a hushed tone." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren, open the door a crack. Take a peek.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'O-okay...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Lauren opens the door while you and Jessie stand watch. The dolls on the edge of the room seem inert. For now." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "He shuts the door without making a sound, and turns to you. What he saw has clearly affected him." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'H-hundreds... Too many...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hundreds? Of doll girls?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren nods." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What else did you see?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Candles, a big altar, weird symbols written all over it...'" .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Maybe we should go back, Mary. I don't think we can take on a hundred at once.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And they're not attacking her?'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren shakes his head. 'They're helping her. She's rearranging things in there and they're carrying candles and incense around.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hmmm...'" .. gsDE)


if(gzTextVar.sManorType == "Demo" or gzTextVar.sManorType == "Simple") then
    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Looks like there's another journal page here. Maybe it will offer some clues." .. gsDE)
elseif(gzTextVar.sManorType == "Normal") then
    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Someone discarded a key here. Maybe it opens something important." .. gsDE)
end
fnDialogueFinale()
    