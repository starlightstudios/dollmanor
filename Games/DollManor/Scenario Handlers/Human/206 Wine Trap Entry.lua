-- |[Wine Trap Entry]|
--Wine Trap entry, friends stay there as usual.

-- |[Entering Brewery, all cases]|
if(gzTextVar.gzPlayer.sLocation == "Brewery West W") then
    gzTextVar.bBreweryNoFollow = true
end

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Winery E") then return end

--Mark handling this.
gbHandledSubscript = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}

-- |[Entering the Antechamber]|
if(gzTextVar.gzPlayer.sPrevLocation ~= "Brewery West W" and gzTextVar.gzPlayer.sPrevLocation ~= "Null") then
    
    --First time.
    if(gzTextVar.bBreweryAntechamberScene == false) then
        
        --Flag.
        gzTextVar.bBreweryAntechamberScene = true
        
        --First pass.
        if(gzTextVar.bAtLeastOneTrapSeen == false) then
            gzTextVar.bAtLeastOneTrapSeen = true
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, wait. There's something weird.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Huh? Did you hear something?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'No. Don't you feel it? Your stomach turning over?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It makes me feel sick.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah, I guess I do feel something. It's dangerous.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What is?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know, but all of us suddenly feeling nervous for no reason? Not a coincidence.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'We should turn back.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I smell a trap.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you going to keep going, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll take a look. Keep Lauren safe. I'll come straight back, won't be a minute.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Close the door behind me, and don't make a sound.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Right. Don't take any risks.'" .. gsDE)
            fnDialogueFinale()
        
        --Repeats.
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sinking feeling, growing sense of dread, nameless fear?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah. You're going ahead?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's what I do. Stay here and don't make a sound.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Repeats.
    else
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Going to take another try, Mary?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I didn't see anything the first time, but there's got to be something in there.'" .. gsDE)
        fnDialogueFinale()
    end

-- |[Exiting the Trap]|
elseif(gzTextVar.gzPlayer.sPrevLocation == "Brewery West W") then

    --Follow.
    gzTextVar.bBreweryNoFollow = false

    --Armed it and escaped.
    if(gzTextVar.bGoopTrapActivated == true) then
        
        --Cutscene.
        if(gzTextVar.bGoopTrapEndCutscene == false) then
            gzTextVar.bGoopTrapEndCutscene = true
            
            --Despawn all the goops.
            local bDespawned = true
            while(bDespawned == true) do
                bDespawned = false
                for i = 1, gzTextVar.zEntitiesTotal, 1 do
                    if(string.sub(gzTextVar.zEntities[i].sDisplayName, 1, 4) == "Goop") then
                        bDespawned = true
                        fnRemoveEntity(i)
                        break
                    end
                end
            end
            
            --Increment this.
            gzTextVar.iTrapsDisarmed = gzTextVar.iTrapsDisarmed + 1
            LM_ExecuteScript(fnResolvePath() .. "800 Complete Trap Stat Change.lua")
            
            --If you got goop'd.
            if(gzTextVar.iGoopTFStage > 0) then
                gzTextVar.iGoopTFStage = 0
                gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
                gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
                gzTextVar.gzPlayer.sLayer1 = "Null"
                TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
                saMaryList = {"Root/Images/DollManor/Characters/Mary"}
                
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "As soon as you exit the brewery's shadow, the rubber slides off you and retreats into the blackness." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Your friends don't notice it happen, but you're relieved. Oddly, your shoes were placed neatly next to the door..." .. gsDE)
            end
            
            --First time.
            if(gzTextVar.bAtLeastOneTrapHandled == false) then
                gzTextVar.bAtLeastOneTrapHandled = true
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? What happened?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I really don't want to talk about it.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you sure?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I am so absolutely sure. Let's just go.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Whatever you did... good job?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks.'" .. gsDE)
                fnDialogueFinale()
            
            --Repeats.
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? What happened?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I really don't want to talk about it.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you sure?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I am so absolutely sure. Let's just go.'" .. gsDE)
                fnDialogueFinale()
            end
        end
        return
    end
    
    --Exiting, didn't activate trap.
    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What's in there?'" .. gsDE)
    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'A bunch of brewing vats and barrels. Nothing dangerous.'" .. gsDE)
    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Just that? No scary doll girls?'" .. gsDE)
    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No, and I didn't like it. At all.'" .. gsDE)
    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's creepy.'" .. gsDE)
    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You're sure we have to do that way?'" .. gsDE)
    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Eventually, but not right now.'" .. gsDE)
    fnDialogueFinale()

end
    