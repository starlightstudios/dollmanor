-- |[String Trap Standard Examine]|
--All of the Grand Archives rooms use this as a special examination. The arguments received indicate how the 
-- engine interperets the grooves and string visibility.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sOrigString = LM_GetScriptArgument(0)
local sGrooveDir = LM_GetScriptArgument(1)
local iRoomIndex = tonumber(LM_GetScriptArgument(2))

--Other variables.
local bIsSpecialLook = false
local sLookDir = "Null"
local bUseDir = false

-- |[Special: Look Floor]|
--Look at the floor to try to spot the grooves.
if(sOrigString == "look floor" or sOrigString == "look down") then
    
    --Flag input handled.
    gbHandledInput = true
    
    --Can't see any grooves.
    if(sGrooveDir == "null") then
        TL_SetProperty("Append", "You look at the floor of the grand archives. There is nothing unusual about it here." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Can see grooves.
    else
        TL_SetProperty("Append", "You look at the floor of the grand archives. You see a curious groove running " .. sGrooveDir .. "." .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
    return

-- |[Special: Try to spot Strings]|
elseif(sOrigString == "look w" or sOrigString == "look west") then
    gbHandledInput = true
    bIsSpecialLook = true
    sLookDir = "west"
    bUseDir = gzTextVar.bStringTrapActiveW
elseif(sOrigString == "look n" or sOrigString == "look north") then
    gbHandledInput = true
    bIsSpecialLook = true
    sLookDir = "north"
    bUseDir = gzTextVar.bStringTrapActiveN
elseif(sOrigString == "look e" or sOrigString == "look east") then
    gbHandledInput = true
    bIsSpecialLook = true
    sLookDir = "east"
    bUseDir = gzTextVar.bStringTrapActiveE
elseif(sOrigString == "look s" or sOrigString == "look south") then
    gbHandledInput = true
    bIsSpecialLook = true
    sLookDir = "south"
    bUseDir = gzTextVar.bStringTrapActiveS
end

-- |[Trying to spot Strings]|
if(bIsSpecialLook) then
    TL_SetProperty("Append", "You look carefully to the " .. sLookDir .. "..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Gauge success.
    if(gzTextVar.iGrandArchiveSpotThreshold < gzTextVar.iaGrandArchiveRolls[iRoomIndex]) then
        if(bUseDir) then
            TL_SetProperty("Append", "You see a faint glimmer of light. Something is hanging from the ceiling..." .. gsDE)
            TL_SetProperty("Create Blocker")
            gzTextVar.bStringTrapSawStrings = true
        else
            TL_SetProperty("Append", "You're absolutely certain there is nothing suspicious there." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    else
        TL_SetProperty("Append", "You can't see anything suspicious, but can't be certain either way..." .. gsDE)
        TL_SetProperty("Create Blocker")
    end
end