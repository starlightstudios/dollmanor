-- |[ ================================= Turn Post Exec: Statue ================================= ]|
--Handles post-turn cutscenes if the player is a statue.

--Setup.
local sBasePath = fnResolvePath()

--"Strange Antechamber" cutscene.
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 Strange Antechamber.lua")
if(gbHandledSubscript == true) then return end

--"Stone Gallery" petrifying scenes, bad end.
LM_ExecuteScript(sBasePath .. "201 Stone Gallery.lua")
if(gbHandledSubscript == true) then return end
