-- |[Stone Gallery]|
--Bad End.

--Check location.
if(gzTextVar.gzPlayer.sLocation ~= "Stone Gallery") then return end

--Mark handling this.
gbHandledSubscript = true

--Setup.
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex

--Is Jessie nearby and crippled? If so, she becomes a statue.
if(gzTextVar.zEntities[j].bIsCrippled) then

    --Un-cripple her. Change her status to stone.
    gzTextVar.zEntities[j].bIsCrippled = false
    gzTextVar.zEntities[j].sQueryPicture = "Root/Images/DollManor/Characters/JessieStone"
    gzTextVar.zEntities[j].sState = "Statue"
        
    --Achievement
    Steam_UnlockAchievement(gciAchievement_NoFightIt)

    --TF sequence.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
    TL_SetProperty("Append", "You stand in the gallery of statues, dragging the wounded form of Jessie behind you. You are unsure of what to do." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "SHE MUST GAZE INTO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You must force the intruder to gaze into the light. You stand her up." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
    TL_SetProperty("Append", "The intruder fights back. Your hold her. She is too hurt to resist your powerful stone hands." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "SHE MUST GAZE INTO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hold the intruder. She looks into the light. You hold the intruder. She looks into the light." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Slowly, she fights you less. She is almost not resisting." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "SHE MUST GAZE INTO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieStoneTF0", ciImageLayerDefault)
    TL_SetProperty("Append", "The intruder has gazed into the light. She begins changing. She becomes rigid. She becomes hardened. She becomes stone." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is barely fighting you. She is no longer an intruder. " .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iJessieIndex, 4, 3, ciCodeFriendly)
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieStoneTF1", ciImageLayerDefault)
    TL_SetProperty("Append", "The flesh vanishes, replaced by stone. She is a statue. You are a statue. You, and she, will obey." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieStone", ciImageLayerDefault)
    TL_SetProperty("Append", "The light floods into her. It floods into you. TAKE YOUR PLACE, it commands her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The statue moves quietly to the side of the room, standing among the gallery. WAIT. She waits." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --If Lauren is not already a statue:
    if(gzTextVar.zEntities[l].sState ~= "Statue") then
        TL_SetProperty("Append", "FIND THE INTRUDER. You must find the intruder. OBEY. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Lauren is a statue already. Everyone is statues.
    else
        TL_SetProperty("Append", "TAKE YOUR PLACE. You move to the side of the room, standing between the statues you helped create." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "WAIT. You wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "And wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "And wait..." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean up.
    TL_SetProperty("Unregister Image")

end

--Is Lauren nearby and crippled? If so, he becomes a statue.
if(gzTextVar.zEntities[l].bIsCrippled) then

    --Un-cripple him. Change his status to stone.
    gzTextVar.zEntities[l].bIsCrippled = false
    gzTextVar.zEntities[l].sQueryPicture = "Root/Images/DollManor/Characters/LaurenStone"
    gzTextVar.zEntities[l].sState = "Statue"
        
    --Achievement
    Steam_UnlockAchievement(gciAchievement_ItDoesntHurt)

    --TF sequence.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
    TL_SetProperty("Append", "You stand in the gallery of statues, dragging the wounded form of Lauren behind you. You have no instructions. You wait." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "HE MUST GAZE INTO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You must force the intruder to gaze into the light. You stand him up. He prickles at your touch." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenGeneralTF0", ciImageLayerDefault)
    TL_SetProperty("Append", "The intruder does not resist. He is still hurt. He is crying. You hold him firm and force him to stare into the light." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "HE MUST GAZE INTO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hold the intruder. He looks into the light. You hold the intruder. He looks into the light." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "His mind grows distant. His eyes grow dim. He gazes into the light." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "HE MUST GAZE INTO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenStoneTF0", ciImageLayerDefault)
    TL_SetProperty("Append", "The intruder has gazed into the light. He begins changing. His skin becomes stone. His muscles become stone. His mind becomes stone. His mind becomes empty." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "He quietly allows the changes. He is no longer crying. His eyes have hardened. He is not an intruder." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iLaurenIndex, 5, 3, ciCodeFriendly)
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenStoneTF1", ciImageLayerDefault)
    TL_SetProperty("Append", "The intruder is emptied. The intruder is stone. He is a statue. You are a statue. He will obey the light." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenStone", ciImageLayerDefault)
    TL_SetProperty("Append", "The light floods into him. It floods into you. TAKE YOUR PLACE, it commands him." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The statue moves quietly to the side of the room, standing among the gallery. WAIT. He waits." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --If Jessie is not already a statue:
    if(gzTextVar.zEntities[j].sState ~= "Statue") then
        TL_SetProperty("Append", "FIND THE INTRUDER. You must find the intruder. OBEY. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Jessie is a statue already. Everyone is statues.
    else
        TL_SetProperty("Append", "TAKE YOUR PLACE. You move to the side of the room, standing between the statues you helped create." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "WAIT. You wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "And wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "And wait..." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean up.
    TL_SetProperty("Unregister Image")

end

--Unflag.
gzTextVar.bForceMoveStatue = false
