-- |[Strange Antechamber]|
--After being petrified, the player enters the Strange Antechamber and Jessie and Lauren flee.

if(gzTextVar.gzPlayer.sLocation == "Strange Antechamber" and gzTextVar.gzPlayer.sPrevLocation == "Unsettling Passage") then

    --Don't repeat if we've seen the dialogue.
    if(gzTextVar.bHasSeenHallwayStatueDialogue == true) then return end

    --Mark handling this.
    gbHandledSubscript = true
    
    --Indices.
    local l = gzTextVar.iLaurenIndex
    local j = gzTextVar.iJessieIndex
    
    --Setup.
    local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
    local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
    local sJessPath = gzTextVar.zEntities[j].sQueryPicture
    local iLaurCnt = 1
    local saLaurList = {sLaurPath}
    local iMaryCnt = 1
    local saMaryList = {sMaryPath}
    local iJessCnt = 1
    local saJessList = {sJessPath}
    
    --Normal statue:
    if(gzTextVar.bSecretStatue == false) then
    
        --Flag.
        gzTextVar.bHasSeenHallwayStatueDialogue = true
        
        --Dialogue.
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'M-M-M-Mary? Is that you?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'What happened? Can you hear me?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You have FOUND THE INTRUDERS. You will BRING THEM TO THE LIGHT." .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You wordlessly grasp towards the intruders. You must bring them to the light." .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "The spry one kicks at you. She is strong. You are pushed away." .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Lauren! Run!'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'B-b-b-but...'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'She's not herself! We can't help her if she gets us! Run!'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You attempt to catch the smaller intruder. You will BRING THEM TO THE LIGHT." .. gsDE)
    
        --Flee Jessie.
        local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
        if(sLocation ~= "Null") then
            TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
        end
        
        --Flee Lauren.
        sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
        if(sLocation ~= "Null") then
            TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
        end
        
        --Resume cutscene.
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "But, the two intruders flee. You MUST PURSUE THEM. You must CATCH THEM. You will BRING THEM TO THE LIGHT." .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You are a statue. You do not think. You OBEY THE LIGHT." .. gsDE)
        fnDialogueFinale()
        
        --Unset flags.
        gzTextVar.bIsJessieFollowing = false
        gzTextVar.bIsLaurenFollowing = false

    --Secret statue:
    else
    
        --Flag.
        gzTextVar.bHasSeenHallwayStatueDialogue = true
        gzTextVar.bForceMoveStatue = true
        
        --Dialogue.
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'So, what happened in there?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The intruders believe you are not a statue. You will BRING THEM TO THE LIGHT." .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I found something you need to see. Follow me.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you sure?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You should follow me. It will be good for you.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary's acting a little odd...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I am not acting a little odd. I want you to see this. Follow me.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You're just being a scaredy cat. I'm sure it's fine.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mmmm... okay...'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The intruders believe you are not a statue. You will bring them to the light." .. gsDE)
        fnDialogueFinale()
        
        --Glags.
        gzTextVar.bIsJessieFollowing = true
        gzTextVar.bIsLaurenFollowing = true

    end
end
