-- |[ ================================== Turn Post Exec: Clay ================================== ]|
--Post-turn exec when a claygirl.

--Setup.
local sBasePath = fnResolvePath()

--Secret claygirl!
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 Clay Your Friends.lua")
if(gbHandledSubscript == true) then return end

--Finale.
LM_ExecuteScript(sBasePath .. "201 Finale.lua")
if(gbHandledSubscript == true) then return end
