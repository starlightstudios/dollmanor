-- |[ ================================== Clay House Entrance =================================== ]|
--Nobody suspects a thing.

--Make sure we're in the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Curious Building Entrance") then return end

--Repeat check.
if(gzTextVar.bClayedFriends == true) then return end
if(gzTextVar.bIsClaySequence == false) then return end
gzTextVar.bClayedFriends = true

--Friends follow you now.
gzTextVar.bClayNoFollow = false

--Mark handling this.
gbHandledSubscript = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local saMClyList = {"Root/Images/DollManor/Characters/ClayGirlBlue"}
local saJClyList = {"Root/Images/DollManor/Characters/ClayGirlRed"}
local saLClyList = {"Root/Images/DollManor/Characters/ClayGirlYellow"}
        
--Achievement
Steam_UnlockAchievement(gciAchievement_NoFightIt)
Steam_UnlockAchievement(gciAchievement_ItDoesntHurt)

fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'Mary? What happened?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "You try to smile coyly, but it just twists your face. Fortunately, the rain is disguising your dripping clay body." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,     "'Are you okay?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "With great difficulty, you nod." .. gsDE)
fnDialogueCutscene("Jessie", iJessCnt, saJessList,     "'I think she's hurt, Lauren.'" .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'Do you need help?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "You walk slowly towards Jessie. She rushes forth, ready to look over you." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMaryList,     "She stops and takes your hand. You keep walking." .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 4, ciCodePlayer)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You fall onto her, covering her. She doesn't even have time to scream before she is covered. Your form dissociates into formless clay." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You cover her totally, mixing your clay with her. She thrashes but you completely surprised her." .. gsDE)
fnDialogueCutscene("You",    iJessCnt, saJClyList,     "She realizes the truth when your clay penetrates her head. She stops fighting and begins allowing the clay to do its work." .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, j, 7, 4, ciCodeUnfriendly)
fnDialogueCutscene("You",    iJessCnt, saJClyList,     "Jessie always was quick-witted. You're glad she realized the joy of being clay. The two of you unmix and stand up." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'Mary? Jessie?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You really, really don't like having to betray Lauren. You feel a little guilty, but there was no way to communicate how it feels." .. gsDE)
fnDialogueCutscene("You",    iJessCnt, saJClyList,     "Jessie places her hand on your body. You can feel her thoughts running along it to yours." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You 'kneel' in front of Lauren and offer a hand." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'D-does it hurt?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You shake your head." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'And you're still... Mary?'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You nod enthusiastically. Jessie tries to give a clay thumb's up, but she is unpracticed in staying firm." .. gsDE)
fnDialogueCutscene("Lauren", iLaurCnt, saLaurList,     "'Well... Okay...'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You slide over to Lauren and cover him. He fights back tears and does not make a peep." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You cover him with clay and work it into and through him. He helps spread it, as does Jessie." .. gsDE)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, l, 8, 4, ciCodeFriendly)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "His skin transforms into a lovely yellow clay. Your clay sister unmixes from you. She feels her new clay body a bit, experimenting with it." .. gsDE)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "When you were connected, the feeling of joy emanated from deep within Lauren. She is happy with her new form." .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You place a clay hand on your friends, allowing you to join your thoughts together." .. gsDE)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "'We can speak when we're connected?'" .. gsDE)
fnDialogueCutscene("You",    iJessCnt, saJClyList,     "'Thank you, Mary.'" .. gsDE)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "'Yeah! Thanks, sis!'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "'Sorry about deceiving you, but it worked out in the end.'" .. gsDE)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "'So what now?'" .. gsDE)
fnDialogueCutscene("You",    iJessCnt, saJClyList,     "'We go show the creator how we turned out.'" .. gsDE)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "'Really? I kind of want to practice transforming.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "'Later. The creator is everything, she must see us.'" .. gsDE)
fnDialogueCutscene("You",    iLaurCnt, saLClyList,     "'Okay, okay. Let's go.'" .. gsDE)
fnDialogueCutscene("You",    iMaryCnt, saMClyList,     "You'll need to go show your wonderful new body to your creator. She is on the second floor of the western building." .. gsDE)

--Change display properties.
gzTextVar.zEntities[j].sQueryPicture = "Root/Images/DollManor/Characters/ClayGirlRed"
gzTextVar.zEntities[j].sState = "Claygirl"
gzTextVar.zEntities[l].sQueryPicture = "Root/Images/DollManor/Characters/ClayGirlYellow"
gzTextVar.zEntities[l].sState = "Claygirl"
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/ClayGirlBlue"
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)