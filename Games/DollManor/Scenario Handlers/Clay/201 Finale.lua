-- |[Finale]|
--Clay finale.

--Make sure this is the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Ritual Altar") then return end

--Both friends must be clay.
local bIsJessieClay = false
local bIsLaurenClay = false
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Claygirl") then bIsJessieClay = true end
if(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Claygirl") then bIsLaurenClay = true end
if(bIsJessieClay == false or bIsLaurenClay == false) then return end

--Mark handling this.
gbHandledSubscript = true
gzTextVar.bClayTrapEnding = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local p = gzTextVar.iPygmalieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = gzTextVar.zEntities[p].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Dialogue.
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Ha ha ha ha! Splendid thrashing, good spliffle!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You and your clay sisters 'kneel' before your creator." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh how wonderfully you turned out. I've had a 'soft' spot for clay for a long time! Wah ha ha ha ha!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You put a hand to your face, instinctively stifling a laugh." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'But the task remains, tickle tickle. Tickle it out.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Your brain is far more plastic now that it's made of clay. You feel something inside your head molding it. Folding it. Squeezing and squishing it." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "It forms back to what it was before, but changed. You're not sure how it's different." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Lovely lovely lovely. Well, dinner time. Have fun, fun!'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh, Lauren. Come here.'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "Lauren eagerly slithers over to your creator. You feel thoughts flowing around the room, which eventually settle into her head." .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "Lauren crouches and folds, shaping herself into a chair. Your creator sits daintily atop it." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Very well done, young one. You show much promise! Keep practicing and you'll be the finest of my menagerie!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Pride boils up in your smoothed clay mind. Lauren is very skilled at transforming herself, and you brought her to your creator! You've done so well!" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "Jessie touches a hand to you. 'Don't go overboard. Come on, let's go see if any of the library dolls want to play.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You nod and slither into the manor with Jessie..." .. gsDE)
fnDialogueFinale()
    