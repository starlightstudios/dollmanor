-- |[Goop your Friends]|
--Turns Jessie and Lauren into rubber clones.

--Make sure this is the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Winery E") then return end

--Mark handling this.
gbHandledSubscript = true
        
--Cutscene only runs once.
if(gzTextVar.bGoopedFriends ~= false) then return end
gzTextVar.bGoopedFriends = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
        
--Flag.
TL_SetProperty("Begin Major Dialogue")
        
--Achievement
Steam_UnlockAchievement(gciAchievement_NoFightIt)
Steam_UnlockAchievement(gciAchievement_ItDoesntHurt)

--Dialogue.
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Characters/MaryRubberFinale") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Jessie calls to you as you approach. 'Mary, what happened? Are you all right?'[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "What a silly human she is. You can't speak, your mouth is simply painted on! The dim light hides your true nature just long enough.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "You plant a hand firmly on Lauren's head, and another on Jessie's back. The shine on your blank, empty eyes leads Jessie to a horrible realization.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "'Mary, fight it! Don't do this!' Jessie shouts. Fight what? Fight being rubber? You've never felt better. How silly these humans are![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lauren is far more accepting, eager, even. He kneels on the floor and allows the rubber to flow from you over him. Changing him.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Jessie's vain attempt to run fails as your rubber sticks you together. She tugs at the goop but only succeeds in changing herself further.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Lauren's blank smile copies yours. She is now Mary, identical to you in every way, rubber inside and out. She stands.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Your clone Mary places her hands on Jessie's back, spreading the rubber. Jessie gives one last push. Then, she is gone.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The third Mary smiles ear to ear as an identical rubber clone of you. Or were you the clone? It's hard to tell.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "It doesn't matter. The three of you are so happy, why does it matter who you were?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "The only thing left is to present yourselves to your creator. You hope she likes how you turned out, you can't wait to show her!") ]])
fnCutsceneBlocker()

--Flags.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, l, 1, 5, ciCodeUnfriendly)
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, j, 1, 5, ciCodeFriendly)
gzTextVar.bBreweryNoFollow = false
gzTextVar.zEntities[j].sQueryPicture = "Root/Images/DollManor/Characters/MaryRubberTrap3"
gzTextVar.zEntities[j].sState = "Rubber"
gzTextVar.zEntities[l].sQueryPicture = "Root/Images/DollManor/Characters/MaryRubberTrap3"
gzTextVar.zEntities[l].sState = "Rubber"
    