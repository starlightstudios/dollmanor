-- |[Finale]|
--Rubber goop finale.

--Make sure this is the right room.
if(gzTextVar.gzPlayer.sLocation ~= "Ritual Altar") then return end

--Mark handling this.
gbHandledSubscript = true
gzTextVar.bGoopTrapEnding = true

--Setup.
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
local p = gzTextVar.iPygmalieIndex
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = gzTextVar.zEntities[p].sQueryPicture
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Dialogue.
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh my oh my!' your creator exclaims as you approach with your rubbery twins in tow." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'You turned out splendidly. Shall I have you work as a servant, or perhaps an entertainer? Or maybe just a toy for the dolls! Whehehe!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You smile blankly, unable to speak. You don't even need to speak. You're so happy your creator likes how you look." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh oh, check the circle. Yes yes. Kneel.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The three of you kneel. Your rubbery legs squeak faintly as they stretch and compress." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You feel several things in your empty head. Something is pushing and pulling. You remain empty and continue to smile." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'All done! Well, I suppose you'll make a good toy. Play with the dollies in the chapel, keep them company, won't you?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You stand eagerly. Your body moves on its own. Your clones stay with your creator, waiting for her to give them orders. You hope they have fun!" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You squeak and creak as your rubbery body finds its way to the chapel. The dolls there immediately want to play checkers. You oblige, smiling glassily at them." .. gsDE)
fnDialogueFinale()
    