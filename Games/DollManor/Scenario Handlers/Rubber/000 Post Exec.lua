-- |[ ================================= Turn Post Exec: Rubber ================================= ]|
--Handles post-turn cutscenes if the player is a rubbery clone.

--Setup.
local sBasePath = fnResolvePath()

--Turn Jessie and Lauren to goop!
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 Goop Your Friends.lua")
if(gbHandledSubscript == true) then return end

--Finale.
LM_ExecuteScript(sBasePath .. "201 Finale.lua")
if(gbHandledSubscript == true) then return end
