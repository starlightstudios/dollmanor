-- |[ ============================ Special Input Handler: Betrayal ============================= ]|
--Used during the betrayal ending. Gets re-routed to from the normal input handler.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

-- |[Submit]|
if(sString == "submit") then
    TL_SetProperty("Append", "'Creator?'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Dolly, you know that good dollies do not speak until they are spoken to. Aren't you a good dolly?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Yes, Creator. I am a good dolly. Forever. But, I have something important to tell you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Important? Important. It has something important. Talk talk, dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'I have retained my intelligence, and my willpower. I come to you with my friends as dolls willingly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I want to continue to be myself, to have my memories, and to be your obedient doll. Will you allow me?'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Yes, creator?'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I will take from you only what I need. Prove your loyalty. Give me your total obedience.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Yes, creator.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You kneel before your creator. She places her hand on your head. You feel her picking through your mind. You lay it open for her, you do not resist." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She searches, and searches, but does not find anything that upsets her. You were already loyal. She pats you on the head, and you stand." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'You are a very good doll, Mary. Very good, very good.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'You will not age, or falter. You will take my place when I do.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Yes, creator.'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Now, help me dress your friends. I'm sure you can't wait to play with them! Wah ha ha!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Yes, creator. I know they cannot wait, either.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gzTextVar.bSpecialEndingBetraySubmit = true
    gzTextVar.bExternalEndingCall = true
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
    
    return
end

-- |[Betray]|
if(sString == "betray") then
    TL_SetProperty("Append", "As your creator busies herself with your sisters, you maneuver behind her." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Dolly, be a dear and fetch - '" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "You smash the back of her head as hard as you can. She crumples over your sister." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The other dolls in the room start moving, but stop after a few seconds. This has never happened before. What do they need to do?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You can faintly hear their thoughts. You smile. You know what you need to do." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You open up Sarah's book. There is a single sentence written on the first page." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'This is wrong.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Help me or don't, Sarah. I'll not listen to moralizing from you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The needed symbols appear on the page. You smirk and write them on Pygmalie's body. You realize they are the same ones on the altar." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You place Pygmalie's body on the altar, close Sarah, and wait. The dolls at the edge of the room stare in silence." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Something is here." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It is large." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It is hungry." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Pygmalie is empty. There was not much to take. It will be hungry again, soon. But, it is sated for now." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You feel a rush of air. It leaves. The storm outside abates." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You touch Pygmalie's emptied mind. You have learned a little about how to do this. You reach in and pull what little is there." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Command. That's the one you need. There were others. You take those, too. Pygmalie drools on herself." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The dolls in the room are now yours." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You think, 'Come to me'. They approach you, bowing before you. They address you, as one. 'Creator', they say." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'This one, Pygmalie. Make her like us.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The dolls inject Pygmalie. She becomes smooth, plastic. She is like you..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gzTextVar.bSpecialEndingBetrayBetray = true
    gzTextVar.bExternalEndingCall = true
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
    
    return
end

-- |[All Other Cases]|
TL_SetProperty("Append", "Please make your choice. You must [submit] or [betray]." .. gsDE)
return