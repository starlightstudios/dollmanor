-- |[ ===================================== Set to Phase 6 ===================================== ]|
--Final phase, final confrontation.
gzTextVar.iGameStage = 6
gzTextVar.bIsJessieFollowing = true
gzTextVar.bIsJessieMainHalled = false
gzTextVar.bIsLaurenFollowing = true
gzTextVar.bIsLaurenMainHalled = false
gzTextVar.bHasSeenPaintedDollDialogue = true
gzTextVar.bHasSeenHallwayDialogue = true
gzTextVar.bHasSeenHallwayReturnDialogue = true
gzTextVar.bHasSeenHallwayStatueDialogue = true
gzTextVar.iHypnoticCheck = 0
gzTextVar.bCloseDoorBehind = false
gzTextVar.bForceMoveStatue = false
gzTextVar.bSecretStatue = false
gzTextVar.bDisabledLight = true
gzTextVar.bDisabledLightCutscene = true

--Mary's skin color goes to 2, since that's the one she has in the ending CGs.
gzTextVar.gzPlayer.iSkinColor = 2

--Setup.
local p = gzTextVar.iPygmalieIndex
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex

--Variable
local sOldLocation = "Null"
local fOldX, fOldY, fOldZ
local fNewX, fNewY, fNewZ

--Move Pygmalie to the upper floor.
if(gzTextVar.zEntities[p].sLocation ~= "Ritual Altar") then
    sOldLocation = gzTextVar.zEntities[p].sLocation
    gzTextVar.zEntities[p].sLocation = "Ritual Altar"
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition("Ritual Altar")
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[p].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Instantly move Jessie to Pygmalie's Study.
if(gzTextVar.zEntities[j].sLocation ~= gzTextVar.gzPlayer.sLocation) then
    sOldLocation = gzTextVar.zEntities[j].sLocation
    gzTextVar.zEntities[j].sLocation = gzTextVar.gzPlayer.sLocation
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[j].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Instantly move Lauren to our position if he wasn't there already.
if(gzTextVar.zEntities[l].sLocation ~= gzTextVar.gzPlayer.sLocation) then
    sOldLocation = gzTextVar.zEntities[l].sLocation
    gzTextVar.zEntities[l].sLocation = gzTextVar.gzPlayer.sLocation
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[l].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Move Sarah to the study. She will now also follow the player. She has no indicator.
if(gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation ~= "Pygmalie's Study") then
    gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation = "Pygmalie's Study"
end

--Spawn the appropriate key if it has not been spawned yet.
if(gzTextVar.bSpawnedScenarioKey == false) then
    
    --Heart Key.
    if(gzTextVar.sPygmalieKey == "Heart Key") then
        gzTextVar.bSpawnedScenarioKey = true
        fnRegisterObject(gzTextVar.gzPlayer.sLocation, "heart key", "heart key", true, gzTextVar.sRootPath .. "Item Handlers/Keys/Heart Key.lua")
    end
end

--Player is not surrendering.
gzTextVar.bIsPlayerSurrendering = false

--Mary needs to become a dancer doll.
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'
gzTextVar.gzPlayer.sFormState = "Doll Dancer"
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Dancer " .. sEndLetter
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    
--Fullheal the player.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
gzTextVar.gzPlayer.bIsCrippled = false
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Unlock all doors. Just in case.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

--Achievement
Steam_UnlockAchievement(gciAchievement_NothingLeftToLose)
        
--Change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 2, 4, ciCodePlayer)
