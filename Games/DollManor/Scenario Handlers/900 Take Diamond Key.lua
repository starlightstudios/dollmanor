-- |[Take Diamond Key]|
--A special script that fires when the player takes the diamond key, but only in the "Normal" manor.
-- This causes the rest of the phase 2 enemies to spawn that didn't initially.
--This of this as Phase 2.5.
if(gzTextVar.bExtraPhase2Enemies == true) then return end

--Flag to block repeats if the player drops the key and takes it again.
gzTextVar.bExtraPhase2Enemies = true
gzTextVar.bHasDiamondKey = true

--Normal manor:
if(gzTextVar.sManorType == "Normal") then
    for i = 1, #gzTextVar.zRespawnList, 1 do
        local sID = gzTextVar.zRespawnList[i].sIdentity
        if(sID == "Doll L" or sID == "Doll G") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnDoll(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        elseif(sID == "Titan A" or sID == "Titan B") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnTitan(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        elseif(sID == "Clay C") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnClayGirl(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        end
    end

--Other manors do nothing.
else
    return
end
