-- |[Deactivate Fog]|
--Turns off fog if it's active.
if(gzTextVar.bIsDarkActive == false) then return end
gzTextVar.bIsDarkActive = false

--Set.
TL_SetProperty("Deactivate Overlays")

--Handle the examination input.
gbHandledInput = true

--Description.
TL_SetProperty("Append", "The darkness lifts..." .. gsDE)

--Standard.
fnListEntities(gzTextVar.gzPlayer.sLocation, true)
fnListObjects(gzTextVar.gzPlayer.sLocation, true)
