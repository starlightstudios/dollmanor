-- |[Activate Fog]|
--Turns on fog if it's not already on. This is called as part of the examination handler in fog-activating rooms.
if(gzTextVar.bIsFogActive == true) then return end
gzTextVar.bIsFogActive = true

--Set.
local fConstant = 0.10
TL_SetProperty("Activate Overlays")
TL_SetProperty("Clear Overlays")
if(gzTextVar.gzPlayer.zWeapon ~= nil and gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
    TL_SetProperty("Register Overlay", "Fog0", "Root/Images/DollManor/Overlays/Fog0Big", gciOverlay_Exclusion, 0, 0, 0, 0, 200.0, 200.0)
else
    TL_SetProperty("Register Overlay", "Fog0", "Root/Images/DollManor/Overlays/Fog0", gciOverlay_Exclusion, 0, 0, 0, 0, 200.0, 200.0)
end
TL_SetProperty("Register Overlay", "Fog4", "Root/Images/DollManor/Overlays/Fog4", gciOverlay_Normal,    LM_GetRandomNumber(0, 256), LM_GetRandomNumber(0, 256),  2.0 * fConstant,  1.5 * fConstant, 512.0, 512.0)
TL_SetProperty("Register Overlay", "Fog1", "Root/Images/DollManor/Overlays/Fog1", gciOverlay_Bypass,    LM_GetRandomNumber(0, 256), LM_GetRandomNumber(0, 256), -1.5 * fConstant,  2.5 * fConstant, 512.0, 512.0)
TL_SetProperty("Register Overlay", "Fog2", "Root/Images/DollManor/Overlays/Fog2", gciOverlay_Bypass,    LM_GetRandomNumber(0, 256), LM_GetRandomNumber(0, 256), -2.5 * fConstant, -2.5 * fConstant, 512.0, 512.0)
--TL_SetProperty("Register Overlay", "Fog3", "Root/Images/DollManor/Overlays/Fog3", gciOverlay_Bypass,    LM_GetRandomNumber(0, 256), LM_GetRandomNumber(0, 256),  2.0 * fConstant,  1.5 * fConstant, 512.0, 512.0)

--Handle the examination input.
gbHandledInput = true

--Description.
TL_SetProperty("Append", "The fog grows thick around you here. It hugs tightly. This isn't natural..." .. gsDE)

--Standard.
fnListEntities(gzTextVar.gzPlayer.sLocation, true)
fnListObjects(gzTextVar.gzPlayer.sLocation, true)
