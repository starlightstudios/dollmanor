-- |[Activate Dark]|
--Turns on darkness if it's not already on. This is called as part of the examination handler in dark-activating rooms.
if(gzTextVar.bIsDarkActive == true) then return end
gzTextVar.bIsDarkActive = true

--Set.
local fConstant = 0.10
TL_SetProperty("Activate Overlays")
TL_SetProperty("Clear Overlays")
if(gzTextVar.gzPlayer.zWeapon ~= nil and gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
    TL_SetProperty("Register Overlay", "Black0", "Root/Images/DollManor/Overlays/Black0Big", gciOverlay_Exclusion, 0, 0, 0.0, 0.0, 1000.0, 400.0)
else
    TL_SetProperty("Register Overlay", "Black0", "Root/Images/DollManor/Overlays/Black0", gciOverlay_Exclusion, 0, 0, 0.0, 0.0, 1000.0, 400.0)
end
TL_SetProperty("Register Overlay", "Black1", "Root/Images/DollManor/Overlays/Black1", gciOverlay_Normal,    0, 0, 0.0, 0.0, 1000.0, 400.0)

--Handle the examination input.
gbHandledInput = true

--Description.
TL_SetProperty("Append", "The darkness holds close, as though you were being gripped by a black hand." .. gsDE)

--Standard.
fnListEntities(gzTextVar.gzPlayer.sLocation, true)
fnListObjects(gzTextVar.gzPlayer.sLocation, true)
