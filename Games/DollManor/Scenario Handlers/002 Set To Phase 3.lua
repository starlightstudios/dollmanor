-- |[ ===================================== Set to Phase 3 ===================================== ]|
--Instantly sets the game to Phase 3. Jessie and Lauren teleport to the player's position, Sarah 
-- appears in Pygmalie's Study, and the difficulty increases as clay girls spawn.
gzTextVar.iGameStage = 3
gzTextVar.bIsJessieFollowing = true
gzTextVar.bIsJessieMainHalled = false
gzTextVar.bIsLaurenFollowing = true
gzTextVar.bIsLaurenMainHalled = false
gzTextVar.bHasSeenPaintedDollDialogue = true
gzTextVar.bHasSeenHallwayDialogue = true
gzTextVar.bHasSeenHallwayReturnDialogue = true
gzTextVar.bHasSeenHallwayStatueDialogue = true
gzTextVar.iHypnoticCheck = 0
gzTextVar.bCloseDoorBehind = false
gzTextVar.bForceMoveStatue = false
gzTextVar.bSecretStatue = false
gzTextVar.bDisabledLight = true
gzTextVar.bDisabledLightCutscene = true

--The stranger can respawn again.
gzTextVar.bStrangerCanRespawn = true

--Setup.
local p = gzTextVar.iPygmalieIndex
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex

--Variable
local sOldLocation = "Null"
local fOldX, fOldY, fOldZ
local fNewX, fNewY, fNewZ

--Move Pygmalie to the upper floor.
if(gzTextVar.zEntities[p].sLocation ~= "Ritual Altar") then
    sOldLocation = gzTextVar.zEntities[p].sLocation
    gzTextVar.zEntities[p].sLocation = "Ritual Altar"
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition("Ritual Altar")
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[p].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Instantly move Jessie to our position if she wasn't there already.
if(gzTextVar.gzPlayer.sLocation ~= gzTextVar.zEntities[j].sLocation) then
    sOldLocation = gzTextVar.zEntities[j].sLocation
    gzTextVar.zEntities[j].sLocation = gzTextVar.gzPlayer.sLocation
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[j].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Instantly move Lauren to our position if he wasn't there already.
if(gzTextVar.gzPlayer.sLocation ~= gzTextVar.zEntities[l].sLocation) then
    sOldLocation = gzTextVar.zEntities[l].sLocation
    gzTextVar.zEntities[l].sLocation = gzTextVar.gzPlayer.sLocation
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[l].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Move Sarah to the study. She has no indicator.
if(gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation ~= "Pygmalie's Study") then
    gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation = "Pygmalie's Study"
end

--Spawn the appropriate key if it has not been spawned yet.
if(gzTextVar.bSpawnedScenarioKey == false) then
    
    --Heart Key.
    if(gzTextVar.sPygmalieKey == "Heart Key") then
        gzTextVar.bSpawnedScenarioKey = true
        fnRegisterObject(gzTextVar.gzPlayer.sLocation, "heart key", "heart key", true, gzTextVar.sRootPath .. "Item Handlers/Keys/Heart Key.lua")
    end
end

-- |[Small Manor]|
--Activate the resin titan and two claygirls.
if(gzTextVar.sManorType == "Demo" or gzTextVar.sManorType == "Simple") then
    for i = 1, #gzTextVar.zRespawnList, 1 do
        if(gzTextVar.zRespawnList[i].sIdentity == "Titan B") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnTitan(gzTextVar.zRespawnList[i].sSpawnPosition, gzTextVar.zRespawnList[i].sIdentity, gzTextVar.zRespawnList[i].sPatrolPath)
        elseif(gzTextVar.zRespawnList[i].sIdentity == "Clay A") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnClayGirl(gzTextVar.zRespawnList[i].sIdentity)
        elseif(gzTextVar.zRespawnList[i].sIdentity == "Clay B") then
            gzTextVar.zRespawnList[i].bIsActive = true
            fnSpawnClayGirl(gzTextVar.zRespawnList[i].sIdentity)
        end
    end

    --Spawn three more clay girls. They will take positions on random items in the manor.
    fnSpawnClayGirl("Claygirl Disguise C")
    fnSpawnClayGirl("Claygirl Disguise D")
    fnSpawnClayGirl("Claygirl Disguise E")

-- |[Normal Manor]|
--Spawn three more disguised claygirls.
elseif(gzTextVar.sManorType == "Normal") then
    fnSpawnClayGirl("Claygirl Disguise C")
    fnSpawnClayGirl("Claygirl Disguise D")
    fnSpawnClayGirl("Claygirl Disguise E")
end

--Build locality.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()
