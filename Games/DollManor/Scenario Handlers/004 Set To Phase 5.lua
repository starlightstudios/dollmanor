-- |[ ===================================== Set to Phase 5 ===================================== ]|
--Mary turns into a book and is going to get dolled.
gzTextVar.iGameStage = 5
gzTextVar.bIsJessieFollowing = false
gzTextVar.bIsJessieMainHalled = false
gzTextVar.bIsLaurenFollowing = false
gzTextVar.bIsLaurenMainHalled = false
gzTextVar.bHasSeenPaintedDollDialogue = true
gzTextVar.bHasSeenHallwayDialogue = true
gzTextVar.bHasSeenHallwayReturnDialogue = true
gzTextVar.bHasSeenHallwayStatueDialogue = true
gzTextVar.iHypnoticCheck = 0
gzTextVar.bCloseDoorBehind = false
gzTextVar.bForceMoveStatue = false
gzTextVar.bSecretStatue = false
gzTextVar.bDisabledLight = true
gzTextVar.bDisabledLightCutscene = true

--Mary's skin color goes to 2, since that's the one she has in the ending CGs.
gzTextVar.gzPlayer.iSkinColor = 2

--Setup.
local p = gzTextVar.iPygmalieIndex
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex

--Variable
local sOldLocation = "Null"
local fOldX, fOldY, fOldZ
local fNewX, fNewY, fNewZ

--Move Pygmalie to the upper floor.
if(gzTextVar.zEntities[p].sLocation ~= "Ritual Altar") then
    sOldLocation = gzTextVar.zEntities[p].sLocation
    gzTextVar.zEntities[p].sLocation = "Ritual Altar"
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition("Ritual Altar")
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[p].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Instantly move Jessie to Pygmalie's Study.
if(gzTextVar.zEntities[j].sLocation ~= "Pygmalie's Study") then
    sOldLocation = gzTextVar.zEntities[j].sLocation
    gzTextVar.zEntities[j].sLocation = "Pygmalie's Study"
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition("Pygmalie's Study")
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[j].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Instantly move Lauren to our position if he wasn't there already.
if(gzTextVar.zEntities[l].sLocation ~= "Pygmalie's Study") then
    sOldLocation = gzTextVar.zEntities[l].sLocation
    gzTextVar.zEntities[l].sLocation = "Pygmalie's Study"
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition("Pygmalie's Study")
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[l].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Move Sarah to the study. She has no indicator.
if(gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation ~= "Pygmalie's Study") then
    gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation = "Pygmalie's Study"
end

--Spawn the appropriate key if it has not been spawned yet.
if(gzTextVar.bSpawnedScenarioKey == false) then
    
    --Heart Key.
    if(gzTextVar.sPygmalieKey == "Heart Key") then
        gzTextVar.bSpawnedScenarioKey = true
        fnRegisterObject(gzTextVar.gzPlayer.sLocation, "heart key", "heart key", true, gzTextVar.sRootPath .. "Item Handlers/Keys/Heart Key.lua")
    end
end

--Player is surrendering.
gzTextVar.bIsPlayerSurrendering = true
