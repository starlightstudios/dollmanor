-- |[ ============================ Special Input Handler: Sacrifice ============================ ]|
--Used during the sacrifice ending. Gets re-routed to from the normal input handler.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

-- |[Mary]|
if(sString == "mary") then
    TL_SetProperty("Append", "You put your hand on Lauren's shoulder." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Lauren... I love you...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "He looks up at you. 'Please, no...'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'I need you to be strong. You're going to have to take care of Jessie from now on.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'But...'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'No buts. One of us has to go, and I have the least to lose. Look at me.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'If there is a cure, we don't have the time to find it. I won't be able to go back home. But you can.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'You, and Jessie, need to leave.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'She's made her choice. Come on, kiddo.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'But can't we use Pygmalie?'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'No, because she has a job here. We don't like it, but it's not her who brought us here.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'She lets people keep living after being... eaten. Her work is awful, but it's important. She's not the bad guy.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Lauren, I love you. Grow up big and strong. Never forget me.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "Lauren has choked up. He cannot speak. He's crying too hard. Instead, he hugs you. Tight. You hug him back." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I'll get him out of here. Don't worry, Mary.'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'It's in your hands now. Goodbye.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You watch as the two run out of the manor, a trail of tears behind both. You look at the altar, wistfully." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You'll still be a doll after this. That much was inevitable. You're glad that you did the right thing. Lauren deserves a chance to grow up." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You reflect on all the things you won't get to experience. The life you won't get to live. It's all right. This is what a real hero would do. You can do this proudly." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gzTextVar.bSpecialEndingSacrificeMary = true
    gzTextVar.bExternalEndingCall = true
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
    
    return
end

-- |[Jessie]|
if(sString == "jessie") then
    TL_SetProperty("Append", "You put your hand on Jessie's shoulder. She winces." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I was afraid you were going to pick me.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Will you? Please?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I've been preparing myself for it, mentally. I can do it.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'J-J-Jessie... No...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I've had a good time hanging out with you, kiddo. You grow up big and strong for me, okay? Don't put up with any nonsense.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'And Mary, will you...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'I'll have to find a way to get by. Hey, on the bright side, I won't need to eat or drink, right?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'It'll be easier to live off the grid like that.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'But... Can't we use Pygmalie?'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'No, because she has a job here. We don't like it, but it's not her who brought us here.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'She lets people keep living after being... eaten. Her work is awful, but it's important. She's not the bad guy.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Lauren... I'll miss you. I know we didn't always get along, but as of now you are my honorary brother. Okay?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "Lauren has choked up. He cannot speak. He's crying too hard. Instead, he hugs Jessie. Tight. She hugs back." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'I'll get him out of here. Come on, Lauren. We need a head start.'" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "'It's in your hands now, Jessie. You know the symbols?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'By heart. See you around, Mary.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "You take Lauren's hand and sprint out of the manor. He bawls the whole way, but you don't stop running." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The storm stops, the sky clears. You hear something behind you. You run." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gzTextVar.bSpecialEndingSacrificeJessie = true
    gzTextVar.bExternalEndingCall = true
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
    
    return
end

-- |[Lauren]|
if(sString == "lauren") then
    TL_SetProperty("Append", "Before you can make a decision, Lauren steps forward." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'What are you doing, Lauren?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Are you... volunteering?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'No. N. O. Not happening. I won't allow it.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Jessie. I'm doing it.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I said no! Absolutely not! You're the one who needs to leave the most! You've got the most life to live!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'I said I'm doing it, Jessie.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I'm not a little kid anymore. You two have done so much for me, let me do something for you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Do you realize what you're saying? You won't be you anymore!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'I'll be a doll, at worst. You, and Mary, will be free. I'll do it.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary! Stop him!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'I won't. This is his decision.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Lauren, you've grown up so much in the last few hours. I'm truly proud of you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'But - '" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'He's a big boy. Most people would kill for a brother like this.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Lauren. We will never, ever forget you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Just - damn...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Fine. Fine. Let's do this thing.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Thanks, Jessie. I'll miss you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I'll miss you too, Lauren.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Do you know what to do?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Yeah. I know. I asked Sarah about it earlier.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I've been planning this since the basement. I - thought I'd chicken out. But I didn't.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Are you sure you don't want me to - '" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Go. You'll need a head start. Sarah isn't sure how long you'll have.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'So this is goodbye?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Goodbye, Jessie. Goodbye, Mary.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Goodbye, Lauren.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gzTextVar.bSpecialEndingSacrificeLauren = true
    gzTextVar.bExternalEndingCall = true
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
    
    return
end

-- |[Pygmalie]|
if(sString == "pygmalie") then
    TL_SetProperty("Append", "You cannot sacrifice Pygmalie, there is not enough of her to truly sate the thing." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Just to be sure, you quickly ask Sarah. She confirms it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It seems you'll have to sacrifice someone else..." .. gsDE)
    TL_SetProperty("Create Blocker")
    return
end

-- |[Sarah]|
if(sString == "sarah") then
    TL_SetProperty("Append", "After considering your options carefully, you place Sarah's book on Pygmalie's head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary? What are you doing?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'The right thing. I need something from her.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'But she said - '" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Then this will be her big chance to make things right.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Sarah", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "Pygmalie - no, Sarah - sputters to life. She wakes. She gasps and panics." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'What - I'm - what did you do?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Aaarrggh - My head! It hurts! It hurts! There's too much!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Sarah, I need two things from you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Give me Command. Make the dolls mine.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Sarah", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Takeittakeittakeit!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "You grasp her firmly by the head. Ideas enter your head, about what to do, about how to do it. You don't know where they come from. You can reach inside her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Perhaps it's an effect of being in the book. Perhaps it's being a doll. Perhaps it's the manor itself. It doesn't matter." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You reach in and you take it. You take the other parts, too. Command. Benevolence. Order. Disruption. You take them all." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "With a single thought, the dolls in the room snap to life. They gather around Sarah." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Prepare her' you utter. The dolls immediately paint the symbols on her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'You said you needed two things from her...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Her knowledge, and her sacrifice.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'This will keep the thing sated long enough for you to escape.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary... You just said for us to escape. Didn't you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Sorry, guys, this is where we part ways.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'But we can go! We can escape! That's what Sarah is doing for us!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Pygmalie did an important job. She kept it fed, regularly. It ate part of her, nibbled. It had to eat less often with her around.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'And, in a way, she let people keep living. Just, as dolls.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'But - '" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'I can't go back, Lauren. I'm a doll. I can't fit in with society like you can.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'But if I stay here, maybe someday, I can beat it. And it won't eat as often. I'll be saving people just by being here.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Who cares!? Who cares about a bunch of people we'll never even meet! Come with us!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Of course I will. Just as soon as I find a way to beat the thing.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'It might take some time. Don't know how long. I'll catch up.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'You'll...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Catch up...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'This isn't goodbye. You think I'm going to let this thing get the better of me? Do you know who you're talking to?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Yeah. Yeah! If anyone can do it, it's Mary!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'I - I - I won't cry...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'But just to be sure - don't let anything happen to Jessie. She's not tough like we are, Lauren.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Hey!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'I promise.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Append", "'Good. I couldn't wish for a better brother.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Now go. The dolls won't stop you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gzTextVar.bSpecialEndingSacrificeSarah = true
    gzTextVar.bExternalEndingCall = true
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)
    
    return
end

-- |[All Other Cases]|
TL_SetProperty("Append", "Please make your choice. You must sacrifice someone to escape. Enter the name of the person to sacrifice." .. gsDE)
return