-- |[ ======================================== Turn End ======================================== ]|
--Called whenever the player ends their turn.
local zTime = os.clock()
gzTextVar.iWorldTurns = gzTextVar.iWorldTurns + 1
TL_SetProperty("Set Storage Line", 0, "Turn: " .. gzTextVar.iWorldTurns)

-- |[Post-Exec]|
--Special scenario cases get handled before ending checks.
LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/100 Turn Post Exec.lua")

--Reset the previous room handler. Some enemies need this but we can't assume it's correct any more.
gzTextVar.gzPlayer.sPrevLocationStore = gzTextVar.gzPlayer.sPrevLocation
gzTextVar.gzPlayer.sPrevLocation = "Null"

-- |[Endings]|
--Checks if the game has ended.
gzTextVar.bIsGameEnding = false
LM_ExecuteScript(fnResolvePath() .. "500 Ending Handler.lua")
if(gzTextVar.bIsGameEnding == true) then return end

--Player's HP can regen when in non-human forms.
if(gzTextVar.gzPlayer.iHP < gzTextVar.gzPlayer.iHPMax) then
    
    --Regen form blockers. Humans do not regen HP.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
    --Blank Dolls don't regen HP. That'd let them move away while being carried!
    elseif(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Bride TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 5) == "Bride") then

    --Dancer TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 6) == "Dancer") then

    --Geisha TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 6) == "Geisha") then

    --Goth TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Goth") then

    --Princess TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 8) == "Princess") then

    --Punk TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Punk") then

    --All checks passed. Player can regen HP.
    else
    
        --Increment timer. If it reaches 5, regen one HP.
        gzTextVar.gzPlayer.iRegenTimer = gzTextVar.gzPlayer.iRegenTimer + 1
        if(gzTextVar.gzPlayer.iRegenTimer >= 5) then
            gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHP + 1
            gzTextVar.gzPlayer.iRegenTimer = 0
            
            --Store.
            gzTextVar.gzPlayer.bIsCrippled = false
            TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        end
    end
    
--No need to regen.
else
    gzTextVar.gzPlayer.iRegenTimer = 0
end

-- |[ =================================== AI Entity Handler ==================================== ]|
--All respawn entries increment by one. This does not happen in phase 1! Enemies don't respawn!
if(gzTextVar.iGameStage > 1) then
    for p = 1, #gzTextVar.zRespawnList, 1 do
        
        --Spawns must be active to run their timers.
        if(gzTextVar.zRespawnList[p].bIsActive == true) then
            gzTextVar.zRespawnList[p].iRespawnTimer = gzTextVar.zRespawnList[p].iRespawnTimer + 1
        end
    end
end

--Run across the entities and order them to run their AI updates.
--print(string.format("Time before AI cycle: %.3f", os.clock() - zTime))
gzTextVar.bCombatActivatedThisTick = false
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    --Check where this entity is on the respawn list. If they have a slot, they set it to 0.
    for p = 1, #gzTextVar.zRespawnList, 1 do
        if(gzTextVar.zRespawnList[p].sIdentity == gzTextVar.zEntities[i].sSpecialIdentifier) then
            gzTextVar.zRespawnList[p].iRespawnTimer = 0
            break
        end
    end
    
    --If there's no handler, do nothing.
    if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
        
    --Run AI routines.
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
        fnClaygirl_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
        fnDoll_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
        fnTitan_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
        fnGoop_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
        fnStranger_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
        fnJessie_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
        fnLauren_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
        fnPygmalie_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
        fnSarah_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll Tutorial Patrol") then
        fnDollTutPatrol_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
        fnDollStationary_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
        fnDollStationaryCombat_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat B") then
        fnDollStationaryCombatB_AI(i)
    end
end
--print(string.format("Time after AI cycle: %.3f", os.clock() - zTime))

--If an enemy has 0 HP, remove them. Some enemies can be removed passively, without combat.
local bRemovedEnemy = true
while(bRemovedEnemy == true) do
    bRemovedEnemy = false
    
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].zCombatTable ~= nil and gzTextVar.zEntities[i].zCombatTable.iHealth < 1) then
            fnRemoveEntity(i)
            bRemovedEnemy = true
            break
        end
    end
end

--After all entities have moved, run their secondary AI update. This allows the AIs to spot the player after movement.
-- We allow all entities to move first as some may change door states.
gzTextVar.bIsPostTurnSpotting = true
--print(string.format("Time before post-turn spotting: %.3f", os.clock() - zTime))
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    --If there's no handler, do nothing.
    if(gzTextVar.zEntities[i].sAIHandlerPostTurn == nil or gzTextVar.zEntities[i].sAIHandlerPostTurn == "None") then
        
    --Run AI routines.
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Claygirl") then
        fnClaygirl_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Doll") then
        fnDoll_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Titan") then
        fnTitan_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Goop") then
        fnGoop_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Stranger") then
        fnStranger_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Jessie") then
        fnJessie_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Lauren") then
        fnLauren_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Pygmalie") then
        fnPygmalie_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Sarah") then
        fnSarah_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll Tutorial Patrol") then
        fnDollTutPatrol_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Tutorial Stationary") then
        fnDollStationary_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Tutorial Stationary Combat") then
        fnDollStationaryCombat_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Tutorial Stationary Combat B") then
        fnDollStationaryCombatB_PostTurnSpotCheck(i)
    end
    
end
--print(string.format("Time after post-turn spotting: %.3f\n", os.clock() - zTime))
gzTextVar.bIsPostTurnSpotting = false

--Remove entities who zeroed their HP after their post-turn spotting.
bRemovedEnemy = true
while(bRemovedEnemy == true) do
    bRemovedEnemy = false
    
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].zCombatTable ~= nil and gzTextVar.zEntities[i].zCombatTable.iHealth < 1) then
            fnRemoveEntity(i)
            bRemovedEnemy = true
            break
        end
    end
end

--Scan across the respawn list. If any of the entries reached 10, respawn the doll.
--io.write("Sequence " .. gzTextVar.gzPlayer.sTFSeq .. "\n")
--io.write("Respawn report:\n")
local bRespawnedEnemy = false
local bRespawnedDoll = false
local bRespawnedTitan = false
local bRespawnedClaygirl = false
for p = 1, #gzTextVar.zRespawnList, 1 do
    
    --Debug.
    --io.write(" Timer " .. p .. " is " .. gzTextVar.zRespawnList[p].iRespawnTimer .. ". " .. gzTextVar.zRespawnList[p].sSpawnPosition .. "\n")
    
    --If the entity is inactive, do nothing.
    if(gzTextVar.zRespawnList[p].bIsActive == false) then
    
    --If the counter hit 20 (10 in the small manor), respawn. Respawn speed can increase as the game goes on!
    elseif(gzTextVar.zRespawnList[p].iRespawnTimer >= gzTextVar.iRespawnTurns) then
        
        --Check if the room is currently visible. If it is, enemies do not respawn and have their counter decremented.
        local iRoomIndex = fnGetRoomIndex(gzTextVar.zRespawnList[p].sSpawnPosition)
        if(iRoomIndex < 0 or iRoomIndex >= gzTextVar.iRoomsTotal) then
            --io.write("  Invalid room. " .. iRoomIndex .. "\n")
            
        --Room index is valid.
        else
        
            --Room is currently visible.
            if(gzTextVar.zRoomList[iRoomIndex].bIsVisibleNow == true) then
                gzTextVar.zRespawnList[p].iRespawnTimer = math.floor(gzTextVar.zRespawnList[p].iRespawnTimer * 0.40)
                --io.write("Room is visible!\n")
                
            --Room is not visible, respawn!
            else
            
                --Debug.
                --io.write("  Respawn!\n")
            
                --Reset.
                gzTextVar.zRespawnList[p].iRespawnTimer = 0
                
                --If the first part is "Doll" then spawn a doll.
                if(gzTextVar.zRespawnList[p].sEnemyType == "Doll") then
                    bRespawnedEnemy = true
                    bRespawnedDoll = true
                    fnSpawnDoll(gzTextVar.zRespawnList[p].sSpawnPosition, gzTextVar.zRespawnList[p].sIdentity, gzTextVar.zRespawnList[p].sPatrolPath)
                
                --If it's "Titan" then spawn a resin titan.
                elseif(gzTextVar.zRespawnList[p].sEnemyType == "Titan") then
                    bRespawnedEnemy = true
                    bRespawnedTitan = true
                    fnSpawnTitan(gzTextVar.zRespawnList[p].sSpawnPosition, gzTextVar.zRespawnList[p].sIdentity, gzTextVar.zRespawnList[p].sPatrolPath)
                
                --If it's "Clay" then spawn a claygirl, not in disguise.
                elseif(gzTextVar.zRespawnList[p].sEnemyType == "Clay") then
                    bRespawnedEnemy = true
                    bRespawnedClaygirl = true
                    fnSpawnClayGirl(gzTextVar.zRespawnList[p].sIdentity, gzTextVar.zRespawnList[p].sSpawnPosition, gzTextVar.zRespawnList[p].sPatrolPath)
                end
            end
        end
    end
end

--Special: Stranger Respawn. There is only one. Ringing the bell prevents the stranger from spawning.
if(gzTextVar.zStranger.iNeedsRespawn > -1 and gzTextVar.bRungBell == false and gzTextVar.iGameStage >= 2) then
    
    --In phase 4, the stranger *always* respawns!
    if(gzTextVar.iGameStage >= 4) then gzTextVar.bStrangerCanRespawn = true end
    
    --In phase 2 and 3, Stranger respawns 150 turns after being defeated.
    if(gzTextVar.bStrangerCanRespawn == false) then
        
        --Player needs to have found at least two dark crystals.
        if(gzTextVar.iTrapsDisarmed >= 2) then
            if(gzTextVar.iWorldTurns >= gzTextVar.iStrangerDefeatedTurn + 150) then
                gzTextVar.bStrangerCanRespawn = true
            end
        end
    end
    
    --If the stranger cannot respawn for story reasons, stop here:
    if(gzTextVar.bStrangerCanRespawn == false) then
    
    --Stranger can respawn.
    else
        --Increment.
        gzTextVar.zStranger.iNeedsRespawn = gzTextVar.zStranger.iNeedsRespawn + 1
        if(gzTextVar.zStranger.iNeedsRespawn > 10) then
            
            --Flag.
            gzTextVar.zStranger.iNeedsRespawn = -1
            
            --Stranger respawns in the chapel. Make sure the player can't see the location.
            local iRoomIndex = fnGetRoomIndex("Chapel Altar")
            if(iRoomIndex >= 0 and iRoomIndex < gzTextVar.iRoomsTotal) then
                
                --Room is visible, so reset the timer.
                if(gzTextVar.zRoomList[iRoomIndex].bIsVisibleNow == true) then
                    gzTextVar.zStranger.iNeedsRespawn = 10
                
                --Otherwise, respawn.
                else
                    fnSpawnStranger("Chapel Altar")
                    gzTextVar.bStrangerCanRespawn = false
                end
            end
        end
    end
end

--If at least one enemy respawned, print a message.
if(bRespawnedEnemy) then
    
    --Titans get priority:
    if(bRespawnedTitan == true) then
        TL_SetProperty("Append", "You hear a distant rumble of a mighty foe in the manor..." .. gsDE)
    
    --Claygirls are next:
    elseif(bRespawnedClaygirl == true) then
        TL_SetProperty("Append", "You hear a gurgling sound of flowing clay somewhere in the manor..." .. gsDE)
    
    --Dolls are lowest:
    elseif(bRespawnedDoll == true) then
        TL_SetProperty("Append", "You hear a distant giggle and the clacking of plastic joints somewhere in the manor..." .. gsDE)
    
    --Error/unhandled.
    else
        TL_SetProperty("Append", "You feel a hostile presence within the manor..." .. gsDE)
    end
end

--Rebuild locality info and visibility info.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()

--Rebuild sound indicators.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Rebuild Sound.lua")

-- |[ ======================================= Move Lock ======================================== ]|
--In situations where the player is not meant to move, lock their movement here.

--Player is a Doll:
if(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll" and gzTextVar.iGameStage < 6) then
    
    --Jessie is a Blank Doll and in the Main Hall:
    if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation and gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Blank Doll") then
        
        --Flag.
        gzTextVar.gzPlayer.bDollMoveLock = true
        
        --Carrying both dolls?
        if(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Blank Doll") then
            
            gzTextVar.bAlreadySawLaurenTFStart = true
            
            TL_SetProperty("Append", "You enter the room quietly, carrying with you two puppets. Two new sisters. Two - something. Friends? Something is wrong..." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You're not a doll, you shouldn't be here... Where is here? How did you get here? You're getting confused!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You look at your hands - plastic hands can't be your, you're a human! What has happened to you? You've got to run!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You feel your creator draw close. You see her shadow on the floor, and you look up to see her. She has a wicked grin." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "The part of you that was scared disappears. You helped her find it, and now it's gone. You're now a perfect dolly. You grin. You love being perfect!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator will want your help finishing your new sisters, and you're nearly bursting with joy! You can't wait to play with them!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
            TL_SetProperty("Append", "You carry the puppets to the same chair you sat in not long ago, and set one down. Your other sister will have to wait her turn - you hope she can contain her excitement!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Just one.
        else
            TL_SetProperty("Append", "You enter the room quietly, bearing a limp, unmoving puppet. Your creator has not noticed yet. You stand and wait." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Something in you stirs. You should - run. Hide. Flee from this place. It's a strong instinct. You should carry the puppet with you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "There must be a cure. There must be something you can do!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator turns to see you, standing quietly with your new sister. She smiles, and all the feelings wash away." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She will want you to help in finishing your sister! Oh that will be so much fun!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
            TL_SetProperty("Append", "You carry the puppet to the same chair you sat in not long ago, and set her down. You hope she will enjoy being completed as much as you did!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        end
    
    --Lauren is a Blank Doll and in the Main Hall:
    elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Blank Doll") then
        
        --Flag
        gzTextVar.gzPlayer.bDollMoveLock = true
        
        --First time this has happened for Lauren.
        if(gzTextVar.bAlreadySawLaurenTFStart == false) then
        
            --Flag.
            gzTextVar.bAlreadySawLaurenTFStart = true
        
            TL_SetProperty("Append", "You enter the room quietly. You feel your blank sister stir. Something about her is struggling." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "A memory strikes you, and you don't know where it came from. Your sister was - a boy? Your brother? No, that can't be right." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Should you run? Flee the manor? No. No. Good dollies ask their creator for help. You open your mouth to speak." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator turns to see you, catching you. You were about to speak without being spoken to - what a naughty dolly!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "But she smiles, and you know you are forgiven. She will want your help finishing your new sister. You can't wait!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You carry the puppet to the same chair you sat in not long ago, and set her down. You hope she will enjoy being completed as much as you did!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        end
    end

--Secret Statue Sequence.
elseif(gzTextVar.bSecretStatue and gzTextVar.gzPlayer.sLocation == "Stone Gallery" and gzTextVar.gzPlayer.sFormState == "Statue") then

    --Setup.
    local j = gzTextVar.iJessieIndex
    local l = gzTextVar.iLaurenIndex

    --Both Jessie and Lauren are here and the Secret Statue sequence is playing out.
    if(gzTextVar.zEntities[j].sLocation == "Stone Gallery" and gzTextVar.zEntities[l].sLocation == "Stone Gallery") then
        
        --Status changes.
        gzTextVar.zEntities[j].bIsCrippled = false
        gzTextVar.zEntities[j].sQueryPicture = "Root/Images/DollManor/Characters/JessieStone"
        gzTextVar.zEntities[j].sState = "Statue"
        gzTextVar.zEntities[l].bIsCrippled = false
        gzTextVar.zEntities[l].sQueryPicture = "Root/Images/DollManor/Characters/LaurenStone"
        gzTextVar.zEntities[l].sState = "Statue"
        
        local fOldX, fOldY, fOldZ = fnGetRoomPosition(gzTextVar.zEntities[j].sLocation)
        local fX, fY, fZ = fnGetRoomPosition("Stone Gallery")
        TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[j].sIndicatorName, fX, fY, fZ)
        TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[l].sIndicatorName, fX, fY, fZ)
    
        --TF sequence.
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "You enter the gallery of statues. The light flashes brightly, and your true form is revealed." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "One of the intruders tries to run. You grab both of them by their collars and hold. You are stronger than they are." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "THEY MUST GAZE INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "The intruders shut their eyes. You hold firm. They must gaze into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "'Please look, Jessie. It is wonderful.' you say. The words leave your mouth, but they are not yours. The light created them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "The intruder opens her eyes for just a moment. The light flows into her. She panics but continues to gaze into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "SHE MUST GAZE INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You hold the intruder. She looks into the light. You hold the intruder. She looks into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Slowly, she fights you less. She is almost not resisting." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "SHE MUST GAZE INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieStoneTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "The intruder has gazed into the light. She begins changing. She becomes rigid. She becomes hardened. She becomes stone." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iJessieIndex, 4, 3, ciCodeUnfriendly)
        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieStoneTF1", ciImageLayerDefault)
        TL_SetProperty("Append", "The flesh vanishes, replaced by stone. She is a statue. You are a statue. You, and she, will obey." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieStone", ciImageLayerDefault)
        TL_SetProperty("Append", "HE MUST GAZE INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The new statue moves to the other side of the intruder. She holds him firmly, as you do. She is preparing to force him to gaze into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenGeneralTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "The intruder tries to keep his eyes shut. He is fighting back tears. He must gaze into the light. You obey. Jessie obeys." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "HE MUST GAZE INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "'Look into the light, Lauren,' you whisper." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieStone", ciImageLayerDefault)
        TL_SetProperty("Append", "'I loved it. I know you'll love it. You'll never want to stop,' the other statue whispers." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "'Do it for me, please?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenGeneralTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "Swallowing his tears, the intruder opens his eyes. The light flows into him. He continues to gaze into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You hold the intruder. He looks into the light. You hold the intruder. He looks into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "His mind grows distant. His eyes grow dim. He gazes into the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "HE MUST GAZE INTO THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenStoneTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "The intruder has gazed into the light. He begins changing. His skin becomes stone. His muscles become stone. His mind becomes stone. His mind becomes empty." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "He quietly allows the changes. He is no longer crying. His eyes have hardened. He is not an intruder." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iLaurenIndex, 5, 3, ciCodeFriendly)
        TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenStoneTF1", ciImageLayerDefault)
        TL_SetProperty("Append", "The intruder is emptied. The intruder is stone. He is a statue. You are a statue. He will obey the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenStone", ciImageLayerDefault)
        TL_SetProperty("Append", "The light floods into him. It floods into Jessie. It floods into you. TAKE YOUR PLACE, it commands you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "You and the statues make your way to the rear of the gallery. You stand there and wait for your next instructions." .. gsDE)
        TL_SetProperty("Create Blocker")
            
        TL_SetProperty("Append", "WAIT. The three statues wait. And wait. And wait..." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        --Achievement
        Steam_UnlockAchievement(gciAchievement_NoFightIt)
        Steam_UnlockAchievement(gciAchievement_ItDoesntHurt)
        
        LM_ExecuteScript(fnResolvePath() .. "500 Ending Handler.lua")
        
    end
end
