--[Function: fnCutsceneInstruction]
--Creates and registers a cutscene event using the provided instruction string.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneInstruction([[io.write("Example instruction.\n")]])
function fnCutsceneInstruction(sInstruction)

	--Argument check.
	if(sInstruction == nil) then return end
	
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("AutoInstruction")
		REvent_SetProperty("InstructionExec", sInstruction)
	DL_PopActiveObject()

end