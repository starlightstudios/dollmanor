fnGetRoomPosition = function(sRoomName)
   
    if(sRoomName == nil) then return 0, 0, 0 end
    
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomName) then
            local fX = gzTextVar.zRoomList[i].fRoomX
            local fY = gzTextVar.zRoomList[i].fRoomY
            local fZ = gzTextVar.zRoomList[i].fRoomZ
            return fX, fY, fZ
        end
    end
    
    return 0, 0, 0
end