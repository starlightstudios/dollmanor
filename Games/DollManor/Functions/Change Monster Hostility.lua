-- |[Change Monster Hostility]|
--By default, monsters have red hostility indicators. This script can turn them on or off if the
-- player changes alignment.

--Argument Listing:
-- 0: iMonsterHostile - 1 if monsters are hostile, 0 if not.
-- 1: iJessieHostile - 1 if Jessie is flagged as hostile, 0 if not.
-- 2: iLaurenHostile - 1 if Lauren is flagged as hostile, 0 if not.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iMonsterHostile = tonumber(LM_GetScriptArgument(0))
local iJessieHostile = tonumber(LM_GetScriptArgument(0))
local iLaurenHostile = tonumber(LM_GetScriptArgument(0))

-- |[Iterate]|
--Iterate across all entities.
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    -- |[Special Entities]|
    --Jessie.
    if(gzTextVar.zEntities[i].sDisplayName == "Jessie") then
        local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
        if(iJessieHostile == 0) then
            gzTextVar.zEntities[i].iIndicatorH = 0
            TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, -1, -1)
        else
            gzTextVar.zEntities[i].iIndicatorH = 1
            TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, ciHostilityX_Enemy, ciHostilityY_Enemy)
        end
        
    --Lauren.
    elseif(gzTextVar.zEntities[i].sDisplayName == "Lauren") then
        local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
        if(iLaurenHostile == 0) then
            gzTextVar.zEntities[i].iIndicatorH = 0
            TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, -1, -1)
        else
            gzTextVar.zEntities[i].iIndicatorH = 1
            TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, ciHostilityX_Enemy, ciHostilityY_Enemy)
        end
        
    --Pygmalie.
    elseif(gzTextVar.zEntities[i].sDisplayName == "Pygmalie") then
        --Pygmalie is never flagged as hostile.
        
    --Sarah.
    elseif(gzTextVar.zEntities[i].sDisplayName == "Sarah") then
        --Sarah doesn't even have an indicator.
        
    --Eileen.
    elseif(gzTextVar.zEntities[i].sDisplayName == "Eileen") then
        --Eileen is never hostile.
    
    --Everything else is a monster.
    else
        local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
        if(iMonsterHostile == 0) then
            gzTextVar.zEntities[i].iIndicatorH = 0
            TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, -1, -1)
        else
            
            --Normal enemy:
            if(gzTextVar.zEntities[i].bIndicatorRed == nil) then
                gzTextVar.zEntities[i].iIndicatorH = 1
                TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, ciHostilityX_Enemy, ciHostilityY_Enemy)
            
            --Titans are marked as bosses:
            else
                gzTextVar.zEntities[i].iIndicatorH = 2
                TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, ciHostilityX_Boss, ciHostilityY_Boss)
            end
        end
    end
end
