--[fnRebuildEntityVisibility]
--Figures out what rooms the player can see (because they are adjacent) and then marks those rooms as having entities or examinables
-- based on what's in them.
fnRebuildEntityVisibility = function()
    
    --Get the index of the room.
    local iRoomIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
    if(iRoomIndex == -1) then return end
    
    --Assemble the connections into an array.
    local iaArray = {}
    iaArray[1] = iRoomIndex
    iaArray[2] = gzTextVar.zRoomList[iRoomIndex].iMoveN
    iaArray[3] = gzTextVar.zRoomList[iRoomIndex].iMoveE
    iaArray[4] = gzTextVar.zRoomList[iRoomIndex].iMoveS
    iaArray[5] = gzTextVar.zRoomList[iRoomIndex].iMoveW
    iaArray[6] = gzTextVar.zRoomList[iRoomIndex].iMoveU
    iaArray[7] = gzTextVar.zRoomList[iRoomIndex].iMoveD
    
    --Now use all the connections and mark them likewise. Start with north.
    for i = 1, 6, 1 do
        
        --Ignore it if it's a -1.
        if(iaArray[i] == -1) then
            
        --Otherwise, set object flags.
        else
            --Get the room's variables.
            local iIndex = iaArray[i]
            local fX = gzTextVar.zRoomList[iIndex].fRoomX
            local fY = gzTextVar.zRoomList[iIndex].fRoomY
            local fZ = gzTextVar.zRoomList[iIndex].fRoomZ
            
            --Clear it.
            TL_SetProperty("Clear Room Flags", fX, fY, fZ)
            
            --Run across the objects. Objects always use the examinable script, everything else is an item.
            local bHasAnObject = false
            local bHasAnItem = false
            for p = 1, gzTextVar.zRoomList[iIndex].iObjectsTotal, 1 do
                if(gzTextVar.zRoomList[iIndex].zObjects[p].bCanBePickedUp == false) then
                    bHasAnObject = true
                else
                    bHasAnItem = true
                end
            end
            
            --Flags.
            TL_SetProperty("Set Room Flags", fX, fY, fZ, false, false, bHasAnObject, bHasAnItem)
        end
    end
            
    --Scan entities. All entities will mark their respective rooms, but the indicators won't be visible
    -- unless the entity is adjacent anyway.
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        
        --Make sure the entity is in a room that exists.
        local iMarkIndex = fnGetRoomIndex(gzTextVar.zEntities[i].sLocation)
        if(iMarkIndex ~= -1) then
        
            --Coordinates.
            local fX = gzTextVar.zRoomList[iMarkIndex].fRoomX
            local fY = gzTextVar.zRoomList[iMarkIndex].fRoomY
            local fZ = gzTextVar.zRoomList[iMarkIndex].fRoomZ
        
            --Call this entity's AI for a hostility check.
            gzTextVar.bLastHostilityCheck = false
        
            --If there's no handler, do nothing.
            if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
                
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
                fnClaygirl_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
                fnDoll_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
                fnTitan_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
                fnGoop_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
                fnStranger_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
                fnJessie_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
                fnLauren_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
                fnPygmalie_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
                fnSarah_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
                fnDollStationary_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
                fnDollStationaryCombat_HostilityCheck(i)
            end
            
            --Entity was hostile. Mark its room.
            if(gzTextVar.bLastHostilityCheck == true) then
                TL_SetProperty("Set Room Flags", fX, fY, fZ, true, false, false, false)
            
            --Not hostile. Mark as a friendly.
            else
                TL_SetProperty("Set Room Flags", fX, fY, fZ, false, true, false, false)
            
            end
        end
    end
end
