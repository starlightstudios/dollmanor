fnGetRoomIndex = function(sRoomName)
   
    if(sRoomName == nil) then return -1 end
    
    --If the first letter is "_", this is a X/Y/Z coordinate string.
    if(string.sub(sRoomName, 1, 1) == "_") then
        return fnGetRoomIndexFromString(sRoomName)
    end
    
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomName) then
            return i
        end
    end
    
    return -1
end