--[fnGenerateIndicatorName]
--Generates a unique name for indicators. Used for items.
fnGenerateIndicatorName = function()
    gzTextCon.iAutoGenIndicators = gzTextCon.iAutoGenIndicators + 1
    return "AutoIndicator" .. gzTextCon.iAutoGenIndicators
end
