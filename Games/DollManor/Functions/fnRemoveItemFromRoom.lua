--[fnRemoveItemFromRoom]
--Removes the item in slot iItem from the room in slot iRoom
fnRemoveItemFromRoom = function(iRoom, iItem)
    
    --Argument check.
    if(iRoom == nil) then return end
    if(iItem == nil) then return end
    if(iRoom < 1 or iRoom > gzTextVar.iRoomsTotal) then return end
    if(iItem < 1 or iItem > gzTextVar.zRoomList[iRoom].iObjectsTotal) then return end
    
    --Name of the room.
    local sRoomName = gzTextVar.zRoomList[iRoom].sName
    
    --If the item has an indicator name other than "Null", delete it.
    local sIndicatorName = gzTextVar.zRoomList[iRoom].zObjects[iItem].sIndicatorName
    if(sIndicatorName ~= "Null") then
        local fX, fY, fZ = fnGetRoomPosition(sRoomName)
        TL_SetProperty("Unregister Entity Indicator", fX, fY, fZ, sIndicatorName)
        gzTextVar.zRoomList[iRoom].zObjects[iItem].sIndicatorName = "Null"
    end
    
    --If the entity is the last one in the list, we can just nil it off and be done with it.
    if(iIndex == gzTextVar.zRoomList[iRoom].iObjectsTotal) then
        gzTextVar.zRoomList[iRoom].zObjects[gzTextVar.zRoomList[iRoom].iObjectsTotal] = nil
        gzTextVar.zRoomList[iRoom].iObjectsTotal = gzTextVar.zRoomList[iRoom].iObjectsTotal - 1
        return
    end
    
    --Otherwise, we need to copy-up.
    for i = iItem, gzTextVar.zRoomList[iRoom].iObjectsTotal, 1 do
        gzTextVar.zRoomList[iRoom].zObjects[i] = gzTextVar.zRoomList[iRoom].zObjects[i + 1]
    end
    
    --Now deallocate the last one and decrement.
    gzTextVar.zRoomList[iRoom].zObjects[gzTextVar.zRoomList[iRoom].iObjectsTotal] = nil
    gzTextVar.zRoomList[iRoom].iObjectsTotal = gzTextVar.zRoomList[iRoom].iObjectsTotal - 1
end