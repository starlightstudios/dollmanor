-- |[fnRemoveItemFromInventory]|
--Removes the item in slot iItem from the player's inventory.
fnRemoveItemFromInventory = function(iItem)
    
    --Argument check.
    if(iItem == nil) then return end
    if(iItem < 1 or iItem > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --If the item has a stack, we can just decrement the stack count.
    if(gzTextVar.gzPlayer.zaItems[iItem].iStackSize > 1) then
        gzTextVar.gzPlayer.zaItems[iItem].iStackSize = gzTextVar.gzPlayer.zaItems[iItem].iStackSize - 1
        return
    end
    
    --If the entity is the last one in the list, we can just nil it off and be done with it.
    if(iIndex == gzTextVar.gzPlayer.iItemsTotal) then
        gzTextVar.gzPlayer.zaItems[gzTextVar.gzPlayer.iItemsTotal] = nil
        gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal - 1
        return
    end
    
    --Otherwise, we need to copy-up.
    for i = iItem, gzTextVar.gzPlayer.iItemsTotal, 1 do
        gzTextVar.gzPlayer.zaItems[i] = gzTextVar.gzPlayer.zaItems[i + 1]
    end
    
    --Now deallocate the last one and decrement.
    gzTextVar.gzPlayer.zaItems[gzTextVar.gzPlayer.iItemsTotal] = nil
    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal - 1
end