--[ ================================= fnMarkMinimapForPosition ================================== ]
--Base vision function.
fnRunVisibility = function(sLocation, iMarkMode)
    
    --[Setup]
    --Argument check.
    if(sLocation == nil) then return end
    
    --Get the index of the room.
    local iRoomIndex = fnGetRoomIndex(sLocation)
    if(iRoomIndex == -1) then return end
    
    --If we're looking for player hostility:
    if(iMarkMode == 2) then
    
        --In phase 1, hostility checks always flat-fail in the Main Hall. The player is effectively invisible.
        if(gzTextVar.iGameStage == 1 and gzTextVar.gzPlayer.sLocation == "Main Hall") then return end
    
        --In all phases, entities cannot see Pygmalie's Study.
        if(gzTextVar.gzPlayer.sLocation == "Pygmalie's Study") then return end
    
    end
    
    --We need the coordinates of the room.
    local fX = gzTextVar.zRoomList[iRoomIndex].fRoomX
    local fY = gzTextVar.zRoomList[iRoomIndex].fRoomY
    local fZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
    
    --[True Sight Bonus]
    --True-Sight. Allows extra spotting distance through "short" handlers.
    local iTrueSight = 0
    
    --True sight *only* affects the player!
    if(iMarkMode == 0 or iMarkMode == 1) then
        
        --Equipping a torch as a weapon gives +1 True Sight.
        if(gzTextVar.gzPlayer.zWeapon ~= nil and gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
            iTrueSight = iTrueSight + 1
        end
        
        --Other items may be added later
    end
    
    --[Marking Mode]
    --Mark this room as explored and visible.
    if(iMarkMode == 0) then
        gzTextVar.zRoomList[iRoomIndex].bIsExplored = true
        gzTextVar.zRoomList[iRoomIndex].bIsVisibleNow = true
        TL_SetProperty("Set Room Explored", fX, fY, fZ, true)
        TL_SetProperty("Set Room Visible", fX, fY, fZ, true)
        TL_SetProperty("Unregister Entity Indicator", fX, fY, fZ, "Sound")
        
    --Unmark room.
    elseif(iMarkMode == 1) then
        gzTextVar.zRoomList[iRoomIndex].bIsVisibleNow = false
        TL_SetProperty("Set Room Visible", fX, fY, fZ, false)
    
    --Room has player? Spot them.
    elseif(iMarkMode == 2) then
        if(sLocation == gzTextVar.gzPlayer.sLocation) then
            gzTextVar.bSpottedPlayer = true
            return
        end
    end
    
    --[ =================================== Simplified Sight ==================================== ]
    --You can see adjacent rooms and that's it. Go away.
    if(gzTextVar.bUseSimpleSight == true) then

        --Assemble the connections into an array.
        local iaArray = {}
        iaArray[1] = gzTextVar.zRoomList[iRoomIndex].iMoveN
        iaArray[2] = gzTextVar.zRoomList[iRoomIndex].iMoveE
        iaArray[3] = gzTextVar.zRoomList[iRoomIndex].iMoveS
        iaArray[4] = gzTextVar.zRoomList[iRoomIndex].iMoveW
        iaArray[5] = gzTextVar.zRoomList[iRoomIndex].iMoveU
        iaArray[6] = gzTextVar.zRoomList[iRoomIndex].iMoveD
        
        --Now use all the connections and mark them likewise. Start with north.
        for i = 1, 6, 1 do
            
            --A -1 can't be entered.
            if(iaArray[i] == -1) then
            
            --Otherwise, set explored/visible.
            else
                local iIndex = iaArray[i]
                fX = gzTextVar.zRoomList[iIndex].fRoomX
                fY = gzTextVar.zRoomList[iIndex].fRoomY
                fZ = gzTextVar.zRoomList[iIndex].fRoomZ
                
                --Mark this room as explored and visible.
                if(iMarkMode == 0) then
                    gzTextVar.zRoomList[iIndex].bIsExplored = true
                    gzTextVar.zRoomList[iIndex].bIsVisibleNow = true
                    TL_SetProperty("Set Room Explored", fX, fY, fZ, true)
                    TL_SetProperty("Set Room Visible", fX, fY, fZ, true)
                    TL_SetProperty("Unregister Entity Indicator", fX, fY, fZ, "Sound")
                    
                --Unmark room.
                elseif(iMarkMode == 1) then
                    gzTextVar.zRoomList[iIndex].bIsVisibleNow = false
                    TL_SetProperty("Set Room Visible", fX, fY, fZ, false)
    
                --Room has player? Spot them.
                elseif(iMarkMode == 2) then
                    if(gzTextVar.zRoomList[iIndex].sName == gzTextVar.gzPlayer.sLocation) then
                        gzTextVar.bSpottedPlayer = true
                        return
                    end
                end
            end
        end
        
        --Reset.
        fX = gzTextVar.zRoomList[iRoomIndex].fRoomX
        fY = gzTextVar.zRoomList[iRoomIndex].fRoomY
        fZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
        
        --Scan adjacent tiles for unenterable rooms. They become visible.
        for i = 1, ciLookupsTotal, 1 do
            
            --Make sure the index exists.
            if(gzTextVar.zRoomLookup[fX+ciLookups[i][1]][fY+ciLookups[i][2]][fZ] ~= nil and gzTextVar.zRoomLookup[fX+ciLookups[i][1]][fY+ciLookups[i][2]][fZ] ~= -1) then
                
                --Check boundaries on the room.
                local iAdjacentIndex = gzTextVar.zRoomLookup[fX+ciLookups[i][1]][fY+ciLookups[i][2]][fZ]
                if(iAdjacentIndex >= 1 and iAdjacentIndex <= gzTextVar.iRoomsTotal) then
                    
                    --If the room is unenterable, we can see it.
                    if(gzTextVar.zRoomList[iAdjacentIndex].bIsUnenterable) then
                        local fNewX = gzTextVar.zRoomList[iAdjacentIndex].fRoomX
                        local fNewY = gzTextVar.zRoomList[iAdjacentIndex].fRoomY
                        local fNewZ = gzTextVar.zRoomList[iAdjacentIndex].fRoomZ
                        
                        --Mark this room as explored and visible.
                        if(iMarkMode == 0) then
                            gzTextVar.zRoomList[iIndex].bIsExplored = true
                            gzTextVar.zRoomList[iIndex].bIsVisibleNow = true
                            TL_SetProperty("Set Room Explored", fNewX, fNewY, fNewZ, true)
                            TL_SetProperty("Set Room Visible", fNewX, fNewY, fNewZ, true)
                            TL_SetProperty("Unregister Entity Indicator", fNewX, fNewY, fNewZ, "Sound")
        
                        --Unmark room.
                        elseif(iMarkMode == 1) then
                            gzTextVar.zRoomList[iIndex].bIsVisibleNow = false
                            TL_SetProperty("Set Room Visible", fNewX, fNewY, fNewZ, false)
    
                        --Room has player? Spot them.
                        elseif(iMarkMode == 2) then
                            if(gzTextVar.zRoomList[iIndex].sName == gzTextVar.gzPlayer.sLocation) then
                                gzTextVar.bSpottedPlayer = true
                                return
                            end
                        end
                    end
                end
            end
        end
    
    --[ ===================================== Complex Sight ===================================== ]
    --You can see until you hit a wall. Max sight range is 3.
    else
    
        --Iterate across the premade lookups.
        for i = 1, ciVisLookupsTotal, 1 do
    
            --Backup position copy.
            local iRayTrueSight = iTrueSight
            local fRayX = gzTextVar.zRoomList[iRoomIndex].fRoomX
            local fRayY = gzTextVar.zRoomList[iRoomIndex].fRoomY
            local fRayZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
            local iRayRoom = gzTextVar.zRoomLookup[fRayX][fRayY][fRayZ]
            
            --Flags.
            local bIsStraightRay = true
            local bMustBeStraightRay = false
            local sFirstString = czVisLookups[i][1]
            local sPrevString = czVisLookups[i][1]
    
            --Begin iterating across the lookup.
            local iSize = #czVisLookups[i]
            for p = 1, #czVisLookups[i], 1 do
    
                --Flag.
                local bDisallow = false
                local bStopAfterNext = false
                local bMustBeUnenterable = false
                local sString = czVisLookups[i][p]
    
                --If the ray changed direction, it loses straightness.
                if(sString ~= sPrevString) then
                    bIsStraightRay = false
                end
    
                --If the ray isn't straight but has to be, stop.
                if(bIsStraightRay == false and bMustBeStraightRay == true) then
                    break
                end
    
                --[North]
                if(sString == "N") then
                    
                    --Vis override: Always.
                    if(gzTextVar.zRoomList[iRayRoom].iVisOverrideN == gzTextCon.iAlwaysVisible and iMarkMode ~= 2) then
                    
                    --Vis override: Always, but acts like a narrow door.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideN == gzTextCon.iAlwaysVisibleNarrow and iMarkMode ~= 2) then
                        bMustBeStraightRay = true
                    
                    --Vis override: Short. Stops the ray after marking the next room.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideN == gzTextCon.iAlwaysVisibleShort and iMarkMode ~= 2) then
                        bStopAfterNext = true
                        
                        --True-sight override.
                        if(iRayTrueSight > 0) then
                            iRayTrueSight = iRayTrueSight - 1
                            bStopAfterNext = false
                        end
                    
                    --Vis override: Never.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideN == gzTextCon.iNeverVisible and iMarkMode ~= 2) then
                        bDisallow = true
                    
                    --Unenterable room. Blocks vision.
                    elseif(gzTextVar.zRoomList[iRayRoom].iMoveN == -1) then
                        bMustBeUnenterable = true
                    
                    --Closed door.
                    elseif(gzTextVar.zRoomList[iRayRoom].sDoorN ~= nil and string.sub(gzTextVar.zRoomList[iRayRoom].sDoorN, 1, 1) == "C")  then
                        
                        --If the door is see-through, and this is NOT an enemy trying to spot the player:
                        if(gzTextVar.zRoomList[iRayRoom].bDoorSeethroughN == false or iMarkMode == 2) then
                            bMustBeUnenterable = true
                        end
                    end
                    
                    --Narrow movement area, forces straight rays.
                    if(gzTextVar.zRoomList[iRayRoom].bMoveOpenN == false) then
                        bMustBeStraightRay = true
                    end
                    
                    --Move the ray.
                    fRayY = fRayY - 1.0
                
                --[East]
                elseif(sString == "E") then
                    
                    --Vis override: Always.
                    if(gzTextVar.zRoomList[iRayRoom].iVisOverrideE == gzTextCon.iAlwaysVisible and iMarkMode ~= 2) then
                    
                    --Vis override: Always, but acts like a narrow door.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideE == gzTextCon.iAlwaysVisibleNarrow and iMarkMode ~= 2) then
                        bMustBeStraightRay = true
                    
                    --Vis override: Short. Stops the ray after marking the next room.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideE == gzTextCon.iAlwaysVisibleShort and iMarkMode ~= 2) then
                        bStopAfterNext = true
                        
                        --True-sight override.
                        if(iRayTrueSight > 0) then
                            iRayTrueSight = iRayTrueSight - 1
                            bStopAfterNext = false
                        end
                    
                    --Vis override: Never.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideE == gzTextCon.iNeverVisible and iMarkMode ~= 2) then
                        bDisallow = true
                    
                    --Unenterable room.
                    elseif(gzTextVar.zRoomList[iRayRoom].iMoveE == -1) then
                        bMustBeUnenterable = true
                    
                    --Closed door.
                    elseif(gzTextVar.zRoomList[iRayRoom].sDoorE ~= nil and string.sub(gzTextVar.zRoomList[iRayRoom].sDoorE, 1, 1) == "C")  then
                    
                        --Door is see-through, and this is NOT an enemy trying to spot the player:
                        if(gzTextVar.zRoomList[iRayRoom].bDoorSeethroughE == false or iMarkMode == 2) then
                            bMustBeUnenterable = true
                        end
                    end
                    
                    --Narrow movement area, forces straight rays.
                    if(gzTextVar.zRoomList[iRayRoom].bMoveOpenE == false) then
                        bMustBeStraightRay = true
                    end
                    
                    --Move the ray.
                    fRayX = fRayX + 1.0
                
                --[South]
                elseif(sString == "S") then
                    
                    --Vis override: Always.
                    if(gzTextVar.zRoomList[iRayRoom].iVisOverrideS == gzTextCon.iAlwaysVisible and iMarkMode ~= 2) then
                    
                    --Vis override: Always, but acts like a narrow door.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideS == gzTextCon.iAlwaysVisibleNarrow and iMarkMode ~= 2) then
                        bMustBeStraightRay = true
                    
                    --Vis override: Short. Stops the ray after marking the next room.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideS == gzTextCon.iAlwaysVisibleShort and iMarkMode ~= 2) then
                        bStopAfterNext = true
                        
                        --True-sight override.
                        if(iRayTrueSight > 0) then
                            iRayTrueSight = iRayTrueSight - 1
                            bStopAfterNext = false
                        end
                    
                    --Vis override: Never.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideS == gzTextCon.iNeverVisible and iMarkMode ~= 2) then
                        bDisallow = true
                        
                    --Unenterable.
                    elseif(gzTextVar.zRoomList[iRayRoom].iMoveS == -1) then
                        bMustBeUnenterable = true
                    
                    --Closed door.
                    elseif(gzTextVar.zRoomList[iRayRoom].sDoorS ~= nil and string.sub(gzTextVar.zRoomList[iRayRoom].sDoorS, 1, 1) == "C")  then
                        
                        --If the door is see-through, and this is NOT an enemy trying to spot the player:
                        if(gzTextVar.zRoomList[iRayRoom].bDoorSeethroughS == false or iMarkMode == 2) then
                            bMustBeUnenterable = true
                        end
                    end
                    
                    --Narrow movement area, forces straight rays.
                    if(gzTextVar.zRoomList[iRayRoom].bMoveOpenS == false) then
                        bMustBeStraightRay = true
                    end
                    
                    --Move the ray.
                    fRayY = fRayY + 1.0
                
                --[West]
                elseif(sString == "W") then
                    
                    --Vis override: Always.
                    if(gzTextVar.zRoomList[iRayRoom].iVisOverrideW == gzTextCon.iAlwaysVisible and iMarkMode ~= 2) then
                    
                    --Vis override: Always, but acts like a narrow door.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideW == gzTextCon.iAlwaysVisibleNarrow and iMarkMode ~= 2) then
                        bMustBeStraightRay = true
                    
                    --Vis override: Short. Stops the ray after marking the next room.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideW == gzTextCon.iAlwaysVisibleShort and iMarkMode ~= 2) then
                        bStopAfterNext = true
                        
                        --True-sight override.
                        if(iRayTrueSight > 0) then
                            iRayTrueSight = iRayTrueSight - 1
                            bStopAfterNext = false
                        end
                    
                    --Vis override: Never.
                    elseif(gzTextVar.zRoomList[iRayRoom].iVisOverrideW == gzTextCon.iNeverVisible and iMarkMode ~= 2) then
                        bDisallow = true
                    
                    --Unenterable.
                    elseif(gzTextVar.zRoomList[iRayRoom].iMoveW == -1) then
                        bMustBeUnenterable = true
                    
                    --Closed door.
                    elseif(gzTextVar.zRoomList[iRayRoom].sDoorW ~= nil and string.sub(gzTextVar.zRoomList[iRayRoom].sDoorW, 1, 1) == "C")  then
                        
                        --If the door is see-through, and this is NOT an enemy trying to spot the player:
                        if(gzTextVar.zRoomList[iRayRoom].bDoorSeethroughW == false or iMarkMode == 2) then
                            bMustBeUnenterable = true
                        end
                    end
                    
                    --Narrow movement area, forces straight rays.
                    if(gzTextVar.zRoomList[iRayRoom].bMoveOpenW == false) then
                        bMustBeStraightRay = true
                    end
                    
                    --Move the ray.
                    fRayX = fRayX - 1.0
                    
                end
                
                --[Room Verify]
                --Get the room at the given position. If it doesn't exist, stop here.
                local o = gzTextVar.zRoomLookup[fRayX][fRayY][fRayZ]
                
                --Check if the room is out of range.
                if(o < 1 or o > gzTextVar.iRoomsTotal) then break end
                
                --[Visibility Checking]
                --Disallow. A flag was set to never allow visibility. Stop here.
                if(bDisallow) then break end
                
                --Unenterable room that does not block LOS? Bypass the usual checks.
                if(gzTextVar.zRoomList[o].bIsUnenterable and gzTextVar.zRoomList[o].bUnenterableBlocksVision == false) then
                    bMustBeStraightRay = false
                    bMustBeUnenterable = false
                    
                --Check if the room has blocks on its reverse edge. If so, straight rays cannot penetrate them.
                else
                    if(sString == "N") then
                        
                        if(gzTextVar.zRoomList[o].bMoveOpenS == false) then
                            bMustBeStraightRay = true
                        end
                    elseif(sString == "E") then
                        if(gzTextVar.zRoomList[o].bMoveOpenW == false) then
                            bMustBeStraightRay = true
                        end
                    elseif(sString == "S") then
                        if(gzTextVar.zRoomList[o].bMoveOpenN == false) then
                            bMustBeStraightRay = true
                        end
                    elseif(sString == "W") then
                        if(gzTextVar.zRoomList[o].bMoveOpenE == false) then
                            bMustBeStraightRay = true
                        end
                    end
    
                    --If the ray isn't straight but has to be, stop.
                    if(bIsStraightRay == false and bMustBeStraightRay == true) then
                        break
                    end
                    
                    --If the room must be unenterable, fail if it wasn't.
                    if(bMustBeUnenterable and gzTextVar.zRoomList[o].bIsUnenterable == false) then
                        break
                    end
                
                end
            
                --Otherwise, mark it as visible.
                if(true) then
                
                    local fNewX = gzTextVar.zRoomList[o].fRoomX
                    local fNewY = gzTextVar.zRoomList[o].fRoomY
                    local fNewZ = gzTextVar.zRoomList[o].fRoomZ
            
                    --Mark this room as explored and visible.
                    if(iMarkMode == 0) then
                        gzTextVar.zRoomList[o].bIsExplored = true
                        gzTextVar.zRoomList[o].bIsVisibleNow = true
                        TL_SetProperty("Set Room Explored", fNewX, fNewY, fNewZ, true)
                        TL_SetProperty("Set Room Visible", fNewX, fNewY, fNewZ, true)
                        TL_SetProperty("Unregister Entity Indicator", fNewX, fNewY, fNewZ, "Sound")
                        
                        --Vertical visibility. If this value is above zero, we can see rooms below this one.
                        if(gzTextVar.zRoomList[o].iVisDown > 0) then
                            for q = 1, gzTextVar.zRoomList[o].iVisDown, 1 do
                                local fDownZ = fNewZ + q
                                local iIndex = gzTextVar.zRoomLookup[fNewX][fNewY][fDownZ]
                                if(iIndex ~= nil and iIndex >= 0 and iIndex < gzTextVar.iRoomsTotal) then
                                    gzTextVar.zRoomList[iIndex].bIsExplored = true
                                    gzTextVar.zRoomList[iIndex].bIsVisibleNow = true
                                    TL_SetProperty("Set Room Explored", fNewX, fNewY, fDownZ, true)
                                    TL_SetProperty("Set Room Visible", fNewX, fNewY, fDownZ, true)
                                    TL_SetProperty("Unregister Entity Indicator", fNewX, fNewY, fDownZ, "Sound")
                                end
                            end
                        end
        
                    --Unmark room.
                    elseif(iMarkMode == 1) then
                        gzTextVar.zRoomList[o].bIsVisibleNow = false
                        TL_SetProperty("Set Room Visible", fNewX, fNewY, fNewZ, false)
                        
                        --Vertical visibility. If this value is above zero, we can see rooms below this one.
                        if(gzTextVar.zRoomList[o].iVisDown > 0) then
                            for q = 1, gzTextVar.zRoomList[o].iVisDown, 1 do
                                local fDownZ = fNewZ + q
                                local iIndex = gzTextVar.zRoomLookup[fNewX][fNewY][fDownZ]
                                if(iIndex ~= nil and iIndex >= 0 and iIndex < gzTextVar.iRoomsTotal) then
                                    gzTextVar.zRoomList[iIndex].bIsVisibleNow = false
                                    TL_SetProperty("Set Room Visible", fNewX, fNewY, fDownZ, false)
                                end
                            end
                        end
    
                    --Room has player? Spot them.
                    elseif(iMarkMode == 2) then
                        if(gzTextVar.zRoomList[o].sName == gzTextVar.gzPlayer.sLocation) then
                            gzTextVar.bSpottedPlayer = true
                            return
                        end
                    end
                    
                    --Change the ray room.
                    iRayRoom = o
                end
                
                --Stop-After-Next. Used when "Short" is used to reduce visibility.
                if(bStopAfterNext) then
                    break
                end
                
                --Update the previous ray direction.
                sPrevString = czVisLookups[i][1]
                
            end
        end
    end
    
    --If in marking mode, and we spot all the unenterable rooms surrounding another room, then 'explore' that room. This helps
    -- to fill the map out a bit, but serves no gameplay purpose.
    if(iMarkMode == 0) then
        
        --Starting position.
        local fX = gzTextVar.zRoomList[iRoomIndex].fRoomX
        local fY = gzTextVar.zRoomList[iRoomIndex].fRoomY
        local fZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
        
        --clamp variables.
        local fXLo = fX - 4
        local fXHi = fX + 4
        local fYLo = fY - 4
        local fYHi = fY + 4
        
        --Edge checkers.
        if(fXLo < gzTextVar.iRoomWst) then fXLo = gzTextVar.iRoomWst end
        if(fXHi > gzTextVar.iRoomEst) then fXHi = gzTextVar.iRoomEst end
        if(fYLo < gzTextVar.iRoomNth) then fYLo = gzTextVar.iRoomNth end
        if(fYHi > gzTextVar.iRoomSth) then fYHi = gzTextVar.iRoomSth end
                
        --Iterate.
        local z = fZ
        for x = fXLo, fXHi, 1 do
            for y = fYLo, fYHi, 1 do
                
                --Get the room's index.
                local iIndex = gzTextVar.zRoomLookup[x][y][z]
                
                --Room is unoccupied? Ignore it.
                if(iIndex == -1) then
                
                --Occupied. Enterable? Ignore it.
                elseif(gzTextVar.zRoomList[iIndex].bIsUnenterable == false) then
                
                --Occupied, unenterable.
                else
                
                    --Check adjacencies. If this tile is in a corner between two revealed tiles, reveal it.
                    local bHasNS = false
                    local bHasEW = false
                    
                    --North check.
                    local iNorthIndex = gzTextVar.zRoomLookup[x][y-1][z]
                    if(iNorthIndex ~= -1 and (gzTextVar.zRoomList[iNorthIndex].bIsUnenterable and gzTextVar.zRoomList[iNorthIndex].bIsExplored)) then
                        bHasNS = true
                    end
                    
                    --Check east.
                    local iEastIndex = gzTextVar.zRoomLookup[x+1][y][z]
                    if(iEastIndex ~= -1 and (gzTextVar.zRoomList[iEastIndex].bIsUnenterable and gzTextVar.zRoomList[iEastIndex].bIsExplored)) then
                        bHasEW = true
                    end
                    
                    --Check south.
                    local iSouthIndex = gzTextVar.zRoomLookup[x][y+1][z]
                    if(iSouthIndex ~= -1 and (gzTextVar.zRoomList[iSouthIndex].bIsUnenterable and gzTextVar.zRoomList[iSouthIndex].bIsExplored)) then
                        bHasNS = true
                    end
                    
                    --Check west.
                    local iWestIndex = gzTextVar.zRoomLookup[x-1][y][z]
                    if(iWestIndex ~= -1 and (gzTextVar.zRoomList[iWestIndex].bIsUnenterable and gzTextVar.zRoomList[iWestIndex].bIsExplored)) then
                        bHasEW = true
                    end
                
                    --All checks passed, reveal the room.
                    if(bHasEW and bHasNS) then
                        gzTextVar.zRoomList[iIndex].bIsExplored = true
                        TL_SetProperty("Set Room Explored", x, y, z, true)
                    end
                
                end
            end
        end
    end

end

--Marks the given location on the minimap as explored and visible, and marks all the rooms adjacent as explored and visible.
-- This is used to make the fog of war dynamically lift as the player explores.
fnMarkMinimapForPosition = function(sLocation)
    fnRunVisibility(sLocation, 0)
end

--Unmark. Does the same as above but unmarks the room and adjacent rooms. This is used before movement to recompute visibility.
-- It does not affect explored state, only visible.
fnUnmarkMinimapForPosition = function(sLocation)
    
    --Never does anything if global sight is on.
    if(gzTextVar.bHasGlobalSight) then return end
    
    --Run the routine.
    fnRunVisibility(sLocation, 1)
end
