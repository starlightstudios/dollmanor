--[ ================================== Move Entity To Location ================================== ]
--Script subroutine used in cutscenes. Causes a given entity to move to a given room. Allows the 
-- cutscene to show this happening in-stride instead of instantly.
--Pass -1 for the entity's index to move the player.

--Argument Listing:
-- 0: iIndex - Index of the moving entity. -1 is the player.
-- 1: sDestination - Target room's name. Must exist, if not this script does nothing.

--Arg check.
local iRequiredArgs = 2
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iIndex = tonumber(LM_GetScriptArgument(0))
local sDestination = LM_GetScriptArgument(1)

--[Destination Resolver]
--Make sure the target exists and get its room index.
local iDestIndex = fnGetRoomIndex(sDestination)
if(iDestIndex == -1) then return end

--[Player Movement]
if(iIndex == -1) then
    
    --Get positions.
    local fOldX, fOldY, fOldZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    local fX, fY, fZ = fnGetRoomPosition(sDestination)

    --Unmark the previous location, and mark this one as visible.
    fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    fnMarkMinimapForPosition(sDestination)

    --Move the player.
    gzTextVar.gzPlayer.sPrevLocation = gzTextVar.gzPlayer.sLocation
    gzTextVar.gzPlayer.sLocation = sDestination
    TL_SetProperty("Set Map Focus", -fX, -fY)

    --[Stranger Grouping]
    --Check if the Stranger grouping is different for this room. This only applies to the player.
    -- While this could theoretically cause problems if the player "jumps" a grouping edge, this
    -- never matters in practice since this script only fires on a player who is TF'd or about to
    -- be and therefore not an issue for the Stranger.
    local sOldGrouping = gzTextVar.zStranger.sPlayerCurrentGrouping
    local sNewGrouping = gzTextVar.zRoomList[iDestIndex].sStrangerGroup

    --New Grouping is "Null", so do nothing.
    if(sNewGrouping == "Null") then

    --New Grouping is the same as the old one, so do nothing.
    elseif(sOldGrouping == sNewGrouping) then

    --New Grouping is different, update it.
    else
        gzTextVar.zStranger.sPlayerCurrentGrouping = sNewGrouping
    end

    --[Indicators]
    --Change the indicator's position.
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, "Player", fX, fY, fZ)
    TL_SetProperty("Player World Position", fX, fY, fZ)

    --Rendering handler. Upper floors render to the ground. Basement does not render under other stuff.
    TL_SetProperty("Set Rendering Z Level", fZ)
    if(fZ < 0) then
        TL_SetProperty("Set Under Rendering Z Level", fZ+1, 0)
    else
        TL_SetProperty("Set Under Rendering Z Level", ciDoNotRenderUnder, ciDoNotRenderUnder)
    end

    --Reresolve fading cases.
    TL_SetProperty("Reresolve Fades")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--[Normal Entity Movement]
else

    --Make sure the entity exists.
    if(iIndex < 1 or iIndex > gzTextVar.zEntitiesTotal) then return end
    
    --Get positions.
    local fOldX, fOldY, fOldZ = fnGetRoomPosition(gzTextVar.zEntities[iIndex].sLocation)
    local fX, fY, fZ = fnGetRoomPosition(sDestination)

    --Move the entity.
    gzTextVar.zEntities[iIndex].sLocation = sDestination

    --Change the indicator's position.
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[iIndex].sIndicatorName, fX, fY, fZ)

end
