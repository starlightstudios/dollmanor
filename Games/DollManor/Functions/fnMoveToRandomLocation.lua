--[fnMoveToRandomLocation]
--Causes the requested entity to move to a random location. Certain flags can be set to narrow the location. A list
-- of all valid moves must be provided.
--The name of the location will be returned, or "Null" if none is found.
fnMoveToRandomLocation = function(iEntitySlot, saValidLocations)
    
    --Arg check.
    if(iEntitySlot       == nil) then return "Null" end
    if(saValidLocations  == nil) then return "Null" end
    
    --Error check.
    if(gzTextVar.saFleeLocations == nil or #gzTextVar.saFleeLocations < 1) then return "Null" end
    
    --Special flags.
    local bDisallowJessie = false
    local bDisallowLauren = false
    local bDisallowPlayer = true --Always disallowed.
    
    --Quick-access flags.
    local i = gzTextVar.iJessieIndex
    local p = gzTextVar.iLaurenIndex
    
    --Jessie checker. If the entity moving is Jessie...
    if(iEntitySlot == i) then
    
        --If Jessie is a human...
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --If Lauren is also a human, then we cannot land on a Lauren room.
            if(gzTextVar.zEntities[p].sState == "Human") then
                bDisallowLauren = true
            end
    
        end
    end
    
    --Lauren checker. If the entity moving is Lauren...
    if(iEntitySlot == p) then
    
        --If Lauren is a human...
        if(gzTextVar.zEntities[p].sState == "Human") then
    
            --If Jessie is also a human, then we cannot land on a Jessie room.
            if(gzTextVar.zEntities[i].sState == "Human") then
                bDisallowJessie = true
            end
    
        end
    end
    
    --Rolling loop. We have 100 tries to find a legal room. It is possible that this will fail if, 
    -- for example, no valid flee locations were set.
    local iTriesLeft = 100
    while(iTriesLeft > 0) do
        
        --Set.
        local bFail = false
        
        --Roll.
        local iRoll = LM_GetRandomNumber(1, #gzTextVar.saFleeLocations)
        local iIndex = fnGetRoomIndex(gzTextVar.saFleeLocations[iRoll])
        
        --If the "Disallow Jessie" rule is set, the room cannot contain Jessie.
        if(bDisallowJessie) then
            if(gzTextVar.zEntities[i].sLocation == gzTextVar.zRoomList[iIndex].sName) then
                bFail = true
            end
        end
        
        --If the "Disallow Lauren" rule is set, the room cannot contain Lauren.
        if(bDisallowLauren) then
            if(gzTextVar.zEntities[p].sLocation == gzTextVar.zRoomList[iIndex].sName) then
                bFail = true
            end
        end
        
        --If the "Disallow Player" rule is set, the room cannot be the one the player is in.
        if(bDisallowPlayer) then
            if(gzTextVar.gzPlayer.sLocation == gzTextVar.zRoomList[iIndex].sName) then
                bFail = true
            end
        end
        
        --All checks passed. Return this entry.
        if(bFail == false) then
            return gzTextVar.zRoomList[iIndex].sName
        
        --Something failed. Give it anuvva go.
        else
            iTriesLeft = iTriesLeft - 1
        end
    end
    
    --Checks failed and no rooms were found. Return "Null".
    return "Null"
end