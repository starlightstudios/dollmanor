--[fnGetRoomIndexFromString]
--Given a string of the format "_3x9x1" translates this into room coordinates for the 3D map. Returns the index of the room in the global array.
fnGetRoomIndexFromString = function(sString)
    
    --Setup.
    local iLen = string.len(sString)
    
    --Variables
    local iXS = 2
    local iXE = 2
    local iYS = 2
    local iYE = 2
    local iZS = 2
    local iZE = 2
    local sXString = "0"
    local sYString = "0"
    local sZString = "0"
    
    --X version.
    for i = iXS, iLen, 1 do
        
        --Get the letter.
        local sLetter = string.sub(sString, i, i)
        
        --Letter is an "x". End the string here.
        if(sLetter == "x") then
            iXE = i - 1
            iYS = i + 1
            sXString = string.sub(sString, iXS, iXE)
            break
        end
    end
    
    --Y version.
    for i = iYS, iLen, 1 do
        
        --Get the letter.
        local sLetter = string.sub(sString, i, i)
        
        --Letter is an "x". End the string here.
        if(sLetter == "x") then
            iYE = i - 1
            iZS = i + 1
            sYString = string.sub(sString, iYS, iYE)
            break
        end
    end
    
    --Z version. The string is assumed to run from iZS to the end of the string.
    iZE = iLen
    sZString = string.sub(sString, iZS, iZE)
    
    --Range-check the three values.
    local iXIndex = tonumber(sXString)
    local iYIndex = tonumber(sYString)
    local iZIndex = tonumber(sZString)
    if(iXIndex < gzTextVar.iRoomWst   or iXIndex > gzTextVar.iRoomEst)   then return -1 end
    if(iYIndex < gzTextVar.iRoomNth   or iYIndex > gzTextVar.iRoomSth)   then return -1 end
    if(iZIndex < gzTextVar.iLowermost or iZIndex > gzTextVar.iUppermost) then return -1 end
    
    --Error checks passed. Return index.
    return gzTextVar.zRoomLookup[iXIndex][iYIndex][iZIndex]
    
end