--[fnMarkLockAsKnown]
--Given a room and direction, marks the lock as "known", placing an indicator next to it.
fnMarkLockAsKnown = function(iRoomIndex, iDoorDir)

    --[Arg Check]
    --Existence check.
    if(iRoomIndex == nil) then return end
    if(iDoorDir   == nil) then return end

    --Range check.
    if(iRoomIndex < 1 or iRoomIndex > gzTextVar.iRoomsTotal) then return end
    if(iDoorDir ~= gzTextCon.iDirNorth and iDoorDir ~= gzTextCon.iDirEast and iDoorDir ~= gzTextCon.iDirSouth and iDoorDir ~= gzTextCon.iDirWest) then return end

    --[Variables]
    local fModX = -1
    local fModY = -1
    local fModZ = -1
    local bIsSouth = false
    local sLockState = " Normal"

    --[North]
    if(iDoorDir == gzTextCon.iDirNorth) then
        
        --Door is nil. Nothing to mark.
        if(gzTextVar.zRoomList[iRoomIndex].sDoorN == nil) then return end
        
        --Get the destination. It has to mark its own south door.
        local sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveN
        if(sDestination == "Null") then return end
        
        --Range check.
        local iDestIndex = fnGetRoomIndex(sDestination)
        if(iDestIndex == -1) then return end
        
        --Flag it as known.
        gzTextVar.zRoomList[iDestIndex].bDoorSLockKnown = true
        
        --Store the state machine variables.
        fModX = gzTextVar.zRoomList[iDestIndex].fRoomX
        fModY = gzTextVar.zRoomList[iDestIndex].fRoomY
        fModZ = gzTextVar.zRoomList[iDestIndex].fRoomZ
        bIsSouth = true
        sLockState = string.sub(gzTextVar.zRoomList[iDestIndex].sDoorS, 2)
        
    --[East]
    elseif(iDoorDir == gzTextCon.iDirEast) then
        
        --Door is nil. Nothing to mark.
        if(gzTextVar.zRoomList[iRoomIndex].sDoorE == nil) then return end
        
        --Flag it as known.
        gzTextVar.zRoomList[iRoomIndex].bDoorELockKnown = true
        
        --Store the state machine variables.
        fModX = gzTextVar.zRoomList[iRoomIndex].fRoomX
        fModY = gzTextVar.zRoomList[iRoomIndex].fRoomY
        fModZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
        bIsSouth = false
        sLockState = string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorE, 2)
        
    --[South]
    elseif(iDoorDir == gzTextCon.iDirSouth) then
        
        --Door is nil. Nothing to mark.
        if(gzTextVar.zRoomList[iRoomIndex].sDoorS == nil) then return end
        
        --Flag it as known.
        gzTextVar.zRoomList[iRoomIndex].bDoorSLockKnown = true
        
        --Store the state machine variables.
        fModX = gzTextVar.zRoomList[iRoomIndex].fRoomX
        fModY = gzTextVar.zRoomList[iRoomIndex].fRoomY
        fModZ = gzTextVar.zRoomList[iRoomIndex].fRoomZ
        bIsSouth = true
        sLockState = string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorS, 2)
        
    --[West]
    elseif(iDoorDir == gzTextCon.iDirWest) then
        
        --Door is nil. Nothing to mark.
        if(gzTextVar.zRoomList[iRoomIndex].sDoorW == nil) then return end
        
        --Get the destination. It has to mark its own east door.
        local sDestination = gzTextVar.zRoomList[iRoomIndex].sMoveW
        if(sDestination == "Null") then return end
        
        --Range check.
        local iDestIndex = fnGetRoomIndex(sDestination)
        if(iDestIndex == -1) then return end
        
        --Flag it as known.
        gzTextVar.zRoomList[iDestIndex].bDoorELockKnown = true
        
        --Store the state machine variables.
        fModX = gzTextVar.zRoomList[iDestIndex].fRoomX
        fModY = gzTextVar.zRoomList[iDestIndex].fRoomY
        fModZ = gzTextVar.zRoomList[iDestIndex].fRoomZ
        bIsSouth = false
        sLockState = string.sub(gzTextVar.zRoomList[iDestIndex].sDoorE, 2)
        
    end

    --[Set Indicator]
    --Set door indicators by tile code.
    if(sLockState == " Normal") then
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
    elseif(sLockState == " Heart") then
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_Heart_X, gzTextVar.iLock_Heart_Y)
    elseif(sLockState == " Club") then
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_Club_X, gzTextVar.iLock_Club_Y)
    elseif(sLockState == " Diamond") then
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_Diamond_X, gzTextVar.iLock_Diamond_Y)
    elseif(sLockState == " Spade") then
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_Spade_X, gzTextVar.iLock_Spade_Y)
    elseif(sLockState == " Small") then
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_Small_X, gzTextVar.iLock_Small_Y)
    else
        TL_SetProperty("Set Room Door Indicator", fModX, fModY, fModZ, bIsSouth, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
    end
end
