fnGetIndexOfItemInInventory = function(sItemName)
   
    if(sItemName == nil) then return -1 end
    
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == sItemName) then
            return i
        end
    end
    
    return -1
end