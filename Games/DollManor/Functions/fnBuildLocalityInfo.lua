-- |[fnBuildLocalityInfo]|
--Builds information for the locality pane based on the player's current position.
fnBuildLocalityInfo = function()
    
    -- |[Setup]|
    --Clear the old data.
    TL_SetProperty("Clear Locality List")
    local uiNavFlags = 0
    
    --Set locality window to icons mode.
    TL_SetProperty("Set Locality Icon Flag", true)

    --Bases. These are always present.
    TL_SetProperty("Register Locality Entry As List With Image", "Commands",  "Root/Images/TxtAdv/Locality/Commands|Up",  "Root/Images/TxtAdv/Locality/Commands|Dn")
    TL_SetProperty("Register Locality Entry As List With Image", "Movement",  "Root/Images/TxtAdv/Locality/Move|Up",      "Root/Images/TxtAdv/Locality/Move|Dn")
    TL_SetProperty("Register Locality Entry As List With Image", "Doors",     "Root/Images/TxtAdv/Locality/Doors|Up",     "Root/Images/TxtAdv/Locality/Doors|Dn")
    TL_SetProperty("Register Locality Entry As List With Image", "Equipment", "Root/Images/TxtAdv/Locality/Equipment|Up", "Root/Images/TxtAdv/Locality/Equipment|Dn")
    TL_SetProperty("Register Locality Entry As List With Image", "Inventory", "Root/Images/TxtAdv/Locality/Inventory|Up", "Root/Images/TxtAdv/Locality/Inventory|Dn")
    TL_SetProperty("Register Locality Entry As List With Image", "Entities",  "Root/Images/TxtAdv/Locality/Entities|Up",  "Root/Images/TxtAdv/Locality/Entities|Dn")
    TL_SetProperty("Register Locality Entry As List With Image", "Objects",   "Root/Images/TxtAdv/Locality/Items|Up",     "Root/Images/TxtAdv/Locality/Items|Dn")

    --Current location.
    local sLocation = gzTextVar.gzPlayer.sLocation
    local iRoomIndex = fnGetRoomIndex(sLocation)
    if(iRoomIndex == -1) then return end
    
    -- |[Basic Commands]|
    TL_SetProperty("Register Locality Entry", "look", "Commands", "INSTANT")
    if(gzTextVar.bCanSpotClaygirls) then
        TL_SetProperty("Register Locality Entry", "search carefully", "Commands", "INSTANT")
    end
    TL_SetProperty("Register Locality Entry", "think", "Commands", "INSTANT")
    TL_SetProperty("Register Locality Entry", "wait", "Commands", "INSTANT")
    
    --Directions are based on what is available.
    if(gzTextVar.zRoomList[iRoomIndex].iMoveN ~= -1) then
        TL_SetProperty("Register Locality Entry", "move north", "Movement", "INSTANT")
        uiNavFlags = uiNavFlags + ciNavNorth
    end
    if(gzTextVar.zRoomList[iRoomIndex].iMoveE ~= -1) then
        TL_SetProperty("Register Locality Entry", "move east", "Movement", "INSTANT")
        uiNavFlags = uiNavFlags + ciNavEast
    end
    if(gzTextVar.zRoomList[iRoomIndex].iMoveS ~= -1) then
        TL_SetProperty("Register Locality Entry", "move south", "Movement", "INSTANT")
        uiNavFlags = uiNavFlags + ciNavSouth
    end
    if(gzTextVar.zRoomList[iRoomIndex].iMoveW ~= -1) then
        TL_SetProperty("Register Locality Entry", "move west", "Movement", "INSTANT")
        uiNavFlags = uiNavFlags + ciNavWest
    end
    if(gzTextVar.zRoomList[iRoomIndex].iMoveU ~= -1) then
        TL_SetProperty("Register Locality Entry", "move up", "Movement", "INSTANT")
        uiNavFlags = uiNavFlags + ciNavUp
    end
    if(gzTextVar.zRoomList[iRoomIndex].iMoveD ~= -1) then
        TL_SetProperty("Register Locality Entry", "move down", "Movement", "INSTANT")
        uiNavFlags = uiNavFlags + ciNavDown
    end
    
    --The player can always look, wait, and think.
    uiNavFlags = uiNavFlags + ciNavLook
    uiNavFlags = uiNavFlags + ciNavWait
    uiNavFlags = uiNavFlags + ciNavThink
 
    --Door closing and opening
    if(gzTextVar.zRoomList[iRoomIndex].sDoorN ~= nil) then
        if(string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorN, 1, 1) == "C") then
            TL_SetProperty("Register Locality Entry", "open north", "Doors", "INSTANT")
        else
            TL_SetProperty("Register Locality Entry", "close north", "Doors", "INSTANT")
        end
    end
    if(gzTextVar.zRoomList[iRoomIndex].sDoorE ~= nil) then
        if(string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorE, 1, 1) == "C") then
            TL_SetProperty("Register Locality Entry", "open east", "Doors", "INSTANT")
        else
            TL_SetProperty("Register Locality Entry", "close east", "Doors", "INSTANT")
        end
    end
    if(gzTextVar.zRoomList[iRoomIndex].sDoorS ~= nil) then
        if(string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorS, 1, 1) == "C") then
            TL_SetProperty("Register Locality Entry", "open south", "Doors", "INSTANT")
        else
            TL_SetProperty("Register Locality Entry", "close south", "Doors", "INSTANT")
        end
    end
    if(gzTextVar.zRoomList[iRoomIndex].sDoorW ~= nil) then
        if(string.sub(gzTextVar.zRoomList[iRoomIndex].sDoorW, 1, 1) == "C") then
            TL_SetProperty("Register Locality Entry", "open west", "Doors", "INSTANT")
        else
            TL_SetProperty("Register Locality Entry", "close west", "Doors", "INSTANT")
        end
    end
    
    -- |[Entities]|
    --Go through the list of entities in this room and add them. Disguised entities do not register!
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].sLocation == sLocation) then
            if(gzTextVar.zEntities[i].sInDisguise == nil or gzTextVar.zEntities[i].sInDisguise == "Null") then
                TL_SetProperty("Register Locality Entry", gzTextVar.zEntities[i].sDisplayName, "Entities", gzTextVar.zEntities[i].sCommandHandler)
            end
        end
    end
    
    -- |[Objects]|
    for i = 1, gzTextVar.zRoomList[iRoomIndex].iObjectsTotal, 1 do
        TL_SetProperty("Register Locality Entry", gzTextVar.zRoomList[iRoomIndex].zObjects[i].sDisplayName, "Objects", gzTextVar.zRoomList[iRoomIndex].zObjects[i].sHandlerScript)
    end
    
    -- |[Inventory Items]|
    --Equipment.
    if(gzTextVar.gzPlayer.zWeapon ~= nil) then
        TL_SetProperty("Register Locality Entry", gzTextVar.gzPlayer.zWeapon.sDisplayName, "Equipment", gzTextVar.gzPlayer.zWeapon.sHandlerScript)
    end
    if(gzTextVar.gzPlayer.zArmor ~= nil) then
        TL_SetProperty("Register Locality Entry", gzTextVar.gzPlayer.zArmor.sDisplayName,  "Equipment", gzTextVar.gzPlayer.zArmor.sHandlerScript)
    end
    if(gzTextVar.gzPlayer.zGloves ~= nil) then
        TL_SetProperty("Register Locality Entry", gzTextVar.gzPlayer.zGloves.sDisplayName, "Equipment", gzTextVar.gzPlayer.zGloves.sHandlerScript)
    end
    
    --Inventory.
    local iInventoryListSize = 0
    local zaInventoryList = {}
    gzTextVar.bIsPriorityCheck = true
    
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        
        --Get the item's priority and store it in an array with the table.
        gzTextVar.iPriorityResponse = -1
        LM_ExecuteScript(gzTextVar.gzPlayer.zaItems[i].sHandlerScript, "Null", 0, 0)
        
        --Determine stack properties.
        local sDisplayString = gzTextVar.gzPlayer.zaItems[i].sDisplayName
        if(gzTextVar.gzPlayer.zaItems[i].iStackSize > 1) then
            sDisplayString = sDisplayString .. " x " .. gzTextVar.gzPlayer.zaItems[i].iStackSize
        end
        
        --Register.
        TL_SetProperty("Register Locality Entry", sDisplayString, "Inventory", gzTextVar.gzPlayer.zaItems[i].sHandlerScript, gzTextVar.iPriorityResponse)
        
    end
    gzTextVar.bIsPriorityCheck = false
    
    -- |[Area Specific Commands]|
    --These can happen in very specific areas. The room's script is run with a flag set.
    gzTextVar.bIsLocalityCheck = true
        LM_ExecuteScript(gzTextVar.sRoomHandlers .. gzTextVar.gzPlayer.sLocation .. ".lua", "Null")
    gzTextVar.bIsLocalityCheck = false

    -- |[Finish Up]|
    --Re-open opened locality categories from the previous state.
    TL_SetProperty("Resume Locality States")
    
    --Store which buttons can be pressed.
    TL_SetProperty("Set Navigation Flags", uiNavFlags)
    
end