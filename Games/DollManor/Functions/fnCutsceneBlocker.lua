--[Function: fnCutsceneBlocker]
--Creates and registers a cutscene blocking event.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneBlocker()
function fnCutsceneBlocker()

	--Autogenerates the instruction name.
	Cutscene_CreateEvent("Blocker")
	DL_PopActiveObject()

end