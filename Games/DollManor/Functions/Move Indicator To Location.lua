--[ ================================ Move Indicator To Location ================================= ]
--Script subroutine used in cutscenes. Moves an indicator to a given location, but not the entity.
--Pass -1 for the entity's index to move the player.

--Argument Listing:
-- 0: sIndicatorName - Name of the indicator.
-- 1: sStartLocation - Starting room's name. Must exist, if not this script does nothing.
-- 2: sDestination - Target room's name. Must exist, if not this script does nothing.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sIndicatorName = LM_GetScriptArgument(0)
local sStartLocation = LM_GetScriptArgument(1)
local sDestination   = LM_GetScriptArgument(2)

--[Unregister]
--Remove indicator if the destination is "Unregister"
if(sDestination == "Unregister") then
    
    --Make sure the room exists.
    local iSrcIndex  = fnGetRoomIndex(sStartLocation)
    if(iSrcIndex == -1) then return end
    
    local fOldX, fOldY, fOldZ = fnGetRoomPosition(sStartLocation)
    TL_SetProperty("Unregister Entity Indicator", fOldX, fOldY, fOldZ, sIndicatorName)

--[Move]
--Otherwise, move to destination.
else

    --Make sure the target exists and get its room index.
    local iSrcIndex  = fnGetRoomIndex(sStartLocation)
    local iDestIndex = fnGetRoomIndex(sDestination)
    if(iDestIndex == -1 or iSrcIndex == -1) then return end

    --Get positions.
    local fOldX, fOldY, fOldZ = fnGetRoomPosition(sStartLocation)
    local fX, fY, fZ          = fnGetRoomPosition(sDestination)

    --Change the indicator's position.
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, sIndicatorName, fX, fY, fZ)
    
end