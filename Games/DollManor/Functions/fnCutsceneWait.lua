--[Function: fnCutsceneWait]
--Creates and registers a cutscene event which will wait the provided number of ticks. 60 ticks is 1 second.
--Built from: ./ZLaunch.lua
--Example: fnCutsceneWait(60)
function fnCutsceneWait(iTicks)

	--Argument check.
	if(iTicks == nil) then return end
	
	--Autogenerates the instruction name.
	Cutscene_CreateEvent("WaitTicks", "Timer")
		TimeEvent_SetProperty("Timer", iTicks)
	DL_PopActiveObject()

end