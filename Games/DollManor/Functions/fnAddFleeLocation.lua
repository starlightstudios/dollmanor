-- |[fnAddFleeLocation]|
--Adds a location that Jessie and Lauren can flee to if Mary is transformed.
fnAddFleeLocation = function(sLocation)
    if(sLocation == nil) then return end
    local iSize = #gzTextVar.saFleeLocations
    gzTextVar.saFleeLocations[iSize+1] = sLocation
end
