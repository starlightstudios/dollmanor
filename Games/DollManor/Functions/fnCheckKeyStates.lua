-- |[fnCheckKeyStates]|
--Checks whether or not the lock needs a key, and if the player has that key. Returns two values:
-- 0th: Boolean indicating whether or not the player can open the door.
-- 1st: The string to print indicating failure or success.
fnCheckKeyStates = function(sLockString)

    --Arg check.
    if(sLockString == nil) then return end
    
    --Setup.
    local sKeyNeeded = "No Key"
    local sUnlockString = "You unlocked the door with your [" .. sKeyNeeded .. "]." .. gsDE
    local sNoKeyString = "The door is locked. There is a symbol next to the key hole." .. gsDE

    --Lock needs a club:
    if(sLockString == " Club") then
        sKeyNeeded = "club key"
        sUnlockString = "You unlocked the door with your [" .. sKeyNeeded .. "]." .. gsDE
        sNoKeyString = "The door is locked. There is a picture of a club next to the key hole." .. gsDE
        
    --Lock needs a spade:
    elseif(sLockString == " Spade") then
        sKeyNeeded = "spade key"
        sUnlockString = "You unlocked the door with your [" .. sKeyNeeded .. "]." .. gsDE
        sNoKeyString = "The door is locked. There is a picture of a spade next to the key hole." .. gsDE
        
    --Lock needs a heart:
    elseif(sLockString == " Heart") then
        sKeyNeeded = "heart key"
        sUnlockString = "You unlocked the door with your [" .. sKeyNeeded .. "]." .. gsDE
        sNoKeyString = "The door is locked. There is a picture of a heart next to the key hole." .. gsDE
        
    --Lock needs a diamond:
    elseif(sLockString == " Diamond") then
        sKeyNeeded = "diamond key"
        sUnlockString = "You unlocked the door with your [" .. sKeyNeeded .. "]." .. gsDE
        sNoKeyString = "The door is locked. There is a picture of a diamond next to the key hole." .. gsDE
        
    --Lock needs a chapel key:
    elseif(sLockString == " Chapel") then
        sKeyNeeded = "chapel key"
        sUnlockString = "You unlocked the door with the [chapel key]." .. gsDE
        sNoKeyString = "The door to the chapel is locked. It must have a unique key." .. gsDE
        
    --Lock needs a small key:
    elseif(sLockString == " Small") then
        sKeyNeeded = "small key"
        sUnlockString = "You unlocked the door with your [small key]. The key snapped off in the lock, and is now useless." .. gsDE
        sNoKeyString = "The door is locked. It seems a [small key] will open this door." .. gsDE

    end

    --If "No Key" is specified, then the key isn't on the list of keys.
    if(sKeyNeeded == "No Key") then
        return true, "You open the door." .. gsDE
    end

    --Try to find the needed key.
    local iKeyIndex = fnGetIndexOfItemInInventory(sKeyNeeded)
    
    --Player does not have the key.
    if(iKeyIndex == -1) then
        return false, sNoKeyString
    
    --Player has the key.
    else
        return true, sUnlockString
    end
                    
end