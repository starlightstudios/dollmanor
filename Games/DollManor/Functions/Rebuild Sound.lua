--[ ======================================= Rebuild Sound ======================================= ]
--The player can "hear" what's going on in the manor with range 2 (1 diagonally) and will be able
-- to hear enemies moving around. This ability does not work on tiles that are "loud", which is
-- tiles that are raining or have something else that would disguise noise.
--Indicators spawn on these tiles. Indicators do not "move", they simply are or aren't there.
-- Indicators are cleared each time this is called and then rebuilt.

--First, remove all existing sound indicators.
for i = 1, #gzTextVar.zaSoundList, 1 do
    
    local p = gzTextVar.zaSoundList[i]
    TL_SetProperty("Unregister Entity Indicator", gzTextVar.zRoomList[p].fRoomX, gzTextVar.zRoomList[p].fRoomY, gzTextVar.zRoomList[p].fRoomZ, "Sound")
    
end

--Clean the sound list.
gzTextVar.zaSoundList = {}

--All sounds are removed. Starting from the player's position, iterate across the nearby rooms in the 3D map.
-- Mark all the rooms the player can hear something at.
--The player cannot hear things on different Z-levels.
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
local z = fZ

--Clamps.
local fXLo = fX - 2
local fXHi = fX + 2
local fYLo = fY - 2
local fYHi = fY + 2
if(fXLo < gzTextVar.iRoomWst) then fXLo = gzTextVar.iRoomWst end
if(fXHi > gzTextVar.iRoomEst) then fXHi = gzTextVar.iRoomEst end
if(fYLo < gzTextVar.iRoomNth) then fYLo = gzTextVar.iRoomNth end
if(fYHi > gzTextVar.iRoomSth) then fYHi = gzTextVar.iRoomSth end

--Iterate.
for x = fXLo, fXHi, 1 do
    for y = fYLo, fYHi, 1 do
        
        --Room index.
        local i = gzTextVar.zRoomLookup[x][y][z]
        
        --Validity check.
        if(i ~= nil and i >= 1 and i <= gzTextVar.iRoomsTotal) then
            
            --If the room is visible, don't bother.
            if(gzTextVar.zRoomList[i].bIsVisibleNow) then
            
            --Does the room have any audio indicators? They all work!
            elseif(gzTextVar.zRoomList[i].iLastHeardSound == gzTextVar.iWorldTurns) then
            
                --Create the indicator.
                TL_SetProperty("Register Entity Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, "Sound", 14, 3, ciPrioritySound)
                
                --Mark it on the sound list.
                local iNextIndex = #gzTextVar.zaSoundList + 1
                gzTextVar.zaSoundList[iNextIndex] = i
            end
            
        end
        
    end
end