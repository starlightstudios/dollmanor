--[ ======================================= fnSpawnGoop() ======================================= ]
--Spawns a rubber goop enemy. These patrol is fixed routes, do not spot the player, and are non-combat.
fnSpawnGoop = function(sSpawnLocation, sIdentifier, saPatrolList)
    
    --[Argument Handler]
    gzTextVar.iLastSpawnedEntity = -1
    if(sSpawnLocation == nil) then return end
    if(sIdentifier == nil) then return end
    --io.write("Spawning goop at " .. sSpawnLocation .. "\n")
    
    --If the patrol list is nil, then the goop will move randomly. We don't check it.
    
    --[Location]
    --Location Resolver. If the first letter of the location is an "_" then we need to resolve the room's name
    -- by its coordinates on the 3D map.
    if(string.sub(sSpawnLocation, 1, 1) == "_") then
        
        --Subroutine does the work. Range-check it.
        local iIndex = fnGetRoomIndexFromString(sSpawnLocation)
        if(iIndex == -1 or iIndex >= gzTextVar.iRoomsTotal) then return end
        
        --Modify the sSpawnLocation to be the name of the room.
        sSpawnLocation = gzTextVar.zRoomList[iIndex].sName
    end
    
    --[Patrol List]
    --Go through the patrol list and do this modification to all of its entries, turning coordinates to names.
    if(saPatrolList ~= nil) then
        for i = 1, #saPatrolList, 1 do
            
            if(string.sub(saPatrolList[i], 1, 1) == "_") then
                
                --Subroutine does the work. Range-check it.
                local iIndex = fnGetRoomIndexFromString(saPatrolList[i])
                if(iIndex == -1 or iIndex >= gzTextVar.iRoomsTotal) then return end
                
                --Modify the sSpawnLocation to be the name of the room.
                saPatrolList[i] = gzTextVar.zRoomList[iIndex].sName
            end
        end
    end
    
    --[Creation]
    --Add space for a new entity.
    gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

    --Shorthand
    local i = gzTextVar.zEntitiesTotal

    --Roll a name. Retry until we get a legal one or make 100 attempts.
    local bGeneratedName = false
    local sUppercase = "Null"
    local sName = "Null"
    for i = 1, 26, 1 do
        
        --Generate the name.
        bGeneratedName = true
        sUppercase = "Goop " .. string.char(65 + i - 1)
        sName = "goop " .. string.char(97 + i - 1)
        
        --Check if the name is taken. If it is, reroll.
        for p = 1, gzTextVar.zEntitiesTotal - 1, 1 do
            if(gzTextVar.zEntities[p].sQueryName == sName) then
                bGeneratedName = false
                break
            end
        end
        
        --Stop if we found a name.
        if(bGeneratedName) then break end
    end

    --Couldn't generate a legal name.
    if(bGeneratedName == false) then
        io.write("Failed to find space for goop enemy!\n")
        gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal - 1
        return
    end

    --[Initialization]
    --Strings
    gzTextVar.iLastSpawnedEntity = i
    gzTextVar.zEntities[i] = {}
    gzTextVar.zEntities[i].sDisplayName = sUppercase
    gzTextVar.zEntities[i].sQueryName = sName
    gzTextVar.zEntities[i].sQueryPicture = "Null"
    gzTextVar.zEntities[i].sUniqueName = sName
    gzTextVar.zEntities[i].sLocation = sSpawnLocation
    gzTextVar.zEntities[i].sPrevLocation = sSpawnLocation --Needed to prevent the player from moving through us.
    gzTextVar.zEntities[i].sSpecialIdentifier = sIdentifier

    --[AI Variables]
    --Command Handler
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Goop.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Goop"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Goop"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Goop"
    gzTextVar.zEntities[i].iMoveIndex = -1
    gzTextVar.zEntities[i].iPatrolIndex = 1
    if(saPatrolList ~= nil) then
        gzTextVar.zEntities[i].saPatrolList = saPatrolList
        gzTextVar.zEntities[i].bMovesRandomly = false
    else
        gzTextVar.zEntities[i].saPatrolList = {"None"}
        gzTextVar.zEntities[i].bMovesRandomly = true
    end
    gzTextVar.zEntities[i].iStayStillTurns = 0
    gzTextVar.zEntities[i].sLastDoorDir = "P"
    gzTextVar.zEntities[i].sIndicatorName = gzTextVar.zEntities[i].sUniqueName
    gzTextVar.zEntities[i].sFirstSawPlayer = "Nowhere"
    gzTextVar.zEntities[i].sLastSawPlayer = "Nowhere"
    gzTextVar.zEntities[i].iChanceToForget = 75
    gzTextVar.zEntities[i].sInDisguise = "Null"

    --[Combat Properties]
    --Goops don't engage in combat so these are dummy values.
    gzTextVar.zEntities[i].zCombatTable = {}
    gzTextVar.zEntities[i].zCombatTable.iHealth = 1
    gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 1
    gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
    gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 1, 300, 0}
    
    --Weaknesses. Goops don't engage in combat, so these are all zeroes.
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses = {}
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Fire] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Water] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Wind] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Earth] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Life] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Death] = 0

    --Indicator.
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
    gzTextVar.zEntities[i].iIndicatorX = 14
    gzTextVar.zEntities[i].iIndicatorY = 3
    gzTextVar.zEntities[i].iIndicatorC = ciCodeUnfriendly
    gzTextVar.zEntities[i].iIndicatorH = 0
    TL_SetProperty("Register Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, gzTextVar.zEntities[i].iIndicatorX, gzTextVar.zEntities[i].iIndicatorY, gzTextVar.zEntities[i].iIndicatorC)
    
    --Debug.
    --io.write("Spawned goop correctly.\n")
end