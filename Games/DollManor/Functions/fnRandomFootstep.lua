--[fnRandomFootstep]
--Plays random footstep noises. Used to add a bit of variety.
fnRandomFootstep = function()
    local iRoll = LM_GetRandomNumber(1, 2)
    if(iRoll == 1) then
        AudioManager_PlaySound("Doll|FootstepsA")
    elseif(iRoll == 2) then
        AudioManager_PlaySound("Doll|FootstepsB")
    elseif(iRoll == 3) then
        AudioManager_PlaySound("Doll|FootstepsC")
    elseif(iRoll == 4) then
        AudioManager_PlaySound("Doll|FootstepsD")
    else
        AudioManager_PlaySound("Doll|FootstepsE")
    end
    
end