--[ ===================================== fnSpawnStranger() ===================================== ]
--A glass woman who relentlessly pursues the player...
fnSpawnStranger = function(sSpawnLocation)
    
    --[Argument Handler]
    gzTextVar.iLastSpawnedEntity = -1
    if(sSpawnLocation == nil) then return end
    
    --[Location]
    --Location Resolver. If the first letter of the location is an "_" then we need to resolve the room's name
    -- by its coordinates on the 3D map.
    if(string.sub(sSpawnLocation, 1, 1) == "_") then
        
        --Subroutine does the work. Range-check it.
        local iIndex = fnGetRoomIndexFromString(sSpawnLocation)
        if(iIndex == -1 or iIndex >= gzTextVar.iRoomsTotal) then return end
        
        --Modify the sSpawnLocation to be the name of the room.
        sSpawnLocation = gzTextVar.zRoomList[iIndex].sName
    end
    
    --[Creation]
    --Add space for a new entity.
    gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1

    --Shorthand
    local i = gzTextVar.zEntitiesTotal

    --[Initialization]
    --Strings
    gzTextVar.iLastSpawnedEntity = i
    gzTextVar.zEntities[i] = {}
    gzTextVar.zEntities[i].sDisplayName = "Stranger"
    gzTextVar.zEntities[i].sQueryName = "stranger"
    gzTextVar.zEntities[i].sUniqueName = "stranger"
    gzTextVar.zEntities[i].sLocation = sSpawnLocation
    gzTextVar.zEntities[i].sSpecialIdentifier = "strangerident"

    --[AI Variables]
    --Command Handler
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Stranger.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Stranger"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Stranger"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Stranger"
    gzTextVar.zEntities[i].iMoveIndex = -1
    gzTextVar.zEntities[i].iStayStillTurns = 0
    gzTextVar.zEntities[i].sLastDoorDir = "P"
    gzTextVar.zEntities[i].sIndicatorName = gzTextVar.zEntities[i].sUniqueName
    gzTextVar.zEntities[i].sFirstSawPlayer = "Nowhere"
    gzTextVar.zEntities[i].sLastSawPlayer = "Nowhere"
    gzTextVar.zEntities[i].iChanceToForget = 5
    gzTextVar.zEntities[i].sInDisguise = "Null"
    
    --Special AI Pieces
    gzTextVar.zEntities[i].sLastGroup = "Null"
    gzTextVar.zEntities[i].iLastRoom = "Null"

    --[Combat Properties]
    --Stranger becomes more deadly each time she is defeated. Good luck.
    gzTextVar.zEntities[i].zCombatTable = {}
    
    --Starting case.
    if(gzTextVar.iStrangerDefeats < 1) then
        local fTimeFactor = 2.00
        gzTextVar.zEntities[i].zCombatTable.iHealth = 27
        gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
        gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4, 300 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5, 400 * fTimeFactor, 0}
        gzTextVar.zStranger.sImgPath = "Root/Images/DollManor/Characters/Stranger0"
    
    --After one defeat, the Stranger gains extra HP.
    elseif(gzTextVar.iStrangerDefeats == 1) then
        local fTimeFactor = 2.00
        gzTextVar.zEntities[i].zCombatTable.iHealth = 50
        gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
        gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4, 300 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5, 400 * fTimeFactor, 0}
        gzTextVar.zStranger.sImgPath = "Root/Images/DollManor/Characters/Stranger1"
    
    --After two defeats, the Stranger gains a new quick attack, +25% DPS, and more HP.
    -- The quick attack is designed to strip off life shields.
    elseif(gzTextVar.iStrangerDefeats == 2) then
        local fTimeFactor = 1.50
        gzTextVar.zEntities[i].zCombatTable.iHealth = 73
        gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 3
        gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4, 300 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5, 400 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[3] = {50,  1,  30 * fTimeFactor, 0}
        gzTextVar.zStranger.sImgPath = "Root/Images/DollManor/Characters/Stranger2"
    
    --After three defeats, the stranger gains another +25% DPS, more HP, and all attacks increase by 2 damage.
    elseif(gzTextVar.iStrangerDefeats >= 3) then
        local fTimeFactor = 1.00
        gzTextVar.zEntities[i].zCombatTable.iHealth = 100
        gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 3
        gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4+2, 300 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5+2, 400 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[3] = {50,  1+2,  30 * fTimeFactor, 0}
        gzTextVar.zStranger.sImgPath = "Root/Images/DollManor/Characters/Stranger3"
    
    --[=[
    --After four defeats, even more HP is gained and the primary attacks increase by another 3 damage.
    elseif(gzTextVar.iStrangerDefeats == 4) then
        local fTimeFactor = 1.00
        gzTextVar.zEntities[i].zCombatTable.iHealth = 130
        gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 3
        gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4+5, 300 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5+5, 400 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[3] = {50,  1+2,  30 * fTimeFactor, 0}
    
    --Additional defeats cause the stranger to gain an additional 15 HP for each defeat and +10% faster
    -- attacks to a cap of 50%. Every 3 defeats adds +1 to all attack damages.
    --The HP stacks indefinitely, as does the attack bonus.
    elseif(gzTextVar.iStrangerDefeats > 4) then
    
        --How many defeats past 4.
        local iDefeatsPast = gzTextVar.iStrangerDefeats - 4
        
        --Compute the health bonus.
        local iHPBonus = (15 * iDefeatsPast)
        
        --Compute the speed bonus.
        local fSpeedBonus = 0.10 * iDefeatsPast
        if(fSpeedBonus > 0.50) then fSpeedBonus = 0.50 end
        local fTimeFactor = 1.00 - fSpeedBonus
        
        --Compute the base damage bonus. Can be zero.
        local iDamageBonus = math.floor(iDefeatsPast / 3.0)
        
        --Set stats.
        gzTextVar.zEntities[i].zCombatTable.iHealth = 130 + iHPBonus
        gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 3
        gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4+5 + iDamageBonus, 300 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 5+5 + iDamageBonus, 400 * fTimeFactor, 0}
        gzTextVar.zEntities[i].zCombatTable.zAttacks[3] = {50,  1+2 + iDamageBonus,  30 * fTimeFactor, 0}]=]
    end
    
    --Set strangers portrait accordingly.
    gzTextVar.zEntities[i].sQueryPicture = gzTextVar.zStranger.sImgPath
    
    --Weaknesses. The stranger takes extra damage from wind.
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses = {}
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Fire] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Water] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Wind] = 1
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Earth] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Life] = 0
    gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Death] = 0
    
    --[Indicator]
    --No indicator variance here.
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
    gzTextVar.zEntities[i].iIndicatorX = 17
    gzTextVar.zEntities[i].iIndicatorY = 4
    gzTextVar.zEntities[i].iIndicatorC = ciCodeUnfriendly
    gzTextVar.zEntities[i].iIndicatorH = 0
    TL_SetProperty("Register Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, gzTextVar.zEntities[i].iIndicatorX, gzTextVar.zEntities[i].iIndicatorY, gzTextVar.zEntities[i].iIndicatorC)
    
    --Determine Hostility
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        gzTextVar.zEntities[i].iIndicatorH = 2
        TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, ciHostilityX_Enemy, ciHostilityY_Enemy)
    end
end