-- |[ ====================================== Spawn Sarah ======================================= ]|
--Starts at "no location".
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1
gzTextVar.iSarahIndex = gzTextVar.zEntitiesTotal

--Shorthand
local i = gzTextVar.iSarahIndex

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].bIsProtected = true
gzTextVar.zEntities[i].sDisplayName = "Sarah"
gzTextVar.zEntities[i].sQueryName = "sarah"
gzTextVar.zEntities[i].sQueryPicture = "Null"
gzTextVar.zEntities[i].sUniqueName = "sarah"
gzTextVar.zEntities[i].sIndicatorName = "Sarah"
gzTextVar.zEntities[i].sLocation = "Pygmalie's Study"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sState = "Book"
gzTextVar.zEntities[i].bMovesRandomly = false
gzTextVar.zEntities[i].iSkinColor = 0
gzTextVar.zEntities[i].iStayStillTurns = 0
gzTextVar.zEntities[i].sLastDoorDir = "P"
gzTextVar.zEntities[i].sSpecialIdentifier = "Sarah"

--No indicator
gzTextVar.zEntities[i].iIndicatorX = "No Indicator"
gzTextVar.zEntities[i].iIndicatorY = "No Indicator"
gzTextVar.zEntities[i].iIndicatorC = "No Indicator"
gzTextVar.zEntities[i].iIndicatorH = 0

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Sarah.lua"
gzTextVar.zEntities[i].sAIHandlerHostility = "Sarah"
gzTextVar.zEntities[i].sAIHandlerPostTurn = "Sarah"
gzTextVar.zEntities[i].sAIHandlerPrimary = "Sarah"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"None"}
gzTextVar.zEntities[i].sFirstSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sLastSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sInDisguise = "Null"