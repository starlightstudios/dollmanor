-- |[ ====================================== Spawn Eileen ====================================== ]|
--Spawns Eileen at the start of the game.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1
gzTextVar.iEileenIndex = gzTextVar.zEntitiesTotal

--Shorthand
local i = gzTextVar.iEileenIndex

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].bIsProtected = true
gzTextVar.zEntities[i].sDisplayName = "Eileen"
gzTextVar.zEntities[i].sQueryName = "eileen"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Princess A"
gzTextVar.zEntities[i].sUniqueName = "eileen"
gzTextVar.zEntities[i].sIndicatorName = "Eileen"
gzTextVar.zEntities[i].sLocation = "Main Hall"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sState = "Doll"
gzTextVar.zEntities[i].bMovesRandomly = true
gzTextVar.zEntities[i].iSkinColor = 0
gzTextVar.zEntities[i].iStayStillTurns = 0
gzTextVar.zEntities[i].sLastDoorDir = "P"
gzTextVar.zEntities[i].sSpecialIdentifier = "Eileen"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Eileen.lua"
gzTextVar.zEntities[i].sAIHandlerHostility = "Neutral"
gzTextVar.zEntities[i].sAIHandlerPostTurn = "Neutral"
gzTextVar.zEntities[i].sAIHandlerPrimary = "Neutral"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"None"}
gzTextVar.zEntities[i].sFirstSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sLastSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sInDisguise = "Null"

--Combat Properties
gzTextVar.zEntities[i].zCombatTable = {}
gzTextVar.zEntities[i].zCombatTable.iHealth = 1
gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4, 240, 0}
gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = { 10, 6, 180, 0}

--Indicator.
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
gzTextVar.zEntities[i].iIndicatorX = 4
gzTextVar.zEntities[i].iIndicatorY = 4
gzTextVar.zEntities[i].iIndicatorC = ciCodeFriendly
gzTextVar.zEntities[i].iIndicatorH = 0
TL_SetProperty("Register Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, gzTextVar.zEntities[i].iIndicatorX, gzTextVar.zEntities[i].iIndicatorY, gzTextVar.zEntities[i].iIndicatorC)
