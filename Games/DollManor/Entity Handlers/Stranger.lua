-- |[ ======================================== Stranger ======================================== ]|
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Make sure the current entity is valid. This is skipped during command building.
if((gzTextVar.iCurrentEntity < 1 or gzTextVar.iCurrentEntity > gzTextVar.zEntitiesTotal) and TL_GetProperty("Is Building Commands") == false) then return end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "attack",    "attack " .. sEntityName)
        TL_SetProperty("Register Popup Command", "talk",      "talk "   .. sEntityName)
    
    --Player is a doll:
    else
        TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    
    end
    return
end

-- |[ ======================================== Examining ======================================= ]|
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin..." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin. She is not of interest." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin. She is hunting intruders." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin. You wave to her with your useless hands. She ignores you." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin. She is hunting intruders." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin. You wish you could be like her someday." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "An enormous woman in a coat with a mask on. You can see through her skin. She is hunting intruders, and never stops to talk, much less play." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Phase 6.
        else
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "She's supposed to be defeated in phase 6." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
        end
    end
    
-- |[ ======================================= Attacking ======================================== ]|
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt to fight." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Run the target's AI with this flag set.
        gzTextVar.bIsTriggeringFights = true
        gzTextVar.bTurnEndsWhenCombatEnds = true
        
        --Fast-access variables.
        local i = gzTextVar.iCurrentEntity
        
        --If there's no handler, do nothing.
        if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
            
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
            fnClaygirl_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
            fnDoll_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
            fnTitan_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
            fnGoop_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
            fnStranger_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
            fnJessie_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
            fnLauren_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
            fnPygmalie_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
            fnSarah_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
            fnDollStationary_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
            fnDollStationaryCombat_AI(i)
        end
        
        --Clean.
        gzTextVar.bIsTriggeringFights = false
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "You have no will, and thus cannot attack this person unless ordered. The light has commanded you to find humans." .. gsDE)
    
    --Claygirl.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "You'd never attack the stranger, even if she's often rude." .. gsDE)
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "Even if you tried, your useless hands would bounce off. The stranger probably wouldn't even notice." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You have no desire to attack the stranger, or do anything else really." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "You'd never attack a fellow work of art." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Append", "You could never bring yourself to attack one of your creator's works unless directly ordered to." .. gsDE)
        
        --Phase 6.
        else
            TL_SetProperty("Append", "Attacking the stranger would blow your cover. So, you don't." .. gsDE)
        end
    end

-- |[ ======================================== Dialogue ======================================== ]|
--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt even to speak." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "'Who are you?' you ask. The masked mouth does not move or speak." .. gsDE)
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "You emptily regard the stranger. She ignores you." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "Your futile attempts to speak without lungs are ignored by the stranger." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "Your mouth is painted on, so you cannot speak. Not like it matters. The stranger speaks to no one." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "You don't care enough to speak to the stranger, and she doesn't enough to speak to you." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Hey!' you say to the stranger. She ignores you, busy tracking down the intruders. She's always like this." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "'Do you want to play?' you ask." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "As always, all you receive is silence in return. The stranger is very dedicated to protect you and the dollies, so you leave her to her work." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Phase 6.
        else
            TL_SetProperty("Append", "The stranger is defeated in phase 6." .. gsDE)
            TL_SetProperty("Create Blocker")
        end

    end

end