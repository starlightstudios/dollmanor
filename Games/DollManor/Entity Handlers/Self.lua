-- |[ ========================================== Self ========================================== ]|
--Handles self-examination.
if(gzTextVar.gzPlayer.sFormState == "Human") then
    
    -- |[Ice Trap]|
    --Special: Ice!
    if(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.200) then
        TL_SetProperty("Append", "This is you. You're a sixteen year old English girl with interests in tennis and chemistry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You're cold, but it's nothing to be worried about just yet." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.400) then
        TL_SetProperty("Append", "This is you. You're a sixteen year old English girl with interests in tennis and chemistry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The cold is starting to get to you. You should hurry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.600) then
        TL_SetProperty("Append", "This is you. You're a sixteen year old English girl with interests in tennis and chemistry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The ice is setting in and seems to be taking over your skin. This is no ordinary cold..." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 0.800) then
        TL_SetProperty("Append", "This is you. You're a sixteen year old English girl with interests in tennis and chemistry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The cold is wearing at you. You're going numb." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Mary freezes. This is in a subscript.
    elseif(gzTextVar.fMaryFreezeAmount > 0.000 and gzTextVar.fMaryFreezeAmount < 1.00) then
        TL_SetProperty("Append", "This is you. You're a sixteen year old English girl with interests in tennis and chemistry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You're mostly numb and need to warm up desperately. The ice is taking hold..." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    -- |[String Trap]|
    --Special: String Trap!
    elseif(gzTextVar.iMaryStringTrapState == 1) then
        TL_SetProperty("Append", "This is you. You've had your clothes stolen by some kind of magical wardrobe, but are otherwise unharmed." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    elseif(gzTextVar.iMaryStringTrapState == 2) then
        TL_SetProperty("Append", "This is you. You've been transformed into a pale plastic ball-jointed doll dressed like a goth. You've retained your willpower, for now." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Hopefully the magical clothes will return you to normal if you can remove them..." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    -- |[Phase 5: Dumb Mary]|
    --In phase 5, unique dialogue appears.
    elseif(gzTextVar.iGameStage == 5) then
        TL_SetProperty("Append", "This is you. You're a... girl? Maybe? You know what girls are. You're probably one. Yeah. That's it." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    -- |[Other Cases]|
    --All other cases.
    else
        TL_SetProperty("Append", "This is you. You're a sixteen year old English girl with interests in tennis and chemistry." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        if(gzTextVar.gzPlayer.iClayInfection < 0) then
        elseif(gzTextVar.gzPlayer.iClayInfection < 5) then
            TL_SetProperty("Append", "There is currently a glob of clay on your arm, and it is worryingly hard to get off..." .. gsDE)
            TL_SetProperty("Create Blocker")
        elseif(gzTextVar.gzPlayer.iClayInfection < 10) then
            TL_SetProperty("Append", "There is currently a glob of clay rapidly spreading across your body..." .. gsDE)
            TL_SetProperty("Create Blocker")
        elseif(gzTextVar.gzPlayer.iClayInfection < 15) then
            TL_SetProperty("Append", "Your body is covered by a distressingly large amount of rapidly spreading clay. You need to wash it off immediately." .. gsDE)
            TL_SetProperty("Create Blocker")
        else
            TL_SetProperty("Append", "You are mostly covered by clay and, if you don't do something quickly, will be overtaken by it in mere moments." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    end

--Statue.
elseif(gzTextVar.gzPlayer.sFormState == "Statue") then

    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "YOU ARE A STATUE. YOU HAVE NO WILL, NO THOUGHTS. YOU DO AS THE LIGHT COMMANDS." .. gsDE)
    TL_SetProperty("Create Blocker")

--Claygirl.
elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then

    --Normal:
    if(gzTextVar.bIsClaySequence == false) then
        TL_SetProperty("Append", "This is you. You're a sixteen your old English girl made entirely out of clay. You have interests in spreading clay and disguising yourself as objects to trap humans." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Special sequence:
    else

        --Disguised:
        if(gzTextVar.bClayedFriends == false) then
            TL_SetProperty("Append", "This is you. You're a sixteen your old English girl made entirely out of clay. By concentrating really hard, you can hold a somewhat-human shape, as long as you don't try to speak or run." .. gsDE)
            TL_SetProperty("Create Blocker")

        --Ending.
        else
            TL_SetProperty("Append", "This is you. You're a sixteen your old English girl made entirely out of clay. Your transformation has left you exhausted, but hopeful." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    end

--Rubber.
elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
    TL_SetProperty("Append", "This is you. You're a sixteen your old English girl encased in rubber head to toe. The rubber has seeped into your body and replaced everything. There is no flesh left. You are no longer human." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You have painted-on clothes and a painted-on permanent grin. You're indescribably happy all the time - you can't wait to spread rubber to everyone you meet!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Ice.
elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
    TL_SetProperty("Append", "This is you. You're an ice sculpture, heart and soul frozen solid. You care for nothing and no one, save your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Ice.
elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
    TL_SetProperty("Append", "This is you, a glass sculpture created by Pygmalie in the image of a fantastical character named 'Mary'." .. gsDE)
    TL_SetProperty("Create Blocker")

--Blank doll.
elseif(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a plain white puppet. You have no mind, your thoughts are empty, and you have no desire to move." .. gsDE)
    TL_SetProperty("Create Blocker")

--Punk Transformation
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 8) == "Bride TF") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a doll, currently being shaped by your creator to become a blushing bride." .. gsDE)
    TL_SetProperty("Create Blocker")

--Dancer Transformation
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 9) == "Dancer TF") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a doll, currently being shaped by your creator to become a beautfiul, graceful dancer." .. gsDE)
    TL_SetProperty("Create Blocker")

--Geisha Transformation
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 9) == "Geisha TF") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a doll, currently being shaped by your creator to become a graceful and mysterious geisha." .. gsDE)
    TL_SetProperty("Create Blocker")

--Goth Transformation
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 7) == "Goth TF") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a doll, currently being shaped by your creator to become a mysterious, melancholy goth." .. gsDE)
    TL_SetProperty("Create Blocker")

--Princess Transformation
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 11) == "Princess TF") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a doll, currently being shaped by your creator to become a beautiful, elegant princess." .. gsDE)
    TL_SetProperty("Create Blocker")

--Punk Transformation
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 7) == "Punk TF") then
    --TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, 0)
    TL_SetProperty("Append", "You are a doll, currently being shaped by your creator to become a rebellious hot-headed punk." .. gsDE)
    TL_SetProperty("Create Blocker")

--Doll - General
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll") then
    
    --String trap unique case.
    if(gzTextVar.iMaryStringTrapState == 3) then
        TL_SetProperty("Append", "This is you. The magical wardrobe your creator made had transformed you into a gothic lolita doll. You are utterly obedient to your creator's will." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        if(gzTextVar.bStringTrapNoFollow == false) then
            TL_SetProperty("Append", "The wardrobes magic causes you to appear as a human, so long as nobody touches you. Fortunately, your friends don't suspect a thing. They'll be so cute in mere moments!" .. gsDE)
            TL_SetProperty("Create Blocker")
        end
        
        TL_SetProperty("Append", "You will need to have your mind erased by your creator later, but for now you'll need to transform your friends into dolls as well." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    --String trap unique case.
    elseif(gzTextVar.iMaryStringTrapState == 4) then
        TL_SetProperty("Append", "This is you. The magical wardrobe your creator made had transformed you into a gothic lolita doll. You are utterly obedient to your creator's will." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Your next task is to introduce yourself to your creator, thank her for making you a doll, and have your mind made utterly obedient. You can't wait!" .. gsDE)
        TL_SetProperty("Create Blocker")
        return
        
    --In phase 6, unique dialogue appears.
    elseif(gzTextVar.iGameStage == 6) then
        TL_SetProperty("Append", "This is you, a doll. Your mind was erased, but saved in a book. Now restored, you have full control over yourself." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Normal.
    else
        TL_SetProperty("Append", "You are a doll, a human soul trapped in an animated doll body. Your mind has been erased and a new one created on top of it. Your creator wishes for you to help her create many more sisters." .. gsDE)
        TL_SetProperty("Create Blocker")
    end
end

-- |[Equipment Listing]|
local sWeaponName = nil
local sArmorName = nil
local sGlovesName = nil
if(gzTextVar.gzPlayer.zWeapon ~= nil) then
    sWeaponName = gzTextVar.gzPlayer.zWeapon.sDisplayName
end
if(gzTextVar.gzPlayer.zArmor ~= nil) then
    sArmorName = gzTextVar.gzPlayer.zArmor.sDisplayName
end
if(gzTextVar.gzPlayer.zGloves ~= nil) then
    sGlovesName = gzTextVar.gzPlayer.zGloves.sDisplayName
end

--Case handling.
local sSentence = "You have no equipment."

--Weapon is yes.
if(sWeaponName ~= nil) then
    
    --Armor is yes.
    if(sArmorName ~= nil) then
        
        --Weapon, Armor, Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wielding a [" .. sWeaponName .. "], wearing a [" .. sArmorName .. "], and wearing a pair of [" .. sGlovesName .. "]. Use the [equipment] command to see related bonuses."
            
        --Weapon and Armor.
        else
            sSentence = "You are wielding a [" .. sWeaponName .. "] and wearing a [" .. sArmorName .. "]. Use the [equipment] command to see related bonuses."
        end

    --No armor.
    else
        --Weapon, Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wielding a [" .. sWeaponName .. "] and wearing a pair of [" .. sGlovesName .. "]. Use the [equipment] command to see related bonuses."
            
        --Weapon only.
        else
            sSentence = "You are wielding a [" .. sWeaponName .. "]. Use the [equipment] command to see related bonuses."
        end

    end

--No weapon.
else
    
    --Armor is yes.
    if(sArmorName ~= nil) then
        
        --Armor, Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wearing a [" .. sArmorName .. "] and a pair of [" .. sGlovesName .. "]. Use the [equipment] command to see related bonuses."
            
        --Just Armor.
        else
            sSentence = "You are wearing a [" .. sArmorName .. "]. Use the [equipment] command to see related bonuses."
        end

    --No armor.
    else
        --Just Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wearing a pair of [" .. sGlovesName .. "]. Use the [equipment] command to see related bonuses."
            
        --No equipment.
        else
            sSentence = "You are not wearing or wielding any equipment."
        end

    end

end

--Send the equipment line.
TL_SetProperty("Append", sSentence .. gsDE)
TL_SetProperty("Unregister Image")
