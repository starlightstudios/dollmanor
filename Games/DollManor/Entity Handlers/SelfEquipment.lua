-- |[ ======================================= Equipment ======================================== ]|
--Displays the player's equipment and related bonuses.


-- |[Equipment Listing]|
local sWeaponName = nil
local sArmorName = nil
local sGlovesName = nil
if(gzTextVar.gzPlayer.zWeapon ~= nil) then
    sWeaponName = gzTextVar.gzPlayer.zWeapon.sDisplayName
end
if(gzTextVar.gzPlayer.zArmor ~= nil) then
    sArmorName = gzTextVar.gzPlayer.zArmor.sDisplayName
end
if(gzTextVar.gzPlayer.zGloves ~= nil) then
    sGlovesName = gzTextVar.gzPlayer.zGloves.sDisplayName
end

--Case handling.
local sSentence = "You have no equipment."

--Weapon is yes.
if(sWeaponName ~= nil) then
    
    --Armor is yes.
    if(sArmorName ~= nil) then
        
        --Weapon, Armor, Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wielding a [" .. sWeaponName .. "], wearing a [" .. sArmorName .. "], and wearing a pair of [" .. sGlovesName .. "]."
            
        --Weapon and Armor.
        else
            sSentence = "You are wielding a [" .. sWeaponName .. "] and wearing a [" .. sArmorName .. "]."
        end

    --No armor.
    else
        --Weapon, Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wielding a [" .. sWeaponName .. "] and wearing a pair of [" .. sGlovesName .. "]."
            
        --Weapon only.
        else
            sSentence = "You are wielding a [" .. sWeaponName .. "]."
        end

    end

--No weapon.
else
    
    --Armor is yes.
    if(sArmorName ~= nil) then
        
        --Armor, Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wearing a [" .. sArmorName .. "] and a pair of [" .. sGlovesName .. "]."
            
        --Just Armor.
        else
            sSentence = "You are wearing a [" .. sArmorName .. "]."
        end

    --No armor.
    else
        --Just Gloves.
        if(sGlovesName ~= nil) then
            sSentence = "You are wearing a pair of [" .. sGlovesName .. "]."
            
        --No equipment.
        else
            sSentence = "You are not wearing or wielding any equipment."
        end

    end

end

--Send the equipment line.
TL_SetProperty("Append", sSentence .. gsDE)
TL_SetProperty("Unregister Image")

--[Bonuses]
--Recompute stats.
fnComputePlayerStatBonuses()

--Display.
local sLftAAlign = ""
local sLftBAlign = "[POS:240]"
local sRgtAAlign = "[POS:320]"
local sRgtBAlign = "[POS:500]"
TL_SetProperty("Append", sLftAAlign .. "Attack Bonus: ")
TL_SetProperty("Append", sLftBAlign .. gzTextVar.iAtkBonus)
TL_SetProperty("Append", sRgtAAlign .. "Defend Bonus: ")
TL_SetProperty("Append", sRgtBAlign .. gzTextVar.iDefBonus .. "\n")
TL_SetProperty("Append", sLftAAlign .. "Starting Shields: ")
TL_SetProperty("Append", sLftBAlign .. gzTextVar.iStartShieldBonus)
TL_SetProperty("Append", sRgtAAlign .. "Sentence Len: ")
TL_SetProperty("Append", sRgtBAlign  .. "+1/Modifier" .. "\n" .. "\n")

TL_SetProperty("Append", sLftAAlign .. "Water Bonus: ")
TL_SetProperty("Append", sLftBAlign .. gzTextVar.iWaterBonus)
TL_SetProperty("Append", sRgtAAlign .. "Fire Bonus: ")
TL_SetProperty("Append", sRgtBAlign .. gzTextVar.iFireBonus .. "\n")
TL_SetProperty("Append", sLftAAlign .. "Wind Bonus: ")
TL_SetProperty("Append", sLftBAlign .. gzTextVar.iWindBonus)
TL_SetProperty("Append", sRgtAAlign .. "Earth Bonus: ")
TL_SetProperty("Append", sRgtBAlign .. gzTextVar.iEarthBonus .. "\n")
TL_SetProperty("Append", sLftAAlign .. "Life Bonus: ")
TL_SetProperty("Append", sLftBAlign .. gzTextVar.iLifeBonus)
TL_SetProperty("Append", sRgtAAlign .. "Death Bonus: ")
TL_SetProperty("Append", sRgtBAlign .. gzTextVar.iDeathBonus .. gsDE)
