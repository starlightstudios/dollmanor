-- |[ ===================================== Spawn Pygmalie ===================================== ]|
--Spawns Pygmalie at the start of the game.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1
gzTextVar.iPygmalieIndex = gzTextVar.zEntitiesTotal

--Shorthand
local i = gzTextVar.iPygmalieIndex

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].bIsProtected = true
gzTextVar.zEntities[i].sDisplayName = "Pygmalie"
gzTextVar.zEntities[i].sQueryName = "pygmalie"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Pygmalie"
gzTextVar.zEntities[i].sUniqueName = "pygmalie"
gzTextVar.zEntities[i].sIndicatorName = "Pygmalie"
gzTextVar.zEntities[i].sLocation = "Main Hall"
gzTextVar.zEntities[i].sSpecialIdentifier = "Pygmalie"

--Indicator.
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
gzTextVar.zEntities[i].iIndicatorX = 7
gzTextVar.zEntities[i].iIndicatorY = 3
gzTextVar.zEntities[i].iIndicatorC = ciCodeFriendly
gzTextVar.zEntities[i].iIndicatorH = 0
TL_SetProperty("Register Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, gzTextVar.zEntities[i].iIndicatorX, gzTextVar.zEntities[i].iIndicatorY, gzTextVar.zEntities[i].iIndicatorC)

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Pygmalie.lua"
gzTextVar.zEntities[i].sAIHandlerHostility = "Pygmalie"
gzTextVar.zEntities[i].sAIHandlerPostTurn = "Pygmalie"
gzTextVar.zEntities[i].sAIHandlerPrimary = "Pygmalie"