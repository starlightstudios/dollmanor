-- |[ ========================================= Sarah ========================================== ]|
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Other.
local i = gzTextVar.iLaurenIndex
local p = gzTextVar.iJessieIndex
    
--Fast-access strings.
local iMaryCnt = 1
local sMaryImg = gzTextVar.gzPlayer.sQuerySprite
local saMaryList = {sMaryImg}

local iLaurCnt = 1
local sLaurImg = gzTextVar.zEntities[i].sQueryPicture
local saLaurList = {sLaurImg}
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

local iJessCnt = 1
local sJessImg = gzTextVar.zEntities[p].sQueryPicture
local saJessList = {sJessImg}

local sSaraPath = "Root/Images/DollManor/Characters/Sarah"
local iSaraCnt = 1
local saSaraList = {sSaraPath}

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
    
    --Player is a statue:
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
    --Player is a claygirl:
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        
    --Player is a doll:
    else
        if(gzTextVar.iGameStage == 6) then
            TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
        end
    end
    
    --Pulse ends here.
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Looking at.
if(sInstruction == "look sarah") then
    
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.iGameStage ~= 5) then
            TL_SetProperty("Append", "A book which has called itself Sarah Lee-Anne, the same name you found on the journal pages in the manor. It is capable of hearing and understanding you, and replies by writing in its pages." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Dumb-dumb.
        else
            TL_SetProperty("Append", "A book. It has words in it. Yep! You know how books work!" .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "A book. You have not been commanded to know what it is. You do not." .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "A book. You have no interest in books." .. gsDE)
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "A book. You have no interest in books." .. gsDE)
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "A book. You have no interest in books." .. gsDE)
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "A book. You have no interest in books." .. gsDE)
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage ~= 6) then
            TL_SetProperty("Append", "A book. You have no interest in books." .. gsDE)
        
        --Phase 6:
        else
        TL_SetProperty("Append", "A book which has called itself Sarah Lee-Anne, the same name you found on the journal pages in the manor. It is capable of hearing and understanding you, and replies by writing in its pages." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        end
    end

--Talking to.
elseif(sInstruction == "talk sarah") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --In Phase 3, the player is trying to find the journal entries.
        if(gzTextVar.iGameStage == 3) then
        
            --Determine how many pages are needed. The amount needed goes down on each difficulty.
            -- Easy is 7, Normal is 10, Hard is 12.
            local iPagesA = 2
            local iPagesB = 4
            local iPagesC = 8
            local iPagesD = 12
        
            --Easy. Reduces all page requirements, cap is 7.
            if(gzTextVar.sDifficulty == "Easy") then 
                iPagesA = 1
                iPagesB = 3
                iPagesC = 6
                iPagesD = 7
        
            --Normal. Reduces all page requirements, cap is 10.
            elseif(gzTextVar.sDifficulty == "Normal") then 
                iPagesA = 2
                iPagesB = 4
                iPagesC = 8
                iPagesD = 10
            end
        
            --Check page count. Responses depend on the number of pages in our possession.
            if(gzTextVar.iTotalPagesFound < iPagesA) then
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'How am I doing, Sarah?'" .. gsDE)
                fnDialogueCutscene("You", iSaraCnt, saSaraList, "The page writes. 'This helps a little, but there are many more pages to go. Please, search for them.'" .. gsDE)
                fnDialogueFinale()
            
            elseif(gzTextVar.iTotalPagesFound < iPagesB) then
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'How am I doing, Sarah?'" .. gsDE)
                fnDialogueCutscene("You", iSaraCnt, saSaraList, "The page writes. 'Good work, my memories are becoming less fuzzy. But, you still have many to go.'" .. gsDE)
                fnDialogueFinale()
            
            elseif(gzTextVar.iTotalPagesFound < iPagesC) then
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'How am I doing, Sarah?'" .. gsDE)
                fnDialogueCutscene("You", iSaraCnt, saSaraList, "The page writes. 'I think you've got over half of the pages. Thank you, Mary. Please, continue.'" .. gsDE)
                fnDialogueFinale()
            
            elseif(gzTextVar.iTotalPagesFound < iPagesD) then
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'How am I doing, Sarah?'" .. gsDE)
                fnDialogueCutscene("You", iSaraCnt, saSaraList, "The page writes. 'You're almost there. I commend your courage. I can almost remember everything.'" .. gsDE)
                fnDialogueFinale()
            
            --Finale.
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'How am I doing, Sarah?'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'Hold the pages up, please.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You put the pages next to Sarah. The scribbling sound can be heard. They seem... saddened, somehow." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You all right, Sarah?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Did it work? Do you remember?'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I - I'm so sorry, Mary. I didn't mean for this to happen.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'She's that lady... Sarah is Pygmalie...'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I was not trying to mislead you. I did not know. I am sorry...'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I think Pygmalie is my body. I think I did this to you, in a way.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I didn't want it to get me, so I stuffed myself in here. But I left myself out there to make more dolls and things. What have I done?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You have to know some way to escape. There's got to be a way out.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'There is, I know there is. The key lies in the symbols. They are the language. The thing knows it and it uses it to change this place.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What is the thing?'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I don't know, I have never seen it. It could be anything, it could be nothing. All I know is that it made this place, and made the rules it hews to.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'If you do not escape, you will be captured and consumed. Either as you are, or a construct. I am sorry.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Come on, this thing is useless. We're better off finding a match and burning the place down.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'You cannot. This place is not a mere building made of wood and stone. It is far more.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'The creature is not mortal, it is not of our reality. It draws people in and then consumes them. But I think it becomes weaker after it has eaten.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'But... but... Then it'd have to eat one of us...' Lauren says, fighting back tears." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You put your hand on his shoulder. He looks up at you." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hey, we're not going to have someone get eaten just so we can make a run for it.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'That is the only way. If it is fully sated, there may be a way out. Its attention may lapse. Or, there may be no escape. Maybe you are doomed.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Sarah, you've got a downer attitude.' Jessie says. You give her a smirk, but can't help but wonder if the book is right." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If there's a way out, how can I sacrifice someone?'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, no! Don't!' Lauren shouts. You pat him on the head." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's a hypothetical, squirt. I'm not really gonna do it.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'The altar, upstairs. I will show you the symbols.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "A set of symbols appear on the page. You read them carefully, committing them to memory." .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'But the symbols are nothing until they are written on the body to be given. The thing will find them and take them, then.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'But your body - Pygmalie - is in that altar room. And there's at least a hundred dolls in there.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I am sorry. The dolls are under her command, completely. There is nothing more I can do.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'If we could get close to her...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Not likely. Not unless...'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary...' Lauren says, tugging at your shirt. He gives you a hug, burying his face in your shirt." .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Don't you dare say it.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Just a second... Sarah...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You pick up Sarah and angle the book such that only you can read it." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Where can I find a book like this one?'" .. gsDE)
                
                if(gzTextVar.sManorType == "Demo" or gzTextVar.sManorType == "Simple") then
                    fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I would guess the chapel on the western side of the manor. The study there may have one.'" .. gsDE)
                    fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'If you search there, you should find a book large enough to record a long story in.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You put Sarah back on the desk." .. gsDE)
                    
                elseif(gzTextVar.sManorType == "Normal") then
                    fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'That is guarded. All books that were large enough to hold a person like you were destroyed. By me. I am sorry.'" .. gsDE)
                    fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'But I left one other, in case this one was not large enough for me. I left it in the care of my most powerful creation.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Where?' you ask." .. gsDE)
                    fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'Atop the bell tower in the chapel is her lair. It will be there. She will not simply let you take it.'" .. gsDE)
                    fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'The key is in the secret compartment in the back of the desk you found me on. Good luck, Mary.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You put Sarah back on the desk. You fiddle with the drawer under it until you see the indent in the wood. There is a key behind it. You take it with you." .. gsDE)
                    
                    --Immediately add the club key to the player's inventory.
                    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
                    local p = gzTextVar.gzPlayer.iItemsTotal
                    gzTextVar.gzPlayer.zaItems[p] = {}
                    gzTextVar.gzPlayer.zaItems[p].sUniqueName = "club key"
                    gzTextVar.gzPlayer.zaItems[p].sDisplayName = "club key"
                    gzTextVar.gzPlayer.zaItems[p].bCanBePickedUp = true
                    gzTextVar.gzPlayer.zaItems[p].sHandlerScript = gzTextVar.sRootPath .. "Item Handlers/Keys/Club Key.lua"
                    gzTextVar.gzPlayer.zaItems[p].sIndicatorName = "Null"
                    gzTextVar.gzPlayer.zaItems[p].bIsStackable = false
                    gzTextVar.gzPlayer.zaItems[p].iStackSize = -1
                    gzTextVar.gzPlayer.zaItems[p].iJournalPage = -1
                    gzTextVar.gzPlayer.zaItems[p].bIsClaygirlTrapped = false
                    gzTextVar.gzPlayer.zaItems[p].sClaygirlName = "Null"
                    gzTextVar.gzPlayer.zaItems[p].saClaygirlList = {}
                    
                    --Locality.
                    fnBuildLocalityInfo()
                
                end
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren, this room is safe. Can you stay here with Jessie and Sarah?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Excuse me?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The dolls won't come in here. They don't know this room exists, it's secret even to them.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'And just what are you going to do?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Not give up.'" .. gsDE)
                fnDialogueFinale()
        
                --Activate phase 4.
                LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/003 Set To Phase 4.lua")
            
            end
    
        --In phase 4, dialogue changes.
        elseif(gzTextVar.iGameStage == 4) then
        
            --Check if Mary has the empty book yet.
            local bHasEmptyBook = false
            for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
                if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "empty storybook") then
                    bHasEmptyBook = true
                    break
                end
            end
        
            --No book.
            if(bHasEmptyBook == false) then
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'Sarah, how have you been?'" .. gsDE)
                fnDialogueCutscene("You", iSaraCnt, saSaraList, "The page writes. 'Your brother Lauren is good company. I enjoy talking with him. He speaks highly of you.'" .. gsDE)
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'If you've heard what he says about me...'" .. gsDE)
                fnDialogueCutscene("You", iSaraCnt, saSaraList, "The page writes. 'I know what it is that you intend to do. Check the western chapel's study. There may be a proper receptacle there.'" .. gsDE)
                fnDialogueCutscene("You", iMaryCnt, saMaryList, "'Thanks, Sarah.'" .. gsDE)
                fnDialogueFinale()
            
            --Hab da book? Begin phase 5!
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sarah...'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'You have the book. I was correct about your intentions.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I knew it. I absolutely knew it.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'One of us has to.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Has to what? One of us has to what?'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'...'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren... I'm going to get us out of here.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'He's a big boy. Tell him straight. He deserves it.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Tell me what straight? What?' he asks as his eyes fill with tears." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You kneel down and wipe his eyes. 'I'm going to become a doll.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'M-M-M-Mary! No!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'm going to put myself into this book, just like Sarah did. And then I'm going to become a doll.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And when I come after you, you're going to put me back together. Can you do that for me?'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren sniffs and cries. He can't speak, so he nods instead." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You have to be very brave. I'm counting on you to take care of Jessie for me.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Don't be such a nerd, nerd' Jessie remarks sarcastically. She quickly wipes a tear away." .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Sarah, what does Mary have to do?'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'I will show you the symbols. Write them on your body and place the book to your temple.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'To undo the effects, you will need to place your head in between the pages of the book. Or rather, your body will need to.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I won't remember who I am once this starts. Don't go easy on me, Jessie'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'As if! I'm gonna cream you!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Lauren, you're on book duty. Once I knock her down, your job is to put her back together.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren continues to cry." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No time like the present. Sarah, let's get to it.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You draw the symbols and place the book to your temple. Then, it all goes dark." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "..." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You wake up. You were doing something, but you can't remember what. There are people near you." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Uhhh, hello? What was I doing?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh, hi. Your name is Mary, and you were just about to go off and find a plastic doll girl. Ask her to take you to her creator, tell her you're volunteering.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I was? Okay!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "It seems like a strange thing to do, but the nice girl seemed like an honest sort. You better go do that." .. gsDE)
                fnDialogueFinale()
    
                --Begin phase 5.
                LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/004 Set To Phase 5.lua")
                
                --Remove the storybook.
                for p = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
                    if(gzTextVar.gzPlayer.zaItems[p].sDisplayName == "empty storybook") then
                        fnRemoveItemFromInventory(p)
                        break
                    end
                end
    
            end
    
        --In phase 5, dialogue changes.
        elseif(gzTextVar.iGameStage == 5) then
            TL_SetProperty("Append", "You big silly! You can't talk to books!" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "Well, okay. You can talk to books, but books can't talk back. How silly you are!" .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "You are a statue. You cannot speak to books." .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "You have no interest in speaking to a book." .. gsDE)
    
    --Doll version.
    else
        if(gzTextVar.iGameStage ~= 6) then
            TL_SetProperty("Append", "You have no interest in speaking to a book." .. gsDE)
        else
            
            --Jessie and Lauren are humans:
            if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Human") then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sarah. Any advice?'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'Pretend to love her and obey her, as though you were her doll.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'That is what she will think you are. Follow her commands. She will suspect you if you don't.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks. Whatever happens in there... Thanks for all your help.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'Considering what I have done, I do not think myself worthy of your thanks.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're helping us now, and that's enough.'" .. gsDE)
            
            --Betrayal.
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You open the book to see if Sarah saw what you did." .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'What is your goal? Why go through all that to just surrender yourself?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I have my reasons.'" .. gsDE)
                fnDialogueCutscene("You",    iSaraCnt, saSaraList, "The page writes. 'Either way, it ends. Do what you will.'" .. gsDE)
            end
        end
    end

end