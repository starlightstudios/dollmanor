-- |[ ======================================== Claygirl ======================================== ]|
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Make sure the current entity is valid. This is skipped during command building.
if((gzTextVar.iCurrentEntity < 1 or gzTextVar.iCurrentEntity > gzTextVar.zEntitiesTotal) and TL_GetProperty("Is Building Commands") == false) then return end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "attack",    "attack " .. sEntityName)
        TL_SetProperty("Register Popup Command", "talk",      "talk "   .. sEntityName)
    
    --Player is a statue:
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
    --Player is a claygirl:
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
    --Player is a doll:
    else
        TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    
    end
    return
end

-- |[ ======================================== Examining ======================================= ]|
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A girl made of flowing clay. She has no face, and flows over herself as she moves." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A girl made of clay. She is not an intruder. You will IGNORE HER. You will FIND THE INTRUDERS." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A fellow claygirl. Like you, she has no face or true form, and reshapes herself constantly. She is hunting for humans, as you are." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A girl made of flowing clay. She has no face, and flows over herself as she moves. You wave hi to her with your useless hands." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A girl made of flowing clay. She has no face, and flows over herself as she moves. She is wholly uninteresting." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A fellow claygirl. Like you, she has no face or true form, and reshapes herself constantly. She is hunting for humans, as you are." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "One of the girls made of clay. Not as refined as you, but just as obedient to your creator. Maybe you can play with her later!" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Phase 6:
        else
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "One of the girls made of clay. Because you are a doll, she doesn't suspect a thing. You give her an idle smile and giggle slightly." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
    end
    
-- |[ ======================================= Attacking ======================================== ]|
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt to fight." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Run the target's AI with this flag set.
        gzTextVar.bIsTriggeringFights = true
        gzTextVar.bTurnEndsWhenCombatEnds = true
        
        --Fast-access variables.
        local i = gzTextVar.iCurrentEntity
        
        --If there's no handler, do nothing.
        if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
            
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
            fnClaygirl_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
            fnDoll_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
            fnTitan_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
            fnGoop_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
            fnStranger_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
            fnJessie_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
            fnLauren_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
            fnPygmalie_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
            fnSarah_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
            fnDollStationary_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
            fnDollStationaryCombat_AI(i)
        end
        
        --Clean.
        gzTextVar.bIsTriggeringFights = false
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "The claygirl is not an intruder. IGNORE THE CLAYGIRL. FIND THE INTRUDERS. BRING THEM TO THE LIGHT." .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "You have not been ordered to attack the claygirl, and it would be pointless anyway. You are formless and cannot be harmed." .. gsDE)
    
    --Doll version.
    else
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Append", "You could never bring yourself to attack one of your sisters unless you creator directly ordered it." .. gsDE)
    
        --Phase 6:
        else
            TL_SetProperty("Append", "Attacking the clay girl would blow your cover. Best not to. She doesn't seem hostile in any case." .. gsDE)
        end
        
    end

-- |[ ======================================== Dialogue ======================================== ]|
--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt even to speak." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "'Hello? Can you hear me? Can you speak?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The clay girl shambles towards you. If she can hear you at all, she gives no indication." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Statue.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "YOU ARE A STATUE. YOU DO NOT THINK. YOU DO NOT SPEAK. FIND THE INTRUDERS. BRING THEM TO THE LIGHT. You obey the light." .. gsDE)
    
    --Claygirl.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "You attempt to speak to the claygirl. You have no mouth, throat, or lungs, so no sound comes out." .. gsDE)
    
    --Rubber.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "You make a series of squeaking noises as your rubbery body expands and contracts. This is as much communication as you can do. The claygirl is no better." .. gsDE)
        
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The claygirl burbles towards you, but keeps her distance and goes around. Your touch is freezing, and she doesn't want to solidify." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The claygirl sidles up next to you. She is unable to speak, but seems to be friendly." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "'Hello, clay sister. How are you?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The claygirl attempts to make a thumbs-up, but her slippery clay hands struggle. You take her meaning." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "'I had a great idea for a sculpture to shape yourself into. Come find me later and I'll tell you all about it.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The claygirl seems happy and will find you later." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Append", "You smile at the clay girl. 'Hello, clay girl. Do you want to play?'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "The clay girl shakes her head and gives a definite 'stop' with her hand. You smile anyway." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "'You're right! We should find the humans first! Wow, you're so smart!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "The clay girl nods enthusiastically. You can play with her later!" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
        --Phase 6:
        else
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "The clay girl approaches you and points at Jessie. Jessie winces at the attention." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "You give a laugh, trying to sound genuine. 'Oh, don't worry! I'm taking them to our creator right now!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'You'll have to wait, I get to play with them first! Hee hee hee!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "The clay girl nods. She seems... jealous? She has no face, so it's hard to say." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end

    end

end