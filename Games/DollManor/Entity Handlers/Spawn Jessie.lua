-- |[ ====================================== Spawn Jessie ====================================== ]|
--Spawns Jessie at the start of the game.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1
gzTextVar.iJessieIndex = gzTextVar.zEntitiesTotal

--Shorthand
local i = gzTextVar.iJessieIndex

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].bIsProtected = true
gzTextVar.zEntities[i].sDisplayName = "Jessie"
gzTextVar.zEntities[i].sQueryName = "jessie"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Jessie"
gzTextVar.zEntities[i].sUniqueName = "jessie"
gzTextVar.zEntities[i].sIndicatorName = "Jessie"
gzTextVar.zEntities[i].sLocation = "Main Hall"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sState = "Human"
gzTextVar.zEntities[i].bMovesRandomly = true
gzTextVar.zEntities[i].iSkinColor = LM_GetRandomNumber(0, 3)
gzTextVar.zEntities[i].iStayStillTurns = 0
gzTextVar.zEntities[i].sLastDoorDir = "P"
gzTextVar.zEntities[i].sSpecialIdentifier = "Jessie"

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Jessie.lua"
gzTextVar.zEntities[i].sAIHandlerHostility = "Jessie"
gzTextVar.zEntities[i].sAIHandlerPostTurn = "Jessie"
gzTextVar.zEntities[i].sAIHandlerPrimary = "Jessie"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"None"}
gzTextVar.zEntities[i].sFirstSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sLastSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sInDisguise = "Null"

--Combat Properties
gzTextVar.zEntities[i].zCombatTable = {}
gzTextVar.zEntities[i].zCombatTable.iHealth = 40
gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4, 240, 0}
gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = { 10, 6, 180, 0}
    
--Weaknesses. Jessie has no weaknesses.
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses = {}
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Fire] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Water] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Wind] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Earth] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Life] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Death] = 0

--Indicator.
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
gzTextVar.zEntities[i].iIndicatorX = 1
gzTextVar.zEntities[i].iIndicatorY = 3
gzTextVar.zEntities[i].iIndicatorC = ciCodeFriendly
gzTextVar.zEntities[i].iIndicatorH = 0
TL_SetProperty("Register Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, gzTextVar.zEntities[i].iIndicatorX, gzTextVar.zEntities[i].iIndicatorY, gzTextVar.zEntities[i].iIndicatorC)