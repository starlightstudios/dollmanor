-- |[ ========================================== Goop ========================================== ]|
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Make sure the current entity is valid. This is skipped during command building.
if((gzTextVar.iCurrentEntity < 1 or gzTextVar.iCurrentEntity > gzTextVar.zEntitiesTotal) and TL_GetProperty("Is Building Commands") == false) then return end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
    
    --Player is a non-human:
    else
        TL_SetProperty("Register Popup Command", "talk", "talk " .. sEntityName)
    
    end
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. It moves on its own, and jiggles slightly with each motion. It doesn't seem to be able to see you." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. It was created by the same person who commands your will." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. It's not as smart as you, or as cute, but rubber has its own charm." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. You don't care about it." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. It jiggles as it moves, but it not a threat to you." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Rubber Mary.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. You are the same, except you cannot dissolve into a formless mass." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. If you cared, you could freeze it solid. You don't." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "A pool of semi-intelligent goop. It can be shaped into anything. Maybe you should do that later." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Append", "A pool of semi-intelligent goop. It transforms humans into rubber toys that love to be played with." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Phase 6.
        else
            if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Human") then
                TL_SetProperty("Append", "A pool of semi-intelligent goop. While unintelligent, it seems to recognize your 'claim' and does not move to subsume your friends." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            else
                TL_SetProperty("Append", "A pool of semi-intelligent goop. While unintelligent, it recognizes you both serve the same creator, and leaves you be." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
        end
    end

--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt even to speak." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "You should not be able to see this dialogue!" .. gsDE)
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "The puddle of goop shifts and jiggles near you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Unregister Image")
        TL_SetProperty("Append", "LOCATE THE INTRUDERS. CAPTURE THE INTRUDERS. OBEY." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You ignore the puddle of goop. You must capture the intruders. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Claygirl.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "The puddle of goop shifts and jiggles near you. You try to mimic its movements." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The goop is slightly playful. You mix a bit of your clay with it, and enjoy the feeling." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Your clay disentangles after a few seconds and slithers back to you. The goop is not smart enough to enjoy playing with you." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Rubber Mary.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "The goop is incapable of speech, nor complex thought. Neither are you. You grin emptily and stare ahead as the goo slides past you." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You don't care about anything, much less talking to a puddle of goop." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "'Goop. What are you?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The goop says nothing, and is incapable of speech." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Very well.' Seems you got the answer you were expecting." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Append", "You giggle at the silly goop. It's not smart enough to talk! What a silly dolly you are!" .. gsDE)
        
        --Phase 6.
        else
            TL_SetProperty("Append", "The goop is incapable of speech, and does not pose a threat to you." .. gsDE)
        end

    end

end