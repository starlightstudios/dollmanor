-- |[ ======================================== Eileen ========================================== ]|
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Other.
local e = gzTextVar.iEileenIndex
local l = gzTextVar.iLaurenIndex
local j = gzTextVar.iJessieIndex
    
--Fast-access strings.
local iMaryCnt = 1
local sMaryImg = gzTextVar.gzPlayer.sQuerySprite
local saMaryList = {sMaryImg}

local iEilnCnt = 1
local sEilnImg = gzTextVar.zEntities[e].sQueryPicture
local saEilnList = {sEilnImg}

local iLaurCnt = 1
local sLaurImg = gzTextVar.zEntities[l].sQueryPicture
local saLaurList = {sLaurImg}

local iJessCnt = 1
local sJessImg = gzTextVar.zEntities[j].sQueryPicture
local saJessList = {sJessImg}

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
    
    --Pulse ends here.
    return
end

-- |[ ====================================== Examination ======================================= ]|
--Looking at.
if(sInstruction == "look eileen") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "A plastic doll girl in a princess costume. Unlike the other dolls, she seems cognizent." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "One of the dolls in the manor. She is not an intruder. You will find the intruders." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "One of the dolls in the manor. You should play with her later!" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "One of the dolls in the manor. You should play with her later!" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "One of the dolls in the manor." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "One of the dolls in the manor. She lives above the chapel and sometimes visits." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "One of your adorable doll sisters. Eileen is one of the smartest dollies!" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
    end
    
-- |[ ======================================== Attacking ======================================= ]|
--Attacking.
elseif(sInstruction == "attack eileen") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "Eileen doesn't seem threatening at all." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to attack the doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to attack the doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to attack the doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to attack the doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to attack the doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to attack Eileen! She's your sister!" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    end
    
-- |[ ========================================= Playing ======================================== ]|
--Playing.
elseif(sInstruction == "play eileen") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "Eileen smiles blankly at you. She doesn't want to play." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "DO NOT PLAY. FIND THE INTRUDERS. BRING THEM TO THE LIGHT. OBEY." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You'll have to play with her later, you have some important things to do first." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You'll have to play with her later, you have some important things to do first." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You have no desire to play with anyone." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You should really find the humans before you play with Eileen." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
        TL_SetProperty("Register Image", "Eileen", sEilnImg, ciImageLayerDefault)
        TL_SetProperty("Append", "You raise your hand. Eileen raises hers." .. gsDE)
        TL_SetProperty("Create Blocker")
        local iRoll = LM_GetRandomNumber(1, 3)
        if(iRoll == 1) then
            TL_SetProperty("Append", "You throw rock, Eileen throws paper! Gah, she always wins!" .. gsDE)
            TL_SetProperty("Create Blocker")
        elseif(iRoll == 2) then
            TL_SetProperty("Append", "You throw paper, Eileen throws scissors! Gah, she always wins!" .. gsDE)
            TL_SetProperty("Create Blocker")
        else
            TL_SetProperty("Append", "You throw scissors, Eileen throws rock! Gah, she always wins!" .. gsDE)
            TL_SetProperty("Create Blocker")
        end
        TL_SetProperty("Append", "You'll get her next time..." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    end

-- |[ ======================================== Dialogue ======================================== ]|
--Talking to.
elseif(sInstruction == "talk eileen") then
    gbHandledInput = true
    
    --Flag to handle savegames.
    if(gzTextVar.bMetEileenWhileDumb == nil) then gzTextVar.bMetEileenWhileDumb = false end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then

        --If Mary is "Dumb", this plays out.
        if(gzTextVar.iGameStage == 5) then
        
            --Has not met Eileen:
            if(gzTextVar.bMetEileen == false) then
                gzTextVar.bMetEileenWhileDumb = true
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, hello there, human. Mary, is it?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hi!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'... You aren't wondering I know your name?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Mary is my name? Well that explains a lot. Thanks!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's because of a symbol I can see on you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Wow! You can read?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'You can't?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Dunno, never tried it! You're really nice.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hmmm, very, very interesting. I can't wait to see how this plays out.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Good luck, Mary.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks. Oh, uh, I'm a little lost. I was supposed to do - volunteer? Something like that.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, well, simply speak to any of my doll sisters. They can help you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yay! Okay!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'... Truly a novelty.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "You decide to walk away as the doll girl is now just shaking her head and muttering." .. gsDE)
                fnDialogueFinale()
        
            --Has met Eileen.
            else
                gzTextVar.bMetEileenWhileDumb = true
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary, still holding your convictions?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nope!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh? Why not?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Because I don't know what a conviction is, and my hands are empty.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'... Mary, do you remember me?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nope!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I see. Do you remember anything?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nope!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Not even your own name?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nope!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'But I called you Mary. That's your name.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You seem nice, so I trust you. You probably know better than I do.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Everyone is just so nice around here.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'That at least explains the book's true purpose. I understand a little more now.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I suppose the creator will have to take that from me, as it may confound my mission.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This creator sounds really nice.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh she is. You should meet her.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Simply speak to any of my sisters, they will take you right to her.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The doll girls are your sisters?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Yes.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I suppose I'm helping you in a way. I wonder if this is the right thing to do? Even her will in me is unsure.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Do come back and play with me when she's done with you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! Thank you!'" .. gsDE)
                fnDialogueFinale()
            end
            return
        end

        --First time.
        if(gzTextVar.bMetEileen == false) then
            gzTextVar.bMetEileen = true
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, hello there, human. Mary, is it?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'How did you know my name?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's written on you in a symbol you can't see. You're doing well.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Just who are you, and why aren't you attacking me?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Eileen. I would simply love to attack you, inject you, turn you into a doll. That'd be spectacular...'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'But I don't feel like it right now.'" .. gsDE)
            if(gzTextVar.gzPlayer.bHasSeenTestament == true) then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'How reassuring. Are you the same Eileen that left those messages?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I'm not sure. There's more than one Eileen in the world, you know.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'How are you not sure?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I don't really remember anything before becoming a doll, but I'm sure I wasn't always a doll. After all, I've transformed others into dolls.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's not a logical leap to assume that happened to me.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Eileen doesn't seem dangerous for the time being." .. gsDE)
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'How reassuring. So what do you feel like doing?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh I'd just love to chat. Stay a moment. My sisters don't come up here, this place is not theirs.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Eileen doesn't seem dangerous for the time being." .. gsDE)
            end
            fnDialogueFinale()
        
        --Second:
        elseif(gzTextVar.iEileenTalk == 0) then
            if(gzTextVar.iGameStage < 4) then
                gzTextVar.bHeardRiddle = true
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'So, Mary, what can I do for you?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nothing, really. I was just surprised a doll didn't attack me on sight.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, oh. You're not that far along yet. You really should come back later.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Why?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'There's some very interesting things I'd like to talk to you about. Try not to lose your intelligence before then, Mary.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's so rare to have a real conversation.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What do the other dolls talk about?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'... Games to play, the weather, or tasks our creator has set for us.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What tasks does she set for you?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh. Oh oh. I'm not supposed to say, it's very naughty.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Yet I want to tell you so badly. Maybe I can give you a hint, like it's a game?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Are you talking to me?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Yes! I'll give her a hint! Maybe we can see how well she does!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary! Mary! I made a poem for you!'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It is my task to replace these four,'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'When those who cannot cross the door,'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Meet a stony fate, as was written in the book,'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I even marked them on my map. Take a look!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'That's a pretty nice poem.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Yay! Okay Mary, we're now officially playing a game. Solve my riddle, if you can!'" .. gsDE)
                fnDialogueFinale()
                return
            end
            
            --Flag.
            gzTextVar.iEileenTalk = 1
            
            --Did not hear the riddle.
            if(gzTextVar.bHeardRiddle == false) then
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'So, Mary, what can I do for you?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What are you doing here?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'This is my room. It's where I live.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And what do you do, here?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I play with my sister dolls when I feel like it, or go on walks, or take care of the gardens.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's been raining for a while, and I haven't seen the sun since I can remember, but the flowers bloom so prettily nonetheless.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'... Do you know anyone named Sarah?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "Eileen looks at you. Her normally absent stare focuses. 'You know a great deal, Mary. I was right about you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Answer the question.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I know the name. In fact, I found the crystals. I found the room, the secret room. My creator even let me keep that knowledge.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Why?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I don't know, and good dollies don't ask. Sometimes she tells me to go hide the crystals again, though.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'But why? Why is there a way to get past the traps?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's part of the rules.'" .. gsDE)
                fnDialogueFinale()
            
            --Did hear the riddle.
            else
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'You've come back. Have you solved my riddle?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sarah.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "Eileen looks at you. Her face goes from smiling to stern in the blink of an eye. 'You know a great deal, Mary. I was right about you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You gave me a hint. How much do you know?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I know the name. In fact, I found the crystals. I found the room, the secret room. My creator even let me keep that knowledge.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Why?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I don't know, and good dollies don't ask. Sometimes she tells me to go hide the crystals again, though.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'So other people find the crystals, too, and you return them when they do? Why?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It's part of the rules.'" .. gsDE)
                fnDialogueFinale()
            end
        
        --Third:
        elseif(gzTextVar.iEileenTalk == 1) then
            gzTextVar.iEileenTalk = 2
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary, why are you here talking to me?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I like being a doll. I have lots of fun, I'm just adorable all the time, and I have a great big family that loves me. Why don't you just give up?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No thank you.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'It was worth a try. I have to keep my creator satisfied.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You mentioned rules. What are the rules?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I only know that it has to be fair.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What does?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "Eileen thinks for a moment. She seems to be struggling to find the words." .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Every time you go somewhere, you push off something else. To jump, you push down on the ground, and the ground pushes up on you.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'You can create anything you want in this place, but you have to create something else, too. It has to be fair.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And is that how all of this came about?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Maybe, maybe not. I know you think I know more than that, but I really don't.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Despite everything, I am a plastic doll dressed like a princess. If you want to play checkers, I'm game. Great wisdom? Look elsewhere.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I know some of the symbols. Sarah showed me them.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Yes, and I can see them on you. They're written inside your head.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Now that I think of it, can you see anything behind my eyes?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You look closely at Eileen's plastic eye. You can make out the hint of a symbol that looks like an upside-down Y with a line through it." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There's a symbol there.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I never thought I'd learn something from a human!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'But what does it mean?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "Eileen shrugs. 'I don't know what they do, just that they're there.'" .. gsDE)
            fnDialogueFinale()
        
        --Fourth, repeats.
        elseif(gzTextVar.iEileenTalk == 2) then
            if(gzTextVar.bRungBell == false) then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Eileen, where can I find a book like the one Sarah is in?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I can't help you,' Eileen suddenly snaps. She looks furious." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'm sorry, did I say something wrong?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'No, no, I apologize. It's not very dignified to raise my voice like that, is it?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'But when you asked that, my creator told me not to answer.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Does that mean she knows where I am?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'No, no. Her will is etched into my mind by the symbols.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I suppose I'll keep looking.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Good luck, Mary. And when you become a doll, please come play with me. I hope the creator lets you keep your intelligence, it's hard to find good conversation.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'm not giving up.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'They all say that. Then they become dolls.'" .. gsDE)
                fnDialogueFinale()
            else
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'You rang the bell? Quite a racket you made.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I suppose the book being hidden inside makes sense. I've never heard the bell ring before.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're not going to stop me?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'The creator wants me to, but as I said, I really don't feel like it. I'm sure one of the other dolls will get you.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'You think you're making progress, but you aren't. There is no way out. You'll be a doll soon.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'You'll get tired, you'll go to sleep, and you'll wake up just like me.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And if I don't?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Nobody has ever escaped. Trust me, you're not the first to try. Still, good luck, Mary. I wish you the best.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'... I suppose I'll be assigned to put the book back in the bell when you are turned. Or maybe you will? I hope you turn out cute!'" .. gsDE)
                fnDialogueFinale()
            end
        end
        
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        if(gzTextVar.bMetEileen == false) then
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hmm. You're in the wrong place if you're looking for humans.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Let's see... Kneel. Obey.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "KNEEL. OBEY. You kneel before the doll. Statues do not think, they obey." .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I was right! Well, as much fun as it would be to have you be my playmate, go find the humans.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "FIND THE HUMANS. You must find the humans. You must bring them to the light. You obey." .. gsDE)
            fnDialogueFinale()
        else
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Interesting. You became a statue? That's not as fun, the statues don't really play.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'They don't do much of anything, actually. Oh well. Go find the humans, Mary. Or, well, statue. I suppose you don't even remember who you were.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You say nothing. Statues do not speak. You must find the humans and bring them to the light." .. gsDE)
            fnDialogueFinale()
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
        --Setup.
        local l = gzTextVar.iLaurenIndex
        local j = gzTextVar.iJessieIndex
        if(gzTextVar.bMetEileen == false) then
            
            --Have to chase down the humans.
            if(gzTextVar.zEntities[j].sState ~= "Claygirl" or gzTextVar.zEntities[l].sState ~= "Claygirl") then
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Well well well, what do we have here? What brings you to my home, clay girl?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You cannot speak, but it seems the doll understands your intent." .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'There's no intruders in here. Good luck finding them, though.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You shuffle away, determined to find the humans and cover them in clay." .. gsDE)
                fnDialogueFinale()
            
            --Already turned the humans:
            else
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Well well well, what do we have here? What brings you to my home, clay girl?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You cannot speak, but it seems the doll understands your intent." .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Are these your friends?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You nod." .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Conversations with a claygirl are always so one-way. Well, if you'd like to play something...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You shake your head. You think for a moment." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You reform your 'face' into a clock, then wind the 'arm' forward a bit." .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, play something later? Well, you are a lot more intellectual than your compatriots.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You give her a thumbs up. She claps her hands and giggles with delight." .. gsDE)
                fnDialogueFinale()
            end
        else
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary? Well, there are worse fates than becoming one of the clay girls.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I suppose we won't be having any interesting conversations, but if you want to play checkers, I'll be here.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You should play with Eileen later! She's a good friend." .. gsDE)
            fnDialogueFinale()
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        if(gzTextVar.bMetEileen == false) then
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, I've never seen the rubber slime transform someone. You turned out pretty well.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You cannot speak. You smile blankly." .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'At least you're happy. Good for you. Go show the creator how well you turned out.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You nod and walk away, squeaking the whole while." .. gsDE)
            fnDialogueFinale()
        else
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh Mary, how could you fall for the rubber goop? It's so easy to avoid!'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Or was it intentional? I suppose you'll make a good pool toy. Wait, which of you is Mary?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'... Maybe you're all Mary. Oh well. Go show the creator how you turned out.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You nod and walk away, squeaking the whole while." .. gsDE)
            fnDialogueFinale()
        end
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        if(gzTextVar.bMetEileen == false) then
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'An ice sculpture. Novel.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You are?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Eileen. You?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Mary.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Do you have anything to talk about?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Well, off with you then. Come back when you want to play. If you ever do...'" .. gsDE)
            fnDialogueFinale()
        else
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'So you turned to ice. Unfortunate.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It doesn't matter.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I suppose not, in the grand scheme. So what will you do now?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Show the creator, and then. Nothing.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Unsurprising, then. Goodbye, Mary.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.bMetEileen == false) then
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, hello. Are you one of the gallery admirers?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yes. And you?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Eileen. I suppose we'll become acquainted soon enough.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Do you visit the chapel often?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'All the time! I hope you'll be better conversation than the lady in the trench coat.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'She's not so bad, just don't get in her way.'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Indeed. I seem to recall doing so once. Most unpleasant.'" .. gsDE)
            fnDialogueFinale()
        else
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Ah, glass. A fine fate.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What do you mean?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Do you recall our previous conversation?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No. Should I?'" .. gsDE)
            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, well. I look forward to seeing you in the chapel. Good day, Mary.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iMaryStringTrapState < 4) then
            if(gzTextVar.bMetEileen == false) then
                
                --Turned into a doll, still mindless.
                if(gzTextVar.iGameStage < 6) then
                    
                    --Check if Lauren and Jessie are humans.
                    local j = gzTextVar.iJessieIndex
                    local l = gzTextVar.iLaurenIndex
                    
                    --Location, Identity
                    local sJessieLocation = gzTextVar.zEntities[j].sLocation
                    local sLaurenLocation = gzTextVar.zEntities[l].sLocation
                    if(gzTextVar.zEntities[j].sState == "Human" and gzTextVar.zEntities[l].sState == "Human") then
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Are you looking to play?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sure! But it will need to be brief, there are humans in the manor.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Don't let me keep you. We have all the time in the world once the humans are like us.'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! We can play hopscotch later!'" .. gsDE)
                        fnDialogueFinale()
                    
                    --If both of them are blank dolls:
                    elseif(gzTextVar.zEntities[j].sState == "Blank Doll" and gzTextVar.zEntities[l].sState == "Blank Doll") then
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Are you looking to play?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sure! But I need to deliver these new sisters to the creator first.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, splendid. Well done. We can all play together!'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! You think of the game, I'll be back!'" .. gsDE)
                        fnDialogueFinale()
                    
                    --If either is a blank doll:
                    elseif(gzTextVar.zEntities[j].sState == "Blank Doll" or gzTextVar.zEntities[l].sState == "Blank Doll") then
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Are you looking to play?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sure! But I need to deliver this new sister to the creator first.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, splendid. Well done. We can all play together!'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! You think of the game, I'll be back!'" .. gsDE)
                        fnDialogueFinale()
                    
                    --If one is a human, and the other is a doll.
                    elseif(gzTextVar.zEntities[j].sState == "Human" or gzTextVar.zEntities[l].sState == "Human") then
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Are you looking to play?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'd love to, but I've heard there's a human in the manor.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh dear. I'll leave it to you, then. Come back whenever you like.'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! I hope you've sharpened your scrabble skills!'" .. gsDE)
                        fnDialogueFinale()
                    
                    --Else catcher.
                    else
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Looking to play a game?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Of course. Any suggestions?'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, I hadn't thought of any. Come back later.'" .. gsDE)
                        fnDialogueFinale()
                    
                    end
                
                --Turned into a doll, has a mind.
                else
                
                    --If we met Eileen while dumb:
                    if(gzTextVar.bMetEileenWhileDumb == true) then
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary! You came back!'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah! Want to play?'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Of course, but you need to deal with those humans first.'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yep! You always were the smart one. I was taking her to the creator!'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'And you came here first?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't you want to meet them?'" .. gsDE)
                        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'My name's Jessie, and this is Lauren. Are you friends?'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Sisters, actually. All dolls are sisters.'" .. gsDE)
                        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You're not as scary as the other dolls.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Scary? What's scary is how good I am at a game of darts.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Now take them to the creator, Mary. Don't dally!'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Right away, sister Eileen!'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Eileen bought your act. Best not to aggravate the situation." .. gsDE)
                        fnDialogueFinale()
                    
                    --General.
                    else
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Are you looking to play?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sure! But I have to deliver these humans to the creator first.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Don't let me keep you. I hope they turn out cute!'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "It seems Eileen believes you are a silly doll. Best not to aggravate the situation." .. gsDE)
                        fnDialogueFinale()
                    end
                end
            else
                if(gzTextVar.iGameStage < 6) then
                    fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, Mary. Are you still confident about your prospects?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Huh? What are you talking about?'" .. gsDE)
                    fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Do you remember being a human? Resolving to escape?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You giggle. 'I was always a dolly, silly! Who would ever want to escape?'" .. gsDE)
                    fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I see you've come around. But there are other humans out there, so go play with them, okay?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! See you later, Eileen!'" .. gsDE)
                    fnDialogueFinale()
                else
                    if(gzTextVar.bIsBetrayal == false) then
                        
                        if(gzTextVar.bMetEileenWhileDumb == true) then
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'So you've met the creator, have you?'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I have, and I am not so witless as before.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh thank goodness. Intelligent conversation is so rare to find. Finally we can have tea parties!'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Those will have to wait.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh yes, your scheme. It will fail, of course, because the creator is all powerful. Still...'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Don't tell me your plans, I want to hear all about it later!'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't you want to escape, Eileen? You could come with us.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Ha ha! No, I am quite happy here. Honestly, I can't understand why you'd want to leave.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'The world is a scary place, filled with uncertainty. This manor is much nicer.'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You only think that because of what she did to your mind. She made you this way.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'I know. Unlike most of the others, I am aware of what I am.'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And you don't want to escape, knowing what you know?'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'No, because she made me that way. She made me not care.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh what torture it would be to be trapped here. She is truly merciful to have changed my mind to enjoy this place.'" .. gsDE)
                            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I don't think you're going to convince her, Mary.'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We're getting out of here. Good day, Eileen.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "Eileen simply grins emptily and waves goodbye." .. gsDE)
                        
                        else
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh, Mary. Are you still confident about your prospects?'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I am.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'And you've retained your wits? Now that *is* interesting.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Don't tell me your plans, I want to hear all about it later.'" .. gsDE)
                            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? Do you know this doll somehow?'" .. gsDE)
                            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'She's a friend. Sort of.'" .. gsDE)
                            fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Do come by later once your little scheme fails. I'd love to play with all of you!'" .. gsDE)
                        end
                    else
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary? I see you've retained your wits, and you bear two unfinished sisters. Care to explain?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I have a plan.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Oh? You decided to join us after all? After that defiance?'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yes, of course! I just didn't want to be dumb like the others. Wouldn't you say I've earned it?'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Certainly. Very well done. Though your friends may not agree.'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yet.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hee hee!'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Whenever you're done, come by and we'll play cards.'" .. gsDE)
                        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Poor choice of game, I've gotten very good with these cards.'" .. gsDE)
                        fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Exactly! I love a challenge!'" .. gsDE)
                    end
                    fnDialogueFinale()
                end
            end
        
        --String trap state:
        else
            if(gzTextVar.bMetEileen == false) then
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Hello, sister! Are you looking to play?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Oh I'd love to, but we were on the way to present ourselves to the creator.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Where are my manners? This is Jessie, my bride...'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Hello, I am pleased to meet you, sister.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And Lauren. She was my brother, and now she is my sister.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I hope the creator lets us keep our memories. I want to stay with my family.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'We're all family now, Lauren. We'll always be together.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'If you are allowed to keep your wits, come play with me later! It's so rare to see a sister who can carry on a conversation.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Of course. Sisters, let's go.'" .. gsDE)
                fnDialogueFinale()
            else
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Mary, you turned out magnificently.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thank you, Eileen. You're pretty, yourself.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'These are my friends. I lured them into the same trap that transformed me.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Very clever! Well done.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This is Jessie. She is my bride.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Hello, I am pleased to meet you, sister.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'She had feelings for me when we were human. I hope the creator lets us stay together.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I hope so, too. You're cute together.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'And you are?'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Lauren, Mary's brother. Well, brother when we were humans.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Did you know Mary before?'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Yes, we met earlier. She's much cuter now.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're too kind. But, we must be off. The creator will want to see us.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'It was nice to meet you, Eileen.'" .. gsDE)
                fnDialogueCutscene("Eileen", iEilnCnt, saEilnList, "'Do come by and play later, sisters. We'll have so much fun!'" .. gsDE)
                fnDialogueFinale()
            end
        end
    end

end