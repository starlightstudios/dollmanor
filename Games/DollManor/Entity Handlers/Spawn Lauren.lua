-- |[ ====================================== Spawn Lauren ====================================== ]|
--Spawns Lauren at the start of the game.
gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal + 1
gzTextVar.iLaurenIndex = gzTextVar.zEntitiesTotal

--Shorthand
local i = gzTextVar.iLaurenIndex

gzTextVar.zEntities[i] = {}
gzTextVar.zEntities[i].bIsProtected = true
gzTextVar.zEntities[i].sDisplayName = "Lauren"
gzTextVar.zEntities[i].sQueryName = "lauren"
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Lauren"
gzTextVar.zEntities[i].sUniqueName = "lauren"
gzTextVar.zEntities[i].sIndicatorName = "Lauren"
gzTextVar.zEntities[i].sLocation = "Main Hall"
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sState = "Human"
gzTextVar.zEntities[i].bMovesRandomly = true
gzTextVar.zEntities[i].iSkinColor = LM_GetRandomNumber(0, 3)
gzTextVar.zEntities[i].iStayStillTurns = 0
gzTextVar.zEntities[i].sLastDoorDir = "P"
gzTextVar.zEntities[i].sSpecialIdentifier = "Lauren"

--Indicator.
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
gzTextVar.zEntities[i].iIndicatorX = 2
gzTextVar.zEntities[i].iIndicatorY = 3
gzTextVar.zEntities[i].iIndicatorC = ciCodeFriendly
gzTextVar.zEntities[i].iIndicatorH = 0
TL_SetProperty("Register Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[i].sIndicatorName, gzTextVar.zEntities[i].iIndicatorX, gzTextVar.zEntities[i].iIndicatorY, gzTextVar.zEntities[i].iIndicatorC)

--Command Handler
gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Lauren.lua"
gzTextVar.zEntities[i].sAIHandlerHostility = "Lauren"
gzTextVar.zEntities[i].sAIHandlerPostTurn = "Lauren"
gzTextVar.zEntities[i].sAIHandlerPrimary = "Lauren"
gzTextVar.zEntities[i].iMoveIndex = -1
gzTextVar.zEntities[i].iPatrolIndex = 1
gzTextVar.zEntities[i].saPatrolList = {"None"}
gzTextVar.zEntities[i].sFirstSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sLastSawPlayer = "Nowhere"
gzTextVar.zEntities[i].sInDisguise = "Null"

--Combat Properties
gzTextVar.zEntities[i].zCombatTable = {}
gzTextVar.zEntities[i].zCombatTable.iHealth = 18
gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 1
gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 2, 300, 0}
    
--Weaknesses. Lauren has no weaknesses.
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses = {}
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Fire] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Water] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Wind] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Earth] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Life] = 0
gzTextVar.zEntities[i].zCombatTable.iaWeaknesses[gciElementSlot_Death] = 0
