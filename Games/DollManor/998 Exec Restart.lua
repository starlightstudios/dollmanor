-- |[Exec Restart]|
--System file, boots back to title screen. Called from the options menu or the [restart] command.
AudioManager_StopMusic()
gsDifficultyOverride = gzTextVar.sDifficulty
LM_ExecuteScript(gzTextVar.sRootPath .. "000 Launcher Script.lua")