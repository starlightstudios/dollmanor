-- |[ ================================== Difficulty Resolver =================================== ]|
--Resolver that receives commands. This one is meant for changing game difficulty.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

-- |[ ======================================= Main Menu ======================================== ]|
-- |[Easy]|
if(sString == "EASY") then
    gzTextVar.sDifficulty = "Easy"
    TL_SetProperty("Append", "Set game difficulty to 'Easy'." .. gsDE)
    TL_SetProperty("Append", "Easy is meant for first-time players or those with limited mouse dexterity (such as those using a trackpad).\n")
    TL_SetProperty("Append", "Enemies are less dangerous in combat and take longer to respawn." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "Returning to main menu." .. gsDE)
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/000 Menu Boot.lua")

-- |[Normal]|
elseif(sString == "NORMAL") then
    gzTextVar.sDifficulty = "Normal"
    TL_SetProperty("Append", "Set game difficulty to 'Normal'." .. gsDE)
    TL_SetProperty("Append", "Normal is the intended experience for most players. Combat is difficult, supplies are limited, and the situation is fraught. Good luck." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "Returning to main menu." .. gsDE)
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/000 Menu Boot.lua")

-- |[Hard]|
elseif(sString == "HARD") then
    gzTextVar.sDifficulty = "Hard"
    TL_SetProperty("Append", "Set game difficulty to 'Hard'." .. gsDE)
    TL_SetProperty("Append", "Hard is meant for experienced players or stealth aficianados. You will suffer serious damage from every encounter and must avoid enemies at all costs.\n")
    TL_SetProperty("Append", "Stop, listen, think carefully, and plan your routes. Your survival is in your hands." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "Returning to main menu." .. gsDE)
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/000 Menu Boot.lua")

-- |[Descriptions]|
elseif(sString == "DESCRIPTIONS") then
    TL_SetProperty("Append", "The difficulty options are here to assist players in tailoring the difficulty to their skills. The game is meant to be difficult. It is a survival-horror game, after all." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Easy is for first-time players or players with limited mouse dexterity, such as those using a laptop trackpad." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Normal is the intended experience. Combat is difficult but winnable. Enemies will damage you even if you are doing well." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Hard is for experienced players. You will not have enough supplies to defeat every enemy you encounter, and one spell of bad luck will be the end for you." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Please select a difficulty that is as challenging as you can handle. You are meant to be tense and nervous. Use your brain and think before you move." .. gsDE)

-- |[Cancel]|
elseif(sString == "CANCEL") then
    TL_SetProperty("Append", "Returning to main menu." .. gsDE)

    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/000 Menu Boot.lua")

-- |[Error]|
else
    TL_SetProperty("Append", "Error: Unhandled menu input: [" .. sString .. "]" .. gsDE)
end