-- |[ ================================= Input Handler: Loading ================================= ]|
--Input handler when loading a game from the main menu.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

-- |[Cancel]|
--Goes back to the main menu.
if(sString == "cancel") then
    
    --Call the main menu script, ordering it to show the menu.
    TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Main Menu/100 Input Handler.lua")
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/100 Input Handler.lua", "showmenu")
    return
end

-- |[Error Checking]|
--Input is assumed to be the save file. Create a name for it.
local sSavePath = "Saves/" .. sString .. ".tls"

--If the file doesn't exist, notify the user.
local fCheckFile = io.open(sSavePath, "r")
if(fCheckFile == nil) then
    TL_SetProperty("Append", "I was unable to find the savefile [" .. sSavePath .. "]. Please check the name and try again, or enter [cancel] to return to the main menu." .. gsDE)
    return
else
    io.close(fCheckFile)
end

-- |[Loading]|
--First, the baseline game needs to be built.
LM_ExecuteScript(gzTextVar.sRootPath .. "999 Delayed Builder.lua")

--Now load a new game overtop of it.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Load Game.lua", sString)