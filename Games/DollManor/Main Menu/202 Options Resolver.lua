-- |[ ==================================== Options Resolver ==================================== ]|
--Resolver that receives commands. This one is meant for changing game options.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

-- |[Temp Function]|
local round = function(fValue)
    return (fValue + 0.5 - (fValue + 0.5) % 1)
end

-- |[ ======================================= Main Menu ======================================== ]|
-- |[Doll Preference]|
if(sString == "DOLLTYPE") then
    gzOptions.iOptionMode = gzOptions.ciOptionDollPref
    TL_SetProperty("Append", "Changes which type of doll you would prefer to be transformed into, if selected randomly.\n")
    TL_SetProperty("Append", "The available options are 'Dancer', 'Bride', 'Punk', 'Geisha', 'Goth', and 'Princess'." .. gsDE)
    TL_SetProperty("Append", "Please enter your desired option, or any other string to allow the game to select one randomly." .. gsDE)

elseif(sString == "DOLLCOLOR") then
    gzOptions.iOptionMode = gzOptions.ciOptionDollColor
    TL_SetProperty("Append", "Changes which plastic skin color you would like your doll to have, if transformed.\n")
    TL_SetProperty("Append", "The numbers are 0 (pure white) to 3 (very dark)." .. gsDE)
    TL_SetProperty("Append", "Please enter your desired option, or another number to allow the game to select one randomly." .. gsDE)

elseif(sString == "MUSICVOL") then
    gzOptions.iOptionMode = gzOptions.ciOptionMusicVol
    local fDefault = AudioManager_GetProperty("Default Music Volume")*100.0
    TL_SetProperty("Append", "Input a number from 0 (silent) to 100 (maximum) for the music volume. Enter any other string to use the default (" .. round(AudioManager_GetProperty("Default Music Volume")*100.0) .. ")." .. gsDE)
    
elseif(sString == "SOUNDVOL") then
    gzOptions.iOptionMode = gzOptions.ciOptionSoundVol
    TL_SetProperty("Append", "Input a number from 0 (silent) to 100 (maximum) for the sound volume. Enter any other string to use the default (" .. round(AudioManager_GetProperty("Default Sound Volume")*100.0) .. ")." .. gsDE)
    
-- |[Cancel]|
elseif(sString == "CANCEL") then
    TL_SetProperty("Append", "Returning to main menu." .. gsDE)

    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/000 Menu Boot.lua")

-- |[Error]|
else
    TL_SetProperty("Append", "Error: Unhandled menu input: [" .. sString .. "]" .. gsDE)
end