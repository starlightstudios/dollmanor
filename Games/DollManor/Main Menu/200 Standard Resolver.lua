-- |[ ======================================= Menu Resolver ======================================= ]|
--Receives and executes menu commands. Does not interface with the player directly ala an input
-- handler, instead this receives the specified inputs from the menu array.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

-- |[ ========================================= Main Menu ========================================= ]|
-- |[New Game]|
--Starts a new game! Run the introduction script. It boots the game when it's done.
if(sString == "NEWGAME") then
    gzTextVar.sManorType = "Normal"
    gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/MainManor/"
    LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/300 Introduction.lua")

--Same as above but skips the intro.
elseif(sString == "NEWGAMENOINTRO") then
    gzTextVar.sManorType = "Normal"
    gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/MainManor/"
    LM_ExecuteScript(gzTextVar.sRootPath .. "999 Delayed Builder.lua")
    
--Run the tutorial.
elseif(sString == "NEWGAMETUTORIAL") then
    gzTextVar.sManorType = "Tutorial"
    gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/Tutorial/"
    LM_ExecuteScript(gzTextVar.sRootPath .. "999 Delayed Builder.lua")
    
    --Add some text.
    TL_SetProperty("Append", "Welcome to String Tyrant. This tutorial will teach you the basics of playing the game." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To begin, there is a note on the ground near you. Type [look note] to read it. You can also use the locality window in the bottom right." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To use the locality window, click the rightmost icon to show nearby objects, and then the note. You can read it from the popup menu." .. gsDE)
    TL_SetProperty("Create Blocker")

-- |[Load Game]|
--Changes menu to loading handler.
elseif(sString == "LOADGAME") then

    --Display.
    TL_SetProperty("Append", "Please enter the name of the savefile you wish to load. Enter [cancel] to return to the main menu." .. gsDE)
        
    --Populate saves data.
    local saSavePaths = {}
    local iRecentSavesTotal = TL_GetSaveFileList("Saves/")
    
    --No saves found.
    if(iRecentSavesTotal == 0) then
        TL_SetProperty("Append", "No savefiles detected in the Saves/ directory." .. gsDE)
    
    --One savefile.
    elseif(iRecentSavesTotal == 1) then
        TL_SetProperty("Append", "Most recent savefile: [" .. TL_GetProperty("Save Path At", 0) .. "]." .. gsDE)
    
    --Two savefiles.
    elseif(iRecentSavesTotal == 2) then
        TL_SetProperty("Append", "Most recent savefiles: [" .. TL_GetProperty("Save Path At", 0) .. "] and [" .. TL_GetProperty("Save Path At", 1) .. "]." .. gsDE)
    
    --Three or more.
    else
    
        --String.
        if(iRecentSavesTotal > 10) then iRecentSavesTotal = 10 end
        local sString = iRecentSavesTotal .. " most recent savefiles: "
    
        for i = 1, iRecentSavesTotal-1, 1 do
            
            --Get the save.
            local sPath = "[" .. TL_GetProperty("Save Path At", i-1) .. "]"
            
            --Append.
            sString = sString .. sPath .. ", "
        end
    
        --Last savefile.
        sString = sString .. "and [" .. TL_GetProperty("Save Path At", iRecentSavesTotal-1) .. "]." .. gsDE
        TL_SetProperty("Append", sString)
    end
    
    --Modify the input string to be the load handler.
    TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Main Menu/101 Input Handler Load.lua")

-- |[Difficulty]|
--Activates difficulty menu. Discrete from the options menu.
elseif(sString == "DIFFICULTY") then

    --Set resolver.
    local sStandardHandler = fnResolvePath() .. "201 Difficulty Resolver.lua"

    --Build the list.
    gzMenu.zCommandList = {}
    fnAddCommand("Easy", "EASY", sStandardHandler)
    fnAddCommand("Normal", "NORMAL", sStandardHandler)
    fnAddCommand("Hard", "HARD", sStandardHandler)
    fnAddCommand("Descriptions", "DESCRIPTIONS", sStandardHandler)
    fnAddCommand("Cancel", "CANCEL", sStandardHandler)
    
    --Change input handler.
    TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Main Menu/102 Input Handler Difficulty.lua")
    
    --Execute.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/102 Input Handler Difficulty.lua", "showmenu")

-- |[Options]|
--Opens the options menu.
elseif(sString == "OPTIONS") then

    --Set resolver.
    local sStandardHandler = fnResolvePath() .. "202 Options Resolver.lua"

    --Build the list.
    gzMenu.zCommandList = {}
    fnAddCommand("Music Volume", "MUSICVOL", sStandardHandler)
    fnAddCommand("Sound Volume", "SOUNDVOL", sStandardHandler)
    fnAddCommand("Doll Type Preference", "DOLLTYPE", sStandardHandler)
    fnAddCommand("Doll Color Preference", "DOLLCOLOR", sStandardHandler)
    fnAddCommand("Cancel", "CANCEL", sStandardHandler)
    
    --Change input handler.
    TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Main Menu/103 Input Handler Options.lua")
    
    --Execute.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/103 Input Handler Options.lua", "showmenu")

-- |[Exit]|
--Why would you want to quit this great game?
elseif(sString == "EXITGAME") then
    
    --In "one-game" mode, exit the game entirely.
    if(gbIsOneGameMode == true or true) then
        Game_Quit()
	else
		MapM_BackToTitle()
	end

-- |[Error]|
else
    TL_SetProperty("Append", "Error: Unhandled menu input: [" .. sString .. "]" .. gsDE)
end