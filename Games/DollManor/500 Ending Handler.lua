-- |[ ===================================== Ending Handler ===================================== ]|
--Checks ending states and displays as needed. Can only happen once per game. The game ends
-- once the endings are finished.

-- |[Variable Collection]|
--Setup
local bIsPlayerDoll = false
local bIsJessieDoll = false
local bIsLaurenDoll = false
local bIsPlayerStatue = false
local bIsJessieStatue = false
local bIsLaurenStatue = false
local bIsPlayerClay = false
local bIsJessieClay = false
local bIsLaurenClay = false
local bIsPlayerGlass = false
local bIsJessieGlass = false
local bIsLaurenGlass = false

--Determine doll status.
if(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll") then bIsPlayerDoll = true end
if(string.sub(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState, 1, 4) == "Doll") then bIsJessieDoll = true end
if(string.sub(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState, 1, 4) == "Doll") then bIsLaurenDoll = true end

--Statue status.
if(gzTextVar.gzPlayer.sFormState == "Statue") then bIsPlayerStatue = true end
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Statue") then bIsJessieStatue = true end
if(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Statue") then bIsLaurenStatue = true end

--Claygirl status.
if(gzTextVar.gzPlayer.sFormState == "Claygirl") then bIsPlayerClay = true end
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Claygirl") then bIsJessieClay = true end
if(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Claygirl") then bIsLaurenClay = true end

--Glass status.
if(gzTextVar.gzPlayer.sFormState == "Glass") then bIsPlayerGlass = true end
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Glass") then bIsJessieGlass = true end
if(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Glass") then bIsLaurenGlass = true end

-- |[Ending 1]|
--In Phase 1, the player becomes a doll, then transformed Jessie and Lauren.
if(bIsPlayerDoll and bIsJessieDoll and bIsLaurenDoll and gzTextVar.bStringTrapEnding == false) then
    
    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary the doll played with her sisters quietly and obeyed every whim of their creator. She was a good doll.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, a visitor would come by. They would entertain the visitor, as their creator bid. They would put on plays and dances, and provide food and water.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The visitors would always elect to stay, and join them. Mary made many new sisters, and her creator was delighted.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slowly, their creator aged. It showed more and more in her mannerisms. She slowed in word and thought. Her loyal dollies did all her tasks for her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Their creator became sick, and the dollies knew that her time was soon. They prepared a funeral for her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "But their creator had one thing for her, her most loyal and obedient dolly. She called Mary to her side.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She told Mary of the things the manor held. The places she had forbade them from going. She told Mary what was in those places.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She told Mary that nobody was ever to go near those places. Nobody was ever to open the seals. Her creator was not playing a game, and Mary took her words seriously.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She did not know what any of them meant. She was a silly doll, meant to play and be played with. But she was also a tool of her creator and would do as she asked.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Finally, her creator placed her hand upon Mary's head. She filled her mind once again. She gave Mary the knowledge she would need to create more sisters herself. Mary would become the new creator.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The exertion taking what little strength she had, her creator fell asleep. Mary put her to bed, knowing it was for the last time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And so, Mary the doll would stand watch with her sisters in the manor. She created many more sisters over the years, as that had been her creator's final wish. Mary would carry it out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 1. Plaything)") ]])
    fnCutsceneBlocker()
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 1)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_ItAlwaysEndsThisWay)
    
-- |[Ending 2]|
--If in Phase 2 and the player is in the Ritual Altar, then they got the bad end of trying to pursue Pygmalie. This is called from Pygmalie's AI script.
elseif(gzTextVar.bExternalEndingCall == true and gzTextVar.iGameStage == 2 and gzTextVar.gzPlayer.sLocation == "Ritual Altar" and gzTextVar.bStringTrapEnding == false) then
        
    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary the doll stood in the room with the ritual altar in it. There were hundreds of others just like her in the same room.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary was a very good dolly. Mary didn't move. Mary was empty and stared ahead of her. Mary was very pretty. Mary loved being with her sisters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, Mary's creator told her to move. Mary would move, and do things for her creator. She would hold people. She would take books and burn them. She would find their guests and play with them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And then, Mary would go back to the room with the altar, and stop moving.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "In the back of her silly, empty mind, Mary could remember something. Her creator had asked her if she had taken something once.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary wondered what that something was. She hadn't taken it. Should she find it for her creator? She wondered where it was.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And then, Mary would be empty again, and Mary would not move.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 2. Face in a Crowd)") ]])
    fnCutsceneBlocker()
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 2)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_ItAlwaysEndsThisWay)

-- |[Ending 3]|
--If in Phase 2 and the player turned everyone into statues.
elseif(bIsPlayerStatue and bIsJessieStatue and bIsLaurenStatue) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Statues") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The three statues stood in the silent room. The blue light pulsed, bathing them. They thought nothing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The light flowed over the three, probing their empty minds. They surrendered every piece of information they had in their previous lives.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "There was a mutual attraction, familiarity. The light took those thoughts. The light ordered the statues to move. They walked unthinking to the gardens.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Two of the statues stood on the plinth, locked in one another's arms. The third stood wordlessly some distance away.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The statues locked lips together in an expression of bliss. They thought nothing as they kissed for eternity.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, the light commanded them to move. To seize intruders. They obeyed without question. The light was their thoughts.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Then, the light commanded them to return to their plinth. The statues resumed their pose, embracing in mindless passion.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 3. Gallery)") ]])
    fnCutsceneBlocker()
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 4)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_ToTheEndOfTime)

-- |[Ending 4]|
--If in Phase 2 and the layer turned everyone into claygirls.
elseif(bIsPlayerClay == true and bIsJessieClay == true and bIsLaurenClay == true and gzTextVar.bIsClaySequence == false) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Claygirls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary the clay girl wandered about the manor, ever vigilant for humans.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, she would find an object and cover it with her clay, shaping herself to look like it. She would do this sometimes for weeks before resuming her wanderings.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She would cross paths with Jessie and Lauren sometimes. They would play together, shaping one another, checking each other to make sure they remained obedient.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She was glad she could be a family with them. She loved being clay. Sometimes she wondered if she had ever been human.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And then she would find another object, and disguise herself, and wait. Mary was a good clay girl.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 4. Malleable)") ]])
    fnCutsceneBlocker()
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 8)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_Faceless)

-- |[Ending 5]|
--In phase 6, Mary betrayed Jessie and Lauren, and submitted to Pygmalie.
elseif(gzTextVar.bSpecialEndingBetraySubmit == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary helped her creator dress her sisters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jessie and Lauren took shape before her. She asked her creator to spare a little of their memories, though they needed to be kept obedient.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Her pretty plastic sisters stood, and they embraced. Jessie forgave her, Lauren thanked her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary knew that they only did this because Pygmalie forced them to. That was fine. This was for the best.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Now they could be together forever.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary made sure to write a journal of her thoughts. She made sure to keep the thing at bay. She made new sisters and sacrificed them, as her creator willed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And Mary hid Sarah again. She would visit her sometimes, to keep her company. Sarah was upset at her, but over the decades eventually forgave her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, her creator grew old and frail. Mary stuck the needle in her neck as she lay dying. Her creator became plastic, like her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And so, Mary became the new creator. Mary spent the rest of eternity providing for the thing, and growing her loving family.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 5. Good Dolly)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 16)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_Willing)
    
    --Hard mode:
    if(gzTextVar.sDifficulty == "Hard") then
        Steam_UnlockAchievement(gciAchievement_ThereIsHopeYet)
    end

-- |[Ending 6]|
--In phase 6, Mary betrayed Jessie and Lauren, and betrayed Pygmalie.
elseif(gzTextVar.bSpecialEndingBetrayBetray == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/DollsMary") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary dressed Jessie, then Lauren. She let them keep themselves. She was not as cruel as Pygmalie.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jessie was angry. She shouted at Mary. Mary accepted her judgements. Then, she asked Jessie for an alternative.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jessie did not have one. She cast her eyes to the floor in shame.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lauren was quiet. Reserved. She would need time to adjust to her body. Mary tried to comfort her, but she withdrew.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "It took many years for the two to forgive Mary, but they did. Of their own accord. Mary did not force them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She gave them the power of Command. The three would perform the role Pygmalie had.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "They kept detailed journals. This sated the thing, sometimes. Sometimes, it needed more. People would appear.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Boys, girls, neither, both. Some wore clothes from the past, some seemed to be from the future. They were all sacrifices.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sarah stayed in her place in the basement. Lauren spent much of her time with Sarah, visiting. Writing. Lauren wrote many books with Sarah.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "In a way, Mary was far kinder to her guests than Pygmalie had been. She treated them well, told them the situation, offered to let them join her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Most fought. All failed. Mary made many dolls. Many, many dolls.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 6. Mistress of the Dolls)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 32)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Hard mode:
    if(gzTextVar.sDifficulty == "Hard") then
        Steam_UnlockAchievement(gciAchievement_ThereIsHopeYet)
    end

-- |[Ending 7]|
--In phase 6, Mary sacrificed herself.
elseif(gzTextVar.bSpecialEndingSacrificeMary == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary stood before the altar. A paintbrush, fresh with paint, sat upon it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Taking the paintbrush, Mary began to draw the symbols on herself. She fought back her own tears. She had to be strong.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She sat herself on the altar and waited. The thing did not take long. It was hungry.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "It came into the room. She could not see it, but it was there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "It drew close. She could feel it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Something left her. Everything left her. She slumped to the side, inert.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The doll forgot everything. She was emptied out. She had no need to move. This was fine.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Someone woke up near her. The doll didn't know the lady's name. The doll didn't even know her own name.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Simple thoughts flooded into her. The lady put the thoughts in her. She was angry at the doll. Would she punish the doll?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "No.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "There was nothing left to punish.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The doll stood up and curtseyed before her creator. She was a pretty dancer. She would do as her creator bid. Forever.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 7. A True Hero)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 64)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Hard mode:
    if(gzTextVar.sDifficulty == "Hard") then
        Steam_UnlockAchievement(gciAchievement_ThereIsHopeYet)
    end

-- |[Ending 8]|
--In phase 6, Jessie sacrificed herself.
elseif(gzTextVar.bSpecialEndingSacrificeJessie == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Escape_ML") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and Lauren fled. Jessie had done her part. They ran through the bushes for what seemed like hours.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, they came back to the brook they had left. They had only been gone a few hours, but it felt like years.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lauren did not stop crying and clutching Mary's legs until they were almost home. Mary held him close.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She told him what to say. He had wandered off on his own. He had went looking for Jessie and Mary, but never found them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The two girls were reported missing. There was a search. Nothing was ever found.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary found an abandoned shack in the woods. She fixed it up. She made it her own place.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She watched over Lauren. She disguised herself carefully, in heavy clothes with hoods.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lauren grew up to be a big, strong man. He visited Mary every time he could.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary spent her time researching. She tried to find out more about the manor. More about what happened there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "But for all her work, there was nothing she could do. Jessie was gone. She was a doll.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She would live for many, many years. She watched the world grow old before her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She never would end the crimes at the manor. But she had survived. Lauren had escaped. That would always be enough for her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 8. Jessie the Great)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 128)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Hard mode:
    if(gzTextVar.sDifficulty == "Hard") then
        Steam_UnlockAchievement(gciAchievement_ThereIsHopeYet)
    end

-- |[Ending 9]|
--In phase 6, Lauren sacrificed himself.
elseif(gzTextVar.bSpecialEndingSacrificeLauren == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Escape_MJ") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and Jessie fled. Lauren had done his part. The storm lifted, the sky cleared. They ran into the bushes for what seemed like hours.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, they came back to the brook they had left. They had only been gone a few hours, but it felt like years.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Alone with Mary, Jessie allowed herself to cry. The two hugged and cried until there were no tears left.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "They worked out what to say, together. Jessie would tell them that her friends had decided to take a shortcut. She didn't see them after that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and Lauren were reported missing. There was a search. Nothing was ever found.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary found an abandoned shack in the woods. She fixed it up. She made it her own place.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She watched over Jessie. She disguised herself carefully, in heavy clothes with hoods.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Jessie grew up. Got a job. Got married. But she remained Mary's best friend, visiting her almost every day.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary spent her time researching. She tried to find out more about the manor. More about what happened there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "But for all her work, there was nothing she could do. Lauren was gone. She was a doll.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She would live for many, many years. She watched the world grow old before her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She never would end the crimes at the manor. But she had survived. Jessie had escaped. Lauren had given everything for her. That was enough for her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 9. World's Best Brother)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 256)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Hard mode:
    if(gzTextVar.sDifficulty == "Hard") then
        Steam_UnlockAchievement(gciAchievement_ThereIsHopeYet)
    end

-- |[Ending 10]|
--In phase 6, Mary sacrifices Pygmalie and Sarah.
elseif(gzTextVar.bSpecialEndingSacrificeSarah == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Escape_JL") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary watched from a window as Jessie and Lauren ran into the bush. She smiled to herself. This truly was the best outcome.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She turned back to the dolls at the altar. Sarah Lee-Anne was prepared. She was still groaning and clutching at her head.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "There was too much for her. Two lifetimes of conflicting memories. Endless guilt. Endless anguish.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary waited. The thing came. It was hungry.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "It devoured Sarah. There was much to eat. It was satisifed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "A rush of air. It was gone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The body of Sarah lay on the altar. Mary stood over it. She smiled. The dolls around her prepared Sarah.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She dressed the new sister. She dressed her as a fellow creator, in a smock and yellow shirt. There was very little left inside her. But that could change.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She felt that Jessie and Lauren had escaped. The storm had lifted. The thing would go and sleep now, until it became hungry again.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary gave the new doll an important task. She was a silly doll, without many thoughts, but her task was to think.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sarah Lee-Anne was to think, come up with ideas, learn. Sarah promised her creator she would do her best.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary tasked all the other dolls around her with the same goal. They all promised their creator they would.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "In the respite provided, the dolls learned many things. They had fun playing, reading, writing. And then they would be consumed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "But the thing never came for Mary. It had so much to eat, it never needed to.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And Mary researched, and experimented, and practiced. There must be a way to stop it, forever. She merely needed to find it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And she would never grow old, or frail, or sick. She would never die. Not as a doll.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "There was a way to stop it, and she would find it, even if it took eternity.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 10. Never Give Up)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 512)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_MuchToAnswerFor)
    
    --Hard mode:
    if(gzTextVar.sDifficulty == "Hard") then
        Steam_UnlockAchievement(gciAchievement_ThereIsHopeYet)
    end

-- |[Ending 11]|
--String Trap ending.
elseif(gzTextVar.bStringTrapEnding == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and her dear bride Jessie spent all their time together. They would go on long walks in the gardens, tend to the flowers, and participate in the plays for their creator's enjoyment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Her sister Lauren, allowed to keep a little more of her intellect than the others, helped their creator dress the new sisters that Mary and Jessie would bring to them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Hundreds of new sisters soon filled the hallways, and Mary the doll was never alone.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Eventually, their creator took ill. Her age caught up to her. She was kind and gracious and caring, but she was also a human.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The dollies prepared themselves. Mary, Jessie, and Lauren were called to her bedside as her condition worsened.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She placed a hand on Lauren's head, and ideas flowed in. Symbols, words, things the silly geisha doll did not understand.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Pygmalie charged Lauren with continuing her task. Lauren was the new creator, and would make many more dolls. Mary and Jessie swore a new allegiance as their beloved creator breathed her last.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The funeral was a sad affair. Many of the dollies were unsure of what to do, but Lauren had been selected as a successor. They followed her orders.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "For the rest of time, the dolls made new sisters of those who stumbled into their humble manor.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 11. Dancing Marionette)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 1024)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_ItAlwaysEndsThisWay)

-- |[Ending 12]|
--Rubber trap ending.
elseif(gzTextVar.bGoopTrapEnding == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary the rubber dolly played with the plastic dollies. She felt blissfully happy all the time, the feeling forced into her by her glassy grinning face.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes she would clean, sometimes she would cook, but always she smiled for her guests and her creator.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She would see her rubbery clones in the manor sometimes. They smiled back at her, staring blankly ahead. She returned the look.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "When her creator wished it, she would cover a guest in her rubber, and make them like her. There were dozens of her, maybe hundreds. She couldn't count that high, and did not care anyway.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "For the rest of time, Mary made rubber clones of herself of those who stumbled into their humble manor.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 12. Identical Twins)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 2048)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end

-- |[Ending 13]|
--Frozen ending.
elseif(gzTextVar.bFreezingTrapEnding == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and the two ice sculptures she made remained in the ice caverns, wandering around. Often, they would stand still for days at a time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nothing mattered to them. Everything was frozen eternally. They could not even bring themselves to lament their fate.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Very rarely, a human would come by. They were trying to escape. Mary would suck the heat from them and leave them a frozen sculpture, just like her. Not because she wanted to, but because her creator did.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The sculptures guarded the caverns for the rest of time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 13. Cool It!)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 4096)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end

-- |[Ending 14]|
--Claygirl unique ending.
elseif(gzTextVar.bClayTrapEnding == true) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Claygirls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and Jessie the claygirls loved to play in the manor, taking the forms of furniture and playing pranks on the doll girls.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Lauren became the creator's favourite, shaping herself into incredibly lifelike shapes. She learned to actuate her mouth and even produce simple sounds from her lungs, as well as hold a human shape for hours at a time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and Jessie were soon charged with practicing with Lauren. Sometimes they would even mistake her for an intruder, so convincing was her disguise.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, humans would visit the manor. The more suspicious among them would meet a crying little girl named Lauren who would make sure they stayed forever.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary and Jessie were so proud of their wonderful clay sister. They made many more clay sisters over the years.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 14. The Perfect Disguise)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 8192)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
    
    --Achievement.
    Steam_UnlockAchievement(gciAchievement_Faceless)

-- |[Ending 15]|
--Glass!
elseif(bIsPlayerGlass and bIsJessieGlass and bIsLaurenGlass) then

    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary, the girl made of glass, would often dream about what it was like to be human. She frequently found Jessie and Lauren in the chapel, doing the same.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "After some time, the three became good friends. Lauren enjoyed pretending to be Mary's little brother, and Jessie eventually confessed romantic feelings for her. Just like the characters they admired.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, the stranger would stand in the chapel hall, unmoving, unflinching. Sometimes, she would disappear, and nobody in the manor knew where she was.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Usually, humans would come visit when that happened. Mary helped to find them for the creator, lest the intruders upset their gracious host.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "For the rest of time, Mary and her friends stayed there and dreamed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 15. Work of Art)") ]])
    
    --Option Flag
    local iEndingFlag = OM_GetOption("String Tyrant Endings")
    local iPreviousFlag = iEndingFlag
    iEndingFlag = bit32.bor(iEndingFlag, 16384)
    if(iPreviousFlag ~= iEndingFlag) then
        OM_SetOption("String Tyrant Endings", iEndingFlag)
        OM_WriteConfigFiles()
    end
end