-- |[ ==================================== Launcher Script ===================================== ]|
--Called from the main menu, does the setup work.

-- |[Switch to String Tyrant's Loading Screen]|
--Change variables from nil if they don't already exist.
if(gbHasBootedLoader == nil) then gbHasBootedLoader = false end
if(gbHasLoadedData   == nil) then gbHasLoadedData   = false end

--If we haven't already loaded the data, do it here. This will not execute on 'restart' commands.
if(gbHasLoadedData == false) then
    
    --Resolve datafiles.
    if(gbHasBootedLoader == false) then
        gbHasBootedLoader = true
        LI_BootStringTyrant(fnResolvePath() .. "Datafiles/STLoad.slf")
    end
    
    --If this is not a boot right from the title screen, this is the load value.
    if(gsMandateTitle ~= "String Tyrant") then
        LI_Reset(316)
    end
else
    --LI_Reset(1)
    --LI_Interrupt()
end

-- |[GUI Text Lookups]|
--Build all the GUI text. This text may have been translated when the program runs, or it may get
-- translated during running. Either way, this file contains lookups for all the GUI text and can
-- be modified to change the strings.
LM_ExecuteScript(fnResolvePath() .. "Translations/000 Build GUI Translation Text.lua")

-- |[Translation Builder]|
--TE_Execute(fnResolvePath(), fnResolvePath() .. "Translations/German.lua")
--TE_Execute(fnResolvePath(), fnResolvePath() .. "Translations/Untranslate.lua")
--LM_ExecuteScript(fnResolvePath() .. "Translations/001 Translatable Files.lua")

-- |[Steam]|
--Activate Steam. Does nothing if not using a steam executable.
giStringTyrantSteamIndex = 0 --C++ constant
Steam_Activate(giStringTyrantSteamIndex)

--Achievement constants. C++/Steam.
gciAchievement_WelcomeToTheManor = 0
gciAchievement_ItAlwaysEndsThisWay = 1
gciAchievement_ToTheEndOfTime = 2
gciAchievement_Faceless = 3
gciAchievement_TurnedOutWonderfully = 4
gciAchievement_RightBehindYou = 5
gciAchievement_Squeak = 6
gciAchievement_ItsColdWhereWeAre = 7
gciAchievement_StainedGlass = 8
gciAchievement_NothingLeftToLose = 9
gciAchievement_Willing = 10
gciAchievement_MuchToAnswerFor = 11
gciAchievement_NoFightIt = 12
gciAchievement_ItDoesntHurt = 13
gciAchievement_YoungLove = 14
gciAchievement_ThereIsHopeYet = 15
gciAchievement_AsTheCrowFlies = 16
gciAchievement_Total = 17

--Steam Achievements. These are the client-side versions, and can be set any time even if steam is still booting.
Steam_AllocateAchievements(gciAchievement_Total)
Steam_SetAchievement(gciAchievement_WelcomeToTheManor,    "WELCOME_TO_THE_MANOR")
Steam_SetAchievement(gciAchievement_ItAlwaysEndsThisWay,  "IT_ALWAYS_ENDS_THIS_WAY")
Steam_SetAchievement(gciAchievement_ToTheEndOfTime,       "TO_THE_END_OF_TIME")
Steam_SetAchievement(gciAchievement_Faceless,             "FACELESS")
Steam_SetAchievement(gciAchievement_TurnedOutWonderfully, "TURNED_OUT_WONDERFULLY")
Steam_SetAchievement(gciAchievement_RightBehindYou,       "RIGHT_BEHIND_YOU")
Steam_SetAchievement(gciAchievement_Squeak,               "SQUEAK")
Steam_SetAchievement(gciAchievement_ItsColdWhereWeAre,    "ITS_COLD_WHERE_WE_ARE")
Steam_SetAchievement(gciAchievement_StainedGlass,         "STAINED_GLASS")
Steam_SetAchievement(gciAchievement_NothingLeftToLose,    "NOTHING_LEFT_TO_LOSE")
Steam_SetAchievement(gciAchievement_Willing,              "WILLING")
Steam_SetAchievement(gciAchievement_MuchToAnswerFor,      "MUCH_TO_ANSWER_FOR")
Steam_SetAchievement(gciAchievement_NoFightIt,            "NO_FIGHT_IT")
Steam_SetAchievement(gciAchievement_ItDoesntHurt,         "IT_DOESNT_HURT")
Steam_SetAchievement(gciAchievement_YoungLove,            "YOUNG_LOVE")
Steam_SetAchievement(gciAchievement_ThereIsHopeYet,       "THERE_IS_HOPE_YET")
Steam_SetAchievement(gciAchievement_AsTheCrowFlies,       "AS_THE_CROW_FLIES")

-- |[Game Options]|
--Setup.
gzOptions = {}

--Constants
gzOptions.ciOptionNone = 0
gzOptions.ciOptionDollPref = 1
gzOptions.ciOptionDollColor = 2
gzOptions.ciOptionMusicVol = 3
gzOptions.ciOptionSoundVol = 4

--Variables.
gzOptions.iOptionMode = gzOptions.ciOptionNone
gzOptions.sOverrideDoll = "Null"
gzOptions.iOverrideDollSkin = -1

-- |[ ===================================== Audio Loading ====================================== ]|
--Handled by a subscript.
if(gzbHasLoadedDollAudio == nil) then
    gzbHasLoadedDollAudio = true
    LM_ExecuteScript(fnResolvePath() ..  "/Audio/ZRouting.lua")
end

-- |[ ====================================== Font Loading ====================================== ]|
LM_ExecuteScript(fnResolvePath() ..  "/Fonts/000 Boot Fonts.lua")

-- |[ ==================================== Graphics Loading ==================================== ]|
--User interface parts, such as combat and level-up. Also contains a lot of portrait pieces that are used in
-- the UI, such as the turn-order portraits.
gsDatafilesPath = fnResolvePath() .. "Datafiles/"
SLF_Open(gsDatafilesPath .. "UITextAdventure.slf")

-- |[Extraction Function]|
local fnExtract = function(psaNames, psPrefix, psDLPath)
	
	--Arg check.
	if(psaNames == nil) then return end
	if(psPrefix == nil) then return end
	if(psDLPath == nil) then return end
	
	--Path.
	DL_AddPath(psDLPath)
	
	--Extraction loop.
	local i = 1
	while(psaNames[i] ~= nil) do
		DL_ExtractBitmap(psPrefix .. psaNames[i], psDLPath .. psaNames[i])
		i = i + 1
	end
end

-- |[Lookup Tables]|
local zaLookups = {}
zaLookups[ 1] = {"TxtDialogue|ST|", "Root/Images/AdventureUI/Dialogue/ST|", {"BorderCard", "TextInput", "TextInputDark", "Mask_CharacterRender", "Mask_LocalityRender", "Mask_TextRender", "Mask_WorldRender"}}
zaLookups[ 2] = {"TxtDeckEd|",      "Root/Images/AdventureUI/DeckEd/",      {"Backing", "ButtonCancel", "ButtonReset", "ButtonSave", "ButtonSmallAdd", "ButtonSmallSub", "ButtonQuestion", "HelpInlay"}}
zaLookups[ 3] = {"TxtDialogue|ST|", "Root/Images/AdventureUI/Dialogue/",    {"BorderCard", "NamelessBox", "NamePanel"}}

-- |[Execution]|
for i = 1, #zaLookups, 1 do
	fnExtract(zaLookups[i][3], zaLookups[i][1], zaLookups[i][2])
end

--Options Menu
DL_AddPath("Root/Images/DollManor/TxtOptions/")
DL_ExtractBitmap("TxtOptions|Backing",    "Root/Images/DollManor/TxtOptions/Backing")
DL_ExtractBitmap("TxtOptions|BtnBack",    "Root/Images/DollManor/TxtOptions/BtnBack")
DL_ExtractBitmap("TxtOptions|BtnCancel",  "Root/Images/DollManor/TxtOptions/BtnCancel")
DL_ExtractBitmap("TxtOptions|BtnSave",    "Root/Images/DollManor/TxtOptions/BtnSave")
DL_ExtractBitmap("TxtOptions|BtnEndings", "Root/Images/DollManor/TxtOptions/BtnEndings")
DL_ExtractBitmap("TxtOptions|Confirm",    "Root/Images/DollManor/TxtOptions/Confirm")

--Manually add this image with different filtering.
local iNearest = DM_GetEnumeration("GL_NEAREST")
local iLinear  = DM_GetEnumeration("GL_LINEAR")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
DL_ExtractBitmap("TxtDialogue|ST|TextAdventureMapParts",  "Root/Images/AdventureUI/Dialogue/ST|TextAdventureMapParts")
DL_ExtractBitmap("TxtDialogue|ST|TextAdventureScrollbar", "Root/Images/AdventureUI/Dialogue/ST|TextAdventureScrollbar")
DL_ExtractBitmap("TxtDialogue|ST|PopupBorderCard",        "Root/Images/AdventureUI/Dialogue/ST|PopupBorderCard")
ALB_SetTextureProperty("Restore Defaults")

-- |[StringTyrantTitle.slf]|
SLF_Open(gsDatafilesPath .. "StringTyrantTitle.slf")
DL_AddPath("Root/Images/DollManor/Title/")
DL_ExtractBitmap("Map|Layer0", "Root/Images/DollManor/Title/Layer0")
DL_ExtractBitmap("Map|Layer1", "Root/Images/DollManor/Title/Layer1")
DL_ExtractBitmap("Map|Layer2", "Root/Images/DollManor/Title/Layer2")
DL_ExtractBitmap("Map|Layer3", "Root/Images/DollManor/Title/Layer3")
DL_ExtractBitmap("Map|Layer4", "Root/Images/DollManor/Title/Layer4")
DL_ExtractBitmap("Map|Layer5", "Root/Images/DollManor/Title/Layer5")
DL_ExtractBitmap("Map|Layer6", "Root/Images/DollManor/Title/Layer6")

-- |[DollManor.slf]|
SLF_Open(gsDatafilesPath .. "DollManor.slf")
DL_AddPath("Root/Images/DollManor/All/")

-- |[Tileset]|
DL_AddPath("Root/Images/DollManor/Tiles/")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
DL_ExtractBitmap("Tileset", "Root/Images/DollManor/Tiles/Tileset")
ALB_SetTextureProperty("Restore Defaults")

-- |[Map]|
DL_AddPath("Root/Images/DollManor/MapLayers/")
DL_ExtractBitmap("Map|Base",    "Root/Images/DollManor/MapLayers/Base")
DL_ExtractBitmap("Map|Layer00", "Root/Images/DollManor/MapLayers/Layer00")
DL_ExtractBitmap("Map|Layer01", "Root/Images/DollManor/MapLayers/Layer01")
DL_ExtractBitmap("Map|Layer02", "Root/Images/DollManor/MapLayers/Layer02")
DL_ExtractBitmap("Map|Layer03", "Root/Images/DollManor/MapLayers/Layer03")
DL_ExtractBitmap("Map|Layer04", "Root/Images/DollManor/MapLayers/Layer04")
DL_ExtractBitmap("Map|Layer05", "Root/Images/DollManor/MapLayers/Layer05")
DL_ExtractBitmap("Map|Layer06", "Root/Images/DollManor/MapLayers/Layer06")
DL_ExtractBitmap("Map|Layer07", "Root/Images/DollManor/MapLayers/Layer07")
DL_ExtractBitmap("Map|Layer08", "Root/Images/DollManor/MapLayers/Layer08")

-- |[Overlays]|
--Uses for darkness/misty sections, needs to repeat since they are stretch-rendered.
local iRepeat = DM_GetEnumeration("GL_REPEAT")
ALB_SetTextureProperty("Wrap S", iRepeat)
ALB_SetTextureProperty("Wrap T", iRepeat)
DL_AddPath("Root/Images/DollManor/Overlays/")
DL_ExtractBitmap("Overlay|Black0",    "Root/Images/DollManor/Overlays/Black0")
DL_ExtractBitmap("Overlay|Black1",    "Root/Images/DollManor/Overlays/Black1")
DL_ExtractBitmap("Overlay|Fog0",      "Root/Images/DollManor/Overlays/Fog0")
DL_ExtractBitmap("Overlay|Fog1",      "Root/Images/DollManor/Overlays/Fog1")
DL_ExtractBitmap("Overlay|Fog2",      "Root/Images/DollManor/Overlays/Fog2")
DL_ExtractBitmap("Overlay|Fog3",      "Root/Images/DollManor/Overlays/Fog3")
DL_ExtractBitmap("Overlay|Fog4",      "Root/Images/DollManor/Overlays/Fog4")
DL_ExtractBitmap("Overlay|Black0Big", "Root/Images/DollManor/Overlays/Black0Big")
DL_ExtractBitmap("Overlay|Fog0Big",   "Root/Images/DollManor/Overlays/Fog0Big")
ALB_SetTextureProperty("Restore Defaults")

--UI Overlays
DL_ExtractBitmap("Overlay|Move",  "Root/Images/DollManor/Overlays/Move")
DL_ExtractBitmap("Overlay|DoorH", "Root/Images/DollManor/Overlays/DoorH")
DL_ExtractBitmap("Overlay|DoorV", "Root/Images/DollManor/Overlays/DoorV")

-- |[ ======================================== Endings ========================================= ]|
DL_AddPath("Root/Images/DollManor/Endings/")
DL_ExtractBitmap("Ending|BigHug",    "Root/Images/DollManor/Endings/BigHug")
DL_ExtractBitmap("Ending|Escape_JL", "Root/Images/DollManor/Endings/Escape_JL")
DL_ExtractBitmap("Ending|Escape_MJ", "Root/Images/DollManor/Endings/Escape_MJ")
DL_ExtractBitmap("Ending|Escape_ML", "Root/Images/DollManor/Endings/Escape_ML")
DL_ExtractBitmap("Ending|Claygirls", "Root/Images/DollManor/Endings/Claygirls")
DL_ExtractBitmap("Ending|Dolls",     "Root/Images/DollManor/Endings/Dolls")
DL_ExtractBitmap("Ending|DollsMary", "Root/Images/DollManor/Endings/DollsMary")
DL_ExtractBitmap("Ending|Statues",   "Root/Images/DollManor/Endings/Statues")

-- |[ ========================================== Mary ========================================== ]|
--Path Setup
DL_AddPath("Root/Images/DollManor/Characters/")
DL_AddPath("Root/Images/DollManor/Transformation/")

--Normal
DL_ExtractBitmap("Mary",           "Root/Images/DollManor/Characters/Mary")
DL_ExtractBitmap("MarySecretClay", "Root/Images/DollManor/Characters/MarySecretClay")
DL_ExtractBitmap("MarySecretDoll", "Root/Images/DollManor/Characters/MarySecretDoll")

--Clay TF
DL_ExtractBitmap("MaryClayBlueTF0",   "Root/Images/DollManor/Transformation/MaryClayBlueTF0")
DL_ExtractBitmap("MaryClayRedTF0",    "Root/Images/DollManor/Transformation/MaryClayRedTF0")
DL_ExtractBitmap("MaryClayYellowTF0", "Root/Images/DollManor/Transformation/MaryClayYellowTF0")

--Doll TF
DL_ExtractBitmap("MaryDollTF0", "Root/Images/DollManor/Transformation/MaryDollTF0")
DL_ExtractBitmap("MaryDollTF1", "Root/Images/DollManor/Transformation/MaryDollTF1")

--Glass
DL_ExtractBitmap("MaryGlass",    "Root/Images/DollManor/Characters/MaryGlass")
DL_ExtractBitmap("MaryGlassTF0", "Root/Images/DollManor/Transformation/MaryGlassTF0")

--Ice
DL_ExtractBitmap("MaryIce",    "Root/Images/DollManor/Characters/MaryIce")
DL_ExtractBitmap("MaryIceTF0", "Root/Images/DollManor/Transformation/MaryIceTF0")
DL_ExtractBitmap("MaryIceTF1", "Root/Images/DollManor/Transformation/MaryIceTF1")
DL_ExtractBitmap("MaryIceTF2", "Root/Images/DollManor/Transformation/MaryIceTF2")

--Stone
DL_ExtractBitmap("MaryStone",    "Root/Images/DollManor/Characters/MaryStone")
DL_ExtractBitmap("MaryStoneTF0", "Root/Images/DollManor/Transformation/MaryStoneTF0")
DL_ExtractBitmap("MaryStoneTF1", "Root/Images/DollManor/Transformation/MaryStoneTF1")
DL_ExtractBitmap("MaryStoneTF2", "Root/Images/DollManor/Transformation/MaryStoneTF2")

--Mary's String Trap TF
DL_ExtractBitmap("MaryStringTrap0", "Root/Images/DollManor/Characters/MaryStringTrap0")
DL_ExtractBitmap("MaryStringTrap1", "Root/Images/DollManor/Characters/MaryStringTrap1")
DL_ExtractBitmap("MaryStringTrap2", "Root/Images/DollManor/Characters/MaryStringTrap2")
DL_ExtractBitmap("MaryStringTrap3", "Root/Images/DollManor/Characters/MaryStringTrap3")
DL_ExtractBitmap("MaryStringTrap4", "Root/Images/DollManor/Characters/MaryStringTrap4")

--Mary's Rubber Trap TF
DL_ExtractBitmap("MaryRubberTrap0",  "Root/Images/DollManor/Characters/MaryRubberTrap0")
DL_ExtractBitmap("MaryRubberTrap1",  "Root/Images/DollManor/Characters/MaryRubberTrap1")
DL_ExtractBitmap("MaryRubberTrap2",  "Root/Images/DollManor/Characters/MaryRubberTrap2")
DL_ExtractBitmap("MaryRubberTrap3",  "Root/Images/DollManor/Characters/MaryRubberTrap3")
DL_ExtractBitmap("MaryRubberFinale", "Root/Images/DollManor/Characters/MaryRubberFinale")

-- |[ ========================================= Jessie ========================================= ]|
--Normal
DL_ExtractBitmap("Jessie", "Root/Images/DollManor/Characters/Jessie")

--General TF
DL_ExtractBitmap("JessieGeneralTF0", "Root/Images/DollManor/Transformation/JessieGeneralTF0")

--Jessie's String Trap TF
DL_ExtractBitmap("JessieStringTrap0", "Root/Images/DollManor/Characters/JessieStringTrap0")
DL_ExtractBitmap("JessieStringTrap1", "Root/Images/DollManor/Characters/JessieStringTrap1")
DL_ExtractBitmap("JessieStringTrap2", "Root/Images/DollManor/Characters/JessieStringTrap2")

--Clay
DL_ExtractBitmap("JessieClayBlueTF0",   "Root/Images/DollManor/Transformation/JessieClayBlueTF0")
DL_ExtractBitmap("JessieClayRedTF0",    "Root/Images/DollManor/Transformation/JessieClayRedTF0")
DL_ExtractBitmap("JessieClayYellowTF0", "Root/Images/DollManor/Transformation/JessieClayYellowTF0")

--Doll TF
DL_ExtractBitmap("JessieDollTF0", "Root/Images/DollManor/Transformation/JessieDollTF0")
DL_ExtractBitmap("JessieDollTF1", "Root/Images/DollManor/Transformation/JessieDollTF1")

--Glass
DL_ExtractBitmap("JessieGlass",    "Root/Images/DollManor/Characters/JessieGlass")
DL_ExtractBitmap("JessieGlassTF0", "Root/Images/DollManor/Transformation/JessieGlassTF0")

--Ice
DL_ExtractBitmap("JessieIce",    "Root/Images/DollManor/Characters/JessieIce")
DL_ExtractBitmap("JessieIceTF0", "Root/Images/DollManor/Transformation/JessieIceTF0")
DL_ExtractBitmap("JessieIceTF1", "Root/Images/DollManor/Transformation/JessieIceTF1")

--Stone
DL_ExtractBitmap("JessieStone",    "Root/Images/DollManor/Characters/JessieStone")
DL_ExtractBitmap("JessieStoneTF0", "Root/Images/DollManor/Transformation/JessieStoneTF0")
DL_ExtractBitmap("JessieStoneTF1", "Root/Images/DollManor/Transformation/JessieStoneTF1")

-- |[ ========================================= Lauren ========================================= ]|
--Normal
DL_ExtractBitmap("Lauren", "Root/Images/DollManor/Characters/Lauren")

--General TF
DL_ExtractBitmap("LaurenGeneralTF0", "Root/Images/DollManor/Transformation/LaurenGeneralTF0")

--Clay.
DL_ExtractBitmap("LaurenClayBlueTF0",   "Root/Images/DollManor/Transformation/LaurenClayBlueTF0")
DL_ExtractBitmap("LaurenClayRedTF0",    "Root/Images/DollManor/Transformation/LaurenClayRedTF0")
DL_ExtractBitmap("LaurenClayYellowTF0", "Root/Images/DollManor/Transformation/LaurenClayYellowTF0")

--Doll TF
DL_ExtractBitmap("LaurenDollTF0", "Root/Images/DollManor/Transformation/LaurenDollTF0")
DL_ExtractBitmap("LaurenDollTF1", "Root/Images/DollManor/Transformation/LaurenDollTF1")

--Glass
DL_ExtractBitmap("LaurenGlass",    "Root/Images/DollManor/Characters/LaurenGlass")
DL_ExtractBitmap("LaurenGlassTF0", "Root/Images/DollManor/Transformation/LaurenGlassTF0")

--Ice
DL_ExtractBitmap("LaurenIce",    "Root/Images/DollManor/Characters/LaurenIce")
DL_ExtractBitmap("LaurenIceTF0", "Root/Images/DollManor/Transformation/LaurenIceTF0")
DL_ExtractBitmap("LaurenIceTF1", "Root/Images/DollManor/Transformation/LaurenIceTF1")

--Stone
DL_ExtractBitmap("LaurenStone",    "Root/Images/DollManor/Characters/LaurenStone")
DL_ExtractBitmap("LaurenStoneTF0", "Root/Images/DollManor/Transformation/LaurenStoneTF0")
DL_ExtractBitmap("LaurenStoneTF1", "Root/Images/DollManor/Transformation/LaurenStoneTF1")

-- |[ ============================== Other Characters and Scenes =============================== ]|
--Pygmalie, no TFs
DL_ExtractBitmap("Pygmalie", "Root/Images/DollManor/Characters/Pygmalie")

--Crowd Shot
DL_ExtractBitmap("Doll Crowd Shot", "Root/Images/DollManor/Characters/DollCrowdShot")

--Stranger
DL_ExtractBitmap("Stranger0", "Root/Images/DollManor/Characters/Stranger0")
DL_ExtractBitmap("Stranger1", "Root/Images/DollManor/Characters/Stranger1")
DL_ExtractBitmap("Stranger2", "Root/Images/DollManor/Characters/Stranger2")
DL_ExtractBitmap("Stranger3", "Root/Images/DollManor/Characters/Stranger3")

--Sarah
DL_ExtractBitmap("Sarah", "Root/Images/DollManor/Characters/Sarah")

-- |[ ======================================= Blank Doll ======================================= ]|
--Blank Dolls, used in Doll TF
DL_ExtractBitmap("BlankDollA",      "Root/Images/DollManor/Characters/BlankDollA")
DL_ExtractBitmap("BlankDollB",      "Root/Images/DollManor/Characters/BlankDollB")
DL_ExtractBitmap("BlankDollC",      "Root/Images/DollManor/Characters/BlankDollC")
DL_ExtractBitmap("BlankDollD",      "Root/Images/DollManor/Characters/BlankDollD")
DL_ExtractBitmap("BlankDollE",      "Root/Images/DollManor/Characters/BlankDollE")
DL_ExtractBitmap("BlankDollUpperA", "Root/Images/DollManor/Characters/BlankDollUpperA")
DL_ExtractBitmap("BlankDollUpperB", "Root/Images/DollManor/Characters/BlankDollUpperB")
DL_ExtractBitmap("BlankDollUpperC", "Root/Images/DollManor/Characters/BlankDollUpperC")
DL_ExtractBitmap("BlankDollUpperD", "Root/Images/DollManor/Characters/BlankDollUpperD")
DL_ExtractBitmap("BlankDollUpperE", "Root/Images/DollManor/Characters/BlankDollUpperE")

-- |[ ================================= Dressed Dolls and TFs ================================== ]|
--Patterns
local iTotal = 6
local saPatterns = {"Bride", "Dancer", "Geisha", "Goth", "Princess", "Punk"}
for i = 1, iTotal, 1 do
    
    --Get the pattern.
    local sPattern = saPatterns[i]
    
    --Rip sequence.
    DL_ExtractBitmap("Doll " .. sPattern .. " A", "Root/Images/DollManor/Characters/Doll " .. sPattern .. " A")
    DL_ExtractBitmap("Doll " .. sPattern .. " B", "Root/Images/DollManor/Characters/Doll " .. sPattern .. " B")
    DL_ExtractBitmap("Doll " .. sPattern .. " C", "Root/Images/DollManor/Characters/Doll " .. sPattern .. " C")
    DL_ExtractBitmap("Doll " .. sPattern .. " D", "Root/Images/DollManor/Characters/Doll " .. sPattern .. " D")
    DL_ExtractBitmap("Doll " .. sPattern .. " E", "Root/Images/DollManor/Characters/Doll " .. sPattern .. " E")
    DL_ExtractBitmap(sPattern .. "TFBaseA", "Root/Images/DollManor/Transformation/" .. sPattern .. "TFBaseA")
    DL_ExtractBitmap(sPattern .. "TFBaseB", "Root/Images/DollManor/Transformation/" .. sPattern .. "TFBaseB")
    DL_ExtractBitmap(sPattern .. "TFBaseC", "Root/Images/DollManor/Transformation/" .. sPattern .. "TFBaseC")
    DL_ExtractBitmap(sPattern .. "TFBaseD", "Root/Images/DollManor/Transformation/" .. sPattern .. "TFBaseD")
    DL_ExtractBitmap(sPattern .. "TFBaseE", "Root/Images/DollManor/Transformation/" .. sPattern .. "TFBaseE")
    DL_ExtractBitmap(sPattern .. "TF0",     "Root/Images/DollManor/Transformation/" .. sPattern .. "TF0")
    DL_ExtractBitmap(sPattern .. "TF1",     "Root/Images/DollManor/Transformation/" .. sPattern .. "TF1")
    DL_ExtractBitmap(sPattern .. "TF2",     "Root/Images/DollManor/Transformation/" .. sPattern .. "TF2")
    DL_ExtractBitmap(sPattern .. "TF3",     "Root/Images/DollManor/Transformation/" .. sPattern .. "TF3")
    DL_ExtractBitmap(sPattern .. "TF4",     "Root/Images/DollManor/Transformation/" .. sPattern .. "TF4")
end

-- |[ ======================================= Clay Girls ======================================= ]|
--Clay Girls
DL_ExtractBitmap("ClayGirlBlue",   "Root/Images/DollManor/Characters/ClayGirlBlue")
DL_ExtractBitmap("ClayGirlRed",    "Root/Images/DollManor/Characters/ClayGirlRed")
DL_ExtractBitmap("ClayGirlYellow", "Root/Images/DollManor/Characters/ClayGirlYellow")

-- |[ ======================================== World UI ======================================== ]|
-- |[Misc]|
DL_AddPath("Root/Images/TxtAdv/UI/")
DL_ExtractBitmap("CombatWordBorderCard", "Root/Images/TxtAdv/UI/CombatWordBorderCard")

-- |[Navigation]|
DL_AddPath("Root/Images/TxtAdv/Navigation/")
DL_ExtractBitmap("Navigation|Look",     "Root/Images/TxtAdv/Navigation/Look")
DL_ExtractBitmap("Navigation|North",    "Root/Images/TxtAdv/Navigation/North")
DL_ExtractBitmap("Navigation|Up",       "Root/Images/TxtAdv/Navigation/Up")
DL_ExtractBitmap("Navigation|West",     "Root/Images/TxtAdv/Navigation/West")
DL_ExtractBitmap("Navigation|Wait",     "Root/Images/TxtAdv/Navigation/Wait")
DL_ExtractBitmap("Navigation|East",     "Root/Images/TxtAdv/Navigation/East")
DL_ExtractBitmap("Navigation|Think",    "Root/Images/TxtAdv/Navigation/Think")
DL_ExtractBitmap("Navigation|South",    "Root/Images/TxtAdv/Navigation/South")
DL_ExtractBitmap("Navigation|Down",     "Root/Images/TxtAdv/Navigation/Down")
DL_ExtractBitmap("Navigation|ZoomBar",  "Root/Images/TxtAdv/Navigation/ZoomBar")
DL_ExtractBitmap("Navigation|ZoomTick", "Root/Images/TxtAdv/Navigation/ZoomTick")

-- |[Locality]|
DL_AddPath("Root/Images/TxtAdv/Locality/")
DL_ExtractBitmap("Locality|Commands|Up",  "Root/Images/TxtAdv/Locality/Commands|Up")
DL_ExtractBitmap("Locality|Move|Up",      "Root/Images/TxtAdv/Locality/Move|Up")
DL_ExtractBitmap("Locality|Doors|Up",     "Root/Images/TxtAdv/Locality/Doors|Up")
DL_ExtractBitmap("Locality|Inventory|Up", "Root/Images/TxtAdv/Locality/Inventory|Up")
DL_ExtractBitmap("Locality|Entities|Up",  "Root/Images/TxtAdv/Locality/Entities|Up")
DL_ExtractBitmap("Locality|Items|Up",     "Root/Images/TxtAdv/Locality/Items|Up")
DL_ExtractBitmap("Locality|Equipment|Up", "Root/Images/TxtAdv/Locality/Equipment|Up")
DL_ExtractBitmap("Locality|Commands|Dn",  "Root/Images/TxtAdv/Locality/Commands|Dn")
DL_ExtractBitmap("Locality|Move|Dn",      "Root/Images/TxtAdv/Locality/Move|Dn")
DL_ExtractBitmap("Locality|Doors|Dn",     "Root/Images/TxtAdv/Locality/Doors|Dn")
DL_ExtractBitmap("Locality|Inventory|Dn", "Root/Images/TxtAdv/Locality/Inventory|Dn")
DL_ExtractBitmap("Locality|Entities|Dn",  "Root/Images/TxtAdv/Locality/Entities|Dn")
DL_ExtractBitmap("Locality|Items|Dn",     "Root/Images/TxtAdv/Locality/Items|Dn")
DL_ExtractBitmap("Locality|Equipment|Dn", "Root/Images/TxtAdv/Locality/Equipment|Dn")

-- |[ ======================================= Combat UI ======================================== ]|
DL_AddPath("Root/Images/DollManor/Combat/")
DL_ExtractBitmap("Combat|AttackBarFrame",         "Root/Images/DollManor/Combat/AttackBarFrame")
DL_ExtractBitmap("Combat|AttackBarFill",          "Root/Images/DollManor/Combat/AttackBarFill")
DL_ExtractBitmap("Combat|Bar_Heart",              "Root/Images/DollManor/Combat/Bar_Heart")
DL_ExtractBitmap("Combat|Bar_Half",               "Root/Images/DollManor/Combat/Bar_Half")
DL_ExtractBitmap("Combat|Bar_Empty",              "Root/Images/DollManor/Combat/Bar_Empty")
DL_ExtractBitmap("Combat|Bar_Shield",             "Root/Images/DollManor/Combat/Bar_Shield")
DL_ExtractBitmap("Combat|Bar_ShieldOrbit",        "Root/Images/DollManor/Combat/Bar_ShieldOrbit")
DL_ExtractBitmap("Combat|Btn_Ace",                "Root/Images/DollManor/Combat/Btn_Ace")
DL_ExtractBitmap("Combat|Btn_Act",                "Root/Images/DollManor/Combat/Btn_Act")
DL_ExtractBitmap("Combat|Btn_Draw",               "Root/Images/DollManor/Combat/Btn_Draw")
DL_ExtractBitmap("Combat|Btn_Shuffle",            "Root/Images/DollManor/Combat/Btn_Shuffle")
DL_ExtractBitmap("Combat|Btn_Shuffle_Highlight",  "Root/Images/DollManor/Combat/Btn_Shuffle_Highlight")
DL_ExtractBitmap("Combat|Btn_Surrender",          "Root/Images/DollManor/Combat/Btn_Surrender")
DL_ExtractBitmap("Combat|Btn_Potion",             "Root/Images/DollManor/Combat/Btn_Potion")
DL_ExtractBitmap("Combat|Card_Attack",            "Root/Images/DollManor/Combat/Card_Attack")
DL_ExtractBitmap("Combat|Card_And",               "Root/Images/DollManor/Combat/Card_And")
DL_ExtractBitmap("Combat|Card_Of",                "Root/Images/DollManor/Combat/Card_Of")
DL_ExtractBitmap("Combat|HealthBarFrame",         "Root/Images/DollManor/Combat/HealthBarFrame")
DL_ExtractBitmap("Combat|HealthBarFill",          "Root/Images/DollManor/Combat/HealthBarFill")
DL_ExtractBitmap("Combat|Card_AndOfHighlight",    "Root/Images/DollManor/Combat/Card_AndOfHighlight")
DL_ExtractBitmap("Combat|Card_Death",             "Root/Images/DollManor/Combat/Card_Death")
DL_ExtractBitmap("Combat|Card_Defend",            "Root/Images/DollManor/Combat/Card_Defend")
DL_ExtractBitmap("Combat|Card_Earth",             "Root/Images/DollManor/Combat/Card_Earth")
DL_ExtractBitmap("Combat|Card_Fire",              "Root/Images/DollManor/Combat/Card_Fire")
DL_ExtractBitmap("Combat|Card_Highlight",         "Root/Images/DollManor/Combat/Card_Highlight")
DL_ExtractBitmap("Combat|Card_Joker",             "Root/Images/DollManor/Combat/Card_Joker")
DL_ExtractBitmap("Combat|Card_Life",              "Root/Images/DollManor/Combat/Card_Life")
DL_ExtractBitmap("Combat|Card_Water",             "Root/Images/DollManor/Combat/Card_Water")
DL_ExtractBitmap("Combat|Card_Wind",              "Root/Images/DollManor/Combat/Card_Wind")
DL_ExtractBitmap("Combat|ExpandableFrameL",       "Root/Images/DollManor/Combat/ExpandableFrameL")
DL_ExtractBitmap("Combat|ExpandableFrameM",       "Root/Images/DollManor/Combat/ExpandableFrameM")
DL_ExtractBitmap("Combat|ExpandableFrameR",       "Root/Images/DollManor/Combat/ExpandableFrameR")
DL_ExtractBitmap("Combat|Icon_Attack",            "Root/Images/DollManor/Combat/Icon_Attack")
DL_ExtractBitmap("Combat|Icon_Death",             "Root/Images/DollManor/Combat/Icon_Death")
DL_ExtractBitmap("Combat|Icon_Defend",            "Root/Images/DollManor/Combat/Icon_Defend")
DL_ExtractBitmap("Combat|Icon_Earth",             "Root/Images/DollManor/Combat/Icon_Earth")
DL_ExtractBitmap("Combat|Icon_Fire",              "Root/Images/DollManor/Combat/Icon_Fire")
DL_ExtractBitmap("Combat|Icon_Life",              "Root/Images/DollManor/Combat/Icon_Life")
DL_ExtractBitmap("Combat|Icon_Water",             "Root/Images/DollManor/Combat/Icon_Water")
DL_ExtractBitmap("Combat|Icon_Wind",              "Root/Images/DollManor/Combat/Icon_Wind")
DL_ExtractBitmap("Combat|Static_WordDivider",     "Root/Images/DollManor/Combat/Static_WordDivider")

--Clean GL State.
ALB_SetTextureProperty("Restore Defaults")

-- |[World Dialogue]|
WD_SetProperty("Construct")

--Options Post-Exec
TL_SetProperty("Post-Exec Script", fnResolvePath() .. "997 Options PostExec.lua")

--Reset Handler
TL_SetProperty("Reset Script", fnResolvePath() .. "998 Exec Restart.lua")

--Ending Handler
TL_SetProperty("Ending Script", fnResolvePath() .. "501 Ending Relive.lua")

--Clean
SLF_Close()

--Nav Buttons. These are constants from C++.
ciNavLook = 1
ciNavNorth = 2
ciNavUp = 4
ciNavWest = 8
ciNavWait = 16
ciNavEast = 32
ciNavThink = 64
ciNavSouth = 128
ciNavDown = 256
ciNavShowNothing = 512

--Dialogue Constants
gsDE = "\n\n" --"Dialogue Ender". Goes at the end of dialogue lines.

-- |[ =================================== Creation and Setup =================================== ]|
--Create the main menu.
Debug_DropEvents()
STT_Create(fnResolvePath() .. "000a Post Title Boot.lua")
if(gbHasLoadedData == false) then
    LI_Finish()
    LI_WaitForKeypress()
end
gbHasLoadedData = true
