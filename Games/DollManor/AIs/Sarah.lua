-- |[ ======================================= Sarah's AI ======================================= ]|
--Follows Mary in phase 6. That is all.

-- |[ =================================== Hostility Checker ==================================== ]|
--AI is checking whether it is hostile to the player.
fnSarah_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = false
end

-- |[ =================================== Post-Turn Spotter ==================================== ]|
--Sarah does nothing post-turn.
fnSarah_PostTurnSpotCheck = function(i)
end

-- |[ ======================================= Normal AI ======================================== ]|
--Main AI sequence.
fnSarah_AI = function(i)
    
    --In Phase 6, she moves to the player's location. That's it.
    if(gzTextVar.iGameStage == 6) then
        gzTextVar.zEntities[gzTextVar.iSarahIndex].sLocation = gzTextVar.gzPlayer.sLocation
    end
    
end

