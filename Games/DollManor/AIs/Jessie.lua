-- |[ ====================================== Jessie's AI ======================================= ]|
--Jessie will remain where she is until you talk to her, at which point she will follow you to
-- the main hall.
--If you're a doll, she'll hold still until you attack her.

-- |[ =================================== Hostility Checker ==================================== ]|
--AI is checking whether it is hostile to the player.
fnJessie_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = false
end

-- |[ =================================== Post-Turn Spotter ==================================== ]|
--Jessie does nothing post-turn.
fnJessie_PostTurnSpotCheck = function(i)
end

-- |[ ======================================= Normal AI ======================================== ]|
--Main AI sequence.
fnJessie_AI = function(i)

    -- |[ ========== Blocking Cases ========== ]|
    --Petrified Jessie never moves.
    if(gzTextVar.zEntities[i].sState == "Statue") then
        return
    end

    --Crippled. Jessie cannot move.
    if(gzTextVar.zEntities[i].bIsCrippled == true) then
        
        --Player must in the same location.
        if(gzTextVar.gzPlayer.sLocation ~= gzTextVar.zEntities[i].sLocation) then return end
        
        --Does not play if the player is a statue.
        if(gzTextVar.gzPlayer.sFormState ~= "Statue") then
            TL_SetProperty("Append", "Jessie lies on the ground, too hurt to move. She lets out a whimper." .. gsDE)
        end
        return
    end

    -- |[ ========== Pathing Checker ========== ]|
    --If Jessie is following, she paths to your location on her turn.
    if(gzTextVar.bIsJessieFollowing and gzTextVar.bIsJessieMainHalled == false) then
        
        --If Jessie is in the same room as Mary, do nothing.
        if(gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[i].sLocation) then
        
        --Otherwise, move to the player's position.
        else
        
            --Do nothing if the archive trap is active:
            if(gzTextVar.bStringTrapNoFollow == true) then return end
            
            --Do nothing if the player is in the brewery trap.
            if(gzTextVar.bBreweryNoFollow == true) then return end
            
            --Do nothing if the player is in the ice caverns trap.
            if(gzTextVar.bFreezingNoFollow == true) then return end
            
            --Do nothing if the player is in the claygirl trap.
            if(gzTextVar.bClayNoFollow == true) then return end
        
            --Run the function to find the destination.
            local sMoveTo = fnGetMovePath(gzTextVar.zEntities[i].sLocation, gzTextVar.gzPlayer.sLocation)
            
            --If the room came back as "Null" do nothing.
            if(sMoveTo == "Null") then return end
            
            --If the room is "Unsettling Passage", do nothing. This is for plot reasons. Once the light is disabled and the cutscene plays, resume following.
            if(sMoveTo == "Unsettling Passage" and gzTextVar.bSecretStatue == false and gzTextVar.bDisabledLightCutscene == false) then
                
                --Jessie must be a human or she will ignore the block.
                if(gzTextVar.zEntities[i].sState == "Human") then
                    return
                end
            end

            --Move indicators.
            local fOldX, fOldY, fOldZ = fnGetRoomPosition(gzTextVar.zEntities[i].sLocation)
            local fX, fY, fZ = fnGetRoomPosition(sMoveTo)
            TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[i].sIndicatorName, fX, fY, fZ)
            
            --Move to that room.
            gzTextVar.zEntities[i].sLocation = sMoveTo
            
            --If the destination was reached, reset the move handler.
            if(gzTextVar.zEntities[i].sLocation == sPatrolTarget) then
                gzTextVar.zEntities[i].iMoveIndex = -1
            end
            
        end
        
        --If the player is a doll, then Jessie is being carried as a blank doll.
        if(gzTextVar.gzPlayer.sFormState ~= "Human") then
            
            --Jessie is a blank doll. Lock player's movement when reaching the main hall.
            if(gzTextVar.gzPlayer.sFormState == "Blank Doll" and gzTextVar.zEntities[i].sLocation == "Main Hall") then
                gzTextVar.gzPlayer.bDollMoveLock = true
            end
            return
        end
        
        --Is this a "Main Hall" location. There are several of these since the player can pass through them.
        local bIsMainHallLocation = false
        if(gzTextVar.gzPlayer.sLocation == "Main Hall")    then bIsMainHallLocation = true end
        if(gzTextVar.gzPlayer.sLocation == "Main Hall NW") then bIsMainHallLocation = true end
        if(gzTextVar.gzPlayer.sLocation == "Main Hall NE") then bIsMainHallLocation = true end
        
        --End of Jessie Following sequence:
        if(gzTextVar.iGameStage == 1 and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[i].sLocation and bIsMainHallLocation and gzTextVar.bIsJessieMainHalled == false) then
            
            --Brief cutscene if Jessie isn't already in the main hall:
            if(gzTextVar.bIsJessieMainHalled == false) then
                
                --Flags.
                gzTextVar.bIsJessieMainHalled = true
                gzTextVar.bIsJessieFollowing = false
            
                --Lauren is not here:
                if(gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneHumans/Main Hall Enter Jessie No Lauren.lua")
                
                    --Move Jessie to the Main Hall. This is needed if we're not currently there.
                    local sOldLocation = gzTextVar.zEntities[i].sLocation
                    gzTextVar.zEntities[i].sLocation = "Main Hall"
                    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
                    fNewX, fNewY, fNewZ = fnGetRoomPosition("Main Hall")
                    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[i].sIndicatorName, fNewX, fNewY, fNewZ)
                
                --Lauren is already here:
                else
                    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneHumans/Main Hall Pygmalie Leaves.lua")
                end
            end
        end
    end
end
