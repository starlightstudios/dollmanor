-- |[ ===================================== Doll Titan AI ====================================== ]|
--Resinous Titans. These have a different transformation sequence but use the same movement logic
-- as regular dolls do. They are extremely strong in combat.

-- |[ =================================== Hostility Checker ==================================== ]|
--AI is checking whether it is hostile to the player.
fnTitan_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = true
end

-- |[ =================================== Post-Turn Spotter ==================================== ]|
--This logic runs after turn execution. The entity cannot move but can attempt to spot the player
-- after movement has occurred. This prevents a quick door close from spotting the player.
--There is a percentage chance this may do nothing, depending on the AI.
fnTitan_PostTurnSpotCheck = function(i)
    
    -- |[Non-Human]|
    --If the player is not a human, don't bother.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then return end

    -- |[Run Sight Checker]|
    --Common: Determine if we can see the player where we are.
    local bCanSeePlayer = fnStandardAI_SpotPlayer(i)

    -- |[Movement Handlers]|
    --Claygirl is moving to the location where it last saw the player.
    if(gzTextVar.zEntities[i].sLastSawPlayer ~= "Nowhere") then
        
        --Can we see the player? Update chasing to the new destination.
        if(bCanSeePlayer == true) then
            gzTextVar.zEntities[i].sLastSawPlayer = gzTextVar.gzPlayer.sLocation
        end

    --We are not chasing the player, but we are patrolling back to where we started.
    elseif(gzTextVar.zEntities[i].sFirstSawPlayer ~= "Nowhere") then
        
        --If we spotted the player, resume the chase.
        if(bCanSeePlayer == true) then
        
            --Store the location where we spotted the player.
            gzTextVar.zEntities[i].sLastSawPlayer = gzTextVar.gzPlayer.sLocation
            
        --Can't see the player. Resume moving back to the starting location.
        else
        end
            
        --We handled the movement.
        return
        
    --Claygirl is neither chasing nor returning to the start of a chase.
    else
        
        --If we can see the player, initialize the chase.
        if(bCanSeePlayer == true) then
            
            --Store both the player's location and where we started the chase. Dolls path back to where they started.
            gzTextVar.zEntities[i].sFirstSawPlayer = gzTextVar.zEntities[i].sLocation
            gzTextVar.zEntities[i].sLastSawPlayer = gzTextVar.gzPlayer.sLocation
        
        --Can't see the player. Do nothing here.
        else
    
        end
    end
end

-- |[ =================================== Catching Function ==================================== ]|
--Called if the entity catches the player and initiates a battle.
fnTitan_CatchPlayer = function(i)
    
    --If the player is not a human, don't bother.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then return end
    
    --Player is surrendering.
    if(gzTextVar.bIsPlayerSurrendering == true) then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneTitan/Surrender To Titan.lua", i)
        return
    end
    
    --Dialogue.
    if(gzTextVar.bCombatActivatedThisTick == false) then
        gzTextVar.bCombatActivatedThisTick = true
        gzTextVar.gzPlayer.bHasEncounteredDoll = true
        TL_SetProperty("Append", "The yellow doll girl smiles at you and says 'Found you.' Defend yourself!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, "Standard", "Standard")
    
    --Combat is active. Run the handler but not in queue.
    elseif(gzTextVar.bCombatActivatedThisTick == true) then
        TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, "Standard", "Standard")
    end
    
end

-- |[ ======================================= Normal AI ======================================== ]|
--Main AI sequence.
fnTitan_AI = function(i)

    -- |[ ======== Detection Handling ======== ]|
    -- |[Same Room, Player Surrendering]|
    --In the same room, player is surrendering.
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.gzPlayer.sFormState == "Human" and gzTextVar.bIsPlayerSurrendering == true) then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneTitan/Surrender To Titan.lua", i)
        return

    -- |[Same Room, Player Hostile]|
    --If in the same room, a battle will start.
    elseif(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsTriggeringFights) then
    
        -- |[Player Crippled]|
        --If the player is crippled but has not been transformed yet:
        if(gzTextVar.gzPlayer.bIsCrippled == true and gzTextVar.gzPlayer.sFormState == "Human") then
            LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneTitan/Defeated By Titan.lua", i)
            return
        
        --Humans trigger a battle.
        elseif(gzTextVar.gzPlayer.sFormState == "Human") then
            fnTitan_CatchPlayer(i)
            return
        
        -- |[Not Crippled, Battle]|
        --Other cases, titan ignores you.
        else
        
        end
    end

    -- |[ ========== Spot the Player ========= ]|
    --Run the AI to check if we can see the player. If so, we don't bother checking doors.
    local bCanSeePlayer = false
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        bCanSeePlayer = fnStandardAI_SpotPlayer(i)
    end

    -- |[ =========== Door Handling ========== ]|
    local bClosedDoorThisTurn = fnStandardAI_HandleDoor(i)

    -- |[ ========= Movement Failure ========= ]|
    --At this point, dolls have a 15% chance to simply not move for a turn. This makes them harder to
    -- predict and slightly easier to outrun. When chasing, this is reduced.
    local iSkipRoll = LM_GetRandomNumber(1, 100)
    local iSkipChance = 15
    if(bCanSeePlayer) then iSkipChance = 5 end
    if(iSkipRoll <= iSkipChance) then
        return 
    end

    -- |[ ======== Spot-Player Logic ========= ]|
    --Attempt to spot the player from our current location. If the player is more than 3 tiles away,
    -- don't bother running the spotter algorithm to save time.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        local bHandledUpdate = fnStandardAI_SpotChasePlayer(i, bCanSeePlayer, fnTitan_CatchPlayer)
        if(bHandledUpdate) then return end
    end

    -- |[ =========== Patrol Logic ========== ]|
    -- |[Door Handler]|
    --If a door was closed this turn, stop here.
    if(bClosedDoorThisTurn == true) then return end
    
    -- |[Run Logic]|
    fnStandardAI_Patrol(i, fnTitan_CatchPlayer)
    
end
