-- |[ ============================ Main Hall Enter Lauren No Jessie ============================ ]|
--When entering the main hall with Lauren, but Jessie is not present, this cutscene plays.

--Fast-access variables.
local l = gzTextVar.iLaurenIndex

--Fast-access strings.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sPygmPath = "Root/Images/DollManor/Characters/Pygmalie"

--Setup
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Dialogue.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'This is the place. I think we should be safe here for the time being.'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Hello, ma'am. Do you live here?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh my goodness, did you make a new friend, little one? Oh, but she's a little off, isn't she?'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Uh, I'm Lauren, and I'm a boy...'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Don't be ridiculous, little one!' Pygmalie says with a laugh." .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Uhhhh...'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'She might be a little loopy, Lauren, but I don't think she's actually dangerous. Just try not to upset her.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'My my, little one. Talking about your creator as if she's not listening in? Naughty, naughty!'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Okay...'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I know this might be tough, but could you stay here? I'm sure Jessie is someplace nearby, so I'm going to look for her.'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Stay here with her?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Keep your distance and be polite.'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'Okay...'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I'll be back soon with Jessie.'" .. gsDE)
fnDialogueFinale()
