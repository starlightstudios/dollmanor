-- |[ ================================= Doll Subscript: Geisha ================================= ]|
--Contains the call strings used for the Geisha transformation. Used when someone other than the player
-- is being transformed. Dialogue only displays if the player is watching.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--Other variables.
local sName = gzTextVar.zEntities[i].sDisplayName
local sUseSkin = "Root/Images/DollManor/Transformation/GeishaTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

-- |[Sequence]|
--Blank Doll: Begin transformation:
if(gzTextVar.zEntities[i].sState == "Blank Doll") then
    
    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF0"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/BlankDoll"
    
    --Sequence
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Well done, my sweet. You've brought a playmate, exactly as I intended.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "You feel your chest swell with pride. Your creator paid you a compliment! You want to cry with joy, but will have to settle for watching a new sister be born." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", 0)
    TL_SetProperty("Append", "'As for you, hmm, let's see what we can do...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Oh, of course. A beautiful, mysterious geisha. That will fit into the ensemble nicely.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 1:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF0") then

    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF1"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GeishaTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/GeishaTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie applies a layer of base blush, but is careful to keep your sister's makeup even. She gives her pouty black lipstick to match." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, I think this is coming together well.' Your creator muses. You stand with your hands folded, quietly waiting for an order." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'But you need something for that head of yours, don't you? Dearest, please get the hairpiece for me.'".. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snap into motion, joints creaking slightly. You know which hairpiece to retrieve. You know exactly what your creator wants." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is a mantlepiece in the workshop which holds hairpieces for special occasions. The hairstyles of great performers are mirrored there. Some look uncannily real." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You pick up a short black piece, tied in a bun. Lovely wooden sticks with hearts on strings balance the color. It is immaculate and seems to shine in the darkness." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/GeishaTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "You and hand the hairpiece to your creator, who puts it on your new sister's head. You hear a slithering sound as the hair digs into her scalp, becoming her hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Marvelous, the perfect choice! Yes, yes!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The hair fits your sister perfectly. You can't wait to see what she will look like when she's finished!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 2:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF1") then

    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF2"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GeishaTF1"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "'There is, of course, no geisha who would go about in such a state. The soft skin must be hidden beneath fine silk. Don't you agree, dearie?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A surge of joy fills you as your creator's orders reach your ears. You make your way to the clotheracks, knowing exactly which dress would fit." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It's a blue silk dress with flower designs on it. When you touch it, it feels like it weighs nothing. It's so soft that you are practically feeling air." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You return to your creator bearing the dress and a set of raised platform shoes. You don't know where the shoes came from. It doesn't matter. Your creator beams at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Good work, dolly. What do we say when we do good work?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "'A good dolly is an obedient dolly,' You say without hesitation." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator pats your head. You are so proud that you remembered what to say when asked, despite never having heard that phrase before." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF2") then

    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF3"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GeishaTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Now let's put this on you... There! Oh how marvellous!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator clasps her heads near her head. Your sister is too cute to resist! You want to play with her so badly, but you remain still until told to move." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Paints. You must fetch the paints for your creator! Your joints spring to life before the thought even finishes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF3") then

    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF4"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GeishaTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Royal blue eyes will match your dress, yes, yes...' Your creator mutters. You bring her the paints." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She pats you on the head and brushes her hand past your cheek. You would melt with joy, but haven't been ordered to, so you stand to the side." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator paints eyes on your sister as you stand, patiently waiting." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 4:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF4") then

    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF5"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GeishaTF3"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "As the eyes finish painting, they move slightly. They focus on objects around the room, then on you, then on your creator. Your sister blinks. Then her eyes go still again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "You hear a strange sucking sound as the eyes become real. Your sister has big blue eyes and she's so cute!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'We've done so well, but we're not done yet. You'll be able to play with her soon, dearie.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You're so excited you can hardly contain yourself! You settle for standing with your hands folded near the chair as your creator puts on the finishing touches." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 5:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF5") then

    --Flags
    gzTextVar.zEntities[i].sState = "Geisha TF6"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GeishaTF4"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Now this is a tricky one, but I'm sure I'm up to the task. Let's fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand atop your sister's head. Her eyes begin to dart back and forth." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Thoughts are flooding into her. Memories, ideas, places, names. Orders. Commands. Obedience." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smirks. She is graceful, elegant, delicate, mysterious, and totally obedient to her creator. Your new sister smiles at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her joints begin to stir. She is ready." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 6:
elseif(gzTextVar.zEntities[i].sState == "Geisha TF6") then

    --Flags
    gzTextVar.zEntities[i].sState = "Doll Geisha"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Geisha " .. sEndLetter
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 3, 4, ciCodeFriendly)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Geisha " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "" .. sName .. " obeys instantly. She stands as her creator searches her for imperfections. There are none." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " smiles and curtseys at her creator, reborn as a doll." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " turns to you. 'I humbly thank you, sister. I owe you much. I look forward to playing with you.' You smile. You share her joy." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

    --Change AIs.
    gzTextVar.zEntities[i].bIsCrippled = false
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Doll.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Doll"
    gzTextVar.gzPlayer.bDollMoveLock = false

end