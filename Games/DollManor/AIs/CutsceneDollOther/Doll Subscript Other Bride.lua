-- |[ ================================= Doll Subscript: Bride ================================== ]|
--Contains the call strings used for the Bride transformation. Used when someone other than the player
-- is being transformed. Dialogue only displays if the player is watching.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--Other variables.
local sName = gzTextVar.zEntities[i].sDisplayName
local sUseSkin = "Root/Images/DollManor/Transformation/BrideTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

-- |[Sequence]|
--Blank Doll: Begin transformation:
if(gzTextVar.zEntities[i].sState == "Blank Doll") then
    
    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF0"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/BlankDoll"
    
    --Sequence
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Well done, my sweet. You've brought a playmate, exactly as I intended.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "You feel your chest swell with pride. Your creator paid you a compliment! You want to cry with joy, but will have to settle for watching a new sister be born." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'As for you, hmm, let's see what we can do...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Oh what a joyous occasion! A wedding, and a beautiful blushing bride! What a thing for us to witness!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 1:
elseif(gzTextVar.zEntities[i].sState == "Bride TF0") then

    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF1"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/BrideTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/BrideTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie applies some blush and eyeliner, as well as pink lipstick. She takes a step back to admire her work." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, yes, I think teal hair would match the dress. Yes. Hmm...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "Your creator waves you over, and you instantly obey. You stand with your hands folded next to her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'How may I help?' you ask. She smirks. An idea appears in your head. There is a wig you should fetch." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You curtsy and journey into the clothesracks that line the room. You search and search. Suddenly, the wig is in front of you, practically glowing." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You deliver the wig to your creator, who takes it and quickly places it on your sister's head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/BrideTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "You hear a scratching sound as the wig attaches itself, becoming your sister's hair. You smile broadly as your creator places a veil over the hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You try to hide your excitement as it builds. You haven't been told to yet, but you want to play with your sister so badly!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 2:
elseif(gzTextVar.zEntities[i].sState == "Bride TF1") then

    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF2"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/BrideTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/BrideTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "'But the best part of a bridal gown is the dress. Yes, yes, I know what theme it should have. Darling, would you?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your joints snap into motion before your creator even finishes her sentence. You once again descend upon the clothesracks." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It's big, blue, and soft. It flows. You see it perfectly in your head, and suddenly you stand before it. It was made just for your sister." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You pick it up, careful not to stretch or stain it. It is immaculate. Your excitement builds as you carry it back to your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Very well done, my dear. And the bouquet?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Somehow, a bouquet appears in your hands. You brought it for your sister! How thoughtful of your creator. You are glad you can help her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Bride TF2") then

    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF3"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/BrideTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "The dress fits your new sister perfectly. She remains totally still, unmoving, unthinking. You are eager to play with her, but she is incomplete." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'It's funny, isn't it, dearie?' Your creator asks. You smile. You don't know what's funny but you want to help." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, most amusing'. You giggle. What a funny joke! Your creator is so smart." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Bride TF3") then

    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF4"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/BrideTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'I think violet eyes would suit you best, we have a very cool color set so far. One moment.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator snaps her fingers. In your hands appears a set of paints. You giggle and hand them to her. She pats your head and turns to the puppet." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 4:
elseif(gzTextVar.zEntities[i].sState == "Bride TF4") then

    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF5"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/BrideTF3"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator paints gorgeous blue eyes onto the puppet. They focus on you briefly, and you swear the mouth almost moved. Then, the eyes go inert again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "A sucking sound emanates from somewhere. Your sister is nearly done!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Your big day awaits, little dearie. Not long now.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You stand near your creator again, hands folded. You wait patiently. Maybe she'll let you help again!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 5:
elseif(gzTextVar.zEntities[i].sState == "Bride TF5") then

    --Flags
    gzTextVar.zEntities[i].sState = "Bride TF6"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/BrideTF4"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'But let's fill in your empty mind. Can't have you sitting there on your big day, can we?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Drawing near to the puppet, your creator places her hand atop your sister's head. Her eyes begin to dart back and forth." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Thoughts are flooding into her. Memories, ideas, places, names. Orders. Commands. Obedience." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smirks. She is pretty, excited, joyous, and utterly obedient to your creator. She smiles." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her joints begin to stir. She is ready." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 6:
elseif(gzTextVar.zEntities[i].sState == "Bride TF6") then

    --Flags
    gzTextVar.zEntities[i].sState = "Doll Bride"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Bride " .. sEndLetter
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 1, 4, ciCodeFriendly)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Bride " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "" .. sName .. " obeys instantly. She stands as her creator searches her for imperfections. There are none." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " smiles and curtseys at her creator, reborn as a doll." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " turns to you. 'Thank you so much, sister. I've never felt better.' You smile. You share her joy." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

    --Change AIs.
    gzTextVar.zEntities[i].bIsCrippled = false
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Doll.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Doll"
    gzTextVar.gzPlayer.bDollMoveLock = false

end