-- |[ ================================== Doll Subscript: Punk ================================== ]|
--Contains the call strings used for the Punk transformation. Used when someone other than the player
-- is being transformed. Dialogue only displays if the player is watching.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--Other variables.
local sName = gzTextVar.zEntities[i].sDisplayName
local sUseSkin = "Root/Images/DollManor/Transformation/PunkTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

-- |[Sequence]|
--Blank Doll: Begin transformation:
if(gzTextVar.zEntities[i].sState == "Blank Doll") then
    
    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF0"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/BlankDoll"
    
    --Sequence
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Well done, my sweet. You've brought a playmate, exactly as I intended.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "You feel your chest swell with pride. Your creator paid you a compliment! You want to cry with joy, but will have to settle for watching a new sister be born." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", 0)
    TL_SetProperty("Append", "'As for you, hmm, let's see what we can do...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I've got that itch, and that itch can only mean one thing - tough dollies rebelling against *the man*!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 1:
elseif(gzTextVar.zEntities[i].sState == "Punk TF0") then

    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF1"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PunkTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/PunkTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie produces some piercings from her smock, and applies them to your sister's face. You hear the sound of plastic snapping as each one goes in." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Nothing says dislike of authority like sticking metal into one's face, right? Hmm, what to do about the hair...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator mulls it over. You stand nearby, empty. You have no ideas. You are incapable of being creative. Dollies are best when they are empty." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, I know... This is a style, isn't it? Yes...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Pygmalie disappears amongst the racks of clothes that line the room. She is searching for something." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'This will do nicely,' you hear from amidst the racks. She reappears." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/PunkTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "She bears a purple wig, parted and with the roots showing. She beams. The wig seems impossible, but good dollies don't ask how or why. Good dollies obey. You grin." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'It fits so well! I'm definitely feeling the violet aesthetic here.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hear a scratching sound as the hair attaches to your sister's head. She now has short purple hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You maintain your icy smile as your creator decides the next step. You can't wait for your sister to be ready!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 2:
elseif(gzTextVar.zEntities[i].sState == "Punk TF1") then

    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF2"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PunkTF1"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Ah, let's see. I think we'll need some alterations. Dearie, go get the clothes I will need.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snap into motion, walking to the clothesracks. Your creator wants a shirt, skirt, and boots. You search for the right ones. You know exactly what they look like." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You emptily pick them up and bring the clothes back. While you carry them, the shirt wears and tatters. The clothes look used and ratty. Your creator smiles." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Possibly retrieved from the bin, yes, these will do. Good work, dearie.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator pats you on the head. You are bursting with joy! You love helping your creator!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Punk TF2") then

    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF3"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PunkTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Ah ha ha ha ha! So wild, so untamed, so riotous! Beautiful and cute, but rough and edgy. I love it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your sister looks tough but caring. You can't wait to play with her!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "With a snap of your creator's fingers, a paintbrush appears in your hand, along with a selection of paints in the other. You stand emptily staring ahead until ordered to move." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Punk TF3") then

    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF4"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PunkTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'She's going to need eyes. Dolly, please get me some paints.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You bring the paints to her. All thoughts of how they appeared in your hands vanish. They were always there. Good dollies don't ask why. You are a good dolly." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 4:
elseif(gzTextVar.zEntities[i].sState == "Punk TF4") then

    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF5"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PunkTF3"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "With paintbrush in hand, your creator paints vibrant yellow eyes on your sister. She smiles as she finishes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "The eyes move on their own, drifting to you, then your creator, then they resume staring straight ahead." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yellow eyes, like a cat's. Visible in the dark, to send the forces of order running!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is almost ready. You can feel excitement building, but remain at your creator's call." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 5:
elseif(gzTextVar.zEntities[i].sState == "Punk TF5") then

    --Flags
    gzTextVar.zEntities[i].sState = "Punk TF6"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PunkTF4"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'There is just one last detail. Let's instill some fighting spirit into you...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Drawing near to the puppet, your creator places her hand atop your sister's head. Her eyes begin to dart back and forth." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Thoughts are flooding into her. Memories, ideas, places, names. Orders. Commands. Obedience." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smirks. She is tough, wild, rebellious, and yet somehow totally obedient to your creator. Your new sister smiles at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her joints begin to stir. She is ready." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 6:
elseif(gzTextVar.zEntities[i].sState == "Punk TF6") then

    --Flags
    gzTextVar.zEntities[i].sState = "Doll Punk"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Punk " .. sEndLetter
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 5, 4, ciCodeFriendly)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Punk " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "" .. sName .. " obeys instantly. She stands as her creator searches her for imperfections. There are none." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " smiles and curtseys at her creator, reborn as a doll." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " turns to you. 'Tch, thanks, loser. I guess I owe you one,' she says with a wink. You smile. You're going to love playing together!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

    --Change AIs.
    gzTextVar.zEntities[i].bIsCrippled = false
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Doll.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Doll"
    gzTextVar.gzPlayer.bDollMoveLock = false

end