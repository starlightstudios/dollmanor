-- |[ ===================================== Pygmalie's AI ====================================== ]|
--"Doesn't do a whole lot." -Salty

-- |[ =================================== Hostility Checker ==================================== ]|
--AI is checking whether it is hostile to the player.
fnPygmalie_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = false
end

-- |[ =================================== Post-Turn Spotter ==================================== ]|
--Pygmalie does nothing post-turn.
fnPygmalie_PostTurnSpotCheck = function(i)
end

-- |[ ======================================= Normal AI ======================================== ]|
--Main AI sequence.
fnPygmalie_AI = function(i)

    --Fast-access Variables.
    local j = gzTextVar.iJessieIndex
    local l = gzTextVar.iLaurenIndex

    --Flags.
    gzTextVar.gzPlayer.bDollMoveLock = false

    -- |[ = Mary Entering Altar Room Handlers = ]|
    --In Phase 2, entering the Ritual Altar room on the second floor causes the player to be defeated.
    if(gzTextVar.iGameStage == 2 and gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.gzPlayer.sFormState == "Human") then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutscenePygmalie/Unprepared Mary.lua")
        
    --In Phase 4, entering the ritual room alone causes Mary to be defeated immediately. She gets dolled as normal.
    elseif(gzTextVar.iGameStage == 4 and gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.gzPlayer.sFormState == "Human") then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutscenePygmalie/Unprepared Mary Alone.lua")
        
    --In Phase 5, entering the ritual room alone causes Mary to be defeated immediately. She gets dolled as normal. The dialogue is different.
    elseif(gzTextVar.iGameStage == 5 and gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.gzPlayer.sFormState == "Human") then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutscenePygmalie/Dumb Mary.lua")
    end

    -- |[ ========= Detection Logic ========== ]|
    --If in the same room, and the player is a blank doll, Pygmalie will begin the transformation.
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation and string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) ~= "Doll") then
        
        --In phase 5 the preference mandates Dancer.
        if(gzTextVar.iGameStage == 5) then
            gzTextVar.gzPlayer.sTFSeq = "Dancer"
        end
        
        -- |[Princess]|
        if(gzTextVar.gzPlayer.sTFSeq == "Princess") then
            LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneDoll/Doll Subscript Princess.lua")
            return
        
        -- |[Geisha]|
        elseif(gzTextVar.gzPlayer.sTFSeq == "Geisha") then
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDoll/Doll Subscript Geisha.lua")
            return
        
        -- |[Dancer]|
        elseif(gzTextVar.gzPlayer.sTFSeq == "Dancer") then
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDoll/Doll Subscript Dancer.lua")
            return
        
        -- |[Goth]|
        elseif(gzTextVar.gzPlayer.sTFSeq == "Goth") then
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDoll/Doll Subscript Goth.lua")
            return
        
        -- |[Bride]|
        elseif(gzTextVar.gzPlayer.sTFSeq == "Bride") then
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDoll/Doll Subscript Bride.lua")
            return
        
        -- |[Punk]|
        elseif(gzTextVar.gzPlayer.sTFSeq == "Punk") then
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDoll/Doll Subscript Punk.lua")
            return
        end
    end

    -- |[ ========= Stop in Phase 6 ========== ]|
    --Jessie and Lauren will not be transformed in phase 6. Everything below this point is TF handlers.
    if(gzTextVar.iGameStage == 6) then return end

    -- |[Jessie TF]|
    --If Jessie is in the same room.
    if(gzTextVar.zEntities[j].sLocation == gzTextVar.zEntities[i].sLocation) then
        
        --Blank Doll:
        if(gzTextVar.zEntities[j].sState == "Blank Doll") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            
            --Roll!
            local iRoll = LM_GetRandomNumber(1, 6)
            if(iRoll == 1) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Bride.lua", j)
            elseif(iRoll == 2) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Dancer.lua", j)
            elseif(iRoll == 3) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Geisha.lua", j)
            elseif(iRoll == 4) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Goth.lua", j)
            elseif(iRoll == 5) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Princess.lua", j)
            else
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Punk.lua", j)
            end
            return

        --Bride TF:
        elseif(string.sub(gzTextVar.zEntities[j].sState, 1, 5) == "Bride") then
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Bride.lua", j)
            return

        --Dancer TF:
        elseif(string.sub(gzTextVar.zEntities[j].sState, 1, 6) == "Dancer") then
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Dancer.lua", j)
            return

        --Geisha TF:
        elseif(string.sub(gzTextVar.zEntities[j].sState, 1, 6) == "Geisha") then
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Geisha.lua", j)
            return

        --Goth TF:
        elseif(string.sub(gzTextVar.zEntities[j].sState, 1, 4) == "Goth") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Goth.lua", j)
            return

        --Princess TF:
        elseif(string.sub(gzTextVar.zEntities[j].sState, 1, 8) == "Princess") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Princess.lua", j)
            return

        --Punk TF:
        elseif(string.sub(gzTextVar.zEntities[j].sState, 1, 4) == "Punk") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Punk.lua", j)
            return

        end
    end

    -- |[Lauren TF]|
    --If Lauren is in the same room.
    if(gzTextVar.zEntities[l].sLocation == gzTextVar.zEntities[i].sLocation) then
        
        --Blank Doll:
        if(gzTextVar.zEntities[l].sState == "Blank Doll") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            
            --Roll!
            local iRoll = LM_GetRandomNumber(1, 6)
            if(iRoll == 1) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Bride.lua", l)
            elseif(iRoll == 2) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Dancer.lua", l)
            elseif(iRoll == 3) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Geisha.lua", l)
            elseif(iRoll == 4) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Goth.lua", l)
            elseif(iRoll == 5) then
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Princess.lua", l)
            else
                LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Punk.lua", l)
            end
            return

        --Bride TF:
        elseif(string.sub(gzTextVar.zEntities[l].sState, 1, 5) == "Bride") then
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Bride.lua", l)
            return

        --Dancer TF:
        elseif(string.sub(gzTextVar.zEntities[l].sState, 1, 6) == "Dancer") then
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Dancer.lua", l)
            return

        --Geisha TF:
        elseif(string.sub(gzTextVar.zEntities[l].sState, 1, 6) == "Geisha") then
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Geisha.lua", l)
            return

        --Goth TF:
        elseif(string.sub(gzTextVar.zEntities[l].sState, 1, 4) == "Goth") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Goth.lua", l)
            return

        --Princess TF:
        elseif(string.sub(gzTextVar.zEntities[l].sState, 1, 8) == "Princess") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Princess.lua", l)
            return

        --Punk TF:
        elseif(string.sub(gzTextVar.zEntities[l].sState, 1, 4) == "Punk") then
            
            gzTextVar.gzPlayer.bDollMoveLock = true
            LM_ExecuteScript(fnResolvePath() .. "AIs/CutsceneDollOther/Doll Subscript Other Punk.lua", l)
            return

        end
    end
end
