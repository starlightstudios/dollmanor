-- |[ ================================== Claygirl Standard AI ================================== ]|
--Standard Claygirl AI. Claygirls wander at random. Since they only spawn in Phase 2, they ignore
-- the usual Main Hall rules.
--Claygirls spawn in disguise on certain items unless there are no items to spawn on. If this
-- happens, the claygirl will intercept the pickup attempt. When in disguise, claygirls are not
-- displayed on the map and will not move or do anything.

-- |[ =================================== Hostility Checker ==================================== ]|
--AI is checking whether it is hostile to the player.
fnClaygirl_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = true
end

-- |[ =================================== Post-Turn Spotter ==================================== ]|
--This logic runs after turn execution. The entity cannot move but can attempt to spot the player
-- after movement has occurred. This prevents a quick door close from spotting the player.
--There is a percentage chance this may do nothing, depending on the AI.
fnClaygirl_PostTurnSpotCheck = function(i)
    
    -- |[Disguised]|
    --Disguised claygirls don't run spotting logic.
    if(gzTextVar.zEntities[i].sInDisguise ~= "Null") then return end
    
    -- |[Non-Human]|
    --If the player is not a human, don't bother.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then return end

    -- |[Run Sight Checker]|
    --Common: Determine if we can see the player where we are.
    local bCanSeePlayer = fnStandardAI_SpotPlayer(i)

    -- |[Movement Handlers]|
    --Claygirl is moving to the location where it last saw the player.
    if(gzTextVar.zEntities[i].sLastSawPlayer ~= "Nowhere") then
        
        --Can we see the player? Update chasing to the new destination.
        if(bCanSeePlayer == true) then
            gzTextVar.zEntities[i].sLastSawPlayer = gzTextVar.gzPlayer.sLocation
        end

    --We are not chasing the player, but we are patrolling back to where we started.
    elseif(gzTextVar.zEntities[i].sFirstSawPlayer ~= "Nowhere") then
        
        --If we spotted the player, resume the chase.
        if(bCanSeePlayer == true) then
        
            --Store the location where we spotted the player.
            gzTextVar.zEntities[i].sLastSawPlayer = gzTextVar.gzPlayer.sLocation
            
        --Can't see the player. Resume moving back to the starting location.
        else
        end
            
        --We handled the movement.
        return
        
    --Claygirl is neither chasing nor returning to the start of a chase.
    else
        
        --If we can see the player, initialize the chase.
        if(bCanSeePlayer == true) then
            
            --Store both the player's location and where we started the chase. Claygirls path back to where they started.
            gzTextVar.zEntities[i].sFirstSawPlayer = gzTextVar.zEntities[i].sLocation
            gzTextVar.zEntities[i].sLastSawPlayer = gzTextVar.gzPlayer.sLocation
        
        --Can't see the player. Do nothing here.
        else
    
        end
    end
end

-- |[ =================================== Catching Function ==================================== ]|
--Called if the entity catches the player and initiates a battle.
fnClaygirl_CatchPlayer = function(i)
    
    --Player is non-human, ignore them.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then return end
    
    --Player is surrendering.
    if(gzTextVar.bIsPlayerSurrendering == true) then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneClaygirl/Surrender To Claygirl.lua", i)
        return
    end
    
    --Dialogue.
    if(gzTextVar.bCombatActivatedThisTick == false) then
        gzTextVar.bCombatActivatedThisTick = true
        TL_SetProperty("Append", "The shambling girl made of clay reaches towards you. There is no way out, defend yourself!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, "Standard", "Standard")
    
    --Combat is active. Run the handler but not in queue.
    elseif(gzTextVar.bCombatActivatedThisTick == true) then
        TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, "Standard", "Standard")
    end
    return
    
end

-- |[ ======================================= Normal AI ======================================== ]|
--Main AI sequence.
fnClaygirl_AI = function(i)

    -- |[ ========== Blocking Cases ========== ]|
    --[Disguised]
    --Disguised claygirls don't any logic.
    if(gzTextVar.zEntities[i].sInDisguise ~= "Null") then return end

    -- |[ ======== Detection Handling ======== ]|
    -- |[Same Room, Player Surrendering]|
    --In the same room, player is surrendering.
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.gzPlayer.sFormState == "Human" and gzTextVar.bIsPlayerSurrendering == true) then
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneClaygirl/Surrender To Claygirl.lua", i)
        return
    
    -- |[Same Room, Player Hostile]|
    --If in the same room, a battle will start.
    elseif(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation or gzTextVar.bIsTriggeringFights) then
        
        -- |[Player Crippled]|
        --If the player is crippled but has not been transformed yet:
        if(gzTextVar.gzPlayer.bIsCrippled == true and gzTextVar.gzPlayer.sFormState == "Human") then
            LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneClaygirl/Defeated By Claygirl.lua", i)
            return
        
        -- |[Not Crippled, Battle]|
        --Humans trigger a battle.
        elseif(gzTextVar.gzPlayer.sFormState == "Human") then
            fnClaygirl_CatchPlayer(i)
            return
        
        --Other cases, claygirl ignores you.
        else
        
        end
    end

    -- |[ ========= Spot the Player ========== ]|
    --Run the AI to check if we can see the player. If so, we don't bother checking doors.
    local bCanSeePlayer = false
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        bCanSeePlayer = fnStandardAI_SpotPlayer(i)
    end

    -- |[ =========== Door Handling ========== ]|
    local bClosedDoorThisTurn = fnStandardAI_HandleDoor(i)

    -- |[ ========= Movement Failure ========= ]|
    --At this point, claygirls have a 20% chance to simply not move for a turn. This makes them harder to
    -- predict and slightly easier to outrun. The chance decreases if they can see you.
    local iSkipRoll = LM_GetRandomNumber(1, 100)
    local iSkipChance = 20
    if(bCanSeePlayer) then iSkipChance = 15 end
    if(iSkipRoll <= iSkipChance) then
        return 
    end

    -- |[ ======== Spot-Player Logic ========= ]|
    --Attempt to spot the player from our current location. If the player is more than 3 tiles away,
    -- don't bother running the spotter algorithm to save time.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        local bHandledUpdate = fnStandardAI_SpotChasePlayer(i, bCanSeePlayer, fnClaygirl_CatchPlayer)
        if(bHandledUpdate) then return end
    end

    -- |[ =========== Patrol Logic =========== ]|
    -- |[Door Handler]|
    --If a door was closed this turn, stop here.
    if(bClosedDoorThisTurn == true) then return end
    
    -- |[Run Logic]|
    fnStandardAI_Patrol(i, fnClaygirl_CatchPlayer)
end