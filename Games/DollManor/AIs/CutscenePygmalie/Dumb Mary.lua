-- |[ ======================================= Dumb Mary ======================================== ]|
--If dumb Mary in phase 5 makes it all the way to the altar without reaching an enemy, this scene
-- plays out. Mary gets dolled.
        
--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sPygmPath = "Root/Images/DollManor/Characters/Pygmalie"
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Cutscene.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "There are lots and lots of doll girls here, carrying things and busying themselves for some sort of ritual or performance." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "In the middle is a lady with bright red hair and a smock. She is muttering to herself." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Miss?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Pygmalie looks at you. She grins wildly, snapping her fingers." .. gsDE)

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/DollCrowdShot", ciImageLayerDefault)
TL_SetProperty("Append", "Suddenly, all the dolls rush to you! They grab you and carry you to the altar." .. gsDE)
TL_SetProperty("Create Blocker")

fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Now, my little one, you've been worrying me sick. Running off like that, who knows what could have happened to you?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I'm sorry! I didn't want to worry you!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You've never seen this lady before, but upsetting her? What is wrong with you! You should have known better!" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Let me make it up to you!'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Hmmm? And how would you do that, dearie?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You think. The doll girls do not let you go. You can hardly move." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You continue to think. It's really hard." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Oh!' you say, as you remember." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Volunteering! I am! That's what I was doing!'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Pygmalie begins clapping and jumping up and down, giddy." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Good! Good! Be my best dearie! Don't you love me?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I do! I love you!' you shout. You have no idea what that word means." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'My dollies will take care of you, then. We'll have so much fun!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Yippee!' you shout. You want to jump and clap like she does, but the dolls haven't let you go yet." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Then, you feel an odd pricking sensation. In your back, your arms, yours legs - the dolls seem to have injected you with something." .. gsDE)

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "There are hundreds upon hundreds of injections. Fluid flows into you so rapidly that you barely have time to register the changes." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "In mere moments, it is done." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your body becomes plastic, your hair falls out, and your mind empties. Pygmalie yips with pure glee." .. gsDE)
TL_SetProperty("Create Blocker")

fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'You were very good for coming to me. Now, you'll help me find your little friends before anything happens to them.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'But not looking like that! Wah ha ha!'" .. gsDE)
fnDialogueFinale()

--Change Mary to a Blank Doll
gzTextVar.gzPlayer.iSkinColor = 2
gzTextVar.bDollSequenceVoluntary = true
gzTextVar.gzPlayer.sFormState = "Blank Doll"
