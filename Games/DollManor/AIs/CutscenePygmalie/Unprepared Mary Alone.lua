-- |[ ================================= Unprepared Mary Alone ================================== ]|
--If the player enters the ritual altar without Jessie and Lauren, they get dolled.

--Fast-access variables.
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex
local p = gzTextVar.iPygmalieIndex

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sPygmPath = "Root/Images/DollManor/Characters/Pygmalie"
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Cutscene.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "This place makes you extremely nervous. Hundreds of dolls line the sides of the room, but they seem to be inert." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Coming here was probably a bad idea..." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Miss?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Pygmalie looks at you. She grins wildly, snapping her fingers." .. gsDE)

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/DollCrowdShot", ciImageLayerDefault)
TL_SetProperty("Append", "Suddenly, all the dolls rush to you! They grab you and carry you to the altar. You struggle but there are far too many." .. gsDE)
TL_SetProperty("Create Blocker")

fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Now, my little one, you've been worrying me sick. Running off like that, who knows what could have happened to you?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'What if you got lost, or hurt, or found by someone who is so unscrupulous?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Lost or found. Ha! You're neither, now! I'm here!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You feel a prick in your skin. One of the dolls stuck a needle in you, somewhere." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Another, and another, and another... hundreds..." .. gsDE)

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "You are injected so many times that the white fluid in the needles rapidly replaces everything that was within you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your body becomes plastic, your hair falls out, and your mind empties. Pygmalie yips with pure glee." .. gsDE)
TL_SetProperty("Create Blocker")

fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'You were very good for coming to me. Now, you'll help me find your little friends before anything happens to them.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'But not looking like that! Wah ha ha!'" .. gsDE)
fnDialogueFinale()

--Change Mary to a Blank Doll
gzTextVar.gzPlayer.iSkinColor = 2
gzTextVar.bDollSequenceVoluntary = true
gzTextVar.gzPlayer.sFormState = "Blank Doll"
