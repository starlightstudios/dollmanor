-- |[ ==================================== Unprepared Mary ===================================== ]|
--If the player enters the ritual altar before phase 5, they get this bad end.

--Fast-access variables.
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex
local p = gzTextVar.iPygmalieIndex

--Setup.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = "Root/Images/DollManor/Characters/Pygmalie"
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Cutscene.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "This place makes you extremely nervous. Hundreds of dolls line the sides of the room, but they seem to be inert. You clear your throat." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Excuse me, miss. Are you all right?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Can't find it, not here? Where is it? Did you take it?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'This is no game - it's important. Why is it important? Always but not even. It's boiling. It's coming.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'But you're here now. Did you take it? No, it's safe. Safe below. Ha ha ha!'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Come to me, my dear.'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Keep your distance, Mary.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Pygmalie snaps her fingers. The dolls at the fringes of the room all burst to life in an instant and bear down on you!" .. gsDE)

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/DollCrowdShot", ciImageLayerDefault)
TL_SetProperty("Append", "There are dozens - hundreds! Too many to fight! They swarm over you, overwhelming you..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/DollCrowdShot", ciImageLayerDefault)
TL_SetProperty("Append", "You call out to Jessie and Lauren to run, but they are gone. You see them, held by the crowd of dolls. They are blank, featureless, plastic. And you feel pricks in your skin." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/DollCrowdShot", ciImageLayerDefault)
TL_SetProperty("Append", "You've been stuck with a dozen needles from the dolls. They grin at you. More needles stick into your skin. In only a few seconds..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your struggle ends. Your skin has become plastic, your hair has fallen out, and your mind has become totally emptied. You are helpless as the dolls carry you to your creator." .. gsDE)
TL_SetProperty("Create Blocker")

--Dialogue.
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'So, did you take it?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "You can feel your creator probing within your emptied mind. You can offer no resistance. She frowns." .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'No, no. It's where I left it, but not here. Fine. Fine!'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Your creator smiles broadly. She is going to make you pretty at last." .. gsDE)
fnDialogueFinale()

gzTextVar.bExternalEndingCall = true
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "500 Ending Handler.lua", 0)