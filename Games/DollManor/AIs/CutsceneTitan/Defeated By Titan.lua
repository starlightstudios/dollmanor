-- |[ =================================== Defeated By Titan ==================================== ]|
--Mary has been defeated in combat by a resin titan! Oh no!

--Argument Listing:
-- 0: i - Which entity is updating.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

-- |[ ======================================= Execution ======================================== ]|
--Change the player's state to blank doll.
gzTextVar.gzPlayer.sFormState = "Blank Doll"
gzTextVar.gzPlayer.bTitanTransformation = true

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The doll, standing over your wounded form, reaches down. It grips you, and you try to struggle, but its strength is incredible." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It grips you and lifts you off the ground, by the throat. You cough and wheeze, trying to breathe." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Your friends pale in shock. Jessie sprints to the creature and kicks it, but her foot finds no purchase. The doll girl doesn't even notice, and Jessie clutches her foot in pain." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Run, damn it' you wheeze. 'You don't have a chance'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    TL_SetProperty("Append", "Jessie, crying, takes Lauren by the collar. He pushes against her but ultimately assents. The two dash away." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Now you are alone with the doll..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Your friends pale in shock. Jessie sprints to the creature and kicks it, but her foot finds no purchase. The doll girl doesn't even notice, and Jessie clutches her foot in pain." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Run, damn it' you wheeze. 'You don't have a chance'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    TL_SetProperty("Append", "Jessie cries, but realizes you are right. She gives a promise to rescue you, later. It is empty, and you both know it, but it means something. Then, she flees." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Now you are alone with the doll..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Lauren pounds his fists against the doll, but only hurts his hands. Frustrated, he begins to cry." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end

    TL_SetProperty("Append", "'Find - Jessie - run!' you wheeze. Lauren fights back his tears and nods. He runs in a panic into the manor." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hope he'll be okay without you. Now, you are alone with the doll girl." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

TL_SetProperty("Append", "'Got you' the doll says in a monotone. She smiles. You try to breathe but she crushes your windpipe. You'll die in moments..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
TL_SetProperty("Append", "But then, something odd takes hold. The doll girl is made of some sort of resin, and it begins flowing into you through your skin." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It flows up your neck and into your mouth, flooding down and in. More flows down your body and covers it. It quickly absorbs through the skin and changes it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Your body becomes rigid wherever it goes. It feels almost good. There is no pain. You even smirk a little." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Then you black out." .. gsDE)
TL_SetProperty("Create Blocker")

--Exec script to move Mary to Pygmalie's position.
local sDestination = gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, sDestination)

--Exec script to change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6 + 9, 3, ciCodePlayer)

TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollE"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You awaken. You are in a room with an altar in it, surrounded by a hundred dolls. You look around quickly for an escape route, but the door is sealed." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "She is here. Pygmalie. Standing at the altar, she is addressing the doll who brought you here. You stand, preparing to confront her." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "She sees you stand and smiles. 'Come'." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "As you make your way to her, you catch sight of your arms. They are ball-jointed and made of the same resin-like material as the doll who defeated you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You stand on the other end of the altar. You might be able to knock Pygmalie out in one sudden strike if you can get behind her. You'll need to wait for an opening." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Kneel'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "You kneel before the altar. Your body did not move on its own, you wanted to kneel. As soon as she said it, you wanted to. A nameless fear takes hold of your heart." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Obey'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "Your creator is good and perfect. You will obey her every command from now until the end of time. All thoughts of resistance vanish in an instant, as do most of your other thoughts." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Now that the formalities are out of the way, shall we get you dressed?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "Your resinous sister giggles a little and goes back to her patrol. You grin and await your creator's future commands. You hope she makes you pretty." .. gsDE)
TL_SetProperty("Create Blocker")