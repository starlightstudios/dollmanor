-- |[ =================================== Surrender To Titan =================================== ]|
--Called when the player surrenders to a titan. Similar to the regular doll, but has a slightly
-- different text layout and also teleports the player to Pygmalie.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. fnResolveName() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--[Common Flags]
gzTextVar.gzPlayer.iClayInfection = -1

--[Phase 5]
--In phase 5, run a different cutscene.
if(gzTextVar.iGameStage == 5) then
    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneTitan/Surrender To Titan Phase 5.lua", i)
    return
end

--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state to blank doll.
gzTextVar.gzPlayer.sFormState = "Blank Doll"
gzTextVar.gzPlayer.bTitanTransformation = true
gzTextVar.gzPlayer.bIsCrippled = true

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The shining orange doll girl approaches you. Instead of preparing to fight, you lower your hands and make your surrender quite clear." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The doll approaches you without hesitation and places her palm on your chest. You feel a strange, warm sensation. It feels good." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'We don't have a chance. If we give in, it will feel so much better.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    TL_SetProperty("Append", "Horrified, Jessie grabs Lauren and flees into the manor. She promises to save you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You giggle, and your new doll sister does the same. You will become so strong. You will be able to protect them better as a doll than you ever did as a human." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'We don't have a chance. If we give in, it will feel so much better.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    TL_SetProperty("Append", "Horrified, Jessie flees into the manor. She shouts promises to rescue you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You giggle, and your new doll sister does the same. You will become so strong. You will be able to protect your friends better as a doll than you ever did as a human." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, stop! Stop! Fight her!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'We don't have a chance. If we give in, it will feel so much better.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "Lauren bursts into tears and tries to punch your doll sister. She barely notices." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Don't be silly, Lauren. It already feels so wonderful...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "Frustrated, Lauren flees into the manor, crying." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Don't worry about him' you whisper to your new sister. She laughs softly." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You will become so strong. You will be able to protect your friends better as a doll than you ever did as a human." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
TL_SetProperty("Append", "The resin flows over you, and then into you. It absorbs quickly into your skin, flowing into your bloodstream." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You're changing, inside and out. The feeling moves down your body into your legs, reaching every extremity." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Your body becomes harder, firmer. You realize you're not breathing anymore. You don't gasp or struggle, you just let it happen. It feels good." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Black Screen.lua", 0)
TL_SetProperty("Register Image", "You", "Null", ciImageLayerDefault)
TL_SetProperty("Append", "Then, you black out." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")
        
--Exec script to move Mary to Pygmalie's position.
local sDestination = gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, sDestination)

--Exec script to change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6 + 9, 3, ciCodePlayer)

TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollE"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "You awaken. You are in a room with an altar in it, surrounded by a hundred dolls. You look around. There are so many dolls here, smiling blankly and staring into nothingness." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "She is here. Pygmalie. Standing at the altar, she is addressing the doll who brought you here. You stand. She turns to see you, and smiles." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Come'." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "As you make your way to her, you take a brief look at yourself. You are a blank doll, made of firm but flexible resin. The thought of being solid and full delights you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You stand on the other end of the altar. You hold your hands crossed in front of you, and wait for your creator to improve you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Kneel'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "You kneel before the altar. Even if your body had not moved on its own, you would gladly have knelt before her." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Obey'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "Nothing in your changes. You were already your creator's perfect doll. Somehow, she knows this, and a warm feeling fills you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'You shall be my knight. You shall be my defender. You will love me as I love you, and you will protect me with all your being.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollE", ciImageLayerDefault)
TL_SetProperty("Append", "'I will, creator'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Your resinous sister giggles a little and goes back to her patrol. You grin and await your creator's future commands. You hope she makes you pretty." .. gsDE)
TL_SetProperty("Create Blocker")