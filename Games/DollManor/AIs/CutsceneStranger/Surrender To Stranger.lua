-- |[ ================================= Surrender To Stranger ================================== ]|
--Called from a few places, player encounters the stranger while the surrender flag is active.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

-- |[Common Flags]|
gzTextVar.gzPlayer.iClayInfection = -1

-- |[Phase 5]|
--In phase 5, run a different cutscene.
if(gzTextVar.iGameStage == 5) then
    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneStranger/Surrender To Stranger Phase 5.lua", i)
    return
end

-- |[All Other Cases]|
--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state to blank doll.
gzTextVar.gzPlayer.sFormState = "Blank Doll"

--Achievement
Steam_UnlockAchievement(gciAchievement_StainedGlass)

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The thumps of the heavy shoes no longer spark fear in you. The hulking masked creature stands before you. You raise your hands in surrender." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "A gloved hand grabs your collar and begins pulling you. You follow along, trying to make this easy." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing? Run!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'We can't win. She just gets back up. Let's make it easy.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie grabs Lauren and flees into the manor. She promises to save you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You chuckle. The stranger will find them sooner or later." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing? Run!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'We can't win. She just gets back up. Let's make it easy.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie flees into the manor. She shouts promises to rescue you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You chuckle. The stranger will find her sooner or later." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, stop! Stop! Fight her!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'We can't win. She just gets back up. Let's make it easy.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Lauren bursts into tears and flees into the manor, crying." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You chuckle. Lauren won't last long on his own." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

--Move Mary to the chapel.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, "Chapel Alter")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The brute leads you to the chapel on the northern edge of the estate. Despite holding you by the collar, she is not trying to hurt you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "She places you in front of the altar and releases her grip. She leaves to look for your friends. You're not quite sure what to do next." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It's raining outside, yet somehow the sun is shining through the windows at the side of the chapel. You walk up to one such window and look through it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "In fact, there is no sunlight. The window itself is glowing, and only the one window you're looking at. The light is quite beautiful." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You feel drowsy. You close your eyes." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryGlassTF0", ciImageLayerDefault)
TL_SetProperty("Append", "You awaken, having drifting off to sleep standing up. You were looking at your favourite stained-glass artwork in the chapel." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The plate beneath it says 'Mary of Coventry' and details her story. She stood up to bullies and protected her little brother, she liked to play tennis and was a good student." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "A simple, brave, capable girl. You always wished you could be just like her. Floating in the endless ether as a disembodied spirit, you never had a body of your own." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Then, a wonderous woman, your creator, made one for you. Blown from glass in the image of your favourite character, you inhabited it." .. gsDE)
TL_SetProperty("Create Blocker")

gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/MaryGlass"
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 3, 5, ciCodePlayer)
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
TL_SetProperty("Append", "Since then, with your flawless glass body, you would come to the chapel to admire Mary. Sometimes you would have silly daydreams of her adventures. In the most recent one, she surrendered to a hulking brute to protect her friends." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "That's enough admiring for today, though. You feel your creator's will in your head. Apparently, there are some intruders in the mansion, and everyone is trying to capture them." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You swore an oath to aid your creator however you could. So, you will find them and capture them." .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Unregister Image")

--Set stats.
gzTextVar.gzPlayer.sFormState = "Glass"
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryGlass"
gzTextVar.gzPlayer.bIsCrippled = false
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Unlock all doors.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
