-- |[ ============================= Surrender To Stranger Phase 5 ============================== ]|
--Called from a few places, player encounters the stranger while the surrender flag is active. This
-- handles when it is called during phase 5, and the player is taken to Pygmalie instead.
--This isn't supposed to be theoretically possible, as the stranger is supposed to be defeated at
-- this point, but it's here for safety.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

-- |[Sequence]|
--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state to blank doll.
gzTextVar.gzPlayer.sFormState = "Blank Doll"
gzTextVar.gzPlayer.bIsCrippled = true
gzTextVar.gzPlayer.iHP = 0
TL_SetProperty("Set Player Stats", 0, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The enormous glass woman approaches you. You grin dumbly at her. It's one of the few things you know how to do." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "'Excuse me, I was told I was supposed to volunteer? To your creator? Am I saying that right?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", gzTextVar.zEntities[i].sQueryPicture, ciImageLayerDefault)
TL_SetProperty("Append", "Her hand grabs your collar and pulls. You stumble along behind." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Black Screen.lua", 0)
TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")
        
--Exec script to move Mary to Pygmalie's position.
local sDestination = gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, sDestination)

TL_SetProperty("Append", "You arrive at the altar room. The stranger pushes you inside, shuts the door, and leaves. There are dozens of dolls around the room." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "In the center is a lady in a smock. She grins wickedly at you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Hello. Are you the creator? I'm here to volunteer!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Oh, how dreadful. There's not much of you left. Perhaps - no. Whee hee! That just means you'll be so much cuter!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'I wanna be cute!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Well then, take this needle. It goes in your vein. Any vein. Be a dear and inject yourself.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Okay! Thanks, miss!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You inject yourself with the needle she handed you. The white fluid rushes into your veins. It hurts a little, but the pain goes away after a second." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
TL_SetProperty("Append", "You look at your wrist. Strange. It didn't look like that before, but then again, maybe it did. You can't really remember." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The feeling spreads. You're enraptured by it. 'Is this supposed to happen, Miss Creator?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Of course, dearie. It's how all my dears were made! Wah ha ha!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
TL_SetProperty("Append", "You continue to stare as the odd sensation spreads." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF1", ciImageLayerDefault)
TL_SetProperty("Append", "Now it becomes euphoria. You giggle wildly as the plastic spreads all around you. Your joints have become balled like the doll girls around you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Some of them come to your side. They support you. A moment later, your legs lose feeling. You'd have fallen over if your sisters hadn't helped you. They're always looking out for you!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollUpperC", ciImageLayerDefault)
TL_SetProperty("Append", "The dolls help you to a sitting position, holding you gently until your body balances. Your hair falls out and you realize you can't feel anything or move anymore." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You aren't too worried about that. You're not worried about anything. You didn't think very much before, and now you don't think at all. Your thoughts become totally empty." .. gsDE)
TL_SetProperty("Create Blocker")

--Change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)

--Dialogue.
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your mind is as empty as your smooth, plastic body. You are a featureless blank slate, waiting to be molded into some other shape." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Able to see but not able to think, you gaze into infinity as an unfinished puppet." .. gsDE)
TL_SetProperty("Create Blocker")

