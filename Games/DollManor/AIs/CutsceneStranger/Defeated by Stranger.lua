-- |[ ================================== Defeated by Stranger ================================== ]|
--Mary is defeated by the stranger and is going to get transformed!

--Change the player's state to glass.
gzTextVar.gzPlayer.sFormState = "Glass"

--Achievement
Steam_UnlockAchievement(gciAchievement_StainedGlass)

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The hulking brute stands over you. You kneel on the ground, too hurt to stand." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Her gloved hand wraps around your neck and lifts you off the ground. This may well be the end." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Jessie and Lauren stand nearby, paralyzed with fright. Jessie looks like she wants to help, but she won't stand a chance. 'Run' you whisper." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie's face hardens. She clutches the crying Lauren and runs into the manor. Maybe you can distract the stranger long enough for them to escape..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Jessie wants to fight, but what chance would she stand? The stranger has incredible physical strength." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Jessie, run! Save Lauren for me...' you wheeze." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie's face hardens. She nods and runs into the manor, honoring your wish." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Lauren screams and shouts. He kicks the stranger's leg and regrets it. The stranger doesn't even notice." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Find - Jessie - run!' you wheeze. Lauren fights back his tears and nods. He runs in a panic into the manor." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hope you bought him enough time." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

TL_SetProperty("Append", "The stranger holds you in the air, choking you. You kick helplessly and feel yourself fading out. You can't breathe..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryGlassTF0", ciImageLayerDefault)
TL_SetProperty("Append", "... And that is where Mary of Coventry's journey ended, that time." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Many years ago, you were a disembodied spirit floating in the void of space. You had always wished to have your own body." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You had admired the world of the humans from a distance. One character in particular caught your attention. An English schoolgirl who was brave and smart." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You don't recall if you heard about her through a book or a play, but you loved Mary and wanted to be just like her." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "One day, you met a human. She could sense your presence and offered to create a body blown from glass for you to live in. You accepted instantly." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "She asked what you wanted to look like, and you answered Mary of Coventry. So, she made you a body." .. gsDE)
TL_SetProperty("Create Blocker")

gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/MaryGlass"
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 3, 5, ciCodePlayer)
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
TL_SetProperty("Append", "You were just in the chapel, admiring the stained-glass depiction of Mary as you often do. You look just like her, and sometimes have daydreams where you are her." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "That's enough admiring for today, though. You feel your creator's will in your head. Apparently, there are some intruders in the mansion, and everyone is trying to capture them." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You swore an oath to aid your creator however you could, out of gratitude for helping you. So, you will find them and capture them." .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Unregister Image")

--Set stats.
gzTextVar.gzPlayer.sFormState = "Glass"
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/MaryGlass"
gzTextVar.gzPlayer.bIsCrippled = false
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Unlock all doors.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
