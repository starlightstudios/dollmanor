-- |[ ================================= Surrender To Claygirl ================================== ]|
--Called from a few places, player encounters a claygirl while the surrender flag is active.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

-- |[Common Flags]|
gzTextVar.gzPlayer.iClayInfection = -1

-- |[Phase 5]|
--In phase 5, run a different cutscene.
if(gzTextVar.iGameStage == 5) then
    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneClaygirl/Surrender To Claygirl Phase 5.lua", i)
    return
end

-- |[All Other Cases]|
--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state.
gzTextVar.gzPlayer.sFormState = "Claygirl"
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
        
--Color. Player's color matches the claygirl who infected her.
local iIndicatorX = 6
local sClayColor = "Blue"
if(gzTextVar.zEntities[i].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlRed") then
    sClayColor = "Red"
    iIndicatorX = 7
elseif(gzTextVar.zEntities[i].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlYellow") then
    sClayColor = "Yellow"
    iIndicatorX = 8
end

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The girl made of billowing clay approaches you menacingly. You lower your hands and make your surrender clear." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "She seems uncertain for a moment. You give a half-bow, showing your vulnerability. She reaches a glob of clay forth and places it on you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You take the clay and spread it. The clay girl backs up, apparently satisfied." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing? Stop it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Why? It feels so good. It's cool and squishy... Try it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie grabs Lauren and flees into the manor. She promises to save you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snicker. The clay girl, despite having no face, likewise seems to jiggle with laughter. How silly of those humans." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing? Stop it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Why? It feels so good. It's cool and squishy... Try it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie flees into the manor. She shouts promises to rescue you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snicker. The clay girl, despite having no face, likewise seems to jiggle with laughter. How silly of those humans." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, stop! Stop! Fight her!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Why? It feels so good. It's cool and squishy... Try it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Lauren bursts into tears and grabs at you. He recoils when you try to spread the clay on him. Frustrated, he flees into the manor, crying." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snicker. The clay girl, despite having no face, likewise seems to jiggle with laughter. How silly of those humans." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

        
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
TL_SetProperty("Append", "You spread the clay along your arms. It feels great. You motion to the claygirl, though she stays at arm's reach for now." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The clay does not require your assistance, it spreads of its own accord. Still, it will be faster if you aid it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Everywhere the clay goes, your flesh disappears. It is replaced by clay. You cover your clothes, legs, and torso with it. Then you spread it to your face." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Everything goes black as you splash it over your eyes. Then, your sight returns, changed." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
TL_SetProperty("Append", "You're covered, head to toe. Your whole body is covered in clay, no, it is clay. You're clay." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You feel at your face. You smoothe out the features, turning the eyes, nose, and mouth into a flat featureless surface." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The other clay girl touches you. She begins to shape you. She is molding you, changing your hair, your body, your legs. You welcome her touch." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You allow her to shape you until you are the same as her. She slithers backwards, the changes completed. One task remains." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You reach your clay fingers into your face and begin squeezing. There is too much in there. You reshape it. Your clay sister will help if you need it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It takes a few moments of reshaping until a feeling of satisfaction washes over you. It is done. You are perfect now." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You should probably go find Jessie and Lauren now. You'll need to cover them in clay and make them like you. Then they can be as happy as you are." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Find them, change them. Then, you can be together forever." .. gsDE)
TL_SetProperty("Create Blocker")
    
--Change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)

--Fullheal the player.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
gzTextVar.gzPlayer.bIsCrippled = false
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Change Mary's properties.
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite