-- |[ ============================= Surrender To Claygirl On Item ============================== ]|
--If the player is surrendering, and picks up an item with a claygirl on it, this plays out. Normally
-- a battle executes, but a surrendering player is transformed instantly.

--Argument Listing:
-- 0: iRoomIndex - The room this is taking place in.
-- 1: iItemIndex - The item in the room that the claygirl was on.
-- 2: iClayIndex - The entity index of the claygirl.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iRoomIndex = tonumber(LM_GetScriptArgument(0))
local iItemIndex = tonumber(LM_GetScriptArgument(1))
local iClayIndex = tonumber(LM_GetScriptArgument(2))

--[Common Flags]
gzTextVar.gzPlayer.iClayInfection = -1

--[Phase 5]
--In phase 5, run a different cutscene.
if(gzTextVar.iGameStage == 5) then
    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneClaygirl/Surrender To Claygirl On Item Phase 5.lua", i)
    return
end

--[All Other Cases]
--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state.
gzTextVar.gzPlayer.sFormState = "Claygirl"
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

--Unset flags.
gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].bIsClaygirlTrapped = false
gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].sClaygirlName = "Null"
gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].saClaygirlList = {}
gzTextVar.zEntities[iClayIndex].sInDisguise = "Null"
            
--Determine clay color.
local iIndicatorX = 6
local sClayColor = "Blue"
if(gzTextVar.zEntities[iClayIndex].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlRed") then
    sClayColor = "Red"
    iIndicatorX = 7
elseif(gzTextVar.zEntities[iClayIndex].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlYellow") then
    sClayColor = "Yellow"
    iIndicatorX = 8
end
            
--Dialogue. Change Mary's image.
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The second you touch the " .. gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].sDisplayName .. ", it becomes soft. It's covered in some sort of clay!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
TL_SetProperty("Append", "The clay coats your hand, feeling cold and slimy. It's almost alive - it *is* alive!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The clay oozes off the " .. gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].sDisplayName .. " and expands, forming a puddle." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
TL_SetProperty("Append", "The puddle gives way to a head, torso, and then legs. A girl made of clay, but lacking a face, menaces you!" .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Unregister Image")

TL_SetProperty("Append", "You rush forward and - embrace the clay girl! You surrender instantly! She is somewhat confused!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "'I want it, it feels so good...' you say. The clay girl obliges your hug and returns it." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing? Stop it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Why? It feels so good. It's cool and squishy... Try it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie grabs Lauren and flees into the manor. She promises to save you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snicker. The clay girl, despite having no face, likewise seems to jiggle with laughter. How silly of those humans." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing? Stop it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Why? It feels so good. It's cool and squishy... Try it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie flees into the manor. She shouts promises to rescue you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snicker. The clay girl, despite having no face, likewise seems to jiggle with laughter. How silly of those humans." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, stop! Stop! Fight her!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'Why? It feels so good. It's cool and squishy... Try it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Lauren bursts into tears and grabs at you. He recoils when you try to spread the clay on him. Frustrated, he flees into the manor, crying." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You snicker. The clay girl, despite having no face, likewise seems to jiggle with laughter. How silly of those humans." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

        
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
TL_SetProperty("Append", "You spread the clay along your arms. It feels great. You keep hugging and press yourself further into the clay." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Everywhere the clay goes, your flesh disappears. It is replaced by clay. You cover your clothes, legs, and torso with it. You bury your head in the clay girl's bosom." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Everything goes black. You pull your head out, totally covered in clay. After a few moments, your sight clears. The clay has become your eyes." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
TL_SetProperty("Append", "You're realize that you are completely covered in clay. It won't be long now." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The changes take only a few more seconds, and then they are complete. There is no more flesh, only clay." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You feel at your face. You smoothe out the features, turning the eyes, nose, and mouth into a flat featureless surface." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The other clay girl touches you. She begins to shape you. She is molding you, changing your hair, your body, your legs. You welcome her touch." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You allow her to shape you until you are the same as her. She slithers backwards, the changes completed. One task remains." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You reach your clay fingers into your face and begin squeezing. There is too much in there. You reshape it. Your clay sister will help if you need it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It takes a few moments of reshaping until a feeling of satisfaction washes over you. It is done. You are perfect now." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You should probably go find Jessie and Lauren now. You'll need to cover them in clay and make them like you. Then they can be as happy as you are." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Find them, change them. Then, you can be together forever." .. gsDE)
TL_SetProperty("Create Blocker")
    
--Change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)

--Fullheal the player.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
gzTextVar.gzPlayer.bIsCrippled = false
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Change Mary's properties.
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite