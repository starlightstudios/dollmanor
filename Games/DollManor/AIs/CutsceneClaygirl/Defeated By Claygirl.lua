-- |[ ================================== Defeated By Claygirl ================================== ]|
--Defeated by a claygirl! Oh no!

--Argument Listing:
-- 0: i - Which entity is updating.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

-- |[ ======================================= Execution ======================================== ]|
--Change the player's state to claygirl.
gzTextVar.gzPlayer.sFormState = "Claygirl"
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

--Color. Player's color matches the claygirl who infected her.
local iIndicatorX = 6
local sClayColor = "Blue"
if(gzTextVar.zEntities[i].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlRed") then
    sClayColor = "Red"
    iIndicatorX = 7
elseif(gzTextVar.zEntities[i].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlYellow") then
    sClayColor = "Yellow"
    iIndicatorX = 8
end

--Transformation sequence.
TL_SetProperty("Register Image", "You", gzTextVar.zEntities[i].sQueryPicture, ciImageLayerDefault)
TL_SetProperty("Append", "The girl made of clay slithers towards you, her 'feet' forming and flowing as she does. You try to push yourself away, but your injuries are too great." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
TL_SetProperty("Append", "The first thing to be covered is your arms and legs. You feel damp clay coat them. It feels cool, soft. Almost pleasing." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Jessie runs towards you, preparing to shove the clay girl away. 'Stop' you hiss. 'It'll get you, too. Run!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I can't just leave you!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
    TL_SetProperty("Append", "'Lauren is your responsibility. Go! Now!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie clearly wants to stay, but after a second she hefts a crying Lauren and flees. Now it's just you and the girl of clay." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Jessie runs towards you, preparing to shove the clay girl away. 'Stop' you hiss. 'It'll get you, too. Run!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'I can't just leave you!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
    TL_SetProperty("Append", "'Take care of Lauren for me, Jessie. Tell him I love him.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Damn it...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie clearly wants to stay, but after a second she realizes the situation and flees. Now it's just you and the girl of clay." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Lauren runs up to the clay girl, preparing to pound her with his fists. You tell him to stop." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary! Mary, get up!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
    TL_SetProperty("Append", "'Sorry, squirt, but this is where we part ways. Go find Jessie. Take care of her for me.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary! No!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryClay" .. sClayColor .. "TF0", ciImageLayerDefault)
    TL_SetProperty("Append", "'Don't let this be in vain, kiddo. Now go!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Lauren's tears overwhelm him, and he cries as he runs away. The clay girl is unmoved by the display." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

TL_SetProperty("Append", "The clay quickly spreads up your arm. The clay girl pours more onto you, covering you. It won't be long now." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You feel the cool, soft sensation of the clay cover your body. Once it reaches your face, everything goes black. You almost panic as you can't breathe, but then, you realize you don't need to." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor, ciImageLayerDefault)
TL_SetProperty("Append", "You're covered, head to toe. The clay seems to be growing in mass, until you realize that it isn't. It's you. Your body has become the clay." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You stand up. You feel at your face with your clay hands, but nothing is there. Somehow, your vision returns. You have no eyes, yet can still see." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The other clay girl touches you. She begins to shape you. She is molding you, changing your hair, your body, your legs. You welcome her touch." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "She doesn't seem to realize yet that you're still Mary. Maybe if you wait until she loses interest, you can escape and find some sort of cure..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Then, you feel something. It feels like hunger, but it isn't in your stomach. It's in your head. It feels wrong." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You reach your clay fingers into your face and prod at the hunger. You squeeze and distort it. The clay girl who changed you stands nearby, ready to help if you need it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "After some work, the hunger fades. You're relieved. It felt so strange. The clay girl turns and begins to wander away." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You should probably go find Jessie and Lauren now. You'll need to find some way to let them know you're okay." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Then, the hunger returns. Once again you reach inside your head. You find it again, squeeze it, distort it. It goes away again." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "What were you thinking? Yes. You need to find Jessie and Lauren. You need to cover them in clay. You need to make them like you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "That is the right thought. You had the right thought, and you're glad you did. You will find your friends and change them..." .. gsDE)
TL_SetProperty("Create Blocker")

--Fullheal the player.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
gzTextVar.gzPlayer.bIsCrippled = false
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

--Change Mary's properties.
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/ClayGirl" .. sClayColor
TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)