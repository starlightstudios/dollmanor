-- |[ ========================= Surrender To Claygirl On Item Phase 5 ========================== ]|
--If the player is surrendering, and picks up an item with a claygirl on it, this plays out. This
-- is the version that plays out in phase 5. Mary gets taken to become a doll instead.

--Argument Listing:
-- 0: iRoomIndex - The room this is taking place in.
-- 1: iItemIndex - The item in the room that the claygirl was on.
-- 2: iClayIndex - The entity index of the claygirl.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iRoomIndex = tonumber(LM_GetScriptArgument(0))
local iItemIndex = tonumber(LM_GetScriptArgument(1))
local iClayIndex = tonumber(LM_GetScriptArgument(2))

--[Common Flags]
gzTextVar.gzPlayer.iClayInfection = -1

--[All Other Cases]
--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state.
gzTextVar.gzPlayer.iSkinColor = 2
gzTextVar.gzPlayer.sFormState = "Blank Doll"

--Unset flags.
gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].bIsClaygirlTrapped = false
gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].sClaygirlName = "Null"
gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].saClaygirlList = {}
gzTextVar.zEntities[iClayIndex].sInDisguise = "Null"
            
--Determine clay color.
local iIndicatorX = 6
local sClayColor = "Blue"
if(gzTextVar.zEntities[iClayIndex].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlRed") then
    sClayColor = "Red"
    iIndicatorX = 7
elseif(gzTextVar.zEntities[iClayIndex].sQueryPicture == "Root/Images/DollManor/Characters/ClayGirlYellow") then
    sClayColor = "Yellow"
    iIndicatorX = 8
end

--Dialogue. Change Mary's image.
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/MaryTF0Clay" .. sClayColor
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The second you touch the " .. gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].sDisplayName .. ", it becomes soft. It's covered in some sort of clay!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
TL_SetProperty("Append", "The clay coats your hand, feeling cold and slimy. It's almost alive - it *is* alive!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The clay oozes off the " .. gzTextVar.zRoomList[iRoomIndex].zObjects[iItemIndex].sDisplayName .. " and expands, forming a puddle." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
TL_SetProperty("Append", "The puddle gives way to a head, torso, and then legs. A girl made of clay, but lacking a face, menaces you!" .. gsDE)
TL_SetProperty("Create Blocker")
TL_SetProperty("Unregister Image")

TL_SetProperty("Append", "'Wait, wait! Take the clay off! I'm supposed to volunteer to the creator? Right?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "'I hope I said that right. I'm trying not to screw this up.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "A long silence passes between you and the clay girl. She reaches forward and absorbs the clay off your hand." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Thanks' you say, relieved. The clay girl says nothing, having no mouth." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You stare at the clay girl. She stares at you. It's very awkward." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "'So, like, what now?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", gzTextVar.zEntities[iClayIndex].sQueryPicture, ciImageLayerDefault)
TL_SetProperty("Append", "She motions with her finger for you to follow." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Follow you? Okay! Where are we going?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", gzTextVar.zEntities[iClayIndex].sQueryPicture, ciImageLayerDefault)
TL_SetProperty("Append", "The clay girl points at her face. She cannot answer." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Sorry!' you state, dumbly." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Black Screen.lua", 0)
TL_SetProperty("Append", "You follow the clay girl with a stupid grin on your face. That was easy! You're doing great so far!" .. gsDE)
TL_SetProperty("Create Blocker")

--Exec script to move Mary to Pygmalie's position.
local sDestination = gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, sDestination)

TL_SetProperty("Append", "You arrive at the altar room. The clay girl opens the door for you but remains outside." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The room has dozens, perhaps hundreds, of human-sized dolls around it. Some are busying themselves moving furniture or giggling to each other." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "In the center is a lady in a smock. She turns to you with a delighted look on her face." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Hello. Are you the creator? The clay girl didn't say much... I'm here to volunteer!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Oh, how dreadful. There's not much of you left. Perhaps - no. Whee hee! That just means you'll be so much cuter!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'I wanna be cute!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Well then, take this needle. It goes in your vein. Any vein. Be a dear and inject yourself.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'Okay! Thanks, miss!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You inject yourself with the needle she handed you. The white fluid rushes into your veins. It hurts a little, but the pain goes away after a second." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryTF0", ciImageLayerDefault)
TL_SetProperty("Append", "You look at your wrist. Strange. It didn't look like that before, but then again, maybe it did. You can't really remember." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The feeling spreads. You're enraptured by it. 'Is this supposed to happen, Miss Creator?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
TL_SetProperty("Append", "'Of course, dearie. It's how all my dears were made! Wah ha ha!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryTF0", ciImageLayerDefault)
TL_SetProperty("Append", "You continue to stare as the odd sensation spreads." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryTF1", ciImageLayerDefault)
TL_SetProperty("Append", "Now it becomes euphoria. You giggle wildly as the plastic spreads all around you. Your joints have become balled like the doll girls around you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Some of them come to your side. They support you. A moment later, your legs lose feeling. You'd have fallen over if your sisters hadn't helped you. They're always looking out for you!" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollUpperC", ciImageLayerDefault)
TL_SetProperty("Append", "The dolls help you to a sitting position, holding you gently until your body balances. Your hair falls out and you realize you can't feel anything or move anymore." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You aren't too worried about that. You're not worried about anything. You didn't think very much before, and now you don't think at all. Your thoughts become totally empty." .. gsDE)
TL_SetProperty("Create Blocker")

--Change Mary's indicator.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)

--Dialogue.
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your mind is as empty as your smooth, plastic body. You are a featureless blank slate, waiting to be molded into some other shape." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Able to see but not able to think, you gaze into infinity as an unfinished puppet." .. gsDE)
TL_SetProperty("Create Blocker")

--Set stats.
gzTextVar.gzPlayer.bIsCrippled = true
