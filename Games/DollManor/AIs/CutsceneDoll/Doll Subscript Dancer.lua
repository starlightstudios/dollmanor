-- |[ ================================= Doll Subscript: Dancer ================================= ]|
--Contains the call strings used for the Dancer transformation.
local sUseSkin = "Root/Images/DollManor/Transformation/DancerTFBase" .. string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--In titan transformations, the end letter is always "E"
if(gzTextVar.gzPlayer.bTitanTransformation) then 
    sEndLetter = "E" 
    sUseSkin = "Root/Images/DollManor/Transformation/DancerTFBaseE"
end

--Blank Doll: Begin transformation:
if(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF0"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF0"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Common handler.
    LM_ExecuteScript(fnResolvePath() .. "Doll Subscript Common.lua")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        
        --In phase 5 this changes slightly.
        if(gzTextVar.iGameStage == 5) then
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Pygmalie snaps her fingers. The dolls in the room heft you to a chair sitting opposite a full-length mirror." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You can see yourself in its reflection. You think nothing. The dolls return to their positions at the edge of the room." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator appears, bearing a bag of cosmetics. She sorts through the bag, searching for lipstick, nail polish, and all the things that will make you cute." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF0", ciImageLayerOverlay)
            TL_SetProperty("Append", "Some base, lipstick, and blush are applied. Your creator hums to herself as she goes." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "'My dear, I have a feeling that you were made to dance. Doesn't that seem like a wonderous role for a dolly?'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF0", ciImageLayerOverlay)
            TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
            TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/DancerTF0", ciImageLayerOverlay)
            TL_SetProperty("Append", "You think nothing. Your creator continues to dote upon you." .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Normal case.
        else
            --Normal:
            if(gzTextVar.bDollSequenceVoluntary == false) then
                TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
                TL_SetProperty("Append", "Upon seeing you, Pygmalie puts her hand to her heart and rushes over." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Oh my goodness, dear! Going about undressed like this, so unbecoming! I'll have you right as rain in just a moment!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Pygmalie picks up your limp form and places you in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Hmm, let's see what we can do with you...'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'A graceful dancer, yes, yes! Oh what a wonderous idea!'" .. gsDE)
                TL_SetProperty("Create Blocker")
            
            --Case: Player is volunteering in some way, and Pygmalie is less excited.
            else
                TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
                TL_SetProperty("Append", "Pygmalie picks your light, empty body up. She spins it a few times, watching as your limbs limply follow her motions." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "She places your body in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them. She keeps saying one word over and over. 'Dancing, dancing... dancing...'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Dancing, yes, you're graceful and acrobatic. Yes. I know why.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Is the performance tonight, and you're not even ready? Silly dolly!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "She gives you a wicked grin, and begins her work." .. gsDE)
                TL_SetProperty("Create Blocker")
        
            end
        end
    
    --Sequence plays out slightly different if in titan mode.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'There are such dangers out there, aren't there, little one? But you will be so strong. You will protect me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'But the first priority of a doll, no matter what, is to be cute. Don't you agree?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Pygmalie gestures to a nearby chair. A full-length mirror sits in front of it. You sit in the chair and wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'My honor guard must be graceful and agile...'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'A beautiful dancer. Of course. Yes. Yes...'" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 1:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF0") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF1"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF0", ciImageLayerOverlay)
    if(gzTextVar.iGameStage ~= 5) then
        TL_SetProperty("Append", "Pygmalie applies some eyeshadow and blush to your face. She smirks to herself and admires her work." .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
    TL_SetProperty("Append", "'I think an - oh, yes, yes I know exactly what you need. One moment, my darling.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She vanishes into the racks behind you. Moments later, she emerges bearing a tiara and a dirty-blonde wig." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smiles and affixes the wig to your head. You feel the weight as it connects to your scalp. It digs into you and becomes your hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Best to tie this up in a bun or you might trip on it. Don't you just love the color?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The blonde color has always been your favourite. You love the accent of the tiara and the butterfly pin. Your creator knows what you've always wanted and joyously gives it to you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/DancerTF1", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 2:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF1") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF2"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'The dress I have in mind - well, best to let you see it for yourself, no?' Pygmalie says, once again vanishing into the clothesracks." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You wait for some time. Your eagerness to be completed has faded. As your creator left you, all thought vanished. You are blank and empty again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Ha ha, oh, marvelous! Marvelous!' you hear from the clothesracks. You are unmoved." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF2") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF3"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'I've really outdone myself this time! Delicate, graceful, feminine, perfect!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator has selected butterflies and hearts as your dancing motif. This fits your personality perfectly, and always has. You feel your empty heart swell." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Soon you will dance for your creator, but you remain still until she tells you otherwise." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/DancerTF2", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF3") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF4"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Can't have you stumbling on the stage, can we? You should have big, beautiful green eyes...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator produces some paints and paintbrushes from behind you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 4:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF4") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF5"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF3"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "Your creator begins to apply the paints to your eyes. She is gentle, trying to avoid tickling you excessively. She is so concerned for you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The pressure on your eyes is altogether unfamiliar. Soon the paint covers them entirely and you are blinded." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Everyone, and I do mean everyone, thinks green eyes are cute. You'll be the finest dancer in the ballet.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your vision clears as your creator steps back. You now have green eyes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/DancerTF3", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 5:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF5") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Dancer TF6"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/DancerTF4"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'There is one thing left to make the perfect, my elegant dancer. Let's fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand on your head, and you feel your thoughts begin to stir." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You see an image of yourself dancing on a stage while a rapt audience watches in total awe." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You twirl and leap and spin, reciting the same motions you have practiced thousands of times. The crowd can hardly contain its excitement." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your dance finishes, and you take a bow. The audience leaps to its feet and cheers for you. Roses are thrown and land in a heap at your feet." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Slightly different during phase 5.
    if(gzTextVar.iGameStage == 5) then
        TL_SetProperty("Append", "You look at a young girl in the front row, with raven hair. She is next to a young boy who is totally unfamiliar to you. They're so happy to see you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "In fact, you somehow know exactly where they are! There's a secret room in the basement! They're waiting there for you, to find them, to play with them!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "None of the other dollies know about that room... The girl and boy must be so sad... So lonely..." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "But you're a good dolly and good dollies make sure little boys and girls have friends! You absolutely must go play with them!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You want to tell your creator, but you also want to keep it as a surprise. She'll be so delighted that you were such a good dolly!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Normal.
    else
        TL_SetProperty("Append", "You look at a young girl in the front row, with raven hair. She is next to a young boy who is totally unfamiliar to you. They're so happy to see you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "They clap, and clap, and as they clap their hands soon become jointed and smooth. It follows up their arms and spreads to their bodies. They are soon dolls, just like you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "They have always been dolls. They always will be dolls." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Append", "You must be studious, graceful, and elegant. You must dance. And you must make more sisters." .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "You feel your joints springing to life. A small smile etches itself onto your face..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/DancerTF4", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 6:
elseif(gzTextVar.gzPlayer.sFormState == "Dancer TF6") then

    --Indicator.
    local iIndicatorX = 2
    if(gzTextVar.gzPlayer.bTitanTransformation == true) then iIndicatorX = iIndicatorX + 11 end

    --Flags
    gzTextVar.gzPlayer.sFormState = "Doll Dancer"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Dancer " .. sEndLetter
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Dancer " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "You obey instantly, standing up and presenting yourself for your creator to look over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey at your creator, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Titan sequence.
    else
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now. You are my personal warrior, a knight at my beck and call. Do you know what that means?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey. 'No harm shall come to you, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She claps with delight and giggles. You bow before her, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You can think of some uncouth humans your creator must be protected from. It is your task to find them." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Default Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")
    
    --Fullheal the player.
    gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
    gzTextVar.gzPlayer.bIsCrippled = false
    TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end