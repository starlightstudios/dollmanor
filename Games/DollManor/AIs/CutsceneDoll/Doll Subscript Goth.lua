-- |[ ================================== Doll Subscript: Goth ================================== ]|
--Contains the call strings used for the Goth transformation.
local sUseSkin = "Root/Images/DollManor/Transformation/GothTFBase" .. string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--In titan transformations, the end letter is always "E"
if(gzTextVar.gzPlayer.bTitanTransformation) then 
    sEndLetter = "E" 
    sUseSkin = "Root/Images/DollManor/Transformation/GothTFBaseE"
end

--Blank Doll: Begin transformation:
if(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF0"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF0"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Common handler.
    LM_ExecuteScript(fnResolvePath() .. "Doll Subscript Common.lua")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        --Normal:
        if(gzTextVar.bDollSequenceVoluntary == false) then
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Upon seeing you, Pygmalie sighs and walks towards you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Going about without your face on, my dear? Oh how unbecoming.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Pygmalie picks up your limp form and places you in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Hmm, let's see what we can do with you...'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'I'm in a melancholy mood today, my dear. Let's try something darker...'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
        --Case: Player is volunteering in some way, and Pygmalie is less excited.
        else
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Pygmalie picks your body. She holds it at arm's length, suspended in the air. She sighs. 'Useless', she says." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She carries you to a full length mirror and a chair opposite. She places you in the chair. 'I can fix that.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "A bag of cosmetics seems to appear in her hands. She sorts through them." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'You need a lesson on manners if you're to be alone. It is when there is no one else that we find out who we truly are.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She lets out a high, shrill laugh. Them, she frowns and begins to work on you." .. gsDE)
            TL_SetProperty("Create Blocker")
    
        end
    
    --Sequence plays out slightly different if in titan mode.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'There are such dangers out there, aren't there, little one? But you will be so strong. You will protect me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'But the first priority of a doll, no matter what, is to be cute. Don't you agree?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Pygmalie gestures to a nearby chair. A full-length mirror sits in front of it. You sit in the chair and wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'My honor guard must be gentle, but firm. Feminine, strong, beautiful...'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'A gothic marvel. Yes, that will be perfect.'" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 1:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF0") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF1"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie applies a layer of base and eyeshadow. She adds eyeliner, and then more and more. Your eyes gain a very thick black outline." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Dark, yes, very dark. Lonely, isolated, but dignified. Elegant. Hmm...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator frowns and turns, walking to the racks of clothes behind you. You hear her mutter complaints..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "She reappears, bearing a heavy violet hairpiece. She gives an indeterminate look, then slots it over your scalp and affixes ribbons to it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, purple is such a sad color. Yes, that will do nicely.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The hairpiece grows heavy as it affixes to your head, giving you fluffy purple hair. Had you any emotions, you would sigh at the futility of it all." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Instead, you remain still. A dark shroud looms around your empty mind." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GothTF1", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 2:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF1") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF2"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Oh, oh, oh. Yes, ones afflicted by depression often wear black, no? It is only suiting.' Pygmalie says. Once again she vanishes among the clothesracks." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "As your creator leaves you, so does the melancholy around you. You become blank again. You think no thoughts as she rummages behind you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'*sigh* Yes, this will have to do.' you hear her mutter." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF2") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF3"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'The boots are nice and long, complementing the futility of our long lives. I've given a moth accent, because we are drawn to light but repulsed by flame.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The pall again looms around you. Your creator is right, nothing matters save the lonliness you feel. Perhaps when you are complete, you can comfort her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "That will have to wait, for she has not ordered you to stand. You are incomplete. You wait." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GothTF2", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF3") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF4"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Red, yes, red eyes. I'll give you red eyes. Stumbling around blind in the dark - a fate too cruel even for us miserable creatures.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "From behind you, Pygmalie produces paints and paintbrushes. She begins mixing and frowns yet again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 4:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF4") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF5"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF3"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "The paint goes on your eyes, covering you in darkness. You feel the brushes swirling as your creator does her grim work." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You feel the pressure on your eyes and are unmoved. You are at home in the blackness, and only come to the light as willed by your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'There. Done. Such a shame, it is, but you'll have to see.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "With those words, the paints become your true eyes. You can see again, for all the good it will do." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GothTF3", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 5:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF5") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Goth TF6"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GothTF4"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'I suppose having you sit there, limp and uncaring, would be too kind a mercy. Let's fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand on your head, and you feel your thoughts begin to stir." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You see an enormous manor at the top of a hill, with a single light shining through a window. This is where you reside." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A storm rages, the wind and rain cutting you from the world. You do not care. You recline in an ancient chair and stare at the wall." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hear a knock at the door. You ease yourself out of the chair to answer it, assuming it was a branch thrown by the wind." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "To your surprise, you find a poor girl swathed in dingy robes. She is shivering. She is lost. But you have found her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You bring her in, make a fire, give her new clothes. They are like yours, they were yours, but now they are hers." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles and thanks you. You make her something to eat, and she is soon tired and must take her rest." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You bring her to your guest room, with its dusty furniture. You apologize at the state of disrepair, but she happily accepts." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The night passes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The next morning, she is dressed as you do, speaks as you do, thinks as you do. She is your doll sister. Forever." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You must be macabre in all ways, elegant in all things, hospitable at all times, and make many more sisters for your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "You feel your joints springing to life. A small smile etches itself onto your face..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GothTF4", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 6:
elseif(gzTextVar.gzPlayer.sFormState == "Goth TF6") then

    --Indicator.
    local iIndicatorX = 0
    if(gzTextVar.gzPlayer.bTitanTransformation == true) then iIndicatorX = iIndicatorX + 11 end

    --Flags
    gzTextVar.gzPlayer.sFormState = "Doll Goth"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Goth " .. sEndLetter
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    
    --Sequence
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Goth " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "You obey instantly, standing up and presenting yourself for your creator to look over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey at your creator, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Titan sequence.
    else
        
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now. You are my personal warrior, a knight at my beck and call. Do you know what that means?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey. 'No harm shall come to you, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She claps with delight and giggles. You bow before her, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You can think of some uncouth humans your creator must be protected from. It is your task to find them." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Default Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

    --Fullheal the player.
    gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
    gzTextVar.gzPlayer.bIsCrippled = false
    TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end