-- |[ ================================= Doll Subscript: Geisha ================================= ]|
--Contains the call strings used for the Geisha transformation.
local sUseSkin = "Root/Images/DollManor/Transformation/GeishaTFBase" .. string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--In titan transformations, the end letter is always "E"
if(gzTextVar.gzPlayer.bTitanTransformation) then 
    sEndLetter = "E" 
    sUseSkin = "Root/Images/DollManor/Transformation/GeishaTFBaseE"
end

--Blank Doll: Begin transformation:
if(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF0"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF0"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Common handler.
    LM_ExecuteScript(fnResolvePath() .. "Doll Subscript Common.lua")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        --Normal:
        if(gzTextVar.bDollSequenceVoluntary == false) then
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Upon seeing you, Pygmalie puts her hand to her heart and rushes over." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Oh my goodness, dear! Going about undressed like this, so unbecoming! I'll have you right as rain in just a moment!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Pygmalie picks up your limp form and places you in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Hmm, let's see what we can do with you...'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Oh, an elegant geisha! Yes, I think that would suit you nicely.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
        --Case: Player is volunteering in some way, and Pygmalie is less excited.
        else
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Pygmalie picks your light, empty body up. She holds it in a hug. A single tear falls upon your shoulder." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She wipes the tear away and carries you to a chair, opposite a full length mirror. She places you in it, allowing you to see your reflection." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them. She sniffles a few times." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Not the time, no, but I feel everything. None of it. Touch this.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Maybe as a geisha you will have some way forward? Yes, that will do. That will do well.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "And as though nothing happened at all, she grins wickedly and begins her work." .. gsDE)
            TL_SetProperty("Create Blocker")
    
        end
    
    --Sequence plays out slightly different if in titan mode.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'There are such dangers out there, aren't there, little one? But you will be so strong. You will protect me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'But the first priority of a doll, no matter what, is to be cute. Don't you agree?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Pygmalie gestures to a nearby chair. A full-length mirror sits in front of it. You sit in the chair and wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'My honor guard must be delicate and mysterious...'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'A shy, elegant geisha! Of course, it's so obvious.'" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 1:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF0") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF1"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "'A bit of makeup, some dark lipstick... Oh, I know!' your creator says with a snap of her finger." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "After some rummaging through the racks of dresses and supplies behind you, Pygmalie appears bearing a wig of short, black hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Look what I found for you! It's as soft as silk and shines when the light strikes it just right. This was just too perfect to pass up!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Black has always been your favourite color, you love the choices your creator is making. You're so glad your creator remembered the little details. She's so kind and perfect." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "As soon as she attaches the wig to your head, you feel a slight weight as it attaches to your scalp. You now have short, black hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I'll give your cheeks some blush, and a bit of nail polish - you're so pretty already, and we've just begun!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GeishaTF1", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 2:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF1") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF2"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Now, let's see', Pygmalie says to herself. She vanishes behind you again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'We can't have you with your pretty, soft skin exposed, can we? After all, the perfect geisha always hides her frame beneath fine silks.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Everyone will see you, and wonder what you have beneath. And these pretty flowers on the dress will make you cute as can be!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You agree with your creator. Being cute, modest, and mysterious has always been your calling in life. She is making you ever more perfect." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF2") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF3"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'It fits you perfectly, as do the shoes I found for you. You'll have no trouble walking on them, because you're my graceful little geisha, right?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is certainly right in every way. The dress fits you wonderfully, and you already know how to walk in such shoes. As soon as she mentioned it, you had always known." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Though she asked you a question, your mind is still unmotivated and empty. You hope your creator finishes you soon, so you can answer her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GeishaTF2", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF3") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF4"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Oh, I'm sure you'll need to see in order to make sure you are seen, won't you? No problem!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She disappears into the clothes racks behind you. You do not feel anticipation. You feel empty and blank when she moves away from you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 4:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF4") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF5"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF3"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "She appears again, this time with a paintbrush and selection of paints. You remain completely motionless as she selects her colors." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Delicately, softly, she begins applying her paints to your eyes. You feel an odd pressure as your vision fills with color and you can no longer see." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "'I think commanding blue eyes match your color scheme. You look marvellous, my sweet.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "The instant she finishes speaking, your vision clears. The paint has become your eyes, and she steps away." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GeishaTF3", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 5:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF5") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Geisha TF6"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/GeishaTF4"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'There is one thing left to make the perfect, graceful geisha. Let's fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand on your head, and you feel your thoughts begin to stir." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You see an image of yourself in an elegant formal party. Others are sipping fine teas and composing poetry as you linger in the background, hiding yourself behind your fan." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You smile, as your guests ask you to dance. You begin to move and twirl, becoming the embodiment of pure grace and finery." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Soon the image changes. You are performing in a play with other geisha. The room is silent, as you go from tableau to tableau with your fellow actresses." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You finish the play, and the audience begins to applaud. You look at them. They are smiling, overjoyed to see your performance." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "As you focus on each one, their features change. Their human skin soon becomes pale plastic, their joints become balls like yours. Moments later, the whole audience is your sister dolls." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You must perform. You must dance. You must be graceful. And you must make more sisters." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GeishaTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "You feel your joints springing to life. A small smile etches itself onto your face..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/GeishaTF4", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Geisha Transformation Stage 6:
elseif(gzTextVar.gzPlayer.sFormState == "Geisha TF6") then

    --Indicator.
    local iIndicatorX = 3
    if(gzTextVar.gzPlayer.bTitanTransformation == true) then iIndicatorX = iIndicatorX + 11 end

    --Flags
    gzTextVar.gzPlayer.sFormState = "Doll Geisha"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Geisha " .. sEndLetter
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    
    --Sequence
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Geisha " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "You obey instantly, standing up and presenting yourself for your creator to look over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey at your creator, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Titan sequence.
    else
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now. You are my personal warrior, a knight at my beck and call. Do you know what that means?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey. 'No harm shall come to you, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She claps with delight and giggles. You bow before her, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You can think of some uncouth humans your creator must be protected from. It is your task to find them." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Default Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

    --Fullheal the player.
    gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
    gzTextVar.gzPlayer.bIsCrippled = false
    TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end
    