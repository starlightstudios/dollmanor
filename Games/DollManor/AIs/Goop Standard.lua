-- |[ ==================================== Goop Standard AI ==================================== ]|
--Rubber Goop Blobs patrol in fixed paths and do not detect the player. If they *end* their turn
-- on the same tile as the player, they start a TF sequence. 3 hits is a bad end.

-- |[ =================================== Hostility Checker ==================================== ]|
--AI is checking whether it is hostile to the player.
fnGoop_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = true
end

-- |[ =================================== Post-Turn Spotter ==================================== ]|
--This logic runs after turn execution. The entity, if it ends its turn on the same location as the
-- player, will activate a TF sequence.
fnGoop_PostTurnSpotCheck = function(i)
    
    --Player is not human, don't do anything.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then return end
    
    --io.write("Monster: " .. gzTextVar.zEntities[i].sLocation .. " x " .. gzTextVar.zEntities[i].sPrevLocation  .. "\n")
    --io.write("Mary:    " .. gzTextVar.gzPlayer.sLocation     .. " x " .. gzTextVar.gzPlayer.sPrevLocationStore .. "\n")
    
    --If we are in the same location as the player:
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        --io.write(" Catch by move.\n")
    
    --If the player tried to swap tiles with us, catch them anyway.
    elseif(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sPrevLocationStore and gzTextVar.zEntities[i].sPrevLocation == gzTextVar.gzPlayer.sLocation) then
        --io.write(" Intercept.\n")
        
    --Otherwise, fail.
    else
        return
    end
    
    --Same location as the player at end of turn. Debug.
    --io.write("  Caught Mary!\n")
    
    --If we caught the player, fire this logic.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/Human/902 Trigger Rubber Trap.lua")
    
    --Monster despawns when it catches Mary.
    gzTextVar.zEntities[i].zCombatTable.iHealth = 0
    return
end


-- |[ ======================================= Normal AI ======================================== ]|
--Main AI sequence.
fnGoop_AI = function(i)
    
    -- |[New Movement]|
    --If the current movement index is -1, move to the next node on the patrol list.
    if(gzTextVar.zEntities[i].iMoveIndex == -1) then
        
        -- |[Staying Still]|
        --Can be flagged to stay still if you want.
        if(gzTextVar.zEntities[i].iStayStillTurns > 0) then
            gzTextVar.zEntities[i].iStayStillTurns = gzTextVar.zEntities[i].iStayStillTurns - 1
            return
        end
        
        --Setup.
        local sPatrolTarget = "Nowhere"
        
        -- |[Fixed Patrol Routes]|
        --Entity patrols between a fixed set of locations.
        if(gzTextVar.zEntities[i].bMovesRandomly == false) then
        
            --Get the current node.
            local p = gzTextVar.zEntities[i].iPatrolIndex
            gzTextVar.zEntities[i].iPatrolIndex = gzTextVar.zEntities[i].iPatrolIndex + 1
            sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[p]
            if(sPatrolTarget == nil) then
                sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[1]
                gzTextVar.zEntities[i].iPatrolIndex = 2
            end
            if(sPatrolTarget == nil) then
                io.write("Warning: Entity " .. i .. " has no patrol paths.\n")
                return
            end
        
        -- |[Wandering Enemies]|
        --Enemy picks a location, paths there, and then picks a new location.
        else
        
            --No valid moves, somehow.
            if(gzTextVar.iRandomPathsTotal < 1) then return end
        
            --Roll a random slot.
            local iRandomRoll = LM_GetRandomNumber(1, gzTextVar.iRandomPathsTotal)
            sPatrolTarget = gzTextVar.saValidRandomPaths[iRandomRoll]
            gzTextVar.zEntities[i].saPatrolList = {sPatrolTarget}
        
        end
        
        --Make the first move in the direction in question.
        gzTextVar.zEntities[i].iMoveIndex = 1
        
        --Run the subroutine. This will move the entity if everything went well.
        gzTextVar.zEntities[i].sPrevLocation = gzTextVar.zEntities[i].sLocation
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Standard Move To Destination.lua", i, sPatrolTarget)
            
        --Goops do NOT emit sound.
        
        --If the destination was reached, reset the move handler.
        if(gzTextVar.zEntities[i].sLocation == sPatrolTarget) then
            gzTextVar.zEntities[i].iMoveIndex = -1
        end

    -- |[Continuing]|
    --Continuing a patrol path.
    else
        
        --Check if the patrol is invalid.
        if(gzTextVar.zEntities[i].saPatrolList == nil) then
            gzTextVar.zEntities[i].iMoveIndex = -1
            gzTextVar.zEntities[i].iPatrolIndex = 1
            gzTextVar.zEntities[i].saPatrolList = {"None"}
            return
        end
        
        --Variables.
        local sPatrolTarget = "Target"
        local sRoomName = gzTextVar.zEntities[i].sLocation
        local iRoomIndex = fnGetRoomIndex(sRoomName)
        
        --When moving along fixed patrols:
        if(gzTextVar.zEntities[i].bMovesRandomly == false) then
        
            --Get the destination again.
            local p = gzTextVar.zEntities[i].iPatrolIndex - 1
            sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[p]
            
            --Increment the move index.
            gzTextVar.zEntities[i].iMoveIndex = gzTextVar.zEntities[i].iMoveIndex + 1
            
            --Error checks.
            if(iRoomIndex == -1 or iTargetIndex == -1) then return end
            
            --Already at destination, so reset.
            if(sRoomName == sPatrolTarget) then
                gzTextVar.zEntities[i].iMoveIndex = -1
                return
            end
        
        --When moving randomly, move towards the patrol target.
        else
            sPatrolTarget = gzTextVar.zEntities[i].saPatrolList[1]
        end
        
        --Run the subroutine. This will move the entity if everything went well.
        gzTextVar.zEntities[i].sPrevLocation = gzTextVar.zEntities[i].sLocation
        LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Standard Move To Destination.lua", i, sPatrolTarget)
            
        --Rubber goops do NOT emit sound!
        
        --If the destination was reached, reset the move handler.
        if(gzTextVar.zEntities[i].sLocation == sPatrolTarget) then
            gzTextVar.zEntities[i].iMoveIndex = -1
        end

    end
end
