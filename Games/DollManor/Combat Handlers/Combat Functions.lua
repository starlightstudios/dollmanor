-- |[ ==================================== Combat Functions ==================================== ]|
--Functions used by the combat engine to compute stat bonuses.

-- |[fnComputePlayerStatBonuses]|
--Computes all the player's stat bonuses, including elemental bonuses, and places them in the state
-- machine variables.
fnComputePlayerStatBonuses = function()
    
    -- |[Reset]|
    --Return the state machine flags back to their defaults.
    gzTextVar.iAtkBonus = 0
    gzTextVar.iDefBonus = 0
    gzTextVar.iStartShieldBonus = 0
    
    --Elemental damage bonuses.
    gzTextVar.iFireBonus = 0
    gzTextVar.iWaterBonus = 0
    gzTextVar.iWindBonus = 0
    gzTextVar.iEarthBonus = 0
    gzTextVar.iLifeBonus = 0
    gzTextVar.iDeathBonus = 0
    
    -- |[Execute]|
    --Determine player properties by checking their equipment.
    gzTextVar.bIsEquipmentCheck = true
    if(gzTextVar.gzPlayer.zWeapon ~= nil) then
        LM_ExecuteScript(gzTextVar.gzPlayer.zWeapon.sHandlerScript, "Null", -1, -1, -1)
    end
    if(gzTextVar.gzPlayer.zArmor ~= nil) then
        LM_ExecuteScript(gzTextVar.gzPlayer.zArmor.sHandlerScript, "Null", -1, -1, -1)
    end
    if(gzTextVar.gzPlayer.zGloves ~= nil) then
        LM_ExecuteScript(gzTextVar.gzPlayer.zGloves.sHandlerScript, "Null", -1, -1, -1)
    end
    
    -- |[Clean]|
    --Clear flags.
    gzTextVar.bIsEquipmentCheck = false
    
end
