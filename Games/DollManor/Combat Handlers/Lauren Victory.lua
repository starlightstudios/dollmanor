-- |[ ===================================== Victory Lauren ===================================== ]|
--Defeat case where the dolled player attacks human Lauren. Lauren does not get removed, only crippled.

--Text.
if(gzTextVar.gzPlayer.sFormState == "Statue") then
    gzTextVar.bForceMoveStatue = true
    TL_SetProperty("Append", "You have harmed the intruder. He is SUBDUED. You will now CARRY HIM TO THE LIGHT." .. gsDE)
else
    TL_SetProperty("Append", "After much roughhousing, Lauren falls to the ground with a thud. He hurt his legs and cannot run. He looks up at you apprehensively." .. gsDE)
end

--Jessie is downed.
gzTextVar.zEntities[gzTextVar.iLaurenIndex].bIsCrippled = true

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")

--Clean.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()