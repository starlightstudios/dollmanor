-- |[ ================================ Standard Defeat Handler ================================= ]|
--Called when the player is defeated after combat under normal circumstances, that is, by a doll.
TL_SetProperty("Append", "After the final blow is struck, you crumple on the ground, defeated. You are far too hurt to move.")
gzTextVar.gzPlayer.bIsCrippled = true

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")
