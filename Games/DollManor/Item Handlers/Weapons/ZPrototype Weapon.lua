-- |[ =================================== Prototype - Weapon =================================== ]|
--For weapons which have no special properties beyond their stats, you can call this script.

--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot  - The item slot of the item in question.
-- 2: iRoomSlot  - The room slot of the item in question.
-- 3: iEquipSlot - The equipment slot of the item in question.
-- 4: iAtkBonus - Attack power bonus if item is equipped.
-- 5: iDefBonus - Defense power bonus if item is equipped.
-- 6: iShieldBonus - Shields at combat start when this is equipped.
-- 7: Description - What string is shown when the item is examined.

--Arg check.
local iRequiredArgs = 7
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "equip", "equip " .. sEntityName)
    
    --Equipment.
    elseif(sEntityHeader == "Equipment") then
        TL_SetProperty("Register Popup Command", "look",    "look "    .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",    "drop "    .. sEntityName)
        TL_SetProperty("Register Popup Command", "unequip", "unequip " .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "equip", "equip " .. sEntityName)
    end
    return
end

-- |[ ========================================== Priority ========================================= ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iWeapons
    return
end

-- |[ ======================================= Trap Builder ======================================== ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "equip"}
    return
end

-- |[ ====================================== Equipment Query ====================================== ]|
--Checking equipment properties. Writes the values associated with this equipment to state machine vars.
if(gzTextVar.bIsEquipmentCheck) then
    gzTextVar.iAtkBonus = gzTextVar.iAtkBonus + tonumber(LM_GetScriptArgument(4))
    gzTextVar.iDefBonus = gzTextVar.iDefBonus + tonumber(LM_GetScriptArgument(5))
    gzTextVar.iStartShieldBonus = gzTextVar.iStartShieldBonus + tonumber(LM_GetScriptArgument(6))
    return
end

-- |[ ======================================= Verification ======================================== ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot  = tonumber(LM_GetScriptArgument(1))
local iRoomSlot  = tonumber(LM_GetScriptArgument(2))
local iEquipSlot = tonumber(LM_GetScriptArgument(3))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1 and iItemSlot ~= -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName
    
--Equipment.
elseif(iRoomSlot == -1 and iItemSlot == -1) then

    if(iEquipSlot == ciEquipSlotWeapon) then
        sLocalName = gzTextVar.gzPlayer.zWeapon.sDisplayName
    elseif(iEquipSlot == ciEquipSlotArmor) then
        sLocalName = gzTextVar.gzPlayer.zArmor.sDisplayName
    elseif(iEquipSlot == ciEquipSlotGloves) then
        sLocalName = gzTextVar.gzPlayer.zGloves.sDisplayName
    else
        return
    end

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[ ====================================== Basic Commands ======================================= ]|
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    local sString = LM_GetScriptArgument(7)
    TL_SetProperty("Append", sString)
    gbHandledInput = true

--Examining it as equipment.
elseif((sInputString == "look eqp " .. sLocalName or sInputString == "examine eqp " .. sLocalName) and iEquipSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    local sString = LM_GetScriptArgument(7)
    TL_SetProperty("Append", sString)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    local sString = LM_GetScriptArgument(7)
    TL_SetProperty("Append", sString)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    --Ignore this if the item in question is already in the inventory.
    if(iRoomSlot == -1) then
        
        --If there are potions in the inventory but not on the ground, print a warning message.
        local iIndexInRoom = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        if(iIndexInRoom == -1) then
            TL_SetProperty("Append", "You already have that in your inventory." .. gsDE)
            gbHandledInput = true
        end
        return
    end
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to pick that up." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Otherwise, take the item.
    TL_SetProperty("Append", "You pick up the [" .. sLocalName .. "]." .. gsDE)
    
    --Increment
    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
    local p = gzTextVar.gzPlayer.iItemsTotal
    
    --Create the item.
    gzTextVar.gzPlayer.zaItems[p] = {}
    gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]
    
    --Remove the item from the original room listing.
    fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    if(gzTextVar.bTakeAll == false) then
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    gbHandledInput = true

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Display.
    TL_SetProperty("Append", "You drop the [" .. sLocalName .. "]." .. gsDE)
    
    --Determine where the player currently is.
    local iRoomIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
    if(iRoomIndex == -1) then return end

    --Add it to the room's inventory.
    gzTextVar.zRoomList[iRoomIndex].iObjectsTotal = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal + 1
    local p = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal
    
    --Unmark visibility before dropping the weapon.
    fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    
    --Torch handling.
    local bOldItemTorch = false
    if(gzTextVar.gzPlayer.zWeapon ~= nil and gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
        bOldItemTorch = true
    end

    --If this is a piece of equipment:
    if(iEquipSlot == ciEquipSlotWeapon) then
        gzTextVar.zRoomList[iRoomIndex].zObjects[p] = gzTextVar.gzPlayer.zWeapon
        gzTextVar.gzPlayer.zWeapon = nil
        
    elseif(iEquipSlot == ciEquipSlotArmor) then
        gzTextVar.zRoomList[iRoomIndex].zObjects[p] = gzTextVar.gzPlayer.zArmor
        gzTextVar.gzPlayer.zArmor = nil
        
    elseif(iEquipSlot == ciEquipSlotGloves) then
        gzTextVar.zRoomList[iRoomIndex].zObjects[p] = gzTextVar.gzPlayer.zGloves
        gzTextVar.gzPlayer.zGloves = nil

    --Inventory item.
    else
        gzTextVar.zRoomList[iRoomIndex].zObjects[p] = gzTextVar.gzPlayer.zaItems[iItemSlot]
        fnRemoveItemFromInventory(iItemSlot)
    end
    
    --Flags.
    gbHandledInput = true
    fnBuildLocalityInfo()
    
    --Because weapons can affect truesight, it is also necessary to rebuild visibility here.
    fnMarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Reresolve Fades")
                
    --Call the item script to get its indicator information.
    gzTextVar.iGlobalIndicatorX = -1
    gzTextVar.iGlobalIndicatorY = -1
    gzTextVar.bIsBuildingIndicatorInfo = true
    LM_ExecuteScript(gzTextVar.zRoomList[iRoomIndex].zObjects[p].sHandlerScript)
    gzTextVar.bIsBuildingIndicatorInfo = false
                
    --If the indicators are not -1's, spawn an indicator in the given room.
    if(gzTextVar.iGlobalIndicatorX ~= -1 and gzTextVar.iGlobalIndicatorY ~= -1) then
        
        --Create a unique name.
        local sIndicatorName = fnGenerateIndicatorName()
        gzTextVar.zRoomList[iRoomIndex].zObjects[p].sIndicatorName = sIndicatorName
        
        --Upload.
        local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
        TL_SetProperty("Register Entity Indicator", fX, fY, fZ, sIndicatorName, gzTextVar.iGlobalIndicatorX, gzTextVar.iGlobalIndicatorY, ciCodeItem)
    end
    
    -- |[Torch Handler]|
    --Unequipping the torch.
    if(bOldItemTorch == true) then
        if(gzTextVar.bIsFogActive) then
            TL_SetProperty("Modify Overlay", "Fog0", "Root/Images/DollManor/Overlays/Fog0")
        end
        if(gzTextVar.bIsDarkActive) then
            TL_SetProperty("Modify Overlay", "Black0", "Root/Images/DollManor/Overlays/Black0")
        end
    end
    
--Equipping it.
elseif(sInputString == "equip " .. sLocalName) then

    --If the item is in the equipment slot, do nothing.
    if(iEquipSlot == ciEquipSlotWeapon) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are already wielding that." .. gsDE)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Display.
    TL_SetProperty("Append", "You wield the [" .. sLocalName .. "]." .. gsDE)
    
    --Unmark visibility before switching weapons.
    fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    
    --Torch handling.
    local bOldItemTorch = false
    local bNewItemTorch = false
    
    --If we already have a weapon, put it in the inventory.
    if(gzTextVar.gzPlayer.zWeapon ~= nil) then
        if(gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
            bOldItemTorch = true
        end
        
        gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
        local p = gzTextVar.gzPlayer.iItemsTotal
        gzTextVar.gzPlayer.zaItems[p] = {}
        gzTextVar.gzPlayer.zaItems[p] = gzTextVar.gzPlayer.zWeapon
        gzTextVar.gzPlayer.zWeapon = nil
    end
    
    --Take from the room and place in the weapon slot.
    if(iRoomSlot ~= -1 and iItemSlot ~= -1) then
        gzTextVar.gzPlayer.zWeapon = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]
        fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    
    --Take from the inventory and place in the weapon slot.
    elseif(iRoomSlot == -1 and iItemSlot ~= -1) then
        gzTextVar.gzPlayer.zWeapon = gzTextVar.gzPlayer.zaItems[iItemSlot]
        fnRemoveItemFromInventory(iItemSlot)
    
    end 

    --Flag.
    gbHandledInput = true
    fnBuildLocalityInfo()
    
    --Torch checker.
    if(gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
        bNewItemTorch = true
    end
    
    --Because weapons can affect truesight, it is also necessary to rebuild visibility here.
    fnMarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Reresolve Fades")
    
    -- |[Torch Handler]|
    --Somehow, both were torches, so do nothing.
    if(bOldItemTorch == true and bNewItemTorch == true) then
    
    --Old was a torch, new is not, so unequip:
    elseif(bOldItemTorch == true and bNewItemTorch == false) then
        if(gzTextVar.bIsFogActive) then
            TL_SetProperty("Modify Overlay", "Fog0", "Root/Images/DollManor/Overlays/Fog0")
        end
        if(gzTextVar.bIsDarkActive) then
            TL_SetProperty("Modify Overlay", "Black0", "Root/Images/DollManor/Overlays/Black0")
        end
    
    --Old was not a torch, new is, so equip:
    elseif(bOldItemTorch == false and bNewItemTorch == true) then
        if(gzTextVar.bIsFogActive) then
            TL_SetProperty("Modify Overlay", "Fog0", "Root/Images/DollManor/Overlays/Fog0Big")
        end
        if(gzTextVar.bIsDarkActive) then
            TL_SetProperty("Modify Overlay", "Black0", "Root/Images/DollManor/Overlays/Black0Big")
        end
    
    --Neither was a torch, so do nothing.
    else
    
    end
    
    --If the player is wearing the Feather Coat and wielding the Twinblade, unlock an achievement.
    if(gzTextVar.gzPlayer.zArmor ~= nil and gzTextVar.gzPlayer.zWeapon ~= nil) then
        if(gzTextVar.gzPlayer.zArmor.sDisplayName == "feather coat" and gzTextVar.gzPlayer.zWeapon.sDisplayName == "twinblade") then
            Steam_UnlockAchievement(gciAchievement_AsTheCrowFlies)
        end
    end

--Unequipping it.
elseif(sInputString == "unequip " .. sLocalName) then

    --If the item is not in the equipment slot, do nothing.
    if(iEquipSlot ~= ciEquipSlotWeapon) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are not wielding that." .. gsDE)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Display.
    TL_SetProperty("Append", "You put the [" .. sLocalName .. "] in your inventory." .. gsDE)
    
    --Unmark visiblity before unequipping the weapon.
    fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    
    --Torch handling.
    local bOldItemTorch = false
    if(gzTextVar.gzPlayer.zWeapon ~= nil and gzTextVar.gzPlayer.zWeapon.sUniqueName == "torch") then
        bOldItemTorch = true
    end
    
    --Place it in the inventory.
    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
    local p = gzTextVar.gzPlayer.iItemsTotal
    
    --Create the item.
    gzTextVar.gzPlayer.zaItems[p] = {}
    gzTextVar.gzPlayer.zaItems[p] = gzTextVar.gzPlayer.zWeapon
    
    --Remove the item from the original room listing.
    gbHandledInput = true
    gzTextVar.gzPlayer.zWeapon = nil
    fnBuildLocalityInfo()
    
    --Because weapons can affect truesight, it is also necessary to rebuild visibility here.
    fnMarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Reresolve Fades")
    
    -- |[Torch Handler]|
    --Unequipping the torch.
    if(bOldItemTorch == true) then
        if(gzTextVar.bIsFogActive) then
            TL_SetProperty("Modify Overlay", "Fog0", "Root/Images/DollManor/Overlays/Fog0")
        end
        if(gzTextVar.bIsDarkActive) then
            TL_SetProperty("Modify Overlay", "Black0", "Root/Images/DollManor/Overlays/Black0")
        end
    end
end