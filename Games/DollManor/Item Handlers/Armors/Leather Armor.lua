-- |[ ===================================== Leather Armor ====================================== ]|
--+2 defense, +1 starting shield.

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 1
    return
end

-- |[ ======================================== Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot  - The item slot of the item in question.
-- 2: iRoomSlot  - The room slot of the item in question.
-- 3: iEquipSlot - The equipment slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[Arg Resolve]
local sInputString = LM_GetScriptArgument(0)
local iItemSlot  = tonumber(LM_GetScriptArgument(1))
local iRoomSlot  = tonumber(LM_GetScriptArgument(2))
local iEquipSlot = tonumber(LM_GetScriptArgument(3))

--[Synonym Handler]
--Figure out if we can switch a synonym to the proper name.
if(TL_GetProperty("Is Building Commands") == false) then
    local sTrueName = "leather armor"
    local saSynonymList = {"armor", "armour"}

    --First, check if the proper name already exists completely in the input string. If it does, there is no need to
    -- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
    local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
    if(iFullnameStart == nil) then
        
        --Base name was not found, so look through the synonym list.
        local iSynStart, iSynEnd
        local sUseSynonym = "Null"
        for i = 1, #saSynonymList, 1 do
            iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
            if(iSynStart ~= nil) then
                sUseSynonym = saSynonymList[i]
                break
            end
        end

        --If a synonym was found, we can swap it out for the true name.
        if(sUseSynonym ~= "Null") then
            local sPreString = string.sub(sInputString, 1, iSynStart-1)
            local sPostString = string.sub(sInputString, iSynEnd+1)
            sInputString = sPreString .. sTrueName .. sPostString
        end
    end
end

--[Use Armor Prototype]
local iAtkBonus = 0
local iDefBonus = 2
local iShields = 1
local sDescription = "A simple leather tunic. Increases defense by 2 and you start combat with +1 shield. You can [equip] or [unequip] it." .. gsDE
LM_ExecuteScript(fnResolvePath() .. "ZPrototype Armor.lua", sInputString, iItemSlot, iRoomSlot, iEquipSlot, iAtkBonus, iDefBonus, iShields, sDescription)
