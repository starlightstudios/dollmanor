-- |[ ==================================== Blunted Scissors ==================================== ]|
--If sharpened, can be used to bypass the string trap

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 2
    gzTextVar.iGlobalIndicatorY = 1
    return
end

-- |[ ======================================== Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",    "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",    "drop "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "sharpen", "sharpen "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

-- |[ ======================================== Priority ======================================== ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iConsumables
    return
end

-- |[ ====================================== Trap Builder ====================================== ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take"}
    return
end

-- |[ ====================================== Verification ====================================== ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[Synonym Handler]|
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"blunted", "scissors"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

-- |[ ===================================== Basic Commands ===================================== ]|
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    TL_SetProperty("Append", "A pair of scissors, blunted from cutting an extremely strong string. You might be able to sharpen them at a workshop." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    TL_SetProperty("Append", "A pair of scissors, blunted from cutting an extremely strong string. You might be able to sharpen them at a workshop." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    --Ignore this if the item in question is already in the inventory.
    if(iRoomSlot == -1) then
        
        --If there are items in the inventory but not on the ground, print a warning message.
        local iIndexInRoom = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        local iIndexInRoomOfAlt = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, "scissors")
        if(iIndexInRoom == -1 and iIndexInRoomOfAlt == -1) then
            TL_SetProperty("Append", "You already have that in your inventory." .. gsDE)
            gbHandledInput = true
        end
        return
    end
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to pick that up." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Otherwise, take the item.
    TL_SetProperty("Append", "You pick up the [" .. sLocalName .. "]." .. gsDE)
    
    --Increment
    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
    local p = gzTextVar.gzPlayer.iItemsTotal
    
    --Create the item.
    gzTextVar.gzPlayer.zaItems[p] = {}
    gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]
    
    --Remove the item from the original room listing.
    fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    if(gzTextVar.bTakeAll == false) then
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    gbHandledInput = true

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)
    LM_ExecuteScript(gzTextVar.sTurnEndScript)

-- |[ ========================================= Sharpen ======================================== ]|
--Special command, replaces the blunted scissors with a pair or normal scissors.
elseif(sInputString == "sharpen " .. sLocalName and iRoomSlot == -1) then

    --Flag handled.
    gbHandledInput = true
    
    --Check if we're in the correct room. There are several workshops.
    if(string.sub(gzTextVar.gzPlayer.sLocation, 1, 12) == "Old Workshop" or string.sub(gzTextVar.gzPlayer.sLocation, 1, 15) == "Chapel Workshop") then
    
        --Sharpen!
        TL_SetProperty("Append", "You use the grinding wheel to sharpen up your blunted scissors. They should be able to cut again." .. gsDE)
        
        --Replace with normal scissors.
        gzTextVar.gzPlayer.zaItems[iItemSlot].sUniqueName = "scissors"
        gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName = "scissors"
        gzTextVar.gzPlayer.zaItems[iItemSlot].sHandlerScript = gzTextVar.sRootPath .. "Item Handlers/ToolsConsumables/Scissors.lua"

        --Rebuild locality info.
        fnBuildLocalityInfo()

    --Not in the correct room.
    else
        TL_SetProperty("Append", "You have nothing to sharpen the scissors with. You should look for a workshop with a grinding wheel." .. gsDE)
    end
end