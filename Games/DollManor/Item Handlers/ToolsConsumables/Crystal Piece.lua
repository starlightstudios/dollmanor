-- |[ ===================================== Crystal Piece ====================================== ]|
--A fragment of a broken crystal. There are four total.

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 4
    gzTextVar.iGlobalIndicatorY = 2
    return
end

-- |[ ======================================= Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")

    --When human and in possession of four crystal pieces, count them. If there are four and in the right place, a new option appears.
    local iTotalCrystals = 0
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "crystal piece") then
            iTotalCrystals = iTotalCrystals + 1
        end
    end
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
        if(gzTextVar.gzPlayer.sLocation == "Stone Gallery" and iTotalCrystals >= 4) then
            TL_SetProperty("Register Popup Command", "use",  "use crystals")
        end
        if(iTotalCrystals >= 4) then
            TL_SetProperty("Register Popup Command", "merge",  "merge crystals")
        end
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

-- |[ ======================================== Priority ======================================== ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iKeys
    return
end

-- |[ ===================================== Trap Builder ======================================= ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take"}
    return
end

-- |[ ===================================== Verification ======================================= ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[Synonym Handler]|
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"crystal", "piece"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

-- |[ ==================================== Basic Commands ====================================== ]|
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    TL_SetProperty("Append", "A chunk of a black crystal. It has been broken into four. Light seems to grow dim when near the crystal." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    TL_SetProperty("Append", "A chunk of a black crystal. It has been broken into four. Light seems to grow dim when near the crystal." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    LM_ExecuteScript(gzTextVar.sStandardTake, sLocalName, iItemSlot, iRoomSlot)

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)

-- |[Special]|
--Merge four crystals into one. Must be in the inventory. Cannot be done if you are a monster.
elseif((sInputString == "merge crystals" or sInputString == "merge crystal pieces") and iRoomSlot == -1) then
    gbHandledInput = true

    -- |[Blocking Check]|
    --If the player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Dumb-Mary. Shouldn't be possible.
        if(gzTextVar.iGameStage == 5) then
            TL_SetProperty("Append", "The little crystal pieces look sharp. Better not touch them!" .. gsDE)
            return
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "DO NOT MERGE THE PIECES. THE CRYSTAL PIECES MUST BE HIDDEN." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "You do not merge the pieces. The crystal pieces must be hidden. You will hide them later." .. gsDE)
        TL_SetProperty("Create Blocker")
        return

    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "You take a look at the crystal pieces. It'd be bad if the humans merged them." .. gsDE)
        return

    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "You don't really know what the pieces are for, but the idea of merging them is far too complicated for you." .. gsDE)
        return

    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You know that the creator would be upset if you did that. You almost want to, just to see what would happen, but ultimately do nothing." .. gsDE)
        return

    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "The crystal pieces aren't supposed to be merged." .. gsDE)
        return

    --Doll version.
    else
        --Before phase 6, the doll player cannot read.
        if(gzTextVar.iGameStage ~= 6) then
            TL_SetProperty("Append", "You take a look at the pieces. Something in your head says you really shouldn't put them together, so you don't." .. gsDE)
            return
        end
    end

    -- |[Count]|
    --Count how many crystals we have.
    local iTotalCrystals = 0
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "crystal piece") then
            iTotalCrystals = iTotalCrystals + 1
        end
    end

    -- |[Not Enough]|
    --You must have four crystals. If not, print a warning.
    if(iTotalCrystals < 4) then
        TL_SetProperty("Append", "You must have four crystals in your inventory to merge them together." .. gsDE)
    
    -- |[Have Enough]|
    --Has all four crystals.
    else

        --Merge!
        TL_SetProperty("Append", "As you touch the crystal pieces together to assemble them, they seem to melt before your eyes." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The light around you dims and you lose sight of the crystals as a black gulf surrounds them. Moments later, the pall lifts." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        if(gzTextVar.bHasSeenHallwayDialogue == false) then
            TL_SetProperty("Append", "In your hands sits a single dark crystal. While you're not sure where this might be useful, the fact that it was carefully hidden means it must be important." .. gsDE)
            TL_SetProperty("Create Blocker")
            
        else
            TL_SetProperty("Append", "In your hands sits a single dark crystal. No doubt you can [use] this to get past the mysterious light..." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
        
        --Add the item to the inventory.
        gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
        local p = gzTextVar.gzPlayer.iItemsTotal

        --Create the item.
        gzTextVar.gzPlayer.zaItems[p] = {}
        gzTextVar.gzPlayer.zaItems[p].sUniqueName = "darkcrystal"
        gzTextVar.gzPlayer.zaItems[p].sDisplayName = "dark crystal"
        gzTextVar.gzPlayer.zaItems[p].bCanBePickedUp = true
        gzTextVar.gzPlayer.zaItems[p].sHandlerScript = gzTextVar.sRootPath .. "Item Handlers/ToolsConsumables/Dark Crystal.lua"
        gzTextVar.gzPlayer.zaItems[p].sIndicatorName = "Null"
        gzTextVar.gzPlayer.zaItems[p].bIsStackable = false
        gzTextVar.gzPlayer.zaItems[p].iStackSize = 1
        
        --Special flags. Not all objects use these.
        gzTextVar.gzPlayer.zaItems[p].iJournalPage = -1
        gzTextVar.gzPlayer.zaItems[p].bIsClaygirlTrapped = false
        gzTextVar.gzPlayer.zaItems[p].sClaygirlName = "Null"
        gzTextVar.gzPlayer.zaItems[p].saClaygirlList = {}

        --Remove the items from the inventory.
        for i = 1, 4, 1 do
            for p = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
                if(gzTextVar.gzPlayer.zaItems[p].sDisplayName == "crystal piece") then
                    fnRemoveItemFromInventory(p)
                    break
                end
            end
        end
        fnBuildLocalityInfo()
    end
end