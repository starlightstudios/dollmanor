-- |[ ====================================== Examinables ======================================= ]|
--This script is used by all examinable objects. This is for objects which can only ever be examined
-- and nothing else.
--Because these items can't be picked up, they should nominally never be in the player's inventory
-- but we often don't check for that.

-- |[ ======================================= Indicator ======================================== ]|
--Negative values indicate this item does not have an indicator.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = -1
    gzTextVar.iGlobalIndicatorY = -1
    return
end

-- |[ ======================================= Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    return
end

-- |[ ===================================== Verification ======================================= ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[ ======================================== Handling ======================================== ]|
--Run across the object listing and find the remap name.
local sRemapTo = "Null"
if(iRoomSlot == -1) then
    sRemapTo = gzTextVar.gzPlayer.zaItems[iItemSlot].sUniqueName
else
    sRemapTo = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sUniqueName
end

--No remap found.
if(sRemapTo == "Null") then
    TL_SetProperty("Append", "There is no object here like that. Types 'objects' to see a list of objects nearby." .. gsDE)
    return
end

--Check to see if this is "look" or "examine"
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --Description listing.
    if(sRemapTo == "mainhallstatue") then
        gbHandledInput = true
        TL_SetProperty("Append", "A suit of armor. It is tarnished from age, yet projects a feeling of security." .. gsDE)
        return
    elseif(sRemapTo == "mainhallbed") then
        gbHandledInput = true
        TL_SetProperty("Append","A bed which has been dragged here from somewhere else. It is unkempt and has not been cleaned in some time." .. gsDE)
        return
    elseif(sRemapTo == "mainhallfood") then
        gbHandledInput = true
        TL_SetProperty("Append", "Food on a crumbling wooden shelf. Breads, vegetables, fruits, dried meats. They do not look appetizing, but are not rotting." .. gsDE)
        return
    end

--Take commands always fail.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then
    gbHandledInput = true
    TL_SetProperty("Append", "You cannot take that with you." .. gsDE)

end