-- |[ =================================== Eileen's Testament =================================== ]|
--Message left by Eileen. Can't be taken or moved, only read.

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 3
    gzTextVar.iGlobalIndicatorY = 0
    return
end

-- |[ ======================================== Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        --Item never exists in the inventory.
    
    --On the ground.
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
    end
    return
end

-- |[ ======================================== Priority ======================================== ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = 0
    return
end

-- |[ ====================================== Trap Builder ====================================== ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take"}
    return
end

-- |[ ====================================== Verification ====================================== ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[ ===================================== Basic Commands ===================================== ]|
--Examining it.
sLocalName = "testament"
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName or sInputString == "read " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Display.
    if(gzTextVar.gzPlayer.bHasSeenTestament == false) then
        gzTextVar.gzPlayer.bHasSeenTestament = true
        TL_SetProperty("Append", "A stone slab with strange glyphs on it, some unknown language. As you look at it, the glyphs seem to blur." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "A moment later, the glyphs un-blur, and are written in plain English. Somehow..." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    end
    
    TL_SetProperty("Append", "'There are signs of human habitation in this building. Sometimes I hear whispers or conversations, but when I search, I find no one.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "'Food is left cooked on the counters of the kitchens. Someone changes the bedsheets. I never see who, but the food keeps me going.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "'There is someone following me, however. Relentless. I hear their heavy footsteps above me or below me, always close. Come find me, stranger.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flag
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName) then

    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --During a "take all" check, don't print this message.
    if(gzTextVar.bTakeAll) then return end
    
    gbHandledInput = true
    TL_SetProperty("Append", "The testament is carved on a solid stone slab. It's far too heavy to move." .. gsDE)
    TL_SetProperty("Create Blocker")
    
end