-- |[ ======================================== Journal ========================================= ]|
--The journal, found when the player takes the key Pygmalie dropped. Pages get added as the player
-- finds them. Pages can be found out of order.

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 2
    return
end

-- |[ ======================================== Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
    
    --On the ground. Note that the item can't normally be dropped, these are here for possible future changes.
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

-- |[ ======================================== Priority ======================================== ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iJournal
    return
end

-- |[ ====================================== Trap Builder ====================================== ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "read"}
    return
end

-- |[ ====================================== Verification ====================================== ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[ ===================================== Basic Commands ===================================== ]|
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Incomplete.
    if(gzTextVar.iTotalPagesFound < ciTotalPages) then
        TL_SetProperty("Append", "Parts of the journal of Sarah Lee-Anne. Some pages are missing. They seem to be scattered at random throughout the area." .. gsDE)
    
    --Complete.
    else
        TL_SetProperty("Append", "The completed journal of Sarah Lee-Anne. It details how she got here and what happened to her. You've found all the pages." .. gsDE)
    end
    
    --Flag
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then

    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Incomplete.
    if(gzTextVar.iTotalPagesFound < ciTotalPages) then
        TL_SetProperty("Append", "Parts of the journal of Sarah Lee-Anne. Some pages are missing. They seem to be scattered at random throughout the area." .. gsDE)
    
    --Complete.
    else
        TL_SetProperty("Append", "The completed journal of Sarah Lee-Anne. It details how she got here and what happened to her. You've found all the pages." .. gsDE)
    end
    
    --Flag
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    --Ignore this if the item in question is already in the inventory.
    if(iRoomSlot == -1) then
        
        --If there are potions in the inventory but not on the ground, print a warning message.
        local iIndexInRoom = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        if(iIndexInRoom == -1) then
            TL_SetProperty("Append", "You already have that in your inventory." .. gsDE)
            gbHandledInput = true
        end
        return
    end
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to pick that up." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Otherwise, take the item.
    TL_SetProperty("Append", "You pick up the [" .. sLocalName .. "]." .. gsDE)
    
    --Increment
    gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
    local p = gzTextVar.gzPlayer.iItemsTotal
    
    --Create the item.
    gzTextVar.gzPlayer.zaItems[p] = {}
    gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]
    
    --Remove the item from the original room listing.
    fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    gbHandledInput = true

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end

    --Display.
    gbHandledInput = true
    TL_SetProperty("Append", "You cannot drop something so important!" .. gsDE)
    return

--Read Handler. This reads the table of contents.
elseif(sInputString == "read " .. sLocalName) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Assemble a list of pages.
    if(gzTextVar.iTotalPagesFound < ciTotalPages) then
        
        --Setup.
        local sString = "Null"
        
        --Exactly one page.
        if(gzTextVar.iTotalPagesFound == 1) then
            local iPage = 1
            for i = 1, ciTotalPages, 1 do
                if(gzTextVar.baPagesFound[i] == true) then
                    iPage = i
                    break
                end
            end
            
            sString = "Some of the pages are missing. You can read a specific page with [read journal 1-14]. You currently have page " .. iPage .. "." .. gsDE
            
        --Two pages.
        elseif(gzTextVar.iTotalPagesFound == 2) then
            local iPageA = -1
            local iPageB = -1
            for i = 1, ciTotalPages, 1 do
                if(gzTextVar.baPagesFound[i] == true) then
                    if(iPageA == -1) then
                        iPageA = i
                    else
                        iPageB = i
                        break
                    end
                end
            end
            sString = "Some of the pages are missing. You can read a specific page with [read journal 1-14]. You currently have pages " .. iPageA .. " and " .. iPageB .. "." .. gsDE
        
        --More than two.
        else
            
            sString = "Some of the pages are missing. You can read a specific page with [read journal 1-14]. You currently have pages "
            
            local iRegged = 0
            for i = 1, ciTotalPages, 1 do
                if(gzTextVar.baPagesFound[i] == true) then
                    iRegged = iRegged + 1
                    if(iRegged < gzTextVar.iTotalPagesFound) then
                        sString = sString .. i .. ", "
                    
                    else
                        sString = sString .. "and " .. i .. "." .. gsDE
                    end
                end
            end
        
        end
        
        --Send the string.
        TL_SetProperty("Append", sString)
        
    --Full listing.
    else
        TL_SetProperty("Append", "There are 14 pages to read. You can read a specific page with [read journal 1-14]." .. gsDE)
    end
    
    --Text.
    gbHandledInput = true

--Read Handler. First page.
elseif(sInputString == "read " .. sLocalName .. " 1" or sInputString == "read " .. sLocalName .. " page 1") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[1] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end
    
    --Dialogue
    TL_SetProperty("Append", "My name is Sarah Lee-Anne. I am writing this in November of the year 1957. I don't know what day it is. I don't know if it is even still November as I write this. It has been so many days that I lost track." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I am writing this because I realized that I am forgetting things the longer I am here. I realized yesterday that I could no longer remember going to primary school. I can remember that I went there, but I cannot remember what I did there, if I did anything. I cannot remember the people I went with, the teachers who taught me, or what I did before and after. It is all gone." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I fear that as I stay here longer, I will lose more. So I am writing what I have here, while I can." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Second page.
elseif(sInputString == "read " .. sLocalName .. " 2" or sInputString == "read " .. sLocalName .. " page 2") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[2] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I do not know how I got here. What I remember was walking home one night from town after the dance. I saw a huge building in the moonlight where the old Finster farmhouse should have been, and I decided to look into it. I saw nothing." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I left, doubled back to the road. I continued on the path home. I then passed a junction, looked to my right again, and there, in the light of the moon, I saw the same building. Same outline, same size." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I walked straight away from the building, figuring I had gotten lost. And as I walked, I found that I was at the building again, but this time behind it. It was... impossible..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I stayed outside for a while. Eventually, I realized that even if I went straight home I would never make it before the sun came up. I was growing too tired to continue walking, so I went inside. The door wasn't locked and nobody was home. The place was decrepit, even older than the Finster farmhouse had been." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I tried to sleep, but was too scared. I ended up laying awake, waiting for the sun to come up. But the sun didn't come up. It stayed night. It's still night, right now." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Third page.
elseif(sInputString == "read " .. sLocalName .. " 3" or sInputString == "read " .. sLocalName .. " page 3") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[3] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I am writing in this journal again with something at least somewhat heartening. It has been some time since last I wrote. I don't know how many days." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Someone came to visit. She was dressed in tribal clothing, made of leathers and simple cloth. She looks like the native peoples, but her skin is darker. She yelled and swore at me in a language I could not understand. I didn't need to. Angry yelling transcends the barriers of language." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There isn't a reservation for a long way from my home. The woman has simple stone tools with her, and what looks like a knife carved from obsidian. I've seen the tools before, in picture books. They're from South America. I think they were called the Mayans or Aztecs." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Fourth page.
elseif(sInputString == "read " .. sLocalName .. " 4" or sInputString == "read " .. sLocalName .. " page 4") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[4] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "The woman and I didn't get on too well, but she calmed down after a few days when it became clear I wasn't the one who brought her here. I don't think she's even seen a white person before." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She's restless. She wanders all over the building. I don't know what she's looking for, we're stuck here. She tried to leave earlier and came right back, looking confused. That's how I felt, too." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I told her my name. She says it funny, like Surrah. She says her name is Ayra, and I guess I say it wrong too because she giggles when I say it. I don't know how you spell it, I tried my best." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "We've been here for a long time. There are beds. We sleep in them when we're tired, but when we wake up it's still night. It's always night. Sometimes, if we search, there's food in the house. I don't know how it gets there. Water, too. Sometimes it rains, sometimes there's glasses full of the stuff sitting near the beds." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I don't question it. I did at first. I was wary. I thought it was ghosts or magic or something. I don't know. But I know when I got thirsty I had to drink it." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Fifth page.
elseif(sInputString == "read " .. sLocalName .. " 5" or sInputString == "read " .. sLocalName .. " page 5") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[5] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "Ayra went to the top floor today. I heard her kicking and shouting a lot. There's a door up there we haven't been able to open. I think she was trying to kick it down. Then she came back to me with a bruised foot. I did my darndest to help but I don't have any medical supplies. Poor girl." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I had a strange dream. There was a great pit around the house, and when I stood on the edge and looked down, I saw great pillars of steam coming from the blackness. Then I woke up. I was covered in tears. Had I been crying while asleep? Can you even do that?" .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Sixth page.
elseif(sInputString == "read " .. sLocalName .. " 6" or sInputString == "read " .. sLocalName .. " page 6") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[6] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I found Ayra on the other side of the door, upstairs, this morning. I guess she broke through it. She didn't look hurt, but she was lying in the room on the far side. Face down, on the floor, drooling on herself." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She's still alive, I guess, but she doesn't respond to her name or when I touch her. Maybe she lost the will to live and has given up. Maybe I should, too." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "In the room past the door, I found an altar. Like the kind at church, with candles and rows of pews in front of it. This isn't a chapel, I think, because there are no crosses or anything. The altar has some symbols on it. I guess I should try to translate them." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Seventh page.
elseif(sInputString == "read " .. sLocalName .. " 7" or sInputString == "read " .. sLocalName .. " page 7") then

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[7] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "Ayra is up and moving around again. I think. It's really hard to say. She doesn't move around when I'm with her, but sometimes I'll leave the room to find something to eat, and I'll come back and she's not where I left her. She still doesn't react to anything I do or say. Is it a prank? Has she gone insane? Or maybe, have I?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Sometimes I go up to that altar up there and wonder if it was worth it. Would it matter if she hadn't broken down the door? At least I had someone to talk to, even if we couldn't understand each other." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I decided to maybe write down the symbols. Maybe if someone finds this book they might be helpful." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Eighth page.
elseif(sInputString == "read " .. sLocalName .. " 8" or sInputString == "read " .. sLocalName .. " page 8") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[8] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I did something. I don't know what, but I did it. I had another dream. I forgot it when I woke up, but I decided to write the symbols I found on Ayra. That did something to her. Her eyes focused on me, and she said something. Then she went inert. Darn it, why can't I understand what she says!? Was she asking for help?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A bowl of milk appeared on the altar. I think it went sour because it was thick and smelled bad. I didn't care because I don't much like milk anyway, so I left it there. But when I came back after going downstairs, the milk was gone, and Ayra was up and walking around again. She didn't talk to me, or even seem to recognize me, but it's something." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Ninth page.
elseif(sInputString == "read " .. sLocalName .. " 9" or sInputString == "read " .. sLocalName .. " page 9") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[9] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I keep having strange dreams. I know they're strange but I don't remember them when I wake up. I wrote the symbols on Ayra's body again. She seems to understand me, at least. I wrote the symbol for Benevolence on her. I don't know how I know it was Benevolence but I wrote it on her and she smiled at me." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The milk appears when I write the word for Command on something. It doesn't matter what. I scratched it on the pews with a knife and the milk poured out on a bed downstairs. Is there a pattern? Maybe I've lost it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I gave the milk to Ayra. She splashed it on her face. She seems to like it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Tenth page.
elseif(sInputString == "read " .. sLocalName .. " 10" or sInputString == "read " .. sLocalName .. " page 10") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[10] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "We're not alone here. There's another thing in the house. I'm not afraid of it, because it hasn't hurt us and we've been here a long time. But it's here. I guess it could kill us if it wanted to, yet it doesn't." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Ayra went limp again. I wrote the words on her but they didn't do anything. Despite the fact that she really isn't all there anymore, I don't want to lose her. I guess she's my only friend." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The thing did it to her. I heard them. Ayra was in the main hall, I was downstairs in the winery. There's so many bottles of wine in there, I decided to break my promise to my mom and have some. And I heard the thing, but Ayra shouted at it. She shouted and it went away and I came back and she's limp and I can't get her up." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Eleventh page.
elseif(sInputString == "read " .. sLocalName .. " 11" or sInputString == "read " .. sLocalName .. " page 11") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[11] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "So, it's been a long time since I last wrote. Months, probably. I figured some things out." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Writing makes me smarter. I stopped writing for a while and I forgot some things. I forgot my name - I forgot I was Sarah Lee-Anne! But then I read it again and I remember it, and some other things, too." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I remember that I could find things that I wanted if I just thought about them as I went to sleep. I thought about a warm jacket and I found one when I woke up. I thought about a needle full of the milk and I found one when I woke up. I thought about sticking the needle into Ayra's neck and I did it when I woke up." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She didn't try to stop me, or do anything really. There's not much of her left. The thing took that. I can't stop the thing, whatever it is. But if I keep writing and thinking then the thing leaves me alone." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Ayra's skin is plastic, now. She's like a doll, so I decided I would make her a doll. I dressed her in my jacket and gave her my shoes. She looks so cute. She does what I tell her. Sometimes I don't even tell her and she still does it. I love Ayra." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Twelfth page.
elseif(sInputString == "read " .. sLocalName .. " 12" or sInputString == "read " .. sLocalName .. " page 12") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[12] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "The thing took more from me. I don't remember what it took but I can't let it take everything. But I figured out what to do." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A boy came to the house. I had Ayra tie him up. We left him in the main hall, and the thing was in there. When I came back, he was empty. It was so easy. And then I had Ayra stick the needle in his neck, and now he's a doll, too." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The thing left us alone for a few weeks after it ate Melissa's thoughts. That's what I named the boy now that he's a doll. Melissa. Melissa Melissa Melissa." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I guess I can't leave the place now, or ever. I've been here a long time, and I've been alone for so long, but now I have Melissa and Ayra. I know what I did was awful. I feel terrible about it, but what could I do? If the thing had gotten me, then what? At least Ayra and Melissa get to stay with me and be my best friends." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "But the thing wants me, now. I keep writing and thinking but I know it's going to get me sooner or later." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Thirteenth page.
elseif(sInputString == "read " .. sLocalName .. " 13" or sInputString == "read " .. sLocalName .. " page 13") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[13] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I've got a lot of friends now. I give some to the thing. Sometimes I make them dolls first, because that way they won't run or hide. When it's done with them they still get to be my friends. But it still wants me." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There are other things I can make them. I need different symbols. I know what they mean. I made some for different purposes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I told Ayra and Melissa to stop the thing, but they didn't. They can't see it, or maybe nothing can see it. I've been here a long time and I have never seen it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It's going to get me but I'm so much smarter than it. I'll make sure there's nothing to get." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Fourteenth page.
elseif(sInputString == "read " .. sLocalName .. " 14" or sInputString == "read " .. sLocalName .. " page 14") then
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end

    --Possession checker.
    gbHandledInput = true
    if(gzTextVar.baPagesFound[14] == false) then
        TL_SetProperty("Append", "You are missing that page. It must be somewhere in the manor." .. gsDE)
        return
    end

    --Dialogue
    TL_SetProperty("Append", "I put it in this room, and I put guards in front of it. The thing is strong but now it won't get me. Once I finish writing this page, it'll be over. I won't remember anything. Then there will be nothing for the thing to take. I win." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "I win." .. gsDE)
    TL_SetProperty("Create Blocker")
    
end