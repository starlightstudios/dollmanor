-- |[ ===================================== Tutorial Note ====================================== ]|
--Tutorial note that provides instructions on how to play the game.

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 0
    gzTextVar.iGlobalIndicatorY = 2
    return
end

-- |[ ======================================== Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        --Item never exists in the inventory.
    
    --On the ground.
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
    end
    return
end

-- |[ ======================================== Priority ======================================== ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = 0
    return
end

-- |[ ====================================== Trap Builder ====================================== ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take"}
    return
end

-- |[ ====================================== Verification ====================================== ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[ ===================================== Basic Commands ===================================== ]|
--Examining it.
sLocalName = "note"
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Display.
    TL_SetProperty("Append", "If you've made it this far, you're ready for whatever String Tyrant will throw at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Stay quiet, avoid enemies, pick up items, and read whatever information you can find. And - be on the lookout for devilish traps." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "You can save the game with the [save] command when at a fountain in the normal game. There's one in the main hall at the start of the game, so keep your eyes out." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "You can also edit the size of your deck with the [deck] command. You may find additional cards to tip the scales in your favour, so keep an eye out." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To experiment further with the combat mechanics, proceed to the north. To learn about the stealth mechanics, proceed to the south." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To exit, type [restart] to return to the main menu. Good luck." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flag
    gbHandledInput = true
end