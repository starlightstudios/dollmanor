-- |[ ===================================== Tutorial Note ====================================== ]|
--Tutorial note that provides instructions on how to play the game.

-- |[ ======================================= Indicator ======================================== ]|
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 0
    gzTextVar.iGlobalIndicatorY = 2
    return
end

-- |[ ======================================== Stacking ======================================== ]|
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

-- |[ ================================== Argument Resolution =================================== ]|
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

-- |[ ==================================== Command Builder ===================================== ]|
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        --Item never exists in the inventory.
    
    --On the ground.
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
    end
    return
end

-- |[ ======================================== Priority ======================================== ]|
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = 0
    return
end

-- |[ ====================================== Trap Builder ====================================== ]|
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take"}
    return
end

-- |[ ====================================== Verification ====================================== ]|
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

-- |[ ===================================== Basic Commands ===================================== ]|
--Examining it.
sLocalName = "note"
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Display.
    TL_SetProperty("Append", "Ahead of you is a real enemy. There's no way around her, so you'll have to fight." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "There's a weapon on the ground ahead of you. [equip] it to improve your chances in combat." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "In combat, you will need to create attacks or defenses using a card deck. This enemy is easy and will not fight back very much." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Click on cards to 'play' them. Element cards increase damage/defense, action cards change whether you are attacking or defending." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "'And' and 'Of' cards let you play additional element cards. Cards you cannot play are greyed out." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Using more of the same element in an attack increases the damage dealt, so pick like-elements when you can. Elements have other special effects as well!" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Click the [Draw] button in the bottom right to draw additional cards into your hand." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Enemies attack after you have spent a certain amount of 'time units'. Each action you do costs time units. When attacked, you lose heart chips. Your hearts show your health, and if they run out, you lose." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "If you use Defend cards, you will create shields that stop damage. They appear above your health bar. Balance your offense and defense." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Also, you have an 'Ace' card in the bottom left. It is more powerful than normal cards, and reappears after a while. Use it early and often!" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Lastly, if you run out of cards, you will need to 'Shuffle' your deck. This consumes a lot of time units. Try to use your cards wisely and spend as little time shuffling as possible!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flag
    gbHandledInput = true
end