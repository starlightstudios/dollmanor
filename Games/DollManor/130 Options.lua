-- |[ ======================================== Options ========================================= ]|
--Changes game options. Can be used at any time.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

-- |[ =================================== Open Options Menu ==================================== ]|
--Opens the options menu.
if(sString == "option" or sString == "options" or sString == "menu" or sString == "settings") then
    gbHandledInput = true
    TL_SetProperty("Open Options")
    return
end


-- |[ =================================== Sound/Music Options ================================== ]|
--Change the music volume.
if(string.sub(sString, 1, 15) == "option musicvol") then
    
    --Resolve the volume.
    local sVolumeString = string.sub(sString, 17)
    local iVolume = tonumber(sVolumeString)
    
    --"Default"
    if(sVolumeString == "default") then
        local iVolume = AudioManager_GetProperty("Default Music Volume") * 100.0
        TL_SetProperty("Append", "Music volume set to " .. iVolume .. " (default)." .. gsDE)
        AudioManager_SetProperty("Music Volume", iVolume / 100.0)
        OM_WriteConfigFiles()
    
    --Cannot coerce the volume.
    elseif(sVolumeString == "" or iVolume == nil) then
        
        local iCurrentVolume = math.floor(AudioManager_GetProperty("Music Volume") * 100.0)
        TL_SetProperty("Append", "Music volume may be changed with 'option musicvol [level]', where [level] is a number from 0 to 100. 100 is maximum volume. Current volume is " .. iCurrentVolume .. ".")
        TL_SetProperty("Append", "'option musicvol default' will return the music volume to its default value." .. gsDE)
    
    --Got a value.
    else
    
        --Clamp.
        if(iVolume <   0) then iVolume =   0 end
        if(iVolume > 100) then iVolume = 100 end
    
        TL_SetProperty("Append", "Music volume set to " .. iVolume .. "." .. gsDE)
        AudioManager_SetProperty("Music Volume", iVolume / 100.0)
        OM_WriteConfigFiles()
        
    end
    
    gbHandledInput = true
    return
end

--Change the sound volume.
if(string.sub(sString, 1, 15) == "option soundvol") then
    
    --Resolve the volume.
    local sVolumeString = string.sub(sString, 17)
    local iVolume = tonumber(sVolumeString)
    
    --"Default"
    if(sVolumeString == "default") then
        local iVolume = AudioManager_GetProperty("Default Sound Volume") * 100.0
        TL_SetProperty("Append", "Sound volume set to " .. iVolume .. " (default)." .. gsDE)
        AudioManager_SetProperty("Sound Volume", iVolume / 100.0)
        OM_WriteConfigFiles()
        
    --Cannot coerce the volume.
    elseif(sVolumeString == "" or iVolume == nil) then
        
        local iCurrentVolume = math.floor(AudioManager_GetProperty("Sound Volume") * 100.0)
        TL_SetProperty("Append", "Sound volume may be changed with 'option soundvol [level]', where [level] is a number from 0 to 100. 100 is maximum volume. Current volume is " .. iCurrentVolume .. ".")
        TL_SetProperty("Append", "'option soundvol default' will return the sound volume to its default value." .. gsDE)
    
    --Got a value.
    else
    
        --Clamp.
        if(iVolume <   0) then iVolume =   0 end
        if(iVolume > 100) then iVolume = 100 end
    
        TL_SetProperty("Append", "Sound volume set to " .. iVolume .. "." .. gsDE)
        AudioManager_SetProperty("Sound Volume", iVolume / 100.0)
        OM_WriteConfigFiles()
        
    end
    
    gbHandledInput = true
    return
end

