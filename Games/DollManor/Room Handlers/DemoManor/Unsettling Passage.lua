--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then

    --Handle the input.
    gbHandledInput = true

    --Description.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        if(gzTextVar.bDisabledLight == false) then
            TL_SetProperty("Append", "A carved stone passage. There is a strange light emanating from the western end of the room. It makes you feel very uneasy." .. gsDE)
        else
            TL_SetProperty("Append", "A carved stone passage. There was once a blue light here, but it's gone now." .. gsDE)
        end
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "A carved stone passage. There is a strange light emanating from the western end of the room. You feel nothing." .. gsDE)
    else
        if(gzTextVar.bDisabledLight == false) then
            TL_SetProperty("Append", "A carved stone passage. There is a strange light emanating from the western end of the room." .. gsDE)
        else
            TL_SetProperty("Append", "A carved stone passage. There was once a blue light here, but it's gone now." .. gsDE)
        end
    end

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    
end