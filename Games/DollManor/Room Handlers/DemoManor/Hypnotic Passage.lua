--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        if(gzTextVar.bDisabledLight == false) then
            TL_SetProperty("Append", "Further into the stone passage, the light is brighter here. You feel drawn towards it. You should go west..." .. gsDE)
        else
            TL_SetProperty("Append", "Further into the stone passage, you can see a row of statues to the west." .. gsDE)
        end
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "Further into the stone passage, the light is brighter here." .. gsDE)
    else
        if(gzTextVar.bDisabledLight == false) then
            TL_SetProperty("Append", "Further into the stone passage, the light is brighter here." .. gsDE)
        else
            TL_SetProperty("Append", "Further into the stone passage, you can see a row of statues to the west." .. gsDE)
        end
    end

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)

end