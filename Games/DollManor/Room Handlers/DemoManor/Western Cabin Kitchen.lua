--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    TL_SetProperty("Append", "The cabin's kitchen has an ancient looking stove and some potatoes. They've probably been sitting here a long time, but they don't look rotten. Still, better not touch them." .. gsDE)

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return
end