--[Arguments]
--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--[Handlers]
--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        if(gzTextVar.bDisabledLight == false) then
            TL_SetProperty("Append", "At the end of the hallway stand dozens of statues. They are amazingly lifelike. The room is bathed in a calming blue glow." .. gsDE)
        else
            TL_SetProperty("Append", "At the end of the hallway stand dozens of statues. They are amazingly lifelike. The crystal of darkness is absorbing the blue light here." .. gsDE)
        end
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "At the end of the hallway stand dozens of statues. They are amazingly lifelike. You will take your place among them when you have captured the intruders." .. gsDE)
    else
        TL_SetProperty("Append", "At the end of the hallway stand dozens of statues. They are amazingly lifelike." .. gsDE)
    end

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)

--Locality Builder.
elseif(gzTextVar.bIsLocalityCheck) then

    --When human, the player can voluntarily 'obey' or 'gaze into the light'.
    if(gzTextVar.gzPlayer.sFormState == "Human" and gzTextVar.bDisabledLight == false) then
        TL_SetProperty("Register Locality Entry", "gaze", "Commands", "INSTANT")
    end
    
    --When human and in possession of four crystal pieces, you can use them here to disable the light.
    if(gzTextVar.gzPlayer.sFormState == "Human" and gzTextVar.bDisabledLight == false) then
        local iTotalCrystals = 0
        for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
            if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "crystal piece") then
                iTotalCrystals = iTotalCrystals + 1
            end
        end
        if(iTotalCrystals >= 4) then
            TL_SetProperty("Register Locality Entry", "use crystals", "Commands", "INSTANT")
        end
    end
    
--[Obey]
--Causes the player to more quickly get petrified. Getting petrified this way changes things a bit.
elseif(sString == "obey" or sString == "gaze") then

    --Handle the input.
    gbHandledInput = true
    
    --Blocker.
    if(gzTextVar.bDisabledLight == true) then
        TL_SetProperty("Append", "While part of you wants to submit to the light, it is already contained within the crystal. You leave it." .. gsDE)
        return
    end
    
    --Cutscene.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Transformation sequence.
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
        gzTextVar.gzPlayer.sFormState = "Statue"
    
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "You submit to the light. You obey its will, and gaze into it. All other thoughts flee you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The light welcomes you. ALLOW THE LIGHT TO FILL YOU. You allow the light to fill you. LOVE THE LIGHT. You love the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "YOU ARE THE LIGHT. The light enters you completely, fills you, changes you. You are the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "YOU ARE THE LIGHT, THE LIGHT IS YOU. YOU HAVE NO THOUGHTS, THE LIGHT IS YOUR THOUGHTS. MARY IS GONE. THERE IS NO MARY. THERE IS THE LIGHT." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryStoneTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "Mary disappears. Your body begins to change. This is good, this is correct. You do not think as you become harder." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "YOU DO NOT MOVE. YOU DO NOT THINK. THIS IS CORRECT. You do not move or think as your skin hardens." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryStoneTF1", ciImageLayerDefault)
        TL_SetProperty("Append", "You become rock as the light flows through and around you. Skin, muscle, bone, all becomes stone. You think no thoughts." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryStoneTF2", ciImageLayerDefault)
        TL_SetProperty("Append", "The sensation overtakes you. The last vestiges of flesh are gone. There is only stone." .. gsDE)
        TL_SetProperty("Create Blocker")
    
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 3, 3, ciCodePlayer)
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryStone", ciImageLayerDefault)
        TL_SetProperty("Append", "You have changed. You are a mere statue. The statues along the side of the room call to you. You should take your place." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "BECOME A VESSEL. You become a vessel. The light flows over you. It enters into you again, changing you. Changing your outside." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/Mary"
        TL_SetProperty("Append", "You are a statue. Your skin has been painted to resemble flesh. Your body has been changed to resemble flesh. But you are a statue." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The light fills you. FIND THE INTRUDERS. You will find the intruders. BRING THEM TO THE LIGHT. You will bring them to the light." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        --Flags.
        gzTextVar.bSecretStatue = true
        gzTextVar.bIsJessieFollowing = false
        gzTextVar.bIsLaurenFollowing = false
        
        --Unlock all doors.
        LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")
        
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "You are already obedient to the light. You must FIND THE INTRUDERS. You must BRING THEM TO THE LIGHT." .. gsDE)
    else
        TL_SetProperty("Append", "You cannot obey the light, your body is not designed for it." .. gsDE)
    end

--[Use Crystals]
--Uses the four crystal pieces to form the Crystal of Darkness, neutralizing the light.
elseif(sString == "use crystals") then

    --Handle the input.
    gbHandledInput = true

    --Already disabled.
    if(gzTextVar.bDisabledLight == true) then
        TL_SetProperty("Append", "The light has already been neutralized by the crystal of darkness." .. gsDE)
        return
    end

    --Statues get special dialogue.
    if(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "THE CRYSTALS MUST BE HIDDEN. HIDE THEM. OBEY." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The crystals in your possession must be hidden. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "FIND THE INTRUDERS. BRING THEM TO THE LIGHT. THEN HIDE THE CRYSTALS." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Before you do anything else, you must find the intruders and bring them to the light. Once that is done, you will hide the crystals. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    
    --Human, if you have four pieces you can disable the light.
    elseif(gzTextVar.gzPlayer.sFormState == "Human") then

        --When human and in possession of four crystal pieces, you can use them here to disable the light.
        local iTotalCrystals = 0
        for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
            if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "crystal piece") then
                iTotalCrystals = iTotalCrystals + 1
            elseif(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "dark crystal") then
                iTotalCrystals = 100
            end
        end
        
        --Disable the light!
        if(iTotalCrystals >= 4) then
            
            --Common flags.
            gzTextVar.bDisabledLight = true
            gzTextVar.iHypnoticCheck = 0
            
            --If the value is 100, that means we used the assembled dark crystal. This changes the dialogue a big.
            if(iTotalCrystals == 100) then
                
                TL_SetProperty("Append", "As you hold up the crystal, the light in the room bends and swirls. It is soon drained and absorbed within the crystal, and it no longer affects you." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "While it is much darker in this room, you feel the pall lift from your mind. This place is safe now." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                for p = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
                    if(gzTextVar.gzPlayer.zaItems[p].sDisplayName == "dark crystal") then
                        fnRemoveItemFromInventory(p)
                        break
                    end
                end
                fnBuildLocalityInfo()
                
            --Otherwise, use the four pieces.
            else
            
                TL_SetProperty("Append", "You put the fragments of the crystal together. The light swirls around it, bending and distorting until it no longer affects you." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "While it is much darker in this room, you feel the pall lift from your mind. This place is safe now." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Remove the items from the inventory.
                for i = 1, 4, 1 do
                    for p = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
                        if(gzTextVar.gzPlayer.zaItems[p].sDisplayName == "crystal piece") then
                            fnRemoveItemFromInventory(p)
                            break
                        end
                    end
                end
                fnBuildLocalityInfo()
            end
            
        --Not enough parts!
        else
            TL_SetProperty("Append", "You don't have all four crystal pieces!" .. gsDE)
        end

    --All other forms.
    else
        TL_SetProperty("Append", "Your creator does not wish for you to do anything to the light. It protects something important. Leave it alone." .. gsDE)
    end
end