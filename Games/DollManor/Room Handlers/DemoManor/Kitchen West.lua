--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    TL_SetProperty("Append", "The barrels here contain apple cider, judging by the labels. No date is written. If you had a cup, you could take some with you." .. gsDE)

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return
end