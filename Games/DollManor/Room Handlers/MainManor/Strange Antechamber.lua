-- |[Arguments]|
--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

-- |[Handlers]|
--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.gzPlayer.iClayInfection == -1) then
            TL_SetProperty("Append", "A strange room. Despite being carved from stone like the rest of the basement, it was done with very different architecture. There is a fountain here you could [drink fountain] from, or you can [save] your game." .. gsDE)
        
        --Wash off the clay infection.
        else
            TL_SetProperty("Append", "A strange room. Despite being carved from stone like the rest of the basement, it was done with very different architecture. The is a fountain here. You could [wash] to clean the clay off yourself, or [save] your game." .. gsDE)
        end
    else
        TL_SetProperty("Append", "A strange room. Despite being carved from stone like the rest of the basement, it was done with very different architecture. You can [save] your game here." .. gsDE)
    end

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return

--Save checker.
elseif(gzTextVar.bIsStateCheckingSave) then
    gzTextVar.bIsAtSaveLocation = true
    return

-- |[Drinking]|
elseif(sString == "drink fountain") then
    
    --Handle the input.
    gbHandledInput = true
    
    --Non-human.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then
        if(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll") then
            TL_SetProperty("Append", "A plastic girl like you has no need for water." .. gsDE)
        elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
            TL_SetProperty("Append", "Water is dangerous for a clay girl. You decide not to drink it." .. gsDE)
        elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
            TL_SetProperty("Append", "A STATUE DOES NOT DRINK, A STATUE OBEYS. FIND THE INTRUDERS, BRING THEM TO THE LIGHT." .. gsDE)
        end
        
    --Crippled.
    elseif(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt to even reach into the fountain..." .. gsDE)
    
    --Player has more than half HP.
    elseif(gzTextVar.gzPlayer.iHP >= gzTextVar.gzPlayer.iHPMax / 2) then
        TL_SetProperty("Append", "The fountain's water tastes fine, but you don't feel any healthier from having some." .. gsDE)
    
    --Heal up.
    else
        gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax / 2
        TL_SetProperty("Append", "You take a drink from the fountain and feel a little better." .. gsDE)
        TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)        
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    return
end

-- |[Washing]|
--Removes clay infection if present.
if(sString == "wash") then
    
    --Handle the input.
    gbHandledInput = true
    
    --No clay infection.
    if(gzTextVar.gzPlayer.iClayInfection < 0 or gzTextVar.gzPlayer.sFormState ~= "Human") then
        TL_SetProperty("Append", "There is no particular need to wash yourself right now." .. gsDE)
    
    --Clay infection.
    else
        gzTextVar.gzPlayer.iClayInfection = -1
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
        TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
        TL_SetProperty("Append", "It takes some effort, but the clay comes off in the water. It disintegrates on contact, dissolving to nothing..." .. gsDE)
    end
    return
end
