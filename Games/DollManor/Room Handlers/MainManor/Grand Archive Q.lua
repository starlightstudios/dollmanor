--[Arguments]
--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--[Trap Activity Check]
gzTextVar.bStringTrapActiveN = false
gzTextVar.bStringTrapActiveE = true
gzTextVar.bStringTrapActiveS = true
gzTextVar.bStringTrapActiveW = false
if(gzTextVar.bStringTrapActivityCheck == true) then
    return
end

--[Normal Functionality]
--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Call the special scripts.
LM_ExecuteScript(gzTextVar.sStringExaminePath, sString, "null", 17)