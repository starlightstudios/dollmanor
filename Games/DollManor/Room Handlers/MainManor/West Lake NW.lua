--[Arguments]
--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--[Handlers]
--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description as human.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.gzPlayer.iClayInfection == -1) then
            TL_SetProperty("Append", "The edge of an underground lake. The water is clear and deep. You can't see the bottom, it goes down a long way." .. gsDE)
        
        --Wash off the clay infection.
        else
            TL_SetProperty("Append", "The edge of an underground lake. The water is clear and deep. You could [wash] to clean the clay off yourself." .. gsDE)
        end
    
    --All non-human forms.
    else
        TL_SetProperty("Append", "The edge of an underground lake. The water is clear and deep. You can't see the bottom, it goes down a long way." .. gsDE)
    end

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return
end

--[Washing]
--Removes clay infection if present.
if(sString == "wash") then
    
    --Handle the input.
    gbHandledInput = true
    
    --No clay infection.
    if(gzTextVar.gzPlayer.iClayInfection < 0 or gzTextVar.gzPlayer.sFormState ~= "Human") then
        TL_SetProperty("Append", "There is no particular need to wash yourself right now." .. gsDE)
    
    --Clay infection.
    else
        gzTextVar.gzPlayer.iClayInfection = -1
        gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
        TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
        TL_SetProperty("Append", "It takes some effort, but the clay comes off in the water. It disintegrates on contact, dissolving to nothing..." .. gsDE)
    end
    return
end
