-- |[Arguments]|
--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

-- |[Handlers]|
--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        if(gzTextVar.bRungBell == false) then
            TL_SetProperty("Append", "The top of the belltower. There is an enormous bronze bell here. You could [ring] it." .. gsDE)
        else
            TL_SetProperty("Append", "The top of the belltower. There is an enormous bronze bell here. Ringing it seems to have neutralized the stranger, for now." .. gsDE)
        end
    else
        TL_SetProperty("Append", "The top of the belltower. There is an enormous bronze bell here." .. gsDE)
    end

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)

--Locality Builder.
elseif(gzTextVar.bIsLocalityCheck) then

    --When human, you can ring the bell.
    if(gzTextVar.gzPlayer.sFormState == "Human" and gzTextVar.bRungBell == false) then
        TL_SetProperty("Register Locality Entry", "ring", "Commands", "INSTANT")
    end

--Save checker.
elseif(gzTextVar.bIsStateCheckingSave) then
    return
    
-- |[Ring]|
--Ring the bell. This defeats the stranger and gives you the book you need.
elseif(sString == "ring") then

    --Handle the input.
    gbHandledInput = true
    
    --Blocker.
    gzTextVar.bRungBell = false
    if(gzTextVar.bRungBell == true) then
        TL_SetProperty("Append", "You have already rung the bell. The stranger is defeated for the time being." .. gsDE)
        return
    end
    
    --Can't ring it.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then
        TL_SetProperty("Append", "You have no desire to ring the bell." .. gsDE)
        TL_SetProperty("Create Blocker")
        return
    
    --Ring it!
    else
    
        --Create an indicator.
        local fX, fY, fZ = fnGetRoomPosition("Belltower Upper NE")
        TL_SetProperty("Register Entity Indicator", fX, fY, fZ, "SpcStranger", 17, 4, ciCodeUnfriendly)
        TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, "SpcStranger", ciHostilityX_Enemy, ciHostilityY_Enemy)
    
        --Cutscene.
        gzTextVar.bRungBell = true
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "Following Sarah's suggestion, you search for the book. It's not here, there aren't a lot of places to hide." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", gzTextVar.zStranger.sImgPath, ciImageLayerDefault)
        TL_SetProperty("Append", "The stranger is here. She has found you, and you won't be getting away. You think quickly." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Indicator To Location.lua", 3, "SpcStranger", "Belltower Upper NE", "Belltower Upper C")
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "She's made of glass. You throw your weight into the bell, ringing it as loud as you can." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", gzTextVar.zStranger.sImgPath, ciImageLayerDefault)
        TL_SetProperty("Append", "The stranger clutches at her 'ears', or rather the sides of the mask. Cracks appear in what little of her body you can see." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "She collapses. You follow it up with a kick, and the glass shatters. The mask lies atop a mount of glass shards, covered in her trench coat." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Indicator To Location.lua", 3, "SpcStranger", "Belltower Upper C", "Unregister")
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "Who knows how long it will be before she gets back up? You can't say." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "Within the bell, as it rang, a book fell out. Sarah was right. This is what you need, and the stranger was guarding it." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "Now all you need to do is make it back to Sarah with the book." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        --Spawn the book.
        fnRegisterObject(gzTextVar.gzPlayer.sLocation, "empty storybook", "empty storybook", true, gzTextVar.sRootPath .. "Item Handlers/Books/Empty Storybook.lua")
        fnBuildLocalityInfo()
        
        --Destroy the stranger.
        for i = 1, gzTextVar.zEntitiesTotal, 1 do
            if(gzTextVar.zEntities[i].sDisplayName == "Stranger") then
                fnRemoveEntity(i)
                break
            end
        end
        
    end

end