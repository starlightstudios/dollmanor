-- |[ ==================================== System Variables ==================================== ]|
--Creates all variables and constants that the rest of the scenario depends on. Everything should
-- be stored in gzTextVar to prevent conflicts with Adventure Mode.
TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "100 Input Handler Script.lua")

-- |[ ===================================== Music and Audio ==================================== ]|
-- |[Music Handling]|
--Register the ambient music tracks. This is done in the launcher to allow the music to fade out in the menu.
--TL_SetProperty("Register Music", "Doll|Ambient|Basement", "Ambient_Basement", 0.0)
--TL_SetProperty("Register Music", "Doll|Ambient|Interior", "Ambient_Interior", 0.0)
--TL_SetProperty("Register Music", "Doll|Ambient|Rain",     "Ambient_Rain", 0.0)

--Create packages to be quickloaded into the rooms as they are created. These allow standardization of
-- audio volumes across many rooms.
gzTextVar.zAudioPack_NoAudio      = {0.0, 0.0, 0.0}
gzTextVar.zAudioPack_Outside      = {0.0, 0.3, 0.9}
gzTextVar.zAudioPack_OutsideCabin = {0.0, 0.3, 0.5}
gzTextVar.zAudioPack_Inside       = {0.0, 1.0, 0.2}
gzTextVar.zAudioPack_Basement     = {0.7, 0.0, 0.0}
gzTextVar.zAudioPack_InsideRain   = {0.0, 0.0, 0.3}

--Music package lookups. These names appear in Tiled under the "Audio" property.
gzTextVar.zAudioLookups = {}
gzTextVar.zAudioLookups[1] = {"None",         gzTextVar.zAudioPack_NoAudio}
gzTextVar.zAudioLookups[2] = {"Outside",      gzTextVar.zAudioPack_Outside}
gzTextVar.zAudioLookups[3] = {"OutsideCabin", gzTextVar.zAudioPack_OutsideCabin}
gzTextVar.zAudioLookups[4] = {"Inside",       gzTextVar.zAudioPack_Inside}
gzTextVar.zAudioLookups[5] = {"Basement",     gzTextVar.zAudioPack_Basement}
gzTextVar.zAudioLookups[6] = {"InsideRain",   gzTextVar.zAudioPack_InsideRain}

--Initial music volume.
TL_SetProperty("Modify Music", "Ambient_Basement", gzTextVar.zAudioPack_Outside[1])
TL_SetProperty("Modify Music", "Ambient_Interior", gzTextVar.zAudioPack_Outside[2])
TL_SetProperty("Modify Music", "Ambient_Rain",     gzTextVar.zAudioPack_Outside[3])

-- |[ ================================== Rooms and Connections ================================= ]|
--All rooms are store in this table.
gzTextVar.iRoomsTotal = 0
gzTextVar.zRoomList = {}

--List of rooms that an AI can "wander" to. Some rooms are off-limits.
gzTextVar.iRandomPathsTotal = 0
gzTextVar.saValidRandomPaths = {}

--Constant version of the room list, stores pathing information.
gzTextCon.zRoomList = {}

--List of locations Jessie and Lauren can flee to if Mary is transformed.
gzTextVar.saFleeLocations = {}

--Auto-generated indicator handles.
gzTextCon.iAutoGenIndicators = 0

-- |[ ================================ State Machine Variables ================================= ]|
--Save Handler
gzTextVar.bIsSaveOverwrite = false
gzTextVar.bIsSaveGlyphUsingNotOverwrite = false
gzTextVar.sSaveOverwritePath = "Null"

--State Machine
gzTextVar.bIsExaminationCheck = false
gzTextVar.bIsLocalityCheck = false
gzTextVar.bIsPriorityCheck = false
gzTextVar.iPriorityResponse = -1
gzTextVar.bIsEquipmentCheck = false
gzTextVar.iItemEmulation = 0
gzTextVar.bIsClaygirlTrapCheck = false
gzTextVar.saClaygirlTrapList = {}
gzTextVar.bEnemySpawnsAtHalfHealth = false
gzTextVar.bCombatActivatedThisTick = false
gzTextVar.iAtkBonus = 0
gzTextVar.iDefBonus = 0
gzTextVar.iStartShieldBonus = 0
gzTextVar.iFireBonus = 0
gzTextVar.iWaterBonus = 0
gzTextVar.iWindBonus = 0
gzTextVar.iEarthBonus = 0
gzTextVar.iLifeBonus = 0
gzTextVar.iDeathBonus = 0

--Spotting the Player
gzTextVar.bIsPostTurnSpotting = false
gzTextVar.sSpottedPlayerAt = "Null"
gzTextVar.bOpenAndMoveInOneTurn = false

--Item Priorities
gzTextVar.iaItemPriorities = {}
gzTextVar.iaItemPriorities.iJournal = 1000
gzTextVar.iaItemPriorities.iKeys = 900
gzTextVar.iaItemPriorities.iConsumables = 800
gzTextVar.iaItemPriorities.iWeapons = 700
gzTextVar.iaItemPriorities.iArmors = 600
gzTextVar.iaItemPriorities.iGloves = 500
gzTextVar.iaItemPriorities.iBooks = 400

--Indicator Builder: Set the flag to true, and any item scripts called will populate the X/Y
-- positions with their indicator tiles. -1's indicate the item has no indicator.
gzTextVar.bIsBuildingIndicatorInfo = false
gzTextVar.iGlobalIndicatorX = -1
gzTextVar.iGlobalIndicatorY = -1

--Stack Builder: Set this flag to true, and item scripts will populate the flag with whether
-- or not they can stack in the inventory.
gzTextVar.bIsCheckingStacking = false
gzTextVar.bCanStack = false

--Placeholders. These get replaced by the scenario builder.
gzTextVar.sVariableKeyDoor = "C Variable A"

--Door Lock Indicators
gzTextVar.iLock_None_X = -1
gzTextVar.iLock_None_Y = -1
gzTextVar.iLock_Heart_X = 10
gzTextVar.iLock_Heart_Y = 2
gzTextVar.iLock_Club_X = 11
gzTextVar.iLock_Club_Y = 2
gzTextVar.iLock_Diamond_X = 12
gzTextVar.iLock_Diamond_Y = 2
gzTextVar.iLock_Spade_X = 13
gzTextVar.iLock_Spade_Y = 2
gzTextVar.iLock_Small_X = 14
gzTextVar.iLock_Small_Y = 2

--Combat Mode
if(gzTextVar.sDifficulty == nil) then gzTextVar.sDifficulty = "Normal" end
if(gzTextVar.bIsCombatWaitMode == nil) then gzTextVar.bIsCombatWaitMode = false end
gzTextVar.bIsCombatTypingBased = false

--Claygirl Information. These are what items claygirls can legally disguise as.
gzTextVar.iClaygirlDisguisesTotal = 0
gzTextVar.saClaygirlDisguises = {}

-- |[AI Variables]|
--Activity Variables
gzTextVar.iCurrentEntity = -1

--Global variable for checking if an entity is hostile. If it is, it gets removed after Mary wins
-- a fight. Entities that do NOT get removed do NOT flag this!
gzTextVar.bLastHostilityCheck = false

--State machine variable. Used to trigger normal combat routines during an AI call.
gzTextVar.bIsTriggeringFights = false

-- |[ ===================================== C++ Constants ====================================== ]|
-- |[System Constants]|
--Image Properties
gzTextVar.fImgStdDepth = -0.250000
gzTextVar.fImgStdDepthInc = -0.000001
gzTextVar.fStdYOffset = -100

--Image Display
ciImageLayersMax = 8
ciImageLayerDefault = 0
ciImageLayerSkin = 0
ciImageLayerOverlay = 1

gciOverlay_Exclusion = 0
gciOverlay_Normal = 1
gciOverlay_Bypass = 2

--Starlight Flip Flags
ciNoMod = 0
ciHFlip = 1
ciVFlip = 2
ciHVFlip = ciHFlip + ciVFlip
ciRotate90 = 4
ciRotate180 = 8
ciRotate270 = 16
ciIsHalfSize = 32

-- |[Inventory Constants]|
--Equipment Slots.
ciEquipSlotWeapon = 0
ciEquipSlotArmor = 1
ciEquipSlotGloves = 2

-- |[World Constants]|
--Map Sizes
gzTextVar.fMapDistance = 1.0

--Animations that display over tiles.
TL_SetProperty("Register Animation", "Rain", 5, 0, 14, 24, 12)
TL_SetProperty("Register Animation", "Altar Torches", 5, 12, 14, 4, 4)

--Visibility Override
gzTextCon.iDirNorth = 0
gzTextCon.iDirEast = 2
gzTextCon.iDirSouth = 4
gzTextCon.iDirWest = 6
gzTextCon.iNoVisOverride = 0
gzTextCon.iAlwaysVisible = 1
gzTextCon.iAlwaysVisibleNarrow = 2
gzTextCon.iAlwaysVisibleShort = 3
gzTextCon.iNeverVisible = 4

-- |[Indicator Constants]|
--Priorities. Used to sort indicators by location when many are on the same tile.
ciPriorityPlayer = 0
ciPriorityFriendly = 1
ciPriorityEnemy = -1
ciPrioritySound = -2

--Door Slots and Information.
ciTilesetDoorX = 10
ciTilesetDoorY = 3
ciSlotDoorSClosed = 4
ciSlotDoorSOpen = 5
ciSlotDoorEClosed = 6
ciSlotDoorEOpen = 7

--Indicator Codes. Based on the AMTL_PRIORITY_ series.
ciCodePlayer = 0
ciCodeFriendly = 1
ciCodeUnfriendly = -1
ciCodeItem = 2

--Hosility
ciHostilityX_None = -1
ciHostilityY_None = -1
ciHostilityX_Enemy = 9
ciHostilityY_Enemy = 0
ciHostilityX_Boss = 10
ciHostilityY_Boss = 0

--Rotation Lookups
ciLookupsTotal = 8
ciLookups = {}
ciLookups[1] = { 0, -1} --N
ciLookups[2] = { 1, -1} --NE
ciLookups[3] = { 1,  0} --E
ciLookups[4] = { 1,  1} --SE
ciLookups[5] = { 0,  1} --S
ciLookups[6] = {-1,  1} --SW
ciLookups[7] = {-1,  0} --W
ciLookups[8] = {-1, -1} --NW

-- |[Combat Constants]|
--Elemental flags. Can be anded together.
gciElement_None = 0
gciElement_Fire = 1
gciElement_Water = 2
gciElement_Wind = 4
gciElement_Earth = 8
gciElement_Life = 16
gciElement_Death = 32

--Elemental slots. Used for computing weaknesses. 0 is physical, but that is ignore
-- when computing bonuses.
gciElementSlot_Physical = 0
gciElementSlot_Fire = 1
gciElementSlot_Water = 2
gciElementSlot_Wind = 3
gciElementSlot_Earth = 4
gciElementSlot_Life = 5
gciElementSlot_Death = 6

--Types of Cards
gciCardType_Action = 0
gciCardType_Modifier = 1
gciCardType_Of = 2
gciCardType_And = 3

--Card Effects
gciCardEffect_None = 0
gciCardEffect_Damage = 0
gciCardEffect_Defend = 1
gciCardEffect_Heal = 2

--Deck Editor Types
gciDeckEd_Water = 0
gciDeckEd_Fire = 1
gciDeckEd_Wind = 2
gciDeckEd_Earth = 3
gciDeckEd_Life = 4
gciDeckEd_Death = 5
gciDeckEd_Attack = 6
gciDeckEd_Defend = 7
gciDeckEd_BridgeAnd = 8
gciDeckEd_BridgeOf = 9

--Deck Editor Stat Bonus Codes
gciDeckEd_Stat_Attack = 0
gciDeckEd_Stat_Defend = 1
gciDeckEd_Stat_StartShield = 2
gciDeckEd_Stat_BonusWater = 3
gciDeckEd_Stat_BonusFire = 4
gciDeckEd_Stat_BonusWind = 5
gciDeckEd_Stat_BonusEarth = 6
gciDeckEd_Stat_BonusLife = 7
gciDeckEd_Stat_BonusDeath = 8

--Deck Editor Help Codes
gciDeckEd_Help_DeckSize = 23
gciDeckEd_Help_Water = 24
gciDeckEd_Help_Fire = 25
gciDeckEd_Help_Wind = 26
gciDeckEd_Help_Earth = 27
gciDeckEd_Help_Life = 28
gciDeckEd_Help_Death = 29
gciDeckEd_Help_Attack = 30
gciDeckEd_Help_Defend = 31
gciDeckEd_Help_BridgeAnd = 32
gciDeckEd_Help_BridgeOf = 33
gciDeckEd_Help_Stats = 34
gciDeckEd_Help_ElementBonus = 35

-- |[Path Constants]|
gzTextVar.sRoomHandlers = gzTextVar.sRootPath .. "Room Handlers/MainManor/"
gzTextVar.sEntityHandlers = gzTextVar.sRootPath .. "Entity Handlers/"
gzTextVar.sTurnEndScript = gzTextVar.sRootPath .. "400 Turn End.lua"
gzTextVar.sExamineHandler = gzTextVar.sRootPath .. "Item Handlers/Examinables.lua"
gzTextVar.sThinkHandler = gzTextVar.sRootPath .. "201 Think Handler.lua"
gzTextVar.sCombatHandler = gzTextVar.sRootPath .. "Combat Handlers/100 Combat Handler.lua"

--Item Standard Scripts
gzTextVar.sStandardTake = gzTextVar.sRootPath .. "Item Handlers/ZStandardTake.lua"

--Modifying Indicators
gzTextVar.sModifyHostility = gzTextVar.sRootPath .. "Functions/Change Monster Hostility.lua"

-- |[ ================================== Sound and Visibility ================================== ]|
--Stores room indexes which currently contain a sound
gzTextVar.zaSoundList = {} 

--Visibility
gzTextVar.bUseSimpleSight = false
gzTextVar.iMaxSight = 3
gzTextVar.bSpottedPlayer = false
gzTextVar.bHasGlobalSight = false
ciDoNotRenderUnder = -1000
ciModifyVisibility = 0
ciDoNotModifyVisibility = 1

--Prebuilt Lookups for Visibility Computations
ciVisLookupsTotal = 28
czVisLookups = {}
czVisLookups[ 1] = {"N", "N", "N"}     
czVisLookups[ 2] = {"N", "N", "E", "N"}
czVisLookups[ 3] = {"N", "E", "N", "N"}
czVisLookups[ 4] = {"N", "E", "N", "E"}
czVisLookups[ 5] = {"E", "N", "E", "N"}
czVisLookups[ 6] = {"E", "E", "N", "E"}
czVisLookups[ 7] = {"E", "N", "E", "E"}
czVisLookups[ 8] = {"E", "E", "E"}
czVisLookups[ 9] = {"E", "S", "E", "E"}
czVisLookups[10] = {"E", "E", "S", "E"}
czVisLookups[11] = {"S", "E", "S", "E"}
czVisLookups[12] = {"E", "S", "E", "S"}
czVisLookups[13] = {"S", "E", "S", "S"}
czVisLookups[14] = {"S", "S", "E", "S"}
czVisLookups[15] = {"S", "S", "S"}    
czVisLookups[16] = {"S", "W", "S", "S"}
czVisLookups[17] = {"S", "S", "W", "S"} 
czVisLookups[18] = {"S", "W", "S", "W"}
czVisLookups[19] = {"W", "S", "W", "S"}
czVisLookups[20] = {"W", "S", "W", "W"}
czVisLookups[21] = {"W", "W", "S", "W"}
czVisLookups[22] = {"W", "W", "W"}
czVisLookups[23] = {"W", "N", "W", "W"}
czVisLookups[24] = {"W", "W", "N", "W"}
czVisLookups[25] = {"N", "W", "N", "W"}
czVisLookups[26] = {"W", "N", "W", "N"}
czVisLookups[27] = {"N", "N", "W", "N"}
czVisLookups[28] = {"N", "W", "N", "N"}

-- |[ ======================================= Functions ======================================== ]|
--Basic functions with no dependencies.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnAddFleeLocation.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnBuildLocalityInfo.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnCutsceneBlocker.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnCutsceneInstruction.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnCutsceneWait.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnDialogueCutscene.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetDelimitedString.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetIndexOfItemInInventory.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetRoomIndexFromString.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetRoomIndex.lua") --Relies on fnGetRoomIndexFromString()
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetRoomPosition.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGenerateIndicatorName.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnNoReadCheck.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRandomFootstep.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRemoveEntity.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRemoveItemFromInventory.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRemoveItemFromRoom.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnListEntities.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnMarkMinimapForPosition.lua")

--Functions that call other functions.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetIndexOfItemInRoom.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnGetMovePath.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnListObjects.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnCheckKeyStates.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRebuildEntityVisibility.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnRegisterObject.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnMarkLockAsKnown.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnMoveToRandomLocation.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnClaygirl.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnDoll.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnWeakDoll.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnTitan.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnGoop.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnStranger.lua")

--Third layer of dependency.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnDollTutPatrol.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnDollSentry.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnDollSentryCombat.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnSpawnDollSentryCombatB.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnCreateJournalPage.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/fnDropItem.lua")
