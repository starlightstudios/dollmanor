-- |[ ======================================= Deck Handler ======================================== ]|
--Script to handle modification of the player's deck. Also has the help text.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

-- |[Deck Help String]|
--Base.
if(sString == "help deck") then
    
    --Flag
    gbHandledInput = true
    
    --Display.
    TL_SetProperty("Append", "Your deck is how you defend yourself, consisting of attack, defense, elemental, and attachment cards." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "Some items you find may also unlock new cards. You may also want to focus your deck as some enemies are vulnerable to certain elements." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "Attack cards allow you to damage the enemy. Defense cards provide shields which will absorb damage instead of your health." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "Elemental cards allow you to deal elemental damage or create elemental shields. The more elements attached, the more damage an attack does, or the more shields it creates." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "Elemental shields have the advantage of completely stopping an attack of that type. A fire attack that normally deals 5 damage will deal 1 damage to a fire shield." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "Attachment cards are [and] and [of]. These allow you to add multiple elemental cards to the same action, increasing the effect." .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "The strongest possible sentence is [element] [and] [element] [and] [element] [action] [of] [element]. Plan accordingly!" .. gsDE)
    TL_SetProperty("Create Blocker")
                    
    TL_SetProperty("Append", "You can use the [deck] command to open the deck editor. This allows you to edit how many cards of each type are in your deck." .. gsDE)
    TL_SetProperty("Create Blocker")

-- |[Deck Editor]|
elseif(sString == "deck") then
    gbHandledInput = true
    
    --Activate.
    TL_SetProperty("Activate Deck Editor")
    TL_SetProperty("Set Deck Editor Post Exec", fnResolvePath() .. "105 Deck Post Exec.lua")
    TL_SetProperty("Set Deck Editor Help Exec", fnResolvePath() .. "121 Deck Help.lua")
    
    --Set sizes.
    TL_SetProperty("Set Deck Size Range", gzTextVar.gzPlayer.zDeck.iMinimum, gzTextVar.gzPlayer.zDeck.iMaximum)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Water,     gzTextVar.gzPlayer.zDeck.iWaterCards,  0, gzTextVar.gzPlayer.zDeck.iMaxWaterCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Fire,      gzTextVar.gzPlayer.zDeck.iFireCards,   0, gzTextVar.gzPlayer.zDeck.iMaxFireCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Wind,      gzTextVar.gzPlayer.zDeck.iWindCards,   0, gzTextVar.gzPlayer.zDeck.iMaxWindCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Earth,     gzTextVar.gzPlayer.zDeck.iEarthCards,  0, gzTextVar.gzPlayer.zDeck.iMaxEarthCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Life,      gzTextVar.gzPlayer.zDeck.iLifeCards,   0, gzTextVar.gzPlayer.zDeck.iMaxLifeCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Death,     gzTextVar.gzPlayer.zDeck.iDeathCards,  0, gzTextVar.gzPlayer.zDeck.iMaxDeathCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Attack,    gzTextVar.gzPlayer.zDeck.iAttackCards, 1, gzTextVar.gzPlayer.zDeck.iMaxAttackCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_Defend,    gzTextVar.gzPlayer.zDeck.iDefendCards, 0, gzTextVar.gzPlayer.zDeck.iMaxDefendCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_BridgeAnd, gzTextVar.gzPlayer.zDeck.iAndCards,    0, gzTextVar.gzPlayer.zDeck.iMaxAndCards)
    TL_SetProperty("Set Hand Counts", gciDeckEd_BridgeOf,  gzTextVar.gzPlayer.zDeck.iOfCards,     0, gzTextVar.gzPlayer.zDeck.iMaxOfCards)
    
    --Recompute stats, upload.
    fnComputePlayerStatBonuses()
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_Attack, gzTextVar.iAtkBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_Defend, gzTextVar.iDefBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_StartShield, gzTextVar.iStartShieldBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_BonusWater, gzTextVar.iWaterBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_BonusFire, gzTextVar.iFireBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_BonusWind, gzTextVar.iWindBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_BonusEarth, gzTextVar.iEarthBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_BonusLife, gzTextVar.iLifeBonus)
    TL_SetProperty("Set Bonus", gciDeckEd_Stat_BonusDeath, gzTextVar.iDeathBonus)
    TL_SetProperty("Set Sentence Bonus", "+1/Modifier")

--[=[
--Deck Show
elseif(sString == "help deck show") then
    
    --Flag
    gbHandledInput = true
    
    --Display.
    TL_SetProperty("Append", "[deck show] will show you how many cards of each type are currently in your deck." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Which cards you get after shuffling are randomized. Choose carefully, make sure you don't have too many of one card and not enough of another." .. gsDE)
    TL_SetProperty("Create Blocker")

--Deck Set 
elseif(sString == "help deck set") then
    
    --Flag
    gbHandledInput = true
    
    --Display.
    TL_SetProperty("Append", "[deck set 'type' 'number'] allows you to change how many of a given card type you have in your deck." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There are maximums for each type of card. You can see these maximums with [deck show]." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The 'type' should be one of [attack], [defend], [fire], [water], [wind], [earth], [life], [death], [or], or [and]." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Example command: [deck set fire 5] will set your deck to have 5 fire cards in it." .. gsDE)
    TL_SetProperty("Create Blocker")

--Deck Ace 
elseif(sString == "help deck ace") then
    
    --Flag
    gbHandledInput = true
    
    --Display.
    TL_SetProperty("Append", "[deck ace] will show you your current Ace card. Ace cards appear in the bottom left of the combat screen and are not randomized." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your Ace card is more powerful than normal cards and is readily available, but takes some time to respawn when used." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your Ace card can be anything except an [and] or [of] card. It can be an attack, defense, or any elemental card." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "However, you will not be able to use the [life] or [death] elementals as an Ace until you unlock them." .. gsDE)
    TL_SetProperty("Create Blocker")

--Deck Ace Set
elseif(sString == "help deck ace set") then
    
    --Flag
    gbHandledInput = true
    
    --Display.
    TL_SetProperty("Append", "[deck ace set 'type'] allows you to change your Ace card." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The 'type' should be one of [attack], [defend], [fire], [water], [wind], [earth], [life], or [death]." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You will not be able to use the [life] or [death] elementals as an Ace until you unlock them." .. gsDE)
    TL_SetProperty("Create Blocker")

--[Deck Show]
--Displays the player's deck properties.
elseif(sString == "deck show") then
    
    --Flag
    gbHandledInput = true
    
    --Compute total.
    local iTotalCards = 0
    local iLen = #gzTextVar.gzPlayer.zDeck.iaCardArray
    for i = 1, iLen, 1 do
        iTotalCards = iTotalCards + gzTextVar.gzPlayer.zDeck.iaCardArray[i]
    end
    
    --Arrays.
    local saNames = {"Attack", "Defend", "Fire", "Water", "Earth", "Wind", "Life", "Death", "Of", "And"}
    
    --Display.
    for i = 1, iLen, 2 do
        
        --Left side.
        local sString = "[" .. saNames[i] .. "]"
        TL_SetProperty("Append", sString)
        
        sString = "[POS:085]cards:"
        TL_SetProperty("Append", sString)
        
        sString = "[POS:150]" .. gzTextVar.gzPlayer.zDeck.iaCardArray[i]
        TL_SetProperty("Append", sString)
        
        sString = "[POS:170](Max: " .. gzTextVar.gzPlayer.zDeck.iaMaxCardArray[i] .. ")"
        TL_SetProperty("Append", sString)
        
        --Right side.
        sString = "[POS:300][" .. saNames[i+1] .. "]"
        TL_SetProperty("Append", sString)
        
        sString = "[POS:385]cards:"
        TL_SetProperty("Append", sString)
        
        sString = "[POS:450]" .. gzTextVar.gzPlayer.zDeck.iaCardArray[i+1]
        TL_SetProperty("Append", sString)
        
        sString = "[POS:470](Max: " .. gzTextVar.gzPlayer.zDeck.iaMaxCardArray[i+1] .. ")"
        TL_SetProperty("Append", sString)
    end
    TL_SetProperty("Append", "\n")

    --Total.
    TL_SetProperty("Append", "You have " .. iTotalCards .. " cards in your deck total." .. gsDE)

--[Deck Set]
--Player can change their deck configuration.
elseif(string.sub(sString, 1, 8) == "deck set") then
    
    --[Setup]
    --Flag
    gbHandledInput = true
    
    --Setup
    local iTargetLetter = 10
    local iLen = string.len(sString)
    
    --Variables.
    local sType = ""
    local sQuantity = ""
    local iQuantity = 0
    local iUseCap = 0
    
    --[Type Resolver]
    --Resolve type.
    for i = 10, iLen, 1 do
        
        --Letter
        iTargetLetter = i
        local sLetter = string.sub(sString, i, i)
        
        --Break on spaces.
        if(sLetter == " ") then
            break
        --Otherwise, append.
        else
            sType = sType .. sLetter
        end
    end
    
    --Error check: Make sure it's a valid type!
    sType = string.lower(sType)
    local iIndex = -1
    if(sType == "attack") then
        iIndex = 1
    elseif(sType == "defend") then
        iIndex = 2
    elseif(sType == "fire") then
        iIndex = 3
    elseif(sType == "water") then
        iIndex = 4
    elseif(sType == "wind") then
        iIndex = 5
    elseif(sType == "earth") then
        iIndex = 6
    elseif(sType == "life") then
        iIndex = 7
    elseif(sType == "death") then
        iIndex = 8
    elseif(sType == "of") then
        iIndex = 9
    elseif(sType == "and") then
        iIndex = 10
    end

    --Index is out of range.
    if(iIndex == -1) then
        TL_SetProperty("Append", "I was unable to determine the type of card. Valid types are [attack], [defend], [fire], [water], [wind], [earth], [life], [death], [of], and [and]." .. gsDE)
        return
    end

    --[Quantity Resolver]
    --Resolve quantity.
    iTargetLetter = iTargetLetter + 1
    for i = iTargetLetter, iLen, 1 do
        
        --Letter
        iTargetLetter = i
        local sLetter = string.sub(sString, i, i)
        
        --Break on spaces.
        if(sLetter == " ") then
            break
        --Otherwise, append.
        else
            sQuantity = sQuantity .. sLetter
        end
    end
    
    --Convert to a numeric.
    iQuantity = tonumber(sQuantity)
    
    --[Setter]
    --Clamp.
    iUseCap = gzTextVar.gzPlayer.zDeck.iaMaxCardArray[iIndex]
    if(iQuantity < 0) then iQuantity = 0 end
    if(iQuantity > iUseCap) then iQuantity = iUseCap end
    
    --Report.
    TL_SetProperty("Append", "Setting your [" .. sType .. "] card count to " .. iQuantity .. "." .. gsDE)
    
    --Set.
    if(sType == "attack") then
        gzTextVar.gzPlayer.zDeck.iAttackCards = iQuantity
    elseif(sType == "defend") then
        gzTextVar.gzPlayer.zDeck.iDefendCards = iQuantity
    elseif(sType == "fire") then
        gzTextVar.gzPlayer.zDeck.iFireCards = iQuantity
    elseif(sType == "water") then
        gzTextVar.gzPlayer.zDeck.iWindCards = iQuantity
    elseif(sType == "wind") then
        gzTextVar.gzPlayer.zDeck.iAttackCards = iQuantity
    elseif(sType == "earth") then
        gzTextVar.gzPlayer.zDeck.iEarthCards = iQuantity
    elseif(sType == "life") then
        gzTextVar.gzPlayer.zDeck.iLifeCards = iQuantity
    elseif(sType == "death") then
        gzTextVar.gzPlayer.zDeck.iDeathCards = iQuantity
    elseif(sType == "of") then
        gzTextVar.gzPlayer.zDeck.iOfCards = iQuantity
    elseif(sType == "and") then
        gzTextVar.gzPlayer.zDeck.iAndCards = iQuantity
    end
    gzTextVar.gzPlayer.zDeck.iaCardArray[iIndex] = iQuantity


--[Deck Ace]
--Displays the player's Ace card.
elseif(sString == "deck ace") then

    --Flag
    gbHandledInput = true
    
    --Action. Attack or Defend cards.
    if(gzTextVar.gzPlayer.zDeck.iAceType == gciCardType_Action) then
        
        --Type. Damage first.
        if(gzTextVar.gzPlayer.zDeck.iAceEffect == gciCardEffect_Damage) then
            TL_SetProperty("Append", "Your Ace card is the attack card 'Batter' which deals +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " damage when used." .. gsDE)
            
        --Defense cards.
        else
            TL_SetProperty("Append", "Your Ace card is the defense card 'Shield' which creates +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " shields when used." .. gsDE)
        end
    
    --Elemental cards.
    else
    
        --Fire:
        if(gzTextVar.gzPlayer.zDeck.iAceElement == gciElement_Fire) then
            TL_SetProperty("Append", "Your Ace card is the fire elemental card 'Incinerating' which provides +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " fire damage/shields when used." .. gsDE)
        
        --Water:
        elseif(gzTextVar.gzPlayer.zDeck.iAceElement == gciElement_Water) then
            TL_SetProperty("Append", "Your Ace card is the water elemental card 'Drowning' which provides +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " water damage/shields when used." .. gsDE)
        
        --Wind:
        elseif(gzTextVar.gzPlayer.zDeck.iAceElement == gciElement_Wind) then
            TL_SetProperty("Append", "Your Ace card is the wind elemental card 'Surging' which provides +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " wind damage/shields when used." .. gsDE)
        
        --Earth:
        elseif(gzTextVar.gzPlayer.zDeck.iAceElement == gciElement_Earth) then
            TL_SetProperty("Append", "Your Ace card is the earth elemental card 'Sundering' which provides +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " earth damage/shields when used." .. gsDE)
        
        --Life:
        elseif(gzTextVar.gzPlayer.zDeck.iAceElement == gciElement_Life) then
            TL_SetProperty("Append", "Your Ace card is the life elemental card 'Flowering' which provides +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " life damage/shields when used." .. gsDE)
        
        --Death:
        elseif(gzTextVar.gzPlayer.zDeck.iAceElement == gciElement_Death) then
            TL_SetProperty("Append", "Your Ace card is the death elemental card 'Slaughtering' which provides +" .. gzTextVar.gzPlayer.zDeck.iAcePower .. " death damage/shields when used." .. gsDE)
            
        end
    end
    
--[Deck Ace Set]
--Changes the player's Ace card.
elseif(string.sub(sString, 1, 12) == "deck ace set") then
    
    --[Setup]
    --Flag
    gbHandledInput = true
    
    --Setup
    local iLen = string.len(sString)
    
    --Variables.
    local sType = ""
    
    --[Type Resolver]
    --Resolve type.
    for i = 14, iLen, 1 do
        
        --Letter
        local sLetter = string.sub(sString, i, i)
        
        --Break on spaces.
        if(sLetter == " ") then
            break
        --Otherwise, append.
        else
            sType = sType .. sLetter
        end
    end
    
    --[Setter]
    local iIndex = -1
    if(sType == "attack") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Action
        gzTextVar.gzPlayer.zDeck.iAceEffect = gciCardEffect_Damage
        TL_SetProperty("Append", "Setting Ace to attack card." .. gsDE)
    elseif(sType == "defend") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Action
        gzTextVar.gzPlayer.zDeck.iAceEffect = gciCardEffect_Defend
        TL_SetProperty("Append", "Setting Ace to defend card." .. gsDE)
    elseif(sType == "fire") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Modifier
        gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_Fire
        TL_SetProperty("Append", "Setting Ace to fire card." .. gsDE)
    elseif(sType == "water") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Modifier
        gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_Water
        TL_SetProperty("Append", "Setting Ace to water card." .. gsDE)
    elseif(sType == "wind") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Modifier
        gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_Wind
        TL_SetProperty("Append", "Setting Ace to wind card." .. gsDE)
    elseif(sType == "earth") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Modifier
        gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_Earth
        TL_SetProperty("Append", "Setting Ace to earth card." .. gsDE)
    elseif(sType == "life") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Modifier
        gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_Life
        TL_SetProperty("Append", "Setting Ace to life card." .. gsDE)
    elseif(sType == "death") then
        gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Modifier
        gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_Death
        TL_SetProperty("Append", "Setting Ace to death card." .. gsDE)
    else
        TL_SetProperty("Append", "I was unable to determine the type of Ace. Valid types are [attack], [defend], [fire], [water], [wind], [earth], [life], or [death]." .. gsDE)
    end
    ]=]
end