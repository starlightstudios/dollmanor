-- |[ ======================================= Boot Fonts ======================================= ]|
--Boots fonts used by String Tyrant.
if(gbBootedStringTyrantFonts == true) then return end
gbBootedStringTyrantFonts = true

-- |[Setup]|
--Paths.
local sFontPath         = fnResolvePath()
local sKerningPath      = "Data/Scripts/Fonts/"
local sLocalKerningPath = fnResolvePath()

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest  = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge     = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS = Font_GetProperty("Constant Precache With Special S")

-- |[ ===================================== Font Registry ====================================== ]|
--Mister Pixel 16
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Mister Pixel 16 DFO", sFontPath .. "MisterPixel.ttf", sLocalKerningPath .. "MisterPixel16_Kerning.lua", 16, ciFontNearest + ciFontEdge + ciFontDownfade)

--Oxygen
Font_SetProperty("Downfade", 0.95, 0.75)
Font_Register("Oxygen 27 DF", sFontPath .. "Oxygen-Regular.ttf", sLocalKerningPath .. "OxyReg27_Kerning.lua", 27, ciFontDownfade + ciFontSpecialS)

Font_SetProperty("Downfade", 0.95, 0.75)
Font_SetProperty("Outline Width", 1)
Font_Register("Oxygen 19 DFO", sFontPath .. "Oxygen-Regular.ttf", sLocalKerningPath .. "OxyReg19_Kerning.lua", 19, ciFontDownfade + ciFontEdge)

--Segoe
Font_Register("Segoe 20",      sFontPath .. "segoeui.ttf",  sLocalKerningPath .. "Segoe20_Kerning.lua", 20, ciFontNoFlags)
Font_Register("Segoe 30",      sFontPath .. "segoeui.ttf",  sLocalKerningPath .. "Segoe30_Kerning.lua", 30, ciFontNoFlags)
Font_Register("Segoe 40 Bold", sFontPath .. "segoeuib.ttf", sLocalKerningPath .. "Segoe40_Kerning.lua", 40, ciFontNoFlags)

--Sanchez
Font_SetProperty("Downfade", 0.90, 0.60)
Font_SetProperty("Outline Width", 3)
Font_Register("Sanchez 35 DFO", sFontPath .. "SanchezRegular.otf", sLocalKerningPath .. "Sanchez30_Kerning.lua", 35, ciFontNearest + ciFontEdge + ciFontDownfade)

--Trigger 28
Font_SetProperty("Downfade", 0.99, 0.60)
Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 DFO", sFontPath .. "TriggerModified.ttf", sLocalKerningPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge + ciFontDownfade)

Font_SetProperty("Outline Width", 1)
Font_Register("Trigger 28 O", sFontPath .. "TriggerModified.ttf", sLocalKerningPath .. "Trigger28_Kerning.lua", 28, ciFontNearest + ciFontEdge)

-- |[ ======================================== Aliases ========================================= ]|
--Mister Pixel 16
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Deck Editor Button")

--Oxygen 27 DF
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Main")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Decision")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "Deck Editor Main")

--Oxygen 19 DFO
Font_SetProperty("Add Alias", "Oxygen 19 DFO", "Deck Editor Small")

--Segoe 20
Font_SetProperty("Add Alias", "Segoe 20", "Text Level Medium")
Font_SetProperty("Add Alias", "Segoe 20", "String Tyrant Title Medium")
Font_SetProperty("Add Alias", "Segoe 20", "Word Combat Medium")
Font_SetProperty("Add Alias", "Segoe 20", "Typing Combat Medium")

--Segoe 30
Font_SetProperty("Add Alias", "Segoe 30", "Text Level Large")

--Segoe 40 Bold
Font_SetProperty("Add Alias", "Segoe 40 Bold", "String Tyrant Title Large")

--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "World Dialogue Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Word Combat Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Typing Combat Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Deck Editor Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Text Lev Options Header")

--Trigger 28
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Typing Combat Command")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Word Combat Cards")

--Trigger 28 NDF
Font_SetProperty("Add Alias", "Trigger 28 O", "Text Lev Options Main")
