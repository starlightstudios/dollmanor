--[ ======================================= Secret Inputs ======================================= ]
--Secondary input handler, does not report results or even get called if secret inputs is off.
-- The player can find out about these inputs from in-game resources.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--[Sample Battle]
--Used for testing. Enemy has a lot of HP and doesn't attack often.
if(sString == "mock battle") then
    gbHandledInput = true
    
    fnSpawnWeakDoll(gzTextVar.gzPlayer.sLocation, "Random Doll " .. string.char(gzTextVar.iDollSpawn), nil)
    gzTextVar.iDollSpawn = gzTextVar.iDollSpawn + 1
        
    TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, gzTextVar.zEntitiesTotal, "Standard", "Standard")
    return
end

--[TF Preference]
--If the player is still a human, they can change what transformation they'd prefer.
if(string.sub(sString, 1, 4) == "pref") then
    
    --Input handled.
    gbHandledInput = true
    
    --Cannot change preferences when not a human.
    if(gzTextVar.gzPlayer.sFormState ~= "Human") then
        TL_SetProperty("Append", "Cannot change preferences when not a human!" .. gsDE)
        return
    end
    
    --Secondary string.
    local sSecondary = string.sub(sString, 6)
    
    --Branch handler.
    if(sSecondary == "dancer") then
        TL_SetProperty("Append", "Doll Preference set to Dancer." .. gsDE)
        gzTextVar.gzPlayer.sTFSeq = "Dancer"
    elseif(sSecondary == "goth") then
        TL_SetProperty("Append", "Doll Preference set to Goth." .. gsDE)
        gzTextVar.gzPlayer.sTFSeq = "Goth"
    elseif(sSecondary == "punk") then
        TL_SetProperty("Append", "Doll Preference set to Punk." .. gsDE)
        gzTextVar.gzPlayer.sTFSeq = "Punk"
    
    --Error:
    else
        TL_SetProperty("Append", "Unknown preference. Valid entries are 'Dancer', 'Goth', and 'Punk'." .. gsDE)
    end

--[Show Everything]
elseif(sString == "reveal all") then
    
    --Input handled.
    gbHandledInput = true

    --No need to do it twice.
    if(gzTextVar.bHasGlobalSight == true) then return end
    
    --Flag on.
    gzTextVar.bHasGlobalSight = true

    --Run through all rooms and mark them as revealed.
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        
        --Position.
        local fNewX = gzTextVar.zRoomList[i].fRoomX
        local fNewY = gzTextVar.zRoomList[i].fRoomY
        local fNewZ = gzTextVar.zRoomList[i].fRoomZ
        
        --Mark.
        gzTextVar.zRoomList[i].bIsExplored = true
        TL_SetProperty("Set Room Explored", fNewX, fNewY, fNewZ, true)
        TL_SetProperty("Set Room Visible", fNewX, fNewY, fNewZ, true)

    end

    --Other.
    TL_SetProperty("Reresolve Fades")

--[Unlock All Doors]
elseif(sString == "unlock all") then
    gbHandledInput = true
    TL_SetProperty("Append", "All doors have been unlocked." .. gsDE)
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

--[Change Game Phase]
elseif(string.sub(sString, 1, 5) == "phase") then
    
    --Input handled.
    gbHandledInput = true
    
    --Secondary string.
    local sSecondary = string.sub(sString, 7)
    
    --Branch handler.
    if(sSecondary == "1") then
        TL_SetProperty("Append", "Setting to Phase 1." .. gsDE)
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Scenario Handlers/000 Set To Phase 1.lua", 0)
    elseif(sSecondary == "2") then
        TL_SetProperty("Append", "Setting to Phase 2." .. gsDE)
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Scenario Handlers/001 Set To Phase 2.lua", 0)
    
    --Error:
    else
        TL_SetProperty("Append", "Unidentified phase. Please enter [phase 1] or [phase 2]." .. gsDE)
    end

--[Spawner]
elseif(string.sub(sString, 1, 5) == "spawn") then

    --Input handled.
    gbHandledInput = true
    
    --Help printer function to centralize it.
    local fnPrintHelp = function()
        TL_SetProperty("Append", "Syntax: spawn [type] [location]\n")
        TL_SetProperty("Append", "Type is one of: doll, titan, dogpile, potion\n")
        TL_SetProperty("Append", "Location is the name of the room, the same one that displays when you enter it.\n")
        TL_SetProperty("Append", " You may also enter 'me' for the location, which will spawn it on you." .. gsDE)
    end
    
    --[Type Resolver]
    --Resolve type.
    local sTypeString, iLastLetter = fnGetDelimitedString(sString, 7)
    if(sTypeString ~= "doll" and sTypeString ~= "titan" and sTypeString ~= "potion"and sTypeString ~= "dogpile") then
        fnPrintHelp()
        return
    end
    
    --Location.
    local sLocation = string.sub(sString, iLastLetter + 2)
    if(sLocation == "") then
        fnPrintHelp()
        return
    end
    
    --Try to find the location. If the location is "disguise" then it's for claygirls only.
    if(sLocation ~= "disguise" and sLocation ~= "me") then
        local iRoomIndex = -1
        for i = 1, gzTextVar.iRoomsTotal, 1 do
            if(string.lower(gzTextVar.zRoomList[i].sName) == sLocation) then
                iRoomIndex = i
                sLocation = gzTextVar.zRoomList[i].sName
                break
            end
        end
        
        --Error: Unable to find the location.
        if(iRoomIndex == -1) then
            TL_SetProperty("Append", "Invalid location specified." .. gsDE)
            return
        end
    
    --Location is "me" so spawn it at the player's position.
    elseif(sLocation == "me") then
        sLocation = gzTextVar.gzPlayer.sLocation
    end

    --Dolls.
    if(sTypeString == "doll") then
        
        --Cannot spawn in disguise, silly!
        if(sLocation == "disguise") then
            TL_SetProperty("Append", "Dolls cannot spawn in disguise." .. gsDE)
            return
        end
        
        --Spawn.
        fnSpawnDoll(sLocation, "Random Doll " .. string.char(gzTextVar.iDollSpawn), nil)
        gzTextVar.iDollSpawn = gzTextVar.iDollSpawn + 1
        
    --Dogpile, spawns 2 dolls.
    elseif(sTypeString == "dogpile") then
        
        --Cannot spawn in disguise, silly!
        if(sLocation == "disguise") then
            TL_SetProperty("Append", "Dolls cannot spawn in disguise." .. gsDE)
            return
        end
        
        --Spawn.
        fnSpawnDoll(sLocation, "Random Doll " .. string.char(gzTextVar.iDollSpawn), nil)
        gzTextVar.iDollSpawn = gzTextVar.iDollSpawn + 1
        fnSpawnDoll(sLocation, "Random Doll " .. string.char(gzTextVar.iDollSpawn), nil)
        gzTextVar.iDollSpawn = gzTextVar.iDollSpawn + 1
    
    --Titans.
    elseif(sTypeString == "titan") then
        
        --Cannot spawn in disguise, silly!
        if(sLocation == "disguise") then
            TL_SetProperty("Append", "Titans cannot spawn in disguise." .. gsDE)
            return
        end
        
        --Spawn.
        fnSpawnTitan(sLocation, "Random Titan " .. string.char(gzTextVar.iTitanSpawn), nil)
        gzTextVar.iTitanSpawn = gzTextVar.iTitanSpawn + 1
    
    --Potion. Here's looking at you, Aaaac.
    elseif(sTypeString == "potion") then
        
        --Cannot spawn in disguise, silly!
        if(sLocation == "disguise") then
            TL_SetProperty("Append", "And just how do you propose to spawn a potion in diguise? Put a fake moustache on it?" .. gsDE)
            return
        end
        
        --Spawn.
        fnRegisterObject(sLocation, "potion", "potion", true, gzTextVar.sRootPath .. "Item Handlers/ToolsConsumables/Health Potion.lua")
    end
    
    --Rebuild locality/visibility.
    TL_SetProperty("Append", "Spawned " .. sTypeString .. " at " .. sLocation .. "." .. gsDE)
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)

--[Wipe Enemies]
elseif(sString == "wipe enemies") then

    --Input handled.
    gbHandledInput = true
    
    --Iterate. Remove all entities that aren't plot characters.
    local bRemoved = true
    while(bRemoved == true) do
        
        --Reset.
        bRemoved = false
        
        --Iterate.
        for i = 1, gzTextVar.zEntitiesTotal, 1 do
            
            --Protected entities do not get deleted. Not having the flag is the same as it being false.
            if(gzTextVar.zEntities[i].bIsProtected == nil or gzTextVar.zEntities[i].bIsProtected == false) then
            
                bRemoved = true
                fnRemoveEntity(i)
                break
            end
        end
    end
    
--[Spawn Torch/Coat/Gloves]
elseif(sString == "give me warm") then

    --Input handled.
    gbHandledInput = true
    
    --Spawn a torch, climber's coat, and salamander gloves. This loadout makes the freezing caverns trivial.
    -- It also helps immensely with the misty woods and brewery.
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "torch",             "torch",             true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Torch.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "climber's coat",    "climber's coat",    true, gzTextVar.sRootPath .. "Item Handlers/Armors/Climbers Coat.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "salamander gloves", "salamander gloves", true, gzTextVar.sRootPath .. "Item Handlers/Gloves/Salamander Gloves.lua")
    
--[Spawn Twinblade/Feather Coat]
elseif(sString == "give me soft") then

    --Input handled.
    gbHandledInput = true
    
    --Spawn Eileen's equipment, which is basically the best combat loadout. Supply your own gloves.
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "twinblade",    "twinblade",    true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Twinblade.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "feather coat", "feather coat", true, gzTextVar.sRootPath .. "Item Handlers/Armors/Feather Coat.lua")
    
--[Spawn Potion]
elseif(sString == "medical license") then

    --Input handled.
    gbHandledInput = true
    
    --Spawn a potion, allowing the player to heal themselves.
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "potion", "potion", true, gzTextVar.sRootPath .. "Item Handlers/ToolsConsumables/Health Potion.lua")
    
--[Save Anywhere]
elseif(sString == "battery backed") then
    gbHandledInput = true
    gzTextVar.bAllowSaveAnywhere = true
    TL_SetProperty("Append", "You now no longer need to be at a fountain to save the game." .. gsDE)
    
--[Spawn Cards]
elseif(sString == "miss wilds wild ride") then
    gbHandledInput = true
    TL_SetProperty("Append", "A bunch of cards appear out of nowhere. Yay!" .. gsDE)
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "card sleeve", "card sleeve", true, gzTextVar.sRootPath .. "Item Handlers/Cards/Card Sleeve.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "water card",  "water card",  true, gzTextVar.sRootPath .. "Item Handlers/Cards/Water Card.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "fire card",   "fire card",   true, gzTextVar.sRootPath .. "Item Handlers/Cards/Fire Card.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "wind card",   "wind card",   true, gzTextVar.sRootPath .. "Item Handlers/Cards/Wind Card.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "earth card",  "earth card",  true, gzTextVar.sRootPath .. "Item Handlers/Cards/Earth Card.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "life card",   "life card",   true, gzTextVar.sRootPath .. "Item Handlers/Cards/Life Card.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "death card",  "death card",  true, gzTextVar.sRootPath .. "Item Handlers/Cards/Death Card.lua")
    
--[Spawn Page]
elseif(sString == "journalism is a hobby") then
    gbHandledInput = true
    TL_SetProperty("Append", "A bunch of pages appear out of nowhere. Yay!" .. gsDE)
    for i = 1, 14, 1 do
        fnCreateJournalPage(gzTextVar.gzPlayer.sLocation, i)
    end

--[Spawn all Weapons]
elseif(sString == "animal eternal") then
    gbHandledInput = true
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "fire poker",       "fire poker",       true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Fire Poker.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "pool cue",         "pool cue",         true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Pool Cue.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "table leg",        "table leg",        true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Table Leg.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "torch",            "torch",            true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Torch.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "shortsword",       "shortsword",       true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Shortsword.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "enchanted rapier", "enchanted rapier", true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Enchanted Rapier.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "twinblade",        "twinblade",        true, gzTextVar.sRootPath .. "Item Handlers/Weapons/Twinblade.lua")

--[Spawn all Armors]
elseif(sString == "doom crossing") then
    gbHandledInput = true
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "climber's coat",    "climber's coat",    true, gzTextVar.sRootPath .. "Item Handlers/Armors/Climbers Coat.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "leather jacket",    "leather jacket",    true, gzTextVar.sRootPath .. "Item Handlers/Armors/Leather Jacket.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "leather gloves",    "leather gloves",    true, gzTextVar.sRootPath .. "Item Handlers/Gloves/Leather Gloves.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "leather armor",     "leather armor",     true, gzTextVar.sRootPath .. "Item Handlers/Armors/Leather Armor.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "salamander gloves", "salamander gloves", true, gzTextVar.sRootPath .. "Item Handlers/Gloves/Salamander Gloves.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "rusalka gloves",    "rusalka gloves",    true, gzTextVar.sRootPath .. "Item Handlers/Gloves/Rusalka Gloves.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "mora gloves",       "mora gloves",       true, gzTextVar.sRootPath .. "Item Handlers/Gloves/Mora Gloves.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "cerulis gloves",    "cerulis gloves",    true, gzTextVar.sRootPath .. "Item Handlers/Gloves/Cerulis Gloves.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "mail vest",         "mail vest",         true, gzTextVar.sRootPath .. "Item Handlers/Armors/Mail Vest.lua")
    fnRegisterObject(gzTextVar.gzPlayer.sLocation, "feather coat",      "feather coat",      true, gzTextVar.sRootPath .. "Item Handlers/Armors/Feather Coat.lua")

--[Enemies Change Power Level]
elseif(string.sub(sString, 1, 23) == "its doll fun and games ") then
    gbHandledInput = true

    --Resolve traps completed.
    local sTrapsDone = string.sub(sString, 24)
    if(sTrapsDone == "0") then
        gzTextVar.iTrapsDisarmed = 0
    elseif(sTrapsDone == "2") then
        gzTextVar.iTrapsDisarmed = 2
    elseif(sTrapsDone == "4") then
        gzTextVar.iTrapsDisarmed = 4
    else
        TL_SetProperty("Append", "Usage: 'its doll fun and games 0' will set enemies to the base power level. 2 and 4 will set them to those respective higher power levels." .. gsDE)
        return
    end
    
    --Run the script.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/Human/800 Complete Trap Stat Change.lua")
    
--[Switch to Active Combat]
elseif(sString == "no time like the present") then
    gbHandledInput = true
    TL_SetProperty("Append", "Combat is now in active mode." .. gsDE)
    TL_SetProperty("Set Combat Active Mode", true)
    
--[Switch to Turn Combat]
elseif(sString == "take your time") then
    gbHandledInput = true
    TL_SetProperty("Append", "Combat is now in turn mode." .. gsDE)
    TL_SetProperty("Set Combat Active Mode", false)
end
