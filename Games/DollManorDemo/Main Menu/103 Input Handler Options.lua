--[ =================================== Input Handler: Options ================================== ]
--Input handler when changing game options.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

--[Temp Function]
local round = function(fValue)
    return (fValue + 0.5 - (fValue + 0.5) % 1)
end

--[ ==================================== Doll Type Preference =================================== ]
if(gzOptions.iOptionMode == gzOptions.ciOptionDollPref) then
    
    --Force lowercase.
    local sUseString = string.lower(sString)
    
    if(sUseString == "dancer") then
        gzOptions.sOverrideDoll = "Dancer"
        TL_SetProperty("Append", "Doll preference set to 'Dancer'." .. gsDE)
        
    elseif(sUseString == "bride") then
        gzOptions.sOverrideDoll = "Bride"
        TL_SetProperty("Append", "Doll preference set to 'Bride'." .. gsDE)
        
    elseif(sUseString == "punk") then
        gzOptions.sOverrideDoll = "Punk"
        TL_SetProperty("Append", "Doll preference set to 'Punk'." .. gsDE)
        
    elseif(sUseString == "geisha") then
        gzOptions.sOverrideDoll = "Geisha"
        TL_SetProperty("Append", "Doll preference set to 'Geisha'." .. gsDE)
        
    elseif(sUseString == "goth") then
        gzOptions.sOverrideDoll = "Goth"
        TL_SetProperty("Append", "Doll preference set to 'Goth'." .. gsDE)
        
    elseif(sUseString == "princess") then
        gzOptions.sOverrideDoll = "Princess"
        TL_SetProperty("Append", "Doll preference set to 'Princess'." .. gsDE)
        
    else
        gzOptions.sOverrideDoll = "Null"
        TL_SetProperty("Append", "Game will select doll type randomly." .. gsDE)
        
    end
    TL_SetProperty("Create Blocker")
    
    --Clear flags.
    gzOptions.iOptionMode = gzOptions.ciOptionNone
    LM_ExecuteScript(LM_GetCallStack(0), "showmenu")
    return
end

--[ =================================== Doll Color Preference =================================== ]
if(gzOptions.iOptionMode == gzOptions.ciOptionDollColor) then

    --Force lowercase.
    local sUseString = string.lower(sString)
    
    if(sUseString == "0") then
        gzOptions.iOverrideDollSkin = 0
        TL_SetProperty("Append", "Plastic skin color set to lightest." .. gsDE)
        
    elseif(sUseString == "1") then
        gzOptions.iOverrideDollSkin = 1
        TL_SetProperty("Append", "Plastic skin color set to light." .. gsDE)
        
    elseif(sUseString == "2") then
        gzOptions.iOverrideDollSkin = 2
        TL_SetProperty("Append", "Plastic skin color set to dark." .. gsDE)
        
    elseif(sUseString == "3") then
        gzOptions.iOverrideDollSkin = 3
        TL_SetProperty("Append", "Plastic skin color set to darkest." .. gsDE)
        
    else
        gzOptions.iOverrideDollSkin = -1
        TL_SetProperty("Append", "Game will select plastic color randomly." .. gsDE)
        
    end
    TL_SetProperty("Create Blocker")

    --Clear flags.
    gzOptions.iOptionMode = gzOptions.ciOptionNone
    LM_ExecuteScript(LM_GetCallStack(0), "showmenu")
    return
end

--[ ======================================== Music Volume ======================================= ]
if(gzOptions.iOptionMode == gzOptions.ciOptionMusicVol) then
    
    --Number from 0 to 100.
    local fNumber = tonumber(sString)
    local iNumber = -1
    if(fNumber ~= nil) then iNumber = round(fNumber) end
    if(iNumber >= 0 and iNumber <= 100) then
        TL_SetProperty("Append", "Set music volume to " .. iNumber .. "." .. gsDE)
        AudioManager_SetProperty("Music Volume", iNumber/100.0)
        
    --Use default.
    else
        TL_SetProperty("Append", "Set music volume to default (" .. round(AudioManager_GetProperty("Default Music Volume")*100.0) .. ")." .. gsDE)
        AudioManager_SetProperty("Music Volume", AudioManager_GetProperty("Default Music Volume"))
    
    end

    --Clear flags.
    gzOptions.iOptionMode = gzOptions.ciOptionNone
    LM_ExecuteScript(LM_GetCallStack(0), "showmenu")
    return
end

--[ ======================================== Sound Volume ======================================= ]
if(gzOptions.iOptionMode == gzOptions.ciOptionSoundVol) then
    
    --Number from 0 to 100.
    local fNumber = tonumber(sString)
    local iNumber = -1
    if(fNumber ~= nil) then iNumber = round(fNumber) end
    if(iNumber >= 0 and iNumber <= 100) then
        TL_SetProperty("Append", "Set sound volume to " .. iNumber .. "." .. gsDE)
        AudioManager_SetProperty("Sound Volume", iNumber/100.0)
        
    --Use default.
    else
        TL_SetProperty("Append", "Set sound volume to default (" .. round(AudioManager_GetProperty("Default Sound Volume")*100.0) .. ")." .. gsDE)
        AudioManager_SetProperty("Sound Volume", AudioManager_GetProperty("Default Music Volume"))
    
    end

    --Clear flags.
    gzOptions.iOptionMode = gzOptions.ciOptionNone
    LM_ExecuteScript(LM_GetCallStack(0), "showmenu")
    return
end

--[ ================================ Standard Menu Functionality ================================ ]
--Setup.
local iLen = #gzMenu.zCommandList

--[Show Menu]
--Displays the menu.
if(sString == "showmenu") then
    
    --Header.
    TL_SetProperty("Append", gsDE .. " [General Options]")
    
    --Iterate.
    for i = 1, iLen, 1 do
        
        --Left side.
        if(i % 2 == 1) then
            local sString = i .. ":"
            TL_SetProperty("Append", sString)
            
            sString = "[POS:035]" .. gzMenu.zCommandList[i].sDisplay
            TL_SetProperty("Append", sString)
        
        --Right side.
        else
            local sString = "[POS:300]" .. i .. ":"
            TL_SetProperty("Append", sString)
            
            sString = "[POS:320]" .. gzMenu.zCommandList[i].sDisplay
            TL_SetProperty("Append", sString)
        end
    end
    
    --Footer.
    TL_SetProperty("Append", gsDE .. "Type the number of the desired option and push enter." .. gsDE)
    return
end

--[Cycle]
--Figure out which index the player typed, if any.
local iIndex = tonumber(sString)
if(iIndex == nil) then
    TL_SetProperty("Append", "Please enter the number preceding the option you wish to activate. Type [showmenu] to repeat the menu options." .. gsDE)
    return
end

--Out of range.
if(iIndex < 1 or iIndex > iLen) then
    TL_SetProperty("Append", "Option out of range. Type [showmenu] to repeat the menu options." .. gsDE)
    return
end

--Otherwise, index is valid. Execute the option.
LM_ExecuteScript(gzMenu.zCommandList[iIndex].sScriptHandler, gzMenu.zCommandList[iIndex].sInternal)