--[ ========================================= Menu Boot ========================================= ]
--Boots the main menu. Uses a variable holder other than gzTextVar.
gzMenu = {}

--Script Handlers
local sStandardHandler = fnResolvePath() .. "200 Standard Resolver.lua"

--[Starting Commands]
--Boot.
gzMenu.zCommandList = {}

--Adder function.
function fnAddCommand(sCommandDisplayString, sInternalName, sScriptHandler)
    
    --Create new command.
    local i = #gzMenu.zCommandList + 1
    gzMenu.zCommandList[i] = {}
    
    --Set properties
    gzMenu.zCommandList[i].sDisplay = sCommandDisplayString
    gzMenu.zCommandList[i].sInternal = sInternalName
    gzMenu.zCommandList[i].sScriptHandler = sScriptHandler
    
end

--Add baseline commands.
fnAddCommand("New Game", "NEWGAME", sStandardHandler)
fnAddCommand("New Game (Skip Intro)", "NEWGAMENOINTRO", sStandardHandler)
fnAddCommand("Tutorial", "NEWGAMETUTORIAL", sStandardHandler)
fnAddCommand("Load Game", "LOADGAME", sStandardHandler)
fnAddCommand("Difficulty", "DIFFICULTY", sStandardHandler)
fnAddCommand("Options", "OPTIONS", sStandardHandler)
fnAddCommand("Quit", "EXITGAME", sStandardHandler)

--Specify the input handler.
TL_SetProperty("String Handler Path", gzTextVar.sRootPath .. "Main Menu/100 Input Handler.lua")

--Run the menu handler once to show the menu.
LM_ExecuteScript(gzTextVar.sRootPath .. "Main Menu/100 Input Handler.lua", "showmenu")

--Fade the automap.
TL_SetProperty("Set Map Fade Timer", 1.0)
TL_SetProperty("Set Map Fade Delta", 0.0)
TL_SetProperty("Set Navigation Flags", ciNavShowNothing)
