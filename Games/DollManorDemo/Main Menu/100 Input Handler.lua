--[ ================================= Input Handler: Main Menu ================================== ]
--Acts as an input handler for the main menu. Uses variable entries, allowing the menu to be dynamically
-- set up. Automatically reports failures and informs the user of how to do simple things like repeat
-- instructions with the [repeat] command.

--Argument Listing:
-- 0: sString - The string to be handled. Should be in all lowercase.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--Don't do anything if the string is empty.
if(sString == "") then return end

--Setup.
local iLen = #gzMenu.zCommandList

--[Show Menu]
--Displays the menu.
if(sString == "showmenu") then
    
    --Header.
    TL_SetProperty("Append", gsDE .. " [Main Menu]")
    
    --Iterate.
    for i = 1, iLen, 1 do
        
        --Left side.
        if(i % 2 == 1) then
            local sString = i .. ":"
            TL_SetProperty("Append", sString)
            
            sString = "[POS:035]" .. gzMenu.zCommandList[i].sDisplay
            TL_SetProperty("Append", sString)
        
        --Right side.
        else
            local sString = "[POS:300]" .. i .. ":"
            TL_SetProperty("Append", sString)
            
            sString = "[POS:320]" .. gzMenu.zCommandList[i].sDisplay
            TL_SetProperty("Append", sString)
        end
    end
    
    --Footer.
    TL_SetProperty("Append", gsDE .. "Type the number of the desired option and push enter.\n")
    TL_SetProperty("Append", "Current game difficulty: " .. gzTextVar.sDifficulty .. gsDE)
    return
end

--[Cycle]
--Figure out which index the player typed, if any.
local iIndex = tonumber(sString)
if(iIndex == nil) then
    TL_SetProperty("Append", "Please enter the number preceding the option you wish to activate. Type [showmenu] to repeat the menu options." .. gsDE)
    return
end

--Out of range.
if(iIndex < 1 or iIndex > iLen) then
    TL_SetProperty("Append", "Option out of range. Type [showmenu] to repeat the menu options." .. gsDE)
    return
end

--Otherwise, index is valid. Execute the option.
LM_ExecuteScript(gzMenu.zCommandList[iIndex].sScriptHandler, gzMenu.zCommandList[iIndex].sInternal)