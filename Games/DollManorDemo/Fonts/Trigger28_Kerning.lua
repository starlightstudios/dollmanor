--[Oxygen-Regular Kerning]
--Sets the kerning values for the listed font.

--Set the main scaler.
StarFont_SetKerning(1.0)

--By

--Letters groupings.
--StarFont_SetKerning(string.byte("i"), -1, 3.0)

--Space sizing.
StarFont_SetKerning(string.byte(" "), -1, 10.0)
StarFont_SetKerning(-1, string.byte(" "), 0.0)

--Punctuation groupings.
StarFont_SetKerning(-1, string.byte("."), 2.0)
StarFont_SetKerning(-1, string.byte(","), 2.0)
StarFont_SetKerning(-1, string.byte("'"), 2.0)
StarFont_SetKerning(-1, string.byte("-"), 3.0)
StarFont_SetKerning(-1, string.byte(":"), 4.0)
StarFont_SetKerning(string.byte("'"), -1, 2.0)
StarFont_SetKerning(string.byte("-"), -1, 3.0)

--Specific punctuations.
--StarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

--Numbers.
--StarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

--Specific letters.
--StarFont_SetKerning(string.byte("a"), string.byte("c"), 1.0)

--Letter-to-punctuation.
--StarFont_SetKerning(string.byte("r"), string.byte(","), 0.0)
