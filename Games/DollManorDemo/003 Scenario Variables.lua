--[ ===================================== Scenario Variables ==================================== ]
--World Turns
gzTextVar.iWorldTurns = 0

--Combat special flags
gzTextVar.iEasyCombats = 2
gzTextVar.bTurnEndsWhenCombatEnds = false

--[State Variables]
--Story
gzTextVar.bEnteredPhase2Naturally = false
gzTextVar.iGameStage = 1
gzTextVar.iRespawnTurns = 20
gzTextVar.bExtraPhase2Enemies = false
gzTextVar.sExtraPhase2Path = gzTextVar.sRootPath .. "Scenario Handlers/900 Take Diamond Key.lua"

--Respawn time is reduced for the smaller manor.
if(gzTextVar.sManorType == "Demo" or gzTextVar.sManorType == "Simple") then
    gzTextVar.iRespawnTurns = 10
end

--Bonuses for basic dolls. Does not apply to titans or claygirls.
gzTextVar.iTrapsDisarmed = 0
gzTextVar.iDollBonusHP = 0
gzTextVar.iDollBonusAtp = 0
gzTextVar.fDollBonusSpd = 0.0

--Mechanical
gzTextVar.bIsPlayerSurrendering = false
gzTextVar.bIsGameEnding = false
gzTextVar.bExternalEndingCall = false
gzTextVar.bCanSpotClaygirls = false
gzTextVar.bTakeAll = false

--Saving
gzTextVar.bAllowSaveAnywhere = false
gzTextVar.bIsStateCheckingSave = false
gzTextVar.bIsAtSaveLocation = false
gzTextVar.bIsUsingSaveGlyph = false

--Jessie/Lauren
gzTextVar.bAlreadySawLaurenTFStart = false
gzTextVar.bMetPygmalie = false
gzTextVar.bNotifiedAboutTalk = false

--Foggy Woods
gzTextVar.bIsFogActive = false
gzTextVar.sActivateFogPath = gzTextVar.sRootPath .. "Scenario Handlers/Environment/000 Activate Fog.lua"
gzTextVar.sDeactivateFogPath = gzTextVar.sRootPath .. "Scenario Handlers/Environment/001 Deactivate Fog.lua"

--Dark Areas
gzTextVar.bIsDarkActive = false
gzTextVar.sActivateDarkPath = gzTextVar.sRootPath .. "Scenario Handlers/Environment/002 Activate Dark.lua"
gzTextVar.sDeactivateDarkPath = gzTextVar.sRootPath .. "Scenario Handlers/Environment/003 Deactivate Dark.lua"

--[ ====================================== Phase Variables ====================================== ]
--[Phase 1]
gzTextVar.bSpokeToPygmalie = false
gzTextVar.bIsJessieFollowing = false
gzTextVar.bIsJessieMainHalled = false
gzTextVar.bIsLaurenFollowing = false
gzTextVar.bIsLaurenMainHalled = false

--Special
gzTextVar.bGoatsicle = false
gzTextVar.bToldJessie = false
gzTextVar.bJessieWillTF = false

--[Phase 2]
--Basic.
gzTextVar.bSpawnedScenarioKey = false
gzTextVar.bSeenAntechamber = false
gzTextVar.bHasSeenPaintedDollDialogue = false
gzTextVar.bAtLeastOneTrapSeen = false
gzTextVar.bAtLeastOneTrapHandled = false

--Phase 2 Statue Trap
gzTextVar.bHasReadOrangeBook = false
gzTextVar.bHasSeenOrangeBookExtra = false
gzTextVar.bHasSeenHallwayDialogue = false
gzTextVar.bHasSeenHallwayReturnDialogue = false
gzTextVar.bHasSeenHallwayStatueDialogue = false
gzTextVar.iHypnoticCheck = 0
gzTextVar.bCloseDoorBehind = false
gzTextVar.bForceMoveStatue = false
gzTextVar.bSecretStatue = false

--Phase 2 String Trap
gzTextVar.sStringTriggerPath = gzTextVar.sRootPath .. "Scenario Handlers/Human/900 Trigger String Trap.lua"
gzTextVar.sStringExaminePath = gzTextVar.sRootPath .. "Scenario Handlers/Human/901 String Trap Standard Examine.lua"
gzTextVar.bStringTrapSeenAntechamber = false
gzTextVar.bStringTrapSawStrings = false
gzTextVar.bStringTrapActive = false
gzTextVar.bStringTrapActivityCheck = false
gzTextVar.bStringTrapActiveN = false
gzTextVar.bStringTrapActiveE = false
gzTextVar.bStringTrapActiveS = false
gzTextVar.bStringTrapActiveW = false
gzTextVar.iMaryStringTrapState = 0
gzTextVar.bStringTrapNoFollow = false
gzTextVar.bStringTrapEnding = false
gzTextVar.bStringTrapDisarmed = false
gzTextVar.bStringTrapDisarmedCutscene = false

--Random Rolls for String Trap. These determine the chances of seeing a set of strings when using the look command.
gzTextVar.iGrandArchiveSpotThreshold = 50
gzTextVar.iaGrandArchiveRolls = {}
for i = 1, 26, 1 do
    gzTextVar.iaGrandArchiveRolls[i] = LM_GetRandomNumber(1, 100)
end

--Phase 2 Goop Trap
gzTextVar.bBreweryNoFollow = false
gzTextVar.bBreweryAntechamberScene = false
gzTextVar.bGoopTrapActivated = false
gzTextVar.bGoopTrapEndCutscene = false
gzTextVar.iGoopTFStage = 0
gzTextVar.bGoopedFriends = false
gzTextVar.bGoopTrapEnding = false

--Phase 2 Freezing Trap
gzTextVar.bFreezingNoFollow = false
gzTextVar.fMaryFreezeAmount = 0.000 --At 100, Mary freezes
gzTextVar.bFreezingTrapFoundCrystal = false
gzTextVar.bFreezingTrapDisarmed = false
gzTextVar.bFreezingEndCutscene = false
gzTextVar.bFreezingAntechamberScene = false
gzTextVar.bFrozeFriends = false
gzTextVar.bFreezingTrapEnding = false

--Phase 2 Claygirl Trap
gzTextVar.bClayNoFollow = false
gzTextVar.bClayDisarmed = false
gzTextVar.bClayEntryCutscene = false
gzTextVar.bClayCompletedCutscene = false
gzTextVar.bSawClayHouse = false
gzTextVar.iClayTimer = 0
gzTextVar.bClayDoorThisTurn = false
gzTextVar.bClayItemThisTurn = false
gzTextVar.bIsClaySequence = false
gzTextVar.bClayedFriends = false
gzTextVar.bClayTrapEnding = false

--[Phase 3]
gzTextVar.bDisabledLight = false
gzTextVar.bDisabledLightCutscene = false

--[Stranger]
gzTextVar.bRungBell = false
gzTextVar.iStrangerDefeats = 0
gzTextVar.bStrangerCanRespawn = false
gzTextVar.iStrangerDefeatedTurn = 0

--[Eileen]
gzTextVar.bMetEileen = false
gzTextVar.iEileenTalk = 0

--[ ===================================== Ending Variables ====================================== ]
--Endings
gzTextVar.bIsBetrayal = false
gzTextVar.bDollSequenceVoluntary = false

--Special Ending Handlers
gzTextVar.bSpecialEndingBetraySubmit = false
gzTextVar.bSpecialEndingBetrayBetray = false
gzTextVar.bSpecialEndingSacrificeMary = false
gzTextVar.bSpecialEndingSacrificeJessie = false
gzTextVar.bSpecialEndingSacrificeLauren = false
gzTextVar.bSpecialEndingSacrificeSarah = false

--[Journal Pages]
--Pages the player has found.
ciTotalPages = 14
gzTextVar.iTotalPagesFound = 0
gzTextVar.baPagesFound = {}
for i = 1, ciTotalPages, 1 do
    gzTextVar.baPagesFound[i] = false
end

--[Debug]
--Debug Spawners
gzTextVar.iDollSpawn = string.byte("A")
gzTextVar.iTitanSpawn = string.byte("A")
gzTextVar.iClaySpawn = string.byte("A")

--[ ====================================== Player Variables ===================================== ]
--Place the player near the main hall.
gzTextVar.gzPlayer = {}
gzTextVar.gzPlayer.sName = "Mary"
gzTextVar.gzPlayer.sLocation = "Entryway"
gzTextVar.gzPlayer.sPrevLocation = gzTextVar.gzPlayer.sLocation
gzTextVar.gzPlayer.sPrevLocationStore = gzTextVar.gzPlayer.sPrevLocation
gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Mary"
gzTextVar.gzPlayer.bIsCrippled = false
gzTextVar.gzPlayer.sFormState = "Human"
gzTextVar.gzPlayer.iRegenTimer = 0
gzTextVar.gzPlayer.bDollMoveLock = false
gzTextVar.gzPlayer.iSkinColor = LM_GetRandomNumber(0, 3)
gzTextVar.gzPlayer.iBaseSkinColor = gzTextVar.gzPlayer.iSkinColor
gzTextVar.gzPlayer.iClayInfection = -1
gzTextVar.gzPlayer.bTitanTransformation = false
gzTextVar.gzPlayer.sSpecialIdentifier = "Mary"
gzTextVar.gzPlayer.bHasSeenTestament = false
gzTextVar.gzPlayer.bHasEncounteredDoll = false
gzTextVar.gzPlayer.bHasSpokenToDoll = false
gzTextVar.gzPlayer.bMetEileen = false

--Image Layers
gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
gzTextVar.gzPlayer.sLayer1 = "Null"

--[Player's Deck]
--Setup
gzTextVar.gzPlayer.zDeck = {}

--Sizes.
gzTextVar.gzPlayer.zDeck.iMinimum = 18
gzTextVar.gzPlayer.zDeck.iMaximum = 35

--Loadout
gzTextVar.gzPlayer.zDeck.iAttackCards = 7
gzTextVar.gzPlayer.zDeck.iDefendCards = 5
gzTextVar.gzPlayer.zDeck.iWaterCards = 4
gzTextVar.gzPlayer.zDeck.iFireCards = 4
gzTextVar.gzPlayer.zDeck.iWindCards = 4
gzTextVar.gzPlayer.zDeck.iEarthCards = 4
gzTextVar.gzPlayer.zDeck.iLifeCards = 0
gzTextVar.gzPlayer.zDeck.iDeathCards = 0
gzTextVar.gzPlayer.zDeck.iOfCards = 2
gzTextVar.gzPlayer.zDeck.iAndCards = 5
gzTextVar.gzPlayer.zDeck.iaCardArray = {7, 5, 4, 4, 4, 4, 0, 0, 2, 5} --Parallel, used for fast-access

--Card Maximums
gzTextVar.gzPlayer.zDeck.iMaxAttackCards = 9
gzTextVar.gzPlayer.zDeck.iMaxDefendCards = 7
gzTextVar.gzPlayer.zDeck.iMaxWaterCards = 4
gzTextVar.gzPlayer.zDeck.iMaxFireCards = 4
gzTextVar.gzPlayer.zDeck.iMaxEarthCards = 4
gzTextVar.gzPlayer.zDeck.iMaxWindCards = 4
gzTextVar.gzPlayer.zDeck.iMaxLifeCards = 0
gzTextVar.gzPlayer.zDeck.iMaxDeathCards = 0
gzTextVar.gzPlayer.zDeck.iMaxOfCards = 4
gzTextVar.gzPlayer.zDeck.iMaxAndCards = 6
gzTextVar.gzPlayer.zDeck.iaMaxCardArray = {9, 7, 4, 4, 4, 4, 0, 0, 4, 6}

--Ace.
gzTextVar.gzPlayer.zDeck.iAceType = gciCardType_Action
gzTextVar.gzPlayer.zDeck.iAceEffect = gciCardEffect_Damage
gzTextVar.gzPlayer.zDeck.iAcePower = 2 --Added to the base properties.
gzTextVar.gzPlayer.zDeck.iAceElement = gciElement_None

--[Doll Form]
--Has an override, use that.
if(gzOptions.sOverrideDoll ~= "Null") then
    gzTextVar.gzPlayer.sTFSeq = gzOptions.sOverrideDoll

--Random roller.
else
    local iTFRoll = LM_GetRandomNumber(1, 3)
    if(iTFRoll == 1) then
        gzTextVar.gzPlayer.sTFSeq = "Dancer"
    elseif(iTFRoll == 2) then
        gzTextVar.gzPlayer.sTFSeq = "Goth"
    else
        gzTextVar.gzPlayer.sTFSeq = "Punk"
    end
end

--Skin color can be overridden.
if(gzOptions.iOverrideDollSkin ~= -1) then
    gzTextVar.gzPlayer.iSkinColor = gzOptions.iOverrideDollSkin
else
    gzTextVar.gzPlayer.iSkinColor = gzTextVar.gzPlayer.iBaseSkinColor
end

--Focus on the player's map position.
local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
TL_SetProperty("Set Map Focus", -fX, -fY)

--Rendering handler. Top floor renders the middle floor, otherwise no secondary rendering.
TL_SetProperty("Set Rendering Z Level", fZ)
if(fZ < 0) then
    TL_SetProperty("Set Under Rendering Z Level", fZ+1, 0)
else
    TL_SetProperty("Set Under Rendering Z Level", ciDoNotRenderUnder, ciDoNotRenderUnder)
end

--Player indicator goes here.
gzTextVar.gzPlayer.iIndicatorX = 0
gzTextVar.gzPlayer.iIndicatorY = 3
gzTextVar.gzPlayer.iIndicatorC = ciCodePlayer
TL_SetProperty("Register Entity Indicator", fX, fY, fZ, "Player", gzTextVar.gzPlayer.iIndicatorX, gzTextVar.gzPlayer.iIndicatorY, gzTextVar.gzPlayer.iIndicatorC)
TL_SetProperty("Player World Position", fX, fY, fZ)

--Mark explored/visible for the player and everything adjacent.
fnMarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)

--Default images.
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)

--Player's combat statistics.
gzTextVar.gzPlayer.iHP = 20
gzTextVar.gzPlayer.iHPMax = 20
gzTextVar.gzPlayer.iAtp = 5
gzTextVar.gzPlayer.iDef = 3
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
TL_SetProperty("Set Storage Line", 0, "Turn: 0")

--[Inventory]
gzTextVar.gzPlayer.iItemsTotal = 0
gzTextVar.gzPlayer.zaItems = {}
gzTextVar.gzPlayer.zWeapon = nil
gzTextVar.gzPlayer.zArmor = nil
gzTextVar.gzPlayer.zGloves = nil

--[ ====================================== Entity Variables ===================================== ]
--These are entities other than the player. This includes allies and enemies.
gzTextVar.zEntitiesTotal = 0
gzTextVar.zEntities = {}
gzTextVar.iLastSpawnedEntity = -1

--[External Calls]
--These scripts spawn characters.
LM_ExecuteScript(gzTextVar.sRootPath .. "Entity Handlers/Spawn Jessie.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "Entity Handlers/Spawn Lauren.lua")
if(gzTextVar.sManorType ~= "Tutorial") then
    LM_ExecuteScript(gzTextVar.sRootPath .. "Entity Handlers/Spawn Pygmalie.lua")
end

--Special variables. If Mary is a doll, this handles [talk Lauren] cases.
gzTextVar.iDollLaurenSpeak = 0

--[ ========================================= Functions ========================================= ]
--Functions that operate on scenario variables.
LM_ExecuteScript(gzTextVar.sRootPath .. "Combat Handlers/Combat Functions.lua")

--Neutral AI Functions
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Neutral AI.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Standard AI.lua")

--Monster AI Functions
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Doll Standard.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Doll Titan.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Doll Tutorial Patrol.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Doll Tutorial Stationary.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Doll Tutorial Stationary Combat.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Doll Tutorial Stationary Combat B.lua")

--Human AI Functions
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Jessie.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Lauren.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/Pygmalie.lua")

