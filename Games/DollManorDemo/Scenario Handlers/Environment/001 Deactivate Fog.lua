--[Deactivate Fog]
--Turns off fog if it's active.
if(gzTextVar.bIsFogActive == false) then return end
gzTextVar.bIsFogActive = false

--Set.
TL_SetProperty("Deactivate Overlays")

--Handle the examination input.
gbHandledInput = true

--Description.
TL_SetProperty("Append", "The fog seems to dissipate..." .. gsDE)

--Standard.
fnListEntities(gzTextVar.gzPlayer.sLocation, true)
fnListObjects(gzTextVar.gzPlayer.sLocation, true)
