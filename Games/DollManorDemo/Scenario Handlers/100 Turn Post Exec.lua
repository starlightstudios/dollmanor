--[ ====================================== Turn Post Exec ======================================= ]
--After a turn is ended but before the entities update, this is called. This handled scenario stuff.

--Get the player's form. This routes to subscripts. The player's form affects cutscenes pretty heavily,
-- as you might expect.
if(gzTextVar.gzPlayer.sFormState == "Human") then
    LM_ExecuteScript(fnResolvePath() .. "Human/000 Post Exec.lua")
    
elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll") then
    LM_ExecuteScript(fnResolvePath() .. "Doll/000 Post Exec.lua")
end

--[Music Handler]
--Modifies music according to whichever room the player is in. Rooms store these values internally.
local iRoomIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
if(iRoomIndex == -1) then return end

TL_SetProperty("Modify Music", "Ambient_Basement", gzTextVar.zRoomList[iRoomIndex].zMusicVolumes[1])
TL_SetProperty("Modify Music", "Ambient_Interior", gzTextVar.zRoomList[iRoomIndex].zMusicVolumes[2])
TL_SetProperty("Modify Music", "Ambient_Rain",     gzTextVar.zRoomList[iRoomIndex].zMusicVolumes[3])