--[ =================================== Turn Post Exec: Human =================================== ]
--Handles post-turn cutscenes if the player is a human.

--Setup.
local sBasePath = fnResolvePath()

--[Same Tile As Pygmalie]
if(gzTextVar.bMetPygmalie == false and gzTextVar.iPygmalieIndex ~= nil and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation) then
    gzTextVar.bMetPygmalie = true
    TL_SetProperty("Append", "There is a woman here. You may want to [talk pygmalie] to her." .. gsDE)
    TL_SetProperty("Create Blocker")
end

--[Same Tile as Jessie or Lauren]
if(gzTextVar.bNotifiedAboutTalk == false and gzTextVar.iJessieIndex ~= nil and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation) then
    gzTextVar.bNotifiedAboutTalk = true
    TL_SetProperty("Append", "You've found Jessie! You should [talk jessie] with her." .. gsDE)
    TL_SetProperty("Create Blocker")
end

if(gzTextVar.bNotifiedAboutTalk == false and gzTextVar.iLaurenIndex ~= nil and gzTextVar.gzPlayer.sLocation == gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation) then
    gzTextVar.bNotifiedAboutTalk = true
    TL_SetProperty("Append", "You've found Lauren! You should [talk lauren] with him." .. gsDE)
    TL_SetProperty("Create Blocker")
end

--"Hall of Painted Dolls" cutscene.
gbHandledSubscript = false
LM_ExecuteScript(sBasePath .. "200 Hall of Painted Dolls.lua")
if(gbHandledSubscript == true) then return end
