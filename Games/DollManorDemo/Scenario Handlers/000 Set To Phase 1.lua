--[ ====================================== Set to Phase 1 ======================================= ]
--Resets the game to phase 1. Note that this is *not* called at game startup, it is used only for
-- debug reasons.
gzTextVar.iGameStage = 1
gzTextVar.bIsJessieFollowing = false
gzTextVar.bIsJessieMainHalled = false
gzTextVar.bIsLaurenFollowing = false
gzTextVar.bIsLaurenMainHalled = false