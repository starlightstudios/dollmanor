--[ ======================================== Introduction ======================================= ]
--A cutscene that plays when the game starts.
TL_SetProperty("Begin Fade In", "Null")
TL_SetProperty("Set Hide Stats Flag", true)

--Fire.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "Your name is Mary. You are a 16-year-old girl from Coventry, West Midlands, England." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
TL_SetProperty("Append", "This is Jessie, your best friend (even if she's from Sheffield...). You've been inseperable since you met at the age of 8." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
TL_SetProperty("Append", "This is Lauren, your little brother. You're walking him home after school to make sure nobody picks on him." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "It is a chilly, overcast day. As you usually do, you're taking a long, meandering path home. You're cutting through the woods, killing time, and enjoying the part of the day that isn't school." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You always hop across the brook that runs through the forest near here. You take your time hopping across the stones so your shoes don't get wet." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
TL_SetProperty("Append", "Lauren usually misses and splashes himself. This time is no exception. You and Jessie help him up, chuckling at him. He takes it with good humour." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "NULL", ciImageLayerDefault)
TL_SetProperty("Append", "You continue along the path. The sky is darkening with threats of rain. You should probably hurry home." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Wait." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "There's the brook." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The brook you just crossed." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You turn around. Jessie is as confused as you. Did you get lost? Did you go the wrong way?" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You reorient yourself, change directions, and resume the course for home." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "There's the brook. Again. There's a box sitting in the middle of the brook, resting on a stone. It was not there before." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You walk over to check it. There is a picture of some sort of red devil creature on it. The box has text on it, 'Miss Wild's Magic Deck'." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
TL_SetProperty("Append", "'Miss Wild?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "'A cartoon imp?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
TL_SetProperty("Append", "'That's Miss Wild! Haven't you heard of her?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
TL_SetProperty("Append", "'No.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
TL_SetProperty("Append", "'Miss Wild's Wild Adventures' is the best show on TV! She travels around the land, getting into wacky adventures and using her magic card deck to set right the wrongs in the world.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "'I didn't know she had merchandise.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "NULL", ciImageLayerDefault)
TL_SetProperty("Append", "You pick up the box. 'You think someone dropped it?'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "NULL", ciImageLayerDefault)
TL_SetProperty("Append", "A note falls out from the box. It reads 'It has to be fair. Good luck.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
TL_SetProperty("Append", "'We're gonna be late, and it's gonna rain any second. Open the box later.'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
TL_SetProperty("Append", "'Aww, c'mon! Look inside!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "You look inside. It's a collection of cards, with different suits. Fire, water, sword, shield. You idly tap a fire card. You'd swear it feels warm for a second." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "NULL", ciImageLayerDefault)
TL_SetProperty("Append", "The sky opens up. Rain pours onto you, chilling you to the bone. It is frigid, a kind of cold you've not felt in the coldest winter. 'Later! Let's go!'" .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The three of you start running as the wind picks up. You run in the direction you think is the right way..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "And there's the brook again. Only this time, there's someone standing there." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "They're wearing a heavy brown trench coat, covering their face and body. Every instinct in your body tells you to not call out, to not alert them to your presence." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "They turn to face you. A distant burst of lightning, followed by the crack of thunder a few seconds later. It illuminates the brook. You see a glint." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Stranger0", ciImageLayerDefault)
TL_SetProperty("Append", "A white mask. A vicious intent. Something is deeply wrong. Your feet are moving before you've even figured out what is happening." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "NULL", ciImageLayerDefault)
TL_SetProperty("Append", "You grab Lauren and run. The three of you run as quickly as you can, without time for a word to pass between you. Into the woods." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You think you see a house in the distance. You're not sure who lives there, and it doesn't matter at all. You need to call the police, call for help." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Stranger0", ciImageLayerDefault)
TL_SetProperty("Append", "And then, the stranger is in front of you. Between you and the house. The three of you stop dead in your tracks. You take the front." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "NULL", ciImageLayerDefault)
TL_SetProperty("Append", "The stranger advances on you. You try to catch sight of their face, but the mask is dark beneath and covers her completely. It doesn't matter." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You shout at Lauren to run for the house. With a shove, you send Jessie after him. The stranger lunges." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You dart behind a tree, your heart pounding through your chest. The stranger misses, recovers, and tries to grab you again." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Grabbing a nearby stick, you swing it at the stranger. The stick roars like an open flame as it slices the air. It connects, and they double over. Adrenaline surges. You run." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "As fast as your legs will carry you. You run towards the house. Your breath gives out. You're near the front door. The rain pounds." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Whoever was in the woods did not follow you. Maybe you lost them, maybe they're still watching and waiting. You look around briefly. No sign of Jessie or Lauren." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You hope they're in the house. You hope that whoever lives here can help. Something about this place is... strange... off... wrong..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You need to find your brother and your friend before you do anything else. Time to get going." .. gsDE)
TL_SetProperty("Create Blocker")

--Execute this script.
TL_SetProperty("Queue Fade Out", gzTextVar.sRootPath .. "999 Delayed Builder.lua")
