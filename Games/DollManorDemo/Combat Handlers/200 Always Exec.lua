-- |[ =============================== Always Exec Combat Handler =============================== ]|
--This file handles things like getting the player's HP and removing potions. Win or lose, this
-- should always occur.

--Store the player's stats after combat is over.
TL_SetProperty("Reresolve Stats After Combat")

--Store player's HP.
gzTextVar.gzPlayer.iHP = TL_GetProperty("Player HP")
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Get potion count. This only does something for Word Combat cases. This must be called after the Reresolve call.
if(gzTextVar.bIsCombatTypingBased == false) then
    
    --Get how many potions remain at the end of the battle.
    local iPotionCount = TL_GetProperty("Player Potions")
    
    --Count how many are in the player's inventory.
    local iInventoryPotions = 0
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "potion") then
            iInventoryPotions = iInventoryPotions + gzTextVar.gzPlayer.zaItems[i].iStackSize
        end
    end
    
    --Remove potions until the counts match.
    while(iInventoryPotions > iPotionCount) do
        
        --Find the first potion available.
        for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
            if(gzTextVar.gzPlayer.zaItems[i].sDisplayName == "potion") then 
                iInventoryPotions = iInventoryPotions - 1
                fnRemoveItemFromInventory(i)
                break
            end
        end
        
    end
end
