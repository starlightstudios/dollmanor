--[ ======================================= Defeat Lauren ======================================= ]
--Defeat case where the dolled player attacks human Lauren, and is defeated. Lauren will retreat to
-- a random location in the manor.

--Get a location to flee to.
local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
if(sLocation ~= "Null") then
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
end

--Text for statue:
if(gzTextVar.gzPlayer.sFormState == "Statue") then
    TL_SetProperty("Append", "You swing hard at the boy. He panics and ducks, causing you to miss. He then pushes your leg out and runs past you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You fall and land hard on the floor. You stand." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "This display of resistance is pointless. Your stone body cannot be harmed." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "But the boy is gone. Your brother is gone." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your... brother?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "THERE IS NO MARY." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is no Mary." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "YOU ARE A STATUE." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is no Mary. You are not Mary. You are a statue." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "OBEY." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is no Mary. You are a statue. Statues obey. You will obey." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "BRING THE INTRUDER TO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You are a statue. You will find the intruder, subdue him, and bring him to the light. You will obey." .. gsDE)
    TL_SetProperty("Create Blocker")


--Claygirl:
elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    TL_SetProperty("Append", "As you lunge at Lauren, he shouts and runs. You overextend and fall onto yourself, collapsing into a puddle of clay." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You reform yourself as quickly as you can, but he's already gone. Lauren has escaped." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "If only you could tell him how wonderful it is to be clay. Oh well. You will find him and show him instead." .. gsDE)
    TL_SetProperty("Create Blocker")

--Doll.
else
    TL_SetProperty("Append", "The little boy shows surprising strength and shoves you away. Before you can recover, he has fled somewhere into the manor." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "You are quite upset at this. A good dolly does not let her creator down, and your creator desires you to have more sisters. You must track the boy down." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "You smile. You will find the boy. He must be somewhere in the manor..." .. gsDE)
    TL_SetProperty("Create Blocker")
end

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")

--Reset player's HP to max.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Clean.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()