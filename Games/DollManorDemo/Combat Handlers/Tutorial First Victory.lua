--[ ================================= Tutorial Victory Handler ================================== ]
--When called at the end of combat, all hostile entities in the same room as the player are defeated
-- and removed from play. This is not used if a transformation is going to occur.

--Get the room the player is in.
--local iRoom = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
--if(iRoom == -1) then return end

--Iterate across all entities and check for location match and hostility match.
local bRemoved = true
while(bRemoved) do
    
    --Reset.
    bRemoved = false
    
    --Iterate.
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        
        --Entity is in the same location.
        if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
            
            --Check if the entity passes the hostility test. If so, it gets removed.
            gzTextVar.bLastHostilityCheck = false
            
            --If there's no handler, do nothing.
            if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
                
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
                fnClaygirl_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
                fnDoll_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
                fnTitan_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
                fnGoop_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
                fnStranger_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
                fnJessie_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
                fnLauren_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
                fnPygmalie_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
                fnSarah_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
                fnDollStationary_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
                fnDollStationaryCombat_HostilityCheck(i)
            end
            
            if(gzTextVar.bLastHostilityCheck == true) then
                bRemoved = true
                fnRemoveEntity(i)
                break
            end
        end
    end
end

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")

--Clean.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()