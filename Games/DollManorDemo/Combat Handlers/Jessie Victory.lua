--[ ======================================= Defeat Jessie ======================================= ]
--Defeat case where the dolled player attacks human Jessie. Jessie does not get removed, only crippled.

--Text.
if(gzTextVar.gzPlayer.sFormState == "Statue") then
    gzTextVar.bForceMoveStatue = true
    TL_SetProperty("Append", "You have harmed the intruder. She is SUBDUED. You will now CARRY HER TO THE LIGHT." .. gsDE)
else
    TL_SetProperty("Append", "After much roughhousing, Jessie falls to the ground with a thud. She twisted her ankle and can no longer run. She lets out a whimper." .. gsDE)
end

--Jessie is downed.
gzTextVar.zEntities[gzTextVar.iJessieIndex].bIsCrippled = true

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")

--Clean.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()