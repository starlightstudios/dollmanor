--[ ================================== Standard Victory Handler ================================= ]
--When called at the end of combat, all hostile entities in the same room as the player are defeated
-- and removed from play. This is not used if a transformation is going to occur.

--Get the room the player is in.
--local iRoom = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
--if(iRoom == -1) then return end

--Iterate across all entities and check for location match and hostility match.
local bRemoved = true
while(bRemoved) do
    
    --Reset.
    bRemoved = false
    
    --Iterate.
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        
        --Entity is in the same location.
        if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
            
            --Check if the entity passes the hostility test. If so, it gets removed.
            gzTextVar.bLastHostilityCheck = false
            
            --If there's no handler, do nothing.
            if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
                
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
                fnClaygirl_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
                fnDoll_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
                fnTitan_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
                fnGoop_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
                fnStranger_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
                fnJessie_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
                fnLauren_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
                fnPygmalie_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
                fnSarah_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
                fnDollStationary_HostilityCheck(i)
            elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
                fnDollStationaryCombat_HostilityCheck(i)
            end
    
            --If this is the Stranger, a special respawn routine is needed.
            if(gzTextVar.zEntities[i].sDisplayName == "Stranger") then
                gzTextVar.zStranger.iNeedsRespawn = 0
                gzTextVar.iStrangerDefeats = gzTextVar.iStrangerDefeats + 1
                gzTextVar.bLastHostilityCheck = true
                gzTextVar.iStrangerDefeatedTurn = gzTextVar.iWorldTurns
                
                --The stranger's respawn flag is set to false here. It will re-activate after a trap is completed.
                -- In phase 4, it auto-reactivates.
                gzTextVar.bStrangerCanRespawn = false
                
                --Special text:
                TL_SetProperty("Append", "The stranger kneels, defeated. Before your eyes, the body fades out of existence, leaving only the mask hovering in place." .. gsDE)
                TL_SetProperty("Create Blocker")
                if(gzTextVar.iStrangerDefeats-1 >=3) then
                    TL_SetProperty("Append", "The mask looks at you and waits. Then it, too, fades." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    TL_SetProperty("Append", "The stranger is gone. For now." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                else
                    TL_SetProperty("Append", "The mask looks at you and repairs the bullet hole in it very slightly. Then it, too, fades out." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    TL_SetProperty("Append", "It will be back, and next time, it may not be defeated so easily..." .. gsDE)
                    TL_SetProperty("Create Blocker")
                end
            end
            
            --Remove.
            if(gzTextVar.bLastHostilityCheck == true) then
                bRemoved = true
                fnRemoveEntity(i)
                break
            end
        end
    end
end

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")

--Clean.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()