--[ ================================== Tutorial Defeat Handler ================================== ]
--Tutorial defeat handler.
TL_SetProperty("Append", "Looks like you lost. We'll move you back and heal you up.")
TL_SetProperty("Create Blocker")
    
--Setup.
local sDestination = "Combat Sample Hall S"
local fOldX, fOldY, fOldZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
local fX, fY, fZ = fnGetRoomPosition(sDestination)

--Unmark the previous location, and mark this one as visible.
fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
fnMarkMinimapForPosition(sDestination)

--Move the character.
gzTextVar.gzPlayer.sPrevLocation = gzTextVar.gzPlayer.sLocation
gzTextVar.gzPlayer.sLocation = sDestination
TL_SetProperty("Set Map Focus", -fX, -fY)

--Change the indicator's position.
TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, "Player", fX, fY, fZ)
TL_SetProperty("Player World Position", fX, fY, fZ)

--Fullheal the player.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
gzTextVar.gzPlayer.bIsCrippled = false
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--SFX.
fnRandomFootstep()
fnBuildLocalityInfo()
