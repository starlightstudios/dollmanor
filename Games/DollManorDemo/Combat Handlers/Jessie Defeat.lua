--[ ======================================= Defeat Jessie ======================================= ]
--Defeat case where the dolled player attacks human Jessie and loses. Jessie flees into the manor.

--Get a location to flee to.
local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
if(sLocation ~= "Null") then
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
end

--Text for statue:
if(gzTextVar.gzPlayer.sFormState == "Statue") then
    TL_SetProperty("Append", "You swing your stone fist at Jessie, but somehow, she catches it and twists your arm." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Locking your arm behind you, she pushes you down. A quick shove and you fall over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her resistance is completely pointless, you have not been harmed in the slightest. You stand up." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is gone. She fled. You must... No..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Mary would never do this to her friends." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "THERE IS NO MARY." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is no Mary." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "YOU ARE A STATUE." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is no Mary. You are not Mary. You are a statue." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "OBEY." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There is no Mary. You are a statue. Statues obey. You will obey." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "BRING THE INTRUDER TO THE LIGHT." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You are a statue. You will find the intruder, subdue her, and bring her to the light. You will obey." .. gsDE)
    TL_SetProperty("Create Blocker")

--Claygirl:
elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    TL_SetProperty("Append", "As you lunge at Jessie, she nimbly dodges back. You collapse over your own clay, bursting into a puddle on the floor." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You reform yourself as quickly as you can, but she's already gone. She has escaped." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It seems she is not eager to become clay. That is fine. She will love it once you eventually catch her." .. gsDE)
    TL_SetProperty("Create Blocker")

--Doll.
else
    TL_SetProperty("Append", "As you lunge at Jessie, she suddenly breaks your grab and slams your head sideways, into the wall. Somehow, you remember her taking self-defense courses..." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "When you stand back up, she has fled into the manor. You feel sad. Good dollies do as their creator bids, and you have let a new sister escape." .. gsDE)
    TL_SetProperty("Create Blocker")

    TL_SetProperty("Append", "Fortunately, you can easily correct your mistake. You will find Jessie and bring her to your creator. You grin emptily." .. gsDE)
    TL_SetProperty("Create Blocker")
end

--Common handling.
LM_ExecuteScript(fnResolvePath() .. "200 Always Exec.lua")

--Reset player's HP to max.
gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Clean.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()