--[ ============================================ Doll =========================================== ]
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Make sure the current entity is valid. This is skipped during command building.
if((gzTextVar.iCurrentEntity < 1 or gzTextVar.iCurrentEntity > gzTextVar.zEntitiesTotal) and TL_GetProperty("Is Building Commands") == false) then return end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "attack",    "attack " .. sEntityName)
        TL_SetProperty("Register Popup Command", "talk",      "talk "   .. sEntityName)
    
    --Player is a doll:
    else
        TL_SetProperty("Register Popup Command", "talk",   "talk "   .. sEntityName)
    
    end
    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A human-sized doll in a costume. She is smiling, but there is a sinister look in her eyes." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A human-sized doll in a costume. She is smiling. She was created by the same one who commands your will." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "A human-sized doll in a costume. She is smiling. She will probably want to play with you once you've made your friends just like you." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "One of your pretty sisters. She smiles idly at you, and you return the look." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Phase 6.
        else
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "One of Pygmalie's doll girls. She doesn't suspect a thing. You give her an idle smile." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
        end
    end
    
--Attacking.
elseif(sInstruction == "attack " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt to fight." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Run the target's AI with this flag set.
        gzTextVar.bIsTriggeringFights = true
        gzTextVar.bTurnEndsWhenCombatEnds = true
        
        --Fast-access variables.
        local i = gzTextVar.iCurrentEntity
        
        --If there's no handler, do nothing.
        if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
            
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Claygirl") then
            fnClaygirl_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
            fnDoll_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
            fnTitan_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Goop") then
            fnGoop_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Stranger") then
            fnStranger_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
            fnJessie_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
            fnLauren_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
            fnPygmalie_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Sarah") then
            fnSarah_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
            fnDollStationary_AI(i)
        elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
            fnDollStationaryCombat_AI(i)
        end
        
        --Clean.
        gzTextVar.bIsTriggeringFights = false
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "You have no will, and thus cannot attack this doll unless ordered. The light has commanded you to find humans." .. gsDE)
    
    --Claygirl.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "Your creator does not want you to attack the doll girl, she wants you to cover the humans in clay! How silly you are!" .. gsDE)
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "Even if you tried, your useless hands would bounce off. Then again, she'd find that pretty funny!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You have no desire to attack the doll, or do anything else really." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Append", "You have no desire to attack the doll girl." .. gsDE)
        TL_SetProperty("Create Blocker")
        
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            TL_SetProperty("Append", "You could never bring yourself to attack one of your sisters unless you creator directly ordered it." .. gsDE)
        
        --Phase 6.
        else
            TL_SetProperty("Append", "Attacking a doll would blow your cover. So, you don't." .. gsDE)
        end
    end

--Talking to.
elseif(sInstruction == "talk " .. gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryName) then
    gbHandledInput = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        TL_SetProperty("Append", "You are too hurt even to speak." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "'Who are you?' you ask the doll. She smiles and approaches you with an icy look in her eyes..." .. gsDE)
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The doll approaches you, grinning. You regard her emptily." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Do you want to play?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Unregister Image")
        TL_SetProperty("Append", "STATUES DO NOT PLAY. YOU ARE A STATUE." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I do not want to play. I am a statue,' you say in a monotone." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The words are not your words, they are the light's words. You merely spoke them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Okay!' the doll says. 'Good luck finding the humans! If I get to them first, I get to play with them!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "The doll wanders off. You feel nothing." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "The doll approaches you, grinning. You stand with your dripping arms at your side." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Hi! Want to play a game with me? Hopscotch, marbles, maybe tic-tac-toe?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        
        local j = gzTextVar.iJessieIndex
        local l = gzTextVar.iLaurenIndex
        if(gzTextVar.zEntities[j].sState ~= "Human" or gzTextVar.zEntities[l].sState ~= "Human") then
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "You shake your head. You are busy trying to find your friends and cover them in clay." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "'Awww' she says with a pout. 'Maybe later?'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "You nod with enthusiasm. You'd love to play! But you have an important job to do first." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "'Yaaay! Good luck! Find them soon, we need more playmates!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        else
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "You shake your head. You need to present yourself to the creator before play time!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "'Awww' she says with a pout. 'Maybe later?'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Unregister Image")
            TL_SetProperty("Append", "You nod with enthusiasm. You'd love to play! But the creator always comes first." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "'Yaaay! I'll get everyone together and we can play cards!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Mary! That's your name, right? Want to play?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "You hold up a hand and wave. You can't speak, of course, but you'd love to play!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "However, you need to show your creator how you turned out. It will have to wait. The doll understands." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Yay! Go show the creator, and we can play jacks later!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Hello, ice girl. Would you like to play?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "'No.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Oh. That's boring. Do you want to do anything else?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "'No.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Well, to each their own. I'm going to go play jump rope!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Mary, was Betty at the chapel when you were last there?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "'No, I didn't see her. Are you looking for her?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'We were going to play hopscotch. Hey, do you want to play?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
        TL_SetProperty("Append", "'Sorry, I have to find the intruders and show them the great works of art. You know, the usual.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
        TL_SetProperty("Append", "'Hee hee hee! Okay! I'm sure they'll fit right in!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
    
        --Normal:
        if(gzTextVar.iGameStage < 6) then
            
            --First time:
            if(gzTextVar.gzPlayer.bHasSpokenToDoll == false) then
                gzTextVar.gzPlayer.bHasSpokenToDoll = true
            
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Hello, sister! Oh, you're new, aren't you?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'Brand new, sister,' you reply." .. gsDE)
                TL_SetProperty("Create Blocker")
            
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'You're really cute! Want to play hide and seek?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'Perhaps later. I have some things to take care of for the creator.'" .. gsDE)
                TL_SetProperty("Create Blocker")
            
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Oooh, important task?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'Every task for the creator is important.'" .. gsDE)
                TL_SetProperty("Create Blocker")
            
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Hee hee! Okay! See you later!' the doll says with a wave." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            else
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Hi, sister Mary!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'How are you doing?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'I'm a little sad because it's raining so hard. I hope it lets up soon.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'It will. I know it.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Want to play later?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'Funny, I was just about to ask you if you've ever played 'Go Fish'. Have you?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'No! Is that a new game?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
                TL_SetProperty("Append", "'Yay! I get to teach everyone a new game! That will be so fun!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You still have things to do for your creator, but you can't wait to teach your sister dolls how to play a new game!" .. gsDE)
                TL_SetProperty("Create Blocker")
            end
            
        --Phase 6.
        else
        
            --Jessie and Lauren are still human:
            if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Human") then
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Sister, are you taking care of these humans?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
                TL_SetProperty("Append", "'I am taking them to the creator. They're going to be so cute!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Tee hee! Okay!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
                TL_SetProperty("Append", "Seems the doll girl bought it..." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Betrayal!
            else
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Sister, do you need help carrying our new friends?' the doll says, gesturing at the two blank dolls you are carrying." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
                TL_SetProperty("Append", "'No, but thank you for offering.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", gzTextVar.zEntities[gzTextVar.iCurrentEntity].sDisplayName, gzTextVar.zEntities[gzTextVar.iCurrentEntity].sQueryPicture, ciImageLayerDefault)
                TL_SetProperty("Append", "'Don't take too long, I can't wait to play with them!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
                TL_SetProperty("Append", "Neither can you..." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            end
        end

    end

end