--[ ========================================== Pygmalie ========================================= ]
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "talk",                  "talk "   .. sEntityName)
    
    --Player is a doll:
    else
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
    end
    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look pygmalie") then
    
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "An imposing woman. She's smiling at you, but there is an insincerity to that smile." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "Your true creator. You will OBEY HER COMMANDS. She is the originator of the light which controls you." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "An imposing woman who controls your will. You want to help her capture humans and cover them in clay." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Doll version.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "Your creator and mistress. She is perfect in every way, and you want to do your best for her. She wants you to help her create many new sisters." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    end
    
--Attacking.
elseif(sInstruction == "attack pygmalie") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "Pygamlie is defenseless and is no threat to you. You cannot bring yourself to attack her." .. gsDE)
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Append", "YOU WILL NOT HARM THIS WOMAN. You will not harm this woman. You are a statue, you obey the light." .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Append", "You could never attack the woman who controls your will." .. gsDE)
    
    --Doll version.
    else
        TL_SetProperty("Append", "You would never attack your creator unless she directly ordered you to." .. gsDE)
    
    end

--Talking to.
elseif(sInstruction == "talk pygmalie") then
    gbHandledInput = true
    
    --Fast-access strings.
    local iMaryCnt = 1
    local sMaryImg = gzTextVar.gzPlayer.sQuerySprite
    local saMaryList = {sMaryImg}
    local iPygmCnt = 1
    local sPygmImg = gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sQueryPicture
    local saPygmList = {sPygmImg}

    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --First time.
        if(gzTextVar.bSpokeToPygmalie == false) then
            
            --Flag.
            gzTextVar.bSpokeToPygmalie = true
            
            --Dialogue.
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Hello, darling. Aren't you adorable?' the lady says, 'My name is Pygmalie. Pleased to meet you.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Mary,' you reply." .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "She cocks her head to one side. 'Mary? What a sweet name.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I'm not sure how I got here. Where are we? What is this place?' you ask." .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Ha ha ha! Those are complicated and very silly thoughts, little one. You don't need to have those thoughts, no, not at all.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Come again? What are you talking about?'" .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'No deference, no submission. You're not ready yet, but you will be. I can't wait!'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Despite looking at you and speaking to you, Pygmalie only seems to be partially aware of your presence. She doesn't seem dangerous, though." .. gsDE)
        
        --Repeats.
        else
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Hello, darling. Aren't you adorable?' the lady says, 'My name is Pygmalie. Pleased to meet you.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Mary. I already told you that,' you reply." .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "She cocks her head to one side. 'Mary? What a sweet name.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Are you all right? Do you live here by yourself? Should I call someone?'" .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Perfectly all right, yes yes. Your concern...'" .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "She places a hand on her heart." .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'It's so touching to see a little cutie like you worrying about me. You'll be so happy.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Pardon?'" .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Run along now, you've games to play. Scoot, scoot, let me do my work! Come back for cookies in an hour!'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Pygmalie returns to flitting about the room and scratching on the canvas there with a paintbrush that has no paint on it." .. gsDE)
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "You stand before the woman. She smiles at you." .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "FIND THE INTRUDERS. TAKE THEM TO THE LIGHT. You are not sure whether that thought came from the light, or the woman." .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "IT DOES NOT MATTER. YOU ARE A STATUE. YOU DO NOT SPEAK, YOU DO NOT THINK. OBEY." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You realize it does not matter who the thought came from. You are a statue, you have no thoughts. You obey." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The light will tell you where they are if you use the [think] command." .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You stand before the woman. She smiles at you. You attempt to speak, but nothing comes out." .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Hard to speak without a mouth or lungs, isn't it sweetie?' she says with a light, high-pitched laugh." .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "The woman is right. You feel your head shift as her thoughts begin changing yours." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You need to find the humans and cover them in clay. Your will changes to suit your creator's. You nod at her, and she goes back to what she was doing." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You can probably use the [think] command to figure out where they are." .. gsDE)
    
    --Doll version.
    else
    
        --Player is crippled. They are being transformed.
        if(gzTextVar.gzPlayer.bIsCrippled == true) then
            TL_SetProperty("Append", "You cannot speak until your creator orders you to." .. gsDE)
    
        --Player is free to move.
        elseif(gzTextVar.gzPlayer.bDollMoveLock == false) then
        
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Hello, darling. Aren't you adorable?' your creator says. You smile and bask in her glowing attention." .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I am pretty because you made me pretty,' you say. Your creator pats your head gently." .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh, but, I think there are new sisters for you to make, aren't there?' she asks you." .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Your joints harden for a moment, your head lolling to one side as a thought enters your head." .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Suddenly you snap to attention. 'Yes, creator. I'll go play with them. Thank you.'" .. gsDE)
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "You are answered by the beaming smile of your creator. You are a good dolly for letting the thought in!" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You curtsey at your creator. She wants you to go play with your new sisters. They should be elsewhere in the manor. You will go searching for them and play with them." .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You can probably use the [think] command to figure out where they are. You're so smart!" .. gsDE)
    
        --Player remains here.
        else
            fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh my sweet, you've done so well. Please help me with your new sister.'" .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You are bursting with pride as your creator showers you with attention. You curtsey to her and stand nearby." .. gsDE)
            fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You eagerly await her orders as she goes about making a new sister to play with." .. gsDE)
        end
    end

--Transform? Yeah, not gonna work. Usually.
elseif(sInstruction == "transform pygmalie") then
    gbHandledInput = true
    
    --Fast-access strings.
    local iMaryCnt = 1
    local sMaryImg = gzTextVar.gzPlayer.sQuerySprite
    local saMaryList = {sMaryImg}
    local iPygmCnt = 1
    local sPygmImg = gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sQueryPicture
    local saPygmList = {sPygmImg}
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "That does not make sense. Transform her into what?" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Claygirl:
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You remove some of your clay and offer it to your beloved creator. She chuckles." .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'It's very cute, but I don't need your clay. Maybe I will make you into a vase later! Ha ha ha ha!'" .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "Your creator lavished you with attention! Your clay heart is filled with pride. For now, though, you have humans to find." .. gsDE)
    
    --Statue:
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "The woman who commands your will stares at you. You feel the light surge within you." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "THIS WOMAN IS NOT AN INTRUDER. YOU DO NOT THINK SHE IS AN INTRUDER. YOU DO NOT THINK. YOU OBEY." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The woman is not an intruder. You do not think. You are a statue, statues do not think. You obey." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "FIND THE INTRUDERS. TAKE THEM TO THE LIGHT. THEY MUST SEE THE LIGHT. OBEY." .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You must find the intruders. You must take them to the light. They must see the light. You obey." .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "The woman barely notices you as you leave. You are an unremarkable statue. She returns to what she was doing." .. gsDE)
    
    --Doll.
    else
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "Your creator lets out a shrill laugh. You can't help but giggle along at whatever joke she's laughing at!" .. gsDE)
        fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh, my sweet beloved dolly! You don't play with me, I play with you? Play with you! I - I play with you!'" .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "The giggles swell up in you. Your creator enjoyed your silly joke! Well done!" .. gsDE)
        fnDialogueCutscene("You",      iMaryCnt, saMaryList, "You laugh a few more times as you decide to find some more silly humans to play with. Maybe they will like your joke, too!" .. gsDE)
    
    end
    
--Volunteering.
elseif(sInstruction == "volunteer" or sInstruction == "surrender") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Skin color letter.
        local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'
    
        --Change the player's state to blank doll.
        gzTextVar.gzPlayer.sFormState = "Blank Doll"
        
        --Transformation sequence.
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "You approach Pygmalie. She's chuckling to herself about something. You tug at her sleeve." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She gives you a sideways glance. You close your eyes." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I give up.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'Give up, darling? Give up what? You're so silly!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "She doesn't seem to understand. Perhaps if you changed your approach..." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I'm a very silly dolly, aren't I?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'Yes, but you're not quite done.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "'Please complete me, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She smiles softly and places her hand on your shoulder. You feel the warmth of the touch. It tingles. You smile." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You need to practice being empty, so you try not to think. Pygmalie - your creator - reaches into her smock and produces a small needle." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'Prove your devotion, dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
        TL_SetProperty("Append", "Taking the needle, you stick it into the vein on one arm, then the other. Finally, you stick it into the vein on your leg. The white fluid courses into you." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'Did I do well, creator?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'Perfectly. You will be my best, most obedient dolly, won't you?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
        TL_SetProperty("Append", "'Yes, creator,' you say. The changes have begun. Your arms and legs are changing, becoming plastic. The excitement builds within you as the numbness crosses your body." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Your creator takes hold of your head, clutching it softly. She seems to be putting thoughts in. You know they are coming, but you cannot understand them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF1", ciImageLayerDefault)
        TL_SetProperty("Append", "A giggle escapes your mouth as the changes proceed. Your body is mostly plastic now, and your hair is falling out. You are becoming more perfect." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You realize your creator was not putting thoughts into your head, but emptiness. You realize you can hardly think about what you are thinking about." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "And then, you think about nothing at all. Your head bobs to the side as your new doll body goes limp." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I will be your best dolly, creator...' you say. Your mouth goes numb." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)
        TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
        gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
        TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
        TL_SetProperty("Append", "Your thoughts become empty. Your mind is as empty as your smooth, plastic body. You are a featureless blank slate, waiting to be molded into some other shape." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        --Optional, skin color change.
        if(sEndLetter ~= "C") then
            TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
            gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter
            TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
            TL_SetProperty("Append", "Your creator smirks and changes the color of your skin to suit her desires. She is proud of your new, shiny, perfect body." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
        
        TL_SetProperty("Append", "Able to see but not able to think, you gaze into infinity as an unfinished puppet. " .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Doll.
    else
        TL_SetProperty("Append", "You are already ready to fulfil your creator's every whim." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
end