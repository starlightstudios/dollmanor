--[Claygirl TF]
--Transforms Lauren into a claygirl.
local i = gzTextVar.iLaurenIndex

--Determine clay color.
local iIndicatorX = 6
local sMyClayColor = string.sub(gzTextVar.gzPlayer.sQuerySprite, 42)
local sLaurenClayColor = "Blue"
if(sMyClayColor == "Blue") then
    sLaurenClayColor = "Red"
    iIndicatorX = 7
elseif(sMyClayColor == "Yellow") then
    sLaurenClayColor = "Blue"
    iIndicatorX = 6
else
    sLaurenClayColor = "Yellow"
    iIndicatorX = 8
end

--Change Lauren's state to claygirl.
gzTextVar.zEntities[i].sState = "Claygirl"
gzTextVar.bIsLaurenFollowing = false
gzTextVar.zEntities[i].bIsCrippled = false
gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/ClayGirl" .. sLaurenClayColor

--Transformation sequence.
TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
TL_SetProperty("Append", "Unable to resist, Lauren pushes desperately to get away. You drop a glob of clay onto him." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenClay" .. sLaurenClayColor .. "TF0", ciImageLayerDefault)
TL_SetProperty("Append", "The clay splashes on him. He dislikes the cool sensation and tries to brush it off, which only causes it to spread further." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Somehow, your clay changes color as it merges with his skin. It spreads and multiplies quickly." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Fighting in vain, the clay spreads and overtakes him. His legs disintegrate and he collapses into a puddle of his own clay." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iLaurenIndex, iIndicatorX, 4, ciCodeFriendly)
TL_SetProperty("Append", "A few moments later, Lauren stands up. She surveys her new body, feeling it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/ClayGirl" .. sLaurenClayColor, ciImageLayerDefault)
TL_SetProperty("Append", "She tries to shape her face into a mouth, perhaps to call for help. You grasp at her and smooth her face. She doesn't try to stop you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You shape the rest of her body, crafting her hair and giving her two breasts. She feels the new chest briefly." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You withdraw and allow your fellow clay girl to acclimate to herself. She has an important task. You must make sure she completes it." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Gingerly, Lauren reaches within her head. She needs to reshape herself. It takes a few minutes for her to squeeze and twist." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You feel somewhere far away that the task is done. Lauren is just like you. A clay girl. Forever." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Unregister Image")