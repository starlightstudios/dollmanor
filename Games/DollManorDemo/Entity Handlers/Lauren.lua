--[ =========================================== Lauren ========================================== ]
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Other.
local i = gzTextVar.iLaurenIndex
local p = gzTextVar.iJessieIndex
    
--Fast-access strings.
local iMaryCnt = 1
local sMaryImg = gzTextVar.gzPlayer.sQuerySprite
local saMaryList = {sMaryImg}

local iLaurCnt = 1
local sLaurImg = gzTextVar.zEntities[i].sQueryPicture
local saLaurList = {sLaurImg}
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

local iJessCnt = 1
local sJessImg = gzTextVar.zEntities[p].sQueryPicture
local saJessList = {sJessImg}

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
    
    --Player is a statue:
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
        if(gzTextVar.zEntities[i].sState ~= "Statue") then
            TL_SetProperty("Register Popup Command", "attack", "attack " .. sEntityName)
        end
    
    --Player is a claygirl:
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
        
        --Attack option for humans.
        if(gzTextVar.zEntities[i].sState == "Human") then
            TL_SetProperty("Register Popup Command", "attack", "attack " .. sEntityName)
        end
            
        --If Lauren is downed:
        if(gzTextVar.zEntities[i].sState == "Human" and gzTextVar.zEntities[i].bIsCrippled == true) then
            TL_SetProperty("Register Popup Command", "transform", "transform " .. sEntityName)
        end
        
    --Player is a doll:
    else
    
        --Lauren is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            TL_SetProperty("Register Popup Command", "play", "play " .. sEntityName)
            TL_SetProperty("Register Popup Command", "talk", "talk " .. sEntityName)
            
            --If Lauren is downed:
            if(gzTextVar.zEntities[i].sState == "Human" and (gzTextVar.zEntities[i].bIsCrippled == true or gzTextVar.iDollLaurenSpeak == 3)) then
                TL_SetProperty("Register Popup Command", "transform", "transform " .. sEntityName)
            end
    
        --Jessie is a blank doll:
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
        
        
        end
    end
    
    --Pulse ends here.
    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look lauren") then
    
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.iGameStage ~= 5) then
            TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
            TL_SetProperty("Append", "Your kid brother, Lauren is frequently bullied and always comes running to you for help. You've made a point of protecting him for years now, but you're not sure you'll be able to in this strange place." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Dumb-dumb.
        else
            TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
            TL_SetProperty("Append", "A little boy. He's crying. Awww, he must be so sad! But the girl here seems to be comforting him, and she told you to do something." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "Still, you really wish you could make him feel better. Something... is telling you that it's really important you keep him safe..." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
        
        --Lauren is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                TL_SetProperty("Append", "The intruder. You must SUBDUE HIM. You must BRING HIM TO THE LIGHT. YOU ARE A STATUE. YOU WILL OBEY." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Append", "You must subdue Lauren. You must bring Lauren to the light. You are a statue. You will obey." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Secret!
            else
                TL_SetProperty("Append", "The intruder. You must BRING HIM TO THE LIGHT. YOU ARE A STATUE. YOU WILL OBEY." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Append", "Lauren does not know you are a statue. You must bring Lauren to the light. You are a statue. You will obey." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
        
        --Statue.
        else
            TL_SetProperty("Append", "An unremarkable statue, standing in the gallery amongst dozens of others. He is as obedient as you." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
        
        --Normal:
        if(gzTextVar.bIsClaySequence == false) then
            --Lauren is a human:
            if(gzTextVar.zEntities[i].sState == "Human") then
                TL_SetProperty("Append", "A faintly familiar human boy. You seem to recall thinking of him recently..." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Append", "Of course! You're supposed to cover him in clay and make him like you." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Statue.
            else
                TL_SetProperty("Append", "A girl made of clay. Her body flows over itself and is constantly reforming." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
        
        --Clay sequence.
        else
            TL_SetProperty("Append", "Lauren, your former little brother, now your clay sister. Her skin has become a splendid yellowish hue. She seems happy with her new body." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "This is Mary, an identical rubber clone of you. Or were you the clone? It's hard to say. Her grin is painted on and her body shines even in darkness." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
        TL_SetProperty("Append", "Lauren, a sculpture of ice. He was your kid brother, but you sucked the heat from him and turned him into this. That would bother you, if anything bothered you." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
    
        if(gzTextVar.zEntities[i].sState == "Human") then
            TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
            TL_SetProperty("Append", "One of the human intruders. He bears an uncanny resemblence to one of the boys in the chapel windows." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        else
            TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
            TL_SetProperty("Append", "A glass sculpture in a shape of Lauren, the boy in the chapel windows. He is so finely crafted you could swear he was alive. But, alas, he is not." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
    
    --Doll version.
    else
        --Lauren is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            --String trap:
            if(gzTextVar.iMaryStringTrapState == 3) then
                TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
                TL_SetProperty("Append", "Lauren, your little brother from when you were human. Small and frail, he would love to be plastic. Lead him to the strings." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Normal cases:
            elseif(gzTextVar.iGameStage ~= 6) then
                TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
                TL_SetProperty("Append", "A human you have no attachment to. Small and frail, you should play with him and make him a new sister." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Phase 6.
            else
                TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerDefault)
                TL_SetProperty("Append", "Your kid brother, but kid is the wrong word. He looks stronger now. Older. The very thought fills you with pride." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Lauren is a blank doll:
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
            TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
            TL_SetProperty("Append", "A puppet containing the soul of Lauren. Your creator wills you to bring her to the main hall. She will be completed there." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Special geisha doll:
        elseif(gzTextVar.zEntities[i].sState == "Geisha Doll Special") then
            TL_SetProperty("Register Image", "Jessie", gzTextVar.zEntities[gzTextVar.iLaurenIndex].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "Your brother transformed into your elegant geisha sister. She carries herself with refined grace and quiet dignity." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Because she was a good little dolly, she has been allowed to keep most of her intelligence. At least, until your creator decides to make her a silly dolly! You can't wait!" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --All other cases.
        else
            local sUseSkin = "Root/Images/DollManor/Transformation/GeishaTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
            
            --Resolve the base.
            if(string.sub(gzTextVar.zEntities[i].sState, 1, 5) == "Bride") then
                sUseSkin = "Root/Images/DollManor/Transformation/BrideTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 6) == "Dancer") then
                sUseSkin = "Root/Images/DollManor/Transformation/DancerTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 6) == "Geisha") then
                sUseSkin = "Root/Images/DollManor/Transformation/GeishaTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 4) == "Goth") then
                sUseSkin = "Root/Images/DollManor/Transformation/GothTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 7) == "Princess") then
                sUseSkin = "Root/Images/DollManor/Transformation/PrincessTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 4) == "Punk") then
                sUseSkin = "Root/Images/DollManor/Transformation/PunkTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
            end
            
            --Set.
            TL_SetProperty("Register Image", "Lauren", sUseSkin, ciImageLayerSkin)
            TL_SetProperty("Register Image", "Lauren", sLaurImg, ciImageLayerOverlay)
            TL_SetProperty("Append", "A puppet containing the soul of Lauren. You are patiently waiting for your creator to finish her so you can play with her." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        end
    end
    
--Attacking.
elseif(sInstruction == "attack lauren" or sInstruction == "play lauren") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.iGameStage ~= 5) then
            
            --Attack version:
            if(sInstruction == "attack lauren") then
                TL_SetProperty("Append", "He probably wouldn't appreciate that." .. gsDE)
            --Play version:
            else
                TL_SetProperty("Append", "Now is probably not the time for hide-and-go-seek with your kid brother." .. gsDE)
            end
    
        --Dumb-dumb.
        else
            TL_SetProperty("Append", "Attack? What's that? Seems like one of those words you'd have to look up first. At. Tack. Hee hee!" .. gsDE)
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
        --Lauren is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                --Not crippled.
                if(gzTextVar.zEntities[i].bIsCrippled == false) then
                    local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Lauren Victory.lua"
                    local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Lauren Defeat.lua"
                    LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
                
                --Crippled, can't attack.
                else
                    TL_SetProperty("Append", "INTRUDER HAS BEEN SUBDUED. BRING HIM TO THE LIGHT. OBEY." .. gsDE)
                end
            
            --Secret.
            else
                TL_SetProperty("Append", "INTRUDER IS NOT AWARE. DO NOT ATTACK. BRING HIM TO THE LIGHT. OBEY." .. gsDE)
            end
        
        --Lauren is a statue.
        else
            TL_SetProperty("Append", "LOCATE INTRUDER. SUBDUE INTRUDER. You have not been ordered to attack the statue. You will locate the intruder. You will subdue the intruder." .. gsDE)
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
        --Lauren is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Lauren Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Lauren Defeat.lua"
                LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
            
            --Crippled, can't attack.
            else
                TL_SetProperty("Append", "You should [transform] the injured boy. You want to cover him in clay." .. gsDE)
            end
        
        --Lauren is a Claygirl.
        else
            TL_SetProperty("Append", "There is no point in attacking a fellow claygirl." .. gsDE)
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "You can't attack a clone of yourself! You can only smear rubber on others, and this clone is already covered!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You can't work up the motivation to attack him." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.zEntities[i].sState == "Human") then
            --Not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Lauren Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Lauren Defeat.lua"
                LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
            
            --Crippled, can't attack.
            else
                TL_SetProperty("Append", "The human has been subdued. You should [transform] him now." .. gsDE)
            end
        else
            TL_SetProperty("Append", "You would never strike such a marvelous work of art." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Doll version.
    else
    
        --Lauren is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            --String trap:
            if(gzTextVar.iMaryStringTrapState == 3) then
                TL_SetProperty("Append", "Lauren doesn't suspect a thing. Let the trap that transformed you work on him, and enjoy the show." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
            --Normal cases:
            elseif(gzTextVar.iGameStage ~= 6) then
    
                --Reached conversation stage 3.
                if(gzTextVar.iDollLaurenSpeak == 3) then
                    TL_SetProperty("Append", "Lauren is waiting for you to [transform] him, and won't put up a fight." .. gsDE)
        
                --Not crippled.
                elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                    local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Lauren Victory.lua"
                    local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Lauren Defeat.lua"
                    LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
                
                --Crippled, can't attack.
                else
                    TL_SetProperty("Append", "Lauren looks all tuckered out. Shame that the fun has to stop now! You should [transform] him." .. gsDE)
                end
    
            --Phase 6:
            else
                TL_SetProperty("Append", "After all that has happened, you could never bring yourself to attack Lauren. And - he might just kick your butt, now!" .. gsDE)
            end
    
        --Lauren is a blank doll.
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
            TL_SetProperty("Append", "The dolly is incomplete. Your creator wants to complete her. Take her to your creator." .. gsDE)
        
        --Anything else.
        else
            TL_SetProperty("Append", "You could never bring yourself to attack one of your sisters unless you creator directly ordered it." .. gsDE)
        end
    
    end

--Talking to.
elseif(sInstruction == "talk lauren") then
    gbHandledInput = true
    
    --If Lauren is a blank doll, nothing happens.
    if(gzTextVar.zEntities[i].sState == "Blank Doll") then
        TL_SetProperty("Append", "Lauren is surprisingly untalktative. Your creator will be able to help her!" .. gsDE)
        TL_SetProperty("Create Blocker")
        
    --Human version.
    elseif(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Stage 1: Player is leading Jessie and Lauren to the Main Hall:
        if(gzTextVar.iGameStage == 1) then
        
            --Hasn't talked to Jessie yet.
            if(gzTextVar.bIsLaurenFollowing == false) then
                
                --Lauren is waiting in the main hall:
                if(gzTextVar.bIsLaurenMainHalled == true) then
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Have you seen Jessie yet? I'm worried about her..." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Not yet. Anything happen while I was away?'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Pygmalie seems to think I'm a girl, but she keeps to herself and just smiles at me.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll go look for Jessie. Stay here.'" .. gsDE)
                    fnDialogueFinale()
                
                --Lauren is not following and not in the main hall:
                elseif(gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                    gzTextVar.bIsLaurenFollowing = true
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary! Oh thank the Lord, I was so scared!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Well don't be. I'm here now.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'What happened? Where are we? Who are those plastic girls?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know. All I know is we're getting out of here. Follow me.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you sure?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This place is dangerous, but I think I know someplace safe. C'mon.'" .. gsDE)
                    fnDialogueCutscene("Nobody",        0,   {"Null"}, "[Lauren will now follow you. You should escort him back to the Main Hall.]" .. gsDE)
                    fnDialogueFinale()
            
                --Jessie is following and not in the main hall:
                elseif(gzTextVar.bIsJessieFollowing == true and gzTextVar.bIsJessieMainHalled == false) then
                    gzTextVar.bIsLaurenFollowing = true
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary! Jessie! You're safe! Oh thank the Lord, I was so scared!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Well don't be. We're here now.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You okay, kiddo?'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Yeah, I hid in here and those pale girls didn't notice me. Who are they?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know. All I know is we're getting out of here. Follow me.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you sure?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This place is dangerous, but I think I know someplace safe. C'mon.'" .. gsDE)
                    fnDialogueCutscene("Nobody",        0,   {"Null"}, "[Lauren will now follow you. You should escort him back to the Main Hall.]" .. gsDE)
                    fnDialogueFinale()
            
                --Jessie is in the main hall:
                elseif(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieFollowing == true) then
                    gzTextVar.bIsLaurenFollowing = true
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary! Oh thank the Lord, I was so scared!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Well don't be. I'm here now.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'What happened? Where are we? Who are those plastic girls?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know. All I know is we're getting out of here. Follow me.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you sure?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This place is dangerous, but I think I know someplace safe. C'mon, Jessie's waiting near the entrance.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You found Jessie?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We can figure out what to do when we meet up with her.'" .. gsDE)
                    fnDialogueCutscene("Nobody",        0,   {"Null"}, "[Lauren will now follow you. You should escort him back to the Main Hall.]" .. gsDE)
                    fnDialogueFinale()
                end
            
            --Has talked to Lauren, on the way to the Main Hall.
            elseif(gzTextVar.bIsLaurenFollowing == true and gzTextVar.bIsLaurenMainHalled == false) then
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Where are we going?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The entranceway. There was a lady there. I think she lives here.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is she nice?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I think she's a little off, but not dangerous.'" .. gsDE)
                fnDialogueFinale()
            end
    
        --Phase 2: Find the crystal pieces.
        elseif(gzTextVar.iGameStage == 2) then
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... I'm scared...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Come on, tough guy. Be strong.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It's so cold and lonely and... *sniff*'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'When we get home I'll make you a big mug of hot chocolate and we'll snuggle with Jessie's puppy. Okay?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh yeah! Totally. Just imagine it, Lauren. We're almost out of here.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'...'" .. gsDE)
            fnDialogueFinale()
        
        --Phase 3: Find the journal.
        elseif(gzTextVar.iGameStage == 3) then
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... Where are we? Are we on Earth anymore?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't know, Lauren.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'How are we going to get home if we're not even on Earth...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren, if I have to, I will build you a spaceship.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I get to be captain!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'No! I wanna be captain!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Then I get to be first officer!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'What's a first officer?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The one who does everything while the captain smokes his pipe and makes decisions.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The captain has to be very brave on a starship.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Okay!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I'll be brave, but you promised I could be captain.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Deal.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Deal.'" .. gsDE)
            fnDialogueFinale()
        
        --Phase 4: Find the storybook.
        elseif(gzTextVar.iGameStage == 4) then
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary! Sarah is my friend!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Great! What are you two talking about?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Unh uh. Can't tell. It's a secret.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'But I'm your sister! Are you keeping secrets from me?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'She's showing me all the symbols and stuff. I'm gonna learn them and then I'll protect you with them.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Wow! Sounds tough. I know you can do it.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Kay! I'll tell Sarah all the nice things you've done. I think she likes you too.'" .. gsDE)
            fnDialogueFinale()
        
        --Phase 5: Dumb dumb.
        elseif(gzTextVar.iGameStage == 5) then
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... Hi...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hi! What's your name?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You forgot my name?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Uh oh... is that bad?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*Psst. It's Lauren!*'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Wait! Your name is Lauren!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I saw Jessie whisper in your ear. I'm not a kid, I'm almost thirteen!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're not a kid?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'No!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Okay! Great! Thanks for clearing that up!'" .. gsDE)
            fnDialogueFinale()
    
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
        --Lauren is not a statue.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                
                --Lauren is not crippled.
                if(gzTextVar.zEntities[i].bIsCrippled == false) then
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... *sniff* Please don't be... gone...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "SUBDUE THE INTRUDER. BRING HIM TO THE LIGHT. OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The intruder is talking to you. He is crying." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU FEEL NOTHING." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You feel nothing. You are a statue. But..." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU FEEL NOTHING. YOU ARE A STATUE. OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The boy continues to cry. His tears stream down his face..." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU FEEL NOTHING. YOU FEEL NOTHING. YOU ARE A STATUE. STATUES FEEL NOTHING. STATUES ARE STONE." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You - you know what to do. You must subdue the intruder. You must bring him to the light. You are a statue, statues obey." .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'P-p-please stay back...' he cries." .. gsDE)
                    fnDialogueFinale()
                    local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Lauren Victory.lua"
                    local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Lauren Defeat.lua"
                    TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
                
                --Crippled.
                else
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... Wake up...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "THERE IS NO MARY. THERE IS ONLY THE LIGHT. YOU ARE A STATUE. YOU OBEY." .. gsDE)
                    fnDialogueFinale()
                end
        
            --Secret statue.
            else
        
                --In the Strange Antechamber:
                if(gzTextVar.gzPlayer.sLocation == "Strange Antechamber") then
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'What's in there, anyway?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "LEAD THE INTRUDERS TO THE LIGHT, OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Something you will like.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Such as?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You will like it. It will make you happy.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is it a surprise?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yes. It made me happy. Follow me.'" .. gsDE)
                    fnDialogueFinale()
        
                --In the hypnotic hallway:
                else
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'My stomach feels... ugh... I'm gonna...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "LEAD THE INTRUDERS TO THE LIGHT, OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I obey.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'*Hurk*...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Follow me. You will feel better soon.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you sure?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Follow me. I am certain. You will feel better soon.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Okay...'" .. gsDE)
                    fnDialogueFinale()
        
                end
            end
            
        --Lauren is a statue.
        else
            TL_SetProperty("Append", "YOU ARE A STATUE. YOU HAVE NO THOUGHTS. YOU DO NOT SPEAK." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "You are a statue. You have no thoughts. You do not speak. You will FIND THE INTRUDERS. You will SUBDUE THE INTRUDERS. You will BRING THE INTRUDERS HERE." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
        --Lauren is not a Claygirl.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Lauren is not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary? Is that you? I can't tell - you have no face!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You do not answer. You have one task, and that is to cover humans in clay. You must make them like you." .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'P-p-please stay back...' he cries." .. gsDE)
                fnDialogueFinale()
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Lauren Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Lauren Defeat.lua"
                TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
            
            --Crippled.
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... Is it really you? Please...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You should [transform] the boy before he suffers any more injuries." .. gsDE)
                fnDialogueFinale()
            
            end
        
        --Lauren is a Claygirl.
        else
            --Normal:
            if(gzTextVar.bIsClaySequence == false) then
                TL_SetProperty("Append", "You attempt to speak with Lauren, but no words come out. Instead, you place your hand onto her face where her mouth would be." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Thoughts exchange between you using the clay on your arm as a conduit. Lauren is happy to be a clay girl, and thanks you for changing her." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "She wants to stay together forever, as a family. You return the sentiment. You love being clay, and so does she." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You ask her if there was something you needed to do. Something about escape. She says you're supposed to prevent humans from escaping." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You withdraw your arm and Lauren begins to slither away. You'll need to search for humans and cover them in clay. You need to make them like you." .. gsDE)
                TL_SetProperty("Create Blocker")
            
            --Special:
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You protrude a clay tendril and touch it to Lauren, allowing you to hear one another's thoughts." .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, thank you so much. You were right, this is spectacular.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I can change my shape however I want, look like whatever I want.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're welcome, Lauren. I really felt bad for trying to trick you.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I completely understand. Transforming me was the right thing to do.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I know. Come on, let's show our creator how pretty you are!'" .. gsDE)
                fnDialogueFinale()
            
            end
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "You approach your identical clone. Neither of you can speak, as your grins are merely permanently painted on. Still, you smile blankly at each other." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You feel some faint familial attachment to your identical clone. It will be fun to play with her later!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Hello, sister.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hello, brother.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is something on your mind?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I know I should feel guilt for what I did to you, but I don't.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I should hold a grudge, but I don't. So it balances out.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What do you want to do once we've shown ourselves to the creator?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Nothing.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Me too.'" .. gsDE)
        fnDialogueFinale()
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.zEntities[i].sState == "Human") then
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary! No!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You've got everyone up in arms. Please don't put up a fight.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Just give me my sister back! No!' the boy says, fighting back tears." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You know, you look a lot like a character I saw once. Come, I'll show you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The boy is making trouble, you'll need to [attack] him before he'll come quietly." .. gsDE)
                fnDialogueFinale()
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Don't hurt me...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't want to, but you humans are rowdy. Come on, I'll show you something pretty and maybe that will calm you down.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You can [transform] Lauren now." .. gsDE)
                fnDialogueFinale()
            end
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Hey Mary! I dreamed I was your brother! It was so weird!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Wow, I dreamed I was your sister!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Cool!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'It felt so natural. Hey, do you want to be my sister?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sure, I'll protect you from bullies and teach you about the world! Don't cry, Lauren! Ha ha!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Wow, this will be so fun! We should ask Jessie to come along, too! She can be our best friend!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Oh, we can write down the fun stories we make up!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Great idea, Lauren. I'm sure our creator will love reading about our adventures.'" .. gsDE)
            fnDialogueFinale()
        end
    
    --Doll version.
    else
    
        --String trap:
        if(gzTextVar.iMaryStringTrapState == 3) then
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, what's the trap in this room? How does it work?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There are grooves on the floor. If you step on the wrong board, it closes the jewel room.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Step where I step, and watch the floor carefully.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Wow, Mary, you're so smart! I want to be just like you some day!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You will be, sooner than you think.'" .. gsDE)
            fnDialogueFinale()
            
        --Normal cases:
        elseif(gzTextVar.iGameStage ~= 6) then
            
            --Special geisha doll:
            if(gzTextVar.zEntities[i].sState == "Geisha Doll Special") then
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Sister? What is on your mind?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We seem to have retained some of our wits.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I did not struggle. Did you?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Not when I knew I was done. It was for the best.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Will our creator give Jessie back her intelligence?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I do not think so, and I do not think she will let us keep ours. A silly doll is an obedient doll.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'But I would never betray her. I love her with every ounce of plastic I am made of.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We all do, but the creator is wise and gracious. She made our sisters dumb for a reason.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It is not our place to ask questions.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Of course. I hope she thinks I'm cute!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're the prettiest dolly here, Lauren. I'm proud of you.'" .. gsDE)
                fnDialogueFinale()
                
            --Lauren is not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                
                --First conversation.
                if(gzTextVar.iDollLaurenSpeak == 0) then
                    gzTextVar.iDollLaurenSpeak = 1
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Please stay away. Please...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You let out a small chuckle to yourself. The little boy is scared, but there's nothing to be scared of." .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Don't - I'll call Mary! She'll beat you up, so you just stay back!'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary's my big sister and she's the toughest girl ever! She'll whallop you good!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Mary? But that's my name.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'B-but - Your voice... Sis? Is that you?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Perhaps you should keep talking to the little boy. A good dolly always reassures the scared and lonely kids in the world." .. gsDE)
                    fnDialogueFinale()
                
                --Second conversation.
                elseif(gzTextVar.iDollLaurenSpeak == 1) then
                    gzTextVar.iDollLaurenSpeak = 2
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'So those doll girls I've seen... Does that mean they used to be people?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're a funny little one! Dolls were never people, they're dollies!'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary... Did they get you?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nobody got me. I've always been this way.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I can't believe it... *sniff* I want my big sister back, you dumb meanie!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The little boy is on the verge of tears. You should do something! You're failing as a dolly!" .. gsDE)
                    fnDialogueFinale()
                    
                --Third conversation.
                elseif(gzTextVar.iDollLaurenSpeak == 2) then
                    gzTextVar.iDollLaurenSpeak = 3
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't cry, Lauren! It'll be okay!'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I want my sister back!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll be your sister. Will you be my sister?'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'*Sniff*'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Please?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We can play together and you won't have to be sad.'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I guess that really is the only way... *sniff*'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Does it hurt?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Just a tiny bit at first, and then you'll feel wonderful.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You extend your hand, and Lauren hesitantly takes it. You can [transform] him now." .. gsDE)
                    fnDialogueFinale()
                
                --Repeat.
                else
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "The little boy is waiting for you to [transform] him. He'll make such a cute sister!" .. gsDE)
                    fnDialogueFinale()
                end
        
            --Crippled.
            else
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Mary, help...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "What a silly boy, he's asking for your help! You should [transform] him." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "He'll make such a cute sister! Your creator will be so proud." .. gsDE)
                fnDialogueFinale()
            end
        
        --Phase 6.
        else
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'What's it feel like, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What does what feel like?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Being a doll. Being made of plastic.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'm very light, and hollow, but also strong and firm.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'However it works, it made me feel like I've always been this way.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is there a way to reverse it?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Maybe, maybe not.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'And if we can't... fix you?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'There are worse fates, Lauren.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren thinks for a moment." .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I'll find a way to fix you. Just leave it to me.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'How?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I talked to Sarah. I read the books in the study. I'll go to the library everyday. I'll fix you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'll hold you to that, Lauren.'" .. gsDE)
            fnDialogueFinale()
        
        end
    end


--Talking to, about goats!
elseif(sInstruction == "talk lauren goatsicle") then
    gbHandledInput = true

    --If Lauren is a blank doll, nothing happens.
    if(gzTextVar.zEntities[i].sState == "Blank Doll") then
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Unsurprisingly, Lauren is not talkative at the moment." .. gsDE)
        fnDialogueFinale()
        
    --Human version.
    elseif(gzTextVar.gzPlayer.sFormState == "Human") then
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'... What?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nothing, just a word I thought of.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is this some kind of joke?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'That's a matter of perspective, isn't it?'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Whatever, Mary.'" .. gsDE)
        fnDialogueFinale()

    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
        --Lauren is not a statue.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "STATUES DO NOT SPEAK. STATUES OBEY." .. gsDE)
                fnDialogueFinale()
        
            --Secret statue.
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU HAVE NO ATTACHMENT TO THAT WORD. FORGET THAT WORD. OBEY." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You decide to forget about the word, and obey the light." .. gsDE)
                fnDialogueFinale()
            end
            
        --Lauren is a statue.
        else
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU HAVE NO ATTACHMENT TO THAT WORD. FORGET THAT WORD. OBEY." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You decide to forget about the word, and obey the light." .. gsDE)
            fnDialogueFinale()
        
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Your attempts to say that word fail as you have no mouth, or lungs." .. gsDE)
        fnDialogueFinale()
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Your attempts to say that word fail as you have no mouth, it's merely painted on. How silly!" .. gsDE)
        fnDialogueFinale()
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Hello, sister.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hello, brother. Goatsicle.'" .. gsDE)
        fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Does that word have some signifigance to you?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No it does not.'" .. gsDE)
        fnDialogueFinale()
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Goatsicle. A word that you seem to remember from somewhere. Oh well, not important." .. gsDE)
        fnDialogueFinale()
    
    --Doll version.
    else
    
        --String trap:
        if(gzTextVar.iMaryStringTrapState == 3) then
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'... What?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nothing, just a word I thought of.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Is this some kind of joke?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'That's a matter of perspective, isn't it?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Whatever, Mary.'" .. gsDE)
            fnDialogueFinale()
            
        --Normal cases:
        elseif(gzTextVar.iGameStage ~= 6) then
            
            --Special geisha doll:
            if(gzTextVar.zEntities[i].sState == "Geisha Doll Special") then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Does that word hold some special place in your heart, sister?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yes. It shall forever represent my love for Jessie. It was a word she told me to say if I returned her feelings.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I see. Now that she is your bride, are you glad you came here?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I am. We can be together forever, now.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Yes, we can. I'm so happy.'" .. gsDE)
                fnDialogueFinale()
                
            --Lauren is not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Is that a word you just made up? How silly!" .. gsDE)
                fnDialogueFinale()
        
            --Crippled.
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Is that a word you just made up? How silly!" .. gsDE)
                fnDialogueFinale()
            end
        
        --Phase 6.
        else
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'... What?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Nothing, just a word I thought of.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Are you trying to distract me?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No, it just popped into my head.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Whatever, Mary.'" .. gsDE)
            fnDialogueFinale()
        end
    end

--Transforming to.
elseif(sInstruction == "transform lauren") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "Why would you want to do that?" .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
        --Lauren is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
        
            --Lauren is not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                TL_SetProperty("Append", "You grasp towards Lauren, but your slow clay legs prevent you from catching him. You will need to [attack] him first." .. gsDE)
                
            --Crippled.
            else
        
                --Determine clay color.
                local iIndicatorX = 6
                local sMyClayColor = string.sub(gzTextVar.gzPlayer.sQuerySprite, 42)
                local sLaurenClayColor = "Blue"
                if(sMyClayColor == "Blue") then
                    sLaurenClayColor = "Red"
                    iIndicatorX = 7
                elseif(sMyClayColor == "Yellow") then
                    sLaurenClayColor = "Blue"
                    iIndicatorX = 6
                else
                    sLaurenClayColor = "Yellow"
                    iIndicatorX = 8
                end
                io.write("Clay color is " .. sMyClayColor .. "\n")
                io.write("Lauren clay color is " .. sLaurenClayColor .. "\n")
        
                --Change Lauren's state to claygirl.
                gzTextVar.zEntities[i].sState = "Claygirl"
                gzTextVar.bIsLaurenFollowing = false
                gzTextVar.zEntities[i].bIsCrippled = false
                gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/ClayGirl" .. sLaurenClayColor
                
                --Transformation sequence.
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
                TL_SetProperty("Append", "Unable to resist, Lauren pushes desperately to get away. You drop a glob of clay onto him." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenClay" .. sLaurenClayColor .. "TF0", ciImageLayerDefault)
                TL_SetProperty("Append", "The clay splashes on him. He dislikes the cool sensation and tries to brush it off, which only causes it to spread further." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Somehow, your clay changes color as it merges with his skin. It spreads and multiplies quickly." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Fighting in vain, the clay spreads and overtakes him. His legs disintegrate and he collapses into a puddle of his own clay." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iLaurenIndex, iIndicatorX, 4, ciCodeFriendly)
                TL_SetProperty("Append", "A few moments later, Lauren stands up. She surveys her new body, feeling it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/ClayGirl" .. sLaurenClayColor, ciImageLayerDefault)
                TL_SetProperty("Append", "She tries to shape her face into a mouth, perhaps to call for help. You grasp at her and smooth her face. She doesn't try to stop you." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You shape the rest of her body, crafting her hair and giving her two breasts. She feels the new chest briefly." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You withdraw and allow your fellow clay girl to acclimate to herself. She has an important task. You must make sure she completes it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Gingerly, Lauren reaches within her head. She needs to reshape herself. It takes a few minutes for her to squeeze and twist." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You feel somewhere far away that the task is done. Lauren is just like you. A clay girl. Forever." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Unregister Image")
            
            end
        end

    --Statue.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
                
        TL_SetProperty("Append", "BRING THE INTRUDER TO THE LIGHT. HE MUST SEE THE LIGHT. OBEY." .. gsDE)
        TL_SetProperty("Create Blocker")
                
        TL_SetProperty("Append", "You will transform the intruder by taking him to the light. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "This clone is already transformed into a rubber copy of you!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You've already transformed Lauren. He has no heat left to drain." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.zEntities[i].sState == "Human") then
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                TL_SetProperty("Append", "The human is a bit too tough to do that. You might need to [attack] him and subdue him by force." .. gsDE)
                TL_SetProperty("Create Blocker")
            else
                TL_SetProperty("Append", "You heft the human in both arms. He's injured, but not too badly. You hope you didn't overdo it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/LaurenGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'Mary... Where are you taking me?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Hush, human. I want to show you something.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/LaurenGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'Show me what?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'You'll like it, I promise,' you say. The will of your creator permeates your thoughts, telling you what to do." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, "Chapel Choir Stand E")
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, i, "Chapel Choir Stand E")
                TL_SetProperty("Append", "You carry the hurt boy to the chapel. He whimpers, but your glass hands are strong and he doesn't keep up the fight." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You set him down gently in front of one of the stained-glass portraits on the walls. Unearthly light shines through it. He stares at it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/LaurenGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'Is that... me?' he asks." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The image of Lauren, a little boy from Coventry, appears in the glass. It was always there. You forget all the times it wasn't." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You hear a hissing sound of something being blown away, like sand in the wind. You look at the boy." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenGlassTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "He has a pained expression as he remembers his past. He was a disembodied spirit, just like you. He admired the character Lauren, just as you admired Mary." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "Perhaps he is daydreaming. Perhaps not. Either way, the dream ends." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Hmm? Oh, Mary, how long have you been standing there?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Not that long. Were you daydreaming about being a human again?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Don't laugh, you do it too!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Of course I do! There's nothing wrong with that. As long as you pay homage to our creator, you can fantasize about whatever you want.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Oh, but what would it be like to be a human, for real. To eat food, to get tired and go to sleep.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Sounds difficult. I'd much rather be a work of art than one of them.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/LaurenGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'You're right, but it's fine to wonder. I think I'll keep dreaming a little more.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'All right. See you later.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You smile. Lauren has always been a lot of fun to talk to, it always leaves you in a good mood. And - what would it be like to be a real human?" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "That's not for you to wonder, but maybe you'll ask your creator about it later." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Transform Lauren.
                gzTextVar.zEntities[i].bIsCrippled = false
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 7, 5, ciCodeFriendly)
                gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/LaurenGlass"
                gzTextVar.zEntities[i].sState = "Glass"
                
            end
        else
            TL_SetProperty("Append", "Transform him into what, a human! Ha ha ha!" .. gsDE)
            TL_SetProperty("Create Blocker")
        end

    --You're a doll! Get him!
    else

        --String trap:
        if(gzTextVar.iMaryStringTrapState == 3) then
            TL_SetProperty("Append", "Lead Jessie into the string trap that transformed you. That will be so much quicker." .. gsDE)
            
        --Lauren is a human.
        elseif(gzTextVar.zEntities[i].sState == "Human") then
        
            --Lauren has been convinced.
            if(gzTextVar.iDollLaurenSpeak == 3) then
        
                --Change Lauren's state to blank doll. She now follows the player.
                gzTextVar.zEntities[i].sState = "Blank Doll"
                gzTextVar.bIsLaurenFollowing = true
                gzTextVar.zEntities[i].bIsCrippled = false
                
                --Transformation sequence.
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
                TL_SetProperty("Append", "Lauren recoils slightly, still uncertain. You smile broadly. This does little to reassure him." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You hand him a small syringe, containing the white liquid that made you perfect. He grimaces, staring at it. You pat him softly on the head." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "He sticks the syringe into the vein on his wrist, and nearly drops it from the pain. You hold his arm and the syringe, aiding him." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "He almost cries, but you whisper 'You're so strong, sister. I love you.' This causes him to catch himself." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF0", 0)
                TL_SetProperty("Append", "The changes begin. His skin goes numb and he almost loses his balance, but you hold him upright. You take the syringe out and inject him in the legs to hasten the process." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Will we be together, Mary?' he asks." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Forever, sister.' you answer. The changes continue." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF1", 0)
                TL_SetProperty("Append", "His knees change to ball joints and are unable to support his weight. You hold him and ease him to a kneel. He seems content to sit that way." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "His body is almost entirely plastic now. His hair has mostly fallen out. He is resigned. He chances one last look at you as the changes reach his head. He almost smiles as his eyes go white. Then, he is emptied." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Optional, skin color change.
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iLaurenIndex, 6, 3, ciCodeFriendly)
                if(sEndLetter ~= "C") then
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
                    TL_SetProperty("Append", "You hold your new sister's body as the changes complete. You clutch her in a hug. She is so lightweight, you barely feel her heft." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
                    TL_SetProperty("Append", "As you hold her close, the will of your creator flows through you into Lauren. Her plastic skin changes color." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "Now she is exactly as your creator willed. Smooth, perfect, clean, empty. You smile and kiss her softly on the cheek." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "Had you had any attachment to this human, you would have been saddened by this, somehow..." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                --Normal:
                else
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDollC", 0)
                    TL_SetProperty("Append", "You hold your new sister's body as the changes complete. She is now blank, smooth, and perfect. You smile and kiss her softly on the cheek." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                end
                
                TL_SetProperty("Append", "Some distant feeling pangs in your mind. You have done something wrong. You have done a taboo." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Then, a crash hits you. Your creator noticed the guilt you felt and removed it. You're now empty, perfect, and obedient as she had willed. You smile emptily." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You will now carry your new sister to your creator. She will be so proud of you!" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "[You are now carrying Lauren. Your creator wills you to deliver the new sister. You obey.]" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
        
            --Lauren is not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                
                --Normal case.
                if(gzTextVar.iGameStage < 6) then
                    TL_SetProperty("Append", "You smile and approach Lauren, but he hastily shoves you away. He cries and runs. He is just quick enough to stay ahead of you. You will need to tire him out first." .. gsDE)
                    
                --Phase 6, ambush your friends!
                else
                
                    --Flag.
                    gzTextVar.bIsBetrayal = true
        
                    --Change Jessie's state to blank doll. She now follows the player.
                    gzTextVar.zEntities[i].sState = "Blank Doll"
                    gzTextVar.bIsJessieFollowing = true
                    gzTextVar.zEntities[i].bIsCrippled = false
                    
                    --Also change Lauren's state.
                    gzTextVar.zEntities[p].sState = "Blank Doll"
                    gzTextVar.bIsLaurenFollowing = true
                    gzTextVar.zEntities[p].bIsCrippled = false
                    
                    TL_SetProperty("Append", "With no warning whatsoever, you suddenly inject a needle of fluid into Lauren!" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "Caught off guard, the needle discharges fully into Lauren before he can pull it out." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "You give Jessie a similar treatment. She fights back a bit harder, but the betrayal was so unexpected that she had no chance." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "'Mary! How could you?'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Unregister Image")
                    TL_SetProperty("Append", "You grin at her. 'I love being plastic. You will, too.'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "'But... but...'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Unregister Image")
                    TL_SetProperty("Append", "'I'll just be a bit more aware, that's all. We're going to be sisters, Jessie. Aren't you excited?'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF1", ciImageLayerDefault)
                    TL_SetProperty("Append", "Jessie falls over as her legs dollify, unable to support her. She gives you a pained look." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF1", ciImageLayerDefault)
                    TL_SetProperty("Append", "Lauren is more accepting. He, too, cannot support himself. He falls to his knees. You pat him on the head for being such a good doll." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "The transformations are brief. You hope they enjoyed them as much as you enjoyed yours." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 6, 3, ciCodeFriendly)
                    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, p, 6, 3, ciCodeFriendly)
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDollUpperC", ciImageLayerDefault)
                    TL_SetProperty("Append", "Lauren has become entirely plastic. All thoughts within her have ceased. She is empty now." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                    --Optional, skin color change.
                    if(sEndLetter ~= "D") then
                        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDollUpper" .. sEndLetter, ciImageLayerDefault)
                        TL_SetProperty("Append", "The will of your creator flows through her, changing her skin. You smile. You will need to practice being empty and obedient." .. gsDE)
                        TL_SetProperty("Create Blocker")
                    
                    --Normal:
                    else
                        TL_SetProperty("Append", "You smile. You will need to practice being empty and obedient. In that way, Lauren has an edge over you." .. gsDE)
                        TL_SetProperty("Create Blocker")
                    end
                    
                    TL_SetProperty("Unregister Image")
                    TL_SetProperty("Append", "You heft the motionless bodies of Jessie and Lauren. You will still need to deliver them to your creator, exactly as she had willed." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "It's odd. You should feel like you betrayed them, but they will be happier this way. You all will be. That keeps you smiling." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                end
                
            --Crippled.
            else
        
                --Change Lauren's state to blank doll. She now follows the player.
                gzTextVar.zEntities[i].sState = "Blank Doll"
                gzTextVar.bIsLaurenFollowing = true
                gzTextVar.zEntities[i].bIsCrippled = false
                
                --Transformation sequence.
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
                TL_SetProperty("Append", "Unable to resist, Lauren is barely holding himself upright. He recoils as you approach." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "He squirms as you inject your needle into his neck. You inject him a few more times at different points, to further speed the changes." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "He withdraws from each injection, but does not fight. The changes begin in earnest." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "He itches himself. You embrace him, holding him close and patting his head. This seems to calm him." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The changes spread over him. His injuries vanish, replaced by cold, hardened plastic." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF1", ciImageLayerDefault)
                TL_SetProperty("Append", "His knees change to ball joints and are unable to support his weight. You hold him and ease him to a kneel. He seems content to sit that way." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "His body is almost entirely plastic now. His hair has mostly fallen out. He is resigned. He chances one last look at you as the changes reach his head. His eyes go white." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Optional, skin color change.
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iLaurenIndex, 6, 3, ciCodeFriendly)
                if(sEndLetter ~= "C") then
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
                    TL_SetProperty("Append", "You hold your new sister's body as the changes complete. You clutch her in a hug. She is so lightweight, you barely feel her heft." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
                    TL_SetProperty("Append", "As you hold her close, the will of your creator flows through you into Lauren. Her plastic skin changes color." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "Now she is exactly as your creator willed. Smooth, perfect, clean, empty. You smile and kiss her softly on the cheek." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "Had you had any attachment to this human, you would have been saddened by this, somehow..." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                --Normal:
                else
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
                    TL_SetProperty("Append", "You hold your new sister's body as the changes complete. She is now blank, smooth, and perfect. Had you any attachment to this human, you would have been saddened at this." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                end
                
                TL_SetProperty("Append", "Some distant pain pangs in your mind. You have done something wrong. You have done a taboo." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Then, a crash hits you. Your creator noticed the guilt you felt and removed it. You're now empty, perfect, and obedient, as she had willed. You smile emptily." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You will now carry your new sister to your creator. She will be so proud of you!" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "[You are now carrying Lauren. Your creator wills you to deliver the new sister. You obey.]" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            end
        
        --Lauren is a blank doll.
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
            gzTextVar.zEntities[i].bIsCrippled = false
            TL_SetProperty("Append", "Your sister is already a blank canvas, waiting for something new to be created atop it." .. gsDE)
            TL_SetProperty("Unregister Image")
        end

    end
end