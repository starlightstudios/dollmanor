--[ =========================================== Jessie ========================================== ]
--Entity command handler. This entity will react when a command is issued towards them.

--Argument Listing:
-- 0: sInstruction - What exactly we want out of the room.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInstruction = LM_GetScriptArgument(0)

--Other.
local i = gzTextVar.iJessieIndex
local p = gzTextVar.iLaurenIndex
    
--Fast-access strings.
local iMaryCnt = 1
local sMaryImg = gzTextVar.gzPlayer.sQuerySprite
local saMaryList = {sMaryImg}

local iLaurCnt = 1
local sLaurImg = gzTextVar.zEntities[p].sQueryPicture
local saLaurList = {sLaurImg}

local iJessCnt = 1
local sJessImg = gzTextVar.zEntities[i].sQueryPicture
local saJessList = {sJessImg}
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityName = TL_GetProperty("Command String")
    
    --Common.
    TL_SetProperty("Register Popup Command", "look", "look " .. sEntityName)
    
    --Player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
        if(gzTextVar.bGoatsicle == true and gzTextVar.bToldJessie == false) then
            TL_SetProperty("Register Popup Command", "goatsicle", "talk "   .. sEntityName .. " goatsicle")
        end
    
    --Player is a statue:
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
        if(gzTextVar.zEntities[i].sState ~= "Statue") then
            TL_SetProperty("Register Popup Command", "attack", "attack " .. sEntityName)
        end
    
    --Player is a claygirl:
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Popup Command", "talk", "talk "   .. sEntityName)
        
        --Attack option for humans.
        if(gzTextVar.zEntities[i].sState == "Human") then
            TL_SetProperty("Register Popup Command", "attack", "attack " .. sEntityName)
        end
            
        --If Lauren is downed:
        if(gzTextVar.zEntities[i].sState == "Human" and gzTextVar.zEntities[i].bIsCrippled == true) then
            TL_SetProperty("Register Popup Command", "transform", "transform " .. sEntityName)
        end
    
    --Player is a doll:
    else
    
        --Jessie is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            TL_SetProperty("Register Popup Command", "play", "play " .. sEntityName)
            TL_SetProperty("Register Popup Command", "talk", "talk " .. sEntityName)
            if(gzTextVar.bGoatsicle == true) then
                TL_SetProperty("Register Popup Command", "goatsicle", "talk "   .. sEntityName .. " goatsicle")
            end
            
            --If Jessie is downed:
            if(gzTextVar.zEntities[i].sState == "Human" and gzTextVar.zEntities[i].bIsCrippled == true) then
                TL_SetProperty("Register Popup Command", "transform", "transform " .. sEntityName)
            end
    
        --Jessie is a blank doll:
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
        
        
        end
    end
    
    --Pulse ends here.
    return
end

--[ ========================================== Dialogue ========================================= ]
--Looking at.
if(sInstruction == "look jessie") then
    
    gbHandledInput = true
    
    --Tutorial:
    if(gzTextVar.sManorType == "Tutorial") then
            TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
            TL_SetProperty("Append", "Your best friend, Jessie is a raven-haired girl you met in grade school. She's athletic and very popular with the boys, likely due to her keen fashion sense and endless supply of energy." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
    --Human version.
    elseif(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.iGameStage ~= 5) then
            TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
            TL_SetProperty("Append", "Your best friend, Jessie is a raven-haired girl you met in grade school. She's athletic and very popular with the boys, likely due to her keen fashion sense and endless supply of energy." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Dumb-dumb.
        else
            TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
            TL_SetProperty("Append", "A very nice girl. Her name is Jessie. You're not sure if you've met her before, but she seems very nice! You like hanging out with her!" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
        
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
        
        --Jessie is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                TL_SetProperty("Append", "The intruder. You must SUBDUE HER. You must BRING HER TO THE LIGHT. YOU ARE A STATUE. YOU WILL OBEY." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Append", "You must subdue Jessie. You must bring Jessie to the light. You are a statue. You will obey." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Secret!
            else
                TL_SetProperty("Append", "The intruder. You will BRING HER TO THE LIGHT. YOU ARE A STATUE. YOU WILL OBEY." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Append", "Jessie does not know you are a statue. You must bring Jessie to the light. You are a statue. You will obey." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
        
        --Statue.
        else
            TL_SetProperty("Append", "An unremarkable statue, standing in the gallery amongst dozens of others. She is as obedient as you." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
        
        --Normal:
        if(gzTextVar.bIsClaySequence == false) then
        
            --Jessie is a human:
            if(gzTextVar.zEntities[i].sState == "Human") then
                TL_SetProperty("Append", "A familiar human girl. Is she your - friend? You seem to remember she is your friend..." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Append", "Of course! She will be your best friend once you cover her in clay and make her like you." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Claygirl.
            else
                TL_SetProperty("Append", "A girl made of clay. Her body flows over itself and is constantly reforming." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
        
        --Clay sequence.
        else
            TL_SetProperty("Append", "Jessie, your best friend and new clay sister. Her clay has become a lovely red hue." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "This is Mary, an identical rubber clone of you. Or were you the clone? It's hard to say. Her grin is painted on and her body shines even in darkness." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
        TL_SetProperty("Append", "Jessie, a sculpture of ice. If your heart were not frozen solid, you would say she is magnificently crafted. Beautiful even." .. gsDE)
        TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
    
        --Jessie is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
            TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
            TL_SetProperty("Append", "One of the intruders at the manor. She bears an uncanny resemblance to one of the girls in the chapel windows." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        else
            TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
            TL_SetProperty("Append", "A glass sculpture in a shape of Jessie, the girl in the chapel windows. Her form is magnificent to behold." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        end
    
    --Doll version.
    else
        --Jessie is a human:
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            --String trap:
            if(gzTextVar.iMaryStringTrapState == 3) then
                TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
                TL_SetProperty("Append", "Jessie, your best friend from when you were human. You can't wait to make her immortal plastic so you can play with her forever." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
            --Normal cases:
            elseif(gzTextVar.iGameStage ~= 6) then
                TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
                TL_SetProperty("Append", "A human you have no attachment to. You should [play] with her, and then make her into a new sister." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            --Phase 6.
            else
                TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
                TL_SetProperty("Append", "Your very best friend forever. After all you've been through, that bond will never fray." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            end
    
        --Jessie is a blank doll:
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
            TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
            TL_SetProperty("Append", "A puppet containing the soul of Jessie. Your creator wills you to bring her to the main hall. She will be completed there." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --Special bridal doll:
        elseif(gzTextVar.zEntities[i].sState == "Bridal Doll Special") then
            TL_SetProperty("Register Image", "Jessie", gzTextVar.zEntities[gzTextVar.iJessieIndex].sQueryPicture, ciImageLayerDefault)
            TL_SetProperty("Append", "Your former best friend transformed into a ball-jointed bridal doll. Her glassy stare suggests she resisted more than you did, and lost a bigger part of her mind to guarantee obedience." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Her latent feelings for you have been brought to the fore, and she considers herself your bride. At least until your creator wills her to be something else - you can't wait to play with her!" .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        --All other cases.
        else
            local sUseSkin = "Root/Images/DollManor/Transformation/GothTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
            
            --Resolve the base.
            if(string.sub(gzTextVar.zEntities[i].sState, 1, 5) == "Bride") then
                sUseSkin = "Root/Images/DollManor/Transformation/BrideTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 6) == "Dancer") then
                sUseSkin = "Root/Images/DollManor/Transformation/DancerTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 6) == "Geisha") then
                sUseSkin = "Root/Images/DollManor/Transformation/GeishaTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 4) == "Goth") then
                sUseSkin = "Root/Images/DollManor/Transformation/GothTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 7) == "Princess") then
                sUseSkin = "Root/Images/DollManor/Transformation/PrincessTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
                
            elseif(string.sub(gzTextVar.zEntities[i].sState, 1, 4) == "Punk") then
                sUseSkin = "Root/Images/DollManor/Transformation/PunkTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65)
            end
            
            --Display.
            TL_SetProperty("Register Image", "Jessie", sUseSkin, ciImageLayerSkin)
            TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerOverlay)
            TL_SetProperty("Append", "A puppet containing the soul of Jessie. You are patiently waiting for your creator to finish her so you can play with her." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Unregister Image")
        
        end
    end
    
--Attacking.
elseif(sInstruction == "attack jessie" or sInstruction == "play jessie") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Normal:
        if(gzTextVar.iGameStage ~= 5) then
            
            --Attack version:
            if(sInstruction == "attack jessie") then
                TL_SetProperty("Append", "She probably wouldn't appreciate that." .. gsDE)
            --Play version:
            else
                TL_SetProperty("Append", "Maybe you shouldn't play a game with Jessie until *after* you have escaped the scary mansion." .. gsDE)
            end
    
        --Dumb-dumb.
        else
            TL_SetProperty("Append", "Attack? What's that? Seems like one of those words you'd have to look up first. At. Tack. Hee hee!" .. gsDE)
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
        --Jessie is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
    
                --Not crippled.
                if(gzTextVar.zEntities[i].bIsCrippled == false) then
                    local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                    local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                    LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
                
                --Crippled, can't attack.
                else
                    TL_SetProperty("Append", "INTRUDER HAS BEEN SUBDUED. BRING HER TO THE LIGHT. OBEY." .. gsDE)
                end
            
            --Secret.
            else
                TL_SetProperty("Append", "INTRUDER IS NOT AWARE. DO NOT ATTACK. BRING HER TO THE LIGHT. OBEY." .. gsDE)
            end
        
        --Jessie is a statue.
        else
            TL_SetProperty("Append", "LOCATE INTRUDER. SUBDUE INTRUDER. You have not been ordered to attack the statue. You will locate the intruder. You will subdue the intruder." .. gsDE)
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
        --Jessie is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
            
            --Crippled, can't attack.
            else
                TL_SetProperty("Append", "You should [transform] the injured girl. She will feel better if you cover her in clay." .. gsDE)
            end
        
        --Jessie is a Claygirl.
        else
            TL_SetProperty("Append", "There is no point in attacking a fellow claygirl." .. gsDE)
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "You can't attack a clone of yourself! You can only smear rubber on others, and this clone is already covered!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You can't work up the motivation to attack her." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            --TF.
            if(gzTextVar.bJessieWillTF == true) then
                TL_SetProperty("Append", "Jessie is surrendering. You should [transform] her now." .. gsDE)
            
            --Not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
            
            --Crippled, can't attack.
            else
                TL_SetProperty("Append", "The human has been subdued. You should [transform] her now." .. gsDE)
            end
        else
            TL_SetProperty("Append", "You would never strike such a marvelous work of art." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
        
    --Doll version.
    else
    
        --Jessie is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --String trap:
            if(gzTextVar.iMaryStringTrapState == 3) then
                TL_SetProperty("Append", "Now now, you can't have all the fun yourself. Lead her to the string trap." .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
                
            --Normal cases:
            elseif(gzTextVar.iGameStage ~= 6) then
            
                --TF.
                if(gzTextVar.bJessieWillTF == true) then
                    TL_SetProperty("Append", "Jessie is surrendering. You can [transform] her now." .. gsDE)
            
                --Not crippled.
                elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                    local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                    local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                    LM_ExecuteScript(gzTextVar.sCombatHandler, i, sVictory, sDefeat)
                
                --Crippled, can't attack.
                else
                    TL_SetProperty("Append", "Jessie is already tired after so much fun. You should [transform] her instead. She'd like that." .. gsDE)
                end
    
            --Phase 6:
            else
                TL_SetProperty("Append", "After all that has happened, you could never bring yourself to attack Jessie. You're not even sure you'd win, she looks much fiercer than she ever did." .. gsDE)
            end
    
        --Jessie is a blank doll.
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
            TL_SetProperty("Append", "The dolly is incomplete. Your creator wants to complete her. Take her to your creator." .. gsDE)
        
        --Anything else.
        else
            TL_SetProperty("Append", "You could never bring yourself to attack one of your sisters unless you creator directly ordered it." .. gsDE)
        end
    
    end

--Talking to.
elseif(sInstruction == "talk jessie") then
    gbHandledInput = true
    
    --Tutorial:
    if(gzTextVar.sManorType == "Tutorial") then
        TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
        TL_SetProperty("Append", "'Hey Mary! Seems you're getting the tutorial pretty well. Good job!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Register Image", "Mary", sMaryImg, ciImageLayerDefault)
        TL_SetProperty("Append", "'Thanks, Jessie.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
    
    --If Jessie is a blank doll, nothing happens.
    elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
        TL_SetProperty("Append", "Jessie is surprisingly untalktative. Your creator will be able to help her!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Human version.
    elseif(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Stage 1: Player is leading Jessie and Lauren to the Main Hall:
        if(gzTextVar.iGameStage == 1) then
        
            --Hasn't talked to Jessie yet.
            if(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieMainHalled == false) then
                
                --Flag.
                gzTextVar.bIsJessieFollowing = true
                
                --Lauren is not following and not in the main hall:
                if(gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == false) then
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary!' Jessie shouts, running up to you. 'Thank goodness! What happened? How did we get here? Where are we?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't know, don't know, and don't know, respectively', you reply with a shrug." .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I've been hiding from - the things. Have you seen them?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If you're talking about those animated puppets, then yes.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What are we going to do? What if those things get us?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I think I know a safe place. Follow me. We can talk there.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Okay...'" .. gsDE)
                    fnDialogueCutscene("Nobody",        0,   {"Null"},    "Jessie will now follow you. You should escort her back to the Main Hall." .. gsDE)
                    fnDialogueFinale()
            
                --Lauren is following and not in the main hall:
                elseif(gzTextVar.bIsLaurenFollowing == true and gzTextVar.bIsLaurenMainHalled == false) then
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary! Lauren!' Jessie shouts, running up to you. 'Thank goodness! What happened? How did we get here? Where are we?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't know, don't know, and don't know, respectively', you reply with a shrug." .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I've been hiding from - the things. Have you seen them?'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Those creatures look just like the dolls Mary has in her room.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I don't have dolls, Lauren! Don't make up stories!'" .. gsDE)
                    fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'You totally do...'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Now doesn't seem like the time to argue. What are we going to do?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I think I know a safe place. Follow me. We can talk more there.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Okay...'" .. gsDE)
                    fnDialogueCutscene("Nobody",        0,   {"Null"},    "Jessie will now follow you. You should escort her back to the Main Hall." .. gsDE)
                    fnDialogueFinale()
            
                --Lauren is in the main hall:
                elseif(gzTextVar.bIsLaurenFollowing == false and gzTextVar.bIsLaurenMainHalled == true) then
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary!' Jessie shouts, running up to you. 'Thank goodness! What happened? How did we get here? Where are we?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't know, don't know, and don't know, respectively.', you reply with a shrug." .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I've been hiding from - the things. Have you seen them?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If you're talking about those animated puppets, then yes.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What are we going to do? What if those things get us?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We can outrun them, I think. Don't lose your cool.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I think the main hall is a safe spot, that's where I left Lauren. Haven't heard any crying since then, so it's got to be a good spot. Follow me.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Okay...'" .. gsDE)
                    fnDialogueCutscene("Nobody",        0,   {"Null"},    "Jessie will now follow you. You should escort her back to the Main Hall." .. gsDE)
                    fnDialogueFinale()
            
                end
            
            --Has talked to Jessie, on the way to the Main Hall.
            elseif(gzTextVar.bIsJessieFollowing == true and gzTextVar.bIsJessieMainHalled == false) then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Where are we going, exactly?' Jessie asks." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The entranceway. There was a lady there who seemed to be fine. I think she lives here.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'A lady? I didn't see anyone earlier...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'If we're going to find out how we got here, she's probably our best chance. Follow me.'" .. gsDE)
                fnDialogueFinale()
            
            --Jessie is waiting in the main hall, but Lauren isn't there yet.
            elseif(gzTextVar.bIsJessieFollowing == true and gzTextVar.bIsJessieMainHalled == true and gzTextVar.bIsLaurenMainHalled == false) then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Have you had any luck finding Lauren?' Jessie asks." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Not yet. Has - has Pygmalie...'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I think she might be suffering from dementia. She keeps calling me a little one and I can't understand anything she says.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'She seems pretty harmless, though.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I better find Lauren...'" .. gsDE)
                fnDialogueFinale()
            
            end
    
        --Phase 2: Find the crystal pieces.
        elseif(gzTextVar.iGameStage == 2) then
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Keep your eyes peeled, Mary. Those things are everywhere.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I can take them.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yeah? So can I.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Please don't fight...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I can't take twenty of them, and there seem to be a lot of them.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'The mighty Jessie, scared of some dolls?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Pfah. No jokes. Don't get overconfident.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'*I'm trying to put on a strong face for Lauren. You should too.*'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh. Yeah, bring em on! These weirdos are nothing!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I want to go home...'" .. gsDE)
            fnDialogueFinale()
        
        --Phase 3: Find the journal.
        elseif(gzTextVar.iGameStage == 3) then
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Sheesh, can you believe all of this? I keep hoping I'll wake up any second.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's real.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I wish it wasn't.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I'm tired... and I'm cold...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What, tired already little guy? You're way tougher than that!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Right? Maybe we should save some of these baddies for Lauren?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'And let this tough guy show us up? No way.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*I tried...*'" .. gsDE)
            fnDialogueFinale()
        
        --Phase 4: Find the storybook.
        elseif(gzTextVar.iGameStage == 4) then
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Whatever you're going to do, be careful. Don't make me come after you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Just make sure Lauren and Sarah are safe.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'He seems to be getting along with her.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'She's really nice and smart. I'm learning a lot.'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'But what's a palooka?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Sounds like Yankee slang.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'We should try to find a cowboy hat to put on the book! Ha ha!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*I'll keep his spirits up. Stay strong, Mary.*'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'*I'll be back soon.*'" .. gsDE)
            fnDialogueFinale()
        
        --Phase 5: Dumb dumb.
        elseif(gzTextVar.iGameStage == 5) then
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Excuse me, nice lady, but what was I supposed to do again?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Did you forget already?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yep!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Go find a doll girl, or... well, anything, but a doll girl is best.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Tell her you're volunteering and want to be plastic. Okay?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Why?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Actually, it was your idea.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Oh yeah! Of course it was! Tee hee!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You're not sure if it was your idea, but you tend to forget things a lot. You had better find a doll girl - before you forget that too!" .. gsDE)
            fnDialogueFinale()
    
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
        --Jessie is not a statue.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                
                --Jessie is not crippled.
                if(gzTextVar.zEntities[i].bIsCrippled == false) then
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary... What have they done to you? Speak to me!'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The intruder is talking to you. You are not sure what to do." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "SUBDUE THE INTRUDER. BRING HER TO THE LIGHT. OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You know what to do. You must subdue the intruder. You must bring her to the light. You will obey." .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I'm sorry, Mary. I'll try to help you, forgive me if I hurt you!'" .. gsDE)
                    fnDialogueFinale()
                    local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                    local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                    TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
                
                --Crippled.
                else
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary... Please...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "THERE IS NO MARY. THERE IS ONLY THE LIGHT. YOU ARE A STATUE. YOU OBEY." .. gsDE)
                    fnDialogueFinale()
                end
        
            --Secret statue.
            else
        
                --In the Strange Antechamber:
                if(gzTextVar.gzPlayer.sLocation == "Strange Antechamber") then
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Well, lead the way, Mary.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "LEAD THE INTRUDERS TO THE LIGHT, OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Follow me. I obey.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Huh?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You will like it. It will make you happy.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Okay...'" .. gsDE)
                    fnDialogueFinale()
        
                --In the hypnotic hallway:
                else
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary... this light is making me nauseous...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "LEAD THE INTRUDERS TO THE LIGHT, OBEY." .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I obey.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Huh? I can't... ugh...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Follow me. Obey.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Ugh... obey? What... we should get out of here...'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Follow me. You will like it.'" .. gsDE)
                    fnDialogueFinale()
        
                end
            end
        
        --Jessie is a statue.
        else
            TL_SetProperty("Append", "YOU ARE A STATUE. YOU HAVE NO THOUGHTS. YOU DO NOT SPEAK." .. gsDE)
            TL_SetProperty("Create Blocker")
            TL_SetProperty("Append", "You are a statue. You have no thoughts. You do not speak. You will FIND THE INTRUDERS. You will SUBDUE THE INTRUDERS. You will BRING THE INTRUDERS HERE." .. gsDE)
            TL_SetProperty("Create Blocker")
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
            
        --Jessie is not a Claygirl.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Jessie is not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Get back! I know karate!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You would scoff at the idea if you had a mouth, or real emotions. You are clay, you cannot be harmed." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You grasp towards the girl." .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'S-stop! Stop!'" .. gsDE)
                fnDialogueFinale()
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
            
            --Crippled.
            else
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Nggh... Sorry Mary... I let you down.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Strange, the girl seems to think she disappointed you. She will love it when you [transform] her, best to do that now." .. gsDE)
                fnDialogueFinale()
            
            end
        
        --Jessie is a Claygirl.
        else
            --Normal:
            if(gzTextVar.bIsClaySequence == false) then
                TL_SetProperty("Append", "You cannot speak with Jessie in the conventional manner. Instead, you reach your arm into her head, through where her mouth would be." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Thoughts exchange between you using the clay on your arm as a conduit. Jessie is very pleased with her malleable new body, and thanks you for changing her." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "She is glad you could be reunited in clay. She wants to be together forever. You do, too." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You ask her if there was something you needed to do. Something about escape. She says that humans sometimes try to escape, so you should prevent that." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You withdraw your arm and Jessie reforms the hole in her head. You'll need to search for humans and cover them in clay. You need to make them like you." .. gsDE)
                TL_SetProperty("Create Blocker")
            
            --Special:
            else
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie touches a clay tendril to you, bridging your thoughts." .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary, why did you transform me first?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I was worried you'd run away and I wouldn't catch you. I wanted you to feel how I feel.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Heh, thanks for that. I probably would have run.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Flesh is so restrictive. Thank you for this.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're welcome. I hope the creator loves these bodies as much as we do.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Jessie disconnects her clay and leaves you to your thoughts." .. gsDE)
                fnDialogueFinale()
            end
        end
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "You approach your identical clone. Neither of you can speak, as your grins are merely permanently painted on. Still, you nuzzle your mouths close to one another." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You feel some faint romantic attachment to your identical clone. It will be fun to play with her later!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Jessie.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Did you want to talk?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I want nothing.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Then why did you approach me?'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'When I look at you, I feel warmth inside. It's wrong. I don't like it.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh. That warmth is attraction. I had a crush on you when I was a human.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I see.'" .. gsDE)
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I hope it goes away in time.'" .. gsDE)
        fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'It will.'" .. gsDE)
        fnDialogueFinale()
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.zEntities[i].sState == "Human") then
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh my goodness... Mary...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You've got everyone up in arms. Please don't put up a fight.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'There must be a way to save you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You know, you look a lot like a character I saw once. Come, I'll show you.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "The girl is making trouble, you'll need to [attack] her before she'll come quietly." .. gsDE)
            else
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary... Please...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's your fault for causing such a commotion. I have something I want to show you, come on.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You can [transform] Jessie now." .. gsDE)
            end
        else
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary, were we human once?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hmm? No, I don't think so. I had the same dream, though.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'That's what I thought. Wouldn't it be funny to be someone you saw in a portrait, or read about in a story book?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Imagine the adventures we could have. Going to school, getting jobs, getting married.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'We should ask the creator if she will let us write about our dreams.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Great idea! Remind me to ask her later!'" .. gsDE)
            fnDialogueFinale()
        end
        
    --Doll version.
    else
    
        --String trap:
        if(gzTextVar.iMaryStringTrapState == 3) then
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'So how'd you figure it out, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Keep your eyes on the floor, there are little grooves. Just step where I step.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh, I get it. You step on the wrong board, and something happens?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yes. Exactly that.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Pretty clever. But I thought it'd be a lot worse. Something magic, like the cards you have.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You'll see the magic soon, just keep your eyes on the floor.'" .. gsDE)
            
        --Normal cases:
        elseif(gzTextVar.iGameStage ~= 6) then
            
            --Special bridal doll:
            if(gzTextVar.zEntities[i].sState == "Bridal Doll Special") then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yes, my beloved?'" .. gsDE)
                fnDialogueCutscene("You",   iMaryCnt, saMaryList, "'How are you feeling?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Obedient, calm, happy.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I am swimming in a sea of bliss. I love you with all my heart, Mary.'" .. gsDE)
                fnDialogueCutscene("You",   iMaryCnt, saMaryList, "'Did you love me when we were humans?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'A crush, nothing more. Maybe it would have been love. I am glad I am a doll now, so I know it is love for certain.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary, could you ask our creator to let us stay lovers forever?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Now that I have you, I cannot fathom letting you go.'" .. gsDE)
                fnDialogueCutscene("You",   iMaryCnt, saMaryList, "You giggle. 'I will ask, but if the creator does not want us to be together, we will obey.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Of course. She is wise and kind, she will do what is right.'" .. gsDE)
                fnDialogueCutscene("You",   iMaryCnt, saMaryList, "Maybe she will let you keep some of your intelligence as a reward for your good work! You hum happily to yourself at the thought." .. gsDE)
            
            --TF.
            elseif(gzTextVar.bJessieWillTF == true) then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Please make it quick, Mary.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It only takes a second.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You can [transform jessie] now." .. gsDE)
                fnDialogueFinale()
            
            --Jessie is not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Uhhh, hello?' Jessie asks you, uncertain." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You giggle. 'Hello, Jessie!' you say with a broad grin." .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'M-M-Mary? Is that you?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You already knew my name! We're like best friends already!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Did those things get you - oh no oh no...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't be silly! I'm a dolly! It's so fun!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Don't you want to be a dolly? We can play together! We'll be best friends and sisters forever!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Don't worry, Mary, if I can save you then I will.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'If I can't - then, I'm still gonna try!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Oh you're so funny! I can't wait to show you to my creator!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You let out a giggle. This girl will make a wonderful sister. You decide to play with her!" .. gsDE)
                fnDialogueFinale()
                local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Jessie Victory.lua"
                local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Jessie Defeat.lua"
                TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
        
            --Crippled.
            else
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'S-stay back..." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'What a silly thing to say. You're so funny!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary? Mary, is that you? Your voice...'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You already know my name! I already know yours, you're Jessie!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Did these things - did they get you? Snap out of it!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'You're such a joker! I hope the creator makes you something fun so we can play!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Does that mean I'm... going to become like you?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You let out a giggle. She already knows! She's very smart, your creator will be so happy when you bring her back." .. gsDE)
            end
            
        --Phase 6.
        else
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'What's up, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Just trying to get myself sorted. It's strange. I was in the book, talking to you, and also out here wandering around.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'But you'll be okay?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Might take a while. Let's keep focused on the task at hand.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'If you need a minute, I can keep watch for you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Thanks, Jessie.'" .. gsDE)
        
        end
    end



--Talking to.
elseif(sInstruction == "talk jessie goatsicle") then
    gbHandledInput = true
    
    --Tutorial:
    if(gzTextVar.sManorType == "Tutorial") then
        TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
        TL_SetProperty("Append", "'Well you've clearly played the main game already if you know that command.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Register Image", "Mary", sMaryImg, ciImageLayerDefault)
        TL_SetProperty("Append", "'Don't dodge the issue.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Register Image", "Jessie", sJessImg, ciImageLayerDefault)
        TL_SetProperty("Append", "'Say it to me in the main game, Mary!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Unregister Image")
        
    --Human version.
    elseif(gzTextVar.gzPlayer.sFormState == "Human") then
        
        if(gzTextVar.bToldJessie == true) then
            TL_SetProperty("Append", "You have already confessed your feelings to Jessie." .. gsDE)
            return
        end
        
        --Variables
        local bPlayStandardScene = false
        local bPlayLaurenScene = false
        local bPlayToughScene = false
        
        --Stage 1: Player is leading Jessie and Lauren to the Main Hall:
        if(gzTextVar.iGameStage == 1) then
        
            --Hasn't talked to Jessie yet.
            if(gzTextVar.bIsJessieFollowing == false and gzTextVar.bIsJessieMainHalled == false) then
                TL_SetProperty("Append", "You should get [talk] to Jessie normally before discussing romance!" .. gsDE)
            
            --Has talked to Jessie, on the way to the Main Hall.
            elseif(gzTextVar.bIsJessieFollowing == true and gzTextVar.bIsJessieMainHalled == false) then
                bPlayStandardScene = true
            
            --Jessie is waiting in the main hall, but Lauren isn't there yet.
            elseif(gzTextVar.bIsJessieFollowing == true and gzTextVar.bIsJessieMainHalled == true and gzTextVar.bIsLaurenMainHalled == false) then
                bPlayStandardScene = true
            
            end
    
        --Phase 2: Find the crystal pieces.
        elseif(gzTextVar.iGameStage == 2) then
            bPlayLaurenScene = true
        
        --Phase 3: Find the journal.
        elseif(gzTextVar.iGameStage == 3) then
            bPlayLaurenScene = true
        
        --Phase 4: Find the storybook.
        elseif(gzTextVar.iGameStage == 4) then
            bPlayToughScene = true
        
        --Phase 5: Dumb dumb.
        elseif(gzTextVar.iGameStage == 5) then
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Uhhh, goats? Popsicles?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh dear, did you forget what you were going to say?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yep!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*sigh* Then don't say silly things!'" .. gsDE)
            fnDialogueFinale()
        end
    
        --Standard scene:
        if(bPlayStandardScene == true) then
            gzTextVar.bToldJessie = true
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hey Jessie. What if I said a silly word to you?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Huh?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I just made it up now. Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you serious, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I found your note, you must have dropped it.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I like you too, Jessie. I just thought I was the only one, so I kept it to myself.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's a bad time, right? Yeah, I can drop it.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie abruptly leans in and kisses you on the lips!" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*mwah*! Oh, oh! I'm sorry, was that too fast?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No, no, it's totally okay. I just didn't think my first kiss was going to happen in a creepy mansion in the middle of the forest.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Y-yeah, I guess this is a bad time to make out.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'But hold my hand until we get out of here?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Deal!'" .. gsDE)
            fnDialogueFinale()
            
        --Lauren is here:
        elseif(bPlayLaurenScene == true) then
            gzTextVar.bToldJessie = true
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hey Jessie. What if I said a silly word to you?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Huh?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I just made it up now. Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you serious, Mary?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I found your note, you must have dropped it.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I like you too, Jessie. I just thought I was the only one, so I kept it to myself.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Hey, what are you two talking about?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'H-hey, Lauren! Uh, can you look away for a second?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Huh? Okay...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie abruptly leans in and kisses you on the lips!" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*mwah*! Oh, oh! I'm sorry, was that too fast?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No, no, it's totally okay. I just didn't think my first kiss was going to happen in a creepy mansion in the middle of the forest.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Y-yeah, I guess this is a bad time to make out.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'But hold my hand until we get out of here?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Deal!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'... I saw that.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Oh, well, uh, Lauren... You see...'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'I know what kissing is, Mary. I'm not a baby.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Of course you aren't, so you know this is a good thing, right?'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Girls are so gross and weird...'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hah! Nerd! When you grow up you'll want to kiss girls, too!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Not gross and weird ones like Jessie!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oy! Oy! You what?!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'He's just teasing you, Jessie!'" .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "Lauren snickers. For a moment, he has forgotten where he is, the danger he is in. And then, he remembers." .. gsDE)
            fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'... I want to go home.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Working on it, kiddo. Let's go.'" .. gsDE)
            fnDialogueFinale()
            
        --Bad time!
        elseif(bPlayToughScene == true) then
            gzTextVar.bToldJessie = true
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hey Jessie. What if I said a silly word to you?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh my gosh, not now. Not now, Mary.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I just made it up now. Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'No no no no no...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Why now? Why do you have to tell me you like me now?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Because I may not get another chance. I like you and I have for a while.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I just didn't know if you liked me back, so I kept my mouth shut.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'And you're going to go off and get yourself into lord knows what kind of trouble?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I'm sorry, Jessie. But I needed to tell you.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'It's okay... it's... okay...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'No time like the present.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie abruptly leans in and kisses you on the lips!" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'*mwah*! T-there! My first kiss! This had better not be the last, Mary!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'...'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Mary, I meant that!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It won't be. I will make it back, and we will escape. Everything will be fine.'" .. gsDE)
            fnDialogueFinale()
        end

    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    
        --Jessie is not a statue.
        if(gzTextVar.zEntities[i].sState == "Human") then
    
            --Normal:
            if(gzTextVar.bSecretStatue == false) then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "STATUES DO NOT SPEAK. STATUES OBEY." .. gsDE)
                fnDialogueFinale()
        
            --Secret statue.
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU HAVE NO ATTACHMENT TO THAT WORD. FORGET THAT WORD. OBEY." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You decide to forget about the word, and obey the light." .. gsDE)
                fnDialogueFinale()
            end
            
        --Jessie is a statue.
        else
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU HAVE NO ATTACHMENT TO THAT WORD. FORGET THAT WORD. OBEY." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle'. The word comes from somewhere within you. The light swarms over you, penetrating you." .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Goatsicle?' the statue asks." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "FORGET THAT WORD. YOU ARE A STATUE. STATUES OBEY. YOU WILL OBEY. JESSIE IS A STATUE, STATUES OBEY THE LIGHT." .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "YOU DO NOT SPEAK. STATUES DO NOT SPEAK, THEY OBEY. OBEY. OBEY. OBEY." .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "The other statue goes silent, as do you. You both obey the light." .. gsDE)
            fnDialogueFinale()
        end
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Your attempts to say that word fail as you have no mouth, or lungs." .. gsDE)
        fnDialogueFinale()
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        fnDialogueCutscene("You",    iMaryCnt, saMaryList, "Your attempts to say that word fail as you have no mouth, it's merely painted on. How silly!" .. gsDE)
        fnDialogueFinale()
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        if(gzTextVar.bToldJessie == true) then
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Hello, Mary.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Do you still have feelings for me?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I have none for you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'As it should be.'" .. gsDE)
            fnDialogueFinale()
        else
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Hello, Mary.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'You found the note. Do you have feelings for me?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'No.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I have none for you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'As it should be.'" .. gsDE)
            fnDialogueFinale()
    
        end
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
    
        --Jessie is not a glass girl.
        if(gzTextVar.zEntities[i].sState == "Human") then
            if(gzTextVar.bToldJessie == true) then
                if(gzTextVar.bJessieWillTF == false) then
                    gzTextVar.bJessieWillTF = true
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle, human.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'...'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you really still in there, Mary?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yes. I want you to join me.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Really? Why did it have to end like this?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'This is not the end. We can be together. Please, don't fight, and you will enjoy it.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'... Okay. Okay. Just do it, Mary.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "It seems the human is surrendering. You should [transform] her now." .. gsDE)
                    fnDialogueFinale()
                else
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'... Okay. Okay. Just do it, Mary.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "It seems the human is surrendering. You should [transform] her now." .. gsDE)
                    fnDialogueFinale()
                end
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle, human.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I'm not falling for that! You're just saying that! You're not the real Mary!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "It was worth a try. You'll need to [attack] the human before she causes any more trouble." .. gsDE)
                fnDialogueFinale()
            end
        
        --Jessie glass:
        else
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Oh... Isn't that the word that Ma - Oh!'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Are you trying to tell me something?'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Ha ha! In that story, Mary and Jessie liked each other. Do you like me?'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Of course!'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Then let's be together, just like our favourite characters.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Y-yay! Thank you, Mary!'" .. gsDE)
            fnDialogueFinale()
    
        end
    
    --Doll version.
    else
    
        --String trap:
        if(gzTextVar.iMaryStringTrapState == 3) then
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
            fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Not right now, Mary. But, thank you.'" .. gsDE)
            fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Of course.'" .. gsDE)
            fnDialogueFinale()
            
        --Normal cases:
        elseif(gzTextVar.iGameStage ~= 6) then
            
            --Special bridal doll:
            if(gzTextVar.zEntities[i].sState == "Bridal Doll Special") then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie raises her veil and kisses you on the lips. 'I love you'." .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I love you too, Jessie.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'To think I wanted to run away and fight, when what I always wanted was right in front of me.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Do you remember much of what came before?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'No. My memories brought about rebellion, disobedience, distrust of the creator. It is better they are gone.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We will make new ones together.'" .. gsDE)
                fnDialogueFinale()
                    
            --Jessie is not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                if(gzTextVar.bJessieWillTF == true) then
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "It seems the human is surrendering. You can [transform] her now." .. gsDE)
                    fnDialogueFinale()
                elseif(gzTextVar.bToldJessie == true) then
                    gzTextVar.bJessieWillTF = true
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hello, friend! Goatsicle!'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie glares at you. Her eyes widen with realization. 'Oh. Oh my.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Want to be my friend, Jessie?'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Do you still - feel... that way? As a doll?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I do! I want to be your sister forever!'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'... Forever?'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'It's wonderful to be plastic. I feel stronger, healthier, more aware, happier. You'll love it.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'I was scared at first, but then the creator made me happy. Please, join me.'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'... Okay. Okay, but we have to be together, Mary.'" .. gsDE)
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yay! You'll be so cute!'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "It seems the human is surrendering. You can [transform] her now." .. gsDE)
                    fnDialogueFinale()
                else
                    fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Hello, friend! Goatsicle!'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Back off! I'm not falling for your trick!'" .. gsDE)
                    fnDialogueCutscene("Jessie", iJessCnt, saJessList, "It was worth a try, but Jessie didn't believe you. You'll need to [play] with her." .. gsDE)
                    fnDialogueFinale()
                end
        
            --Crippled.
            else
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "The wounded human just weeps." .. gsDE)
                fnDialogueFinale()
            end
        
        --Phase 6.
        else
            if(gzTextVar.bToldJessie == true) then
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Heh, thanks, Mary. We'll get through this.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Yeah. Hold my plastic hand?'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie smiles and grips your hand tight." .. gsDE)
                fnDialogueFinale()
            else
                gzTextVar.bToldJessie = true
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Goatsicle.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Really? Now, Mary?'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'We may not get another chance. I like you, Jessie, I have for a while. I was just worried you didn't like me that way back.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'So I kept it to myself, because I didn't want to ruin our friendship.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie smiles and grips your hand tight. 'Never.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "She leans in and kisses you on the lips. 'Mwah!'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Ewwww!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Lauren!'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Girls are gross!'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'I couldn't ask for a better first kiss.'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "'Me neither. Your lips are so soft.'" .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "'Yours are, too, despite being plastic.'" .. gsDE)
                fnDialogueCutscene("Lauren", iLaurCnt, saLaurList, "'Stop it, kissing is gross!'" .. gsDE)
                fnDialogueCutscene("You",    iMaryCnt, saMaryList, "You and Jessie share a chuckle. Lauren doesn't know it yet, but he'll want to kiss people someday, too." .. gsDE)
                fnDialogueCutscene("Jessie", iJessCnt, saJessList, "Jessie keeps holding your hand and smiling firmly. You have renewed confidence that everything will work out." .. gsDE)
                fnDialogueFinale()
            end
        end
    end

--Transforming to.
elseif(sInstruction == "transform jessie") then
    gbHandledInput = true
    
    --Human version.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        TL_SetProperty("Append", "Why would you want to do that?" .. gsDE)
    
    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    
        --Jessie is a human.
        if(gzTextVar.zEntities[i].sState == "Human") then
        
            --Jessie is not crippled.
            if(gzTextVar.zEntities[i].bIsCrippled == false) then
                TL_SetProperty("Append", "You grasp towards Jessie, but your slow clay legs are no match for her swift fleshy ones. You need to [attack] her first." .. gsDE)
                
            --Crippled.
            else
        
                --Determine clay color.
                local iIndicatorX = 6
                local sMyClayColor = string.sub(gzTextVar.gzPlayer.sQuerySprite, 42)
                local sJessieClayColor = "Blue"
                if(sMyClayColor == "Blue") then
                    sJessieClayColor = "Yellow"
                    iIndicatorX = 8
                elseif(sMyClayColor == "Yellow") then
                    sJessieClayColor = "Red"
                    iIndicatorX = 7
                else
                    sJessieClayColor = "Blue"
                    iIndicatorX = 6
                end
                io.write("Clay color is " .. sMyClayColor .. "\n")
                io.write("Jessie clay color is " .. sJessieClayColor .. "\n")
        
                --Change Lauren's state to claygirl.
                gzTextVar.zEntities[i].sState = "Claygirl"
                gzTextVar.bIsLaurenFollowing = false
                gzTextVar.zEntities[i].bIsCrippled = false
                gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/ClayGirl" .. sJessieClayColor
                
                --Transformation sequence.
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
                TL_SetProperty("Append", "Jessie tries to push away from you, but your clay comes off and sticks to her." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieClay" .. sJessieClayColor .. "TF0", ciImageLayerDefault)
                TL_SetProperty("Append", "She shrieks as the clay covers her arms. It begins to spread." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Your clay changes color as it snakes along her arms. It begins to consume and change her flesh, making more of the new clay." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Jessie tries desperately to get the clay off, but only spreads it further. She tries to scream, only for the clay to reach her mouth and cover it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Jessie tries to run, but stumbles over her own legs. Her body collapses into a pool of clay, subducting what flesh remained. All that is left is clay." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iJessieIndex, iIndicatorX, 4, ciCodeFriendly)
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/ClayGirl" .. sJessieClayColor, ciImageLayerDefault)
                TL_SetProperty("Append", "Now, Jessie rises from the pool. Her head, chest, arms, legs - all clay. Her face still persists beneath the sheen of new clay." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You reach towards her and smooth her face. You do not need one, she does not need one. She tries to push you away at first, but allows you to shape her." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Now knowing what she must do, Jessie begins to mold her body. She changes her hair, smooths her stomach, and increases her bust size. She is happy with her new self." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "There is a final task. You hover nearby, waiting, but Jessie is not doing it. She stares at you, expectantly. She motions for you to leave. You do not." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Jessie is not being a good clay girl. You reach into her head for her, helping her. She tries to stop you, but cannot. Her will has been sapped. You reach in." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You reshape the inside of her head. Some things are moved, some are squished. It takes a few minutes. She stands idly waiting for you to finish reshaping her." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Then, it is done. A feeling of accomplishment rushes through you. Jessie is now a clay girl, like you. Forever." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Unregister Image")
            
            end
        end

    --Statue.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
                
        TL_SetProperty("Append", "BRING THE INTRUDER TO THE LIGHT. SHE MUST SEE THE LIGHT. OBEY." .. gsDE)
        TL_SetProperty("Create Blocker")
                
        TL_SetProperty("Append", "You will transform the intruder by taking her to the light. You obey." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        TL_SetProperty("Append", "This clone is already transformed into a rubber copy of you!" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        TL_SetProperty("Append", "You've already transformed Jessie. She has no heat left to drain." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        if(gzTextVar.zEntities[i].sState == "Human") then
            
            if(gzTextVar.bJessieWillTF == true) then
                TL_SetProperty("Append", "You take Jessie's hand and lead her to the chapel. She does not struggle or try to run." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, "Chapel Choir Stand W")
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, i, "Chapel Choir Stand W")
                TL_SetProperty("Append", "You hold Jessie's hand as you bring her to the right spot. Her eyes light up in wonder when she sees it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "One of the stained-glass portraits along the wall glows with unearthly light. Jessie is enraptured." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'W-what? How am I in the glass?' she asks, confused." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The image of Jessie, an athletic girl from Sheffield, is in the glass pane. The human stares at it in awe." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'She always was. Just admire her story, her form. Appreciate her as I do.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "You hear a hissing sound of something being blown away, like sand in the wind. You look at the girl." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGlassTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "She is in great pain, imaginging some horrible fate. As you look, she notices your stare and winks." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Hah! Got you!' Jessie says. 'You really believed I was hurt, didn't you?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Were you daydreaming about something?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Yeah, I was just remembering about one of Jessie's adventures. She got trapped in a scary mansion with her friends.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'I always forget the ending, though.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Maybe we can ask the creator to make one for us. Then we can all pretend to be humans!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Great idea, Mary! Maybe we can get the doll girls to play along?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Heh, okay. See you later, Jessie.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Transform Jessie.
                gzTextVar.zEntities[i].bIsCrippled = false
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 6, 5, ciCodeFriendly)
                gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/JessieGlass"
                gzTextVar.zEntities[i].sState = "Glass"
            
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                TL_SetProperty("Append", "The human is athletic, strong, and quick. You'll need to [attack] her and tire her out first." .. gsDE)
                TL_SetProperty("Create Blocker")
            else
                TL_SetProperty("Append", "You heft the human in both arms. She's too hurt to fight back, but struggles in vain." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'Please Mary, please remember.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Hush, now. You've made a lot of commotion. You're lucky I found you first.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'Please...'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'I want to show you something, human. After that, I'll let the creator decide what to do with you.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, -1, "Chapel Choir Stand W")
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, i, "Chapel Choir Stand W")
                TL_SetProperty("Append", "You carry the hurt girl to the chapel. By the time you arrive, she has already recovered, and can stand on her own. You keep a hand on her shoulder just in case." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "One of the stained-glass portraits along the wall glows with unearthly light. You lead her towards it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "'W-what? How am I in the glass?' she asks, confused." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The image of Jessie, an athletic girl from Sheffield, is in the glass pane. The human stares at it in awe." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "You hear a hissing sound of something being blown away, like sand in the wind. You look at the girl." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGlassTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "She is in great pain, imaginging some horrible fate. As you look, she notices your stare and winks." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Hah! Got you!' Jessie says. 'You really believed I was hurt, didn't you?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Were you daydreaming about something?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Yeah, I was just remembering about one of Jessie's adventures. She got trapped in a scary mansion with her friends.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'I always forget the ending, though.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Maybe we can ask the creator to make one for us. Then we can all pretend to be humans!'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/JessieGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Great idea, Mary! Maybe we can get the doll girls to play along?'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/MaryGlass", ciImageLayerDefault)
                TL_SetProperty("Append", "'Heh, okay. See you later, Jessie.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Transform Jessie.
                gzTextVar.zEntities[i].bIsCrippled = false
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 6, 5, ciCodeFriendly)
                gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/JessieGlass"
                gzTextVar.zEntities[i].sState = "Glass"
                
            end
        else
            TL_SetProperty("Append", "Transform her into what, a human! Ha ha ha!" .. gsDE)
            TL_SetProperty("Create Blocker")
        end

    --You're a doll! Get her!
    else

        --String trap:
        if(gzTextVar.iMaryStringTrapState == 3) then
            TL_SetProperty("Append", "Lead Jessie into the string trap that transformed you. That will be so much quicker." .. gsDE)
            
        --Jessie is a human.
        elseif(gzTextVar.zEntities[i].sState == "Human") then
        
            --Voluntary.
            if(gzTextVar.bJessieWillTF == true) then
        
                --Change Jessie's state to blank doll. She now follows the player.
                gzTextVar.zEntities[i].sState = "Blank Doll"
                gzTextVar.bIsJessieFollowing = true
                gzTextVar.zEntities[i].bIsCrippled = false
                
                --Transformation sequence.
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieGeneralTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "You hold Jessie's hand. You produce a needle filled with white fluid. Jessie is frightened of it." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'It hurts for a second, then it goes numb. Are you ready?' You ask. She swallows and nods." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "You inject a needle into her arm, letting out a cooing noise to reassure her. There's no pain. She fights her panic. You pat her arm and hold her. 'You're doing great.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The changes begin as the fluid circulates through her. Her skin becomes plastic and numb, just like yours. She begins to accept the changes." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF1", ciImageLayerDefault)
                TL_SetProperty("Append", "You help her up to a more comfortable position. She cannot balance anymore, so you hold her. 'Thanks, Mary.'" .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Call me sister,' you reply." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'Sister... Okay.' she says." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "'The less you resist, the less the creator needs to remove. Embrace it,' You say." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Most of her body is plastic now, and her hair has mostly fallen out. She's almost ready. Her eyes grow distant, and empty." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDollD", ciImageLayerDefault)
                TL_SetProperty("Append", "She is now totally thoughtless and motionless. She does nothing as you pull her to a sitting position. You run your hands over her smooth plastic body." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Optional, skin color change.
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iJessieIndex, 6, 3, ciCodeFriendly)
                if(sEndLetter ~= "D") then
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
                    TL_SetProperty("Append", "As you do so, the will of your creator flows through you into Jessie. Her plastic skin changes color." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "Now she is exactly as your creator willed. Smooth, perfect, clean, empty. You will take her to your creator now, to be complete." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                --Normal:
                else
                    TL_SetProperty("Append", "It is perfect, smooth, and clean. She is perfect, just like you. You will take her to your creator now, to be complete." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                end
                
                TL_SetProperty("Append", "[You are now carrying Jessie. Your creator wills you to deliver the new sister. You obey.]" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
        
            --Jessie is not crippled.
            elseif(gzTextVar.zEntities[i].bIsCrippled == false) then
                
                --Normal case.
                if(gzTextVar.iGameStage < 6) then
                    TL_SetProperty("Append", "You smile and approach Jessie, but she nimbly dodges away from you. You'll have to [play] with her to tire her out first." .. gsDE)
                
                --In phase 6, you can ambush her!
                else
                
                    --Flag.
                    gzTextVar.bIsBetrayal = true
        
                    --Change Jessie's state to blank doll. She now follows the player.
                    gzTextVar.zEntities[i].sState = "Blank Doll"
                    gzTextVar.bIsJessieFollowing = true
                    gzTextVar.zEntities[i].bIsCrippled = false
                    
                    --Also change Lauren's state.
                    gzTextVar.zEntities[p].sState = "Blank Doll"
                    gzTextVar.bIsLaurenFollowing = true
                    gzTextVar.zEntities[p].bIsCrippled = false
                    
                    TL_SetProperty("Append", "With no warning whatsoever, you suddenly inject a needle of fluid into Jessie!" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "Jessie shouts and tries to pull the needle out. It is already far, far too late for that." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "Before he can react, you likewise stick the needle into Lauren's neck. He shudders and begins crying." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "'Mary! How could you?'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Unregister Image")
                    TL_SetProperty("Append", "You grin at her. 'I love being plastic. You will, too.'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                    TL_SetProperty("Append", "'But... but...'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Unregister Image")
                    TL_SetProperty("Append", "'I'll just be a bit more aware, that's all. We're going to be sisters, Jessie. Aren't you excited?'" .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF1", ciImageLayerDefault)
                    TL_SetProperty("Append", "Jessie falls over as her legs dollify, unable to support her. She gives you a pained look." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Register Image", "Lauren", "Root/Images/DollManor/Transformation/LaurenDollTF1", ciImageLayerDefault)
                    TL_SetProperty("Append", "Lauren is more accepting. He, too, cannot support himself. He falls to his knees. You pat him on the head for being such a good doll." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 6, 3, ciCodeFriendly)
                    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, p, 6, 3, ciCodeFriendly)
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDollUpperD", ciImageLayerDefault)
                    TL_SetProperty("Append", "Jessie has become entirely plastic. Her protests have ceased, as have all thoughts within her. She is empty now." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                    --Optional, skin color change.
                    if(sEndLetter ~= "D") then
                        TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDollUpper" .. sEndLetter, ciImageLayerDefault)
                        TL_SetProperty("Append", "The will of your creator flows through her, changing her skin. You smile. You will need to practice being empty and obedient." .. gsDE)
                        TL_SetProperty("Create Blocker")
                    
                    --Normal:
                    else
                        TL_SetProperty("Append", "You smile. You will need to practice being empty and obedient. In that way, Jessie has an edge over you." .. gsDE)
                        TL_SetProperty("Create Blocker")
                    end
                    
                    TL_SetProperty("Unregister Image")
                    TL_SetProperty("Append", "You heft the motionless bodies of Jessie and Lauren. You will still need to deliver them to your creator, exactly as she had willed." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "It's odd. You should feel like you betrayed them, but they will be happier this way. You all will be. That keeps you smiling." .. gsDE)
                    TL_SetProperty("Create Blocker")
                end
                
            --Crippled.
            else
        
                --Change Jessie's state to blank doll. She now follows the player.
                gzTextVar.zEntities[i].sState = "Blank Doll"
                gzTextVar.bIsJessieFollowing = true
                gzTextVar.zEntities[i].bIsCrippled = false
                
                --Transformation sequence.
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
                TL_SetProperty("Append", "You stand over Jessie's limp form. She seems upset about something. She's funny like that." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF0", ciImageLayerDefault)
                TL_SetProperty("Append", "You inject a needle into her neck, letting out a cooing noise to reassure her. There's no pain. She has other ideas, and panics. You giggle." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The white fluid from your needle drains into her. She grips at it, trying to stop you, but is too weak. You smile and pat her head." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "The changes begin as the fluid circulates through her. Her skin becomes plastic and numb, just like yours. She slowly begins to accept the changes." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Transformation/JessieDollTF1", ciImageLayerDefault)
                TL_SetProperty("Append", "You help her up to a more comfortable position. She cannot balance on her numbed arms and legs, and gives you a begrudged look. You love helping your sisters." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Append", "Most of her body is plastic now, and her hair has mostly fallen out. She's almost ready. Her eyes grow distant, and empty." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDollD", ciImageLayerDefault)
                TL_SetProperty("Append", "She is now totally thoughtless and motionless. She does nothing as you pull her to a sitting position. You run your hands over her smooth plastic body." .. gsDE)
                TL_SetProperty("Create Blocker")
                
                --Optional, skin color change.
                TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, gzTextVar.iJessieIndex, 6, 3, ciCodeFriendly)
                if(sEndLetter ~= "D") then
                    TL_SetProperty("Register Image", "Jessie", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
                    TL_SetProperty("Append", "As you do so, the will of your creator flows through you into Jessie. Her plastic skin changes color." .. gsDE)
                    TL_SetProperty("Create Blocker")
                    
                    TL_SetProperty("Append", "Now she is exactly as your creator willed. Smooth, perfect, clean, empty. You will take her to your creator now, to be complete." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                --Normal:
                else
                    TL_SetProperty("Append", "It is perfect, smooth, and clean. She is perfect, just like you. You will take her to your creator now, to be complete." .. gsDE)
                    TL_SetProperty("Create Blocker")
                
                end
                
                TL_SetProperty("Append", "[You are now carrying Jessie. Your creator wills you to deliver the new sister. You obey.]" .. gsDE)
                TL_SetProperty("Create Blocker")
                TL_SetProperty("Unregister Image")
            
            end
        
        --Jessie is a blank doll.
        elseif(gzTextVar.zEntities[i].sState == "Blank Doll") then
            gzTextVar.zEntities[i].bIsCrippled = false
            TL_SetProperty("Append", "Your sister is already a blank canvas, waiting for something new to be created atop it." .. gsDE)
            TL_SetProperty("Unregister Image")
        end

    end

end