--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true
    TL_SetProperty("Append", "A dusty old workshop. There is a grinding wheel here. You could [sharpen] a blunt object to make it usable again." .. gsDE)

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return
end