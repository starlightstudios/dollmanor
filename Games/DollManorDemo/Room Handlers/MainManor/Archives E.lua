--Look/Examine handling.
if(gzTextVar.bIsExaminationCheck) then
    
    --Handle the input.
    gbHandledInput = true

    --Description.
    TL_SetProperty("Append", "This archive has exactly one book out of place. It's sitting on the floor. Someone was drawing doodles on the pages. The words on the page are faded and blurred, but the doodles aren't." .. gsDE)

    --Standard.
    fnListEntities(gzTextVar.gzPlayer.sLocation, true)
    fnListObjects(gzTextVar.gzPlayer.sLocation, true)
    return
end