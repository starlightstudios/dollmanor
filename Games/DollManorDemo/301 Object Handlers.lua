--[ ======================================= Object Handlers ===================================== ]
--Handles doing things to various objects, like taking or dropping them.

--Argument Listing:
-- 0: sInputString - The input string in question.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sInputString = LM_GetScriptArgument(0)

--[ ========================================= Taking Items ====================================== ]
if(string.sub(sInputString, 1, 4) == "take") then
    
    --Get the item.
    local sItemName = string.sub(sInputString, 6)
    
    --Get the room in question.
    local iRoom = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
    if(iRoom == -1) then return end
    
    --Search the object list to see if it exists.
    for i = 1, gzTextVar.zRoomList[iRoom].iObjectsTotal, 1 do
        if(gzTextVar.zRoomList[iRoom].zObjects[i].sDisplayName == sItemName) then
            
            --If the item cannot be taken...
            if(gzTextVar.zRoomList[iRoom].zObjects[i].bCanBePickedUp == false) then
                TL_SetProperty("Append", "You cannot take that with you.\n")
            
            --Item can be taken.
            else
                
                --Text.
                TL_SetProperty("Append", "You pick up the [" .. gzTextVar.zRoomList[iRoom].zObjects[i].sDisplayName .. "].\n")
                
                --Increment
                gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
                local p = gzTextVar.gzPlayer.iItemsTotal
                
                --Create the item.
                gzTextVar.gzPlayer.zaItems[p] = {}
                gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoom].zObjects[i]
                
                --Remove the item from the original room listing.
                fnRemoveItemFromRoom(iRoom, i)
            
            end
        
            --Mark input as handled.
            gbHandledInput = true
            return
        end
    end
end

--[ ======================================= Inventory Checks ==================================== ]
--Run across the player's inventory and see if any of them handle the input.
for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
    
    
end
