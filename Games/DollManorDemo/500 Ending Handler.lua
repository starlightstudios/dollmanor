--[ ====================================== Ending Handler ======================================= ]
--Checks ending states and displays as needed. Can only happen once per game. The game ends
-- once the endings are finished.

--[Variable Collection]
--Setup
local bIsPlayerDoll = false
local bIsJessieDoll = false
local bIsLaurenDoll = false
local bIsPlayerStatue = false
local bIsJessieStatue = false
local bIsLaurenStatue = false
local bIsPlayerClay = false
local bIsJessieClay = false
local bIsLaurenClay = false
local bIsPlayerGlass = false
local bIsJessieGlass = false
local bIsLaurenGlass = false

--Determine doll status.
if(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll") then bIsPlayerDoll = true end
if(string.sub(gzTextVar.zEntities[gzTextVar.iJessieIndex].sState, 1, 4) == "Doll") then bIsJessieDoll = true end
if(string.sub(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState, 1, 4) == "Doll") then bIsLaurenDoll = true end

--[Ending 1]
--In Phase 1, the player becomes a doll, then transformed Jessie and Lauren.
if(bIsPlayerDoll and bIsJessieDoll and bIsLaurenDoll and gzTextVar.bStringTrapEnding == false) then
    
    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary the doll played with her sisters quietly and obeyed every whim of their creator. She was a good doll.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, a visitor would come by. They would entertain the visitor, as their creator bid. They would put on plays and dances, and provide food and water.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The visitors would always elect to stay, and join them. Mary made many new sisters, and her creator was delighted.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Slowly, their creator aged. It showed more and more in her mannerisms. She slowed in word and thought. Her loyal dollies did all her tasks for her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Their creator became sick, and the dollies knew that her time was soon. They prepared a funeral for her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "But their creator had one thing for her, her most loyal and obedient dolly. She called Mary to her side.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She told Mary of the things the manor held. The places she had forbade them from going. She told Mary what was in those places.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She told Mary that nobody was ever to go near those places. Nobody was ever to open the seals. Her creator was not playing a game, and Mary took her words seriously.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "She did not know what any of them meant. She was a silly doll, meant to play and be played with. But she was also a tool of her creator and would do as she asked.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Finally, her creator placed her hand upon Mary's head. She filled her mind once again. She gave Mary the knowledge she would need to create more sisters herself. Mary would become the new creator.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The exertion taking what little strength she had, her creator fell asleep. Mary put her to bed, knowing it was for the last time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And so, Mary the doll would stand watch with her sisters in the manor. She created many more sisters over the years, as that had been her creator's final wish. Mary would carry it out.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 1. Plaything)") ]])
    fnCutsceneBlocker()
    
--[Ending 2]
--If in Phase 2 and the player is in the Ritual Altar, then they got the bad end of trying to pursue Pygmalie. This is called from Pygmalie's AI script.
elseif(gzTextVar.bExternalEndingCall == true and gzTextVar.iGameStage == 2 and gzTextVar.gzPlayer.sLocation == "Ritual Altar" and gzTextVar.bStringTrapEnding == false) then
        
    --Flag.
    TL_SetProperty("Begin Ending")
    gzTextVar.bIsGameEnding = true
    
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Set Scene Image", "Root/Images/DollManor/Endings/Dolls") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Many years passed...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary the doll stood in the room with the ritual altar in it. There were hundreds of others just like her in the same room.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary was a very good dolly. Mary didn't move. Mary was empty and stared ahead of her. Mary was very pretty. Mary loved being with her sisters.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Sometimes, Mary's creator told her to move. Mary would move, and do things for her creator. She would hold people. She would take books and burn them. She would find their guests and play with them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And then, Mary would go back to the room with the altar, and stop moving.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "In the back of her silly, empty mind, Mary could remember something. Her creator had asked her if she had taken something once.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Mary wondered what that something was. She hadn't taken it. Should she find it for her creator? She wondered where it was.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "And then, Mary would be empty again, and Mary would not move.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "The end.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "(Ending 2. Face in a Crowd)") ]])
    fnCutsceneBlocker()

end