--[Deck Help]
--Fired when the player clicks on one of the deck editor's help buttons. The button index is passed in.
-- This file then populates the help information.

--Argument Listing:
-- 0: iIndex --Which button was pressed.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iIndex = tonumber(LM_GetScriptArgument(0))

--[Deck Size]
if(iIndex == gciDeckEd_Help_DeckSize) then
    TL_SetProperty("Set Help Header", "Deck Size")
    TL_SetProperty("Set Help Line", 0, "This is the size of your deck. You can increase the maximum size by")
    TL_SetProperty("Set Help Line", 1, "finding special items.")
    TL_SetProperty("Set Help Line", 3, "If you run out of cards, you'll need to shuffle your deck. This will leave")
    TL_SetProperty("Set Help Line", 4, "you vulnerable, but larger decks give you less direct control over which")
    TL_SetProperty("Set Help Line", 5, "cards are available.")

--[Water Cards]
elseif(iIndex == gciDeckEd_Help_Water) then
    TL_SetProperty("Set Help Header", "Water Elemental Cards")
    TL_SetProperty("Set Help Line", 0, "Water Element cards increase damage by 1 and defense by 0.5 when added")
    TL_SetProperty("Set Help Line", 1, "to an action.")
    TL_SetProperty("Set Help Line", 3, "If multiple Water cards are used in the same action, the effect increases")
    TL_SetProperty("Set Help Line", 4, "by +1/+.5 per additional card.")
    TL_SetProperty("Set Help Line", 6, "Water damage and defense is increased by your water elemental bonus.")
    TL_SetProperty("Set Help Line", 8, "Certain enemies will take extra damage from water cards.")
    
    if(gzTextVar.bIsCombatWaitMode == true) then
        TL_SetProperty("Set Help Line", 10, "In Turn-Combat, water cards slightly slow enemy actions when attacking.")
        TL_SetProperty("Set Help Line", 11, "This effect becomes stronger the more water cards in one attack.")
        TL_SetProperty("Set Help Line", 12, "Water cards have no special defense effect.")
    end

--[Fire Cards]
elseif(iIndex == gciDeckEd_Help_Fire) then
    TL_SetProperty("Set Help Header", "Fire Elemental Cards")
    TL_SetProperty("Set Help Line", 0, "Fire Element cards increase damage by 1 and defense by 0.5 when added")
    TL_SetProperty("Set Help Line", 1, "to an action.")
    TL_SetProperty("Set Help Line", 3, "If multiple Fire cards are used in the same action, the effect increases")
    TL_SetProperty("Set Help Line", 4, "by +1/+.5 per additional card.")
    TL_SetProperty("Set Help Line", 6, "Fire damage and defense is increased by your fire elemental bonus.")
    TL_SetProperty("Set Help Line", 8, "Certain enemies will take extra damage from fire cards.")
    
    if(gzTextVar.bIsCombatWaitMode == true) then
        TL_SetProperty("Set Help Line", 10, "In Turn-Combat, fire cards get +1 damage per card past the first.")
        TL_SetProperty("Set Help Line", 11, "Fire cards are great for offense. Combo them with death cards!")
        TL_SetProperty("Set Help Line", 12, "Fire cards have no special defense effect.")
    end

--[Wind Cards]
elseif(iIndex == gciDeckEd_Help_Wind) then
    TL_SetProperty("Set Help Header", "Wind Elemental Cards")
    TL_SetProperty("Set Help Line", 0, "Wind Element cards increase damage by 1 and defense by 0.5 when added")
    TL_SetProperty("Set Help Line", 1, "to an action.")
    TL_SetProperty("Set Help Line", 3, "If multiple Wind cards are used in the same action, the effect increases")
    TL_SetProperty("Set Help Line", 4, "by +1/+.5 per additional card.")
    TL_SetProperty("Set Help Line", 6, "Wind damage and defense is increased by your wind elemental bonus.")
    TL_SetProperty("Set Help Line", 8, "Certain enemies will take extra damage from wind cards.")
    
    if(gzTextVar.bIsCombatWaitMode == true) then
        TL_SetProperty("Set Help Line", 10, "In Turn-Combat, played wind cards allow you to draw 3 cards without")
        TL_SetProperty("Set Help Line", 11, "allowing the enemy time to move. This applies to offense and defense.")
        TL_SetProperty("Set Help Line", 12, "Each wind card past the first allows 1 more free draw.")
    end

--[Earth Cards]
elseif(iIndex == gciDeckEd_Help_Earth) then
    TL_SetProperty("Set Help Header", "Earth Elemental Cards")
    TL_SetProperty("Set Help Line", 0, "Earth Element cards increase damage by 1 and defense by 0.5 when added")
    TL_SetProperty("Set Help Line", 1, "to an action.")
    TL_SetProperty("Set Help Line", 3, "If multiple Earth cards are used in the same action, the effect increases")
    TL_SetProperty("Set Help Line", 4, "by +1/+.5 per additional card.")
    TL_SetProperty("Set Help Line", 6, "Earth damage and defense is increased by your earth elemental bonus.")
    TL_SetProperty("Set Help Line", 8, "Certain enemies will take extra damage from earth cards.")
    
    if(gzTextVar.bIsCombatWaitMode == true) then
        TL_SetProperty("Set Help Line", 10, "In Turn-Combat, earth cards slow the enemy down slightly when attacking.")
        TL_SetProperty("Set Help Line", 11, "In addition, each earth card past the first provides 1 free shield,")
        TL_SetProperty("Set Help Line", 12, "regardless of whether you are attacking or defending.")
    end

--[Life Cards]
elseif(iIndex == gciDeckEd_Help_Life) then
    TL_SetProperty("Set Help Header", "Life Elemental Cards")
    TL_SetProperty("Set Help Line", 0, "A rare special card that increases damage by 2 and creates 1 shield which")
    TL_SetProperty("Set Help Line", 1, "blocks all damage from a single attack.")
    TL_SetProperty("Set Help Line", 3, "When used on an attack, also stuns the enemy for 5 seconds and resets")
    TL_SetProperty("Set Help Line", 4, "their attack timers.")

--[Death Cards]
elseif(iIndex == gciDeckEd_Help_Death) then
    TL_SetProperty("Set Help Header", "Death Elemental Cards")
    TL_SetProperty("Set Help Line", 0, "A rare special card that increases damage by 2 and defense by 1 when")
    TL_SetProperty("Set Help Line", 1, "added to an action.")
    TL_SetProperty("Set Help Line", 3, "If used on an attack, doubles the damage of the entire attack in")
    TL_SetProperty("Set Help Line", 4, "addition to its own attack bonus.")

--[Attack Cards]
elseif(iIndex == gciDeckEd_Help_Attack) then
    TL_SetProperty("Set Help Header", "Attack Cards")
    TL_SetProperty("Set Help Line", 0, "Causes your next action to damage the enemy. Only one action can")
    TL_SetProperty("Set Help Line", 1, "be performed at a time. Selecting a different action replaces")
    TL_SetProperty("Set Help Line", 2, "the selected card.")
    TL_SetProperty("Set Help Line", 4, "Your basic attacks increase in damage as more elements are added")
    TL_SetProperty("Set Help Line", 5, "to the action. Your attack power bonus from equipment is also")
    TL_SetProperty("Set Help Line", 6, "added to the damage dealt.")
    
--[Defend Cards]
elseif(iIndex == gciDeckEd_Help_Defend) then
    TL_SetProperty("Set Help Header", "Defend Cards")
    TL_SetProperty("Set Help Line", 0, "Causes your next action to create defensive shields. These appear")
    TL_SetProperty("Set Help Line", 1, "above your health chips, and take damage in place of your health.")
    TL_SetProperty("Set Help Line", 3, "Every shield created stops half a heart of damage. Shields created")
    TL_SetProperty("Set Help Line", 4, "with the Life element stop all incoming damage by themselves. Shields")
    TL_SetProperty("Set Help Line", 5, "are lost when they block an attack.")
    TL_SetProperty("Set Help Line", 7, "Elemental indicators appear around your shields indicating an element")
    TL_SetProperty("Set Help Line", 8, "they block in their entirety. Certain enemies use certain elements.")
    
--[And Cards]
elseif(iIndex == gciDeckEd_Help_BridgeAnd) then
    TL_SetProperty("Set Help Header", "And Cards")
    TL_SetProperty("Set Help Line", 0, "A 'Bridge' card, you can play additional element cards by placing")
    TL_SetProperty("Set Help Line", 1, "these cards between them. Once an element card is played, play")
    TL_SetProperty("Set Help Line", 2, "the 'And' card and then play another element.")
    TL_SetProperty("Set Help Line", 4, "More cards increase the effect by your Sentence Length bonus.")
    
--[Of Cards]
elseif(iIndex == gciDeckEd_Help_BridgeOf) then
    TL_SetProperty("Set Help Header", "Of Cards")
    TL_SetProperty("Set Help Line", 0, "A 'Bridge' card, you can play one extra element at the end of the")
    TL_SetProperty("Set Help Line", 1, "action sentence by playing this card first.")
    TL_SetProperty("Set Help Line", 3, "More cards increase the effect by your Sentence Length bonus.")

--[Stats]
elseif(iIndex == gciDeckEd_Help_Stats) then
    TL_SetProperty("Set Help Header", "Your Statistics")
    TL_SetProperty("Set Help Line", 0, "These show the bonuses you receive from your worn equipment.")
    TL_SetProperty("Set Help Line", 2, "Attack Bonus increases the amount of damage your Attack cards deal.")
    TL_SetProperty("Set Help Line", 4, "Defend Bonus increases the shields created by your Defend cards.")
    TL_SetProperty("Set Help Line", 6, "Starting Shields are created at the start of combat for free to")
    TL_SetProperty("Set Help Line", 7, "protect you until you can act.")
    TL_SetProperty("Set Help Line", 9, "Sentence Length increases the effect bonus from playing more cards.")

--[Elemental Bonuses]
elseif(iIndex == gciDeckEd_Help_ElementBonus) then
    TL_SetProperty("Set Help Header", "Elemental Bonuses")
    TL_SetProperty("Set Help Line", 0, "These are the bonuses provided by your equipment. They amplify the")
    TL_SetProperty("Set Help Line", 1, "effect of the matching element when played.")
end