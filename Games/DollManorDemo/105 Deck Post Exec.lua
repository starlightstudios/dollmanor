--[Deck Post-Exec]
--When the deck handler closes under the "Accept"/"Defaults" buttons, this populates the data back into Lua.
-- The cancel button does not do anything other than close the editor.

--Argument Listing:
-- 0: sType - Will be "Accept" or "Defaults".

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--[Accept]
--Take the data out of the Deck Editor and put it in the lua values.
if(sString == "Accept") then
    
    --Collect values.
    gzTextVar.gzPlayer.zDeck.iAttackCards = TL_GetProperty("Deck Card Count", gciDeckEd_Attack)
    gzTextVar.gzPlayer.zDeck.iDefendCards = TL_GetProperty("Deck Card Count", gciDeckEd_Defend)
    gzTextVar.gzPlayer.zDeck.iWaterCards  = TL_GetProperty("Deck Card Count", gciDeckEd_Water)
    gzTextVar.gzPlayer.zDeck.iFireCards   = TL_GetProperty("Deck Card Count", gciDeckEd_Fire)
    gzTextVar.gzPlayer.zDeck.iWindCards   = TL_GetProperty("Deck Card Count", gciDeckEd_Wind)
    gzTextVar.gzPlayer.zDeck.iEarthCards  = TL_GetProperty("Deck Card Count", gciDeckEd_Earth)
    gzTextVar.gzPlayer.zDeck.iLifeCards   = TL_GetProperty("Deck Card Count", gciDeckEd_Life)
    gzTextVar.gzPlayer.zDeck.iDeathCards  = TL_GetProperty("Deck Card Count", gciDeckEd_Death)
    gzTextVar.gzPlayer.zDeck.iOfCards     = TL_GetProperty("Deck Card Count", gciDeckEd_BridgeOf)
    gzTextVar.gzPlayer.zDeck.iAndCards    = TL_GetProperty("Deck Card Count", gciDeckEd_BridgeAnd)
    
    --Parallel array for fast-access.
    gzTextVar.gzPlayer.zDeck.iaCardArray = {gzTextVar.gzPlayer.zDeck.iAttackCards, gzTextVar.gzPlayer.zDeck.iDefendCards, gzTextVar.gzPlayer.zDeck.iWaterCards, gzTextVar.gzPlayer.zDeck.iFireCards, gzTextVar.gzPlayer.zDeck.iWindCards, gzTextVar.gzPlayer.zDeck.iEarthCards, gzTextVar.gzPlayer.zDeck.iLifeCards, gzTextVar.gzPlayer.zDeck.iDeathCards, gzTextVar.gzPlayer.zDeck.iOfCards, gzTextVar.gzPlayer.zDeck.iAndCards}
    
--[Defaults]
--Reset everything to defaults.
else
    gzTextVar.gzPlayer.zDeck.iAttackCards = 7
    gzTextVar.gzPlayer.zDeck.iDefendCards = 5
    gzTextVar.gzPlayer.zDeck.iWaterCards = 4
    gzTextVar.gzPlayer.zDeck.iFireCards = 4
    gzTextVar.gzPlayer.zDeck.iWindCards = 4
    gzTextVar.gzPlayer.zDeck.iEarthCards = 4
    gzTextVar.gzPlayer.zDeck.iLifeCards = 0
    gzTextVar.gzPlayer.zDeck.iDeathCards = 0
    gzTextVar.gzPlayer.zDeck.iOfCards = 2
    gzTextVar.gzPlayer.zDeck.iAndCards = 5
    gzTextVar.gzPlayer.zDeck.iaCardArray = {7, 5, 4, 4, 4, 4, 0, 0, 2, 5}
end