--Delayed Builder script. Used to construct the world and start the game in-stride with a cutscene.

--Load Interrupt.
if(gzTextVar.sManorType == "Normal") then
    LI_Reset(384)
elseif(gzTextVar.sManorType == "Tutorial") then
    LI_Reset(37)
end

--Boot the game!
TL_SetProperty("Set Hide Stats Flag", false)
LM_ExecuteScript(gzTextVar.sRootPath .. "001 System Variables.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "002 Map Builder.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "003 Scenario Variables.lua")
LM_ExecuteScript(gzTextVar.sRootPath .. "004 Scenario Builder.lua")

--Build the initial command listing.
LM_ExecuteScript(gzTextVar.sRootPath .. "200 Command Builder.lua")

--Set combat type.
if(gzTextVar.bIsCombatWaitMode == true) then
    TL_SetProperty("Set Combat Active Mode", false)
else
    TL_SetProperty("Set Combat Active Mode", true)
end

--Reresolve fading properties once everything is in place.
TL_SetProperty("Reresolve Fades")

--Fade the automap.
TL_SetProperty("Set Map Fade Timer", 1.0)
TL_SetProperty("Set Map Fade Delta", (1.0 / (60.0 * 3.0) * -1.0))
fnBuildLocalityInfo()
LI_WaitForKeypress()
LI_Finish()

--Fade the world in.
TL_SetProperty("Begin Fade In", "Null")

--Open Locality Categories
TL_SetProperty("Set Locality Open State", "Equipment", true)
TL_SetProperty("Set Locality Open State", "Inventory", true)
TL_SetProperty("Set Locality Open State", "Entities", true)
TL_SetProperty("Set Locality Open State", "Objects", true)