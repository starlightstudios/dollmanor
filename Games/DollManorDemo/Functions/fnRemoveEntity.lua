--[fnRemoveEntity]
--Removes the entity in the provided index.
fnRemoveEntity = function(iIndex)
    
    --Argument check.
    if(iIndex == nil) then return end
    if(iIndex < 1 or iIndex > gzTextVar.zEntitiesTotal) then return end
    
    --Remove the entity indicator.
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[iIndex].sLocation)
    TL_SetProperty("Unregister Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[iIndex].sIndicatorName)
    
    --If the entity is the last one in the list, we can just nil it off and be done with it.
    if(iIndex == gzTextVar.zEntitiesTotal) then
        gzTextVar.zEntities[gzTextVar.zEntitiesTotal] = nil
        gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal - 1
        return
    end
    
    --Otherwise, we need to copy-up.
    for i = iIndex, gzTextVar.zEntitiesTotal, 1 do
        gzTextVar.zEntities[i] = gzTextVar.zEntities[i + 1]
    end
    
    --Now deallocate the last one and decrement.
    gzTextVar.zEntities[gzTextVar.zEntitiesTotal] = nil
    gzTextVar.zEntitiesTotal = gzTextVar.zEntitiesTotal - 1
end