--[fnRegisterObject]
--Creates an item that will be located in the given room. These are usually inventory items, but can also be
-- examinable objects if the player wants to get more details.

--These variables are populated with the last-used information in case the slot of the room/item is needed.
giLastRoom = -1
giLastItem = -1

--Function.
fnRegisterObject = function(sRoomName, sObjectUniqueName, sObjectDisplayName, bCanBePickedUp, sHandlerScript)
    
    --Reset these to -1.
    giLastItem = -1
    giLastRoom = -1
    
    --Argument Check
    if(sRoomName          == nil) then return end
    if(sObjectUniqueName  == nil) then return end
    if(sObjectDisplayName == nil) then return end
    if(bCanBePickedUp     == nil) then return end
    
    --Find the room in question.
    local iRoom = -1
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomName) then
            iRoom = i
            break
        end
    end
    if(iRoom == -1) then return end
    
    --Increment object count.
    gzTextVar.zRoomList[iRoom].iObjectsTotal = gzTextVar.zRoomList[iRoom].iObjectsTotal + 1
    local p = gzTextVar.zRoomList[iRoom].iObjectsTotal
    
    --Create the object.
    gzTextVar.zRoomList[iRoom].zObjects[p] = {}
    gzTextVar.zRoomList[iRoom].zObjects[p].sUniqueName = sObjectUniqueName
    gzTextVar.zRoomList[iRoom].zObjects[p].sDisplayName = sObjectDisplayName
    gzTextVar.zRoomList[iRoom].zObjects[p].bCanBePickedUp = bCanBePickedUp
    gzTextVar.zRoomList[iRoom].zObjects[p].sHandlerScript = sHandlerScript
    gzTextVar.zRoomList[iRoom].zObjects[p].sIndicatorName = "Null"
    
    --Special flags. Not all objects use these.
    gzTextVar.zRoomList[iRoom].zObjects[p].bIsStackable = false
    gzTextVar.zRoomList[iRoom].zObjects[p].iStackSize = 1
    gzTextVar.zRoomList[iRoom].zObjects[p].iJournalPage = -1
    gzTextVar.zRoomList[iRoom].zObjects[p].bIsClaygirlTrapped = false
    gzTextVar.zRoomList[iRoom].zObjects[p].sClaygirlName = "Null"
    gzTextVar.zRoomList[iRoom].zObjects[p].saClaygirlList = {}
    
    --Stack Check. Run the script with the state machine variable set.
    gzTextVar.bIsCheckingStacking = true
    gzTextVar.bCanStack = false
    LM_ExecuteScript(sHandlerScript)
    gzTextVar.zRoomList[iRoom].zObjects[p].bIsStackable = gzTextVar.bCanStack
    gzTextVar.bIsCheckingStacking = false
    gzTextVar.bCanStack = false
    
    --Store.
    giLastRoom = iRoom
    giLastItem = p
    
    --[Indicator]
    --Call the item script to get its indicator information. If it has no indicator, it will not change the state machine vars.
    gzTextVar.iGlobalIndicatorX = -1
    gzTextVar.iGlobalIndicatorY = -1
    gzTextVar.bIsBuildingIndicatorInfo = true
    LM_ExecuteScript(sHandlerScript)
    gzTextVar.bIsBuildingIndicatorInfo = false
    
    --If the indicators are not -1's, spawn an indicator in the given room.
    if(gzTextVar.iGlobalIndicatorX ~= -1 and gzTextVar.iGlobalIndicatorY ~= -1) then
        
        --Create a unique name.
        local sIndicatorName = fnGenerateIndicatorName()
        gzTextVar.zRoomList[iRoom].zObjects[p].sIndicatorName = sIndicatorName
        
        --Upload.
        local fX, fY, fZ = fnGetRoomPosition(sRoomName)
        TL_SetProperty("Register Entity Indicator", fX, fY, fZ, sIndicatorName, gzTextVar.iGlobalIndicatorX, gzTextVar.iGlobalIndicatorY, ciCodeItem)
    end
end
