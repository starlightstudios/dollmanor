--[fnNoReadCheck]
--Checks if the player cannot read because they are non-human. Returns true if they cannot, false if not.
-- Also calls ZStandardNoRead.lua if they cannot read. Can be toggled off.
fnNoReadCheck = function(bCallNoRead)
    if(bCallNoRead == nil) then bCallNoRead = false end
    
    --Setup.
    local bReturnValue = false
    local sCallPath = gzTextVar.sRootPath .. "Item Handlers/ZStandardNoRead.lua"
    
    --If the player is a human:
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
        --Dumb-Mary.
        if(gzTextVar.iGameStage == 5) then
            bReturnValue = true
        end
    
    --Statue version.
    elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
        bReturnValue = true

    --Claygirl version.
    elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
        bReturnValue = true

    --Rubber version.
    elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
        bReturnValue = true

    --Ice version.
    elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
        bReturnValue = true

    --Glass version.
    elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
        bReturnValue = true

    --Doll version.
    else
        --Before phase 6, the doll player cannot read.
        if(gzTextVar.iGameStage ~= 6) then
            bReturnValue = true
        end
    end
    
    --Handler.
    if(bCallNoRead == true and bReturnValue == true) then
        gbHandledInput = true
        LM_ExecuteScript(sCallPath)
    end
    return bReturnValue
end