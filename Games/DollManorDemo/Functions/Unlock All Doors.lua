--[ ===================================== Unlock All Doors ====================================== ]
--A subroutine used either by debug or when the player is transformed. Unlocks all doors in the manor,
-- setting them to Normal instead of whatever lock they had previously.
for i = 1, gzTextVar.iRoomsTotal, 1 do
    
    if(gzTextVar.zRoomList[i].sDoorN ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorN, 1, 1) == "C") then
        gzTextVar.zRoomList[i].sDoorN = "C Normal"
    elseif(gzTextVar.zRoomList[i].sDoorN ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorN, 1, 1) == "O") then
        gzTextVar.zRoomList[i].sDoorN = "O Normal"
    end
    if(gzTextVar.zRoomList[i].sDoorE ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorE, 1, 1) == "C") then
        gzTextVar.zRoomList[i].sDoorE = "C Normal"
        TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
    elseif(gzTextVar.zRoomList[i].sDoorE ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorE, 1, 1) == "O") then
        gzTextVar.zRoomList[i].sDoorE = "O Normal"
        TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
    end
    if(gzTextVar.zRoomList[i].sDoorS ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorS, 1, 1) == "C") then
        gzTextVar.zRoomList[i].sDoorS = "C Normal"
        TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
    elseif(gzTextVar.zRoomList[i].sDoorS ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorS, 1, 1) == "O") then
        gzTextVar.zRoomList[i].sDoorS = "O Normal"
        TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
    end
    if(gzTextVar.zRoomList[i].sDoorW ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorW, 1, 1) == "C") then
        gzTextVar.zRoomList[i].sDoorW = "C Normal"
    elseif(gzTextVar.zRoomList[i].sDoorW ~= nil and string.sub(gzTextVar.zRoomList[i].sDoorW, 1, 1) == "O") then
        gzTextVar.zRoomList[i].sDoorW = "O Normal"
    end
    
end