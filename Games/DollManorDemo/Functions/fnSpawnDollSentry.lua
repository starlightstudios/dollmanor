--[ ==================================== fnSpawnDollSentry() ==================================== ]
--Spawns a doll enemy. Dolls are enemies unless the player is also a doll.
-- This particular version does not move, but is otherwise identical to normal dolls.
fnSpawnDollSentry = function(sSpawnLocation, sIdentifier, saPatrolList)
    
    --[Argument Handler]
    if(sSpawnLocation == nil) then return end
    if(sIdentifier == nil) then return end
    
    --[Creation]
    --Call the original.
    fnSpawnDoll(sSpawnLocation, sIdentifier, saPatrolList)
    
    --Locate the last-spawned entity.
    if(gzTextVar.iLastSpawnedEntity == -1) then return end
    
    --Change the AI to the stationary one.
    local i = gzTextVar.iLastSpawnedEntity
    gzTextVar.zEntities[i].sAIHandlerHostility = "Tutorial Stationary"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Tutorial Stationary"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Tutorial Stationary"
    
end