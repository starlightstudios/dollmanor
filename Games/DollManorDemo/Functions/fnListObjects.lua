--[fnListObjects]
--Lists all objects at a given location. If bSkipNoObjects is true, then "There are no objects" will not display.
fnListObjects = function(sLocation, bSkipNoObjects)
    
    --Argument check.
    if(sLocation == nil)      then return end
    if(bSkipNoObjects == nil) then return end
    
    --Get the index of the room.
    local iRoomIndex = fnGetRoomIndex(sLocation)
    
    --No objects.
    if(gzTextVar.zRoomList[iRoomIndex].iObjectsTotal == 0) then
        if(bSkipNoObjects == false) then
            TL_SetProperty("Append", "There are no objects in this room." .. gsDE)
        end
    
    --One object.
    elseif(gzTextVar.zRoomList[iRoomIndex].iObjectsTotal == 1) then
        TL_SetProperty("Append", "You can see a [" .. gzTextVar.zRoomList[iRoomIndex].zObjects[1].sDisplayName .. "]." .. gsDE)
    
    --Two objects.
    elseif(gzTextVar.zRoomList[iRoomIndex].iObjectsTotal == 2) then
        TL_SetProperty("Append", "You can see a [" .. gzTextVar.zRoomList[iRoomIndex].zObjects[1].sDisplayName .. "] and a [" .. gzTextVar.zRoomList[iRoomIndex].zObjects[2].sDisplayName .. "]." .. gsDE)
    
    --List.
    else
    
        --Setup.
        local sSentence = "You can see "
        
        --All members except the end.
        for i = 1, gzTextVar.zRoomList[iRoomIndex].iObjectsTotal - 1, 1 do
            sSentence = sSentence .. "[" .. gzTextVar.zRoomList[iRoomIndex].zObjects[i].sDisplayName .. "], "
        end
        
        --Add to the end.
        sSentence = sSentence .. "and [" .. gzTextVar.zRoomList[iRoomIndex].zObjects[gzTextVar.zRoomList[iRoomIndex].iObjectsTotal].sDisplayName .. "]." .. gsDE
        
        --Append it.
        TL_SetProperty("Append", sSentence)
    
    end
end