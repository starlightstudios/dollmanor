--[fnCreateJournalPage]
--Handles creating a journal page. Depends on fnRegisterObject().
fnCreateJournalPage = function(sLocation, iPage)
    
    --Arg check.
    if(sLocation == nil) then return end
    if(iPage     == nil) then return end
    
    --Call.
    fnRegisterObject(sLocation, "journal page", "journal page", true, gzTextVar.sRootPath .. "Item Handlers/Books/Journal Page.lua")
    
    --Mark the item as having a specific page. This allows the read text to display correctly.
    if(giLastRoom ~= -1 and giLastItem ~= -1) then
        gzTextVar.zRoomList[giLastRoom].zObjects[giLastItem].iJournalPage = iPage
    end
    
end