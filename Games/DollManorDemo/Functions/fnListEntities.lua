--[fnListEntities]
--Lists all entities at a given location. If bSkipNoEntities is true, then "There are no entities" will not display.
fnListEntities = function(sLocation, bSkipNoEntities)
    
    --Argument check.
    if(sLocation == nil)       then return end
    if(bSkipNoEntities == nil) then return end

    --Count entities.
    local iCount = 0
    local saEntityNames = {}
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].sLocation == sLocation) then
            
            --Disguised entities do not appear!
            if(gzTextVar.zEntities[i].sInDisguise == nil or gzTextVar.zEntities[i].sInDisguise == "Null") then
                iCount = iCount + 1
                saEntityNames[iCount] = gzTextVar.zEntities[i].sDisplayName
            end
        end
    end
    
    --No entities.
    if(iCount == 0) then
        if(bSkipNoEntities == false) then
            TL_SetProperty("Append", "There are no entities in the room with you." .. gsDE)
        end

    --One entity.
    elseif(iCount == 1) then
        TL_SetProperty("Append", "You can see [" .. saEntityNames[1] .. "] here." .. gsDE)
    
    --Two entities.
    elseif(iCount == 2) then
        TL_SetProperty("Append", "You can see [" .. saEntityNames[1] .. "] and [" .. saEntityNames[2] .. "] here." .. gsDE)
    
    --List.
    else
        --Setup.
        local sSentence = "You can see "
        
        --All members except the end.
        for i = 1, iCount-1, 1 do
            sSentence = sSentence .. "[" .. saEntityNames[i] .. "], "
        end
        
        --Add to the end.
        sSentence = sSentence .. "and [" .. saEntityNames[iCount] .. "] here." .. gsDE
        
        --Append it.
        TL_SetProperty("Append", sSentence)
    
    end
end