--[fnGetIndexOfItemInRoom]
--Returns the first found index of the named item in the named room, or -1 if none exist.
fnGetIndexOfItemInRoom = function(sRoomName, sItemName)
   
    if(sRoomName == nil) then return -1 end
    if(sItemName == nil) then return -1 end
    
    --Get the room in question.
    local iRoom = fnGetRoomIndex(sRoomName)
    if(iRoom == -1) then return end
    
    --Iterate.
    for i = 1, gzTextVar.zRoomList[iRoom].iObjectsTotal, 1 do
        if(gzTextVar.zRoomList[iRoom].zObjects[i].sDisplayName == sItemName) then
            return i
        end
    end
    
    return -1
end