--[fnDialogueCutscene]
--Standard simplified dialogue cutscene. Pass "Null" for sDisplayImg to use none. sDialogueB and sDialogueC are optional.
fnDialogueCutscene = function(sSpeakerName, iImgTotal, saDisplayImg, sDialogueA, sDialogueB, sDialogueC)
    
    --Argument check.
    if(iImgTotal == nil) then return end
    if(saDisplayImg == nil) then return end
    if(sDialogueA == nil) then return end
    
    --Display image set.
    for i = 1, iImgTotal, 1 do
        TL_SetProperty("Register Image", sSpeakerName, saDisplayImg[i], i-1)
    end
    
    --Dialogue additions.
    TL_SetProperty("Append", sDialogueA)
    if(sDialogueB ~= nil) then TL_SetProperty("Append", sDialogueB) end
    if(sDialogueC ~= nil) then TL_SetProperty("Append", sDialogueC) end
    
    --Ends the sequence.
    TL_SetProperty("Create Blocker")
end
fnDialogueFinale = function()
    TL_SetProperty("Unregister Image")
end