--[fnDropItem]
--Drops the item out of the inventory and places it in the room the player is in. Does several other tasks at once.
fnDropItem = function(piItemIndex)
    
    --Determine where the player currently is.
    local iRoomIndex = fnGetRoomIndex(gzTextVar.gzPlayer.sLocation)
    if(iRoomIndex == -1) then return end
    
    --Display.
    gbHandledInput = true
    TL_SetProperty("Append", "You drop the [" .. gzTextVar.gzPlayer.zaItems[piItemIndex].sDisplayName .. "]." .. gsDE)
    
    --[No Stack]
    --If the item's stack size was 1, then we can just wholesale move it to the room inventory.
    if(gzTextVar.gzPlayer.zaItems[piItemIndex].iStackSize == 1) then
    
        --Add it to the room's inventory.
        gzTextVar.zRoomList[iRoomIndex].iObjectsTotal = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal + 1
        local p = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal
        
        --Create the object, remove it from the inventory.
        gzTextVar.zRoomList[iRoomIndex].zObjects[p] = gzTextVar.gzPlayer.zaItems[piItemIndex]
        fnRemoveItemFromInventory(piItemIndex)
    
        --Call the item script to get its indicator information.
        gzTextVar.iGlobalIndicatorX = -1
        gzTextVar.iGlobalIndicatorY = -1
        gzTextVar.bIsBuildingIndicatorInfo = true
        LM_ExecuteScript(gzTextVar.zRoomList[iRoomIndex].zObjects[p].sHandlerScript)
        gzTextVar.bIsBuildingIndicatorInfo = false
                    
        --If the indicators are not -1's, spawn an indicator in the given room.
        if(gzTextVar.iGlobalIndicatorX ~= -1 and gzTextVar.iGlobalIndicatorY ~= -1) then
            
            --Create a unique name.
            local sIndicatorName = fnGenerateIndicatorName()
            gzTextVar.zRoomList[iRoomIndex].zObjects[p].sIndicatorName = sIndicatorName
            
            --Upload.
            local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
            TL_SetProperty("Register Entity Indicator", fX, fY, fZ, sIndicatorName, gzTextVar.iGlobalIndicatorX, gzTextVar.iGlobalIndicatorY, ciCodeItem)
        end
    
    --[Stack]
    --If there is a stack size of 2 or higher, we need to clone the item over.
    else
    
        --Create a copy.
        fnRegisterObject(gzTextVar.gzPlayer.sLocation, gzTextVar.gzPlayer.zaItems[piItemIndex].sUniqueName, gzTextVar.gzPlayer.zaItems[piItemIndex].sDisplayName, true, gzTextVar.gzPlayer.zaItems[piItemIndex].sHandlerScript)
        local p = gzTextVar.zRoomList[iRoomIndex].iObjectsTotal
    
        --Decrement the base item's stack size by 1.
        gzTextVar.gzPlayer.zaItems[piItemIndex].iStackSize = gzTextVar.gzPlayer.zaItems[piItemIndex].iStackSize - 1
    
    end
    
    --Locality handling.
    
end
