--[ ================================== Change Entity Indicator ================================== ]
--Script subroutine used in cutscenes. Changes the given entity's map indicator. Allows this to be
-- done in stride instead of immediately.
--Pass -1 to modify the player's indicator.

--Argument Listing:
-- 0: iIndex - Index of the changing entity. -1 is the player.
-- 1: iIndicatorX - X position on the tile sheet of the desired indicator.
-- 2: iIndicatorY - Y position on the tile sheet of the desired indicator.
-- 3: iPriority - Priority flag to specify where the indicator goes. Player, enemy, or friendly.
-- 4: iHostileX - X position for hosility. Pass -1 to set no hostility. (Optional)
-- 5: iHostileY - Y position for hosility. Pass -1 to set no hostility. (Optional)

--Arg check.
local iRequiredArgs = 4
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local iIndex      = tonumber(LM_GetScriptArgument(0))
local iIndicatorX = tonumber(LM_GetScriptArgument(1))
local iIndicatorY = tonumber(LM_GetScriptArgument(2))
local iPriority   = tonumber(LM_GetScriptArgument(3))
local bModifyHostility = false
local iHostileX = -1
local iHostileY = -1

if(iArgs >= 6) then
    bModifyHostility = true
    iHostileX   = tonumber(LM_GetScriptArgument(4))
    iHostileY   = tonumber(LM_GetScriptArgument(5))
end

--[Player]
if(iIndex == -1) then
    
    --Stoe.
    gzTextVar.gzPlayer.iIndicatorX = iIndicatorX
    gzTextVar.gzPlayer.iIndicatorY = iIndicatorY
    gzTextVar.gzPlayer.iIndicatorC = iPriority
    
    --Set.
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
    TL_SetProperty("Modify Entity Indicator", fX, fY, fZ, "Player", iIndicatorX, iIndicatorY, iPriority)
    TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, "Player", -1, -1)

--[Normal Entity]
else

    --Make sure the entity exists.
    if(iIndex < 1 or iIndex > gzTextVar.zEntitiesTotal) then return end
    
    --Store.
    gzTextVar.zEntities[iIndex].iIndicatorX = iIndicatorX
    gzTextVar.zEntities[iIndex].iIndicatorY = iIndicatorY
    gzTextVar.zEntities[iIndex].iIndicatorC = iPriority

    --Set.
    local fX, fY, fZ = fnGetRoomPosition(gzTextVar.zEntities[iIndex].sLocation)
    TL_SetProperty("Modify Entity Indicator", fX, fY, fZ, gzTextVar.zEntities[iIndex].sIndicatorName, iIndicatorX, iIndicatorY, iPriority)
    if(bModifyHostility == true) then
        TL_SetProperty("Modify Entity Indicator Hosility", fX, fY, fZ, gzTextVar.zEntities[iIndex].sIndicatorName, iHostileX, iHostileY)
    end

end
