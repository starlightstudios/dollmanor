--[fnGetDelimitedString]
--Returns a string delimited by spaces which was part of a larger string. Used for picking arguments out.
fnGetDelimitedString = function(sString, iStartIndex)
    
    --Arg check.
    if(sString == nil)     then return "Null", 0 end
    if(iStartIndex == nil) then return "Null", 0 end
    
    --Setup.
    local sNewString = ""
    local iLen = string.len(sString)
    local iLastLetter = 0
    
    --Iterate.
    for i = iStartIndex, iLen, 1 do
        
        --Letter
        iLastLetter = i
        local sLetter = string.sub(sString, i, i)
        
        --Break on spaces.
        if(sLetter == " ") then
            iLastLetter = iLastLetter - 1
            break
        --Otherwise, append.
        else
            sNewString = sNewString .. sLetter
        end
    end
    
    --Return it.
    return sNewString, iLastLetter
    
end