--[fnGetMovePath]
--Given a location, returns the first instruction on the path between the two locations. Returns "Null" if
-- no path can be found between the two.
fnGetMovePath = function(sStartLocation, sEndLocation)
    
    --Argument check.
    if(sStartLocation == nil) then return "Null" end
    if(sEndLocation   == nil) then return "Null" end
    
    --Room indices.
    local iStartRoomIndex = fnGetRoomIndex(sStartLocation)
    local iEndRoomIndex   = fnGetRoomIndex(sEndLocation)
    
    --Error checks.
    if(iStartRoomIndex == -1 or iEndRoomIndex == -1) then return end
    
    --Get the first letter of matching movement.
    local sPathInstructions = gzTextCon.zRoomList[iEndRoomIndex].zPathListing[iStartRoomIndex]
    if(sPathInstructions == "") then return "Null" end
    local sFirstLetter = string.sub(sPathInstructions, 1, 1)
    
    --Move in the requested direction:
    local sDestination = "Null"
    if(sFirstLetter == "N") then
        return gzTextVar.zRoomList[iStartRoomIndex].sMoveN
    elseif(sFirstLetter == "S") then
        return gzTextVar.zRoomList[iStartRoomIndex].sMoveS
    elseif(sFirstLetter == "E") then
        return gzTextVar.zRoomList[iStartRoomIndex].sMoveE
    elseif(sFirstLetter == "W") then
        return gzTextVar.zRoomList[iStartRoomIndex].sMoveW
    elseif(sFirstLetter == "U") then
        return gzTextVar.zRoomList[iStartRoomIndex].sMoveU
    elseif(sFirstLetter == "D") then
        return gzTextVar.zRoomList[iStartRoomIndex].sMoveD
    end

    --Failure, something went wrong.
    return "Null"
end