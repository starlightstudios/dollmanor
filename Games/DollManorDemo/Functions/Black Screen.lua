--[ ======================================= Black Screen ======================================== ]
--Routine to change the Z-render level to -100, used for cutscenes where the player blacks out.
TL_SetProperty("Set Rendering Z Level", -100)