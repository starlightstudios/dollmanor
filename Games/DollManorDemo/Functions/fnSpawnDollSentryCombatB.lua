--[ ================================= fnSpawnDollSentryCombat() ================================= ]
--Spawns a doll enemy. Dolls are enemies unless the player is also a doll.
-- This particular version does not move, but is otherwise identical to normal dolls.
fnSpawnDollSentryCombatB = function(sSpawnLocation, sIdentifier, saPatrolList)
    
    --[Argument Handler]
    if(sSpawnLocation == nil) then return end
    if(sIdentifier == nil) then return end
    
    --[Creation]
    --Call the original.
    fnSpawnDoll(sSpawnLocation, sIdentifier, saPatrolList)
    
    --Locate the last-spawned entity.
    if(gzTextVar.iLastSpawnedEntity == -1) then return end
    
    --Change the AI to the stationary one.
    local i = gzTextVar.iLastSpawnedEntity
    gzTextVar.zEntities[i].sAIHandlerHostility = "Tutorial Stationary Combat B"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Tutorial Stationary Combat B"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Tutorial Stationary Combat B"
    
    --Combat properties. Greatly increase the attack time so the player isn't overwhelmed.
    local fTimeFactor = 1.50
    gzTextVar.zEntities[i].zCombatTable.iHealth = 35
    gzTextVar.zEntities[i].zCombatTable.iAttacksTotal = 2
    gzTextVar.zEntities[i].zCombatTable.zAttacks = {}
    gzTextVar.zEntities[i].zCombatTable.zAttacks[1] = {100, 4, 400 * fTimeFactor, 0} --0.60 DPS
    gzTextVar.zEntities[i].zCombatTable.zAttacks[2] = {100, 6, 500 * fTimeFactor, 0} --0.72 DPS
    
end