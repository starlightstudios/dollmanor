--[Audio Routing]
--Loads audio files needed by Doll Manor.

--[Variables]
local sBasePath = fnResolvePath()

--[Registration Function]
--Loads music with set loops.
local function fnRegisterMusic(psMusicName, psPath, pfLoopStart, pfLoopEnd)
	
	--Arg check.
	if(psMusicName == nil) then return end
	if(psPath == nil) then return end
	
	--If the pfLoopStart and/or pfLoopEnd are nil, then this doesn't loop.
	if(pfLoopStart == nil or pfLoopEnd == nil) then
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath)
		
	--Otherwise, register with looping.
	else
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath, pfLoopStart, pfLoopEnd)
	end

end

--[Music]
--Fixed Run Tracks
fnRegisterMusic("Doll|BattleTheme",      sBasePath .. "Music/BattleTheme.ogg", 1.000, 58.000)
fnRegisterMusic("Doll|BattleThemeOutro", sBasePath .. "Music/BattleThemeOutro.ogg")

--Ambient Tracks
fnRegisterMusic("Doll|Ambient|Basement", sBasePath .. "Music/Ambient_Basement.ogg", 7.531,  60 +  1.475)
fnRegisterMusic("Doll|Ambient|Interior", sBasePath .. "Music/Ambient_Interior.ogg", 0.000, 120 +  0.000)
fnRegisterMusic("Doll|Ambient|Rain",     sBasePath .. "Music/Ambient_Rain.ogg",     0.050,       21.000)

--[SFX]
--World
AudioManager_Register("Doll|OpenDoor",       "AsSound", "AsSample", sBasePath .. "SFX/OpenDoor.ogg")
AudioManager_Register("Doll|OpenDoorLocked", "AsSound", "AsSample", sBasePath .. "SFX/OpenDoorLocked.ogg")
AudioManager_Register("Doll|CloseDoor",      "AsSound", "AsSample", sBasePath .. "SFX/CloseDoor.ogg")
AudioManager_Register("Doll|FootstepsA",     "AsSound", "AsSample", sBasePath .. "SFX/FootstepsA.ogg")
AudioManager_Register("Doll|FootstepsB",     "AsSound", "AsSample", sBasePath .. "SFX/FootstepsB.ogg")
AudioManager_Register("Doll|FootstepsC",     "AsSound", "AsSample", sBasePath .. "SFX/FootstepsC.ogg")
AudioManager_Register("Doll|FootstepsD",     "AsSound", "AsSample", sBasePath .. "SFX/FootstepsD.ogg")
AudioManager_Register("Doll|FootstepsE",     "AsSound", "AsSample", sBasePath .. "SFX/FootstepsE.ogg")
AudioManager_Register("Doll|TextRoll",       "AsSound", "AsSample", sBasePath .. "SFX/TextRoll.ogg")
AudioManager_Register("World|FlipSwitch",    "AsSound", "AsSample", sBasePath .. "SFX/FlipSwitch.wav")
AudioManager_Register("World|TakeItem",      "AsSound", "AsSample", sBasePath .. "SFX/TakeItem.wav")

--UI.
AudioManager_Register("Menu|TextTick", "AsSound", "AsSample", sBasePath .. "SFX/TextTick.ogg")

--Combat .
AudioManager_Register("Combat|AttackMiss",         "AsSound", "AsSample", sBasePath .. "SFX/AttackMiss.wav")
AudioManager_Register("Combat|Impact_Slash",       "AsSound", "AsSample", sBasePath .. "SFX/Impact_Slash.ogg")
AudioManager_Register("Combat|Impact_Strike_Crit", "AsSound", "AsSample", sBasePath .. "SFX/Impact_Strike_Crit.ogg")
AudioManager_Register("Combat|MonsterDie",         "AsSound", "AsSample", sBasePath .. "SFX/MonsterDie.wav")
AudioManager_Register("Combat|Impact_Pierce",      "AsSound", "AsSample", sBasePath .. "SFX/Impact_Pierce.ogg")
AudioManager_Register("Combat|Impact_Pierce_Crit", "AsSound", "AsSample", sBasePath .. "SFX/Impact_Pierce_Crit.ogg")
AudioManager_Register("Combat|Impact_Buff",        "AsSound", "AsSample", sBasePath .. "SFX/Impact_Buff.ogg")
AudioManager_Register("Combat|Debuff",             "AsSound", "AsSample", sBasePath .. "SFX/Debuff.ogg")
