--[ ===================================== Standardized Take ===================================== ]
--For all objects which have a "take" command, you can just use this.

--Argument Listing:
-- 0: sLocalName - The name of the item when referring to itself. eg "Blue Storybook"
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg Resolve
local sLocalName = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--[ ========================================= Execution ========================================= ]
--Ignore this if the item in question is already in the inventory.
if(iRoomSlot == -1) then
    
    --If there are potions in the inventory but not on the ground, print a warning message.
    local iIndexInRoom = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
    if(iIndexInRoom == -1) then
        TL_SetProperty("Append", "You already have that in your inventory." .. gsDE)
        gbHandledInput = true
    end
    return
end

--Wound handler.
if(gzTextVar.gzPlayer.bIsCrippled) then
    gbHandledInput = true
    TL_SetProperty("Append", "You are too hurt to pick that up." .. gsDE)
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
    return
end

--If this is an emulation check, don't actually do anything. Just indicate we responded.
if(gzTextVar.iItemEmulation == 1) then
    gzTextVar.iItemEmulation = 2
    return
end

--Otherwise, take the item.
TL_SetProperty("Append", "You pick up the [" .. sLocalName .. "]." .. gsDE)

--Increment
gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
local p = gzTextVar.gzPlayer.iItemsTotal

--Create the item.
gzTextVar.gzPlayer.zaItems[p] = {}
gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]

--Remove the item from the original room listing.
fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
if(gzTextVar.bTakeAll == false) then
    LM_ExecuteScript(gzTextVar.sTurnEndScript)
end
gbHandledInput = true
