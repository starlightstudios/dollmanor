--[ =========================================== Torch =========================================== ]
--+1 Attack, provides heat in the ice caverns.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 0
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot  - The item slot of the item in question.
-- 2: iRoomSlot  - The room slot of the item in question.
-- 3: iEquipSlot - The equipment slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[Arg Resolve]
local sInputString = LM_GetScriptArgument(0)
local iItemSlot  = tonumber(LM_GetScriptArgument(1))
local iRoomSlot  = tonumber(LM_GetScriptArgument(2))
local iEquipSlot = tonumber(LM_GetScriptArgument(3))

--[Use Weapon Prototype]
local iAtkBonus = 1
local iDefBonus = 0
local iShields = 0
local sDescription = "A torch with a cloth dipped in pine resin. When lit, it provides heat and light, increasing physical damage by 1 and fire damage by 2. You can [equip] or [unequip] it." .. gsDE
LM_ExecuteScript(fnResolvePath() .. "ZPrototype Weapon.lua", sInputString, iItemSlot, iRoomSlot, iEquipSlot, iAtkBonus, iDefBonus, iShields, sDescription)

--Bonus to fire damage. Not handled as part of the prototype.
if(gzTextVar.bIsEquipmentCheck) then
    gzTextVar.iFireBonus = gzTextVar.iFireBonus + 2
    return
end