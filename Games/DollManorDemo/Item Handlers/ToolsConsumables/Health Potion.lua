--[ ======================================= Health Potion ======================================= ]
--Health Potion handler script. Deals with things like examining and consuming.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 2
    gzTextVar.iGlobalIndicatorY = 2
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    gzTextVar.bCanStack = true
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drink", "drink " .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drink", "drink " .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ========================================== Priority ========================================= ]
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iConsumables
    return
end

--[ ======================================= Trap Builder ======================================== ]
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "drink"}
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[Remap]
--Remaps the input string to handle potions if they are stacked. This strips off the " x 3" or whatever the stack is.
local iSpaceStart, iSpaceEnd = string.find(sInputString, " " .. sLocalName)
local iTargetStart, iTargetEnd = string.find(sInputString, sLocalName)
if(iSpaceStart == nil or iSpaceEnd == nil) then return end
if(iTargetStart == nil or iTargetEnd == nil) then return end

--Reassemble the string.
local sFirstString = string.sub(sInputString, 1, iSpaceStart)
local sSecondString = string.sub(sInputString, iTargetStart, iTargetEnd)
sInputString = sFirstString .. sSecondString

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    TL_SetProperty("Append", "A bubbling red potion. Will restore all HP when used with the [drink] command." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    TL_SetProperty("Append", "A bubbling red potion. Will restore all HP when used with the [drink] command." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    --Ignore this if the item in question is already in the inventory.
    if(iRoomSlot == -1) then
        
        --If there are potions in the inventory but not on the ground, print a warning message.
        local iIndexInRoom = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        if(iIndexInRoom == -1) then
            TL_SetProperty("Append", "You already have that in your inventory." .. gsDE)
            gbHandledInput = true
        end
        return
    end
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to pick that up." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Otherwise, take the item.
    gbHandledInput = true
    TL_SetProperty("Append", "You pick up the [" .. sLocalName .. "]." .. gsDE)
    
    --[Stack Checking]
    --Scan the player's inventory for an existing item with the same name.
    local iExistingSlot = -1
    for i = 1, gzTextVar.gzPlayer.iItemsTotal, 1 do
        
        --The unique names must match. If they do, increment the stack size by 1.
        if(gzTextVar.gzPlayer.zaItems[i].sUniqueName == gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sUniqueName) then
            gzTextVar.gzPlayer.zaItems[i].iStackSize = gzTextVar.gzPlayer.zaItems[i].iStackSize + 1
            iExistingSlot = i
            break
        end
    end
    
    --[No Stack]
    --If the item did not stack, it needs a new entry in the inventory.
    if(iExistingSlot == -1) then
    
        --Increment
        gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
        local p = gzTextVar.gzPlayer.iItemsTotal
        
        --Create the item.
        gzTextVar.gzPlayer.zaItems[p] = {}
        gzTextVar.gzPlayer.zaItems[p] = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot]
    end
    
    --[Removal]
    --Remove the item from the original room listing.
    fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    if(gzTextVar.bTakeAll == false) then
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)
    LM_ExecuteScript(gzTextVar.sTurnEndScript)

--Drinking it.
elseif(sInputString == "drink " .. sLocalName) then

    --Check if there's one in the player's inventory. If so, remove it entirely.
    local iInRoomIndex = -1
    local bIsInventoryCase = false
    local iInInventoryIndex = fnGetIndexOfItemInInventory(sLocalName)
    if(iInInventoryIndex ~= -1) then
    
        --Wound handler.
        if(gzTextVar.gzPlayer.bIsCrippled) then
            TL_SetProperty("Append", "You are too hurt to drink that." .. gsDE)
            LM_ExecuteScript(gzTextVar.sTurnEndScript)
            return
        end
    
        --If this is an emulation check, don't actually do anything. Just indicate we responded.
        if(gzTextVar.iItemEmulation == 1) then
            gzTextVar.iItemEmulation = 2
            return
        end
        
        --Standard.
        bIsInventoryCase = true

    --If the item was not found in the inventory, check if there's one on the floor.
    else
    
        --Check if there's one on the floor. If so, remove it.
        iInRoomIndex = fnGetIndexOfItemInRoom(gzTextVar.gzPlayer.sLocation, sLocalName)
        if(iInRoomIndex ~= -1) then
    
            --Wound handler.
            if(gzTextVar.gzPlayer.bIsCrippled) then
                gbHandledInput = true
                TL_SetProperty("Append", "You are too hurt to drink that." .. gsDE)
                LM_ExecuteScript(gzTextVar.sTurnEndScript)
                return
            end
    
            --If this is an emulation check, don't actually do anything. Just indicate we responded.
            if(gzTextVar.iItemEmulation == 1) then
                gzTextVar.iItemEmulation = 2
                return
            end
            
            --Normal.
        
        --Item was not found.
        else
    
            --Wound handler.
            if(gzTextVar.gzPlayer.bIsCrippled) then
                gbHandledInput = true
                TL_SetProperty("Append", "You can't drink something you don't have, and are far too injured to do so anyway." .. gsDE)
                LM_ExecuteScript(gzTextVar.sTurnEndScript)
                return
            end
    
            --If this is an emulation check, don't actually do anything. Just indicate we responded.
            if(gzTextVar.iItemEmulation == 1) then
                gzTextVar.iItemEmulation = 2
                return
            end
            
            --Normal.
            TL_SetProperty("Append", "You can't drink something you don't have." .. gsDE)
            gbHandledInput = true
            return
        end
    end
        
    --Check if the player has less than max HP.
    if(gzTextVar.gzPlayer.iHP >= gzTextVar.gzPlayer.iHPMax) then
        TL_SetProperty("Append", "You are at maximum health already." .. gsDE)
        gbHandledInput = true
    
    --Drink the potion.
    else
    
        --If this is an emulation check, don't actually do anything. Just indicate we responded.
        if(gzTextVar.iItemEmulation == 1) then
            gzTextVar.iItemEmulation = 2
            return
        end
    
        --Remove.
        if(bIsInventoryCase) then
            fnRemoveItemFromInventory(iInInventoryIndex)
        else
            fnRemoveItemFromRoom(iRoomSlot, iInRoomIndex)
        end
    
        --Restore.
        gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
        TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
    
        --Display.
        TL_SetProperty("Append", "You drink the potion and immediately feel much better. All HP restored!" .. gsDE)
        gbHandledInput = true
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
end