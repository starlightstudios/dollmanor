--[ ====================================== Blue Storybook ======================================= ]
--A book the player can find. Gives hints on what to do. This one gives worldbuilding.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 2
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end


--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ======================================= Trap Builder ======================================== ]
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "read"}
    return
end

--[ ========================================== Priority ========================================= ]
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iBooks + 0
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[Synonym Handler]
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"blue", "storybook", "book"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "A storybook titled 'The Tragic Tale of Max Korber', about the titular character finding a strange mansion in the mountains. The inside cover says Korber is famous, but you've never head of him. No author is listed." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "A storybook titled 'The Tragic Tale of Max Korber', about the titular character finding a strange mansion in the mountains. The inside cover says Korber is famous, but you've never head of him. No author is listed." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    LM_ExecuteScript(gzTextVar.sStandardTake, sLocalName, iItemSlot, iRoomSlot)

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)

--Read Handler. This reads the table of contents.
elseif(sInputString == "read " .. sLocalName) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Text.
    TL_SetProperty("Append", "'The Tragic Tale of Max Korber'. This story has three chapters. Read a chapter with [read blue storybook 1, 2 or 3]" .. gsDE)
    gbHandledInput = true

--Read Handler. First chapter.
elseif(sInputString == "read " .. sLocalName .. " 1" or sInputString == "read " .. sLocalName .. " page 1") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "This is the story of Max Korber. Max was a mountain climber, the finest in the land. There was no peak he could not brave. He climbed the tallest mountain and the smallest mountain. The steepest cliff posed no challenge to him. Max loved to climb." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "One day, Max was hiking in the mountains of Scotland. There were many tough cliffs to conquer and Max defeated them one by one. His planned route contained no less than seven, but Max was up to the challenge." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "When Max climbed the fifth cliff, he saw something odd in the distance. A lonely manor estate, tucked into the hills. Surrounded by boulders and steep cliffs, it seemed impossible. How could a building be built in such mountainous terrain? Who would live there? Max needed to know. So he made his way to the manor." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "When Max stood before it, he felt a chill blow through his jacket. Max was tough, prepared for the cold, and accustomed to hardship, but there was something about this place which scared even him." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Second chapter.
elseif(sInputString == "read " .. sLocalName .. " 2" or sInputString == "read " .. sLocalName .. " page 2") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Max knocked on the door and, to his surprise, was almost immediately answered. A lady stood before him, dressed in a smock stained with paints. She ushered him in and called for her servants to prepare for their guest." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Max was taken aback but could not turn down their hospitality. The servants took his coat and offered to prepare a bath. They were all very polite but somewhat odd. Max thought that they seemed oddly stiff and cold, but he was sure his eyes were playing tricks on him. The daylight was fading and the shadows grew long. He was tired." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "He ate well that night and slept soundly in the room made for him. When he awoke, he looked around in shock. The manor lay in total disrepair. The walls had holes in them, cobwebs infested every corner, and there was no sign of the polite people who had taken care of him the night before." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Puzzled, Max searched the manor. He searched high and low, but found nary a clue. The place was abandoned. He fetched his coat, still near the entrance hall and immaculate compared to the dismal building. As he prepared to leave, he stepped outside and looked down." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Clouds. Clouds as far down as he could see. There was no ground beneath him, none beneath the house. It was as though he was on a rock floating in the sky." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Third chapter.
elseif(sInputString == "read " .. sLocalName .. " 3" or sInputString == "read " .. sLocalName .. " page 3") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Max stumbled backwards into the house again. There was some mistake. This was some dream. As he did, he bumped into something firm. It clutched him in its cold, plastic hands." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Max tried to scream as he looked into the eyes of the gothic doll girl who held him. Nothing came out. His mouth had turned to plastic. Soon the rest of him followed, and Max slumped listlessly in the hands of the doll girl." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The creator took the blank doll and dressed it. She made it into a pretty princess. The princess loved adventure. The princess was a good, obedient little dolly. And the princess wrote down her silly dream about the man named Max Korber, just in case her creator wanted to read it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Max Korber was never seen again. The end." .. gsDE)
    TL_SetProperty("Create Blocker")
    
end