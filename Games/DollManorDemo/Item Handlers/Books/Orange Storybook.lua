--[ ===================================== Orange Storybook ====================================== ]
--A book the player can find. Gives hints on what to do. This one gives worldbuilding and hints on
-- how to get past the petrification trap.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 2
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ========================================== Priority ========================================= ]
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iBooks + 1
    return
end

--[ ======================================= Trap Builder ======================================== ]
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "read"}
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[Synonym Handler]
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"orange", "storybook", "book"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "'Aleksanteri and the Trial of the Gods'. It seems to be a translation of a much older work. There are notes scribbled in the margins. The inside cover indicates the story was written in ancient Greek text and translated by a professor at Oxford. The author's comments are written in [blocks like this]." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "'Aleksanteri and the Trial of the Gods'. It seems to be a translation of a much older work. There are notes scribbled in the margins. The inside cover indicates the story was written in ancient Greek text and translated by a professor at Oxford. The author's comments are written in [blocks like this]." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    LM_ExecuteScript(gzTextVar.sStandardTake, sLocalName, iItemSlot, iRoomSlot)

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)

--Read Handler. This reads the table of contents.
elseif(sInputString == "read " .. sLocalName) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Text.
    TL_SetProperty("Append", "'Aleksanteri and the Trial of the Gods'. This story has three chapters. Read a chapter with [read orange storybook 1, 2, or 3]" .. gsDE)
    gbHandledInput = true

--Read Handler. First chapter.
elseif(sInputString == "read " .. sLocalName .. " 1" or sInputString == "read " .. sLocalName .. " page 1") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    gzTextVar.bHasReadOrangeBook = true
    
    --Text.
    TL_SetProperty("Append", "Aleksanteri bid farewell to the ship captain. He stood on the shore as it sailed into the distance. It passed below the horizon, and was gone. Aleksanteri stood alone on the shoreline, listening to the ocean. He was waiting." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "He did not wait long. A turtle swam ashore and approached him. It spoke to him in a familiar, deep voice. 'Mortal, I am Poseidon,' it said." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "[This is the only surviving document of the epic of Aleksanteri. While the other stories are mentioned in other works, all of these stories except a few fragments have been lost.]" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Poseidon told Aleksanteri that he must use the Crystal of Darkness. Poseidon was very clear that Aleksanteri would not be able to overcome the sorceress by himself. He must put faith in the gods, and pay them homage. Aleksanteri told the turtle he would prepare the sacrifices." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Second chapter.
elseif(sInputString == "read " .. sLocalName .. " 2" or sInputString == "read " .. sLocalName .. " page 2") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    gzTextVar.bHasReadOrangeBook = true
    
    --Text.
    TL_SetProperty("Append", "[A segment is missing in the original script. Considering later events, it is presumed that Aleksanteri was able to perform the needed sacrifices and journeyed to the sorceress' abode.]" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The great gate of the temple swung open before him, and Aleksanteri entered. The sorceress stood waiting, with a great feast prepared. She beckoned him over and asked him to dine with her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Aleksanteri agreed, but as per the warnings of Athena, he was careful not to eat the food. Instead, he would chew it and then dispose of it when the sorceress' attention was elsewhere." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Then, Aleksanteri told the sorceress he must take his rest. He went to the bedchambers prepared for him and, waiting until night fell, looked to the moon. As Aphrodite had said, the moon would glow red, then go dark as an eclipse. This was his moment. He slipped past the guards in the moonless black." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "[The guards are not mentioned anywhere else. Presumably they are in the missing section of the scripts.]" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "He found the secret passage and made his way to the basement. Here he would search for hours until he found the shrine where the sorceress drew her power. It was covered in a calming blue light, and it nearly brought Aleksanteri to sleep." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Then, the warning of Poseidon reminded him. He drew forth the Crystal of Darkness and placed it upon the ground, whereupon the blue light was removed and stored within the crystal. He entered the secret room." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Third chapter.
elseif(sInputString == "read " .. sLocalName .. " 3" or sInputString == "read " .. sLocalName .. " page 3") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    gzTextVar.bHasReadOrangeBook = true
    
    --Text.
    TL_SetProperty("Append", "There he found the [Untranslatable. Possibly 'soul' but the Greek word for soul is not the one used. The rest of the context suggests it is something like the soul but distinct from one.] of the sorceress. He knew now that he would be able to defeat her with her power in his possession." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "As he left the chamber, a statue blocked his way. She was so finely carved as to be indistinguishable from a human, save for the stone skin. Aleksanteri fell in love with the statue, so finely crafted and beautiful was she." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "His passionate gaze awoke the statue. She saw what he had done to her beloved light, and bade him to shatter the crystal. She explained that the crystal would forever prevent them from being lovers." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Again the warnings of Poseidon reached him, but he was so taken with the statue girl that he took the crystal and broke it. The blue light flooded the room, and he was turned to stone for defying the gods." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "[The narrative states that the statue and Aleksanteri then hid the four pieces of the Crystal of Darkness, two apiece, though it is not clear if the statue is metaphorically alive or is literally alive. It is also unclear how the story's author came to know the details if the hero of the story became stone, and how they were able to move despite being petrified.]" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "[The story seems like the classic Greek story of lust but is quite different from the usual pattern. Aleksanteri heeds the warnings of the gods but then fails to do so later in the same story, so the moral implication suggests it is lust that motivates him, as opposed to hubris or vanity like similar stories.]" .. gsDE)
    TL_SetProperty("Create Blocker")

end
