--[ ====================================== White Storybook ======================================= ]
--A book the player can find. Gives hints on what to do. This one gives worldbuilding and hints on
-- how to defeat the Resinous Titan dolls.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 2
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ========================================== Priority ========================================= ]
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iBooks + 3
    return
end

--[ ======================================= Trap Builder ======================================== ]
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "read"}
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[Synonym Handler]
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"white", "storybook", "book"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "'The Warrior Mia Himura and the Golden Titans'. Apparently this story was hand-written in a blank book. No author is listed." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "'The Warrior Mia Himura and the Golden Titans'. Apparently this story was hand-written in a blank book. No author is listed." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    LM_ExecuteScript(gzTextVar.sStandardTake, sLocalName, iItemSlot, iRoomSlot)

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)

--Read Handler. This reads the table of contents.
elseif(sInputString == "read " .. sLocalName) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Text.
    TL_SetProperty("Append", "'The Warrior Mia Himura and the Golden Titans'. This story has five chapters. Read a chapter with [read white storybook 1, 2, 3, 4, 5, or 6]" .. gsDE)
    
    gbHandledInput = true

--Read Handler. First chapter.
elseif(sInputString == "read " .. sLocalName .. " 1" or sInputString == "read " .. sLocalName .. " page 1") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Mia travelled for most of her life. She was second to none with a sword and sold her services to any daimyo with the coin to buy it. She never stayed in the same place for long, always seeking the next big adventure and the next big payout." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "One day, Mia found the manor of a secretive samurai. She had heard the samurai had powers unheard of elsewhere, and Mia wanted to see for herself if they were real." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She entered the manor after receiving no answer to her knocks. She dared not cause offense, should the rumours about the samurai be true. She was quiet and careful." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She wandered the house. The floor did not creak, the wind did not blow, and the birds did not sing. The manor was quiet. Perhaps no one was here?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Then, she heard it." .. gsDE)
    TL_SetProperty("Create Blocker")

    
--Read Handler. Second chapter.
elseif(sInputString == "read " .. sLocalName .. " 2" or sInputString == "read " .. sLocalName .. " page 2") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Far away, someone was giggling. Mia strained her ears to catch the faint laughter. Something about it unnerved her. She found a side room and ducked inside, concealing herself behind a bookcase." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Text.
    TL_SetProperty("Append", "The giggle repeated, louder and closer. Mia unsheathed her sword, ready. Three attackers appeared in the dim outline of the manor's door. They lunged towards her, and met her steel." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "They cut apart easily, heads falling to the side. The giggling continued from their severed heads, and Mia understood the samurai's awful magics. Two more of the giggling attackers appeared, and came for her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She sliced them apart the same as the others. The giggling from the fallen heads was beginning to unnerve her..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
--Read Handler. Third chapter.
elseif(sInputString == "read " .. sLocalName .. " 3" or sInputString == "read " .. sLocalName .. " page 3") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "The giggling receded into the distance as Mia pressed on. She could hear the clacking of the joints as they searched for her. She found a place to hide, concealing herself behind a bookcase." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Text.
    TL_SetProperty("Append", "Then, silence broke. Mia listened, straining. Something bigger, heavier, was in the manor. There was only one giggling. 'Where are you?' a voice asked, from somewhere far away." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Mia held her breath and kept her eyes focused on the entrance to the room. A giggle pierced the silence, closer. 'Where are you?' the voice repeated. The heavy footsteps drew nearer." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Silence. Mia waited, staring, straining. Why had the footsteps gone still? Her mind raced. What sort of magic made a person's skin hard and joints rotate?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Mia's eyes widened as she realized there was a face concealed in the darkness. How long it it been looking at her?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Found you.'" .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Fourth chapter.
elseif(sInputString == "read " .. sLocalName .. " 4" or sInputString == "read " .. sLocalName .. " page 4") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Mia sprung from her hiding place, her sword ready. The creature showed itself, blocking the doorway and grinning at her. It gleamed in the dark, glowing yellow against the black. It looked like the others, but its skin was translucent, like the sap of a tree. Mia leapt forth and struck!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her blade carved through the girl's neck. In one end, and out the other. The resin reformed as her blade left it. The girl smiled at her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Mia struck again and again, slashing through the girl. Each blow sailed through her body. Was she harming it? The girl continued to smile at her. Then, it struck. It grabbed her by the throat. Mia coughed and struggled. It was fast, very fast, faster than her, and far faster than the others." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Got you'" .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Fifth chapter.
elseif(sInputString == "read " .. sLocalName .. " 5" or sInputString == "read " .. sLocalName .. " page 5") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "The girl held her by the throat. It lifted her off the ground. Mia dropped the sword. She struggled to breathe, her feet kicking at the empty air. The girl continued to smile. And Mia felt its grip tighten." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The girl's grip tightened more and more, squeezing her. Mia struggled, and felt the girl's hand spread. It seemed to grow larger. It continued to squeeze, but it also seemed to be covering more and more of her. It flowed down over her body and up her neck." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It reached her face and flowed into her mouth. It spread up to her eyes, and she could no longer see anything but golden sap. She choked and fought, but her strength was fading." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She tried one last time to break the grip of the girl, but her hands were limp. She could not move. Her hands were covered with the yellow sap, and she lost consciousness." .. gsDE)
    TL_SetProperty("Create Blocker")

--Read Handler. Sixth chapter.
elseif(sInputString == "read " .. sLocalName .. " 6" or sInputString == "read " .. sLocalName .. " page 6") then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Mia awoke. She was in a different room than where she had lost consciousness, but she was alive. She groped in the dim light for her sword, but found nothing. She felt odd, stiff. Was it a dream?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The dim light shattered at the opening of a distant door. Mia turned to see it, to see the outline of a tall thin woman in it. A corona of brilliant light surrounded her. The woman walked towards her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Stand.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Mia stood up. The light reflect on her skin. Her skin was like amber, a golden resin like the girl's that had grasped her. She tried to open her mouth to scream, but nothing came out." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Come to me.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her legs sprung to life. She moved towards the woman. She fought herself but the command was stronger than her will." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Kneel.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She kneeled before the woman. This magician must be the cause. If she could kill the sorceress she could break whatever curse held her. She would need to bide her time, and choose her moment." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Obey.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her mind began to fade. Thoughts of rebellion left her. This woman was her creator. She would obey her for the rest of time." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her creator took her nude body and dressed it in finery. She would be a powerful warrior in service of her creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "As her mind began to wane, her creator gave her one task before it was no more. She would write about the dream she had. The dream of the legendary warrior, Mia Himura." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The End" .. gsDE)
    TL_SetProperty("Create Blocker")
    
end