--[ ======================================= Red Storybook ======================================= ]
--A book the player can find. Gives hints on what to do. This one gives worldbuilding and hints on
-- how to detect claygirls. Having this in the inventory allows the "check closely" command on items.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 1
    gzTextVar.iGlobalIndicatorY = 2
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "drop",  "drop "  .. sEntityName)
    
    --On the ground:
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "read",  "read "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ========================================== Priority ========================================= ]
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = gzTextVar.iaItemPriorities.iBooks + 2
    return
end

--[ ======================================= Trap Builder ======================================== ]
--Builds a list of words that will trigger a claygirl trap, if this item is trapped. But this would
-- never be trapped because that'd be cruel.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take", "read"}
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[Synonym Handler]
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"red", "storybook", "book"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "'Dahlia and the Clay Soldier'. A story in 1940's typewriter font, the pages look like weathered vellum. No author is listed." .. gsDE)
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    TL_SetProperty("Append", "'Dahlia and the Clay Soldier'. A story in 1940's typewriter font, the pages look like weathered vellum. No author is listed." .. gsDE)
    gbHandledInput = true

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    LM_ExecuteScript(gzTextVar.sStandardTake, sLocalName, iItemSlot, iRoomSlot)

--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to drop that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end

    --Call the subroutine.
    fnDropItem(iItemSlot)

--Read Handler. This reads the table of contents.
elseif(sInputString == "read " .. sLocalName) then
    
    --Set this flag to true. Player does not need to keep the book. Reading any one page allows spotting claygirls.
    gzTextVar.bCanSpotClaygirls = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Text.
    TL_SetProperty("Append", "'Dahlia and the Clay Soldier'. This story has five chapters. Read a chapter with [read red storybook 1, 2, 3, 4, or 5]" .. gsDE)
    gbHandledInput = true
    
    if(bPrev == false and gzTextVar.bCanSpotClaygirls == true) then
        TL_SetProperty("Append", "(Looking over the book has unlocked the [search carefully] command.)" .. gsDE)
        TL_SetProperty("Create Blocker")
    end

--Read Handler. First chapter.
elseif(sInputString == "read " .. sLocalName .. " 1" or sInputString == "read " .. sLocalName .. " page 1") then
    
    --Set this flag to true. Player does not need to keep the book. Reading any one page allows spotting claygirls.
    local bPrev = gzTextVar.bCanSpotClaygirls
    gzTextVar.bCanSpotClaygirls = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Dahlia was a dancer. She was in the ballet, and travelled all across the world. Dahlia danced for crowds from Europe to Asia to North America. When the war came, Dahlia danced for the boys on the front, free of charge." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The boys gawked and hollered. Dahlia took it as a compliment, considering the nature of the war. These silly boys might not make it back, they had to be up front. One of them asked her to go on a date with him, before he was put back on the line. Dahlia, flying back to London the next morning with the ballet, had nothing else to do. She agreed." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The boy couldn't have been a day over eighteen, while Dahlia was in her twenties. He was a polite, quiet boy. He had a southern accent but spoke very eloquently. He told her he had grown up in the hills but his mother insisted he go to the big city for school. Dahlia was enraptured." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The night came to an end, and Dahlia had to get back to the house she had been put up in. The boy offered to walk her home. Arm in arm, the two went into the street." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "As the sun set, the lights went out. Nobody wanted to give a target for the bombers. The two stuck to the streets and used what little moonlight they could. But, eventually, they got lost." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    if(bPrev == false and gzTextVar.bCanSpotClaygirls == true) then
        TL_SetProperty("Append", "(Reading this has unlocked the [search carefully] command.)" .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
--Read Handler. Second chapter.
elseif(sInputString == "read " .. sLocalName .. " 2" or sInputString == "read " .. sLocalName .. " page 2") then
    
    --Set this flag to true. Player does not need to keep the book. Reading any one page allows spotting claygirls.
    gzTextVar.bCanSpotClaygirls = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "After much searching on empty roads, they found their way to the house. Standing alone on a hill at the end of a long cobbled road was the house Dahlia had been staying in." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiled at the boy and asked if he wanted to come in. He sputtered, said he didn't have leave to stay overnight. She put her hand daintily on his chest and he agreed to come in anyway." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Inside, the house was quiet. Dahlia was confused. The layout was different. None of her things were there, and the French family who lived there were absent. She called out to them, but heard no reply. The boy was eager and Dahlia decided that she had more important things to do. They found the first bedroom they could." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She lit a candle, careful to keep it from the windows, and told the boy that she needed to get ready. He waited eagerly on the bed, still in his uniform. She reminded him to get ready too, and he laughed before he took his clothes off. Dahlia went into the adjoining room." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She heard a cry, a shout of excitement. She assumed the boy was preparing in his own way. She got ready for the moment and opened the door, prepared to see the boy. She did not see what she expected." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    if(bPrev == false and gzTextVar.bCanSpotClaygirls == true) then
        TL_SetProperty("Append", "(Reading this has unlocked the [search carefully] command.)" .. gsDE)
        TL_SetProperty("Create Blocker")
    end

--Read Handler. Third chapter.
elseif(sInputString == "read " .. sLocalName .. " 3" or sInputString == "read " .. sLocalName .. " page 3") then
    
    --Set this flag to true. Player does not need to keep the book. Reading any one page allows spotting claygirls.
    gzTextVar.bCanSpotClaygirls = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "The boy had his hands wrapped around a small bottle that had been on the nightstand. In it, there was bubbling red liquid. He was holding it with a pained expression on his face, barely visible in the dim light of the candle. Dahlia expected him to be nervous. Perhaps he had found some wine." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She drew nearer to see that his arms had been covered in something. That something was not the bedsheets as she had expected, but a curious, thick substance, like clay. It seemed to be spreading down his body, covering it. Dahlia reached for it but the boy slapped her hand away. She brought the candle over to see his face, only to see that his mouth had been covered by the clay." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The clay seemed to withdraw as she held the candle close. In shock, she drew backwards, and the clay overtook the boy's mouth again. He seemed to be screaming beneath it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Terrified, Dahlia stumbled backwards. As she did, the clay wrapped the boy up. It engulfed him wholly. Dahlia tripped and fell, hurting her ankle as she did so. She struggled to stand, and watched, horrified, as the boy writhed and twisted. Then, he stopped fighting. His shape changed. He became curvy, feminine. His chest expanded to produce two pert breasts and a narrow waist." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "His face vanished entirely. It was smooth and flat. His hair had vanished into the clay mass and became longer, more feminine. Now reformed, the clay girl stood and glared at her with eyeless rage." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    if(bPrev == false and gzTextVar.bCanSpotClaygirls == true) then
        TL_SetProperty("Append", "(Reading this has unlocked the [search carefully] command.)" .. gsDE)
        TL_SetProperty("Create Blocker")
    end

--Read Handler. Fourth chapter.
elseif(sInputString == "read " .. sLocalName .. " 4" or sInputString == "read " .. sLocalName .. " page 4") then
    
    --Set this flag to true. Player does not need to keep the book. Reading any one page allows spotting claygirls.
    gzTextVar.bCanSpotClaygirls = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "Dahlia limped from the room. The clay girl followed her on legs made of a great mass of flowing clay. The clay girl was not fast, but on her injured ankle Dahlia could not run. She struggled and cursed. And then she saw them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There were more. Many more. At the end of the hallway stood another clay girl, and behind her, two more. She was surrounded, and the clay girls were close. Dahlia ducked into a side room and slammed the door shut. The clay girls reached the door and merely stood in front of it, their shadows cast by their candles faintly visible in the cracks beneath the door." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Dahlia's chest had tightened. She had almost fainted but a surge of adrenaline kept her upright. She limped to the window, scratching at it and trying to open it. She could not grip the edges. It was then she realized that the window had been sealed to its frame. It could not be opened." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Dahlia looked for something to throw through the window, something heavy she could use to break it. She found a discarded fire poker and prepared to throw it when she felt a tickling sensation. She screamed. The fire poker was covered in clay!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    if(bPrev == false and gzTextVar.bCanSpotClaygirls == true) then
        TL_SetProperty("Append", "(Reading this has unlocked the [search carefully] command.)" .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
--Read Handler. Fifth chapter.
elseif(sInputString == "read " .. sLocalName .. " 5" or sInputString == "read " .. sLocalName .. " page 5") then
    
    --Set this flag to true. Player does not need to keep the book. Reading any one page allows spotting claygirls.
    gzTextVar.bCanSpotClaygirls = true
    
    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to read that." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --No-read check.
    if(fnNoReadCheck(true) == true) then return end
    
    --Flag.
    gbHandledInput = true
    
    --Text.
    TL_SetProperty("Append", "The clay snaked over her body. She dropped the poker, only for it to splatter on the floor. It had looked solid, but in fact it had been made of clay. As it hit the floor, it spread and grew in size. Then, a head emerged from the mass, and a body formed after it. Somehow, the clay girl had disguised herself as the fire poker." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Dahlia recoiled in horror as the girl slid towards her. The tickling moved up her arm. She would soon be covered in clay. The faceless clay girl stood over her now, and drops of clay fell from her onto Dahlia. Dahlia tried to scream again, but nothing came out." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Soon, she was engulfed in the clay. It covered her in soft, cool, tickling goo. It felt even slightly pleasurable, except she could not breathe. She feared she would faint, but then she began to breathe easily, somehow." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She felt her sight return. No longer was she covered in the clay - she was the clay! She quickly felt at her face only to feel the smooth unbroken clay. She had no mouth, no eyes, but still she could somehow see." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She grasped at the clay, but could not pull it off. She pushed and tugged. It was then that she realized there was no Dahlia beneath the clay. " .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The clay was Dahlia. Dahlia was the clay." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Dahlia stood on her formless legs. A thought entered her head. She needed to write this down, write down what had happened to her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She opened the door. Her fellow clay girls parted and let her past. There was a typewriter near the entrance. She put a piece of paper in and began writing." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Then, when the whole story was on the page, Dahlia slid backwards and looked at her work. She had done what she had to do. Dahlia would have smiled, but she had no face. She was not a person. She was clay. Forever." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Dahlia went into the manor. She did not know why she did what she did anymore. She simply did. She found a place. She found an object. She became that object, and lay in wait." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The End." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    if(bPrev == false and gzTextVar.bCanSpotClaygirls == true) then
        TL_SetProperty("Append", "(Reading this has unlocked the [search carefully] command.)" .. gsDE)
        TL_SetProperty("Create Blocker")
    end
    
end
