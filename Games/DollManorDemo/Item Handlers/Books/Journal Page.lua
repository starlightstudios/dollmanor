--[ ======================================= Journal Page ======================================== ]
--A page from a journal. Exists only as an entity in the world, gets folded into the journal when
-- picked up. It therefore cannot be dropped.

--[ ========================================= Indicator ========================================= ]
--Building indicator info. Item returns its icon position into the global variable positions.
if(gzTextVar.bIsBuildingIndicatorInfo == true) then
    gzTextVar.iGlobalIndicatorX = 0
    gzTextVar.iGlobalIndicatorY = 2
    return
end

--[ ========================================= Stacking ========================================= ]
--Checking whether or not the given item can stack. Used only when creating the item.
if(gzTextVar.bIsCheckingStacking == true) then
    return
end

--[ ==================================== Argument Resolution ==================================== ]
--Argument Listing:
-- 0: sInputString - The input string in question.
-- 1: iItemSlot - The item slot of the item in question.
-- 2: iRoomSlot - The room slot of the item in question.

--Arg check.
local iRequiredArgs = 3
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs and TL_GetProperty("Is Building Commands") == false) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--[ ====================================== Command Builder ====================================== ]
--Used for building a list of commands when this entity is selected on the locality window.
-- This ignores script argument requirements. Also note that the arguments are INVALID at this point.
if(TL_GetProperty("Is Building Commands") == true) then
    
    --First, resolve the name of the clicked entity.
    local sEntityHeader = TL_GetProperty("Header String")
    local sEntityName = TL_GetProperty("Command String")
    
    --In the inventory:
    if(sEntityHeader == "Inventory") then
        --Item never exists in the inventory.
    
    --On the ground.
    else
        TL_SetProperty("Register Popup Command", "look",  "look "  .. sEntityName)
        TL_SetProperty("Register Popup Command", "take",  "take "  .. sEntityName)
    end
    return
end

--[ ========================================== Priority ========================================= ]
--Returns the priority of this item when queried.
if(gzTextVar.bIsPriorityCheck == true) then
    gzTextVar.iPriorityResponse = 0
    return
end

--[ ======================================= Trap Builder ======================================== ]
--Builds a list of words that will trigger a claygirl trap, if this item is trapped.
if(gzTextVar.bIsClaygirlTrapCheck == true) then
    gzTextVar.saClaygirlTrapList = {"take"}
    return
end

--[ ======================================= Verification ======================================== ]
--Arg resolve.
local sInputString = LM_GetScriptArgument(0)
local iItemSlot = tonumber(LM_GetScriptArgument(1))
local iRoomSlot = tonumber(LM_GetScriptArgument(2))

--Variables.
local sLocalName = "Null"

--Verify the item index.
if(iRoomSlot == -1) then
    
    --Range check.
    if(iItemSlot < 1 or iItemSlot > gzTextVar.gzPlayer.iItemsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.gzPlayer.zaItems[iItemSlot].sDisplayName

--Item is a room object.
else
    
    --Range check.
    if(iRoomSlot < 1 or iRoomSlot > gzTextVar.iRoomsTotal) then return end
    if(iItemSlot < 1 or iItemSlot > gzTextVar.zRoomList[iRoomSlot].iObjectsTotal) then return end
    
    --Store name.
    sLocalName = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].sDisplayName

end

--[Synonym Handler]
--Figure out if we can switch a synonym to the proper name.
local sTrueName = sLocalName
local saSynonymList = {"page"}

--First, check if the proper name already exists completely in the input string. If it does, there is no need to
-- do anything here. This prevents accidentally replacing "look fire poker" with "look fire fire poker"
local iFullnameStart, iFullnameEnd = string.find(sInputString, sTrueName)
if(iFullnameStart == nil) then
    
    --Base name was not found, so look through the synonym list.
    local iSynStart, iSynEnd
    local sUseSynonym = "Null"
    for i = 1, #saSynonymList, 1 do
        iSynStart, iSynEnd = string.find(sInputString, saSynonymList[i])
        if(iSynStart ~= nil) then
            sUseSynonym = saSynonymList[i]
            break
        end
    end

    --If a synonym was found, we can swap it out for the true name.
    if(sUseSynonym ~= "Null") then
        local sPreString = string.sub(sInputString, 1, iSynStart-1)
        local sPostString = string.sub(sInputString, iSynEnd+1)
        sInputString = sPreString .. sTrueName .. sPostString
    end
end

--[ ====================================== Basic Commands ======================================= ]
--Examining it.
if(sInputString == "look " .. sLocalName or sInputString == "examine " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Display.
    TL_SetProperty("Append", "A journal page. You should pick it up to take a closer look at it." .. gsDE)
    
    --Flag
    gbHandledInput = true

--Examining it in the inventory. One must exist in the inventory or this will fail.
elseif((sInputString == "look inv " .. sLocalName or sInputString == "examine inv " .. sLocalName) and iItemSlot ~= -1) then

    --Cannot occur, item does not exist in the inventory.

--Taking it.
elseif(sInputString == "take " .. sLocalName or sInputString == "get " .. sLocalName) then

    --Wound handler.
    if(gzTextVar.gzPlayer.bIsCrippled) then
        gbHandledInput = true
        TL_SetProperty("Append", "You are too hurt to pick that up." .. gsDE)
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
        return
    end
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --[First Page]
    --Upon finding the first journal page:
    if(gzTextVar.iTotalPagesFound == 0) then
        
        --Increment
        gzTextVar.gzPlayer.iItemsTotal = gzTextVar.gzPlayer.iItemsTotal + 1
        local p = gzTextVar.gzPlayer.iItemsTotal
    
        --Create the item.
        gzTextVar.gzPlayer.zaItems[p] = {}
        
        --Create the journal item.
        gzTextVar.gzPlayer.zaItems[p].sUniqueName = "journal"
        gzTextVar.gzPlayer.zaItems[p].sDisplayName = "journal"
        gzTextVar.gzPlayer.zaItems[p].bCanBePickedUp = true
        gzTextVar.gzPlayer.zaItems[p].sHandlerScript = gzTextVar.sRootPath .. "Item Handlers/Books/Journal.lua"
        gzTextVar.gzPlayer.zaItems[p].bIsStackable = false
        gzTextVar.gzPlayer.zaItems[p].iStackSize = 1
        gzTextVar.gzPlayer.zaItems[p].iJournalPage = -1
        gzTextVar.gzPlayer.zaItems[p].bIsStackable = 1
        gzTextVar.gzPlayer.zaItems[p].iStackSize = 1
        
        --Unlock whichever page of the journal we picked up.
        local iPage = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].iJournalPage
        if(iPage >= 1 and iPage <= ciTotalPages) then
            gzTextVar.baPagesFound[iPage] = true
        end
        
        --Display changes for the first one.
        TL_SetProperty("Append", "You pick up the journal page. You decide to keep them all in one place. The [journal] will store all the pages you find. Page " .. iPage .. " is available." .. gsDE)
        
        
    --[Successive Pages]
    else
    
        local iPage = gzTextVar.zRoomList[iRoomSlot].zObjects[iItemSlot].iJournalPage
        if(iPage >= 1 and iPage <= ciTotalPages) then
            gzTextVar.baPagesFound[iPage] = true
        end
        TL_SetProperty("Append", "You pick up the journal page. Page " .. iPage .. " is available from the [journal]." .. gsDE)
    
    end
    
    --Increment.
    gzTextVar.iTotalPagesFound = gzTextVar.iTotalPagesFound + 1
    
    --Remove the item from the original room listing.
    fnRemoveItemFromRoom(iRoomSlot, iItemSlot)
    if(gzTextVar.bTakeAll == false) then
        LM_ExecuteScript(gzTextVar.sTurnEndScript)
    end
    gbHandledInput = true
    
--Drop Handler.
elseif(sInputString == "drop " .. sLocalName and iRoomSlot == -1) then
    
    --Pages cannot be dropped.

--Read Handler. Prompts you to take the page.
elseif(sInputString == "read " .. sLocalName) then
    
    --If this is an emulation check, don't actually do anything. Just indicate we responded.
    if(gzTextVar.iItemEmulation == 1) then
        gzTextVar.iItemEmulation = 2
        return
    end
    
    --Display
    TL_SetProperty("Append", "The page looks important. You should [take] it before you read it, for safe-keeping." .. gsDE)
    
end