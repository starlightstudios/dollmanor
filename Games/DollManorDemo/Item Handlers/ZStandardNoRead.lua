--[Standard No-Read]
--When the player is a monster, other than Doll in phase 6, they cannot read anything. This gets called instead.

--Human.
if(gzTextVar.gzPlayer.sFormState == "Human") then
    TL_SetProperty("Append", "You look at the thing. It has little symbols all over it. It's really fun to look at!" .. gsDE)
    
--Statue version.
elseif(gzTextVar.gzPlayer.sFormState == "Statue") then
    TL_SetProperty("Append", "STATUES DO NOT READ. STATUES OBEY. YOU ARE A STATUE. OBEY." .. gsDE)

--Claygirl version.
elseif(gzTextVar.gzPlayer.sFormState == "Claygirl") then
    TL_SetProperty("Append", "While disguising yourself as a book might be fun later, you have more important things to do than read right now." .. gsDE)

--Rubber version.
elseif(gzTextVar.gzPlayer.sFormState == "Rubber") then
    TL_SetProperty("Append", "You currently have a very important task for your creator. Reading will have to wait." .. gsDE)

--Ice version.
elseif(gzTextVar.gzPlayer.sFormState == "Ice") then
    TL_SetProperty("Append", "You don't care about anything, much less reading things." .. gsDE)

--Glass version.
elseif(gzTextVar.gzPlayer.sFormState == "Glass") then
    TL_SetProperty("Append", "You're a little busy to be stopping and reading things." .. gsDE)

--Doll version.
else
    TL_SetProperty("Append", "Reading? How silly! You only read and write when your creator wants you to. Playing with your sisters is much more fun!" .. gsDE)

end