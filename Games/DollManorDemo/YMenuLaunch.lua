-- |[ ====================================== Menu Launcher ===================================== ]|
--Script called when this game is selected from the main menu.
local sGameName = "Doll Manor Demo"

--Make sure the entry exists.
local zGameEntry = SysPaths:fnGetGameEntry(sGameName)
if(zGameEntry == nil) then
	io.write("Unable to launch game " .. sGameName.. ", no game entry was found.\n")
	return
end

--Clear main menu.
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()

--Run launcher script.
LM_ExecuteScript(zGameEntry.sActivePath)
