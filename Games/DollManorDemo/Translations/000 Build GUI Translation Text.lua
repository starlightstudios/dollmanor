-- |[ =============================== Build GUI Translation Text =============================== ]|
--At program startup, takes all of the static strings in the program that are used for GUI and the
-- like and creates a list of them which the internal UI will use. If these are changed, the changed
-- versions appear in the program, allowing the UI to be translated.

-- |[ ====================================== C++ Constants ===================================== ]|
--The C++ program will look for specific strings in specific slots. These variables mirror that
-- internal setup, do not edit them.
local saTranslations = {}

-- |[Title Screen]|
--Main Menu
local iTitle_Main_NewGame = 0
local iTitle_Main_LoadGame = 1
local iTitle_Main_Options = 2
local iTitle_Main_Quit = 3

--New Game Menu
local iTitle_NewGame_Tutorial = 4
local iTitle_NewGame_StartGame = 5
local iTitle_NewGame_StartGameSkipIntro = 6
local iTitle_NewGame_GameDifficulty = 7
local iTitle_NewGame_CombatStyle = 8
local iTitle_NewGame_Back = 9
local iTitle_NewGame_Difficulty_Easy = 10
local iTitle_NewGame_Difficulty_Normal = 11
local iTitle_NewGame_Difficulty_Hard = 12
local iTitle_NewGame_Combat_Turn = 13
local iTitle_NewGame_Combat_Active = 14
local iTitle_NewGame_DifficultyEasy_Desc0 = 15
local iTitle_NewGame_DifficultyEasy_Desc1 = 16
local iTitle_NewGame_DifficultyEasy_Desc2 = 17
local iTitle_NewGame_DifficultyNormal_Desc0 = 18
local iTitle_NewGame_DifficultyNormal_Desc1 = 19
local iTitle_NewGame_DifficultyNormal_Desc2 = 20
local iTitle_NewGame_DifficultyHard_Desc0 = 21
local iTitle_NewGame_DifficultyHard_Desc1 = 22
local iTitle_NewGame_DifficultyHard_Desc2 = 23
local iTitle_NewGame_CombatTurn_Desc0 = 24
local iTitle_NewGame_CombatTurn_Desc1 = 25
local iTitle_NewGame_CombatTurn_Desc2 = 26
local iTitle_NewGame_CombatActive_Desc0 = 27
local iTitle_NewGame_CombatActive_Desc1 = 28
local iTitle_NewGame_CombatActive_Desc2 = 29

--Load Game Menu
local iTitle_LoadGame_ShowingFiles = 30
local iTitle_LoadGame_Next5 = 31
local iTitle_LoadGame_Prev5 = 32
local iTitle_LoadGame_Back = 33
local iTitle_LoadGame_FileI = 34
local iTitle_LoadGame_ShowNext_Desc = 35
local iTitle_LoadGame_ShowPrev_Desc = 36

--Options Menu
local iTitle_Options_MusicVolume = 37
local iTitle_Options_SoundVolume = 38
local iTitle_Options_Resolution = 39
local iTitle_Options_ToggleFullscreen = 40
local iTitle_Options_Apply = 41
local iTitle_Options_Cancel = 42
local iTitle_Options_MusicVol_Desc0 = 43
local iTitle_Options_MusicVol_Desc1 = 44
local iTitle_Options_MusicVol_Desc2 = 45
local iTitle_Options_SoundVol_Desc0 = 46
local iTitle_Options_SoundVol_Desc1 = 47
local iTitle_Options_SoundVol_Desc2 = 48
local iTitle_Options_Resolution_Desc0 = 49
local iTitle_Options_Resolution_Desc1 = 50
local iTitle_Options_Resolution_Desc2 = 51
local iTitle_Options_Fullscreen_Desc0 = 52
local iTitle_Options_Fullscreen_Desc1 = 53
local iTitle_Options_Apply_Desc0 = 54
local iTitle_Options_Cancel_Desc0 = 55

--Total
local iTranslationsTotal = 56

-- |[ =================================== String Assignment ==================================== ]|
-- |[Title Screen]|
--Main Menu
saTranslations[iTitle_Main_NewGame]  = "New Game"
saTranslations[iTitle_Main_LoadGame] = "Load Game"
saTranslations[iTitle_Main_Options]  = "Options"
saTranslations[iTitle_Main_Quit]     = "Quit"

--New Game Menu
saTranslations[iTitle_NewGame_Tutorial]               = "Tutorial"
saTranslations[iTitle_NewGame_StartGame]              = "Start Game"
saTranslations[iTitle_NewGame_StartGameSkipIntro]     = "Start Game (Skip Intro)"
saTranslations[iTitle_NewGame_GameDifficulty]         = "Game Difficulty"
saTranslations[iTitle_NewGame_CombatStyle]            = "Combat Style"
saTranslations[iTitle_NewGame_Back]                   = "Back"
saTranslations[iTitle_NewGame_Difficulty_Easy]        = "Easy"
saTranslations[iTitle_NewGame_Difficulty_Normal]      = "Normal"
saTranslations[iTitle_NewGame_Difficulty_Hard]        = "Hard"
saTranslations[iTitle_NewGame_Combat_Turn]            = "Turn Mode"
saTranslations[iTitle_NewGame_Combat_Active]          = "Active Mode"
saTranslations[iTitle_NewGame_DifficultyEasy_Desc0]   = "Easy is intended for players struggling with normal, or who are"
saTranslations[iTitle_NewGame_DifficultyEasy_Desc1]   = "playing with a trackpad or other disability."
saTranslations[iTitle_NewGame_DifficultyEasy_Desc2]   = "Click to change."
saTranslations[iTitle_NewGame_DifficultyNormal_Desc0] = "Normal is the difficulty the game was intended to be played at"
saTranslations[iTitle_NewGame_DifficultyNormal_Desc1] = "for first time players with a mouse and keyboard."
saTranslations[iTitle_NewGame_DifficultyNormal_Desc2] = "Click to change."
saTranslations[iTitle_NewGame_DifficultyHard_Desc0]   = "Hard is for players who want a challenge and have mastered the game's"
saTranslations[iTitle_NewGame_DifficultyHard_Desc1]   = "combat system and memorized its layout. Good luck."
saTranslations[iTitle_NewGame_DifficultyHard_Desc2]   = "Click to change."
saTranslations[iTitle_NewGame_CombatTurn_Desc0]       = "In turn-based mode, you have as much time as you need to consider your choices."
saTranslations[iTitle_NewGame_CombatTurn_Desc1]       = "Enemies only attack when you make a certain number of moves. Think carefully."
saTranslations[iTitle_NewGame_CombatTurn_Desc2]       = "Click to change."
saTranslations[iTitle_NewGame_CombatActive_Desc0]     = "In active mode, enemies charge attacks constantly, and you can play cards"
saTranslations[iTitle_NewGame_CombatActive_Desc1]     = "as fast as you can click them. Your cards automatically draw. Think quickly!"
saTranslations[iTitle_NewGame_CombatActive_Desc2]     = "Click to change."

--Load Game Menu
saTranslations[iTitle_LoadGame_ShowingFiles]  = "Showing files %i-%i of %i"
saTranslations[iTitle_LoadGame_Next5]         = "Next 5"
saTranslations[iTitle_LoadGame_Prev5]         = "Prev 5"
saTranslations[iTitle_LoadGame_Back]          = "Back To Title"
saTranslations[iTitle_LoadGame_FileI]         = "File %i"
saTranslations[iTitle_LoadGame_ShowNext_Desc] = "Shows the next 5 savefiles."
saTranslations[iTitle_LoadGame_ShowPrev_Desc] = "Shows the previous 5 savefiles."

--Options Menu
saTranslations[iTitle_Options_MusicVolume]      = "Music Volume"
saTranslations[iTitle_Options_SoundVolume]      = "Sound Volume"
saTranslations[iTitle_Options_Resolution]       = "Resolution"
saTranslations[iTitle_Options_ToggleFullscreen] = "Toggle Fullscreen"
saTranslations[iTitle_Options_Apply]            = "Apply"
saTranslations[iTitle_Options_Cancel]           = "Cancel"
saTranslations[iTitle_Options_MusicVol_Desc0]   = "Volume of ambient music."
saTranslations[iTitle_Options_MusicVol_Desc1]   = "Left-click to increase by 5, right-click to decrease by 5."
saTranslations[iTitle_Options_MusicVol_Desc2]   = "Hold Ctrl to increment/decrement by 1."
saTranslations[iTitle_Options_SoundVol_Desc0]   = "Volume of sound effects, including combat."
saTranslations[iTitle_Options_SoundVol_Desc1]   = "Left-click to increase by 5, right-click to decrease by 5."
saTranslations[iTitle_Options_SoundVol_Desc2]   = "Hold Ctrl to increment/decrement by 1."
saTranslations[iTitle_Options_Resolution_Desc0] = "Cycles the resolution. Resolution is changed when Apply is clicked."
saTranslations[iTitle_Options_Resolution_Desc1] = "Default resolution is 1366x768."
saTranslations[iTitle_Options_Resolution_Desc2] = "Some systems may not allow dynamic resolution changing."
saTranslations[iTitle_Options_Fullscreen_Desc0] = "Toggles fullscreen mode on or off."
saTranslations[iTitle_Options_Fullscreen_Desc1] = "This can also be done with the F11 or F12 keys."
saTranslations[iTitle_Options_Apply_Desc0]      = "Apply changes and return to the previous menu."
saTranslations[iTitle_Options_Cancel_Desc0]     = "Discard changes and return to the previous menu."

-- |[ ========================================= Upload ========================================= ]|
--Allocate space.
OM_SetTranslation("Allocate", iTranslationsTotal)

--Set.
for i = 0, #saTranslations, 1 do
    OM_SetTranslation("Set String", i, saTranslations[i])
end
