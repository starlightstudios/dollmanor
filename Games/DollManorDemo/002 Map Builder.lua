--[ ====================================== Map Construction ===================================== ]
--Setup.
local sBasePath = fnResolvePath()

--Reset rendering flags.
TL_SetProperty("Set Rendering Z Level", 0)
TL_SetProperty("Set Under Rendering Z Level", ciDoNotRenderUnder, ciDoNotRenderUnder)

--Build a list of rooms.
LM_ExecuteScript(sBasePath .. "Map Builder/000 Room Setup.lua")

--Connect the rooms together.
LM_ExecuteScript(sBasePath .. "Map Builder/010 Connectivity.lua")

--Build path lookups from each room to every other room.
LM_ExecuteScript(sBasePath .. "Map Builder/020 Pathing.lua")

--Finalize the world and build the 3D lookup array.
TL_SetProperty("Finalize World")