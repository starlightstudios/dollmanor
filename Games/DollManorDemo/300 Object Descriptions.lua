--[ ===================================== Object Descriptions =================================== ]
--Produces a description of the requested object. Note that some objects can appear in different
-- places and may have different descriptions in those places.

--Argument Listing:
-- 0: sObjectName - Name of the object in question.
-- 1: sLocation - Where the object is.

--Arg check.
local iRequiredArgs = 2
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local sLocation = LM_GetScriptArgument(1)

--[ ======================================== Room Query ========================================= ]
--Locate the room in question.
local iRoom = -1
for i = 1, gzTextVar.iRoomsTotal, 1 do
    if(gzTextVar.zRoomList[i].sName == sLocation) then
        iRoom = i
        break
    end
end
if(iRoom == -1) then return end

--Flag.
gbHandledInput = true

--[ ========================================== Listing ========================================== ]
--Run across the object listing and find the remap name.
local sRemapTo = "Null"
for i = 1, gzTextVar.zRoomList[iRoom].iObjectsTotal, 1 do
    if(gzTextVar.zRoomList[iRoom].zObjects[i].sDisplayName == sObjectName) then
        sRemapTo = gzTextVar.zRoomList[iRoom].zObjects[i].sUniqueName
        break
    end
end
if(sRemapTo == "Null") then
    TL_SetProperty("Append", "There is no object here like that. Types 'objects' to see a list of objects nearby.")
    return
end

if(sRemapTo == "mainhallstatue") then
    TL_SetProperty("Append", "A suit of armor. It is tarnished from age, yet projects a feeling of security.")
    return
elseif(sRemapTo == "mainhallbed") then
    TL_SetProperty("Append", "A bed which has been dragged here from somewhere else. It is unkempt and has not been cleaned in some time.")
    return
elseif(sRemapTo == "mainhallfood") then
    TL_SetProperty("Append", "Food on a crumbling wooden shelf. Breads, vegetables, fruits, dried meats. They do not look appetizing, but are not rotting.")
    return
end
