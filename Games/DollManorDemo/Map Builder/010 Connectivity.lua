--[ ==================================== Build Connectivity ===================================== ]
--[Connector Function]
--Connects two rooms on the room list. Uses a non-optimized search function. The connection is A to B,
-- and the reverse connection auto-applies B to A.
--This uses string searches. It may be easier to read in some cases.
fnConnectRooms = function(sRoomA, sRoomB, sDirectionCode, sDoorCode)
    
    --Argument Check
    if(sRoomA == nil) then return end
    if(sRoomB == nil) then return end
    if(sDirectionCode == nil) then return end
    
    --Locate rooms A and B.
    local iIndexA = -1
    local iIndexB = -1
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        if(gzTextVar.zRoomList[i].sName == sRoomA) then
            iIndexA = i
            if(iIndexB ~= -1) then break end
        elseif(gzTextVar.zRoomList[i].sName == sRoomB) then
            iIndexB = i
            if(iIndexA ~= -1) then break end
        end
    end
    
    --Iterate.
    for i = 1, gzTextVar.iRoomsTotal, 1 do
        
        --If this is room A:
        if(gzTextVar.zRoomList[i].sName == sRoomA) then
            if(sDirectionCode == "N") then
                gzTextVar.zRoomList[i].sMoveN = sRoomB
                gzTextVar.zRoomList[i].iMoveN = iIndexB
            elseif(sDirectionCode == "S") then
                gzTextVar.zRoomList[i].sMoveS = sRoomB
                gzTextVar.zRoomList[i].iMoveS = iIndexB
            elseif(sDirectionCode == "E") then
                gzTextVar.zRoomList[i].sMoveE = sRoomB
                gzTextVar.zRoomList[i].iMoveE = iIndexB
            elseif(sDirectionCode == "W") then
                gzTextVar.zRoomList[i].sMoveW = sRoomB
                gzTextVar.zRoomList[i].iMoveW = iIndexB
            elseif(sDirectionCode == "U") then
                gzTextVar.zRoomList[i].sMoveU = sRoomB
                gzTextVar.zRoomList[i].iMoveU = iIndexB
            elseif(sDirectionCode == "D") then
                gzTextVar.zRoomList[i].sMoveD = sRoomB
                gzTextVar.zRoomList[i].iMoveD = iIndexB
            end
        end
    
        --If this is room B:
        if(gzTextVar.zRoomList[i].sName == sRoomB) then
            if(sDirectionCode == "N") then
                gzTextVar.zRoomList[i].sMoveS = sRoomA
                gzTextVar.zRoomList[i].iMoveS = iIndexA
            elseif(sDirectionCode == "S") then
                gzTextVar.zRoomList[i].sMoveN = sRoomA
                gzTextVar.zRoomList[i].iMoveN = iIndexA
            elseif(sDirectionCode == "E") then
                gzTextVar.zRoomList[i].sMoveW = sRoomA
                gzTextVar.zRoomList[i].iMoveW = iIndexA
            elseif(sDirectionCode == "W") then
                gzTextVar.zRoomList[i].sMoveE = sRoomA
                gzTextVar.zRoomList[i].iMoveE = iIndexA
            elseif(sDirectionCode == "U") then
                gzTextVar.zRoomList[i].sMoveD = sRoomA
                gzTextVar.zRoomList[i].iMoveD = iIndexA
            elseif(sDirectionCode == "D") then
                gzTextVar.zRoomList[i].sMoveU = sRoomA
                gzTextVar.zRoomList[i].iMoveU = iIndexA
            end
        end
    end
    
end

--This function attempts to connect rooms based on their cardinal directions. Easier to use if you
-- don't mind geometry, and it maintains internal consistency.
--Door codes can be nil, which means there is no door there.
fnConnectRoomsByDirection = function(sRoomName, bNorth, bSouth, bEast, bWest, bUp, bDown, sDoorCodeN, sDoorCodeS, sDoorCodeE, sDoorCodeW)
    
    --Argument Check
    if(sRoomName == nil) then return end
    if(bNorth == nil) then return end
    if(bSouth == nil) then return end
    if(bEast == nil) then return end
    if(bWest == nil) then return end
    if(bUp == nil) then return end
    if(bDown == nil) then return end
    
    --Iterate.
    local i = -1
    for p = 1, gzTextVar.iRoomsTotal, 1 do
        
        --If this is our room, store it.
        if(gzTextVar.zRoomList[p].sName == sRoomName) then
            i = p
            break
        end
    end
    
    --Room not found.
    if(i == -1) then return end
    
    --Reset the door codes to 'nil' if they are "None".
    if(sDoorCodeN == "None") then sDoorCodeN = nil end
    if(sDoorCodeS == "None") then sDoorCodeS = nil end
    if(sDoorCodeE == "None") then sDoorCodeE = nil end
    if(sDoorCodeW == "None") then sDoorCodeW = nil end
    
    --Clear this room's connectivity variables back to strings.
    gzTextVar.zRoomList[i].sMoveN = "Null"
    gzTextVar.zRoomList[i].sMoveS = "Null"
    gzTextVar.zRoomList[i].sMoveE = "Null"
    gzTextVar.zRoomList[i].sMoveW = "Null"
    gzTextVar.zRoomList[i].sMoveU = "Null"
    gzTextVar.zRoomList[i].sMoveD = "Null"
    
    --Begin iterating looking for case matches.
    for p = 1, gzTextVar.iRoomsTotal, 1 do
        
        --North.
        if(bNorth and gzTextVar.zRoomList[p].fRoomY == gzTextVar.zRoomList[i].fRoomY - gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomX == gzTextVar.zRoomList[p].fRoomX and gzTextVar.zRoomList[i].fRoomZ == gzTextVar.zRoomList[p].fRoomZ) then
            gzTextVar.zRoomList[i].sMoveN = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveS = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveN = p
            gzTextVar.zRoomList[p].iMoveS = i
            
            if(sDoorCodeN ~= nil) then
                gzTextVar.zRoomList[i].sDoorN = sDoorCodeN
                gzTextVar.zRoomList[p].sDoorS = sDoorCodeN
            end
        end
        
        --South.
        if(bSouth and gzTextVar.zRoomList[p].fRoomY == gzTextVar.zRoomList[i].fRoomY + gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomX == gzTextVar.zRoomList[p].fRoomX and gzTextVar.zRoomList[i].fRoomZ == gzTextVar.zRoomList[p].fRoomZ) then
            gzTextVar.zRoomList[i].sMoveS = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveN = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveS = p
            gzTextVar.zRoomList[p].iMoveN = i
            
            if(sDoorCodeS ~= nil) then
                gzTextVar.zRoomList[i].sDoorS = sDoorCodeS
                gzTextVar.zRoomList[p].sDoorN = sDoorCodeS
            end
        end
        
        --West.
        if(bWest and gzTextVar.zRoomList[p].fRoomX == gzTextVar.zRoomList[i].fRoomX - gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomY == gzTextVar.zRoomList[p].fRoomY and gzTextVar.zRoomList[i].fRoomZ == gzTextVar.zRoomList[p].fRoomZ) then
            gzTextVar.zRoomList[i].sMoveW = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveE = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveW = p
            gzTextVar.zRoomList[p].iMoveE = i
            
            if(sDoorCodeW ~= nil) then
                gzTextVar.zRoomList[i].sDoorW = sDoorCodeW
                gzTextVar.zRoomList[p].sDoorE = sDoorCodeW
            end
        end
        
        --East.
        if(bEast and gzTextVar.zRoomList[p].fRoomX == gzTextVar.zRoomList[i].fRoomX + gzTextVar.fMapDistance and gzTextVar.zRoomList[i].fRoomY == gzTextVar.zRoomList[p].fRoomY and gzTextVar.zRoomList[i].fRoomZ == gzTextVar.zRoomList[p].fRoomZ) then
            gzTextVar.zRoomList[i].sMoveE = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveW = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveE = p
            gzTextVar.zRoomList[p].iMoveW = i
            
            if(sDoorCodeE ~= nil) then
                gzTextVar.zRoomList[i].sDoorE = sDoorCodeE
                gzTextVar.zRoomList[p].sDoorW = sDoorCodeE
            end
        end
        
        --Up.
        if(bUp and gzTextVar.zRoomList[p].fRoomX == gzTextVar.zRoomList[i].fRoomX and gzTextVar.zRoomList[i].fRoomY == gzTextVar.zRoomList[p].fRoomY and gzTextVar.zRoomList[i].fRoomZ == gzTextVar.zRoomList[p].fRoomZ + gzTextVar.fMapDistance) then
            gzTextVar.zRoomList[i].sMoveU = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveD = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveU = p
            gzTextVar.zRoomList[p].iMoveD = i
        end
        
        --Down.
        if(bDown and gzTextVar.zRoomList[p].fRoomX == gzTextVar.zRoomList[i].fRoomX and gzTextVar.zRoomList[i].fRoomY == gzTextVar.zRoomList[p].fRoomY and gzTextVar.zRoomList[i].fRoomZ == gzTextVar.zRoomList[p].fRoomZ - gzTextVar.fMapDistance) then
            gzTextVar.zRoomList[i].sMoveD = gzTextVar.zRoomList[p].sName
            gzTextVar.zRoomList[p].sMoveU = gzTextVar.zRoomList[i].sName
            
            gzTextVar.zRoomList[i].iMoveD = p
            gzTextVar.zRoomList[p].iMoveU = i
        end
        
    end
end

--[ ========================================= Execution ========================================= ]
--[Setup Connectivity]
--Build connectivity listing.
local iCount = 0
for i = 1, gzTextVar.iRoomsTotal, 1 do
    
    --Door variables. These should be "None" if there's no door here.
    local sDoorN = gzTextVar.zRoomList[i].sBuildDoorN
    local sDoorS = gzTextVar.zRoomList[i].sBuildDoorS
    local sDoorE = gzTextVar.zRoomList[i].sBuildDoorE
    local sDoorW = gzTextVar.zRoomList[i].sBuildDoorW
    fnConnectRoomsByDirection(gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveN, gzTextVar.zRoomList[i].sMoveS, gzTextVar.zRoomList[i].sMoveE, gzTextVar.zRoomList[i].sMoveW, gzTextVar.zRoomList[i].sMoveU, gzTextVar.zRoomList[i].sMoveD, sDoorN, sDoorS, sDoorE, sDoorW)
    
    --Interrupt.
    iCount = iCount + 1
    if(iCount == 20) then
        LI_Interrupt()
        iCount = 0
    end
end

--[Layer Builder]
--The rooms have already been built by the Tiled information. Place doors and animations.
for i = 1, gzTextVar.iRoomsTotal, 1 do
    
    --Southern Door Layer
    if(gzTextVar.zRoomList[i].sDoorS ~= nil) then
        
        --Register the door indicator.
        if(string.sub(gzTextVar.zRoomList[i].sDoorS, 1, 1) == "C") then
            TL_SetProperty("Add Room Tile Layer", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, ciTilesetDoorX, ciTilesetDoorY, ciSlotDoorSClosed, 0)
        else
            TL_SetProperty("Add Room Tile Layer", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, ciTilesetDoorX, ciTilesetDoorY, ciSlotDoorSOpen, 0)
        end
        
        --If the door is "LockKnown", then set its key indicator. Some doors can be known by default.
        if(gzTextVar.zRoomList[i].bDoorSLockKnown == true) then
        
            --Get the locking state.
            local sLockState = string.sub(gzTextVar.zRoomList[i].sDoorS, 2)
                
            --Set door indicators by tile code.
            if(sLockState == " Normal") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
            elseif(sLockState == " Heart") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_Heart_X, gzTextVar.iLock_Heart_Y)
            elseif(sLockState == " Club") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_Club_X, gzTextVar.iLock_Club_Y)
            elseif(sLockState == " Diamond") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_Diamond_X, gzTextVar.iLock_Diamond_Y)
            elseif(sLockState == " Spade") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_Spade_X, gzTextVar.iLock_Spade_Y)
            elseif(sLockState == " Small") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_Small_X, gzTextVar.iLock_Small_Y)
            else
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, true, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
            end
        end
    end
    
    --Eastern Door Layer
    if(gzTextVar.zRoomList[i].sDoorE ~= nil) then
        
        --Register the door indicator.
        if(string.sub(gzTextVar.zRoomList[i].sDoorE, 1, 1) == "C") then
            TL_SetProperty("Add Room Tile Layer", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, ciTilesetDoorX, ciTilesetDoorY, ciSlotDoorEClosed, 0)
        else
            TL_SetProperty("Add Room Tile Layer", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, ciTilesetDoorX, ciTilesetDoorY, ciSlotDoorEOpen, 0)
        end
        
        --If the door is "LockKnown", then set its key indicator. Some doors can be known by default.
        if(gzTextVar.zRoomList[i].bDoorELockKnown == true) then
        
            --Get the locking state.
            local sLockState = string.sub(gzTextVar.zRoomList[i].sDoorE, 2)
                
            --Set door indicators by tile code.
            if(sLockState == " Normal") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
            elseif(sLockState == " Heart") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_Heart_X, gzTextVar.iLock_Heart_Y)
            elseif(sLockState == " Club") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_Club_X, gzTextVar.iLock_Club_Y)
            elseif(sLockState == " Diamond") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_Diamond_X, gzTextVar.iLock_Diamond_Y)
            elseif(sLockState == " Spade") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_Spade_X, gzTextVar.iLock_Spade_Y)
            elseif(sLockState == " Small") then
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_Small_X, gzTextVar.iLock_Small_Y)
            else
                TL_SetProperty("Set Room Door Indicator", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, false, gzTextVar.iLock_None_X, gzTextVar.iLock_None_Y)
            end
        end
    end
    
    --Cross-register all animations.
    for p = 1, #gzTextVar.zRoomList[i].saAnimations, 1 do
        TL_SetProperty("Register Animation To Room", gzTextVar.zRoomList[i].fRoomX, gzTextVar.zRoomList[i].fRoomY, gzTextVar.zRoomList[i].fRoomZ, gzTextVar.zRoomList[i].saAnimations[p])
    end
    
    --Interrupt.
    iCount = iCount + 1
    if(iCount == 20) then
        LI_Interrupt()
        iCount = 0
    end
end

--[Uploader]
--After all the rooms are uploaded, upload the connections.
for i = 1, gzTextVar.iRoomsTotal, 1 do
    if(gzTextVar.zRoomList[i].sMoveN ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveN)
    end
    if(gzTextVar.zRoomList[i].sMoveS ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveS)
    end
    if(gzTextVar.zRoomList[i].sMoveE ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveE)
    end
    if(gzTextVar.zRoomList[i].sMoveW ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveW)
    end
    if(gzTextVar.zRoomList[i].sMoveU ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveU)
    end
    if(gzTextVar.zRoomList[i].sMoveD ~= "Null") then
        TL_SetProperty("Register Connection", gzTextVar.zRoomList[i].sName, gzTextVar.zRoomList[i].sMoveD)
    end
    
    --Interrupt.
    iCount = iCount + 1
    if(iCount == 20) then
        LI_Interrupt()
        iCount = 0
    end
end

--[Manual Connections]
--Connections are normally blocked by walls/floors. Connections can manually be added here.
--TL_SetProperty("Register Connection", "Vinyard North E", "Vinyard South E")