--[ ======================================= Build Pathing ======================================= ]
--Every room on the map needs to know the path to every other room. This may take a while to build.
local iCount = 0
for i = 1, gzTextVar.iRoomsTotal, 1 do
    
    --List of rooms adjacent on the pulse.
    local zaAdjacentList = {}
    
    --Iterate across all other rooms. Set them to empty strings to initialize them.
    for p = 1, gzTextVar.iRoomsTotal, 1 do
        gzTextCon.zRoomList[i].zPathListing[p] = ""
    end
    
    --The first adjacency case is this one.
    local iAdjacencyCursor = 1
    zaAdjacentList[iAdjacencyCursor] = {}
    zaAdjacentList[iAdjacencyCursor].iIndex = i
    zaAdjacentList[iAdjacencyCursor].sMoves = ""
    
    --Begin pathing pulse.
    while(iAdjacencyCursor <= #zaAdjacentList) do
        
        --Fast-access variables.
        local u = zaAdjacentList[iAdjacencyCursor].iIndex
        local sMoveString = zaAdjacentList[iAdjacencyCursor].sMoves
        local iArrayMax = #zaAdjacentList
        
        --io.write(" Operate " .. iAdjacencyCursor .. " - " .. zaAdjacentList[iAdjacencyCursor].sMoves .. "\n")
        
        --Check the north connection.
        if(gzTextVar.zRoomList[u].iMoveN ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveN
            local iMoveLen = string.len(gzTextCon.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextCon.zRoomList[i].zPathListing[q] = "S" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextCon.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
            
        end
        
        --South connection.
        if(gzTextVar.zRoomList[u].iMoveS ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveS
            local iMoveLen = string.len(gzTextCon.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextCon.zRoomList[i].zPathListing[q] = "N" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextCon.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        --West connection.
        if(gzTextVar.zRoomList[u].iMoveW ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveW
            local iMoveLen = string.len(gzTextCon.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextCon.zRoomList[i].zPathListing[q] = "E" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextCon.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        --East connection.
        if(gzTextVar.zRoomList[u].iMoveE ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveE
            local iMoveLen = string.len(gzTextCon.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextCon.zRoomList[i].zPathListing[q] = "W" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextCon.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        --Up connection.
        if(gzTextVar.zRoomList[u].iMoveU ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveU
            local iMoveLen = string.len(gzTextCon.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextCon.zRoomList[i].zPathListing[q] = "D" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextCon.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        --Down connection.
        if(gzTextVar.zRoomList[u].iMoveD ~= -1) then
            
            --Fast-access.
            local q = gzTextVar.zRoomList[u].iMoveD
            local iMoveLen = string.len(gzTextCon.zRoomList[i].zPathListing[q])
            
            --If the new path is longer than the old one, ignore it.
            if((string.len(sMoveString)+1 >= iMoveLen and iMoveLen > 0) or q == i) then
           
            --Otherwise, add it.
            else
                --Append the next movement to the path.
                gzTextCon.zRoomList[i].zPathListing[q] = "U" .. sMoveString
                
                --Create a new entry on the pulse list.
                zaAdjacentList[iArrayMax+1] = {}
                zaAdjacentList[iArrayMax+1].iIndex = q
                zaAdjacentList[iArrayMax+1].sMoves = gzTextCon.zRoomList[i].zPathListing[q]
                iArrayMax = iArrayMax + 1
            end
        end
        
        iAdjacencyCursor = iAdjacencyCursor + 1
    end
    
    --Interrupt.
    iCount = iCount + 1
    if(iCount == 20) then
        LI_Interrupt()
        iCount = 0
    end
    
    
    --Debug.
    --for p = 1, gzTextVar.iRoomsTotal, 1 do
     --   io.write("Path to main hall for " .. gzTextVar.zRoomList[p].sName .. " is: " .. gzTextCon.zRoomList[1].zPathListing[p] .. "\n")
    --end
    --break
end

