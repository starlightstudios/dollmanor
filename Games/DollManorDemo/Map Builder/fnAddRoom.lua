--[fnAddRoom]
--Adds a new room to the global list. Used in the room setup.
fnAddRoom = function(sRoomName, fRoomX, fRoomY, fRoomZ, sConnections, sOpenFlags)
    
    --If the file doesn't exist, we create it.
    if(true) then
        local fCheckFile = io.open(gzTextVar.sRoomHandlers .. sRoomName .. ".lua", "r")
        if(fCheckFile == nil) then
            file = io.open(gzTextVar.sRoomHandlers .. sRoomName .. ".lua", "w")
            if(file ~= nil) then io.close(file) end
        else
            io.close(fCheckFile)
        end
    end
    
    --Argument Check
    if(sRoomName == nil) then return end
    if(fRoomX == nil) then return end
    if(fRoomY == nil) then return end
    if(fRoomZ == nil) then return end
    if(sConnections == nil) then return end
    if(sOpenFlags == nil) then return end
    
    --[Debug Error Checker]
    --Checks if the room name is not unique. Not for release copies as it slows down building.
    if(false) then
        for i = 1, gzTextVar.iRoomsTotal, 1 do
            if(gzTextVar.zRoomList[i].sName == sRoomName) then
                io.write("Duplicate room " .. sRoomName .. " found at " .. fRoomX .. "x" .. fRoomY .. "x" .. fRoomZ .. "\n")
            end
        end
    end
    
    --Variables.
    local bConnectN = false
    local bConnectE = false
    local bConnectS = false
    local bConnectW = false
    local bConnectU = false
    local bConnectD = false
    local bOpenN = false
    local bOpenE = false
    local bOpenS = false
    local bOpenW = false
    
    --Connection Resolve
    local iLen = string.len(sConnections)
    for i = 1, iLen, 1 do
        local iLetter = string.sub(sConnections, i, i)
        if(iLetter == "N" or iLetter == "n") then
            bConnectN = true
        elseif(iLetter == "E" or iLetter == "e") then
            bConnectE = true
        elseif(iLetter == "S" or iLetter == "s") then
            bConnectS = true
        elseif(iLetter == "W" or iLetter == "w") then
            bConnectW = true
        elseif(iLetter == "U" or iLetter == "u") then
            bConnectU = true
        elseif(iLetter == "D" or iLetter == "d") then
            bConnectD = true
        end
    end
    
    --Open Resolve
    iLen = string.len(sOpenFlags)
    for i = 1, iLen, 1 do
        local iLetter = string.sub(sOpenFlags, i, i)
        if(iLetter == "N" or iLetter == "n") then
            bOpenN = true
        elseif(iLetter == "E" or iLetter == "e") then
            bOpenE = true
        elseif(iLetter == "S" or iLetter == "s") then
            bOpenS = true
        elseif(iLetter == "W" or iLetter == "w") then
            bOpenW = true
        end
    end
    
    --Increment.
    gzTextVar.iRoomsTotal = gzTextVar.iRoomsTotal + 1
    giRoomToMod = gzTextVar.iRoomsTotal
    
    --Add to the random pather.
    if(bAddToRandomPather == true) then
        gzTextVar.iRandomPathsTotal = gzTextVar.iRandomPathsTotal + 1
        gzTextVar.saValidRandomPaths[gzTextVar.iRandomPathsTotal] = sRoomName
    end
    
    --Create a version for the constants list.
    gzTextCon.zRoomList[gzTextVar.iRoomsTotal] = {}
    
    --Set.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal] = {}
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sName = sRoomName
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sDescription = "Room has no description"
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sIdentity = "No Identity"
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].fRoomX = fRoomX * gzTextVar.fMapDistance
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].fRoomY = fRoomY * gzTextVar.fMapDistance
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].fRoomZ = fRoomZ * gzTextVar.fMapDistance
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bIsUnenterable = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bUnenterableBlocksVision = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bIsExplored = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bIsVisibleNow = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bEnemiesCannotSee = false
    
    --Visibility overrides. Always/Never/Narrow/Short allow vision to bypass normal connectivity.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideN = gzTextCon.iNoVisOverride
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideE = gzTextCon.iNoVisOverride
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideS = gzTextCon.iNoVisOverride
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideW = gzTextCon.iNoVisOverride
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisDown = 0
    
    --Audio Properties. Use the no-audio pack until one gets set.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].zMusicVolumes = gzTextVar.zAudioPack_NoAudio
    
    --Stranger Grouping
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sStrangerGroup = "Null"
    
    --Store the position of the room in the 3D lookups.
    gzTextVar.zRoomLookup[fRoomX][fRoomY][fRoomZ] = gzTextVar.iRoomsTotal
    
    --Default movement connections.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveN = bConnectN
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveS = bConnectS
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveE = bConnectE
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveW = bConnectW
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveU = bConnectU
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sMoveD = bConnectD
    
    --Door connections.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sBuildDoorN = "None"
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sBuildDoorS = "None"
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sBuildDoorE = "None"
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sBuildDoorW = "None"
    
    --"Open" or "Closed". Only affects tile-based rendering. Determines if we use corners or long walls here.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenN = bOpenN
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenS = bOpenS
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenE = bOpenE
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenW = bOpenW
    
    --Integer reps. All default to -1.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveN = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveS = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveE = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveW = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveU = -1
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iMoveD = -1
    
    --Doors. Doors can be in four directions, not up and down. This gets built in the connectivity phase.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sDoorN = nil
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sDoorS = nil
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sDoorE = nil
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].sDoorW = nil
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bDoorSLockKnown = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bDoorELockKnown = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bDoorSeethroughN = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bDoorSeethroughS = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bDoorSeethroughE = false
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bDoorSeethroughW = false
    
    --Sound
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iLastHeardSound = -1
    
    --Object Listing
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iObjectsTotal = 0
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].zObjects = {}
    
    --Display Listing
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iaDisplayObjects = {}
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].saAnimations = {}
    
    --Path Listing
    gzTextCon.zRoomList[gzTextVar.iRoomsTotal].zPathListing = {}
end