--[fnAddUnenterableRoom]
--These rooms appear along the side of the map and make it look nicer visually. They have no gameplay impact.
fnAddUnenterableRoom = function(sRoomName, fRoomX, fRoomY, fRoomZ, sVisibleFrom)
    
    --Argument Check
    if(sRoomName == nil) then return end
    if(fRoomX == nil) then return end
    if(fRoomY == nil) then return end
    if(fRoomZ == nil) then return end
    if(sVisibleFrom == nil) then return end
    
    --Variables.
    local bVisibleN = false
    local bVisibleE = false
    local bVisibleS = false
    local bVisibleW = false
    
    --Connection Resolve
    local iLen = string.len(sVisibleFrom)
    for i = 1, iLen, 1 do
        local iLetter = string.sub(sVisibleFrom, i, i)
        if(iLetter == "N" or iLetter == "n") then
            bVisibleN = true
        elseif(iLetter == "E" or iLetter == "e") then
            bVisibleE = true
        elseif(iLetter == "S" or iLetter == "s") then
            bVisibleS = true
        elseif(iLetter == "W" or iLetter == "w") then
            bVisibleW = true
        end
    end
    
    --Run the basic room creator.
    fnAddRoom(sRoomName, fRoomX, fRoomY, fRoomZ, "", "")
    
    --Flags.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bIsUnenterable = true
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bUnenterableBlocksVision = true
    
    --"Open" or "Closed". Only affects tile-based rendering. Determines if we use corners or long walls here.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenN = bVisibleN
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenS = bVisibleS
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenE = bVisibleE
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].bMoveOpenW = bVisibleW
    
    --Visibility overrides. The default is for unenterable rooms to block visibility in all directions.
    -- This can be overridden by scripts or map properties.
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideN = gzTextCon.iNeverVisible
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideE = gzTextCon.iNeverVisible
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideS = gzTextCon.iNeverVisible
    gzTextVar.zRoomList[gzTextVar.iRoomsTotal].iVisOverrideW = gzTextCon.iNeverVisible
end