--[ =========================================== Help ============================================ ]
--Basically all the help handlers in one handy document.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local sString = LM_GetScriptArgument(0)

--[ ======================================== Basic Help ========================================= ]
--Basic version, lists the other commands the player can get help with.
if(sString == "help") then
    TL_SetProperty("Append", "Welcome to String Tyrant. Type commands here, or use the list on the right with your mouse." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "The 'think' command will allow you to review your current objectives in case you have forgotten them." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "You may get more information on a command with 'help [commandname]'." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To get a list of options, type 'help options'." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "To exit this game, type 'quit' or 'exit' in this console. Thank you!" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Other commands you can get help with: [help move] [help look] [help think]" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Game concepts: [help doors] [help noise]" .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Game options: [option musicvol] [option soundvol]" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gbHandledInput = true
    return
end

--[ ======================================= Command Help ======================================== ]
--Movement command and some advice on movement.
if(sString == "help move") then
    TL_SetProperty("Append", "You may move north, south, east, or west. You may also type the first letter ('n', 's') to move that direction." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "If at a stairway, you may move up or down depending on the direction of the stairs." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Each movement is one 'turn' in the game world. Enemies move after you do." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "The compass in the bottom left of the map window can be clicked to quickly move. You may also use the move commands in the bottom right window." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "If you do not want to move, use the [wait] command." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gbHandledInput = true
    return
end

--Look.
if(sString == "help look") then
    TL_SetProperty("Append", "The 'look' command will show you what is in the same world tile as you, including items, entities, or objects." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "When you first enter a room, you may receive a description. The 'look' command will repeat that description." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Objects you see with the 'look' command may be interactable. Check the command window in the bottom right to see if you can use any commands on them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gbHandledInput = true
    return
end

--Think.
if(sString == "help think") then
    TL_SetProperty("Append", "The 'think' command will remind you of your current objectives. It does not consume a turn to think." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Thinking may give you a hint if you are not sure where to go next." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gbHandledInput = true
    return
end

--[ ======================================== Concept Help ======================================= ]
--Help with the concept of doors.
if(sString == "help door" or sString == "help doors") then
    TL_SetProperty("Append", "Doors block your line of sight, and the line of sight of enemies. You can [open] and [close] doors." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Closing a door may prevent an enemy from seeing you. Sometimes, enemies will forget where you are if you close the door on them. Closing or opening a door consumes a turn." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "You may move through an unlocked door in one turn by simply using the move command. If you want to see what is ahead without entering, use the [open] command." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "If a door is locked, you will be informed what key can open it when you try to open it." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Some doors are opened by a small key. These keys are broken when used, but the door will remain open." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Doors that are locked for you are not locked for enemies. Be wary around doors you cannot open." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gbHandledInput = true
    return
end

--Help with the concept of sound.
if(sString == "help noise" or sString == "help sound") then
    TL_SetProperty("Append", "You can hear other entities moving around in the spaces you cannot see. Your hearing is limited to two squares, while your sight is limited to three." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Entities you can hear are represented by a monster on the map. You cannot tell what the entity is until you see it." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Sometimes, entities may stop moving. You can only hear an entity after it has moved. Just because you can't hear anything doesn't mean the room is safe!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    gbHandledInput = true
    return
end


--[ ======================================= Options Help ======================================== ]
if(sString == "help options") then
    TL_SetProperty("Append", "You may change options with the format 'option [name] [value]'. Examples are provided here." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Music volume may be changed with 'option musicvol [level]', where [level] is a number from 0 to 100. 100 is maximum volume." .. gsDE)
    TL_SetProperty("Create Blocker")
    TL_SetProperty("Append", "Sound volume may be changed with 'option soundvol [level]', where [level] is a number from 0 to 100. 100 is maximum volume." .. gsDE)
    TL_SetProperty("Create Blocker")

    gbHandledInput = true
    return
end
