--[ ============================== Doll Stationary AI with Combat =============================== ]
--Stationary doll AI. Doesn't move or chase the player, but attacks them if they enter the same tile.

--[ ====================================== Hostility Checker ==================================== ]
--AI is checking whether it is hostile to the player.
fnDollStationaryCombatB_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = true
end

--[ ===================================== Post-Turn Spotter ===================================== ]
--Do nothing during the post-turn.
fnDollStationaryCombatB_PostTurnSpotCheck = function(i)
end

--[ ========================================= Normal AI ========================================= ]
--Main AI sequence.
fnDollStationaryCombatB_AI = function(i)
    
    --In the same room.
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        
        --Paths.
        local sVictory = gzTextVar.sRootPath .. "Combat Handlers/Tutorial Second Victory.lua"
        local sDefeat  = gzTextVar.sRootPath .. "Combat Handlers/Tutorial Second Defeat.lua"
                        
        --Dialogue.
        if(gzTextVar.bCombatActivatedThisTick == false) then
            gzTextVar.bCombatActivatedThisTick = true
            TL_SetProperty("Append", "The doll smiles at you. Her grin is icy, and she bears down on you. Prepare for a fight!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
            TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
        
        --Combat is active. Run the handler but not in queue.
        elseif(gzTextVar.bCombatActivatedThisTick == true) then
            TL_SetProperty("Exec Script", gzTextVar.sCombatHandler, 3, i, sVictory, sDefeat)
        end
        return
    end

    --Otherwise, the doll does nothing. This is a tutorial AI.
end