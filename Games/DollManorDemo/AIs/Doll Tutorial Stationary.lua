--[ ==================================== Doll Stationary AI ===================================== ]
--Stationary doll AI. Doesn't move or chase the player, and moves them back if they enter the same tile.

--[ ====================================== Hostility Checker ==================================== ]
--AI is checking whether it is hostile to the player.
fnDollStationary_HostilityCheck = function(i)
    gzTextVar.bLastHostilityCheck = true
end


--[ ===================================== Post-Turn Spotter ===================================== ]
--Do nothing during the post-turn.
fnDollStationary_PostTurnSpotCheck = function(i)
end

--[ ========================================= Normal AI ========================================= ]
--Main AI sequence.
fnDollStationary_AI = function(i)
    
    --In the same room.
    if(gzTextVar.zEntities[i].sLocation == gzTextVar.gzPlayer.sLocation) then
        
        --Dialogue.
        TL_SetProperty("Append", "Go around the enemy, using the north and south passages. Normally, walking onto the same tile as an enemy triggers a battle." .. gsDE)
        TL_SetProperty("Create Blocker")
        TL_SetProperty("Append", "For now, we'll just move you back." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        --Setup.
        local sDestination = "Evade Hall W"
        local fOldX, fOldY, fOldZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
        local fX, fY, fZ = fnGetRoomPosition(sDestination)
        
        --Unmark the previous location, and mark this one as visible.
        fnUnmarkMinimapForPosition(gzTextVar.gzPlayer.sLocation)
        fnMarkMinimapForPosition(sDestination)

        --Move the character.
        gzTextVar.gzPlayer.sPrevLocation = gzTextVar.gzPlayer.sLocation
        gzTextVar.gzPlayer.sLocation = sDestination
        TL_SetProperty("Set Map Focus", -fX, -fY)

        --Change the indicator's position.
        TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, "Player", fX, fY, fZ)
        TL_SetProperty("Player World Position", fX, fY, fZ)

        --SFX.
        fnRandomFootstep()

        return
    end
    
    --Otherwise, the AI does nothing.
end
