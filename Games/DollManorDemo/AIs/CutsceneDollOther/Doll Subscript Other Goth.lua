--[ =================================== Doll Subscript: Goth =================================== ]
--Contains the call strings used for the Goth transformation. Used when someone other than the player
-- is being transformed. Dialogue only displays if the player is watching.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--Other variables.
local sName = gzTextVar.zEntities[i].sDisplayName
local sUseSkin = "Root/Images/DollManor/Transformation/GothTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

--[Sequence]
--Blank Doll: Begin transformation:
if(gzTextVar.zEntities[i].sState == "Blank Doll") then
    
    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF0"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/BlankDoll"
    
    --Sequence
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Well done, my sweet. You've brought a playmate, exactly as I intended.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "You feel your chest swell with pride. Your creator paid you a compliment! You want to cry with joy, but will have to settle for watching a new sister be born." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", 0)
    TL_SetProperty("Append", "'As for you, hmm, let's see what we can do...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I'm in a melancholy mood today, my dear. Let's try something darker...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 1:
elseif(gzTextVar.zEntities[i].sState == "Goth TF0") then

    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF1"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GothTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/GothTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie applies a layer of base and eyeshadow. She adds eyeliner, and then more and more. " .. sName .. "'s eyes gain a very thick black outline." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Dark, yes, very dark. Lonely, isolated, but dignified. Elegant. Hmm...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator frowns and turns, walking to the racks of clothes behind you. She mutters complaints while you stare idly at the puppet." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She looks so pretty already. Maybe your creator will even ask you to help!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/GothTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator reappears, bearing a heavy violet hairpiece. She gives an indeterminate look, then slots it over the puppet's scalp and affixes ribbons to it." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, purple is such a sad color. Yes, that will do nicely.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The hairpiece grows heavy as it affixes to " .. sName .. "'s head, giving her fluffy purple hair. She remains unmoved." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Despite the joy you feel, you can sense a dark pall hanging over the puppet's mind." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 2:
elseif(gzTextVar.zEntities[i].sState == "Goth TF1") then

    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF2"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GothTF1"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Oh, oh, oh. Yes, ones afflicted by depression often wear black, no? It is only suiting.' your creator says, turning to you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Dear, you know what dress I am referring to, don't you? Please fetch it.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your joints immediately move. You know exactly which dress would fit. Towards the back of the racks, a black striped dress with a moth theme beckons to you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It is so soft and delicate to the touch. The excitement is indescribeable. Your new sister will be wearing this in mere moments! You rush back to your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Very well done, my dear. And you brought the boots, too.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You had not noticed them, but you are carrying a pair of long leather boots as well. You smile broadly and give them to your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Goth TF2") then

    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF3"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GothTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'The boots are nice and long, complementing the futility of our long lives. I've given a moth accent, because we are drawn to light but repulsed by flame.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You swear the puppet almost moved itself under the weight of the burden crushing it. She looks so gloomy in her new dress and boots." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You stand aside once again, waiting for an order. You hope your sister will be ready soon. You want to play with her." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Goth TF3") then

    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF4"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GothTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Red, yes, red eyes. I'll give you red eyes. Stumbling around blind in the dark - a fate too cruel even for us miserable creatures.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator snaps her fingers. In your hands appears a set of paints. You giggle and hand them to her. She pats your head and turns to the puppet." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 4:
elseif(gzTextVar.zEntities[i].sState == "Goth TF4") then

    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF5"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GothTF3"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator paints beautiful red eyes on the puppet. You hear a brief sucking sound as she finishes. The eyes begin to move and focus." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "The eyes drift across to you, then your creator, then they resume staring straight ahead." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'There. Done. Such a shame, it is, but you'll have to see.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is almost ready. You can feel excitement building, but remain at your creator's call." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 5:
elseif(gzTextVar.zEntities[i].sState == "Goth TF5") then

    --Flags
    gzTextVar.zEntities[i].sState = "Goth TF6"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/GothTF4"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'I suppose having you sit there, limp and uncaring, would be too kind a mercy. Let's fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Drawing near to the puppet, your creator places her hand atop your sister's head. Her eyes begin to dart back and forth." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Thoughts are flooding into her. Memories, ideas, places, names. Orders. Commands. Obedience." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smirks. She is dark, elegant, remote, mysterious, and utterly obedient to her creator. Your new sister smiles at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her joints begin to stir. She is ready." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Goth Transformation Stage 6:
elseif(gzTextVar.zEntities[i].sState == "Goth TF6") then

    --Flags
    gzTextVar.zEntities[i].sState = "Doll Goth"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Goth " .. sEndLetter
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/GothTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 0, 4, ciCodeFriendly)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Goth " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "" .. sName .. " obeys instantly. She stands as her creator searches her for imperfections. There are none." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " smiles and curtseys at her creator, reborn as a doll." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " turns to you. 'Thank you so much, sister. You've brought light to my dark heart.' You smile. You share her joy." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

    --Change AIs.
    gzTextVar.zEntities[i].bIsCrippled = false
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Doll.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Doll"
    gzTextVar.gzPlayer.bDollMoveLock = false
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end