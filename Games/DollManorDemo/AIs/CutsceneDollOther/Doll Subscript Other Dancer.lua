--[ =================================== Doll Subscript: Dancer =================================== ]
--Contains the call strings used for the Dancer transformation. Used when someone other than the player
-- is being transformed. Dialogue only displays if the player is watching.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--Other variables.
local sName = gzTextVar.zEntities[i].sDisplayName
local sUseSkin = "Root/Images/DollManor/Transformation/DancerTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

--[Sequence]
--Blank Doll: Begin transformation:
if(gzTextVar.zEntities[i].sState == "Blank Doll") then
    
    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF0"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/BlankDoll"
    
    --Sequence
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Well done, my sweet. You've brought a playmate, exactly as I intended.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "You feel your chest swell with pride. Your creator paid you a compliment! You want to cry with joy, but will have to settle for watching a new sister be born." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", 0)
    TL_SetProperty("Append", "'As for you, hmm, let's see what we can do...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'How about a dancer? Spinning and leaping gracefully, what a show you'll put on for us!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 1:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF0") then

    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF1"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/DancerTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/DancerTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie applies a layer of base and eyeshadow. She is careful and deliberate, making sure your sister's makeup is done perfectly." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'We must have big pink lips so everyone can see your wonderous smile. Isn't that right, my sweet?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator finishes with the makeup and stands back. She looks to you. 'Blue or yellow?' she asks." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yellow', you say instantly. You didn't even think before speaking, the word simply came to you. Your creator is delighted. She pats you on the head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She walks to the clothesracks behind you and leaves you with your sister. You look at her makeup. She's so pretty!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/DancerTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator reappears, bearing a dirtied blonde hairpiece. She attaches it to the head and ties the hair up into a bun, complete with butterfly pin." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Can't have you tripping over your own hair, can we? Oh you look marvellous!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The hairpiece makes a crunching sound as it digs into your sister's head. It has become her hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You stand idle again, waiting for any command from your creator. You hope you can help more!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 2:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF1") then

    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF2"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/DancerTF1"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Let us complete the motif... Butterflies and hearts, such a lovely combination.' Your creator turns to you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Dear, please fetch the dress for me. You know which one I speak of, don't you?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your joints begin to move before you even think of it. The dress is towards the back of the racks, and you've been asked to fetch it! How exciting!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You find it quickly and run your fingers briefly over it. It is so bright it could be seen from leagues away - your sister will love dancing in it!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "As you approach your creator, she has finished slipping a matching pair of soft red shoes over your sister's feet." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Very well done, my dear. I'm so proud of you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A compliment! Your heart nearly skips a beat at the thought! You don't even have a heart, you're a doll! What a silly dolly you are!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF2") then

    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF3"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/DancerTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Such a wonderful piece, one of my finest. Wouldn't you agree?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "'Yes, creator. She's perfect.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "You eagerly await the next step. You've done so well for your creator, you can't wait to help her again!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF3") then

    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF4"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/DancerTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'But you must see to be seen, isn't that right, my dear? I'll give you big green eyes! Everyone loves green eyes!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A set of paints appears in your hands, though you do not recall fetching them. You giggle and blush." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You must have gotten them earlier and simply forgot. What a silly dolly you are!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 4:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF4") then

    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF5"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/DancerTF3"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator paints deep, thoughtful green eyes on the puppet. Your sister blinks a few times as the eyes finish." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "The eyes focus on you, then your creator, and then resume sitting straight ahead. From far away, you hear a whisper. It says something like 'calibration' but you can't make out the rest." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You instantly forget the whispering and focus on your sister's big beautiful eyes. She looks so pretty!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Everyone will love you, my dear, I'm sure of it.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The only thing greater than your desire to play with your new sister is your desire to be a good doll for your creator. You're so excited you might burst!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 5:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF5") then

    --Flags
    gzTextVar.zEntities[i].sState = "Dancer TF6"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/DancerTF4"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'One last thing - the dances! I must teach you to dance! Ha ha ha ha!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Drawing near to the puppet, your creator places her hand atop your sister's head. Her eyes begin to dart back and forth." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Thoughts are flooding into her. Memories, ideas, places, names. Orders. Commands. Obedience." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smirks. She is graceful, agile, delicate, and totally obedient to her creator. Your new sister smiles at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her joints begin to stir. She is ready." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Dancer Transformation Stage 6:
elseif(gzTextVar.zEntities[i].sState == "Dancer TF6") then

    --Flags
    gzTextVar.zEntities[i].sState = "Doll Dancer"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Dancer " .. sEndLetter
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/DancerTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 2, 4, ciCodeFriendly)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Dancer " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "" .. sName .. " obeys instantly. She stands as her creator searches her for imperfections. There are none." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " smiles and curtseys at her creator, reborn as a doll." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " turns to you. 'Thank you so much, sister! I can't wait to dance for you!' You smile. You share her joy." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

    --Change AIs.
    gzTextVar.zEntities[i].bIsCrippled = false
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Doll.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Doll"
    gzTextVar.gzPlayer.bDollMoveLock = false

end