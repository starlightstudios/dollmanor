--[ =================================== Doll Subscript: Princess =================================== ]
--Contains the call strings used for the Princess transformation. Used when someone other than the player
-- is being transformed. Dialogue only displays if the player is watching.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--Other variables.
local sName = gzTextVar.zEntities[i].sDisplayName
local sUseSkin = "Root/Images/DollManor/Transformation/PrincessTFBase" .. string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.zEntities[i].iSkinColor + 65) --65 being 'A'

--[Sequence]
--Blank Doll: Begin transformation:
if(gzTextVar.zEntities[i].sState == "Blank Doll") then
    
    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF0"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/BlankDoll"
    
    --Sequence
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Well done, my sweet. You've brought a playmate, exactly as I intended.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "You feel your chest swell with pride. Your creator paid you a compliment! You want to cry with joy, but will have to settle for watching a new sister be born." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", 0)
    TL_SetProperty("Append", "'As for you, hmm, let's see what we can do...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Of course. A royal aura pervades this one, and I'll give her appearance to match! Ha ha ha!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 1:
elseif(gzTextVar.zEntities[i].sState == "Princess TF0") then

    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF1"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PrincessTF1"
    
    --Sequence
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/PrincessTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie puts on some simple makeup, being sure to give your sister pretty pink lips. She chuckles to herself as she does so. You giggle. Whatever joke she thought of was so funny!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'But it won't do, no, won't do... She needs a tiara! Of course!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator whirls to you excitedly. 'Don't you agree?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Unregister Image")
    TL_SetProperty("Append", "'Of course, creator. A princess must have a crown,' you reply. The words came to you before you even thought of them. What a clever dolly you are!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/PrincessTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "'And I know just where one is! Oh this will be so lovely!' Your creator says, dashing to the clothesracks. You grin as you wait for her return." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", sName, sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", sName, "Root/Images/DollManor/Transformation/PrincessTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator comes bearing a hairpiece with short, curly blonde hair. She attaches it to your sister's head. She grins and adjusts the tiara slightly." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'So elegant, so royal! We've only just begun and you're looking spectacular!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The hairpiece grows heavy as it affixes to " .. sName .. "'s head. She now has curly blonde hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You continue to grin as you wait for your creator to give you another order." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 2:
elseif(gzTextVar.zEntities[i].sState == "Princess TF1") then

    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF2"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PrincessTF1"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator puts a finger to her lips. She is deep in thought. Something catches your eye in the gloom, and you turn to look." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Before your eyes, you see a pink dress with roses attaches begin to form from nothing. Thread and fabric from nowhere stitch together until it is created." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It floats to the clothesracks as you watch. You barely notice, and soon you have forgotten it. You're a silly doll, thinking silly thoughts." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'She must have a dress to perform her royal duties. Dearie, would you get it for me? You know the one.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your joints move. You find the dress you saw earlier, and bear it aloft to your creator. She is happy and pats you on the head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Some socks are in your hands, and you pull them onto your sister before your creator fits the dress onto her. She looks lovely." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Princess TF2") then

    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF3"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PrincessTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'It fits! What a marvel, it's as if the dress was made for you!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "An odd thought occurs to you. The dress was made for her, wasn't it?" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The thought vanishes as quickly as it came. You are empty again. You smile blankly as your creator tugs at the edges of the dress, searching for errors." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Now, a paintbrush and selection of paints has appeared in your hands. You know that a good dolly doesn't ask how or why, she obeys. You hold the paints until ordered otherwise." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 3:
elseif(gzTextVar.zEntities[i].sState == "Princess TF3") then

    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF4"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PrincessTF2"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'I think she'll need eyes if she is to govern her subjects, won't she? What sort of princess doesn't have pretty eyes?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hold up to the paints and your creator takes them. She smiles at you. You are a good dolly. You wait patiently as she begins her artistry." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 4:
elseif(gzTextVar.zEntities[i].sState == "Princess TF4") then

    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF5"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PrincessTF3"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "Soon, hazel eyes adorn your new sister. She blinks, and scans the room." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "The eyes drift across to you, then your creator, then they resume staring straight ahead." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Aren't your eyes so cute? They match you perfectly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is almost ready. You can feel excitement building, but remain at your creator's call." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 5:
elseif(gzTextVar.zEntities[i].sState == "Princess TF5") then

    --Flags
    gzTextVar.zEntities[i].sState = "Princess TF6"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Transformation/PrincessTF4"
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "'One last thing - a lifetime of grooming for the role, of course! Let's get you ready for your duties...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Drawing near to the puppet, your creator places her hand atop your sister's head. Her eyes begin to dart back and forth." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Thoughts are flooding into her. Memories, ideas, places, names. Orders. Commands. Obedience." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "She smirks. She is just, gracious, kind, regal, and utterly obedient to your creator. Your new sister smiles at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Her joints begin to stir. She is ready." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 6:
elseif(gzTextVar.zEntities[i].sState == "Princess TF6") then

    --Flags
    gzTextVar.zEntities[i].sState = "Doll Princess"
    gzTextVar.zEntities[i].sQueryPicture = "Root/Images/DollManor/Characters/Doll Princess " .. sEndLetter
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, i, 4, 4, ciCodeFriendly)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Princess " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "" .. sName .. " obeys instantly. She stands as her creator searches her for imperfections. There are none." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " smiles and curtseys at her creator, reborn as a doll." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "" .. sName .. " turns to you. 'It is from your kindness that I have the right to be your sister. Thank you.' You smile. Your new sister is the best!" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

    --Change AIs.
    gzTextVar.zEntities[i].bIsCrippled = false
    gzTextVar.zEntities[i].sCommandHandler = gzTextVar.sEntityHandlers .. "Doll.lua"
    gzTextVar.zEntities[i].sAIHandlerHostility = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPostTurn = "Doll"
    gzTextVar.zEntities[i].sAIHandlerPrimary = "Doll"
    gzTextVar.gzPlayer.bDollMoveLock = false

end