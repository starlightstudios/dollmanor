--[ ====================================== Defeated By Doll ===================================== ]
--Mary has been defeated in combat by a doll! Oh no!

--Argument Listing:
-- 0: i - Which entity is updating.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--[ ========================================= Execution ========================================= ]
--Skin color letter.
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--Change the player's state to blank doll.
gzTextVar.gzPlayer.sFormState = "Blank Doll"

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The doll, standing over your wounded form, smiles and leans down. You can barely muster a whimper." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The doll approaches you without hesitation and injects a small needle into your neck. You try to push yourself away, but you can't catch your breath no matter how hard you breathe." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Jessie and Lauren pale in horror as the doll girl injects you. Jessie attempts to intervene, but the doll pushes her away. You let out a gasp. 'Run!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie is about to protest when she sees the changes overtaking you. With a pained expression, she grabs Lauren and spirits him into the manor. She calls back promises to save you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Now you are alone with the doll..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Jessie tries to fight back the doll, but is shoved away. The doll shows impressive strength. She stands as little chance as you did." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Jessie, run! Run, save Lauren!' you wheeze." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie looks ready to fight, but realizes the gravity of the situation. She flees into the manor, hurling promises to save you as she goes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Now you are alone with the doll..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Lauren screams and shouts. He pounds his fists on the doll girl, who ignores him. She is much stronger." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Find - Jessie - run!' you wheeze. Lauren fights back his tears and nods. He runs in a panic into the manor." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hope he'll be okay without you. Now, you are alone with the doll girl." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

TL_SetProperty("Append", "The doll smiles emptily and stands you up. She supports you as you feel the fluid in the needle flow into your body." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
TL_SetProperty("Append", "She smiles at you and withdraws the needle. Your legs seem to have recovered enough to stand on your own, but you catch sight of your wrist as you struggle to balance." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "It seems to have become rigid, pale white, and completely numb. You realize that you can feel the same lack of sensation all over your body now. It's spreading up your legs." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You're losing feeling wherever the plastic goes. You have only a few seconds before it takes over your whole body, but you're oddly complacent..." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF1", ciImageLayerDefault)
TL_SetProperty("Append", "As the plastic spreads, an odd euphoria takes you. This is wonderful, you love it, you want it. An involuntary giggle escapes your mouth." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Very soon, you can't feel most of your body. Thoughts of running away are soon replaced by curiosity. You wonder what will happen when you're completely changed. It excites you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Your hair soon begins to fall out. You barely notice. It feels so good, so right, that you wouldn't protest even if you could." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The doll stands nearby, smiling emptily. She begins tugging at your clothes, stripping you. You can't resist, and wouldn't anyway. You want to help her, desperately, but are almost entirely numb. You begin to smile with the same empty expression as she does." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollUpperC", ciImageLayerDefault)
TL_SetProperty("Append", "The doll helps you to a sitting position, holding you gently until your body balances. Your hair completely falls out, and you feel your head becoming smooth." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You want to smile, but your mouth has gone numb like the rest of your body. You look forward to the coming numbness. The less you can feel, the more complete you are." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Now your thoughts are fading. The numbness has reached your mind, and you will be completed soon. The euphoria begins to subside, and you know that would excite you if you could feel excitement anymore." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The last of your body dollifies as you watch behind unmoving, unblinking eyes. As your head transforms, all motivation and concern leave you. Your final thought is hoping your new creator will make you pretty. You want to be a good doll. And soon, you want nothing." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your thoughts become empty. Your mind is as empty as your smooth, plastic body. You are a featureless blank slate, waiting to be molded into some other shape." .. gsDE)
TL_SetProperty("Create Blocker")

--Optional, skin color change.
if(sEndLetter ~= "C") then
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
    gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
    TL_SetProperty("Append", "Your plastic skin changes color, changing as the will of your creator washes over you. You will be with her soon." .. gsDE)
    TL_SetProperty("Create Blocker")
end
            
TL_SetProperty("Append", "Able to see but not able to think, you gaze into infinity as an unfinished puppet." .. gsDE)
TL_SetProperty("Create Blocker")
