--[ =================================== Doll Subscript: Bride =================================== ]
--Contains the call strings used for the Bride transformation.
local sUseSkin = "Root/Images/DollManor/Transformation/BrideTFBase" .. string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--In titan transformations, the end letter is always "E"
if(gzTextVar.gzPlayer.bTitanTransformation) then 
    sEndLetter = "E" 
    sUseSkin = "Root/Images/DollManor/Transformation/BrideTFBaseE"
end

--Blank Doll: Begin transformation:
if(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF0"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF0"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Common handler.
    LM_ExecuteScript(fnResolvePath() .. "Doll Subscript Common.lua")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        
        --Normal:
        if(gzTextVar.bDollSequenceVoluntary == false) then
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Upon seeing you, Pygmalie shrieks and dashes towards you in a panic!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'My dear! Today is the big day, and you're not even dressed! Don't worry, I'll take care of you!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Pygmalie picks up your limp form and places you in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Oh, I can fix this, I can fix this...'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'A bride without her wedding dress, so unsuiting! But I am a genius and no challenge is too great!'" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Case: Player is volunteering in some way, and Pygmalie is less excited.
        else
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Pygmalie picks your light, empty body up and mulls over what to do with you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She places your limp form in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them. She mumbles to herself." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'A bride, on a wedding day, hmm, no, unless she - yes, quite right...'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She looks you in your empty eyes. 'Today is your big day, my dear, and you don't even have your dress on.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She gives you a wicked grin, and begins her work." .. gsDE)
            TL_SetProperty("Create Blocker")
    
        end
    
    --Sequence plays out slightly different if in titan mode.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'There are such dangers out there, aren't there, little one? But you will be so strong. You will protect me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'That is, of course, no excuse for going about undressed!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Pygmalie gestures to a nearby chair. A full-length mirror sits in front of it. You sit in the chair and wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'You will be the cutest of my honor guard, yes you will!'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'I'll dress you as a blushing bride... Yes, that will fit you to a tee.'" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 1:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF0") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF1"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie focuses carefully on your cheeks, giving them a bright red color. She giggles in delight as she applies your eyeshadow and liner." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Oh, won't you look so spectacular! I'm distracting myself, I should focus on your makeup, but I can't help it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Excited, she finishes your makeup and dashes into the racks of clothes behind you. You hear her tossing pieces around, searching for something." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "She emerges, with a miffed look on her face. She is undeterred. You are unthinking, merely waiting to be completed." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I couldn't find the dress I was thinking of, but I did find you this veil! And, of course, a little teal hair to match the dress!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She places the hair and veil on your head. She adjusts it for a moment before smirking to herself. As soon as she does, you feel a crawling sensation. The hair attaches to your head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The veil hangs over your blank eyes. It does little to obscure your creator's beaming face." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/BrideTF1", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 2:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF1") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF2"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "Without another word, your creator once again dashes to the clothesracks. As she moves away from you, you cease to notice anything. You cease to anticipate. You cease to think." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You sit unmoved as she tosses dresses and underwear to the side. She is searching very hard." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Found it!' you hear her shout." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF2") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF3"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Look! A true marvel! Put it on, dear, put it on!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "The dress fits over your frame. Your creator is nearly jumping with glee, but composes herself long enough to affix a necklace and wristband of beads." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I found a bouquet for you, as well! You know the tradition, right? Oh, I hope someone as cute as you catches it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Still unmoving, you are finding it difficult to contain your urges. You want to clap with excitement and thank your creator, but you are incomplete. So you wait." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/BrideTF2", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF3") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF4"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'What color eyes... perhaps a violet? Yes, violet. Very good!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "From behind you, Pygmalie produces paints and paintbrushes. She mixes together a light brown and smiles." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 4:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF4") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF5"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF3"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'I can't wait to see you on the altar...' she says as she applies the paints to your eyes. You feel an odd pressure as your sight goes dark." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Smiling on your big, special day. Dancing the dance for just you two... Oh, I'm going to cry thinking about it!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "She wipes away a tear and gives a sniff as your vision returns. The eyes she has painted on are now your eyes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You try to fight back your own tears of joy. Your creator will tell you when you are ready, and you must hold out until then." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/BrideTF3", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 5:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF5") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Bride TF6"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/BrideTF4"
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/BrideTF4")
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Yes, we can't have that. We should fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand on your head, and you feel your thoughts begin to stir." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "It's the big day. Your bridesmaids cluster around you, giggling and chattering. You are composed, but nervous. The chapel stands before you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your best friend goes to the door to open it for you. You focus on her. She is young, bright-eyed, smiling. You focus harder. Her skin becomes smooth, hard." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She turns to plastic, like you. Her joints become balled and she goes limp briefly before resuming her pert pose. You grin." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The bridesmaids follow suit, turning to hard plastic. You let out a giggle and their excited chattering resumes." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The door opens before you and they fall silent. Rows and rows of revellers await. You enter, focusing on each of them. They turn to plastic as well." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You reach the altar and turn to see the guests, all dolls. All sisters. This is your big day, but theirs too. They are closer than family." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You smile broadly as another doll approaches. She is dressed as you are. She is your bride, and you hers. You say your vows." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Joined together in matrimony, you kiss your bride. The chapel erupts in cheers." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "There are many more brides to make, many more sisters to create. You will bring them into your new family, as your creator bids." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/BrideTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "You feel your joints springing to life. A small smile etches itself onto your face..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/BrideTF4", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Bride Transformation Stage 6:
elseif(gzTextVar.gzPlayer.sFormState == "Bride TF6") then

    --Indicator.
    local iIndicatorX = 1
    if(gzTextVar.gzPlayer.bTitanTransformation == true) then iIndicatorX = iIndicatorX + 11 end

    --Flags
    gzTextVar.gzPlayer.sFormState = "Doll Bride"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Bride " .. sEndLetter
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    
    --Sequence
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Bride " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "You obey instantly, standing up and presenting yourself for your creator to look over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey at your creator, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Titan sequence.
    else
        
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now. You are my personal warrior, a knight at my beck and call. Do you know what that means?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey. 'No harm shall come to you, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She claps with delight and giggles. You bow before her, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You can think of some uncouth humans your creator must be protected from. It is your task to find them." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Default Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

    --Fullheal the player.
    gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
    gzTextVar.gzPlayer.bIsCrippled = false
    TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)

    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end