--[ =================================== Doll Subscript Common =================================== ]
--Common handler used by all subscripts. This adds text if Jessie or Lauren see the player about
-- to be transformed.

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Append", "Jessie and Lauren turn to see you as you are dragged into the room. Their faces twist in horror at the sight. You merely gaze at them, unable to think." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Lauren screams and flees into the manor. Jessie chases after him, no less panicked at the sight of you. The two are gone in seconds. You do not care." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Jessie recoils as you are dragged into the room. 'Mary? Mary! Oh my gosh what have they done to you?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You think no thoughts and do not answer. You cannot move." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Jessie looks ready to fight, but sees your pretty sister and decides not to. She is indecisive for a moment, then runs into the manor." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You hear promises. She says she will try to help you. You think no thoughts. You are empty." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false
    
    --Dialogue.
    TL_SetProperty("Append", "Lauren looks up as you are dragged into the room. His eyes open in shock. He is clearly fighting back tears. 'Mary? Mary, no! Nooo!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You think no thoughts. You cannot move. You are empty." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Crying loudly, Lauren panicks and flees into the manor. Your sister pays him no attention as she presents you to your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end