--[ ================================== Doll Subscript: Princess ================================= ]
--Contains the call strings used for the Princess transformation.
local sUseSkin = "Root/Images/DollManor/Transformation/PrincessTFBase" .. string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--In titan transformations, the end letter is always "E"
if(gzTextVar.gzPlayer.bTitanTransformation) then 
    sEndLetter = "E" 
    sUseSkin = "Root/Images/DollManor/Transformation/PrincessTFBaseE"
end
    
--Blank Doll: Begin transformation:
if(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF0"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF0"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Common handler.
    LM_ExecuteScript(fnResolvePath() .. "Doll Subscript Common.lua")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        --Normal:
        if(gzTextVar.bDollSequenceVoluntary == false) then
            TL_SetProperty("Append", "Upon seeing you, Pygmalie puts her hand to her heart and rushes over." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "'My my, what if your adoring public sees you! Your mystique would be shattered!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Pygmalie picks up your limp form and places you in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'A princess must never be seen in such a state... But I can help you, my dear.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Just sit there and let me take care of everything.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
        --Case: Player is volunteering in some way, and Pygmalie is less excited.
        else
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Pygmalie picks you up, gently. She is very careful. 'I did not realize I was in a royal presence', she says wistfully." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She seats you in a chair opposite a full length mirror. You see her behind you, searching for something." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She produces a bag of cosmetics and sorts through what she has. She grins." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Oh, it's obvious. You were incognito, my dear, but now that you are found you must let us see your true form.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'A lovely princess! To think you were the whole time! Ha ha ha!'" .. gsDE)
            TL_SetProperty("Create Blocker")
    
        end
    
    --Sequence plays out slightly different if in titan mode.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'There are such dangers out there, aren't there, little one? But you will be so strong. You will protect me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'But the first priority of a doll, no matter what, is to be cute. Don't you agree?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Pygmalie gestures to a nearby chair. A full-length mirror sits in front of it. You sit in the chair and wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'You will be regal, formal, polite, and a great warrior.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'A princess of battle. Clothed in a grand dress but wicked of sword and wit. Oh ho ho ho!'" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 1:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF0") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF1"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "After some rummaging through the racks of dresses and supplies behind you, Pygmalie appears bearing a wig of lovely blonde hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Curly hair, and it's blonde! Your favourite, right?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Yellow has always been your favourite color, and you are glad your creator remembered that. She's so kind and perfect." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "As soon as she attaches the wig to your head, you feel a great weight as it attaches to your scalp. You now have short, curly, blonde hair." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Hidden in her smock she produces a fine tiara. It seems to glow in the darkness of the manor. She fits it to your head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'I'll give your cheeks some blush to match, and a bit of nail polish - you're so pretty already, and we've just begun!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PrincessTF1", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 2:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF1") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF2"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Now, let's see', Pygmalie says to herself. She vanishes behind you again." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Clothes make the princess, do they not? But, I don't think you want anything too fancy or formal. It's best to just be huggable and cute, for now.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You agree with your creator. You want to be huggable and cute, and are certain she will make it so." .. gsDE)
    TL_SetProperty("Create Blocker")

    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF2") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF3"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "'There we go! A simple, pretty dress, long socks, and shoes. Perfect!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator slips the dress over and you drags the socks up your legs before fitting the shoes. You agree that your wardrobe is perfect." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You want to thank your creator for her attention to detail, but have not been addressed directly and will not act of your own volition." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PrincessTF2", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF3") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF4"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'You're looking magnificent, but I think I've forgotten something vital. Eyes! Nobody can see your gorgeous hazel eyes!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She pulls out a paintbrush and selection of paints from nearby and steps in front of you. You remain completely motionless as she selects her colors." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 4:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF4") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF5"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF3"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "Delicately, softly, she begins applying her paints to your eyes. You feel an odd pressure as your vision fills with color and you can no longer see." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Royal blue eyes. You look marvellous, my sweet. So fitting, so dainty!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "The instant she finishes speaking, your vision clears. The paint has become your eyes, and she steps away." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PrincessTF3", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 5:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF5") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Princess TF6"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PrincessTF4"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Now, let's fill in your empty mind...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand on your head, and you feel your empty mind begin to fill." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You have thoughts of sitting on a throne, surrounded by retainers. With dignity and grace, you issue decrees and pass judgements. Your royal spirit brings awe to the court." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A cur is placed before you, accused of crimes most heinous. You glare at her. She kneels, pushing herself as low as possible. She cries and begs forgiveness." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She has carried many burdens in her life, and you deign she is worthy of another chance. You bid her to your side. She obeys and kneels before you. You offer your hand." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "With hesistation, the cur kisses it. Her head becomes plastic and the change flows down her body. In an instant, she collapses at your feet." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Moments later, she rises before you. Your new retainer, dressed in finery, is eager to serve. She takes her place in the galleries." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The court observers applaud. You smile. You watch as the same happens to them. They become plastic, their joints become balled, and they become your sisters." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Filled with royal essence, you have been raised to be elegant, fair, capable, and obedient to your creator. She wishes you to make many more sisters to play with." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PrincessTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "You feel your joints springing to life. A small smile etches itself onto your face..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PrincessTF4", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Princess Transformation Stage 6:
elseif(gzTextVar.gzPlayer.sFormState == "Princess TF6") then

    --Indicator.
    local iIndicatorX = 4
    if(gzTextVar.gzPlayer.bTitanTransformation == true) then iIndicatorX = iIndicatorX + 11 end

    --Flags
    gzTextVar.gzPlayer.sFormState = "Doll Princess"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Princess " .. sEndLetter
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    
    --Sequence
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Princess " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "You obey instantly, standing up and presenting yourself for your creator to look over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey at your creator, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Titan sequence.
    else
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now. You are my personal warrior, a knight at my beck and call. Do you know what that means?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey. 'No harm shall come to you, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She claps with delight and giggles. You bow before her, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You can think of some uncouth humans your creator must be protected from. It is your task to find them." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Default Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")
    
    --Fullheal the player.
    gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
    gzTextVar.gzPlayer.bIsCrippled = false
    TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end