--[ ==================================== Doll Subscript: Punk =================================== ]
--Contains the call strings used for the Punk transformation.
local sUseSkin = "Root/Images/DollManor/Transformation/PunkTFBase" .. string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 is 'A'
local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'

--In titan transformations, the end letter is always "E"
if(gzTextVar.gzPlayer.bTitanTransformation) then 
    sEndLetter = "E" 
    sUseSkin = "Root/Images/DollManor/Transformation/PunkTFBaseE"
end

--Blank Doll: Begin transformation:
if(gzTextVar.gzPlayer.sFormState == "Blank Doll") then
    
    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF0"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF0"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Common handler.
    LM_ExecuteScript(fnResolvePath() .. "Doll Subscript Common.lua")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        --Normal:
        if(gzTextVar.bDollSequenceVoluntary == false) then
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Upon seeing you, Pygmalie gains a wicked grin and saunters over." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'You're such a naughty little one. Staying out past your bedtime, listening to silly music, such was my youth as well!'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Pygmalie picks up your limp form and places you in a chair, sitting opposite a full-length mirror. You can see your reflection in it as Pygmalie fawns over you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'If you're going to cause a ruckus, best to look the part.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Heh. Yes. Yes, oh yes.'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
        --Case: Player is volunteering in some way, and Pygmalie is less excited.
        else
            TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
            TL_SetProperty("Append", "Pygmalie picks you up and holds you by your back. You are held in a strained pose, as if she does not care about you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She dumps you in a chair opposite a full length mirror. You see her behind you, angrily throwing aside clothes in a search for something." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She finds it, a bag filled with cosmetics. She snickers to herself." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Yeah, tough! Cool! Hip! That's us, right dearie?'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Or maybe it isn't. Well, I don't care! And that's the coolest thing of all, right?'" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "'Let's get to work on you, punk', she says, beginning to apply makeup." .. gsDE)
            TL_SetProperty("Create Blocker")
    
        end
    
    --Sequence plays out slightly different if in titan mode.
    else
        TL_SetProperty("Register Image", "Pygmalie", "Root/Images/DollManor/Characters/Pygmalie", ciImageLayerDefault)
        TL_SetProperty("Append", "'There are such dangers out there, aren't there, little one? But you will be so strong. You will protect me.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'But the first priority of a doll, no matter what, is to be cute. Don't you agree?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "Pygmalie gestures to a nearby chair. A full-length mirror sits in front of it. You sit in the chair and wait." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You creator picks up a nearby bag full of cosmetics and begins sorting through them." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'My honor guard must be unpredictable. Tough. Rebellious.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "'A no-nonsense street punk. I think that will do. Yes, oh yes.'" .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 1:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF0") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF1"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF0", ciImageLayerOverlay)
    TL_SetProperty("Append", "Pygmalie begins digging piercings into your plastic. You dimly feel the prick and stick of a piece of metal." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'There - and - there. Somehow, sticking bits of metal into oneself is considered taboo and wrong. Feh.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She applies some stars and glitter, along with some light makeup. She grins at her work. You remain motionless and empty." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF1", ciImageLayerOverlay)
    TL_SetProperty("Append", "'Now this is going to be interesting... Well, I never met a challenge I could not master. Wait here!' she says and disappears behind you amongst the clothesracks." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Ha ha ha ha!' she laughs, emerging. She carries a wig, but promptly tears it in half before fitting it to your head." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "The hair seems to come alive briefly as it attaches itself to your scalp, becoming your hair. Part of it is missing, but that is exactly as your creator willed." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Perhaps my finest work, my sweet. I know you'll love it', your creator says. She is right, you do." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PunkTF1", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 2:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF1") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF2"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF1"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Now, perhaps some long nails and - ah, I've just the thing!' She disappears again. You hear her searching through the racks, and soon, some ripping sounds." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "For a few moments you had felt something, but as she left you that something disappeared. You can no longer recall what that something was." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You cannot recall anything. You are empty." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF2") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF3"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator promptly slips some heavy rubber boots on your feet and places a tattered skirt and shirt on you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'A spider theme for a tough, independent dolly - yes, and long nails to indicate you're wild and untamed. Right, my sweet?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "She is right, but you'd never admit it. You're too proud to. You're not the sort to just let someone else tell you how to live." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "No one, of course, except your creator. To her you are totally devoted." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Let's give you something different for eyes...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PunkTF2", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 3:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF3") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF4"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF2"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF2", ciImageLayerOverlay)
    TL_SetProperty("Append", "Your creator produces a set of paints and paintbrushes. She thinks for a few seconds before mixing." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Yes, bright yellow. Oh, that will look splendid.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 4:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF4") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF5"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF3"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "The paint is mixed and she begins brushing it across your eyes. Your sight goes black, black as your heart." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "'Oh, glorious yellow eyes for a hard-nosed punk. Yes!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your vision suddenly clears and is sharper than ever. You see the beaming face your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF3", ciImageLayerOverlay)
    TL_SetProperty("Append", "You almost want to stand up and thank her, but you're too proud and rebellious to do that. So you sit unmoving and unthinking." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PunkTF3", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 5:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF5") then

    --Flags
    gzTextVar.gzPlayer.sFormState = "Punk TF6"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Transformation/PunkTF4"
    gzTextVar.gzPlayer.sLayer0 = sUseSkin
    gzTextVar.gzPlayer.sLayer1 = gzTextVar.gzPlayer.sQuerySprite
    
    --Sequence
    TL_SetProperty("Append", "'Your mind is still empty. Let's fix that, and make you a real dolly, shall we?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your creator places her hand on your head, and you feel your thoughts begin to stir." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You are in the alley of some great big city. Steam and smoke drift listlessly overhead. It's a cold day, but you don't care. You don't care about anything." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Your friends huddle around you, playing dice and telling lewd jokes. You smirk as one of the boys gives you a wink." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "He stands before you, and you embrace. You make out for a few minutes, without caring who sees you in a public place. Nobody matters, nothing matters." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Now you're bored of him, so you push him away. He doesn't care. As you push him, his legs turn to plastic and his knees become ball joints. He crumples to the ground, inert." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "A few seconds later, your new doll sister rises. Holding hands, you go to your friends, still playing dice. They don't notice as their hair falls out to be replaced with smooth plastic." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Soon your new sisters and giggling and playing. You smirk again. You see two constables coming down the alley towards you. Your sisters stand as one with you. They get closer." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You swarm around them, toying and taunting. One of your sisters gets behind them and knocks one over. The constable becomes plastic before she hits the ground. She stands and grabs the other constable." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You shove a lit cigarette into the constable's mouth, forcing a deep drag. When the smoke comes back out, your new doll sister smiles at you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Everyone will know the simple pleasures of chance and reckless love. Nobody will tell you and your sisters how to live or what to do. Nobody but your creator." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/PunkTF4", ciImageLayerOverlay)
    TL_SetProperty("Append", "You feel your joints springing to life. A small smile etches itself onto your face..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Clean Up
    TL_SetProperty("Default Image", "You", sUseSkin, ciImageLayerSkin)
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Transformation/PunkTF4", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

--Punk Transformation Stage 6:
elseif(gzTextVar.gzPlayer.sFormState == "Punk TF6") then

    --Indicator.
    local iIndicatorX = 5
    if(gzTextVar.gzPlayer.bTitanTransformation == true) then iIndicatorX = iIndicatorX + 11 end

    --Flags
    gzTextVar.gzPlayer.sFormState = "Doll Punk"
    gzTextVar.gzPlayer.sQuerySprite = "Root/Images/DollManor/Characters/Doll Punk " .. sEndLetter
    gzTextVar.gzPlayer.sLayer0 = gzTextVar.gzPlayer.sQuerySprite
    gzTextVar.gzPlayer.sLayer1 = "Null"
    
    --Sequence
    TL_SetProperty("Append", "'Stand up, my little doll, and let your creator look at you.'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, iIndicatorX, 4, ciCodePlayer)
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Doll Punk " .. sEndLetter, ciImageLayerSkin)
    TL_SetProperty("Register Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Append", "You obey instantly, standing up and presenting yourself for your creator to look over." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Normal sequence.
    if(gzTextVar.gzPlayer.bTitanTransformation == false) then
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now, so go play wherever you want, my little dolly.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey at your creator, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    --Titan sequence.
    else
        TL_SetProperty("Append", "She smiles. 'Very good. You are ready now. You are my personal warrior, a knight at my beck and call. Do you know what that means?'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You smile and curtsey. 'No harm shall come to you, creator.'" .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "She claps with delight and giggles. You bow before her, as a newly crafted doll." .. gsDE)
        TL_SetProperty("Create Blocker")
        
        TL_SetProperty("Append", "You can think of some uncouth humans your creator must be protected from. It is your task to find them." .. gsDE)
        TL_SetProperty("Create Blocker")
    
    end
    
    --Clean Up
    TL_SetProperty("Default Image", "You", gzTextVar.gzPlayer.sQuerySprite, ciImageLayerDefault)
    TL_SetProperty("Default Image", "You", "Null", ciImageLayerOverlay)
    TL_SetProperty("Unregister Image")

    --Fullheal the player.
    gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHPMax
    gzTextVar.gzPlayer.bIsCrippled = false
    TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        
    --Monsters lose hostility markers.
    LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
    
    --Unlock all doors.
    LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Unlock All Doors.lua")

end