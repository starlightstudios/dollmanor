--[ ===================================== Surrender To Doll ===================================== ]
--Called from a few places, player encounters a doll while the surrender flag is active.

--Argument Listing:
-- 0: iIndex - The entity index of the acting entity.

--Arg check.
local iRequiredArgs = 1
local iArgs = LM_GetNumOfArgs()
if(iArgs < iRequiredArgs) then
	Debug_ForcePrint(fnResolvePath() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
	return
end

--Arg resolve.
local i = tonumber(LM_GetScriptArgument(0))

--[Common Flags]
gzTextVar.gzPlayer.iClayInfection = -1

--[Phase 5]
--In phase 5, run a different cutscene.
if(gzTextVar.iGameStage == 5) then
    LM_ExecuteScript(gzTextVar.sRootPath .. "AIs/CutsceneDoll/Surrender To Doll Phase 5.lua", i)
    return
end

--[All Other Cases]
--Unset flag.
gzTextVar.bIsPlayerSurrendering = false

--Change the player's state to blank doll.
gzTextVar.gzPlayer.sFormState = "Blank Doll"

--Transformation sequence.
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
TL_SetProperty("Append", "The doll girl approaches you. Instead of preparing to fight, you lower your hands and make your surrender quite clear." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The doll approaches you without hesitation and injects a small needle into your neck. The prick hurts a little, but the pain ends quickly. She then begins to undress you, and you offer no resistance." .. gsDE)
TL_SetProperty("Create Blocker")

--If both Jessie and Lauren are in the same room...
if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then
    
    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'I want to be like them. I want to be... plastic...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie grabs Lauren and flees into the manor. She promises to save you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You giggle, and your new doll sister does the same. You hope you will make a good doll so you can play with Jessie and Lauren..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Flee Lauren.
    sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Jessie.
elseif(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsJessieFollowing = false
    gzTextVar.bIsJessieMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Jessie", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, what are you doing?'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'I want to be like them. I want to be... plastic...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Horrified, Jessie flees into the manor. She shouts promises to rescue you from whatever has come over you." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You giggle, and your new doll sister does the same. You hope you will make a good doll so you can play with Jessie soon..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Jessie.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iJessieIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iJessieIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

--Just Lauren.
elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.gzPlayer.sLocation) then

    --Flags.
    gzTextVar.bIsLaurenFollowing = false
    gzTextVar.bIsLaurenMainHalled = false

    --Dialogue.
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Lauren", ciImageLayerDefault)
    TL_SetProperty("Append", "'Mary, stop! Stop! Fight her!'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/Mary", ciImageLayerDefault)
    TL_SetProperty("Append", "'I don't want to fight... I want to be plastic...'" .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "Lauren bursts into tears and tries to punch your doll sister. She barely notices. Frustrated, he flees into the manor, crying." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    TL_SetProperty("Append", "You giggle, and your new doll sister does the same. You hope you will make a good doll so you can play with Lauren soon..." .. gsDE)
    TL_SetProperty("Create Blocker")
    
    --Flee Lauren.
    local sLocation = fnMoveToRandomLocation(gzTextVar.iLaurenIndex, gzTextVar.saFleeLocations)
    if(sLocation ~= "Null") then
        TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Move Entity To Location.lua", 2, gzTextVar.iLaurenIndex, sLocation)
    end
    
    --Rebuild locality info.
    fnRebuildEntityVisibility()
    fnBuildLocalityInfo()

end

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF0", ciImageLayerDefault)
TL_SetProperty("Append", "You look down at yourself as the changes begin. Parts of your skin are becoming hardened, like plastic, and going numb. You catch sight of your wrist as it deforms and changes into a ball joint. The doll continues to undress you." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Everywhere the plastic spreads, you lose feeling entirely. New spots continue to appear and quickly spread." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Transformation/MaryDollTF1", ciImageLayerDefault)
TL_SetProperty("Append", "As the plastic spreads, an odd euphoria takes you. This is wonderful, you love it, you want it. An involuntary giggle escapes your mouth." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Very soon, you can't feel most of your body. You quietly fold yourself into a sitting position, eager to see the transformation through." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Your hair soon begins to fall out. You barely notice. It feels so good, so right, that you wouldn't protest even if you could." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The doll stands nearby, smiling emptily. Your clothes lay in a heap, forgotten." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollUpperC", ciImageLayerDefault)
TL_SetProperty("Append", "The doll helps you to a sitting position, holding you gently until your body balances. Your hair completely falls out, and you feel your head becoming smooth." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "You want to smile, but your mouth has gone numb like the rest of your body. You look forward to the coming numbness. The less you can feel, the more complete you are." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "Now your thoughts are fading. The numbness has reached your mind, and you will be completed soon. The euphoria begins to subside, and you know that would excite you if you could feel excitement anymore." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "The last of your body dollifies as you watch behind unmoving, unblinking eyes. As your head transforms, all motivation and concern leave you. Your final thought is hoping your new creator will make you pretty. You want to be a good doll." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Append", "And soon, you want nothing." .. gsDE)
TL_SetProperty("Create Blocker")

TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Functions/Change Entity Indicator.lua", 4, -1, 6, 3, ciCodePlayer)
TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDollC"
TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDollC", ciImageLayerDefault)
TL_SetProperty("Append", "Your thoughts become empty. Your mind is as empty as your smooth, plastic body. You are a featureless blank slate, waiting to be molded into some other shape." .. gsDE)
TL_SetProperty("Create Blocker")

--Optional, skin color change.
if(sEndLetter ~= "C") then
        
    local sEndLetter = string.char(gzTextVar.gzPlayer.iSkinColor + 65) --65 being 'A'
    TL_SetProperty("Default Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
    gzTextVar.gzPlayer.sLayer0 = "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter
    TL_SetProperty("Register Image", "You", "Root/Images/DollManor/Characters/BlankDoll" .. sEndLetter, ciImageLayerDefault)
    TL_SetProperty("Append", "Your plastic skin changes color, changing as the will of your creator washes over you. You will be with her soon." .. gsDE)
    TL_SetProperty("Create Blocker")
end

TL_SetProperty("Append", "Able to see but not able to think, you gaze into infinity as an unfinished puppet." .. gsDE)
TL_SetProperty("Create Blocker")

--Clean up.
TL_SetProperty("Unregister Image")

--Set stats.
gzTextVar.gzPlayer.bIsCrippled = true
gzTextVar.gzPlayer.iHP = 0
TL_SetProperty("Set Player Stats", 0, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)

--Monsters lose hostility markers.
LM_ExecuteScript(gzTextVar.sModifyHostility, 0, 0, 0)
