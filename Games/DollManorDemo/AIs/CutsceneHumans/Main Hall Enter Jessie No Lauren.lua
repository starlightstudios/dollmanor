--[ ============================= Main Hall Enter Jessie No Lauren ============================== ]
--When entering the main hall with Jessie, but Lauren is not present, this cutscene plays.

--Fast-access variables.
local j = gzTextVar.iJessieIndex

--Fast-access strings.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = "Root/Images/DollManor/Characters/Pygmalie"

--Setup
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Dialogue.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'This is the place. I think we should be safe here for the time being.'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Is this the lady you were talking about?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Oh my goodness, did you make a new friend, little one? Oh, but she's a little off, isn't she?'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Hi. My name's Jessie.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Little off, little off, but fix fix fix it!' Pygmalie says with a laugh." .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Is she... all right?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'She doesn't seem dangerous. Maybe living all alone has sent her barmy.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'My my, little one. Talking about your creator as if she's not listening in? Naughty, naughty!'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'So, what's the plan, then?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Lauren's got to be somewhere around here. You stay here and try to get what you can out of Pygmalie, I'll go looking for him.'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'I guess I can manage that. Be careful.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Aren't I always?'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'No.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Yeah, well, I'll find him just to spite you now.'" .. gsDE)
fnDialogueFinale()
