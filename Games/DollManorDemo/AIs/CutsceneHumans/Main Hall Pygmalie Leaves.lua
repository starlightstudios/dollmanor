--[ ================================= Main Hall Pygmalie Leaves ================================= ]
--When entering the main hall with both Jessie and Lauren present, this scene plays.
if(gzTextVar.iGameStage >= 2) then return end

--Fast-access variables.
local j = gzTextVar.iJessieIndex
local l = gzTextVar.iLaurenIndex
local p = gzTextVar.iPygmalieIndex

--Fast-access strings.
local sMaryPath = gzTextVar.gzPlayer.sQuerySprite
local sLaurPath = gzTextVar.zEntities[l].sQueryPicture
local sJessPath = gzTextVar.zEntities[j].sQueryPicture
local sPygmPath = "Root/Images/DollManor/Characters/Pygmalie"

--Setup
local iLaurCnt = 1
local saLaurList = {sLaurPath}
local iMaryCnt = 1
local saMaryList = {sMaryPath}
local iJessCnt = 1
local saJessList = {sJessPath}
local iPygmCnt = 1
local saPygmList = {sPygmPath}

--Variables.
local sOldLocation
local fOldX, fOldY, fOldZ
local fNewX, fNewY, fNewZ

--Reposition Jessie to Mary's position.
sOldLocation = gzTextVar.zEntities[j].sLocation
gzTextVar.zEntities[j].sLocation = gzTextVar.gzPlayer.sLocation
fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[j].sIndicatorName, fNewX, fNewY, fNewZ)
    
--Reposition Lauren to Mary's position.
sOldLocation = gzTextVar.zEntities[l].sLocation
gzTextVar.zEntities[l].sLocation = gzTextVar.gzPlayer.sLocation
fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
fNewX, fNewY, fNewZ = fnGetRoomPosition(gzTextVar.gzPlayer.sLocation)
TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[l].sIndicatorName, fNewX, fNewY, fNewZ)

--Reposition Pygmalie to the middle of the Main Hall. This does not occur in the small manor.
if(gzTextVar.sManorType == "Normal") then
    sOldLocation = gzTextVar.zEntities[p].sLocation
    gzTextVar.zEntities[p].sLocation = "Main Hall N"
    fOldX, fOldY, fOldZ = fnGetRoomPosition(sOldLocation)
    fNewX, fNewY, fNewZ = fnGetRoomPosition("Main Hall N")
    TL_SetProperty("Move Entity Indicator", fOldX, fOldY, fOldZ, gzTextVar.zEntities[p].sIndicatorName, fNewX, fNewY, fNewZ)
end

--Dialogue.
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'This is the place.'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Look at all your friends, little one! You have so many and they're...'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Disobedient, unruly, independent...'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Is there a problem, ma'am?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Just like it said, just like it said. I have to go check something, quickly.'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'What's with that lady?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Again again, I thought it wasn't but it was! The great throbbing glow returns with each step! They must be stopped!'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Pygmalie? What's wrong?'" .. gsDE)
fnDialogueCutscene("Pygmalie", iPygmCnt, saPygmList, "'Stop them! Stop them! Can't let them have the pieces or all ends in ash!'" .. gsDE)

--Where Pygmalie goes changes.
if(gzTextVar.sManorType == "Normal") then
    fnDialogueCutscene("Nobody",   "Null",    "Pygmalie flees, screaming, to the west. You can hear her run down the hall to the north and slam a distant door." .. gsDE)

--Small manor:
else
    fnDialogueCutscene("Nobody",   "Null",    "Pygmalie flees, screaming, to the north. You can hear her quickly climb the stairs and a door slamming above." .. gsDE)
end
fnDialogueCutscene("Nobody",   "Null",    "You hear running as plastic feet scramble after her. Then, the main hall is quiet..." .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Was it something I said?'" .. gsDE)
fnDialogueCutscene("Jessie",   iJessCnt, saJessList, "'Should we go after her?'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'I think so. This place doesn't seem so safe, all of a sudden. I can't quite place it...'" .. gsDE)
fnDialogueCutscene("Lauren",   iLaurCnt, saLaurList, "'... I want to go home...'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "'Follow me.'" .. gsDE)
fnDialogueCutscene("You",      iMaryCnt, saMaryList, "(In her haste, Pygmalie seems to have dropped some things that look important...)" .. gsDE)
fnDialogueFinale()

--Flag. This causes the key to spawn where Pygmalie left it, instead of what the player is.
gzTextVar.bEnteredPhase2Naturally = true

--Activate phase 2.
TL_SetProperty("Exec Script", gzTextVar.sRootPath .. "Scenario Handlers/001 Set To Phase 2.lua", 0)
fnRebuildEntityVisibility()
fnBuildLocalityInfo()