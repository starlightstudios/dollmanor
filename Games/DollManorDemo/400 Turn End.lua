--[ ========================================= Turn End ========================================== ]
--Called whenever the player ends their turn.
local zTime = os.clock()
gzTextVar.iWorldTurns = gzTextVar.iWorldTurns + 1
TL_SetProperty("Set Storage Line", 0, "Turn: " .. gzTextVar.iWorldTurns)

--[Post-Exec]
--Special scenario cases get handled before ending checks.
LM_ExecuteScript(gzTextVar.sRootPath .. "Scenario Handlers/100 Turn Post Exec.lua")

--Reset the previous room handler. Some enemies need this but we can't assume it's correct any more.
gzTextVar.gzPlayer.sPrevLocationStore = gzTextVar.gzPlayer.sPrevLocation
gzTextVar.gzPlayer.sPrevLocation = "Null"

--[Endings]
--Checks if the game has ended.
gzTextVar.bIsGameEnding = false
LM_ExecuteScript(fnResolvePath() .. "500 Ending Handler.lua")
if(gzTextVar.bIsGameEnding == true) then return end

--Player's HP can regen when in non-human forms.
if(gzTextVar.gzPlayer.iHP < gzTextVar.gzPlayer.iHPMax) then
    
    --Regen form blockers. Humans do not regen HP.
    if(gzTextVar.gzPlayer.sFormState == "Human") then
        
    --Blank Dolls don't regen HP. That'd let them move away while being carried!
    elseif(gzTextVar.gzPlayer.sFormState == "Blank Doll") then

    --Dancer TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 6) == "Dancer") then

    --Goth TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Goth") then

    --Punk TF:
    elseif(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Punk") then

    --All checks passed. Player can regen HP.
    else
    
        --Increment timer. If it reaches 5, regen one HP.
        gzTextVar.gzPlayer.iRegenTimer = gzTextVar.gzPlayer.iRegenTimer + 1
        if(gzTextVar.gzPlayer.iRegenTimer >= 5) then
            gzTextVar.gzPlayer.iHP = gzTextVar.gzPlayer.iHP + 1
            gzTextVar.gzPlayer.iRegenTimer = 0
            
            --Store.
            gzTextVar.gzPlayer.bIsCrippled = false
            TL_SetProperty("Set Player Stats", gzTextVar.gzPlayer.iHP, gzTextVar.gzPlayer.iHPMax, gzTextVar.gzPlayer.iAtp, gzTextVar.gzPlayer.iDef, gzTextVar.gzPlayer.sQuerySprite)
        end
    end
    
--No need to regen.
else
    gzTextVar.gzPlayer.iRegenTimer = 0
end

--[ ==================================== AI Entity Handler ====================================== ]
--All respawn entries increment by one. This does not happen in phase 1! Enemies don't respawn!
if(gzTextVar.iGameStage > 1) then
    for p = 1, #gzTextVar.zRespawnList, 1 do
        
        --Spawns must be active to run their timers.
        if(gzTextVar.zRespawnList[p].bIsActive == true) then
            gzTextVar.zRespawnList[p].iRespawnTimer = gzTextVar.zRespawnList[p].iRespawnTimer + 1
        end
    end
end

--Run across the entities and order them to run their AI updates.
--print(string.format("Time before AI cycle: %.3f", os.clock() - zTime))
gzTextVar.bCombatActivatedThisTick = false
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    --Check where this entity is on the respawn list. If they have a slot, they set it to 0.
    for p = 1, #gzTextVar.zRespawnList, 1 do
        if(gzTextVar.zRespawnList[p].sIdentity == gzTextVar.zEntities[i].sSpecialIdentifier) then
            gzTextVar.zRespawnList[p].iRespawnTimer = 0
            break
        end
    end
    
    --If there's no handler, do nothing.
    if(gzTextVar.zEntities[i].sAIHandlerPrimary == nil or gzTextVar.zEntities[i].sAIHandlerPrimary == "None") then
        
    --Run AI routines.
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll") then
        fnDoll_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Titan") then
        fnTitan_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Jessie") then
        fnJessie_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Lauren") then
        fnLauren_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Pygmalie") then
        fnPygmalie_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll Tutorial Patrol") then
        fnDollTutPatrol_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary") then
        fnDollStationary_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat") then
        fnDollStationaryCombat_AI(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Tutorial Stationary Combat B") then
        fnDollStationaryCombatB_AI(i)
    end
end
--print(string.format("Time after AI cycle: %.3f", os.clock() - zTime))

--If an enemy has 0 HP, remove them. Some enemies can be removed passively, without combat.
local bRemovedEnemy = true
while(bRemovedEnemy == true) do
    bRemovedEnemy = false
    
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].zCombatTable ~= nil and gzTextVar.zEntities[i].zCombatTable.iHealth < 1) then
            fnRemoveEntity(i)
            bRemovedEnemy = true
            break
        end
    end
end

--After all entities have moved, run their secondary AI update. This allows the AIs to spot the player after movement.
-- We allow all entities to move first as some may change door states.
gzTextVar.bIsPostTurnSpotting = true
--print(string.format("Time before post-turn spotting: %.3f", os.clock() - zTime))
for i = 1, gzTextVar.zEntitiesTotal, 1 do
    
    --If there's no handler, do nothing.
    if(gzTextVar.zEntities[i].sAIHandlerPostTurn == nil or gzTextVar.zEntities[i].sAIHandlerPostTurn == "None") then
        
    --Run AI routines.
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Doll") then
        fnDoll_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Titan") then
        fnTitan_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Jessie") then
        fnJessie_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Lauren") then
        fnLauren_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Pygmalie") then
        fnPygmalie_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPrimary == "Doll Tutorial Patrol") then
        fnDollTutPatrol_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Tutorial Stationary") then
        fnDollStationary_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Tutorial Stationary Combat") then
        fnDollStationaryCombat_PostTurnSpotCheck(i)
    elseif(gzTextVar.zEntities[i].sAIHandlerPostTurn == "Tutorial Stationary Combat B") then
        fnDollStationaryCombatB_PostTurnSpotCheck(i)
    end
    
end
--print(string.format("Time after post-turn spotting: %.3f\n", os.clock() - zTime))
gzTextVar.bIsPostTurnSpotting = false

--Remove entities who zeroed their HP after their post-turn spotting.
bRemovedEnemy = true
while(bRemovedEnemy == true) do
    bRemovedEnemy = false
    
    for i = 1, gzTextVar.zEntitiesTotal, 1 do
        if(gzTextVar.zEntities[i].zCombatTable ~= nil and gzTextVar.zEntities[i].zCombatTable.iHealth < 1) then
            fnRemoveEntity(i)
            bRemovedEnemy = true
            break
        end
    end
end

--Scan across the respawn list. If any of the entries reached 10, respawn the doll.
--io.write("Sequence " .. gzTextVar.gzPlayer.sTFSeq .. "\n")
--io.write("Respawn report:\n")
local bRespawnedEnemy = false
local bRespawnedDoll = false
local bRespawnedTitan = false
local bRespawnedClaygirl = false
for p = 1, #gzTextVar.zRespawnList, 1 do
    
    --If the entity is inactive, do nothing.
    if(gzTextVar.zRespawnList[p].bIsActive == false) then
    
    --If the counter hit 20 (10 in the small manor), respawn. Respawn speed can increase as the game goes on!
    elseif(gzTextVar.zRespawnList[p].iRespawnTimer >= gzTextVar.iRespawnTurns) then
        
        --Check if the room is currently visible. If it is, enemies do not respawn and have their counter decremented.
        local iRoomIndex = fnGetRoomIndex(gzTextVar.zRespawnList[p].sSpawnPosition)
        if(iRoomIndex < 0 or iRoomIndex >= gzTextVar.iRoomsTotal) then
            
        --Room index is valid.
        else
        
            --Room is currently visible.
            if(gzTextVar.zRoomList[iRoomIndex].bIsVisibleNow == true) then
                gzTextVar.zRespawnList[p].iRespawnTimer = math.floor(gzTextVar.zRespawnList[p].iRespawnTimer * 0.40)
                
            --Room is not visible, respawn!
            else
            
                --Reset.
                gzTextVar.zRespawnList[p].iRespawnTimer = 0
                
                --If the first part is "Doll" then spawn a doll.
                if(gzTextVar.zRespawnList[p].sEnemyType == "Doll") then
                    bRespawnedEnemy = true
                    bRespawnedDoll = true
                    fnSpawnDoll(gzTextVar.zRespawnList[p].sSpawnPosition, gzTextVar.zRespawnList[p].sIdentity, gzTextVar.zRespawnList[p].sPatrolPath)
                
                --If it's "Titan" then spawn a resin titan.
                elseif(gzTextVar.zRespawnList[p].sEnemyType == "Titan") then
                    bRespawnedEnemy = true
                    bRespawnedTitan = true
                    fnSpawnTitan(gzTextVar.zRespawnList[p].sSpawnPosition, gzTextVar.zRespawnList[p].sIdentity, gzTextVar.zRespawnList[p].sPatrolPath)
                end
            end
        end
    end
end

--If at least one enemy respawned, print a message.
if(bRespawnedEnemy) then
    
    --Titans get priority:
    if(bRespawnedTitan == true) then
        TL_SetProperty("Append", "You hear a distant rumble of a mighty foe in the manor..." .. gsDE)
    
    --Dolls are lowest:
    elseif(bRespawnedDoll == true) then
        TL_SetProperty("Append", "You hear a distant giggle and the clacking of plastic joints somewhere in the manor..." .. gsDE)
    
    --Error/unhandled.
    else
        TL_SetProperty("Append", "You feel a hostile presence within the manor..." .. gsDE)
    end
end

--Rebuild locality info and visibility info.
fnRebuildEntityVisibility()
fnBuildLocalityInfo()

--Rebuild sound indicators.
LM_ExecuteScript(gzTextVar.sRootPath .. "Functions/Rebuild Sound.lua")

--[ ========================================= Move Lock ========================================= ]
--In situations where the player is not meant to move, lock their movement here.

--Player is a Doll:
if(string.sub(gzTextVar.gzPlayer.sFormState, 1, 4) == "Doll" and gzTextVar.iGameStage < 6) then
    
    --Jessie is a Blank Doll and in the Main Hall:
    if(gzTextVar.zEntities[gzTextVar.iJessieIndex].sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation and gzTextVar.zEntities[gzTextVar.iJessieIndex].sState == "Blank Doll") then
        
        --Flag.
        gzTextVar.gzPlayer.bDollMoveLock = true
        
        --Carrying both dolls?
        if(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Blank Doll") then
            
            gzTextVar.bAlreadySawLaurenTFStart = true
            
            TL_SetProperty("Append", "You enter the room quietly, carrying with you two puppets. Two new sisters. Two - something. Friends? Something is wrong..." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You're not a doll, you shouldn't be here... Where is here? How did you get here? You're getting confused!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You look at your hands - plastic hands can't be your, you're a human! What has happened to you? You've got to run!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You feel your creator draw close. You see her shadow on the floor, and you look up to see her. She has a wicked grin." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "The part of you that was scared disappears. You helped her find it, and now it's gone. You're now a perfect dolly. You grin. You love being perfect!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator will want your help finishing your new sisters, and you're nearly bursting with joy! You can't wait to play with them!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
            TL_SetProperty("Append", "You carry the puppets to the same chair you sat in not long ago, and set one down. Your other sister will have to wait her turn - you hope she can contain her excitement!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        --Just one.
        else
            TL_SetProperty("Append", "You enter the room quietly, bearing a limp, unmoving puppet. Your creator has not noticed yet. You stand and wait." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Something in you stirs. You should - run. Hide. Flee from this place. It's a strong instinct. You should carry the puppet with you." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "There must be a cure. There must be something you can do!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator turns to see you, standing quietly with your new sister. She smiles, and all the feelings wash away." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "She will want you to help in finishing your sister! Oh that will be so much fun!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
            TL_SetProperty("Append", "You carry the puppet to the same chair you sat in not long ago, and set her down. You hope she will enjoy being completed as much as you did!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        end
    
    --Lauren is a Blank Doll and in the Main Hall:
    elseif(gzTextVar.zEntities[gzTextVar.iLaurenIndex].sLocation == gzTextVar.zEntities[gzTextVar.iPygmalieIndex].sLocation and gzTextVar.zEntities[gzTextVar.iLaurenIndex].sState == "Blank Doll") then
        
        --Flag
        gzTextVar.gzPlayer.bDollMoveLock = true
        
        --First time this has happened for Lauren.
        if(gzTextVar.bAlreadySawLaurenTFStart == false) then
        
            --Flag.
            gzTextVar.bAlreadySawLaurenTFStart = true
        
            TL_SetProperty("Append", "You enter the room quietly. You feel your blank sister stir. Something about her is struggling." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "A memory strikes you, and you don't know where it came from. Your sister was - a boy? Your brother? No, that can't be right." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Should you run? Flee the manor? No. No. Good dollies ask their creator for help. You open your mouth to speak." .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "Your creator turns to see you, catching you. You were about to speak without being spoken to - what a naughty dolly!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "But she smiles, and you know you are forgiven. She will want your help finishing your new sister. You can't wait!" .. gsDE)
            TL_SetProperty("Create Blocker")
            
            TL_SetProperty("Append", "You carry the puppet to the same chair you sat in not long ago, and set her down. You hope she will enjoy being completed as much as you did!" .. gsDE)
            TL_SetProperty("Create Blocker")
        
        end
    end
end
