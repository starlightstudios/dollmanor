===================================================================================================
==                                             Forward                                           ==
===================================================================================================
The main website for this game is located at https://hopremastered.wordpress.com/string-tyrant/
It can also be found on itch.io. [Pending]
And on Steam. [Pending]
Project updates can be found there.

The project is funded by Patreon. https://www.patreon.com/Saltyjustice
Updates are mirrored there. If you like the game and want more, support it!

If you got the game from somewhere other than one of these two locations, and there were no links
to said locations, then it was done without permission. Please notify the author:
 Saltyjustice AT StarlightStudios DOT org


===================================================================================================
==                                          Game Credits                                         ==
===================================================================================================
Programming, Design, Concept, Story, Playtesting, Scripting, Level Design, EVERYTHING
 SaltyJustice (Saltyjustice AT StarlightStudios DOT org)

Concepts, Linework, Coloring
 Koops

Linework touch-ups, The Stranger concept
 Chickenwhite (http://chickenwhite.tumblr.com/) (May be NSFW)

Inking, Shading, UI
 Stinkehund (https://stinkehund.deviantart.com/)

Rubber Guest Art
 Goop-Sinpai (https://www.deviantart.com/goop-sinpai/gallery/) (https://www.patreon.com/GoopSinpai)

Doll Guest Art
 InkyFluffDraws (https://www.patreon.com/inkyfluffsdraws/posts) (May be NSFW)

Sprites, Tiles
 Urimas "The Spaceman" Ebonheart (https://www.patreon.com/urimasebonheart)

Very Helpful Testers
 Klaysee, Aaaac, Trinity, and many others!

Based off of Chambers of Pandemonium
 By Pashaimeru


===================================================================================================
==                                         Audio Credits                                         ==
===================================================================================================
Much of this audio was taken from open-license libraries on www.OpenGameArt.org. Consider 
supporting them if you like what you heard!


--[ ======================================= Sound Effects ======================================= ]
Lokif (???) for the GUI SFX.
Kenney Vleugels (www.kenney.nl) for some of the combat SFX.
qat (???) for the weapon-pickup SFX.
Additional sound effects from https://www.zapsplat.com

