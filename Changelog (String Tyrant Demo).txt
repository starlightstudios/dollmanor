//--[Version 1.00]
*Initial Release

//--[Version 1.01]
*Added 'dark' and 'light' UI themes. You can toggle them from the in-game options menu.
*Added ending relive to the options menu. You can replay endings you've already unlocked.
*Increased text window text size. Can be toggled from the in-game options menu.
*Added itch.io manifest so the app doesn't launch the no-sound file.
*Fixed the tutorial text concerning doors.
*The 'think' handler in the tutorial will now display unique text.
*Fixed a bug that caused items to not appear on the locality window.
*Modified some colors to be easier on the eyes.
*Changed the dialogue for meeting Lauren if you have not met Pygmalie yet.
*Added a notification to talk to Jessie/Lauren the first time you meet them.
*Updated the 'think' text for the early part of the game to give a few more hints on what to do.

//--[Version 1.02]
*Fixed Jessie not having dialogue when spoken to while in the main hall as you go to find Lauren.
*Fixed several typos.
*Fixed storybooks not costing a turn to drop.
*Fixed incorrect door highlighting with mouseover for west/east doors in some rooms.
*Fullscreen, Dark Mode, and Large Text size options are now saved between playthroughs.
*You can now set doll type/color preference from the options menu after getting any game-over.
*Options are now split into their own configuration files.
*Added OSX/Linux ports.

//--[Version 1.02a]
*OSX port now uses local SDL dylib.
*Fixed a bug where the doll transformation images are not visible.

//--[Version 1.03]
*Fixed loading from title showing "Dogs" instead of "No files found" if no files are found.
*Fixed being unable to unequip gloves. Apparently, you just take them off. Also fixed it in the game.
*Fixed clicking the locality scrollbar affecting the text scrollbar.
*Fixed high numbered endings not displaying their description on the endings menu.
*Typing 'options', 'menu', or 'settings' will now open the in-game options menu.